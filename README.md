Notre [open data national (data.gouv.fr)](https://www.data.gouv.fr/fr/) est une source de cartes et de photographies du territoire. 

Un outil.

   - Mais il est compact, morcellé, aride : d'un d'emploi complexe.  
     L'on peut télécharger un fichier, mais ensuite, qu'en faire ?  

### Certes, l'open data est là, mais il y a loin de la coupe aux lèvres.

Pourtant, il propose des données sur tous les sujets. Complétées des vôtres, il permet des analyses avancées.  
il devrait vous suggérer des actions pertinentes.

   - Pour le stocker et l'observer, une base de données et un système d'information géographique sont utiles.

   - ainsi que quelques fonctions pour débuter nos analyses.

### C'est l'objet de ce projet open source

__Fournir un socle de données déjà associées entre-elles, organisées pour être très exploitables.__  
À destination des stastisticiens, chargés d'études, et informaticiens.

   - Il prépare des jeux de données (datasets) depuis notre open data national et local sur plusieurs thématiques :  
     entreprises présentes sur notre sol, comptes des collectivités, équipements et services communaux, revenus, besoins en main d'oeuvre...  

   - il les vérifie, les associe, et les rend comparables sur plusieurs années.    
     Puis, il les propose au format [Apache Parquet](https://parquet.apache.org/docs/), avec des tris et des déclinaisons pratiques.  

   - les champs qui portent un contenu de même nature, quelque soit le jeu de données dont ils proviennent, portent le même nom et ont le même type.

__Ces données acquises, il permet de les exploiter facilement.__
   - Un [Spark](https://spark.apache.org/) adjoint permet du data-engineering ou machine learning. 
   - Une base PostGIS, un serveur Elastic, un SIG Geoserver, 
   - ainsi qu'une IHM Angular de démarrage sont déjà connectés.    

__Vous pouvez débuter vos analyses, traitements statistiques ou cartographiques  
sans avoir à bâtir un système complexe__

---

## Les données qu'il extrait, nettoie, associe et rend comparables

(Suivez les liens pour le détail des principales fonctions proposées, la présentation des sources des données, et des remarques sur leur extraction ou exploitation)

- [La France est déclinée en Régions → Départements → Communes](docs/fonctions/cog.md), ainsi qu'en [intercommunalités, groupements (ex : syndicats) avec leurs communes membres (périmètres)](docs/fonctions/intercos_groupements_perimetres.md)    
  d'après le _Code Officiel Géographique_ de l'_INSEE_ et la _Base Nationale des Intercommunalités (BANATIC)_  
  Une attention particulière est portée sur la qualité des données d'outremer.  
  pour toutes les années de 2016 à 2024.
 
- Les données de [nos 3.8 millions d'entreprises](docs/fonctions/entreprises.md) sont accessibles par siren, géographiquement, ou par activité (code APE)  
  à partir de la base _SIRENE_ de l'_INSEE_  
  de 2018 à 2024.

- [La balance des comptes des communes et des intercommunalités](docs/fonctions/comptes.md),  
  et les comptes individuels pour leur analyse financière  
  sont extraites depuis les données de la _DGFIP_  
  de 2018 à 2022 (ces données sont publiées avec un certain délai).

- [Les commerces, équipements et services présents dans les communes](docs/fonctions/equipements.md)  
  sont extraits de la _Base Permanente des Equipements_ de l'_INSEE_  
  de 2018 à 2021 (en attente de données 2022 et +)

- [Les revenus et imposition des ménages français](docs/fonctions/revenus.md), par communes  
  de 2017 à 2022 (délai)

- [Les Besoins en Main d'Oeuvre (BMO)](docs/fonctions/emploi.md)  
  de 2019 à 2024  
  ainsi que des indicateurs de structure et d'activité de la population, d'après le recensement de la population en 2016  
  (en cours de réacquisition : l'INSEE a publié les résultats des recensements 2015, 2021)

- [La liste des associations existantes sur notre territoire](docs/fonctions/associations.md), géographiquement ou par thème social est accessible  
  extraite du _Répertoire National des Associations (RNA, Waldec)_  
  de 2019 à 2024  
  à employer avec prudence, car ces données sont réputées peu actualisées : de nombreuses associations, bien que marquées actives, n'existent souvent plus.

- Diplômes et formation (2016)  
  (en cours de réacquisition : recensement INSEE 2015, 2021)

__Catalogue des données open data souveraines__

- [La recherche des jeux de données est simplfiée par une indexation interne du catalogue](docs/fonctions/catalogue_datagouv.md) data.gouv.fr  
  (dernière extraction : 14 Août 2024)

Chaque des jeu de données a, pour toutes ses années, la même structure, permettant les comparaisons.  
Leurs anomalies (données manquantes, inconsistantes...) sont rapportées finement.

## À qui se destine t-il ?

- Aux statisticiens

  - qui ont besoin de données liées et nettoyées comme point de départ de leurs études

  - pour préparer des travaux d'analyse de données avancées (algorithmes avec ou sans apprentissage, machine learning, etc.), grâce à [Spark](https://spark.apache.org/) inclus

  - les données produites sont proposées sous forme de datasets [Parquet](https://parquet.apache.org/docs/), généralement partitionnés par départements, avec des tris de données initiaux les plus adaptés.
    
  - Des services REST permettent de recevoir ces données sous forme d'objets, si souhaité.

- Aux informaticiens, en data-engineering, pour constituer d'autres bases de données

  - Une application _Angular_ de départ, connectée, est proposée
  
    ![IHM simple](./ihm.png) 

  Sont également mis à disposition :

  - une base _PostGIS_ (l'extension SIG de _PostgreSQL_)
  - une instance de _Geoserver_
  - une base _Elastic_


  Ce projet est adapté à votre langage de développement de prédilection :

  - si vous utilisez _Java_, vous tirerez bénéfice des classes proposées, réutilisables.

  - si vous développez avec [Spark](https://spark.apache.org/) en _Python_, _Scala_ ou _R_, vous relirez sans adaptation les datasets [Parquet](https://parquet.apache.org/docs/) proposés, pour leur exploitation immédiate, et profiterez de nombreux exemples de requêtes.

  - si vous employez _Tensorflow_ ou d'autres outils, ou n'utilisez pas _Spark_, les datasets [Parquet](https://parquet.apache.org/docs/) seront votre point de départ, ou des interactions par requêtes REST.

---

## Données et datasets

Les datasets générés sont en cours de téléversement sur [data.gouv.fr](https://www.data.gouv.fr/fr/organizations/marc-le-bihan/#/datasets)

   - Ils y sont placés avec le tri et la sélection de données jugée la plus utile
   - Pour en obtenir d'autres, il faut les produire depuis les données open data brutes, comme décrit dans chaque section, en appelant les méthodes REST de chargement proposées par l'application, par Swagger UI/OpenAPI.  


## Les motivations de ce projet

[L'économie du territoire, son développement, lui ont servi de fil directeur initial](./docs/projet/collectivites/sujets.md)  
Un premier socle de données, de nature plutôt économique, a été placé à côté de lui. 

Mais d'autres de toute nature : environnement, santé, logement, mobilité... peuvent être adjointes et analysées.

---

# Installation de l'application

Deux modes de fonctionnement sont disponibles aujourd'hui :

- [Vagrant](./deploiement/vagrant_sans_composants/README.md)
- [Local](./deploiement/local/README.md)


Lorsque l'application est démarrée :

- Sa documentation (et appels d'expérimentation) sont accessibles sur [http://localhost:9090/swagger-ui.html](http://localhost:9090/swagger-ui.html)

- Les déclarations OpenAPI (json schema) des méthodes qu'elle propose et objets qu'elle met à disposition, depuis : [http://localhost:9090/v3/api-docs](http://localhost:9090/v3/api-docs)
