Déploiements
------------

Plusieurs possibilités de déploiement sont disponibles ou en mise au point :

- [Local (disponible)](./local) : déploiement local pour le développement

- [vagrant sans composants (disponible)](./vagrant_sans_composants) : équivalent d'un déploiement de développement, c'est à dire : "_Tout sur la même machine_", mais dans une VM à part. À des fins de recette ou d'installation rapide

- [docker (en cours)](./docker) déploiement en `docker compose` pour des tests containerisés des modules répartis en composants/noeuds
- [vagrant kubernetes (en cours)](./vagrant_kubernetes) : déploiement par kubernetes [minikube] 
