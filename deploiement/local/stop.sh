#!/bin/bash
# Arrêt, en mode développement, des composants de l'application
source ecoemploi-start-stop-common.sh

emit "general" "stop" "ending" "Arrêt des sous-systèmes ecoemploi" ""

# Arrêt de l'IHM (front Angular)
emit "application" "stop" "ending" "Demande d'arrêt de l'IHM (front)" ""

if ! STOP_ECOEMPLOI_FRONT_IHM=$(sudo systemctl stop ecoemploi-front-ihm.service); then
  emit "application" "stop" "fail" "L'arrêt de l'IHM (front) a échoué" "$STOP_ECOEMPLOI_FRONT_IHM"
  log "WARN" "L'arrêt du front Angular a échoué : $STOP_ECOEMPLOI_FRONT_IHM"
else
  log "INFO" "Le front Angular s'est arrêté. $STOP_IHM_FRONT"
fi

# Arrêt de l'IHM (backend Java)
emit "application" "stop" "ending" "Demande d'arrêt de l'IHM (backend)" ""

if ! STOP_ECOEMPLOI_BACK_IHM=$(sudo systemctl stop ecoemploi-back-ihm.service); then
  emit "application" "stop" "fail" "L'arrêt de l'IHM (backend) a échoué" "$STOP_ECOEMPLOI_BACK_IHM"
  log "WARN" "L'arrêt du backend ihm (Java) a échoué : $STOP_ECOEMPLOI_BACK_IHM"
else
  log "INFO" "Le backend ihm s'est arrêté. $STOP_IHM_BACK"
fi

# Arrêt du moteur (backend Java)
emit "application" "stop" "ending" "Demande d'arrêt du moteur métier (backend)" ""

if ! STOP_ECOEMPLOI_BACK_METIER=$(sudo systemctl stop ecoemploi-back-metier.service); then
  emit "application" "stop" "fail" "L'arrêt du moteur (backend) a échoué" "$STOP_ECOEMPLOI_BACK_METIER"
  log "WARN" "L'arrêt du moteur (backend Java) a échoué : $STOP_ECOEMPLOI_BACK_METIER"
else
  log "INFO" "Le moteur (backend Java) s'est arrêté."
fi

# Arrêt d'ELK
emit "elk" "stop" "ending" "Arrêt d'ELK" ""

# Filebeat
log "INFO" "Arrêt de Filebeat..."

if ! STOP_FILEBEAT=$(sudo systemctl stop filebeat); then
   emit "elk" "stop" "fail" "Arrêt de Filebeat échoué" "$STOP_FILEBEAT"
   log "ERROR" "Filebeat ne s'est pas arrêté : $STOP_FILEBEAT"
else
   log "INFO" "Filebeat s'est arrêté."
fi

# Logstash
log "INFO" "Arrêt de Logstash..."

if ! STOP_LOGSTASH=$(sudo systemctl stop logstash); then
   emit "elk" "stop" "fail" "Arrêt de Logstash échoué" "$STOP_LOGSTASH"
   log "ERROR" "Logstash ne s'est pas arrêté : $STOP_LOGSTASH"
else
   log "INFO" "Logstash s'est arrêté."
fi

# Kibana
log "INFO" "Arrêt de Kibana..."

if ! STOP_KIBANA=$(sudo systemctl stop kibana); then
   emit "elk" "stop" "fail" "Arrêt de Kibana échoué" "$STOP_KIBANA"
   log "ERROR" "Kibana ne s'est pas arrêté : $STOP_KIBANA"
else
   log "INFO" "Kibana s'est arrêté."
fi

# Elasticsearch
log "INFO" "Arrêt d'Elasticsearch"

if ! STOP_ELASTIC=$(sudo systemctl stop elasticsearch); then
   emit "elk" "stop" "fail" "Arrêt d'Elasticsearch échoué" "$STOP_ELASTIC"
   log "ERROR" "Elasticsearch ne s'est pas arrêté : $STOP_ELASTIC"
else
   log "INFO" "Elasticsearch s'est arrêté."
fi

# Arrêt de geoserver
emit "geoserver" "stop" "ending" "Demande d'arrêt de geoserver" ""

if ! SIOP_GEOSERVER=$(sudo systemctl stop geoserver); then
  emit "geoserver" "stop" "fail" "L'arrêt de geoserver a échoué" "$SIOP_GEOSERVER"
  log "WARN" "L'arrêt de Geoserver a échoué : $SIOP_GEOSERVER"
else
  log "INFO" "Geoserver s'est arrêté."
fi

# Arrêt de Kafka et Zookeeper si l'option kafka est présente dans l'un des paramètres
if [ "$*" == "kafka" ]; then
  emit "general" "stop" "ending" "Demande d'arrêt de Kafka" ""

  kafka-server-stop.sh || zookeeper-server-stop.sh
  log "INFO" "Zookeeper et Kafka se sont stoppés."
fi
