#!/bin/bash
# Démarrrage, en mode développement, des composants de l'application
source ecoemploi-start-stop-common.sh

# Démarrage de Zookeeper et de Kafka
demarrer_kafka() {
  # Démarrage partie Zookeeper
  ZOOKEEPER_PORT=2181
  DELAI_ATTENTE=0.1

  log "INFO" "Démarrage de Zookeeper puis de Kafka..."
  zookeeper-server-start.sh -daemon "$KAFKA_HOME/config/zookeeper.properties" &&
  while ! nc -z localhost $ZOOKEEPER_PORT; do sleep $DELAI_ATTENTE; done

  # Démarrage partie Kafka
  if ! MESSAGE_DEMARRAGE_KAFKA=$(kafka-server-start.sh -daemon "$KAFKA_HOME/config/server.properties"); then
    log "ERROR" "Echec du démarrage de Kafka : $MESSAGE_DEMARRAGE_KAFKA"
    exit $?
  fi

  while ! nc -z localhost "$KAFKA_BOOTSTRAP_SERVER_PORT"; do sleep $DELAI_ATTENTE; done

  log "INFO" "Zookeeper et Kafka ont démarré."
}

# Créer le topic de vivacité de l'application
creer_topic_vie() {
  # Vérifier la nécessité de créer le topic
  if ! LIST_TOPIC=$(kafka-topics.sh --bootstrap-server=localhost:9092 --list); then
    log "ERROR" "Kafka $BOOTSTRAP_SERVER a échoué à être interrogé sur ses topics : $LIST_TOPIC"
    exit $?
  fi

  TOPIC_VIE_EXISTE=$(echo "$LIST_TOPIC" | grep -F "$TOPIC_VIE")

  # Si nécessaire, le créer
  if [ -z "$TOPIC_VIE_EXISTE" ]; then
    log "INFO" "Création du $TOPIC_VIE sur Kafka $BOOTSTRAP_SERVER..."

    if ! MESSAGE_CREATE_TOPIC_VIE=$(kafka-topics.sh --bootstrap-server "$BOOTSTRAP_SERVER" --create --topic "$TOPIC_VIE" --partitions 1 --replication-factor 1); then
      log "ERROR" "Echec de la création du topic $TOPIC_VIE de Kafka $BOOTSTRAP_SERVER : $MESSAGE_CREATE_TOPIC_VIE"
      exit $?
    fi

    log "INFO" "Le topic $TOPIC_VIE a été créé sur Kafka $BOOTSTRAP_SERVER."
  else
    log "INFO" "Le topic $TOPIC_VIE existant sera réutilisé sur Kafka $BOOTSTRAP_SERVER."
  fi
}

# Démarrer Postgresql
gestion_postgresql() {
  POSTGRESQL_HOST="localhost"
  POSTGRESQL_PORT="5434"

  (controlled_service_start "postgresql.service" "Postgresql" "sgbd") || exit $?

  # Attendre la notification de Postgres qu'il est effectivement prêt, un certain temps
  while : ; do
    if MESSAGE_POSTGRESQL_IS_READY=$(pg_isready -h "$POSTGRESQL_HOST" -p "$POSTGRESQL_PORT"); then
      break
    fi

    sleep $DELAI_ATTENTE;
  done

  if [ $? -ne 0 ]; then
    emit "sgbd" "start" "fail" "Démarrage de la base de données" "$MESSAGE_POSTGRESQL_IS_READY"
    log "ERROR" "Postgresql n'a pas réussi à démarrer : $MESSAGE_POSTGRESQL_IS_READY"
    exit $?
  else
    emit "sgbd" "start" "success" "Démarrage de la base de données" "$MESSAGE_POSTGRESQL_IS_READY"
    log "INFO" "Postgresql est prêt."
  fi
}

# Démarrer les applications eco emploi : backend métier, backend du front (Java), et front (Angular)
demarrer_applications() {
  (controlled_service_start "ecoemploi-back-metier.service" "Ecoemploi, backend métier Java/Spark" "application") || exit $?
  (controlled_service_start "ecoemploi-back-ihm.service" "Ecoemploi, backend ihm Java" "application") || exit $?
  (controlled_service_start "ecoemploi-front-ihm.service" "Ecoemploi, front ihm Angular" "application") || exit $?
}

# Démarrer ELK
demarrer_elastic() {
  (controlled_service_start "elasticsearch" "Elasticsearch" "elk") || exit $?
  (controlled_service_start "kibana" "Kibana" "elk") || exit $?
  (controlled_service_start "logstash" "Logstash" "elk") || exit $?
  (controlled_service_start "filebeat" "Filebeat" "elk") || exit $?
}

# Démarrer Geoserver
demarrer_geoserver() {
  export GEOSERVER_DATA_DIR=/data/comptes-france/geoserver
  (controlled_service_start "geoserver" "Geoserver" "geoserver") || exit $?
}

# Démarrer Zookeeper en tâche de fond, avec -daemon, et attendre qu'il soit prêt, puis créer (si requis) son topic de suivi de démarrage
demarrer_kafka
creer_topic_vie
emit "general" "init" "start" "Initialisation des scripts" ""
gestion_postgresql
demarrer_geoserver
demarrer_elastic
demarrer_applications
