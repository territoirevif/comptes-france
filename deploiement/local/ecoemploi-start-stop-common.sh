#!/bin/bash
# Partie commune des scripts de démarrage et d'arrêt de l'application, en mode dev

# Communication avec Kafka
KAFKA_BOOTSTRAP_SERVER_PORT=9092
BOOTSTRAP_SERVER="localhost:$KAFKA_BOOTSTRAP_SERVER_PORT"
TOPIC_VIE="ecoemploi-statut"

# Logger un message, horodaté
# $1 Sevérité
# $2 Message
log() {
   TIMESTAMP=$(date "+%Y-%m-%d %H:%M:%S")

   if [ "$1" = "ERROR" ]; then
      echo "$TIMESTAMP [$1] $2" >&2
   else
      echo "$TIMESTAMP [$1] $2"
   fi
}

# Emettre un évènement dans le $TOPIC_VIE de Kafka
# $1 : Composant
# $2 : Phase : init, start
# $3 : Statut : start, fail
# $4 : description
# $5 : Message
emit() {
# Laisser la ligne ci-dessous non indentée pour que EOF fonctionne.
MESSAGE_EMIT_KAFKA=$(cat << EOF
{"composant":"$1","phase":"$2","statut":"$3","description":"$4","message":"$5"}
EOF
)

  if ! MESSAGE_EMISSION_VERS_KAFKA=$(echo "$MESSAGE_EMIT_KAFKA" | kafka-console-producer.sh --broker-list $BOOTSTRAP_SERVER --topic $TOPIC_VIE); then
    log "WARN" "Le message vers le topic $TOPIC_VIE n'a pu être émis : $MESSAGE_EMISSION_VERS_KAFKA"
  fi
}

# Démarrer de manière contrôlée, un service
# $1 : Nom du service (systemd) que l'on veut démarrer
# $2 : Description (pour les logs, Kafka...) de ce service
# $3 : Code du composant pour le message Kafka
controlled_service_start() {
  log "INFO" "Démarrage de $2 ($1)"
  emit "$3" "start" "starting" "$2 ($1)" ""

  if STARTING_RESPONSE=$(sudo systemctl start "$1"); then
    sleep 3;

    if ! (sudo systemctl is-active "$1" --quiet) then
       emit "$3" "start" "fail" "Echec du démarrage du service de $2 ($1)" "$STARTING_RESPONSE"
       log "ERROR" "Echec du démarrage de $2 ($1) : $STARTING_RESPONSE"
       exit $?
    fi
  else
     emit "$3" "start" "fail" "Echec du démarrage de $2 ($1)" "$STARTING_RESPONSE"
     log "ERROR" "Echec du démarrage de $2 ($1) : $STARTING_RESPONSE"
     exit $?
  fi

  emit "$3" "start" "started" "$2 ($1)" ""
}
