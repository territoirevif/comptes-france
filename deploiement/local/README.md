Déploiement local
-----------------

Le mode de déploiement local est destiné aux développeurs qui désirent mettre au point l'application ou le traitement des données sur leur ordinateur local.

   - La procédure d'installation complète est décrite ci-dessous, elle peut être facilitée par l'emploi de scripts bash, présents dans le répertoire [common](../common)
   - Les commandes `start.sh` et `stop.sh` démarrent ou stoppent l'application

Toutes les commandes pour l'installer entièrement depuis un ordinateur vide (dans cet exemple, sous Debian)

## Installations requises

Ces commandes doivent être exécutées et les applications ci-dessous installées, pour que _ecoemploi_ puisse fonctionner.  
Elles installent:

- Les clefs et les certificats
- Java 17
- Postgres 15 et Postgis 3
- NodeJS 16 et Angular 14
- ELK 8.x
- Geoserver 2.23

### Mise à jour des clefs et certificats

```bash
sudo apt install gnupg2 wget ca-certificates
sudo apt-get update
```

### Java, Vim

```bash
sudo apt-get install openjdk-17-jdk
```

### Postgresql 15 et Postgis 3.x

Le choix de Postgresql est imposé parce que l'installation du package `postgis` provoque la mise à jour du Postgresql installé dans la dernière version.

```bash
echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list

sudo apt-get update
sudo apt -y install gnupg2
sudo apt-get update

sudo apt -y install postgresql-15 postgresql-client-15
sudo apt -y install postgis postgresql-15-postgis-3 postgresql-15-postgis-3-scripts
```

### NodeJS et Angular

```bash
sudo curl -fsSL https://deb.nodesource.com/setup_16.x | sudo bash -
sudo apt-get install -y nodejs
sudo npm install -g @angular/cli
```

### ELK

```bash
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update && sudo apt-get install elasticsearch
sudo systemctl start elasticsearch.service
```

Depuis le répertoire `/usr/share/elasticsearch/bin/` :

```bash
sudo ./elasticsearch-plugin install ingest-attachment
```

Lors des updates d'Elastic, il peut empêcher son redémarrage.  
Dans ce cas, exécuter un `sudo /usr/share/elasticsearch/bin/elasticsearch-plugin remove ingest-attachment` et relancer ensuite sa commande d'installation.

Installation de logstash, kibana et filebeat

```bash
sudo apt-get install logstash
sudo apt-get install kibana
sudo apt-get install filebeat
```

En version 8.x d'Elastic (si un upgrade a eu lieu depuis une 7.x auparavant ?), il est possible que Kibana ne démarre pas.

Il faut éditer `kibana.service` (dans `/etc/systemd/system`) et y supprimer dans l'ExecStart:
l'option `--logging.dest="/var/log/kibana/kibana.log"` qui provoque un incident.


Avant le premier démarrage d'ELK, ces commandes sont nécessaires:

```bash
sudo systemctl daemon-reload
sudo systemctl enable elasticsearch.service
```

Attention! La version 8.9 va instituer un mot de passe.  
Exemple: Ox-JoQT0po=pdOT-LeE*

Il est connu seulement à l'installation, ou en réinitialisant le mot de passe par:

```bash
sudo /usr/share/elasticsearch/bin/elasticsearch-reset-password -u elastic
```

Kibana doit se connecter à _Elastic_, mais ne peut pas utiliser le compte `elastic`, qui est admin, pour cela.

Il faut en créer un autre:

```bash
curl -XPOST "http://localhost:9200/_security/role/kib_role" \
    -H 'Content-Type: application/json' -u elastic:Ox-JoQT0po=pdOT-LeE* \
    -d'{ \ 
       "cluster" : ["monitor", "read_ilm", "manage_ilm", "manage_index_templates", "manage", "read_security", "manage_security"], \
       "indices" : [{ "names" : ["*"], "privileges" : ["create_index", "read", "write", "create", "manage", "view_index_metadata" ], \
       "allow_restricted_indices" : true }], \
       "applications" : [ ], "run_as" : [ ], "metadata" : { }, "transient_metadata" : { "enabled" : true}}'
 
curl -XPOST "http://localhost:9200/_security/user/kib" -H 'Content-Type: application/json'  -u elastic:Ox-JoQT0po=pdOT-LeE* \
    -d'{ "password": "kibananas",  "enabled": true,  "roles": [ "kib_role" ], "full_name": "Kibana default user",  "email": "t@dummy.com",  "metadata" : { "intelligence": 7  }}'
```

et le paramétrer dans `/etc/systemd/kibana.yaml`

```yaml
# =================== System: Elasticsearch ===================
# The URLs of the Elasticsearch instances to use for all your queries.
elasticsearch.hosts: ["http://localhost:9200"]

# If your Elasticsearch is protected with basic authentication, these settings provide
# the username and password that the Kibana server uses to perform maintenance on the Kibana
# index at startup. Your Kibana users still need to authenticate with Elasticsearch, which
# is proxied through the Kibana server.
elasticsearch.username: "kib"
elasticsearch.password: "kibananas"
```

### Kafka 3.4

```bash
cd /tmp && wget https://downloads.apache.org/kafka/3.4.0/kafka_2.12-3.4.0.tgz
sudo tar -xvf kafka_2.12-3.4.0.tgz -C /opt
export KAFKA_HOME="/opt/kafka_2.12-3.4.0"
```

### Geoserver

```bash
sudo unzip geoserver-2.23.0-bin.zip -d /opt/geoserver
sudo chown $(whoami):$(whoami) -R /opt/geoserver
echo "export GEOSERVER_HOME=/opt/geoserver" >> ~/.profile
```

Créer un [geoserver.service (systemd)](https://gitlab.com/territoirevif/comptes-france/-/blob/master/deploiement/local/systemd/geoserver.service) pour faciliter son lancement

## Flask

```bash
sudo apt install python3.11-venv
python3 -m venv .venv
. .venv/bin/activate
pip install Flask
```

Remarque : `.  .venv/bin/activate` sélectionne un environnement  
et son exécution est obligatoire avant toute commande flask.

Par exemple, avant un `flask --app hello run`

## Outils pour tester différents modes de déploiement

### Kubernetes

```bash
sudo apt install -y gnupg2
sudo apt -y install curl apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt update
sudo apt -y install vim git curl wget kubelet kubeadm kubectl
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
sudo swapoff -a
sudo modprobe overlay
sudo modprobe br_netfilter
sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

sudo sysctl --system
```

### Virtualbox et Vagrant

```bash
sudo apt-get install virtualbox
sudo apt-get install vagrant
```

### Docker CE et docker compose

_Vagrant_ et _Kubernetes_ sont préférés.

```bash
sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo docker run hello-world

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose -version
```

## Pour l'environnement de développement

### Git, Maven

```bash
sudo apt-get install git
sudo apt install maven
```

# Outils optionnels, mais recommandés

## Gestionnaire SGBD : DBeaver

```bash
echo "deb https://dbeaver.io/debs/dbeaver-ce /" | sudo tee /etc/apt/sources.list.d/dbeaver.list
curl -fsSL https://dbeaver.io/debs/dbeaver.gpg.key | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/dbeaver.gpg
sudo apt-get update
sudo apt install dbeaver-ce
```

## Installation d'Anaconda (extension Python)

```bash
wget https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh
chmod +x Anaconda3-2021.05-Linux-x86_64.sh 
conda config --set changeps1 False
```

## Installation de pandoc (pour Markdown)

```bash
wget https://github.com/jgm/pandoc/releases/download/2.14.0.1/pandoc-2.14.0.1-1-amd64.deb
sudo dpkg -i pandoc-2.14.0.1-1-amd64.deb

sudo apt-get install pdflatex
sudo apt-get install texlive-latex-base
sudo apt-get install texlive-fonts-recommended
sudo apt-get install texlive-fonts-extra
```

# Outils optionnels

### Gestionnaire SGBD : Pgadmin 4

```bash
sudo curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'
sudo apt install pgadmin4
```

### QGIS

```bash
sudo apt install gnupg software-properties-common
wget -qO - https://qgis.org/downloads/qgis-2022.gpg.key | sudo gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/qgis-archive.gpg --import
sudo chmod a+r /etc/apt/trusted.gpg.d/qgis-archive.gpg
sudo add-apt-repository "deb https://qgis.org/debian $(lsb_release -c -s) main"
sudo apt update
sudo apt install qgis qgis-plugin-grass
```

### Notepad++

```bash
sudo apt install snapd
sudo snap install core
sudo snap install snapd
sudo snap refresh
sudo snap install notepad-plus-plus
```

### Vim

```bash
sudo apt-get install vim
```

### rsync

```bash
sudo apt-get install rsync
```

### Installation de Jupyter

```bash
pip install jupyterlab
pip install notebook
```

Editer le git config pour y mettre son e-mail, nom, et réglages de crlf.

# Outils prochainement obsolètes

Présents sur des installations passées, ils seront déclassés.

## Zeppelin et son Spark associé (Abandon prévu)

\underline{Revert prochain :}

_Apache Zeppelin_ est un projet opensource qui ne paraît plus être vivant.  
Il sera remplacé au profit de _Jupyter_.

```bash
# Dezip et move dans /opt avec Path réglé
tar xvf zeppelin-0.10.1-bin-all.tgz -C /home/lebihan/Téléchargements/
sudo mv zeppelin-0.10.1-bin-all /opt

wget https://downloads.apache.org/spark/spark-3.1.1/spark-3.1.1-bin-hadoop2.7.tgz 
tar xvf spark-3.1.1-bin-hadoop2.7.tgz 
sudo mv spark-3.1.1-bin-hadoop2.7 /opt
```
