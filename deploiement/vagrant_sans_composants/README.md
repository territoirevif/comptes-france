Installation sur machine virtuelle Vagrant
==========================================

Le mode de déploiement _Vagrant sans composants_ crée une machine virtuelle (VM) par Vagrant,  
copie très proche de l'environnement local de développement.

Elle est :

- sans noeuds : tous ses constituants sont disposés "_à plat_", comme sur une machine de développement
- son répertoire `data` (données brutes, datasets Parquet et CSV produits) sont partagés avec l'hôte

### Construction de la VM Vagrant

Depuis le répertoire `deploiement/vagrant_sans_composants` :

```bash
(source ../common/010_Versions.sh && vagrant up)
```

### Remarque

Ne pas oublier d'ajouter à l'image _Vagrant_ de la RAM et du CPU

   ```yaml
   config.vm.provider "virtualbox" do |v|
     v.memory = 8192
     v.cpus = 10
   end
   ```
