echo "Création du déploiement Kubernetes pour ecoemploi"
cp /vagrant/ecoemploi_images/backend-java-metier/docker_image-backend-java-metier.tar docker_image-backend-java-metier.tar

kubectl apply -f /vagrant/ecoemploi_scripts/ecoemploi-namespace.yaml
kubectl apply -f /vagrant/ecoemploi_scripts/ecoemploi-postgis-configmap.yaml
kubectl apply -f /vagrant/ecoemploi_scripts/ecoemploi-postgis-pv-pvclaim.yaml

# TODO : Un "kubectl get pvc --namespace ecoemploi" vérifiera leur bonne liaison
kubectl apply -f /vagrant/ecoemploi_scripts/ecoemploi-postgis-sgbd.yaml
kubectl apply -f /vagrant/ecoemploi_scripts/ecoemploi-postgis-service.yaml

# La commande qui suit doit permettre d'entrer dans psql
# kubectl exec -it [pod] --namespace ecoemploi --  psql -h localhost -U postgres --password -p 5432 comptesfrance

kubectl apply -f /vagrant/ecoemploi_scripts/ecoemploi-kafka-zookeeper.yaml
kubectl apply -f /vagrant/ecoemploi_scripts/ecoemploi-kafka-broker.yaml

# Installation des applications
docker load -i docker_image-backend-java-metier.tar
minikube image load ecoemploi/backend-java-metier
kubectl apply -f /vagrant/ecoemploi_scripts/ecoemploi-application-backend-metier.yaml
