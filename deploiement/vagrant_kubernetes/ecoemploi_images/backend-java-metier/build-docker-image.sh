#!/bin/bash
echo "Construction de l'image : Backend Java Métier..."
BACKEND_JAVA_HOME="$ECOEMPLOI_HOME/metier-et-gestion/AppMetierEtGestion/target"

# Supprimer les liens vers une construction précédente
rm -rf ./backend-java-metier/application-metier-et-gestion.jar || true

if [ -f "$BACKEND_JAVA_HOME/application-metier-et-gestion.jar" ]; then
   ln "$BACKEND_JAVA_HOME/application-metier-et-gestion.jar" ./backend-java-metier/application-metier-et-gestion.jar
fi

rm -rf ./backend-java-metier/application.properties || true

if [ -f "$BACKEND_JAVA_HOME/classes/application.properties" ]; then
   ln "$BACKEND_JAVA_HOME/classes/application.properties" ./backend-java-metier/application.properties
fi

rm -rf ./backend-java-metier/application-sparkdocker.properties || true

if [ -f "$BACKEND_JAVA_HOME/classes/application-sparkdocker.properties" ]; then
   ln "$BACKEND_JAVA_HOME/classes/application-sparkdocker.properties" ./backend-java-metier/application-sparkdocker.properties
fi

sudo docker build ./backend-java-metier -t ecoemploi/backend-java-metier
