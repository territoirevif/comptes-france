Installation par Kubernetes
===========================

Installation de l'application par _Kubernetes_.  
Avec possibilité de le faire dans une VM _Vagrant_, pour faciliter la mise au point.

## Prérequis

Cette procédure d'installation dépend d'un _Docker hub_, car des `docker save` / `docker load` ont lieu. 

### Docker hub

Repository privé :

- namespace: mlebihan
- nom: ecoemploi


```bash
# docker tag local-image:tagname new-repo:tagname
# docker push new-repo:tagname

docker push mlebihan/ecoemploi:tagname
```


# Lancement de l'environnement Vagrant-Kubernetes

```bash
vagrant up
export KUBECONFIG=$ECOEMPLOI_HOME/deploiement/vagrant_kubernetes/configs/config
kubectl proxy
```


## Dashboard Kubernetes

Ouvrir un navigateur sur __dashboard Kubernetes__ :

[http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/overview?namespace=kubernetes-dashboard](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/overview?namespace=kubernetes-dashboard)

et s'authentifier avec le token contenu dans le fichier :  
`cat $ECOEMPLOI_HOME/deploiement/vagrant_kubernetes/configs/token`
