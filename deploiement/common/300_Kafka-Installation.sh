#!/bin/bash
if [ -n "$1" ]; then
  KAFKA_VERSION="$1"
fi

echo "300 : Installation de Kafka ${KAFKA_VERSION} basé sur Scala ${KAFKA_SCALA_VERSION}, mode update : ${KAFKA_UPDATE}"
export DEBIAN_FRONTEND=noninteractive

# Vérifier les paramètres obligatoires
if [ -z "${KAFKA_VERSION}" ]; then
  echo "Précisez la version de Kafka à installer dans la variable d'environnement KAFKA_VERSION. [Recommandée : 3.7.2]" >&2;
  exit 1;
fi

if [ -n "$2" ]; then
  KAFKA_SCALA_VERSION="$2"
fi

if [ -z "${KAFKA_SCALA_VERSION}" ]; then
  echo "Précisez la version de Scala liée dans la variable d'environnement KAFKA_SCALA_VERSION. [Recommandée : 2.13]" >&2;
  exit 1;
fi

if [ -n "$3" ]; then
  KAFKA_UPDATE="$3"
fi

if [ "${KAFKA_UPDATE}" != "true" ] && [ "${KAFKA_UPDATE}" != "false" ]; then
  echo "Indiquez si Kafka est en installation initiale (false) ou update (true) dans la variable d'environnement KAFKA_UPDATE. ${KAFKA_UPDATE} n'est pas accepté." >&2;
  exit 1;
fi

update=${KAFKA_UPDATE}

if [ "$update" == "false" ]; then
   cd /tmp && wget -q "https://downloads.apache.org/kafka/${KAFKA_VERSION}/kafka_${KAFKA_SCALA_VERSION}-${KAFKA_VERSION}.tgz"
   sudo tar -xf "kafka_${KAFKA_SCALA_VERSION}-${KAFKA_VERSION}.tgz" -C /opt
   sudo chown vagrant:vagrant -R "/opt/kafka_${KAFKA_SCALA_VERSION}-${KAFKA_VERSION}"
   echo "export KAFKA_HOME=/opt/kafka_${KAFKA_SCALA_VERSION}-${KAFKA_VERSION}" >> ~/.profile
else
   echo "En mode update, Kafka ne se réinstallera pas."
fi

export KAFKA_HOME=/opt/kafka_${KAFKA_SCALA_VERSION}-${KAFKA_VERSION}
