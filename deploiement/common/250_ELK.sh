#!/bin/bash
if [ -n "$1" ]; then
  ELASTIC_VERSION="$1"
fi

if [ -n "$2" ]; then
  ELASTIC_UPDATE="$2"
fi

echo "250 : Installation d'ELK ${ELASTIC_VERSION}, mode update : ${ELASTIC_UPDATE}"
export DEBIAN_FRONTEND=noninteractive

if [ -z "${ELASTIC_VERSION}" ]; then
  echo "Précisez la version d'ELK à installer dans la variable d'environnement ELASTIC_VERSION. [Recommandée : 8.x]" >&2;
  exit 1;
fi

if [ "${ELASTIC_UPDATE}" != "true" ] && [ "${ELASTIC_UPDATE}" != "false" ]; then
  echo "Indiquez si ELK est en installation initiale (false) ou update (true) dans la variable d'environnement ELASTIC_UPDATE. ${ELASTIC_UPDATE} n'est pas accepté." >&2;
  exit 1;
fi

update=${ELASTIC_UPDATE}

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/${ELASTIC_VERSION}/apt stable main" | sudo tee "/etc/apt/sources.list.d/elastic-${ELASTIC_VERSION}.list"
sudo apt-get -y -qq update && sudo apt-get -y -qq install elasticsearch -o Dpkg::Progress-Fancy="0" -o Dpkg::Use-Pty="0"

echo "ELK : Installation de logstash"
sudo apt-get install logstash

echo "ELK : Installation de kibana"
sudo apt-get install kibana

echo "ELK : Installation de filebeat"
sudo apt-get install filebeat

sudo systemctl daemon-reload
sudo systemctl enable elasticsearch.service
sudo systemctl start elasticsearch.service

if [ "$update" == "true" ]; then
  echo "ELK : Déinstallation du précédent plugin ingest"
  (cd /usr/share/elasticsearch/bin/ || exit 1; sudo ./elasticsearch-plugin remove ingest-attachment --silent)
fi

echo "ELK : Installation du plugin ingest"
(cd /usr/share/elasticsearch/bin/ || exit 1; sudo ./elasticsearch-plugin install ingest-attachment --silent --batch)

# Attention! La version 8.x va instituer un mot de passe pour pouvoir se connecter sur la console.
# Exemple: Ox-JoQT0po=pdOT-LeE*

# Il est connu seulement à l'installation, ou en réinitialisant le mot de passe par:
# sudo /usr/share/elasticsearch/bin/elasticsearch-reset-password -u elastic
