#!/bin/bash
# $1: Version de Python
echo "110 : Installation de Python Java $1"
export DEBIAN_FRONTEND=noninteractive

if [ -z "$1" ]; then
  echo "Précisez la version de Python à installer. [Recommandée : 3.11]" >&2;
  exit 1;
fi

# gcc, g++, make sont recommandés pour nodejs et d'autres outils
#sudo apt-get -y -qq install "openjdk-$1-jdk" "gcc" "g++" "make" -o Dpkg::Progress-Fancy="0" -o Dpkg::Use-Pty="0"
