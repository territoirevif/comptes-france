#!/bin/bash
if [ -n "$1" ]; then
  TOMCAT_VERSION="$1"
fi

if [ -n "$2" ]; then
  TOMCAT_MAJOR_VERSION="$2"
fi

if [ -n "$3" ]; then
  TOMCAT_ADMIN_USER="$3"
fi

if [ -n "$4" ]; then
  TOMCAT_ADMIN_PASSWORD="$4"
fi

echo "500 : Installation d'Apache Tomcat ${TOMCAT_VERSION} (majeure : ${TOMCAT_MAJOR_VERSION}), admin user : ${TOMCAT_ADMIN_USER}, mode update : ${TOMCAT_UPDATE}"
export DEBIAN_FRONTEND=noninteractive

if [ -z "${TOMCAT_VERSION}" ]; then
  echo "Précisez la version d'Apache Tomcat à installer dans la variable d'environnement TOMCAT_VERSION. [Recommandée : 10.1.29]" >&2;
  exit 1;
fi

if [ -z "${TOMCAT_MAJOR_VERSION}" ]; then
  echo "Précisez la version majeure d'Apache Tomcat à installer dans la variable d'environnement TOMCAT_MAJOR_VERSION. [Recommandée : 10]" >&2;
  exit 1;
fi

if [ -z "${TOMCAT_ADMIN_USER}" ]; then
  echo "Précisez le profil utilisateur d'admininistration tomcat dans la variable d'environnement TOMCAT_ADMIN_USER." >&2;
  exit 1;
fi

if [ -z "${TOMCAT_ADMIN_PASSWORD}" ]; then
  echo "Précisez le mot de passe utilisateur d'admininistration tomcat dans la variable d'environnement TOMCAT_ADMIN_PASSWORD." >&2;
  exit 1;
fi

if [ -n "$5" ]; then
  TOMCAT_UPDATE="$5"
fi

if [ "${TOMCAT_UPDATE}" != "true" ] && [ "${TOMCAT_UPDATE}" != "false" ]; then
  echo "Indiquez si Tomcat est en installation initiale (false) ou update (true) dans la variable d'environnement TOMCAT_UPDATE. ${TOMCAT_UPDATE} n'est pas accepté." >&2;
  exit 1;
fi

update=${TOMCAT_UPDATE}

# Téléchargement et décompresssion
if [ "$update" == "false" ]; then
  wget -q "https://dlcdn.apache.org/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz" -O "apache-tomcat-${TOMCAT_VERSION}.tar.gz"
  sudo mkdir /opt/tomcat
  sudo tar -xf "apache-tomcat-${TOMCAT_VERSION}.tar.gz" -C /opt/tomcat --strip-components=1
fi

# Création de l'utilisateur et du groupe tomcat
if [ "$update" == "false" ]; then
  sudo groupadd tomcat
  sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat
fi

# Attribution de /opt/tomcat à l'utilisateur/groupe tomcat.
# Rendre les .sh exécutables : le *.sh ne sera pas accepté sans sh -c
if [ "$update" == "false" ]; then
  sudo sh -c 'chmod +x /opt/tomcat/bin/*.sh'
  sudo chown -R tomcat: /opt/tomcat
fi

# Premier démarrage après rechargement des services
sudo cp systemd/tomcat.service /etc/systemd/system
sudo systemctl daemon-reload

# Définition de l'utilisateur administrateur
sudo cp common/tomcat-users.xml /opt/tomcat/conf
sudo sed -i.backup -r "s/userx/${TOMCAT_ADMIN_USER}/" /opt/tomcat/conf/tomcat-users.xml
sudo sed -i.backup2 -r "s/passx/${TOMCAT_ADMIN_PASSWORD}/" /opt/tomcat/conf/tomcat-users.xml

# Copie du context du manager et du host-manager avec la balise <Valve> commentée
sudo cp common/tomcat-manager-context.xml /opt/tomcat/webapps/manager/META-INF/context.xml
sudo cp common/tomcat-host-manager-context.xml /opt/tomcat/webapps/host-manager/META-INF/context.xml

sudo systemctl start tomcat
sudo systemctl enable tomcat

# Arrêt de Tomcat
sudo systemctl stop tomcat
