#!/bin/bash
if [ -n "$1" ]; then
  JAVA_VERSION="$1"
fi

echo "100 : Installation du JDK Java ${JAVA_VERSION}"
export DEBIAN_FRONTEND=noninteractive

if [ -z "$JAVA_VERSION" ]; then
  echo "Précisez la version du JDK à installer dans la variable d'environnement JAVA_VERSION. [Recommandée : 17]" >&2;
  exit 1;
fi

# gcc, g++, make sont recommandés pour nodejs et d'autres outils
sudo apt-get -y -qq install "openjdk-${JAVA_VERSION}-jdk" "gcc" "g++" "make" -o Dpkg::Progress-Fancy="0" -o Dpkg::Use-Pty="0"

# Pas besoin de mettre JAVA_HOME dans le PATH : il est déjà dans /usr/bin
echo "JAVA_HOME=/usr/lib/jvm/java-${JAVA_VERSION}-openjdk-amd64/" >> ~/.profile
export JAVA_HOME=/usr/lib/jvm/java-${JAVA_VERSION}-openjdk-amd64/
