#!/bin/bash
echo "050 : Installation des certificats et mise à jour"
export DEBIAN_FRONTEND=noninteractive

# sudo apt-get -y -qq update
sudo apt-get -y -qq install gnupg2 wget ca-certificates ca-certificates-java -o Dpkg::Progress-Fancy="0" -o Dpkg::Use-Pty="0"
sudo apt-get -y -qq update
