#!/bin/bash
if [ -n "$1" ]; then
  GEOSERVER_VERSION="$1"
fi

if [ -n "$2" ]; then
  GEOSERVER_PORT="$2"
fi

if [ -n "$3" ]; then
  GEOSERVER_UPDATE="$3"
fi

echo "350 : Installation de Geoserver ${GEOSERVER_VERSION} sur le port ${GEOSERVER_PORT}, mode update : ${GEOSERVER_UPDATE}"
export DEBIAN_FRONTEND=noninteractive

if [ -z "${GEOSERVER_VERSION}" ]; then
  echo "Précisez la version de Geoserver à installer dans la variable d'environnement GEOSERVER_VERSION. [Recommandée : 2.23.1]" >&2;
  exit 1;
fi

if [ -z "${GEOSERVER_PORT}" ]; then
  echo "Précisez le port d'écoute de Geoserver dans la variable d'environnement GEOSERVER_PORT. [Recommandé : 8081]" >&2;
  exit 1;
fi

if [ "${GEOSERVER_UPDATE}" != "true" ] && [ "${GEOSERVER_UPDATE}" != "false" ]; then
  echo "Indiquez si Geoserver est en installation initiale (false) ou update (true) dans la variable d'environnement GEOSERVER_UPDATE. ${GEOSERVER_UPDATE} n'est pas accepté." >&2;
  exit 1;
fi

port=${GEOSERVER_PORT}
update=${GEOSERVER_UPDATE}

# Ne pas unzipper pour réinstaller Geoserver en mode update
if [ "$update" == "false" ]; then
  wget -q "https://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/geoserver-${GEOSERVER_VERSION}-bin.zip" -O "geoserver-${GEOSERVER_VERSION}-bin.zip"

  sudo unzip -q "geoserver-${GEOSERVER_VERSION}-bin.zip" -d /opt/geoserver
  sudo chown "$(whoami):$(whoami)" -R /opt/geoserver
  echo "export GEOSERVER_HOME=/opt/geoserver" >> ~/.profile
  echo "export PATH=$GEOSERVER_HOME/bin:$PATH" >> ~/.profile

  sed -i "s/8080/$port/g" /opt/geoserver/start.ini
  sed -i "s/8080/$port/g" /opt/geoserver/etc/jetty-http.xml
else
  echo "En mode update, Geoserver ne se réinstallera pas."
fi

sudo cp systemd/geoserver.service /etc/systemd/system
sudo systemctl daemon-reload

sudo systemctl enable geoserver.service
sudo systemctl start geoserver.service
