#!/bin/bash
if [ -n "$1" ]; then
  APPLICATION_KAFKA_ZOOKEEPER_PORT="$1"
fi

echo "550 : Démarrage des applications back, port Kafka Zookeeper : ${APPLICATION_KAFKA_ZOOKEEPER_PORT}, port Kafka Bootstrap server : ${APPLICATION_KAFKA_BOOTSTRAP_SERVER_PORT}, host Kafka : ${APPLICATION_KAFKA_HOST}"
export DEBIAN_FRONTEND=noninteractive

DELAI_ATTENTE=0.1

if [ -z "${APPLICATION_KAFKA_ZOOKEEPER_PORT}" ]; then
  echo "Précisez le port de zookeeper dans la variable d'environnement APPLICATION_KAFKA_ZOOKEEPER_PORT. [Recommandé : 2181]" >&2;
  exit 1;
fi

ZOOKEEPER_PORT=${APPLICATION_KAFKA_ZOOKEEPER_PORT}

if [ -n "$2" ]; then
  APPLICATION_KAFKA_BOOTSTRAP_SERVER_PORT="$2"
fi

if [ -z "${APPLICATION_KAFKA_BOOTSTRAP_SERVER_PORT}" ]; then
  echo "Précisez le port du bootstrap server de kafka dans la variable d'environnement APPLICATION_KAFKA_BOOTSTRAP_SERVER_PORT. [Recommandé : 9092]" >&2;
  exit 1;
fi

KAFKA_BOOTSTRAP_SERVER_PORT=${APPLICATION_KAFKA_BOOTSTRAP_SERVER_PORT}

if [ -n "$3" ]; then
  APPLICATION_KAFKA_HOST="$3"
fi

if [ -z "${APPLICATION_KAFKA_HOST}" ]; then
  echo "Précisez le host de zookeeper et du bootstrap server de kafka dans la variable d'environnement APPLICATION_KAFKA_HOST. [Recommandé : localhost]" >&2;
  exit 1;
fi

HOST=$3

echo "Démarrage de Zookeeper (port $ZOOKEEPER_PORT) et de Kafka (port $KAFKA_BOOTSTRAP_SERVER_PORT) sur $HOST"

"$KAFKA_HOME"/bin/zookeeper-server-start.sh -daemon "$KAFKA_HOME/config/zookeeper.properties" &&
while ! nc -z "$HOST" "$ZOOKEEPER_PORT"; do sleep $DELAI_ATTENTE; done

"$KAFKA_HOME"/bin/kafka-server-start.sh -daemon "$KAFKA_HOME/config/server.properties"
while ! nc -z "$HOST" "$KAFKA_BOOTSTRAP_SERVER_PORT"; do sleep $DELAI_ATTENTE; done

echo "Demande de démarrage du Backend metier et backend IHM"
sudo systemctl start ecoemploi-back-metier.service
sudo systemctl start ecoemploi-back-ihm.service
