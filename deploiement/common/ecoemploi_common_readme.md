---
header-includes:
  \input{$APPRENTISSAGE_HOME/apprentissage-header-include}
  \input{$APPRENTISSAGE_HOME/apprentissage-include}

title: "Installation ecoemploi : éléments communs aux différents déploiements"
subtitle: ""
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

Toutes les commandes pour l'installer entièrement depuis un ordinateur vide (sous _Debian 11_)

# Installations requises

Ces commandes doivent être exécutées et les applications ci-dessous installées, pour que _ecoemploi_ puisse fonctionner.  
Elles installent:

   - Les clefs et les certificats
   - Java 17
   - Postgres 15 et Postgis 3
   - NodeJS 16 et Angular 14
   - ELK 7
   - Geoserver 2.23

### Mise à jour des clefs et certificats

```bash
sudo apt install gnupg2 wget ca-certificates
sudo apt-get update
```

### Java, Vim

```bash
sudo apt-get install openjdk-17-jdk
```

### Postgresql 15 et Postgis 3.x

Le choix de Postgresql est imposé parce que l'installation du package `postgis` provoque la mise à jour du Postgresql installé dans la dernière version.

```bash
echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list

sudo apt-get update
sudo apt -y install gnupg2
sudo apt-get update

sudo apt -y install postgresql-15 postgresql-client-15
sudo apt -y install postgis postgresql-15-postgis-3 postgresql-15-postgis-3-scripts
```

### NodeJS et Angular

```bash
sudo curl -fsSL https://deb.nodesource.com/setup_16.x | sudo bash -
sudo apt-get install -y nodejs
sudo npm install -g @angular/cli
```

### ELK

```bash
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update && sudo apt-get install elasticsearch
sudo systemctl start elasticsearch.service
```

Depuis le répertoire `/usr/share/elasticsearch/bin/` :

```bash
sudo ./elasticsearch-plugin install ingest-attachment
```

Lors des updates d'Elastic, il peut empêcher son redémarrage.  
Dans ce cas, exécuter un `sudo /usr/share/elasticsearch/bin/elasticsearch-plugin remove ingest-attachment` et relancer ensuite sa commande d'installation.

### Kafka 3.4

```bash
cd /tmp && wget https://downloads.apache.org/kafka/3.4.0/kafka_2.12-3.4.0.tgz
sudo tar -xvf kafka_2.12-3.4.0.tgz -C /opt
export KAFKA_HOME="/opt/kafka_2.12-3.4.0"
```

### Geoserver

```bash
sudo unzip geoserver-2.23.0-bin.zip -d /opt/geoserver
sudo chown $(whoami):$(whoami) -R /opt/geoserver
echo "export GEOSERVER_HOME=/opt/geoserver" >> ~/.profile
```

Créer un [geoserver.service (systemd)](https://gitlab.com/territoirevif/comptes-france/-/blob/master/deploiement/local/systemd/geoserver.service) pour faciliter son lancement

# Installations selon les objectifs

## Pour des installation de recette ou production

### Kubernetes

```bash
sudo apt install -y gnupg2
sudo apt -y install curl apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt update
sudo apt -y install vim git curl wget kubelet kubeadm kubectl
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
sudo swapoff -a
sudo modprobe overlay
sudo modprobe br_netfilter
sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

sudo sysctl --system
```

### Virtualbox et Vagrant

```bash
sudo apt-get install virtualbox
sudo apt-get install vagrant
```

## Pour l'environnement de développement

### Git, Maven

```bash
sudo apt-get install git
sudo apt install maven
```

# Outils optionnels, mais recommandés

## Gestionnaire SGBD : DBeaver

```bash
echo "deb https://dbeaver.io/debs/dbeaver-ce /" | sudo tee /etc/apt/sources.list.d/dbeaver.list
curl -fsSL https://dbeaver.io/debs/dbeaver.gpg.key | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/dbeaver.gpg
sudo apt-get update
sudo apt install dbeaver-ce
```

## Installation d'Anaconda (extension Python)

```bash
wget https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh
chmod +x Anaconda3-2021.05-Linux-x86_64.sh 
conda config --set changeps1 False
```

## Installation de pandoc (pour Markdown)

```bash
wget https://github.com/jgm/pandoc/releases/download/2.14.0.1/pandoc-2.14.0.1-1-amd64.deb
sudo dpkg -i pandoc-2.14.0.1-1-amd64.deb

sudo apt-get install pdflatex
sudo apt-get install texlive-latex-base
sudo apt-get install texlive-fonts-recommended
sudo apt-get install texlive-fonts-extra
```

# Outils optionnels

### Gestionnaire SGBD : Pgadmin 4

```bash
sudo curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'
sudo apt install pgadmin4
```

### QGIS

```bash
sudo apt install gnupg software-properties-common
wget -qO - https://qgis.org/downloads/qgis-2022.gpg.key | sudo gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/qgis-archive.gpg --import
sudo chmod a+r /etc/apt/trusted.gpg.d/qgis-archive.gpg
sudo add-apt-repository "deb https://qgis.org/debian $(lsb_release -c -s) main"
sudo apt update
sudo apt install qgis qgis-plugin-grass
```

### Notepad++

```bash
sudo apt install snapd
sudo snap install core
sudo snap install snapd
sudo snap refresh
sudo snap install notepad-plus-plus
```

### Vim

```bash
sudo apt-get install vim
```

### rsync

```bash
sudo apt-get install rsync
```

### Installation de Jupyter

```bash
pip install jupyterlab
pip install notebook
```

Editer le git config pour y mettre son e-mail, nom, et réglages de crlf.

# Outils prochainement obsolètes

Présents sur des installations passées, ils seront déclassés.

## Docker CE et compose (Abandon prévu)

\underline{Revert prochain :}

_Docker_ n'est plus retenu comme solution de démarrage pour le projet _ecoemploi_.

_Vagrant_ et _Kubernetes_ sont choisis.

```bash
sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo docker run hello-world

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose -version
```

## Zeppelin et son Spark associé (Abandon prévu)

\underline{Revert prochain :}

_Apache Zeppelin_ est un projet opensource quasiment plus vivant.  
Il sera remplacé au profit de _Jupyter_.

```bash
# Dezip et move dans /opt avec Path réglé
tar xvf zeppelin-0.10.1-bin-all.tgz -C /home/lebihan/Téléchargements/
sudo mv zeppelin-0.10.1-bin-all /opt

wget https://downloads.apache.org/spark/spark-3.1.1/spark-3.1.1-bin-hadoop2.7.tgz 
tar xvf spark-3.1.1-bin-hadoop2.7.tgz 
sudo mv spark-3.1.1-bin-hadoop2.7 /opt
```

# Commandes utilitaires

## Alimentation à la main des tables géographiques des communes

```bash
export PGPASSWORD=postgres;shp2pgsql -s 4326 -I /data/comptes-france/territoire/2016/communes.shp public.communes_2016 | psql -d comptesfrance -U postgres -h localhost -q
```
