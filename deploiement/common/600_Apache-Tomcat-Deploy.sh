#!/bin/bash
if [ -n "$1" ]; then
  TOMCAT_DEPLOY_UPDATE="$1"
fi

echo "600 : Déploiement du front dans Apache Tomcat, mode update : ${TOMCAT_DEPLOY_UPDATE}"
export DEBIAN_FRONTEND=noninteractive

if [ "${TOMCAT_DEPLOY_UPDATE}" != "true" ] && [ "${TOMCAT_DEPLOY_UPDATE}" != "false" ]; then
  echo "Indiquez si Tomcat est en tout premier déploiement (false) ou en update/redéploiement (true) dans la variable d'environnement TOMCAT_DEPLOY_UPDATE. ${TOMCAT_DEPLOY_UPDATE} n'est pas accepté." >&2;
  exit 1;
fi

update=${TOMCAT_DEPLOY_UPDATE}

# Redémarrage de tomcat
if [ "$update" == "false" ]; then
  sudo mkdir -p /opt/tomcat/webapps/ecoemploi
fi

sudo cp -r front-ihm/. /opt/tomcat/webapps/ecoemploi
sudo chown -R tomcat:tomcat /opt/tomcat/webapps/ecoemploi
sudo systemctl start tomcat
