#!/bin/bash
if [ -n "$1" ]; then
  NODEJS_VERSION="$1"
fi

echo "200 : Installation de NodeJS ${NODEJS_VERSION} et d'Angular"
export DEBIAN_FRONTEND=noninteractive

if [ -z "${NODEJS_VERSION}" ]; then
  echo "Précisez la version de NodeJS à installer dans la variable d'environnement ${NODEJS_VERSION}. [Recommandée : 18]" >&2;
  exit 1;
fi

sudo curl -fsSL "https://deb.nodesource.com/setup_${NODEJS_VERSION}.x" | sudo bash -
sudo apt-get -y -qq install nodejs -o Dpkg::Progress-Fancy="0" -o Dpkg::Use-Pty="0"
sudo npm --quiet install -g @angular/cli
