#!/bin/bash
echo "010 : Définition des versions des composants installés"

# 100 : JDK
export JAVA_VERSION="17"

# 150 : Postgres, Postgis
export POSTGRES_VERSION="15"
export POSTGIS_VERSION="3"
export POSTGRES_PORT="5432"
export POSTGRES_PASSWORD="postgres"

# 200 : NodeJS
export NODEJS_VERSION="18"

# 250 : ELK
export ELASTIC_VERSION="8.x"
export ELASTIC_UPDATE="false"

# 300 : Kafka
export KAFKA_VERSION="3.7.2"
export KAFKA_SCALA_VERSION="2.13"
export KAFKA_UPDATE="false"

# 350 : Geoserver
export GEOSERVER_VERSION="2.23.1"
export GEOSERVER_PORT="8081"
export GEOSERVER_UPDATE="false"

# 500 - Tomcat (installation du serveur web)
export TOMCAT_VERSION="10.1.34"
export TOMCAT_MAJOR_VERSION="10"
export TOMCAT_ADMIN_USER="tomcat"
export TOMCAT_ADMIN_PASSWORD="tomcat"
export TOMCAT_UPDATE="false"

# 550 - Applications
export APPLICATION_KAFKA_ZOOKEEPER_PORT="2181"
export APPLICATION_KAFKA_BOOTSTRAP_SERVER_PORT="9092"
export APPLICATION_KAFKA_HOST="localhost"

# 600 - Tomcat (déploiement d'application web)
export TOMCAT_DEPLOY_UPDATE=false
