#!/bin/bash
if [ -n "$1" ]; then
  POSTGRES_VERSION="$1"
fi

if [ -n "$2" ]; then
  POSTGIS_VERSION="$2"
fi

if [ -n "$3" ]; then
  POSTGRES_PASSWORD="$3"
fi

echo "150 : Installation de Postgresql ${POSTGRES_VERSION} et Postgis ${POSTGIS_VERSION}"
export DEBIAN_FRONTEND=noninteractive

if [ -z "${POSTGRES_VERSION}" ]; then
  echo "Précisez la version de Postgresql à installer dans la variable d'environnement POSTGRES_VERSION. [Recommandée : 15]" >&2;
  exit 1;
fi

if [ -z "${POSTGIS_VERSION}" ]; then
  echo "Précisez la version de Postgis à installer dans la variable d'environnement POSTGIS_VERSION. [Recommandée : 3]" >&2;
  exit 1;
fi

if [ -z "${POSTGRES_PASSWORD}" ]; then
  echo "Précisez le mot de passe d'initialisation de l'utilisateur postgres dans la variable d'environnement POSTGRES_PASSWORD." >&2;
  exit 1;
fi

if [ -n "$4" ]; then
  POSTGRES_PORT="$4"
fi

if [ -z "${POSTGRES_PORT}" ]; then
  echo "Précisez le port de connexion de la base de données Postgresql dans la variable d'environnement POSTGRES_PORT. [Recommandé : 5432]" >&2;
  exit 1;
fi

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list
sudo apt-get -y -qq update

sudo apt-get -y -qq install "postgresql-${POSTGRES_VERSION}" "postgresql-client-${POSTGRES_VERSION}" -o Dpkg::Progress-Fancy="0"
sudo apt-get -y -qq install "postgis" "postgresql-${POSTGRES_VERSION}-postgis-${POSTGIS_VERSION}" "postgresql-${POSTGRES_VERSION}-postgis-${POSTGIS_VERSION}-scripts" -o Dpkg::Progress-Fancy="0" -o Dpkg::Use-Pty="0"

# PGHOST behaves the same as the host connection parameter.
# PGHOSTADDR behaves the same as the hostaddr connection parameter. This can be set instead of or in addition to PGHOST to avoid DNS lookup overhead.
# PGPORT behaves the same as the port connection parameter.
# PGDATABASE behaves the same as the dbname connection parameter.
# PGUSER behaves the same as the user connection parameter.
# PGPASSWORD behaves the same as the password connection parameter. Use of this environment variable is not recommended for security reasons, as some operating systems allow non-root users to see process environment variables via ps; instead consider using a password file (see Section 33.15).
# PGPASSFILE behaves the same as the passfile connection parameter.
# PGCHANNELBINDING behaves the same as the channel_binding connection parameter.
# PGSERVICE behaves the same as the service connection parameter.

echo "Création de la base de données"

# Créer l'utilisateur postgres
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD '$POSTGRES_PASSWORD';"

# Créer la base de données
psql -f common/160_CreateDB-comptesfrance.sql postgresql://postgres:$POSTGRES_PASSWORD@localhost:$POSTGRES_PORT
