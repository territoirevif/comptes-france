Démarrage des composants en mode Docker
---------------------------------------

### Construction

```bash
sudo docker-compose -f docker-compose.yml -f elk/docker-compose-elk.yml build
```

Cette commande se trouve dans le script `build-comptes-france.sh`

### Démarrage

```bash
sudo docker-compose -f docker-compose.yml -f elk/docker-compose-elk.yml up
```

Cette commande se trouve dans le script `start-comptes-france.sh`

### Recompiler et tout redéployer

```bash
mvn clean install && sudo docker-compose -f docker-compose.yml -f elk/docker-compose-elk.yml down && \
sudo docker-compose -f docker-compose.yml -f elk/docker-compose-elk.yml build && \
sudo docker-compose -f docker-compose.yml -f elk/docker-compose-elk.yml up -d
```

### Arrêt

```bash
sudo docker-compose -f docker-compose.yml -f elk/docker-compose-elk.yml down 
```

Cette commande se trouve dans les scripts `stop-comptes-france.sh`, variante `down-comptes-france.sh` 

### Accès en mode console

Accès à un des composants en mode console :

`sudo docker exec -it spark bin/bash`


### Remarque

Attention : pour que les containers Elastic démarrent,  
il faut que le host porteur des containers Docker ait vu cette commande exécutée :

```bash
sysctl -w vm.max_map_count=262144
```
