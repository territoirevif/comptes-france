# Elasticsearch, logstash, kibana.

Cluster ELK : port 9300
Noeuds : master, data, client...

## Elasticseach : analyse de documents

```bash
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update && sudo apt-get install elasticsearch
sudo systemctl start elasticsearch.service

# Pour contrôle :
sudo journalctl --unit elasticsearch
curl -X GET "localhost:9200/?pretty"
```

### Configuration

Les fichiers de configuration sont à cet emplacement sur l'image Docker : `/usr/share/elasticsearch/config/`.

#### Test de bon fonctionnement

```bash
curl http://172.20.0.3:9200
```

```
{
  "name" : "es01",
  "cluster_name" : "es-docker-cluster",
  "cluster_uuid" : "8QKY_9nrRbyTplLAXMbM8g",
  "version" : {
    "number" : "7.6.2",
    "build_flavor" : "default",
    "build_type" : "docker",
    "build_hash" : "ef48eb35cf30adf4db14086e8aabd07ef6fb113f",
    "build_date" : "2020-03-26T06:34:37.794943Z",
    "build_snapshot" : false,
    "lucene_version" : "8.4.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
```
