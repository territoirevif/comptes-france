#!/bin/bash
echo "Le moteur métier Spark attends que les sources de données soient prêtes (360 secondes)"
sleep 360

echo "Démarrage du moteur métier Spark"
java -Xmx5g -Duser.language=fr -jar application-metier-et-gestion.jar --spring.profiles.active=sparkdocker

