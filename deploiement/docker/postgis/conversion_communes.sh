# Alimentation de tables externes par commande de conversion PostGIS.
echo "Création des extensions PostGIS avant alimentation des tables par shp2pgsql."
psql -d comptesfrance -U postgres -q -c 'CREATE EXTENSION postgis'
psql -d comptesfrance -U postgres -q -c 'CREATE EXTENSION postgis_topology'

echo "Début de la conversion communes OpenStreetMap (shapefile -> tables communes_<annee>)..."
export PGPASSWORD=postgres

for annee in `seq 2016 2022`; do
   echo "Acquisition des polygones de communes OpenStreetMap $annee..."
  
   # EPSG:4326 = WGS84
   shp2pgsql -s 4326 -I /data/comptes-france/territoire/$annee/communes.shp public.communes_$annee | psql -d comptesfrance -U postgres -q
done

echo "Fin de la conversion des communes OpenSteetMap"
