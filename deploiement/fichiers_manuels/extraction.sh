#!/bin/bash
# Extrait un csv d'exemple depuis un csv plus large
#
#   $1: Fichier CSV large
#   $2: Fichier CSV filtré
#   $3: Expression grep
echo "sample.sh <fichier csv source> <fichier csv cible> <expression grep de filtrage>"
echo "Filtre les lignes d'un fichier CSV par une expression grep, et en produit un autre."

# Vérification de l'existence du fichier csv d'entrée
if [ -z "$1" ]; then
  echo "Précisez le nom du fichier csv dont le nom va être repris pour extraire l'entête du CSV." >&2;
  exit 1;
fi

if [ ! -f "$1" ]; then
  echo "Le fichier csv d'où les données doivent être extraites, $1, n'a pas été trouvé" >&2;
  exit 1;
fi

# Vérification qu'un fichier csv de sortie nous a été donné
if [ -z "$2" ]; then
  echo "Précisez le nom du fichier csv cible, contenant les lignes filtrées par grep." >&2;
  exit 1;
fi

# Ainsi qu'une expression grep de filtrage
if [ -z "$3" ]; then
  echo "Précisez l'instruction grep de filtrage." >&2;
  exit 1;
fi

header_file=$(mktemp)
rows_file=$(mktemp)
greped_file=$(mktemp)

awk 'FNR>1' "$1" > "$rows_file"
awk 'FNR<2' "$1" > "$header_file"

cat "$rows_file" | grep -E "$3" >"$greped_file"
cat "$header_file" "$greped_file" >"$2"

count=$(wc -l "$2")
echo "Fichier csv $2 produit. Il a $count lignes (entête inclu)."

rm "$header_file"
rm "$rows_file"
rm "$greped_file"
