#!/bin/bash
# Rassemble plusieurs fichiers csv waldec en un seul
echo "rassemble_waldec.sh <un fichier CSV parmi ceux de la liste>"
echo "Rassemble plusieurs fichiers Waldec CSV en un seul."

if [ -z "$1" ]; then
  echo "Précisez le nom d'un des fichiers csv dont le nom va être repris pour extraire l'entête du CSV." >&2;
  exit 1;
fi

if [ ! -f "$1" ]; then
  echo "Le fichier csv $1 n'a pas été trouvé" >&2;
  exit 1;
fi

awk 'FNR>1' *.csv > rna_waldec.cs
awk 'FNR<2' $1 > header.cs
cat header.cs rna_waldec.cs >rna_waldec.csv
rm header.cs
rm rna_waldec.cs
