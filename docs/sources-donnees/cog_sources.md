Les sources du Code Officiel Géographique
-----------------------------------------

Voici les liens de téléchargement des données brutes traitées par l'application, et leur lieu de stockage.  
Les éventuels traitements qu'il faut accomplir dessus pour les faire prendre en charge par l'application.

## 1) Le code officiel géographique (COG)

Il paraît, en général, Fin Février sur le site de l'_INSEE_ : [Accueil >> Téléchargement de fichiers >> Géographie administrative >> Code officiel géographique (COG)](https://www.insee.fr/fr/information/2560452)

S'y trouvent les communes et évènements sur les communes, cantons (et pseudos-cantons), arrondissements, départements, régions, pays... de [2024](https://www.insee.fr/fr/information/7766585), [2023](https://www.insee.fr/fr/information/6800675), [2022](https://www.insee.fr/fr/information/6051727), [2021](https://www.insee.fr/fr/information/5057840), [2020](https://www.insee.fr/fr/information/4316069), [2019](https://www.insee.fr/fr/information/3720946)...


| catalogue ou dataset id  | slug catalogue                 | Fournisseur |
|--------------------------|--------------------------------|-------------|
| 58c984b088ee386cdb1261f3 | code-officiel-geographique-cog | INSEE       |

Les ressources qui suivent sont à placer dans le répertoire `$TERRITOIRE_DIR/[année]` (par défaut valorisé à : `/data/comptes-france/territoire/[année]`),    
en renommant chaque fichier obtenu avec le nom en regard :

| ressource id                                                                                                        | fichier de destination | Dernier téléchargement |
|---------------------------------------------------------------------------------------------------------------------|------------------------|------------------------|
| [8262de72-138f-4596-ad2f-10079e5f4d7c](https://www.insee.fr/fr/statistiques/fichier/7766585/v_commune_2024.csv)     | communes.csv           | 2024-02-20             |
| [e436f772-b05d-47f8-b246-265faab8679f](https://www.insee.fr/fr/statistiques/fichier/7766585/v_departement_2024.csv) | departements.csv       | 2024-02-20             |
| [2da99392-eac0-41a2-acfb-7187033fdcd2](https://www.insee.fr/fr/statistiques/fichier/7766585/v_mvt_commune_2024.csv) | mvtcommune.csv         | 2024-02-20             |


## 2) Le contour des communes

Il est pris d'[Admin express](https://geoservices.ign.fr/adminexpress) : il s'agit d'une série de fichiers _Shapefile_.

- Télécharger le fichier `.7z` _France Entière_ de l'année voulue, qui est projeté en WGS 84.
- Le décomprimer dans `$TERRITOIRE_DIR/[année]`, par défaut valorisé à : `/data/comptes-france/territoire/[année]`

### Insertion en base de données PostGIS

Les communes y sont projetées en `WGS84` (`EPSG:4326`)

L'application insère les données de ces shapefiles dans sa base de données.    
Insérer les contours des communes dune nouvelle année, réclame de la recompiler avec son fichier _Liquibase_ courant, `src/main/resources/db/changelog/compte-france-postgis-v[version en cours de développement].xml`, complété d'une copie d'un `changeset` d'une année passée dans la nouvelle année.

Il est donné ici pour exemple en 2024, mais mieux vaut le reprendre tel que vous le verrez dans son changelog _Liquibase_ à l'instant de l'adaptation :

```xml
<changeSet id="shp2pgsql_admin_express_2024" author="mlebihan" failOnError="false">
    <preConditions onFail="MARK_RAN">
        <not>
            <tableExists tableName="communes_2024" schemaName="public" />
        </not>
    </preConditions>

    <comment>Importation des shapefiles des contours des communes 2024 depuis admin express</comment>

    <executeCommand executable="/bin/bash">
        <arg value="-c"/>
        <arg value="export PGPASSWORD=${password};shp2pgsql -s 4326 -I /data/comptes-france/territoire/2024/COMMUNE.shp public.communes_2024 | psql -d comptesfrance -U postgres -h localhost -p ${port} -q"/>
    </executeCommand>
</changeSet>

<!-- Le changeset qui suit dépend de l'exécution des commandes sh2pgsql précédentes -->
<changeSet id="communes_admin_express_2024_indexation" author="mlebihan" failOnError="false">
    <comment>Indexation des communes admin express 2024 par code INSEE</comment>

    <sql>
        CREATE INDEX idx_communes_2024_insee ON communes_2024(insee_com) ;
        COMMENT ON INDEX idx_communes_2024_insee IS 'Accès à la table des communes 2024 (admin express) par code insee.';
    </sql>

    <rollback>
        <sql>
            DROP INDEX idx_communes_2024_insee;
        </sql>
    </rollback>
</changeSet>

<changeSet id="calcul_surface_communes_2024" author="mlebihan">
    <comment>Calcul de la surface des communes 2024 en hectares</comment>

    <sql>
        ALTER TABLE communes_2024 ADD COLUMN surf_ha NUMERIC;
        COMMENT ON COLUMN communes_2024.surf_ha IS 'Surface de la commune en hectares';

        UPDATE communes_2024 SET surf_ha = round(st_area(geom, true) / 10000);

        CREATE VIEW communes_2024_centroid_view AS
        SELECT gid, insee_com, nom, surf_ha, ST_CENTROID(geom) as centroid
        FROM communes_2024;
    </sql>
</changeSet>
```
