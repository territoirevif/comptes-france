
CODE_ISO3_REGION=$1 # ARA
CODE_REGION=$2      # 44
DATE_PUBLICATION=$3 # 01/04/2021

# A : Liste des groupements
# B : Coordonnées des groupements
# C : Compétences des groupements
# D : Périmètres des groupements
# E : Périmètres des EPCI à fiscalité propre
FORMAT=$4

URL_BASE="https://www.banatic.interieur.gouv.fr/V5/fichiers-en-telechargement/telecharger.php"
URL="$URL_BASE?zone=R$CODE_REGION&date=$DATE_PUBLICATION&format=$FORMAT"
PREFIXE_FICHIER="inconnu"

if [ "$FORMAT" = "A" ]; then
   PREFIXE_FICHIER="epci_groupements_"
fi

if [ "$FORMAT" = "B" ]; then
   PREFIXE_FICHIER="epci_coordonnees_"
fi

if [ "$FORMAT" = "C" ]; then
   PREFIXE_FICHIER="epci_competences_"
fi

if [ "$FORMAT" = "D" ]; then
   PREFIXE_FICHIER="epci_perimetres_"
fi

if [ "$FORMAT" = "E" ]; then
   PREFIXE_FICHIER="epci_perimetres_epci_gfp_"
fi

FICHIER_CIBLE="$PREFIXE_FICHIER$CODE_ISO3_REGION.csv"

wget $URL -O $FICHIER_CIBLE
# https://www.banatic.interieur.gouv.fr/V5/fichiers-en-telechargement/telecharger.php?zone=R44&date=01/04/2021&format=A
