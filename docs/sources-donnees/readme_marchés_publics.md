
## Marchés publics

(Pour exploitation future).

* [Bulletin Officiel d'Annonces de Marchés Publics (BOMAP)](https://www.data.gouv.fr/fr/datasets/boamp/), mise à jour quotidienne

	- [Schéma XSD](https://www.data.gouv.fr/fr/datasets/r/db39393a-150a-4a2e-a63d-b4e195fbbb2a)
	- [Données XML, HTML](https://www.data.gouv.fr/fr/datasets/r/2f339f80-14c4-4d9b-b52a-70c9deca80cc)
	- [Flux historique](https://www.data.gouv.fr/fr/datasets/r/27dd4d68-30b8-4ec8-b80c-df91378735bc)
