# Description des sources

- [Code Officiel Géographique](cog_sources.md)
- [Intercommunalités, groupements et périmètres](intercos_groupements_perimetres_sources.md)

# Recommandation sur le traitement des fichiers CSV

   - à l'import, lors de la déclaration des types de colonnes, indiquer celles en `Text` au lieu de `Standard`.    
   - si des dates sont présentes, ne pas les convertir à l'import au format `YYYY-MM-DD` ou autre : attendre d'être sur la feuille ce calcul : risque de passer l'année de quatre à deux chiffres, sinon.    
   - sur la feuille de calcul, mettre en Format `UK sans décimales` les valeurs numériques (sauf si elles en ont).    
   - au moment de la sauvegarde en CSV, cocher `Edit Settings` et choisir d'encadrer les textes par des guillemets `"`. Sinon, les joins ne se font pas sur les codes ayant des zéro initiaux.

# Instructions de déploiement et paramétrage

Les créations de tables, d'index et de vues sont exécutées par _Liquibase_ au moment de l'initialisation.  
mais un `CREATE DATABASE comptesfrance` initial doit avoir été fait.

(rollback Liquibase, si requis pour mise au point : `mvn liquibase:rollback -Dliquibase.rollbackCount=1` ou avec `rollbackTag`)
