
# Données cartographiques de l'IGN

* `WMS vecteur Géoportail` signifie que la couche est accessible au travers du [service WMS vecteur Géoportail](https://wxs.ign.fr/votreclef/geoportail/v/wms/)
* `WMS raster Géoportail` accessible au travers du [service WMS raster Géoportail](https://wxs.ign.fr/votreclef/geoportail/r/wms)
* `WMS vecteur Inspire` accessible au travers du [service WMS vecteur Inspire](https://wxs.ign.fr/votreclef/inspire/v/wms)
* `WMS raster Inspire` accessible au travers du [service WMS raster Inspire](https://wxs.ign.fr/votreclef/inspire/r/wms)
* `WMTS` accessible au travers du [service WMTS Géoportail](https://wxs.ign.fr/votreclef/geoportail/wmts)
* `WFS` accessible au travers du [service WFS Géoportail](https://wxs.ign.fr/votreclef/geoportail/wfs)
* `Géocodage` signifie que la donnée est utilisée au travers du [service de géocodage](https://wxs.ign.fr/votreclef/geoportail/ols)
