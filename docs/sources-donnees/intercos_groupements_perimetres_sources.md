Les sources des intercommunalités, groupements et périmètres
------------------------------------------------------------

Voici les liens de téléchargement des données brutes traitées par l'application, et leur lieu de stockage.  
Les éventuels traitements qu'il faut accomplir dessus pour les faire prendre en charge par l'application.

## Les groupements (intercommunalités et syndicats), leurs périmètres et leurs compétences

Intercommunalités, Base Nationale des Intercommunalités (BANATIC)

Fournisseur : Base Nationale des Intercommunalité BANATIC (DGCL)

URL : https://www.data.gouv.fr/fr/datasets/base-nationale-sur-les-intercommunalites/  
ou bien le site web BANATIC, mais sous la forme d'une feuille Excel à télécharger après sélections dans un formulaire.

Période de publication : en Mars de chaque année


## Ces instructions étaient applicables jusqu'en 2024

```
https://www.banatic.interieur.gouv.fr/V5/fichiers-en-telechargement/telecharger.php
   ?zone=R[code région COG]&date=[Date_publication]&format=[données_desirées]
```


`Région` : code région, exemple : 44 pour ARA  
`Date_publication` : au premier jour d'un mois, exemple : 01/04/2021  
`données_desirées` : A = groupements, B = coordonnées groupements, C = compétences groupements, D = périmètres, E = périmètres EPCI GFP (=?)

Format : CSV, ISO-8859-1, Séparateur : tabulation, Séparateur décimal : virgule.
Certains codes et libellés sont dans le même champ, sous la forme "code - libellé"

[https://www.banatic.interieur.gouv.fr/V5/fichiers-en-telechargement/fichiers-telech.php](https://www.banatic.interieur.gouv.fr/V5/fichiers-en-telechargement/fichiers-telech.php) propose une page web d'accès aux données.

- Les fichiers nationaux n'assurent que la liste des groupements et périmètres des EPCI. Ceux des syndicats ne sont garantis présents que dans les fichiers régionaux.

    - Un script `telechargement_fichiers_banatic.sh` télécharge du site Internet ces fichiers régionaux

        - Son paramètre d'entrée est une date : 01/01/AAAA avec un autre mois possible pour l'année courante (en fonction des publications du site).

    - Un autre script, `telechargement_fichier_banatic.sh` prend en argument: un code région (`ARA`), un numéro de région (`84`), une date, un format de fichier: `A`.

### [Table de correspondance SIREN - INSEE](https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document_reference/TableCorrespondanceSirenInsee.zip)

Sur le site de la [Base Nationale sur l'Intercommunalité (Banatic)](https://www.banatic.interieur.gouv.fr)  
menu à gauche, lien "Accès rapide → Télécharger le fichier"

URL :

[https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document_reference/TableCorrespondanceSirenInsee.zip]([https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document_reference/TableCorrespondanceSirenInsee.zip)

Dans ce fichier `zip`, se trouvent des fichiers annuels, de 2007 à l'année en cours  
au format `xls`, à convertir en CSV à virgules
