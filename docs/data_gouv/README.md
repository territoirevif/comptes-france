Descriptions des jeux de données pour [data.gouv.fr](https://www.data.gouv.fr/fr/organizations/marc-le-bihan/#/datasets)
------------------------------------------------------------------------------------------------------------------------

- [Code Officiel Géographique](cog_datagouvfr.md)
- [Intercommunalités, groupements et périmètres](intercos_groupements_perimetres_datagouvfr.md)
