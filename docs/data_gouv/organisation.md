Présentation de l'organisation sous [data.gouv.fr](https://www.data.gouv.fr/fr/organizations/marc-le-bihan/)
------------------------------------------------------------------------------------------------------------

Ingénieur d'études et développement Java, Spring Boot, Spark (data engineering) et licencié dans le domaine des collectivités locales (grâce au Conservatoire National des Arts et Métiers, qu'on ne remerciera jamais assez pour tout ce qu'il apporte)

Je réalise :

   - Un outil open source d'acquisition, de nettoyage, mise en liaison, et d'analyse de données open data territoriales  

   - Des contributions publiques sous la forme de projet collaboratifs, par des documents et des ressources que les collectivités locales peuvent exploiter  

   - Une base de ressource documentaire sur l'informatique, les mathématiques, statistiques, l'analyse spatiale, et territoire qui me permettent d'élaborer ces applications et contributions
