# Description du jeu de données

Les intercommunalités, les groupements (ex : syndicats) et leurs communes membres (périmètres)  
reprises depuis la base nationale des intercommunalités (BANATIC)

   - il met côte à côte leurs groupements et leurs périmètres

     - c'est à dire, pour les intercommunalités, toutes leurs communes membres avec leurs compétences
     - et pour tous les groupements, les syndicats qui y sont attachés et les adhésions aux syndicats mixtes éventuels
     - le code et nom des communes sièges et membres est rapporté avec leurs populations totales, municipales, comptées à part
     - toutes les compétences ayant existé sont présentées (vides, si elles ne sont pas encore ou plus applicables)

## Description d'une instance de fichier

Intercommunalités, les groupements (ex : syndicats) et leurs communes membres (périmètres) 2024, ajoutant aux données BANATIC les codes communes membres.      
[Détails](https://gitlab.com/territoirevif/comptes-france/-/blob/master/docs/fonctions/intercos_groupements_perimetres.md?ref_type=heads)

### Format

Ce fichier `zip` le propose au format Apache Parquet (MIME : `application/vnd.apache.parquet`)

### Tri

Ce jeu de données est partitionné par département, et est trié par département et code commune.

### Structure

```
sirenGroupement: string
nomGroupement: string
populationGroupement: integer
natureJuridique: string
codeCommuneSiege: string
nomCommuneSiege: string
sirenCommuneSiege: string
arrondissementSiege: string
populationCommuneSiegeTotale: integer
populationCommuneSiegeMunicipale: integer
populationCommuneSiegeCompteApart: integer
nombreCommunesMembres: integer
nombreDeCompetencesExercees: integer
dateCreation: string
dateEffet: string
modeRepartitionSieges: string
autreModeRepartitionSieges: string
modeDeFinancement: string
syndicatCarte: string
interdepartemental: string
DGFBonifiee: string
DSC: string
REOM: string
TEOM: string
autreTaxe: string
autreRedevance: string
competenceConservee: string
type: string
sirenMembre: string
nomMembre: string
populationMembre: integer
codeCommuneMembre: string
nomCommuneMembre: string
sirenCommuneMembre: string
populationCommuneMembreTotale: integer
populationCommuneMembreMunicipale: integer
populationCommuneMembreCompteApart: integer
sirenAdhesion: string
nomAdhesion: string
populationAdhesion: string
adresseSiege1: string
adresseSiege2: string
adresseSiege3: string
codePostalEtVilleSiege: string
telephoneSiege: string
faxSiege: string
courrielSiege: string
siteInternet: string
annexeAdresse1: string
annexeAdresse2: string
annexeAdresse3: string
codePostalEtVilleAnnexe: string
telephoneAnnexe: string
faxAnnexe: string
civilitePresident: string
prenomPresident: string
nomPresident: string
representationSubstitution: string
C1004: string
C1010: string
C1015: string
C1020: string
C1025: string
C1030: string
C1502: string
C1505: string
C1507: string
C1510: string
C1515: string
C1520: string
C1525: string
C1528: string
C1529: string
C1530: string
C1531: string
C1532: string
C1533: string
C1534: string
C1535: string
C1540: string
C1545: string
C1550: string
C1555: string
C1560: string
C2005: string
C2010: string
C2015: string
C2510: string
C2515: string
C2520: string
C2521: string
C2525: string
C2526: string
C3005: string
C3010: string
C3015: string
C3020: string
C3025: string
C3210: string
C3220: string
C3505: string
C3510: string
C3515: string
C4005: string
C4006: string
C4007: string
C4008: string
C4010: string
C4015: string
C4016: string
C4017: string
C4020: string
C4025: string
C4505: string
C4510: string
C4515: string
C4520: string
C4525: string
C4530: string
C4531: string
C4532: string
C4535: string
C4540: string
C4545: string
C4550: string
C4555: string
C4560: string
C5005: string
C5010: string
C5015: string
C5210: string
C5220: string
C5505: string
C5510: string
C5515: string
C5520: string
C5525: string
C5530: string
C5535: string
C5540: string
C5545: string
C5550: string
C5555: string
C7005: string
C7010: string
C7012: string
C7015: string
C7020: string
C7025: string
C7030: string
C9905: string
C9910: string
C9915: string
C9920: string
C9922: string
C9923: string
C9924: string
C9925: string
C9930: string
C9935: string
C9940: string
C9950: string
C9999: string
codeRegion: string
codeDepartement: string
```

