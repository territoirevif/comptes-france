## Description de la ressource

Code Officiel Géographique de l'INSEE auquel est ajouté, en regard de chaque commune :

   - sa strate communale
   - le nom et chef lieu de son département
   - le code, nom et type de son intercommunalité (EPCI) 
   - sa population totale, municipale, comptée à part
   - sa localisation (centroïde) et sa surface

## Description d'une instance de fichier

Code Officiel Géographique 2024 ajoutant aux données de l’INSEE la state communale, l’intercommunalité de rattachement et population de ces communes, leur centroïde, et leur surface.    
[Détails](https://gitlab.com/territoirevif/comptes-france/-/blob/master/docs/fonctions/cog.md?ref_type=heads)

### Format

Ce fichier `zip` le propose au format Apache Parquet (MIME : `application/vnd.apache.parquet`)  

### Tri

Ce jeu de données est partitionné par département, et est trié par département et code commune.  

### Structure

```
typeCommune: string
codeCommune: string
nomCommune: string
populationTotale: integer
populationMunicipale: integer
populationCompteApart: integer
codeRegion: string
nomDepartement: string
arrondissement: string
codeCanton: string
codeCommuneParente: string
sirenCommune: string
codeEPCI: string
nomEPCI: string
natureJuridiqueEPCI: string
strateCommune: integer
latitude: double
longitude: double
surface: double
typeNomEtCharniere: string
nomMajuscules: string
typeNomEtCharniereDepartement: string
codeCommuneChefLieuDepartement: string
codeDepartement: string
```
