title: Analyse financière des communes

- Base équipement
- Balance des comptes
- Comptes intercommunalités
- BOMAP
- Compétences
- Modes de gestion (SPA, SPIC, DSP...)
- SIVU | SIVOM...
- SPA : Détection sur FCTVA
- Historique des marchés publics
- Régie (C ou IC)
- DSP (affermage, concession)
- Régie intéressée (rémunérée en fonction des résultats)
- gérance (rémunération forfaitaire) --> Marché public
- le délégataire doit présenter un rapport de performances annuel
- Modes de gestion : PPP, SPIC, EPIC, SEM, SEMOP, Association, SPL.
- EPL (entreprise publique locale)
- Quel est le niveau de compte toujours alimenté ? 3, 4 ou 5 ?
- Quels comptes sont affectés par quels compétences, par quels equipements ?
- Comment savoir si une C ou IC exerce réelement une compétence ou non ?

Compte 2031 :

- 2031 CP (Budget principal)
- 2031 CAn (Budget annexe n)
- 2031 IP (Intercommunal, principal) 
- 2031 IAn (Intercommunal, annexe n)
- 2031 D, R, E : Département, région ou état.

Après cela, on renverse : combien un équipement, une compétence coûte t-elle ?
Quel ensemble de comptes est-il engagé, marque t-il l'application effective d'une compétence ?

- Analyse réparties par strate, car les plans de comptes diffèrent.
- Depuis combien de temps une compétence est-elle exercée ?
- Depuis combien de temps un équipement est-il possédé ?

