

# Quelles entreprises, quels commerces

[cf. La revitalisation des centres-bourgs, Petite Ville de Demain](../../../../apprentissage/-/blob/master/territoire/t902-la_revitalisation_des_centres_bourgs-anct.pdf)

## Bulletin Officiel des Annonces des Marchés Publics (BOMAP)

[cf. Les procédures de marchés publics](../../../../apprentissage/-/blob/master/territoire/t652-UE-les_procedures_de_marches_publics.pdf)


# Modules

## Module d'analyse des comptes rendus ou procès verbaux des conseils municipaux ou communautaires

- Une action dans une commune, a t-elle une répercussion, ou provoque t-elle des réactions, dans d'autres ?
- Quels sont les sujets de réflexion ?


## Module de comparaison des collectivités locales

- Détecter les communes qui se ressemblent considérant ensemble :

    - leur emplacement, leur forme    
      [cf. Le modèle de paysage](../../../../apprentissage/-/blob/master/analyse_spatiale/a160-le_modele_de_paysage.pdf)

    - équipements
    - activité des entreprises
    - âge, formation, revenus de la population
