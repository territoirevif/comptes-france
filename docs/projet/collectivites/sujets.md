__L'analyse de l'économie des territoires, les manière d'aider leur développement ont motivé la fondation de ce projet open source.__  

Cette page énumère des sujets qu'elle pourrait aviser.

# Voici quelques questions que des chargés d'études, agents ou élus territoriaux peuvent se poser

## Quelles communes, quelles intercommunalités ressemblent à la nôtre ?

### Comment font-elles face aux problèmes que nous rencontrons ?

- En les observant, nous incitent-elles à prendre un rôle qui aiderait notre territoire ? Lequel ?
  
  - leurs intercommunalités ou syndicats exercent-ils des compétences que nous n'exerçons pas ?

  - Des communes limitrophes pourraient-elles ajouter à un Service Public Local (SPL) un rôle supplémentaire ?


- Quel mode de gestion utilisent-elles pour servir une compétence, gérer un équipement ou service ?  
  Ce choix est-il plus intéressant que le nôtre ?

  - Déterminer le mode de gestion utilisé pour une compétence, un équipement ou service    
    [cf. Services publics et montages, mode de gestion](../../../../apprentissage/-/blob/master/territoire/t305-services_publics-et-montages-modes_de_gestion.pdf)

    - Relier les comptes impliqués  
    - Est-ce l'intercommunalité qui exerce la compétence ou a t-elle été renvoyée, par contrat, à certaines communes ?


## Développement, attractivité, revitalisation

- Quel commerce ou service profiterait aux entreprises locales, s'il apparaissait ? Où ?

- Les professionnels de quels métiers devons-nous attirer ? D'où ?


## Quelles activités, quels métiers dans les collectivités ?

- Ces activités ou métiers croissent-ils ou régressent-ils, localement ?

  - Quels motifs de dépôt de bilan d'une entreprise ? Economique ? Pas de repreneur ?


## Collectivités et programmes opérationnels européens

### Thématiques pour chaque région

Observer les choix de chacune, en fonction de son contexte, pour s'en inspirer.

   - Quelles sont les thématiques des programmes opérationels, par axes, dans chaque région ?

### Quelles collectivités répondent à quels critères d’opérations européennes ? 

Création d'indicateurs en lien avec les Programmes Opérationnels (PO)

   - Quels éléments peuvent argumenter sur un point d’une opération pour une collectivité ?

   - Prise en charge des indicateurs de [réalisation et de résultats européens](https://cohesiondata.ec.europa.eu/2021-2027-Indicators/2021-2027-ERDF-CF-JTF-Common-Indicator-metadata/4t73-mihb/about_data)
