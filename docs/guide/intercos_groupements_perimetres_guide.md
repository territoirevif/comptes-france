Exemples d'emploi du jeu de données intercommunalités, groupements et périmètres
--------------------------------------------------------------------------------

- Obtenir la liste des communes de l'intercommunalité Entr'allier (EPCI 200071470)

> [http://localhost:9090/interco/communes/communesIntercoHtml?anneeCOG=2019&codeEPCI=200071470](http://localhost:9090/interco/communes/communesIntercoHtml?anneeCOG=2019&codeEPCI=200071470)

> `curl -X GET "http://localhost:9090/interco/interco/communes/communesIntercoHtml?anneeCOG=2019&codeEPCI=200071470&locale=fr" -H  "accept: */*"`
