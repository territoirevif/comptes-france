Les balances des comptes et les comptes individuels des communes et intercommunalités
=====================================================================================

- Budgets principaux et annexes des communes et des intercommunalités  

- Comptes individuels des communes et des intercommunalités (analyse financière)

# La balance des comptes

## des communes

Le dataset reprend la balance des comptes des communes en y ajoutant des cumuls par comptes.

| anneeExercice | codeRegion | codeCommune | nomCommune | siren     | siret          | budgetPrincipal | libelleBudget               | numeroCompte | libelleCompte                             | nomenclatureComptable | soldeDebiteur | soldeCrediteur | typeEtablissement | libelleTypeEtablissement | natureEtablissement       | sousTypeEtablissement | libelleSousTypeEtablissement | codeActivite | libelleActivite                | codeSecteur | numeroFINESS | typeBudget | libelleTypeBudget | codeBudget | categorieCollectivite | populationMunicipale | typeBalance | balanceEntreeDebit | balanceEntreeCredit | operationBudgetaireDebit | operationBudgetaireCredit | operationNonBudgetaireDebit | operationNonBudgetaireCredit | operationOrdreBudgetaireDebit | operationOrdreBudgetaireCredit | nombreChiffresNumeroCompte | cumulSD3 | cumulSC3   | cumulSD4 | cumulSC4  | cumulSD5 | cumulSC5 | cumulSD6 | cumulSC6 | codeDepartement |
|---------------|------------|-------------|------------|-----------|----------------|-----------------|-----------------------------|--------------|-------------------------------------------|-----------------------|---------------|----------------|-------------------|--------------------------|---------------------------|-----------------------|------------------------------|--------------|--------------------------------|-------------|--------------|------------|-------------------|------------|-----------------------|----------------------|-------------|--------------------|---------------------|--------------------------|---------------------------|-----------------------------|------------------------------|-------------------------------|--------------------------------|----------------------------|----------|------------|----------|-----------|----------|----------|----------|----------|-----------------|
| 2022          | 053        | 29046       | Douarnenez | 212900468 | 21290046800217 | false           | PORT PLAISANCE - DOUARNENEZ | 1021         | Dotation                                  | M4                    | 0.0           | 943550.32      | 410               | Communes (BA)            | COLLECTIVITE_TERRITORIALE | 00                    | NULL                         | 21           | Gestion des ports et aéroports | NULL        | NULL         | 3          | Budget annexe     | NULL       | Commune               | 13956                | DEF         | 0.0                | 943550.32           | 0.0                      | 0.0                       | 0.0                         | 0.0                          | 0.0                           | 0.0                            | 4                          | 0.0      | 1202355.92 | 0.0      | 943550.32 | 0.0      | 0.0      | 0.0      | 0.0      | 29              |
| 2022          | 053        | 29046       | Douarnenez | 212900468 | 21290046800217 | false           | PORT PLAISANCE - DOUARNENEZ | 10228        | Autres fonds                              | M4                    | 0.0           | 95349.17       | 410               | Communes (BA)            | COLLECTIVITE_TERRITORIALE | 00                    | NULL                         | 21           | Gestion des ports et aéroports | NULL        | NULL         | 3          | Budget annexe     | NULL       | Commune               | 13956                | DEF         | 0.0                | 95349.17            | 0.0                      | 0.0                       | 0.0                         | 0.0                          | 0.0                           | 0.0                            | 5                          | 0.0      | 1202355.92 | 0.0      | 95349.17  | 0.0      | 95349.17 | 0.0      | 0.0      | 29              |
| 2022          | 053        | 29046       | Douarnenez | 212900468 | 21290046800217 | false           | PORT PLAISANCE - DOUARNENEZ | 1027         | Mise à disposition (chez le bénéficiaire) | M4                    | 0.0           | 163456.43      | 410               | Communes (BA)            | COLLECTIVITE_TERRITORIALE | 00                    | NULL                         | 21           | Gestion des ports et aéroports | NULL        | NULL         | 3          | Budget annexe     | NULL       | Commune               | 13956                | DEF         | 0.0                | 163456.43           | 0.0                      | 0.0                       | 0.0                         | 0.0                          | 0.0                           | 0.0                            | 4                          | 0.0      | 1202355.92 | 0.0      | 163456.43 | 0.0      | 0.0      | 0.0      | 0.0      | 29              |

_les premiers comptes du budget annexe du Port de Plaisance de Douarnenez (Finistère), en 2022_

## des intercommunalités

| anneeExercice | codeRegion | codeCommune | nomCommune            | siren     | siret          | budgetPrincipal | libelleBudget        | numeroCompte | libelleCompte                               | nomenclatureComptable | soldeDebiteur | soldeCrediteur | typeEtablissement | libelleTypeEtablissement                | natureEtablissement | sousTypeEtablissement | libelleSousTypeEtablissement | codeActivite | libelleActivite                                                                        | codeSecteur | numeroFINESS | typeBudget | libelleTypeBudget | codeBudget | categorieCollectivite | populationMunicipale | typeBalance | balanceEntreeDebit | balanceEntreeCredit | operationBudgetaireDebit | operationBudgetaireCredit | operationNonBudgetaireDebit | operationNonBudgetaireCredit | operationOrdreBudgetaireDebit | operationOrdreBudgetaireCredit | nombreChiffresNumeroCompte | cumulSD3 | cumulSC3      | cumulSD4 | cumulSC4   | cumulSD5 | cumulSC5   | cumulSD6 | cumulSC6 | codeDepartement |
|---------------|------------|-------------|-----------------------|-----------|----------------|-----------------|----------------------|--------------|---------------------------------------------|-----------------------|---------------|----------------|-------------------|-----------------------------------------|---------------------|-----------------------|------------------------------|--------------|----------------------------------------------------------------------------------------|-------------|--------------|------------|-------------------|------------|-----------------------|----------------------|-------------|--------------------|---------------------|--------------------------|---------------------------|-----------------------------|------------------------------|-------------------------------|--------------------------------|----------------------------|----------|---------------|----------|------------|----------|------------|----------|----------|-----------------|
| 2022          | 024        | 45082       | Châteauneuf-sur-Loire | 244500427 | 24450042700111 | true            | CC DES LOGES JARGEAU | 10222        | F.C.T.V.A.                                  | M14                   | 0.0           | 7732712.52     | 403               | GFP - CC – Communautés de Communes (BP) | EPCI/GFP            | 00                    | NULL                         | 40           | Administration générale (toutes les activités sont retracées dans des budgets annexes) | NULL        | NULL         | 1          | Budget principal  | NULL       | GFP                   | 8212                 | DEF         | 0.0                | 7597763.54          | 0.0                      | 134948.98                 | 0.0                         | 0.0                          | 0.0                           | 0.0                            | 5                          | 30344.0  | 1.692260357E7 | 30344.0  | 7732712.52 | 0.0      | 7732712.52 | 0.0      | 0.0      | 45              |
| 2022          | 024        | 45082       | Châteauneuf-sur-Loire | 244500427 | 24450042700111 | true            | CC DES LOGES JARGEAU | 102291       | Reprise sur F.C.T.V.A.                      | M14                   | 30344.0       | 0.0            | 403               | GFP - CC – Communautés de Communes (BP) | EPCI/GFP            | 00                    | NULL                         | 40           | Administration générale (toutes les activités sont retracées dans des budgets annexes) | NULL        | NULL         | 1          | Budget principal  | NULL       | GFP                   | 8212                 | DEF         | 30344.0            | 0.0                 | 0.0                      | 0.0                       | 0.0                         | 0.0                          | 0.0                           | 0.0                            | 6                          | 30344.0  | 1.692260357E7 | 30344.0  | 7732712.52 | 30344.0  | 0.0        | 30344.0  | 0.0      | 45              |
| 2022          | 024        | 45082       | Châteauneuf-sur-Loire | 244500427 | 24450042700111 | true            | CC DES LOGES JARGEAU | 1027         | Mise à disposition (chez le bénéficiaire) 2 | M14                   | 0.0           | 9189891.05     | 403               | GFP - CC – Communautés de Communes (BP) | EPCI/GFP            | 00                    | NULL                         | 40           | Administration générale (toutes les activités sont retracées dans des budgets annexes) | NULL        | NULL         | 1          | Budget principal  | NULL       | GFP                   | 8212                 | DEF         | 0.0                | 9189891.05          | 0.0                      | 0.0                       | 0.0                         | 0.0                          | 0.0                           | 0.0                            | 4                          | 30344.0  | 1.692260357E7 | 0.0      | 9189891.05 | 0.0      | 0.0        | 0.0      | 0.0      | 45              |

_les premiers comptes du budget principal de la communauté de communes des Loges (Loiret), en 2022_

# Les comptes individuels

## des communes

```txt
Libellé du budget : Douarnenez
Population légale en vigueur au 1er janvier de l'exercice : 14520	Budget principal seul
Strate : Strate commune : communes de 10 000 à 20 000 hab		Type groupement d'appartenance : appartenant à un groupement fiscalisé (FPU)
Exercice : 01/01/2019		N° de département : 29		N° insee de la commune : 29046		N° de région : 53


ANALYSE DES EQUILIBRES FINANCIERS FONDAMENTAUX

En milliers d'Euros	Euros par habitant	Moyenne de la strate	OPERATIONS DE FONCTIONNEMENT	Ratios de structure	Moyenne de la strate (en %% des produits)


						OPERATIONS DE FONCTIONNEMENT

     21442	      1477	      1354	TOTAL DES PRODUITS DE FONCTIONNEMENT = A			en % des produits CAF
     21240	      1463	      1303		PRODUITS DE FONCTIONNEMENT CAF
     12379	       853	       563			dont : Impôts Locaux			     58,28	     43,19
      1418	        98	       115			Autres impôts et taxes				      8,82
      1794	       124	       177			Dotation globale de fonctionnement	      8,44	     13,57

     20000	      1377	      1213	TOTAL DES CHARGES DE FONCTIONNEMENT = B				en % des charges CAF
     18156	      1250	      1107		Charges de fonctionnement CAF
     11226	       773	       655			dont : Charges de personnel		     61,83	     59,18
      4493	       309	       272			achats et charges externes		     24,75	     24,58
       911	        63	        24			Charges financières			      5,02	      2,14
       229	        16	        33			Contingents				      1,26	      2,98
      1024	        71	        89			Subventions versées			      5,64	      8,02
      1442	        99	       141	Résultat comptable = A - B = R


						OPERATIONS D'INVESTISSEMENT

      7099	       489	       482	TOTAL DES RESSOURCES D'INVESTISSEMENT = C			en % des ressources
      1800	       124	        79		dont : Emprunts bancaires et dettes assimilées	     25,36	     16,33
       526	        36	        72		Subventions reçues				      7,42	     14,84
       450	        31	        40		Fonds de compensation de la TVA (FCTVA)		      6,34	      8,28
         0	         0	         0		Retour de biens affectés, concédés, ...		      0,00	      0,00

      6481	       446	       500	TOTAL DES EMPLOIS D'INVESTISSEMENT = D				en % des emplois
      4515	       311	       376		dont : Dépenses d'équipement			     69,66	     75,37
      1926	       133	        86		Remboursement d'emprunts et dettes assimilées	     29,72	     17,18
         0	         0	         1		Charges à répartir				      0,00	      0,11
         0	         0	         0		Immobilisations affectées, concédées, ...	      0,00	      0,00

      -618	       -43	        17	Besoin ou capacité de financement résiduel de la section d'investissement = D - C
         0	         0	         1	+ Solde des opérations pour le compte de tiers
      -617	       -43	        18	Besoin ou capacité de financement de la section d'investissement = E
      2059	       142	       123	Résultat d'ensemble = R - E


						AUTOFINANCEMENT							en % des produits CAF

      3855	       266	       214	Excédent brut de fonctionnement				     18,15	     16,43
      3084	       212	       196	Capacité d'autofinancement = CAF			     14,52	     15,01
      1158	        80	       110	CAF nette du remboursement en capital des emprunts	      5,45	      8,43

						ENDETTEMENT							en % des produits CAF
     25092	      1728	       850	Encours total de la dette au 31 décembre N		    118,13	     65,23
     25092	      1728	       837	Encours des dettes bancaires et assimilées		    118,13	     64,27
     25064	      1726	       828	Encours des dettes bancaires net de l'aide du fonds de soutien pour la sortie des emprunts toxiques	    118,00	     63,52
      2837	       195	       108	Annuité de la dette					     13,36	      8,28
      1516	       104	       286	FONDS DE ROULEMENT


						ELEMENTS DE FISCALITE DIRECTE LOCALE
Les bases imposées et les réductions (exonérations, abattements) accordées sur délibérations
Bases nettes imposées au profit de la commune               Taxe					Réductions de base accordées sur délibérations
En milliers d'Euros  Euros par habitant		Moyenne de la strate			En milliers d'Euros     Euros par habitant   Moyenne de la strate
     25249	      1739	      1441	Taxe d'habitation (y compris THLV)			   2142,17	    147,53	    116,45
     23211	      1599	      1384	Taxe foncière sur les propriétés bâties			      0,00	      0,00	      0,86
       177	        12	        11	Taxe foncière sur les propriétés non bâties		      0,00	      0,00	      0,02
         0	         0	         0	Taxe additionnelle à la taxe foncière sur les propriétés non bâties
         0	         0	         0	Cotisation foncière des entreprises			      0,00	      0,00	      0,00

Les taux et les produits de la fiscalité directe locale
Produits des impôts locaux            Taxe								Taux voté (%)	Taux moyen de la strate (%)
      6133	       422	       240	Taxe d'habitation (y compris THLV)			     24,27	     16,66
      6135	       423	       313	Taxe foncière sur les propriétés bâties			     26,38	     22,58
       116	         8	         6	Taxe foncière sur les propriétés non bâties		     65,39	     53,69
         0	         0	         0	Taxe additionnelle à la taxe foncière sur les propriétés non bâties		      0,00	      0,00
         0	         0	         0	Cotisation foncière des entreprises			      0,00	      0,00

Les produits des impôts de répartition
Produits des impôts de répartition      Taxe
         0	         0	         0	Cotisation sur la valeur ajoutée des entreprises
         0	         0	         0	Imposition forfaitaire sur les entreprises de réseau
         0	         0	         0	Taxe sur les surfaces commerciales
```

### Incident ouvert auprès de [Collectivités Territoriales](#incident1)

## des intercommunalités

```txt
Libellé du budget : CC DOUARNENEZ COMMUNAUTE
Population légale en vigueur au 1er janvier de l'exercice : 18755	Budget principal seul
Exercice : 2022		Code EPCI : 242900645


ANALYSE DES EQUILIBRES FINANCIERS FONDAMENTAUX

En milliers d'Euros	Euros par habitant	Moyenne de la strate	OPERATIONS DE FONCTIONNEMENT	Ratios de structure	Moyenne de la strate (en % des produits)


						OPERATIONS DE FONCTIONNEMENT

     23445	      1250	TOTAL DES PRODUITS DE FONCTIONNEMENT = A			en % des produits CAF	     44,51
     22377	      1193		PRODUITS DE FONCTIONNEMENT CAF	     42,31
      4516	       241			dont : Impôts Locaux			
      4409	       235			Autres impôts et taxes			
      1608	        86			Dotation globale de fonctionnement	

     20681	      1103	TOTAL DES CHARGES DE FONCTIONNEMENT = B				en % des charges CAF	     44,54
     17403	       928		Charges de fonctionnement CAF	     41,17
      7950	       424			dont : Charges de personnel		     33,80
      6651	       355			achats et charges externes		     58,63
       750	        40			Charges financières			     46,83
       787	        42			Subventions versées			      0,00
      2764	       147	Résultat comptable = A - B = R	     44,25


						OPERATIONS D'INVESTISSEMENT

      8974	       479	TOTAL DES RESSOURCES D'INVESTISSEMENT = C			en % des ressources	     43,65
      2330	       124		dont : Emprunts bancaires et dettes assimilées	     37,93
       918	        49		Subventions reçues				     55,68
       396	        21		Fonds de compensation de la TVA (FCTVA)		     47,17

     10424	       556	TOTAL DES EMPLOIS D'INVESTISSEMENT = D				en % des emplois	     60,54
      6174	       329		dont : Dépenses d'équipement			     68,50
      2025	       108		Remboursement d'emprunts et dettes assimilées	     54,62



						AUTOFINANCEMENT							en % des produits CAF

      4974	       265	Capacité d'autofinancement = CAF			     84,57
      2949	       157	CAF nette du remboursement en capital des emprunts	    100,48

						ENDETTEMENT							en % des produits CAF
     27264	      1454	Encours total de la dette au 31 décembre N		     67,55
     27207	      1451	Encours des dettes bancaires et assimilées		     67,78
     27207	      1451	Encours des dettes bancaires net de l'aide du fonds de soutien pour la sortie des emprunts toxiques	     67,78
      2706	       144	Annuité de la dette					     70,74


						ELEMENTS DE FISCALITE DIRECTE LOCALE
Les bases imposées et les réductions (exonérations, abattements) accordées sur délibérations
Bases nettes imposées au profit de la commune               Taxe					Réductions de base accordées sur délibérations
En milliers d'Euros  Euros par habitant		Moyenne de la strate			En milliers d'Euros     Euros par habitant   Moyenne de la strate
      5960	       318	Taxe d'habitation (y compris THLV)				
     26978	      1438	Taxe foncière sur les propriétés bâties			      0,00
       450	        24	Taxe foncière sur les propriétés non bâties		      0,00
        83	         4	Taxe additionnelle à la taxe foncière sur les propriétés non bâties	
      5972	       318	Cotisation foncière des entreprises			     15,00

Les taux et les produits de la fiscalité directe locale
Produits des impôts locaux            Taxe								Taux voté (%)	Taux moyen de la strate (%)
       683	        36	Taxe d'habitation (y compris THLV)			     11,47
       410	        22	Taxe foncière sur les propriétés bâties			      1,52
        12	         1	Taxe foncière sur les propriétés non bâties		      2,75
        24	         1	Taxe additionnelle à la taxe foncière sur les propriétés non bâties		     29,17
      1595	        85	Cotisation foncière des entreprises			     26,76

Les produits des impôts de répartition
Produits des impôts de répartition      Taxe
      1254	        67	Cotisation sur la valeur ajoutée des entreprises	
        73	         4	Imposition forfaitaire sur les entreprises de réseau	
       320	        17	Taxe sur les surfaces commerciales	
```

# Les plans de comptes soutenus

| Nomenclature | Libellé                                                                                    |
|--------------|--------------------------------------------------------------------------------------------|
| M14          | Communes et groupements à fiscalité propre (GFP)                                           |
| M14A         | Communes de moins de 500 habitants                                                         |
| M21          | Comptabilité des établissements publics de santé                                           |
| M22          | Établissements sociaux et médico-sociaux (EPSMS)                                           |
| M4           | Services publics locaux à caractère industriel ou commercial                               |
| M41          | Services publics locaux de production et distribution d'énergies                           |
| M42          | Services publics locaux des abattoirs                                                      |
| M43          | Services publics locaux de transport de personnes                                          |
| M43A         | Services publics de transport de personnes - Nomenclature M43 abrégée                      |
| M44          | Établissements publics fonciers locaux                                                     |
| M49          | Services publics d'assainissement et distribution d'eau potable                            |
| M49A         | Services publics d'assainissement et distribution d'eau potable - Nomenclature M49 abrégée |
| M52          | Départements                                                                               |
| M57          | Collectivités territoriales uniques et Métropoles                                          |
| M57A         | Collectivités territoriales uniques et Métropoles - Nomenclature M57 abrégée               |
| M61          | Services départementaux d'incendie et de secours (SDIS)                                    |
| M71          | Régions                                                                                    |
| M832         | Centres départementaux de gestion de la fonction publique territoriale (CDFPT)             |


# Source des données

Fournisseur : DGFIP

URL : [https://data.economie.gouv.fr/explore/?sort=modified&refine.publisher=DGFiP](https://data.economie.gouv.fr/explore/?sort=modified&refine.publisher=DGFiP)

[Ministère de l'économie](https://data.economie.gouv.fr/explore/?sort=modified)
  ou plus détaillé : [ici](https://data.economie.gouv.fr/explore/?sort=title)

Voici les liens de téléchargement des données brutes traitées par l'application, et leur lieu de stockage :

## Balances comptables

- des communes en [2022](https://data.economie.gouv.fr/explore/dataset/balances-comptables-des-communes-en-2022/export/),
  [2021](https://data.economie.gouv.fr/explore/dataset/balances-comptables-des-communes-en-2021/export/),
  [2020](https://data.economie.gouv.fr/explore/dataset/balances-comptables-des-communes-en-2020/export/)  
  - stockage : `/comptabilite/[année]/balances-communes.csv`  
  - date de la dernière acquisition des données : celles téléchargeables au 30/06/2024 
  - date d'effet : exercice 2022
  
- [des groupements à fiscalité propre depuis 2010](https://data.economie.gouv.fr/explore/dataset/balances-comptables-des-groupements-a-fiscalite-propre-depuis-2010/export)
  - stockage : `$COMPTES_DIR/[année]/balances-communes.csv`, par défaut valorisé à : `/data/comptes-france/comptabilite/[année]/balances-communes.csv`

## Comptes individuels des communes [ou intercommunalités]

- [Des communes, de 2000 à 2022](https://data.economie.gouv.fr/explore/dataset/comptes-individuels-des-communes-fichier-global-a-compter-de-2000/information/)  
   Au 05/06/2024, à sa source, il est endommagé : de nombreux champs lui manquent et il ne respecte plus sa maquette.  
   Celui présent dans l'application porte sur l'exercice 2019.

- [des groupements à fiscalité propre de 2007 à 2022](https://data.economie.gouv.fr/explore/dataset/comptes-individuels-des-groupements-a-fiscalite-propre-fichier-global-a-compter-/information/)

## Autres éléments

- [Plans de comptes](https://www.collectivites-locales.gouv.fr/finances-locales/instructions-budgetaires-et-comptables)    
  Extraits des instructions budgétaires et comptables (présentes au format `.pdf`)
  - stockage : `comptabilite/plan-de-comptes_[nomenclature].csv`
  - date de la dernière acquisition des données : celles téléchargeables le 20/09/2020 pour les nomenclatures `M14`, `M14A`, `M22`, `M4`, `M41`, `M43`, `M49`, `M49A`, `M57`    
    30/06/2024 pour `M57A`


# Exemples d'emploi

- La balance des comptes de la commune de Douarnenez (29046) en 2022

  [Depuis un navigateur Internet](http://localhost:9090/comptesCommunes/codeCommune?annee=2022&anneeCOG=2022&codeCommune=29046)

   ```bash
   curl -X 'GET' \
   'http://localhost:9090/comptesCommunes/codeCommune?anneeCOG=2022&annee=2022&codeCommune=29046' \
   -H 'accept: */*'
   ```

  En réponse, il retourne des comptes :


   ```json
   {
         "numeroCompte": "2154",
         "anneeExercice": 2022,
         "codeCommune": "29046",
         "siret": "21290046800217",
         "libelleBudget": "PORT PLAISANCE - DOUARNENEZ",
         "libelleCompte": "Matériel industriel",
         "budgetPrincipal": false,
         "soldeCrediteur": 0,
         "soldeDebiteur": 1708736.83
   }
   ```

- Obtenir les comptes individuels de 2019 de la commune de Douarnenez (29046) – COG 2019

> [http://localhost:9090/comptesCommunes/comptesIndividuelsCommune?annee=2019&anneeCOG=2019&codeCommune=29046](http://localhost:9090/comptesCommunes/comptesIndividuelsCommune?annee=2019&anneeCOG=2019&codeCommune=29046)

- Obtenir les comptes individuels de 2019 de l'intercommunalité CC Douarnenez (EPCI : 242900645) – COG 2019

> [http://localhost:9090/comptesCommunes/comptesIndividuelsIntercommunalite?annee=2019&anneeCOG=2019&codeEPCI=242900645](http://localhost:9090/comptesCommunes/comptesIndividuelsIntercommunalite?annee=2019&anneeCOG=2019&codeEPCI=242900645)


## Jeux de données existants mais non encore exploités

### Vont être prochainement extraits

- [des départements depuis 2010](https://data.economie.gouv.fr/explore/dataset/balances-comptables-des-departements/export/)

- [des établissements publics locaux depuis 2010](https://data.economie.gouv.fr/explore/dataset/balances-comptables-des-etablissements-publics-locaux-depuis-2010/export/)

- [des régions depuis 2010](https://data.economie.gouv.fr/explore/dataset/balances-comptables-des-regions-/export/)

- [des syndicats depuis 2010](https://data.economie.gouv.fr/explore/dataset/balances-comptables-des-syndicats-depuis-2010/export/)

### Pourraient être intéressants à examiner

- [des collectivités et des établissements publics locaux avec la présentation croisée nature-fonction](https://data.economie.gouv.fr/explore/dataset/balances-comptables-des-collectivites-et-des-etablissements-publics-locaux-avec7/export/)

- [Observatoire des finances et de la gestion publique locales (OFGL)](https://data.ofgl.fr/explore/?exclude.theme=INTERNE&sort=title) : Comptes consolidés, aggrégats comptables,
    * avec un fichier méthodologique qui donne les formules employées.
    * Dotation des communes : montants et critères, donne la population DGF.

- [Observatoire des territoires : potentiel financier par habitant](https://www.observatoire-des-territoires.gouv.fr/potentiel-financier-par-habitant)

---

### [1] Incident ouvert auprès de [Collectivités Territoriales](https://www.collectivites-locales.gouv.fr/) le 22/08/2024{#incident1}  
sur le sujet du fichier des comptes individuels de communes

Je vous soumets un problème concernant le fichier des comptes individuels des communes que vous publiez depuis l'exercice 2022.

Il s'agit d'un fichier global pour plusieurs années, mais il lui manque à présent de nombreux champs qui existaient encore en 2020, et qui sont mentionnés dans la maquette adjointe donnant la présentation de l'analyse financière d'une commune pour une année.

Quand je compare les versions du fichier
comptes-individuels-des-communes-fichier-global-a-compter-de-2000.csv que j'ai téléchargé en 2020 et très récemment ce mois d'Août 2024, je constate :

qu'ont été ajoutés les champs :

```
avance
favance
fpotfis
fptp
ftamen
mavance
mpotfis
mptp
mtamen
pop2
potfis
ptp
ravance
rimpo2
rmavance
rmtamen
rtamen
tamen
```

Et supprimés ceux-ci :

```
bfb
bfbexod
bfnb
bfnbexod
btafnb
bth
bthexod
btp
btpexod
cfcaf
cvaec
det2cal
encdbr
fbfb
fbfbexod
fbfnb
fbfnbexod
fbtafnb
fbth
fbthexod
fbtp
fbtpexod
fcfcaf
fcvaec
fdet2cal
fencdbr
fiferc
fpcfe
fpfcaf
fptafnb
ftascomc
iferc
mbfb
mbfbexod
mbfnb
mbfnbexod
mbtafnb
mbth
mbthexod
mbtp
mbtpexod
mcfcaf
mcvaec
mdet2cal
mencdbr
miferc
mpcfe
mpfcaf
mptafnb
mtascomc
mtmtafnb
pcfe
pfcaf
ptafnb
rdet2cal
repart
rmdet2cal
rmencdbr
tafnb
tascomc
```

(ce que vous pouvez constater assez commodément si vous exécutez un :

```bash
head -n 1 comptes-individuels-des-communes-fichier-global-a-compter-de-2000.csv | sed 's/;/\n/g' | sort
```

sur chacun des fichiers)

Ce sont près de soixante champs qui ont disparu, alors que les maquettes n'ont que peu bougé.

Comme les valeurs de ces champs sont soit encore employées dans l'exercice 2022, mais en tout cas, l'ont été dans les états 2021 et antérieurs, ils doivent demeurer pour que l'on puisse les recréer.  
(exemple : la taxe d'habitation peut ne plus avoir de sens en 2022, elle en avait encore une en 2017 ou 2018, et recréer une analyse financière pour ces années réclame qu'elle soit là). 
