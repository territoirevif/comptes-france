Le maillage territorial en régions, départements, intercommunalités, communes, et autres groupements
====================================================================================================

- La France est déclinée en régions, départements, intercommunalités, communes

## Communes

- Communes géolocalisées (avec choix d'inclure celles déléguées ou associées)

|typeCommune|codeCommune|nomCommune          |populationTotale|populationMunicipale|populationCompteApart|codeRegion|nomDepartement|arrondissement|codeCanton|codeCommuneParente|sirenCommune|codeEPCI |nomEPCI                           |natureJuridiqueEPCI|strateCommune|latitude          |longitude         |surface|typeNomEtCharniere|nomMajuscules       |typeNomEtCharniereDepartement|codeCommuneChefLieuDepartement|codeDepartement|
|-----------|-----------|--------------------|----------------|--------------------|---------------------|----------|--------------|--------------|----------|------------------|------------|---------|----------------------------------|-------------------|-------------|------------------|------------------|-------|------------------|--------------------|-----------------------------|------------------------------|---------------|
|COM        |62001      |Ablain-Saint-Nazaire|1979            |1939                |40                   |32        |Pas-de-Calais |627           |6217      |NULL              |216200014   |246200364|CA de Lens - Liévin               |CA                 |3            |50.39673221866612 |2.6975687515901514|996.0  |1                 |ABLAIN SAINT NAZAIRE|2                            |62041                         |62             |
|COM        |62002      |Ablainzevelle       |236             |233                 |3                    |32        |Pas-de-Calais |621           |6209      |NULL              |216200022   |200035442|CC du Sud Artois                  |CC                 |1            |50.15304459722964 |2.741154945755897 |429.0  |1                 |ABLAINZEVELLE       |2                            |62041                         |62             |
|COM        |62003      |Acheville           |612             |589                 |23                   |32        |Pas-de-Calais |627           |6208      |NULL              |216200030   |246200364|CA de Lens - Liévin               |CA                 |3            |50.38460420135926 |2.8824774376786424|307.0  |1                 |ACHEVILLE           |2                            |62041                         |62             |

_quelques communes du Pas-de-Calais_

Ce jeu de données est disponible sur [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/code-officiel-geographique-2024/)


## Evolution des communes au fil du temps / historique des communes

Une commune peut changer de nom, fusionner. Les anciennes, devenir déléguées ou associées.

Une fonction dit qui se substitue à elle, si c'est le cas, à une date donnée.

  > Qu'est devenue __Saint-Bois__ (code commune `01340`) au 1er Janvier 2024 ?
  > 
  > Elle est devenue __Arboys en Bugey__ (code commune `01015`) au 1er Janvier 2016  
  > par création de commune nouvelle
