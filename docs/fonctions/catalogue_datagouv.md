Le catalogue des jeux de données de data.gouv.fr
================================================

Rechercher des jeux de données sur Internet, [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/catalogue-des-donnees-de-data-gouv-fr/) ou le site web de leur producteur est possible, mais fastidieux et incertain.

Par commodité, [le Portail Interministériel en dresse la liste](https://www.data.gouv.fr/fr/datasets/catalogue-des-donnees-de-data-gouv-fr/). Celle-ci est reprise et indexée.  

   - Pour permettre une par titre, description ou organisation l'ayant produit.

| id_catalogue             | titre_catalogue                                       | slug_catalogue                                   | acronyme_catalogue | url_catalogue                                                                         | organisation_catalogue                                     | id_organisation_catalogue | owner | owner_id | description_catalogue                                                                                                                                                                              | frequence_catalogue | licence_catalogue   | date_debut_couverture_catalogue | date_fin_couverture_catalogue | granularite_spatiale_catalogue | zones_spatiales_catalogue | featured_catalogue | date_creation_catalogue | date_modification_catalogue | tags_catalogue                                                                                       | archive_catalogue | nombre_de_ressources_catalogue | main_resources_count | downloads | nombre_moissonneurs_catalogue | moissonneur_domaine_catalogue | moissonneur_date_creation_catalogue | moissonneur_date_modification_catalogue | score_qualite_catalogue | metrique_discussions_catalogue | metrique_reutilisation_catalogue | metrique_suiveurs_catalogue | metrique_vues_catalogue | metric.resources_downloads |
|--------------------------|-------------------------------------------------------|--------------------------------------------------|--------------------|---------------------------------------------------------------------------------------|------------------------------------------------------------|---------------------------|-------|----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------|---------------------|---------------------------------|-------------------------------|--------------------------------|---------------------------|--------------------|-------------------------|-----------------------------|------------------------------------------------------------------------------------------------------|-------------------|--------------------------------|----------------------|-----------|-------------------------------|-------------------------------|-------------------------------------|-----------------------------------------|-------------------------|--------------------------------|----------------------------------|-----------------------------|-------------------------|----------------------------|
| 601ddc2c1dc7a081b3c24368 | Adresses postales                                     | sync7e3dff7-1e251473-4dc0-4f03-bb89-aa54269c8e3f | NULL               | http://www.data.gouv.fr/fr/datasets/sync7e3dff7-1e251473-4dc0-4f03-bb89-aa54269c8e3f/ | Communauté d'agglomération Arles Crau Camargue Montagnette | 534fff55a3a7292c64a77cde  | NULL  | NULL     | Base données locale des adresses postales                                                                                                                                                          | NULL                | Other (Attribution) | NULL                            | NULL                          | NULL                           | NULL                      | false              | 2000-01-01              | 2021-02-06                  | administration,adresse,adresses,donnees-publiques,france,localisation,numero,postale,voirie          | NULL              | 1                              | 1                    | 3         | NULL                          | trouver.datasud.fr            | NULL                                | NULL                                    | 0.22                    | 0                              | 0                                | 0                           | 298                     | NULL                       |
| 601ddc2c77a857095dc1ffa2 | Conteneurs pour la collecte des ordures ménagères     | syncf263124-e995d010-f8ef-4f2f-8129-05865cb83936 | NULL               | http://www.data.gouv.fr/fr/datasets/syncf263124-e995d010-f8ef-4f2f-8129-05865cb83936/ | Communauté d'agglomération Arles Crau Camargue Montagnette | 534fff55a3a7292c64a77cde  | NULL  | NULL     | Conteneurs pour la collecte des ordures ménagères, placés sur la voie publique, avec indication de la contenance du bac et du type de bac (roulant ou enterré).                                    | NULL                | Other (Attribution) | NULL                            | NULL                          | NULL                           | NULL                      | false              | 2016-06-30              | 2021-02-06                  | conteneur,dechets,donnees-publiques,environnement,france,menagers,ordures-menageres                  | NULL              | 1                              | 1                    | 8         | NULL                          | trouver.datasud.fr            | NULL                                | NULL                                    | 0.22                    | 0                              | 0                                | 0                           | 304                     | 1                          |
| 601ddc2c790d424193c24374 | Zones d'activités économiques - Périmètres en vigueur | sync2c91750-98fbf851-fa39-495a-8a8a-489b83e37100 | NULL               | http://www.data.gouv.fr/fr/datasets/sync2c91750-98fbf851-fa39-495a-8a8a-489b83e37100/ | Communauté d'agglomération Arles Crau Camargue Montagnette | 534fff55a3a7292c64a77cde  | NULL  | NULL     | Périmètres des zones d'activités économiques de compétence communautaire ou communale. Les périmètres de compétence communautaire sont définis par la délibération CC2017_122 du 12 juillet 2017 . | NULL                | Other (Attribution) | NULL                            | NULL                          | NULL                           | NULL                      | false              | 2011-01-14              | 2021-02-06                  | activite,commerce,donnees-publiques,economie,france,industrie,systemes-de-maillage-geographique,zone | NULL              | 1                              | 1                    | 3         | NULL                          | trouver.datasud.fr            | NULL                                | NULL                                    | 0.22                    | 0                              | 0                                | 0                           | 335                     | NULL                       |


# Source des données

Voici les liens de téléchargement des données brutes traitées par l'application, et leur lieu de stockage. Les éventuels traitements qu'il faut accomplir dessus pour les faire prendre en charge par l'application :

Fournisseur : Portail unique interministériel

## Datasets : la liste des jeux de données de data.gouv.fr

Il dresse la liste de chaque jeu de données distinct existant. Son titre, son but, son rôle.

URL : https://www.data.gouv.fr/fr/datasets/catalogue-des-donnees-de-data-gouv-fr/

Parution : Quotidienne

## Resources : déclinaison en fichiers horodatés des jeux de données

Chaque jeu de données peut se décliner en plusieurs fichiers, disponibles à différentes dates.  
Ce fichier les rassemble.   

## Organization : liste des producteurs / fournisseurs de ces données open data

## Tag : les étiquettes associées aux données

## Harvest : la situation des moissonneurs de données

---

# Exemples d'emploi

## Recherche de contenu dans le catalogue data.gouv.fr 

### Paramètres

#### searchText

Les mots à rechercher.  
Ce paramètre est au format _simple query_ d'Elastic :  
 > `|quimper | navettes`  
 >
 > recherchera les entrées du catalogue qui mentionnent `quimper` ET `navettes` dans n'importe lequel de leurs champs

#### field (optionnel)
Le nom du champ dans lequel limiter la recherche  
usage suggéré : `titre`, `description` ou `organisation`


### Retour

Exemple : les trois premiers retours de la recherche de `quimper` limitée au champ `titre`

```json
[
  {
    "catalogueId": "5b913fe1634f4175ddf4834a",
    "titre": "Quartiers - Ville de Quimper",
    "slug": "quartiers-ville-de-quimper",
    "acronyme": null,
    "url": "http://www.data.gouv.fr/fr/datasets/quartiers-ville-de-quimper/",
    "organisationId": "5a82e90b88ee387c99662abb",
    "organisation": "Quimper Bretagne Occidentale",
    "description": "Quartiers de la ville de Quimper. Les Quartiers correspondent aux communes d&#x27;avant la fusion de 1960 (Grand Quimper). Leurs périmètres délimitent les conseils de quartiers de la Ville de Quimper.\n\n__Origine__\n\nMise à jour par les communes par l&#x27;intermédiaire d&#x27;une application de gestion des points adresses\n\n\n__Organisations partenaires__\n\nQuimper Bretagne Occidentale\n\n➞ [Consulter cette fiche sur geo.data.gouv.fr](https://geo.data.gouv.fr/fr/datasets/13b26acb568972c9ae50159ca0edd81136502772)",
    "frequence": "unknown",
    "licence": "Licence Ouverte / Open Licence version 2.0",
    "dateDebut": null,
    "dateFin": null,
    "granulariteSpatiale": "other",
    "zonesSpatiales": "Quimper",
    "prive": null,
    "dateCreation": [
      2018,
      9,
      6
    ],
    "dateModification": [
      2020,
      1,
      24
    ]
  },
  {
    "catalogueId": "61eebf7b309d5631d6bce9c4",
    "titre": "Hôtel Mercure Quimper",
    "slug": "hotel-mercure-quimper",
    "acronyme": null,
    "url": "http://www.data.gouv.fr/fr/datasets/hotel-mercure-quimper/",
    "organisationId": "6061aa4e33c4c10f542093f8",
    "organisation": "Zephyre",
    "description": "2 bornes 22 kW AC",
    "frequence": "unknown",
    "licence": "License Not Specified",
    "dateDebut": null,
    "dateFin": null,
    "granulariteSpatiale": null,
    "zonesSpatiales": null,
    "prive": null,
    "dateCreation": [
      2022,
      1,
      24
    ],
    "dateModification": [
      2022,
      3,
      29
    ]
  },
  {
    "catalogueId": "5b5f06c688ee38311753817c",
    "titre": "Voies - Quimper Bretagne Occidentale",
    "slug": "voies-quimper-bretagne-occidentale",
    "acronyme": null,
    "url": "http://www.data.gouv.fr/fr/datasets/voies-quimper-bretagne-occidentale/",
    "organisationId": "5a82e90b88ee387c99662abb",
    "organisation": "Quimper Bretagne Occidentale",
    "description": "Filaire de voies Quimper Bretagne Occidentale.\nLes segments de voies sont renseignés par catégorie et par usage \n  \n•  Liste des catégories \n Code;Libellé;Description \nAU;Autoroute;Voie à deux sens de circulation séparée où la vitesse est limitée à 130 km/h \nVE;Voie express;Voie à deux sens de circulation séparée où la vitesse est limitée à 110 km/h \nV1;Voie principale;Axe important desservant l&#x27;agglomération  \nV2;Voie secondaire;Axe de fréquentation moyenne desservant les quartiers ou les lieuxdits  \nV3;Voie tertiaire;Voie de quartier ou lieu-dit \nV4;Voie quaternaire;Voie étroite de quartier ou lieu-dit \nBR; Bretelle;Bretelle d&#x27;autoroute ou de voie-express  \n \n•  Liste des usages \nCode;Libellé;Description \nM;Mixte;Voie à priorité piétonne ouverte à la circulation \nC;Circulation;Voie ouverte à la circulation des véhicules  \nP;Piéton;Voie exclusivement piétonne \nB;BUS;Voie exclusivement réservée aux transports en commun \n\n•  Liste des particularités \nCode;Libellé;Description \nO;Sans particularité   \nPO;Passe sur un ouvrage d’art;Ouvrage d’art sur lequel la voie passe  \nTU;Passe sous un ouvrage d’art;Ouvrage d’art sous lequel la voie passe \nPS;Place sans stationnement;Espace dégagé sans stationnement possible \nPK;Place avec stationnement;Espace dégagé où le stationnement est autorisé \nES;Escalier;Série de marches servant à monter ou à descendre\n\n__Origine__\n\nMise à jour effectuée par la cellule SIG de Quimper Bretagne Occidentale à partir d&#x27;orthophotographie et de plans topologiques, en fonction de remontés d&#x27;information des communes de l&#x27;agglomération\n\n\n__Organisations partenaires__\n\nQuimper Bretagne Occidentale\n\n➞ [Consulter cette fiche sur geo.data.gouv.fr](https://geo.data.gouv.fr/fr/datasets/23798d41abec9aa16be7d022340fb69a7c8d1fda)",
    "frequence": "unknown",
    "licence": "Licence Ouverte / Open Licence version 2.0",
    "dateDebut": null,
    "dateFin": null,
    "granulariteSpatiale": "other",
    "zonesSpatiales": null,
    "prive": null,
    "dateCreation": [
      2018,
      7,
      30
    ],
    "dateModification": [
      2020,
      1,
      24
    ]
  }
]
```

## Obtenir la liste des valeurs énumérées d'un champ présent dans les jeux de données de data.gouv.fr

Les champs du catalogue [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/catalogue-des-donnees-de-data-gouv-fr/) :

   - `frequence`
   - `licence`
   - `granularité spatiale`
   - `zones spatiales`
   - `tags (étiquettes)`
   - `score qualité`

contiennent des valeurs énumérées qui ne sont pas documentées par des métadonnées.  
Cette méthode permet de retrouver les valeurs distinctes qu'elles peuvent prendre.

En retour, une liste de (valeur distincte, nombre d'occurrences) triée par nombre d'occurrences décroissant est renvoyée.

### Paramètres

- __Nom du champ__ à valeurs énumérées dont on veut connaître les valeurs distinctes.  
  Il peut prendre les valeurs suivantes :

    - `frequence_catalogue` : les fréquences de parution de catalogue

         ```bash
         curl -X 'GET' \
           'http://localhost:9090/catalogue/enumererValeursDistinctesChampDataGouvFr?champ=frequence_catalogue' \
           -H 'accept: */*'
         ```
    
         http://localhost:9090/catalogue/enumererValeursDistinctesChampDataGouvFr?champ=frequence_catalogue
    
         #### Retour
            
         ```json
         {  
            "unknown": 36401,
            "annual": 4536,
            "punctual": 2809,
            "irregular": 1111,
            "monthly": 586,
            "daily": 573,
            "continuous": 473,
            "weekly": 449,
            "quarterly": 308,
            "semiannual": 304,
            "quinquennial": 90,
            "bimonthly": 75,
            "biweekly": 61,
            "threeTimesAYear": 56,
            "biennial": 52,
            "triennial": 44,
            "hourly": 23,
            "semiweekly": 13,
            "fourTimesAWeek": 10,
            "semimonthly": 9,
            "semidaily": 8,
            "threeTimesAWeek": 6,
            "threeTimesADay": 5,
            "fourTimesADay": 4,
            "threeTimesAMonth": 1,
            "null": 0
         }
         ```

    - `licence_catalogue` : Licence des données 

         ```bash
         curl -X 'GET' \
           'http://localhost:9090/catalogue/enumererValeursDistinctesChampDataGouvFr?champ=licence_catalogue' \
           -H 'accept: */*'
         ```

         http://localhost:9090/catalogue/enumererValeursDistinctesChampDataGouvFr?champ=licence_catalogue
    
         #### Retour
          
         ```json
         {
            "Licence Ouverte / Open Licence version 2.0": 32213,
            "License Not Specified": 19162,
            "Licence Ouverte / Open Licence": 6727,
            "Open Data Commons Open Database License (ODbL)": 6173,
            "Other (Attribution)": 1442,
            "Other (Open)": 154,
            "Other (Public Domain)": 133,
            "Creative Commons Attribution": 122,
            "Creative Commons Attribution Share-Alike": 103,
            "Open Data Commons Attribution License": 37,
            "Creative Commons CCZero": 34,
            "Open Data Commons Public Domain Dedication and Licence (PDDL)": 21,
            "null": 0
         }    
         ```
    
    - `tags_catalogue` : les tags (étiquettes) de description de catalogue
    
        ```bash
        curl -X 'GET' \
          'http://localhost:9090/catalogue/enumererValeursDistinctesChampDataGouvFr?champ=tags_catalogue' \
          -H 'accept: */*'
        ```
    
      http://localhost:9090/catalogue/enumererValeursDistinctesChampDataGouvFr?champ=tags_catalogue
    
      #### Retour

      Extrait des premières valeurs retournées :
    
        ```json
        {
            "donnees-ouvertes": 6497,
            "passerelle-inspire": 5182,
            "geoscientific-information": 1474,
            "environnement": 1378,
            "geospatial": 1176,
            "grand-public": 1048,
            "planning-cadastre": 991,
            "urbanisme": 990,
            "risque-zonages-risque-naturel": 924,
            "administration": 866,
            "citoyennete": 865,
            "finances-publiques": 811,
            "culture": 798,
            "usage-des-sols": 787,
            "mobilite": 725,
            "transport": 683,
            "open-data": 680,
            "transports": 663,
            "environment": 619,
            "amenagement-du-territoire": 597,
            "logement": 596,
            "territoire": 576,
            "bretagne": 576,
            "budget": 575,
            "equipements": 551,
            "tourisme": 513,
            "education": 509,
            "patrimoine": 508,
            "zones-a-risque-naturel": 504
        }
        ```
    
    - `granularite_spatiale_catalogue` :
    
        ```bash
        curl -X 'GET' \
          'http://localhost:9090/catalogue/enumererValeursDistinctesChampDataGouvFr?champ=granularite_spatiale_catalogue' \
          -H 'accept: */*'
        ```
    
        http://localhost:9090/catalogue/enumererValeursDistinctesChampDataGouvFr?champ=granularite_spatiale_catalogue
    
        #### Retour
    
        ```json
        {
            "other": 24331,
            "fr:commune": 2395,
            "fr:epci": 1298,
            "country": 1278,
            "fr:departement": 800,
            "poi": 573,
            "fr:region": 448,
            "fr:canton": 71,
            "fr:collectivite": 49,
            "country-group": 48,
            "fr:iris": 38,
            "country-subset": 23,
            "fr:arrondissement": 19,
            "null": 0
         }
         ```

    - `zones_spatiales_catalogue` :
    
        ```bash
        curl -X 'GET' \
          'http://localhost:9090/catalogue/enumererValeursDistinctesChampDataGouvFr?champ=zones_spatiales_catalogue' \
          -H 'accept: */*'
        ```
    
        http://localhost:9090/catalogue/enumererValeursDistinctesChampDataGouvFr?champ=zones_spatiales_catalogue
    
        #### Retour
    
        Extrait des premières valeurs retournées :
    
        ```json
        {
            "France": 3138,
            "Métropole de LYON": 529,
            "CU DE BREST": 329,
            "Metropolitan France": 195,
            "Antibes": 157,
            "World": 104,
            "Calvados": 71,
            "Toulouse": 63,
            "Saint-Malo": 57,
            "Saint-Paul-lès-Dax": 48,
            "European Union": 46,
            "Grand-Est": 43,
            "Blois": 41,
            "Bar-le-Duc": 38,
            "CA Saint-Louis Agglomération": 36,
            "Bordeaux": 34,
            "CA Lisieux Normandie": 34,
            "Sixt-sur-Aff": 33,
            "DROM-COM": 32
        }  
        ```
