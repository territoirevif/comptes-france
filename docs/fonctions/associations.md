Les associations du Répertoire National des Associations (RNA, Waldec)
======================================================================

- Liste des associations

|numero_waldec|ancien_numero_waldec|siret         |numero_rup |gestion|nature|groupement|titre                                                                                                                                      |titre_court                           |objet                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |code_objet_social1|code_objet_social2|adresse_siege_complement                                                   |adresse_siege_numero_voie|obsolete_adresse_siege_repetition|adresse_siege_type_voie|adresse_siege_libelle_voie|adresse_siege_distribution     |adresse_siege_code_postal|nom_declarant                        |adresse_gestion_complement_association|adresse_gestion_complement_geo        |adresse_gestion_libelle_voie          |adresse_gestion_distribution_facturation|adresse_gestion_code_postal|adresse_gestion_achemine     |adresse_gestion_pays|civilite_declarant|site_web                                         |autorisation_publication_web|observation                                                                                                 |position_activite|maj_time           |libelle_objet_social1                                                                                                                                     |libelle_objet_social2                                                                                                                                     |codeCommune|nomCommune              |codeRegion|codeEPCI |nomEPCI                 |populationTotale|date_creation|date_derniere_declaration|date_publication_JO_creation|date_dissolution|nombreAnneesExistence|nombreAnneesDerniereDeclaration|codeDepartement|
|-------------|--------------------|--------------|-----------|-------|------|----------|-------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------|------------------|---------------------------------------------------------------------------|-------------------------|---------------------------------|-----------------------|--------------------------|-------------------------------|-------------------------|-------------------------------------|--------------------------------------|--------------------------------------|--------------------------------------|----------------------------------------|---------------------------|-----------------------------|--------------------|------------------|-------------------------------------------------|----------------------------|------------------------------------------------------------------------------------------------------------|-----------------|-------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|------------------------|----------|---------|------------------------|----------------|-------------|-------------------------|----------------------------|----------------|---------------------|-------------------------------|---------------|
|W024005157   |0024006729          |NULL          |NULL       |751P   |D     |S         |INSTITUT EUROPEEN KAROLUS - CENTRE DE RECHERCHE SUR L'HISTOIRE DES FAMILLES                                                                |INSTITUT EUROPEEN KAROLUS             |constituer, gérer et mettre à la disposition de ses membres et du public une documentation sur l'histoire des familles et ses sources d'information, aussi bien en France qu'en Europe et dans le monde, documentation la plus complète possible sur les études généalogiques et historiques, les recherches sur l'histoire des familles et des lieux, les sciences de l'histoire, l'héraldique, la sigillographie, l'onomastique et toutes autres activités susceptibles d'aider au développement de la recherche généalogique \n le tout contribuant à la participation de la sauvegarde et à la protection du patrimoine des archives \n                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |006000            |010005            |NULL                                                                       |77                       |NULL                             |RUE                    |de Reuilly                |_                              |75012                    |NULL                                 |NULL                                  |NULL                                  |77 RUE DE REUILLY                     |NULL                                    |75012                      |PARIS                        |FRANCE              |SF                |NULL                                             |0                           |Reprise  =>  date ag=0000-00-00                                                                             |A                |2022-06-21 12:45:39|CULTURE, PRATIQUES D'ACTIVITÉS ARTISTIQUES, PRATIQUES CULTURELLES                                                                                         |collections d'objets, de documents, bibliothèques spécialisées pour la sauvegarde et l'entretien du patrimoine                                            |75112      |Paris 12e Arrondissement|11        |200054781|Métropole du Grand Paris|2149216         |1996-03-18   |2022-06-09               |1996-06-05                  |1901-01-01      |28.34246575342466    |2.0986301369863014             |75             |
|W032002543   |0032004850          |79068466600015|NULL       |751P   |D     |S         |INTERNATIONAL WOMEN IN PHOTO                                                                                                               |INTERNATIONAL WOMEN IN PHOTO          |promouvoir des femmes photographes de toutes origines, nationalités, religions âges et orientations sexuelles \n uvrer pour la parité,l'égalité et la diversité par le biais de l'image dans le monde \n mettra en place des actions et des projets pour atteindre les objectifs décrits, seule ou en partenariat avec d'autres entités , à titre d'exemple \n augmenter la visibilité et la diversité des femmes photographes dans le secteur de la photographie en France et à l'international \n proposer des actions autour de l'éducation et de la formation en photographie pour les femmes et les étudiantes \n                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |006025            |000000            |3ème Droite                                                                |42                       |NULL                             |RUE                    |de Tocqueville            |NULL                           |75017                    |NULL                                 |NULL                                  |NULL                                  |7 RUE DU PRINTEMPS                    |NULL                                    |75017                      |PARIS                        |FRANCE              |CF                |NULL                                             |0                           |NULL                                                                                                        |A                |2021-07-20 20:00:04|photographie, cinéma (dont ciné-clubs)                                                                                                                    |NULL                                                                                                                                                      |75117      |Paris 17e Arrondissement|11        |200054781|Métropole du Grand Paris|2149216         |2001-04-04   |2021-01-18               |1901-01-01                  |1901-01-01      |23.293150684931508   |3.4876712328767123             |75             |

_quelques associations_

- Liste des codes objets sociaux des associations

|code_objet_social|libelle_objet_social                                      |
|-----------------|----------------------------------------------------------|
|001000           |ACTIVITÉS POLITIQUES                                      |
|001005           |associations à caractère politique général                |
|001010           |soutien, financement de partis et de campagnes électorales|
|001015           |action politique locale                                   |
|001020           |action politique globale                                  |
|001025           |activités citoyennes européennes                          |

- Pour commodité, accès possible par des thèmes sociaux qui regroupent des codes objets sociaux :    

  - __Retour à l'emploi__ (Lutte contre l'illettrisme, Aide à l'insertion des jeunes, Groupements de chômeurs - aide aux chômeurs, Réinsertion des délinquants, Soutien - reclassement des détenus, Aide à l'emploi, développement local, promotion de solidarités économiques - vie locale, Entreprises d'insertion - associations intermédiaires, régies de quartier, Aide à la création d'activités économiques individuelles, Promotion d'initiatives de développement durable)


  - __Professionnels__ (Organisation de professions (hors caractère syndical), Comité - défense d'un emploi, Groupement d'employeurs)


  - __Formations__ (Éducation formation, Organisation de professions enseignantes - amicales de personnel, Associations périscolaires - coopération - aide à l'enseignement, Œuvres sociales en faveur des élèves - œuvres en faveur pupilles de la nation, Organisme de gestion d'établissement d'enseignement général et technique, Organisme de gestion d'établissement d'enseignement supérieur, Établissement de formation professionnelle - formation continue, Centre d'enseignement et de formation associations d’étudiants - d’élèves, Études et formations linguistiques, Apprentissage)


  - __Aspects sociaux liés à l'emploi__ (Associations philanthropiques, Amicales laïques, Médiation - prévention, Amicales - groupements affinitaires - groupements d'entraide (hors défense de droits fondamentaux), Groupements d'entraide et de solidarité, Prévention et lutte contre l'alcoolisme - la toxicomanie, Interventions sociales, Aide et conseils aux familles, Associations familiales - services sociaux pour les familles, Lutte contre le surendettement, Aide aux personnes en danger - solitude - désespoir - soutien psychologique et moral, Associations caritatives - humanitaires - aide au développement - développement du bénévolat, Secours financiers et autres services aux personnes en difficulté, Secours en nature - distribution de nourriture et de vêtements, Associations caritatives à buts multiples, Associations caritatives intervenant au plan international, Crèches - garderies - haltes garderies, Activités civiques - information civique, Comités de défense et d'animation de quartier - association locale ou municipale, Logement, Aide au logement, Associations et comités de locataires - de propriétaires - comités de logement)


  - __Soutien publics particuliers__ (Aide aux réfugiés et aux immigrés (hors droits fondamentaux), Défense des droits des femmes - condition féminine, Défense des droits des personnes homosexuelles, Défense des droits des personnes en situation de handicap, Association pour la défense de droits de minorités, Défense des droits des personnes rapatriées, Défense des droits des personnes étrangères ou immigrées - de personnes réfugiées, Associations féminines pour l'entraide et la solidarité (hors défense de droits fondamentaux), Associations de personnes homosexuelles pour l'entraide et la solidarité (hors défense de droits fondamentaux), Associations de personnes en situation de handicap pour l'entraide et la solidarité (hors défense de droits fondamentaux), Associations de classe d'âge, Lutte contre les discriminations, Aide sociale aux personnes en situation de handicap, Établissements - services pour personnes handicapées (y compris c.a.t), Établissements et services pour adultes en difficulté - chrs (centres d'hébergement et de réadaptation sociale), Aide aux accidentés du travail)


  - __Développement touristique__ (Clubs de loisirs - relations - Activités festives (soirées...), Centres de loisirs - clubs de loisirs multiples, Loisirs pour personnes en situation de handicap, Centres aérés - colonies de vacances, Mouvements éducatifs de jeunesse et d'éducation populaire, Sports - activités de plein air, Associations multisports locales, Handisport, Natation - baignade (natation - plongée), Associations pour la promotion du sport - médailles - mérite sportif, Activités de plein air (dont saut à l'élastique), Représentation d'intérêts régionaux et locaux, Actions de sensibilisation et d'éducation à l'environnement et au développement durable, Comités de défense et d'animation de quartier - association locale ou municipale, Promotion d'initiatives de développement durable, Tourisme, Gîtes ruraux - camping - caravaning - naturisme, Syndicats d'initiative - offices de tourisme - salons du tourisme, Sauvetage - secourisme - protection civile)

- Certaines fonctions peuvent adjoindre aux associations qui se réfèrent à un siret des détails sur l'établissement qu'elles mentionnent. 

# Jeux de données

## Le répertoire national des associations

Le [Répertoire National des Associations (RNA)](https://www.data.gouv.fr/fr/datasets/repertoire-national-des-associations/) est remis par une multitude de fichiers csv présents dans un fichier `rna_waldec_[date].zip`
- horodatés
- et classés par département.

Il faut les fusionner en un seul :

   ```bash
   # Remplacez la date du fichier rna_waldec_20240701_dpt_01.csv par celle du fichier présent dans l'archive que vous avez téléchargée 
   awk 'FNR>1' *.csv > rna_waldec.cs
   awk 'FNR<2' rna_waldec_20240701_dpt_01.csv > header.cs
   cat header.cs rna_waldec.cs >rna_waldec.csv
   rm header.cs 
   rm rna_waldec.cs
   ```

- Le fichier RNA est un fichier CSV __multiligne__ : veillez à mettre ce paramètre dans votre outil de lecture de ce CSV, si vous l'employez directement.

## Les codes d'objets sociaux

- Les codes d'objets sociaux sont téléchargeables sur
  [https://www.data.gouv.fr/fr/datasets/repertoire-national-des-associations-nomenclature-waldec/](https://www.data.gouv.fr/fr/datasets/repertoire-national-des-associations-nomenclature-waldec/)


## Les thèmes parents des codes d'objets sociaux

- Les thèmes parents des codes objets sociaux  
  [https://data.orleans-metropole.fr/explore/dataset/repertoire-national-des-associations-themes-parents-nomenclature-waldec/information/?disjunctive.objet_social_parent_id](https://data.orleans-metropole.fr/explore/dataset/repertoire-national-des-associations-themes-parents-nomenclature-waldec/information/?disjunctive.objet_social_parent_id)


# Exemples d'utilisation

## Obtenir la liste des associations dans une commune, sous forme d'objets Association

### Requête de lancement

#### Paramètres

- Année de référence du Code Officiel des Communes
- Année de référence du Registre National des Associations
- Code de la commune

#### Exemple : les associations de Douarnenez (code commune : 29046) inscrites au RNA en 2023

```bash
curl -X 'GET' \
'http://localhost:9090/associations/obtenirAssociations?anneeRNA=2023&anneeCOG=2023&codeCommune=29046' \
-H 'accept: */*'
```

[http://localhost:9090/associations/obtenirAssociations?anneeRNA=2023&anneeCOG=2023&codeCommune=29046](http://localhost:9090/associations/obtenirAssociations?anneeRNA=2023&anneeCOG=2023&codeCommune=29046)

#### Retour

```json
[
  {
    "numeroWaldec": {
      "numero": "W031000941"
    },
    "ancienNumeroWaldec": {
      "numeroWaldec": "0031005181"
    },
    "siret": null,
    "numeroRUP": null,
    "gestion": "294P",
    "nature": "D",
    "groupement": "SIMPLE",
    "titre": "STUDIOCHIC",
    "titreCourt": "STUDIOCHIC",
    "objet": "le développement d'actions culturelles et de projets liés à l'écologie",
    "codeObjetSocial1": "006000",
    "codeObjetSocial2": "000000",
    "adresseSiege": {
      "annulationLogique": false,
      "nom": "STUDIOCHIC",
      "complementNom": null,
      "complementVoie": null,
      "numeroVoie": "80",
      "typeVoie": "RUE",
      "repetitionNumeroDansVoie": null,
      "voie": "Préfet Collignon",
      "codePostal": "29100",
      "codeCommune": null,
      "bureauDistributeur": "_",
      "cedex": null,
      "libelleCedex": "29046",
      "ville": "Douarnenez",
      "codePaysEtranger": null,
      "nomPaysEtranger": null,
      "commentaire": null
    },
    "nomDeclarant": null,
    "adresseGestion": {
      "annulationLogique": false,
      "nom": null,
      "complementNom": "DOUARNENEZ",
      "complementVoie": null,
      "numeroVoie": null,
      "typeVoie": null,
      "repetitionNumeroDansVoie": null,
      "voie": "80  RUE PRéFET COLLIGNON",
      "codePostal": "29100",
      "codeCommune": null,
      "bureauDistributeur": null,
      "cedex": null,
      "libelleCedex": null,
      "ville": null,
      "codePaysEtranger": null,
      "nomPaysEtranger": "FRANCE",
      "commentaire": null
    },
    "civiliteDeclarant": "PM",
    "siteWeb": null,
    "autorisationPublicationWeb": null,
    "observation": null,
    "positionActivite": "A",
    "majTime": 1364203502,
    "libelleObjetSocial1": null,
    "libelleObjetSocial2": null,
    "codeCommune": {
      "id": "29046"
    },
    "nomCommune": "Douarnenez",
    "codeRegion": {
      "id": "53"
    },
    "codeDepartement": {
      "id": "29"
    },
    "codeEPCI": {
      "id": "242900645"
    },
    "nomEPCI": "CC Douarnenez Communauté",
    "populationTotale": 14379,
    "dateCreation": [
      2006,
      1,
      3
    ],
    "dateDerniereDeclaration": [
      2013,
      3,
      14
    ],
    "datePublicationJOcreation": [
      2006,
      1,
      28
    ],
    "dateDissolution": [
      1901,
      1,
      1
    ],
    "nombreAnneesExistence": 17.50958904109589,
    "nombreAnneesDerniereDeclaration": 10.312328767123288
  }
]
```

## Exportation en fichier CSV des associations ayant un thème social particulier

Elle ajoute aux associations sélectionnées l'entreprise (établissement) si elles en mentionnent un.


### Requête de lancement

#### Paramètres

- Thème social
- Année de référence du Code Officiel des Communes
- Année de référence du Registre National des Associations

#### Exemple : exporter un CSV des associations touristiques en France, en 2023

```bash
curl -X 'GET' \
'http://localhost:9090/associations/exporterAssociationsThemeSocial?themeObjetSocial=DEVELOPPEMENT_TOURISTIQUE&anneeRNA=2023&anneeCOG=2023' \
-H 'accept: */*'
```

[http://localhost:9090/associations/exporterAssociationsThemeSocial?themeObjetSocial=DEVELOPPEMENT_TOURISTIQUE&anneeRNA=2023&anneeCOG=2020](http://localhost:9090/associations/exporterAssociationsThemeSocial?themeObjetSocial=DEVELOPPEMENT_TOURISTIQUE&anneeRNA=2020&anneeCOG=2023)

#### En retour

- Le chemin d'accès vers le fichier CSV produit

```csv
Région,Nom de l'intercommunalité,Département,Nom de la commune,Population communale,Nom de l'association,Objet social de l'association,Site web,Catégorie d'objet social n°1,Catégorie d'objet social n°2,Nombre d'années d'existence,Catégorie Juridique,Libellé,Activité (APE),Dénomination entreprise,Dénomination usuelle 1,Dénomination usuelle 2,Dénomination usuelle 3,Signe,Enseigne 1,Enseigne 2,Enseigne 3,Dénomination établissement,Domaine Economie Sociale et Solidaire,Siret,Nom du déclarant,Civilité déclarant (gestion),Date de création,Nombre d'années depuis la dernière déclaration,Date de dernière déclaration,Active,Date de dissolution,Code de la commune,Complément de voie (siège),Numéro de voie (siège),Type de voie (siège),Libellé de voie (siège),Distribution (siège),Code postal (siège),Complément association (gestion),Complément géographique (gestion),Libellé de voie (gestion),Distribution facturation (gestion),Code postal (gestion),Acheminement (gestion),Pays (gestion),Complément adresse (établissement),Numéro de voie (établissement),Répétition voie (établissement),Type de voie (établissement),Libellé de voie (établissement),Code postal (établissement),Distribution (établissement),Cedex (établissement),Libellé cedex (établissement),Code de l'intercommunalité (code EPCI),Numéro WALDEC/RNA,Diffusable
01,CA CAP Excellence,971,Abymes,53443,'ALYZE FORCE 7',"developper,organiser,diriger des activites liees a la communica- tion,a l'expression et a la promotion artistique,culturelle et sportive",,,,34.78630136986301,,,,,,,,,,,,,,,,PM,1988-09-28,34.78630136986301,1988-09-28,A,1901-01-01,97101,,,,,_,97139,,,,,97139,LES ABYMES,FRANCE,,,,,,,,,,200018653,W9G2011920,
01,CA CAP Excellence,971,Abymes,53443,'LES RACINES' ASSOCIATION DES RETRAITES DU CREDIT AGRICOLE DE LA GUADELOUPE,rapprocher ses membres et le personnel en service pour mettre en commun des activités culturelles de loisirs et de solidarité,,,,34.18630136986302,,,,,,,,,,,,,,,,PM,1989-05-05,11.216438356164383,2012-04-18,A,1901-01-01,97101,,,,Crédit Agricole de Petit Pérou,_,97139,,,Crédit Agricole de Petit Pérou,,97139,LES ABYMES,FRANCE,,,,,,,,,,200018653,W9G2000459,
01,CA CAP Excellence,971,Abymes,53443,'MANGO-POM'M',"pratique de diverses distractions:sports,danse, theatre, travail manuel",,,,34.78630136986301,,,,,,,,,,,,,,,,PM,1988-09-28,34.78630136986301,1988-09-28,A,1901-01-01,97101,,,,,_,97139,,,,,97139,LES ABYMES,FRANCE,,,,,,,,,,200018653,W9G2011919,
01,CA CAP Excellence,971,Abymes,53443,'STARFIELD',la pratique  de l'éducation physique et des sports et en particulier du Baseball et/ou du cricket et rounders,,,,24.704109589041096,Association déclarée,Les Abymes,Activités de clubs de sports,'STARFIELD',,,,,,,,,true,,,SF,1998-10-26,1.1534246575342466,2022-05-09,A,1901-01-01,97101,,,,Terrain Deloumeaux,_BAZIN,97139,,3 RICHEPLAINE,12 LOT BALTAZIA,,97180,STE ANNE,FRANCE,,,,,TERRAIN DELOUMEAUX,97139,,,,200018653,W9G2002640,O
```
