Les équipements des communes
============================

- les équipements présents dans les communes sont proposés en dataset :

|zonageAireAttraction|annee|CodeBassinDeVie|codeCommune|DOM|codeEPCI |codeIRIS |x        |y         |QP      |QUALI_IRIS|QUALI_QP|QUALI_QVA|QUALI_ZFU|QUALI_ZUS|qualiteGeolocalisation|QVA|codeRegion|SDOM|typeEquipement|codeUniteUrbaine|ZFU|ZUS|libelleTypeEquipement                              |codeDepartement|
|--------------------|-----|---------------|-----------|---|---------|---------|---------|----------|--------|----------|--------|---------|---------|---------|----------------------|---|----------|----|--------------|----------------|---|---|---------------------------------------------------|---------------|
|108                 |2021 |59122          |59001      |A  |200068500|590010000|715188.15|7015264.31|CSZ     |X         |X       |X        |X        |X        |4                     |CSZ|32        |A1  |A129          |CSZ             |CSZ|CSZ|MAIRIE                                             |59             |
|108                 |2021 |59122          |59001      |A  |200068500|590010000|715189.9 |7015250   |CSZ     |X         |X       |X        |X        |X        |4                     |CSZ|32        |A2  |A208          |CSZ             |CSZ|CSZ|AGENCE POSTALE                                     |59             |
|108                 |2021 |59122          |59001      |A  |200068500|590010000|714936.03|7014740.85|CSZ     |X         |X       |X        |X        |X        |4                     |CSZ|32        |A4  |A403          |CSZ             |CSZ|CSZ|MENUISIER CHARPENTIER SERRURIER                    |59             |

_quelques équipements de la commune d'Abancourt (Nord) : Mairie, agence postale et menuisier-charpentier-serrurier_

et en base spatiale :

![Un Camping de Falgoux (Cantal)](equipement_camping_g103_falgoux.png)
_un camping de Falgoux (Cantal)_

## Types d'équipements

|codeTypeEquipement|libelleTypeEquipement                                                        |
|------------------|-----------------------------------------------------------------------------|
|A101              |POLICE                                                                       |
|A104              |GENDARMERIE                                                                  |
|A105              |COUR D’APPEL (CA)                                                            |
|A108              |CONSEIL DE PRUD’HOMMES (CPH)                                                 |
|A109              |TRIBUNAL DE COMMERCE (TCO)                                                   |
|A120              |DRFIP (DIRECTION RÉGIONALE  DES FINANCES PUBLIQUES)                          |
|A121              |DDFIP (DIRECTION DÉPARTEMENTALE DES FINANCES PUBLIQUES)                      |
|A122              |RÉSEAU DE PROXIMITÉ PÔLE EMPLOI                                              |
|A124              |MAISON DE JUSTICE ET DU DROIT                                                |
|A125              |ANTENNE DE JUSTICE                                                           |
|A126              |CONSEIL DÉPARTEMENTAL D’ACCÈS AU DROIT (CDAD)                                |
|A128              |IMPLANTATIONS FRANCE SERVICES (IFS)                                          |
|A129              |MAIRIE                                                                       |

_quelques types d'équipements_

## Modalités

Les différentes modalités du dataset (`QVA`, `ZFU`, `DOM`, etc.) sont décodées :  

|codeTypeModalite|libelleTypeModalite                                                       |codeModalite|libelleModalite                        |typeChamp|
|----------------|--------------------------------------------------------------------------|------------|---------------------------------------|---------|
|AAV2020         |Zonage en aire d'attraction des villes 2020 d'implantation de l'équipement|001         |Paris                                  |CHAR     |
|AAV2020         |Zonage en aire d'attraction des villes 2020 d'implantation de l'équipement|002         |Lyon                                   |CHAR     |
|BV2012          |Bassin de vie 2012 d'implantation de l'équipement                         |01004       |Ambérieu-en-Bugey                      |CHAR     |
|BV2012          |Bassin de vie 2012 d'implantation de l'équipement                         |01033       |Valserhône                             |CHAR     |

_quelques modalités_

# Sources de données

## 2023

### Dessin de fichier et modalités

- `dessin_fichier_bpe.csv` est constitué à partir du contenu des colonnes `Variable`, `Libellé`, `Type`, `Longueur` du fichier `BPE23_anonymisee_dessin_fichier.html`

- `donnees_ref_externe.txt` liste les champs qui ont une référence externe ou une donnée non énumérable exhaustivement.


## 2021 et antérieures

* [Base équipement géolocalisée INSEE](https://www.insee.fr/fr/statistiques/3568638?sommaire=3568656) :


(Attention: elle n'est pas archivée sur le site de l'INSEE : seule l'année courante (souvent deux années avant celle où nous sommes) s'y trouve).

	- Le fichier `varmod_bpe18_ensemble_xy.csv` est renommé `varmod_ensemble_xy.csv`.

# Exemples d'utilisation

- Recenser les équipements des communes, et les écrire en base de données : ils seront ensuite visualisables sous un Système d'Information Géographique.

> [http://localhost:9090/equipement/recenserEquipements?anneeCOG=2019&anneeBaseEquipement=2018](http://localhost:9090/equipement/recenserEquipements?anneeCOG=2019&anneeBaseEquipement=2018)




