Revenus et imposition
=====================

- Les impots sur le revenu dans les communes

| tranche_fiscale  |nombre_foyers_fiscaux|revenu_fiscal_reference|impot_net_total|nombre_foyers_fiscaux_imposes|revenu_fiscal_reference_foyers_fiscaux_imposes|traitement_salaires_nombre_foyers|traitements_salaires_montant|retraites_pensions_nombre_foyers|retraites_pensions_montant|typeCommune|codeCommune|arrondissement|typeNomEtCharniere|nomMajuscules|nomCommune|libelle|codeCanton|codeCommuneParente|codeRegion|sirenCommune|populationTotale|populationMunicipale|populationCompteApart|strateCommune|longitude        |latitude           |surface|sirenCommuneMembre|codeEPCI |nomEPCI           |codeCommuneChefLieuDepartement|typeNomEtCharniereDepartement|nomMajusculesDepartement|nomDepartement|libelleDepartement|montant_fiscal_par_foyer|montant_foyer_impose|montant_salaire_par_foyer|montant_retraite_par_foyer|frequence_imposition_menages|codeDepartement|
|------------------|---------------------|-----------------------|---------------|-----------------------------|----------------------------------------------|---------------------------------|----------------------------|--------------------------------|--------------------------|-----------|-----------|--------------|------------------|-------------|----------|-------|----------|------------------|----------|------------|----------------|--------------------|---------------------|-------------|-----------------|-------------------|-------|------------------|---------|------------------|------------------------------|-----------------------------|------------------------|--------------|------------------|------------------------|--------------------|-------------------------|--------------------------|----------------------------|---------------|
| de 100 000       |50                   |7553.0                 |988.0          |NULL                         |NULL                                          |44                               |4491.0                      |NULL                            |NULL                      |COM        |97616      |NULL          |0                 |SADA         |Sada      |Sada   |97612     |NULL              |06        |200008878   |11619           |11156               |463                  |7            |45.11799159356275|-12.860995972761165|1110.0 |200008878         |200059871|CC du Centre-Ouest|97608                         |0                            |MAYOTTE                 |Mayotte       |Mayotte           |151060                  |NULL                |102068                   |NULL                      |NULL                        |976            |
| 0 à 10 000       |1318                 |2499.0                 |-25.0          |NULL                         |NULL                                          |411                              |2759.0                      |108                             |776.0                     |COM        |97616      |NULL          |0                 |SADA         |Sada      |Sada   |97612     |NULL              |06        |200008878   |11619           |11156               |463                  |7            |45.11799159356275|-12.860995972761165|1110.0 |200008878         |200059871|CC du Centre-Ouest|97608                         |0                            |MAYOTTE                 |Mayotte       |Mayotte           |1896                    |NULL                |6712                     |7185                      |NULL                        |976            |
| 10 001 à 12 000  |82                   |899.0                  |-2.0           |NULL                         |NULL                                          |72                               |899.0                       |NULL                            |NULL                      |COM        |97616      |NULL          |0                 |SADA         |Sada      |Sada   |97612     |NULL              |06        |200008878   |11619           |11156               |463                  |7            |45.11799159356275|-12.860995972761165|1110.0 |200008878         |200059871|CC du Centre-Ouest|97608                         |0                            |MAYOTTE                 |Mayotte       |Mayotte           |10963                   |NULL                |12486                    |NULL                      |NULL                        |976            |
| 12 001 à 15 000  |192                  |2584.0                 |-14.0          |NULL                         |NULL                                          |179                              |2835.0                      |13                              |180.0                     |COM        |97616      |NULL          |0                 |SADA         |Sada      |Sada   |97612     |NULL              |06        |200008878   |11619           |11156               |463                  |7            |45.11799159356275|-12.860995972761165|1110.0 |200008878         |200059871|CC du Centre-Ouest|97608                         |0                            |MAYOTTE                 |Mayotte       |Mayotte           |13458                   |NULL                |15837                    |13846                     |NULL                        |976            |
| 15 001 à 20 000  |267                  |4640.0                 |-20.0          |NULL                         |NULL                                          |240                              |5115.0                      |19                              |324.0                     |COM        |97616      |NULL          |0                 |SADA         |Sada      |Sada   |97612     |NULL              |06        |200008878   |11619           |11156               |463                  |7            |45.11799159356275|-12.860995972761165|1110.0 |200008878         |200059871|CC du Centre-Ouest|97608                         |0                            |MAYOTTE                 |Mayotte       |Mayotte           |17378                   |NULL                |21312                    |17052                     |NULL                        |976            |
| 20 001 à 30 000  |552                  |13932.0                |78.0           |216                          |5535.0                                        |524                              |15457.0                     |31                              |651.0                     |COM        |97616      |NULL          |0                 |SADA         |Sada      |Sada   |97612     |NULL              |06        |200008878   |11619           |11156               |463                  |7            |45.11799159356275|-12.860995972761165|1110.0 |200008878         |200059871|CC du Centre-Ouest|97608                         |0                            |MAYOTTE                 |Mayotte       |Mayotte           |25239                   |25625               |29498                    |21000                     |0.391304347826087           |976            |
| 30 001 à 50 000  |708                  |26634.0                |523.0          |343                          |13005.0                                       |688                              |29101.0                     |35                              |671.0                     |COM        |97616      |NULL          |0                 |SADA         |Sada      |Sada   |97612     |NULL              |06        |200008878   |11619           |11156               |463                  |7            |45.11799159356275|-12.860995972761165|1110.0 |200008878         |200059871|CC du Centre-Ouest|97608                         |0                            |MAYOTTE                 |Mayotte       |Mayotte           |37618                   |37915               |42297                    |19171                     |0.4844632768361582          |976            |
| 50 001 à 100 000 |346                  |23290.0                |701.0          |228                          |15924.0                                       |339                              |24751.0                     |21                              |374.0                     |COM        |97616      |NULL          |0                 |SADA         |Sada      |Sada   |97612     |NULL              |06        |200008878   |11619           |11156               |463                  |7            |45.11799159356275|-12.860995972761165|1110.0 |200008878         |200059871|CC du Centre-Ouest|97608                         |0                            |MAYOTTE                 |Mayotte       |Mayotte           |67312                   |69842               |73011                    |17809                     |0.6589595375722543          |976            |
| Total            |3515                 |82031.0                |2230.0         |830                          |40762.0                                       |2497                             |85409.0                     |246                             |3337.0                    |COM        |97616      |NULL          |0                 |SADA         |Sada      |Sada   |97612     |NULL              |06        |200008878   |11619           |11156               |463                  |7            |45.11799159356275|-12.860995972761165|1110.0 |200008878         |200059871|CC du Centre-Ouest|97608                         |0                            |MAYOTTE                 |Mayotte       |Mayotte           |23337                   |49110               |34204                    |13565                     |0.2361308677098151          |976            |

_Les impôts sur le revenu des 3 515 foyers présents à Sada (Mayotte) en 2022_


- Par commune, calcul de quelques taux sur les tranches fiscales totalisatrices 

|nomCommune                  |nomDepartement|populationMunicipale|nombre_foyers_fiscaux|montant_fiscal_par_foyer|nombre_foyers_fiscaux_imposes|frequence_imposition_menages|montant_foyer_impose|montant_salaire_par_foyer|montant_retraite_par_foyer|traitement_salaires_nombre_foyers|retraites_pensions_nombre_foyers|
|----------------------------|--------------|--------------------|---------------------|------------------------|-----------------------------|----------------------------|--------------------|-------------------------|--------------------------|---------------------------------|--------------------------------|
|Abancourt                   |Nord          |469                 |240                  |35562                   |141                          |0.5875                      |46801               |35307                    |27343                     |153                              |96                              |
|Abscon                      |Nord          |4309                |2406                 |21597                   |766                          |0.3183707398171239          |37599               |26600                    |20600                     |1518                             |825                             |
|Aibes                       |Nord          |372                 |190                  |28936                   |85                           |0.4473684210526316          |44258               |32252                    |23714                     |107                              |63                              |
|Aix-en-Pévèle               |Nord          |1320                |655                  |46224                   |411                          |0.6274809160305344          |60717               |45136                    |28750                     |476                              |200                             |

_Dans quelques communes du Nord en 2022_

# Source des données

Fournisseur : Ministère de l'Économie, des Finances et de la Souveraineté industrielle et numérique

Voici les liens de téléchargement des données brutes traitées par l'application, et leur lieu de stockage :

## Impôts sur les revenus

* [L'impôt sur le revenu par collectivité territoriale (IRCOM)](https://www.data.gouv.fr/fr/datasets/l-impot-sur-le-revenu-par-collectivite-territoriale/)  
  pour l'année _n_ (déclarés en _n - 1)_

  1. Dézipper le fichier `ircom-2023-revenus-2022.zip`. Il se décomprime en :

     ```
     [departemental]
     [national]
     [regional]
     ircom_communes_complet_revenus_2022.xlsx
     notice_explicative_ircom_revenus_2022.pdf
     ```
    
  2. Ouvrir sous _Librecalc_ ou _Excel_ le fichier `ircom_communes_complet_revenus_2022.xlsx`  
     (les instructions sont données pour _Librecalc_)

     ![Le fichier .xlsx IRCOM reçu](traitement_source_donnees_ircom_01.png)
   
     _Le fichier IRCOM reçu_

     - Supprimer les premières lignes avant le tableau et remplacer les deux lignes (ici 6 et 7) de titre  
       par une unique ligne de titre de colonnes, dont les noms sont:  
       `code_departement,INSEE,nom_commune,tranche_fiscale,nombre_foyers_fiscaux,revenu_fiscal_reference,impot_net_total,nombre_foyers_fiscaux_imposes,revenu_fiscal_reference_foyers_fiscaux_imposes,traitement_salaires_nombre_foyers,traitements_salaires_montant,retraites_pensions_nombre_foyers,retraites_pensions_montant`

     - Supprimer également les lignes pied de page en bas du tableau

     - Déclarer que les colonnes `nombre_foyers_fiscaux,revenu_fiscal_reference,impot_net_total,nombre_foyers_fiscaux_imposes,revenu_fiscal_reference_foyers_fiscaux_imposes,traitement_salaires_nombre_foyers,traitements_salaires_montant,retraites_pensions_nombre_foyers,retraites_pensions_montant` sont de type `Nombre` en choissant un format de nombre sans séparateur de milliers  
       et que celles  `code_departement,INSEE,nom_commune,tranche_fiscale` sont de type `Texte`

     ![Le fichier .xlsx IRCOM modifié](traitement_source_donnees_ircom_02.png)
  
     _L'apparence du fichier une fois adapté sous la feuille de calcul_
     
  3. Sélectionner _Enregistrer Sous_ en demandant une sauvegarde de type _Texte CSV_  
     En choisissant pour nom de sauvegarde et destination `$REVENUS_IMPOSITION_DIR/comptes-france/revenus/[annee]/impots_revenus.csv`  
     c'est à dire, par défaut : `/data/comptes-france/revenus/2022/impots_revenus.csv`  
     et lorsque celui-ci a été choisi, cochez la case _Editer les paramètres du filtre_ et appuyez sur le bouton _Enregistrer_.  
     Un formulaire apparaît, cochez comme présenté, puis sauvegardez.

     ![Les paramètres de filtre de sauvergarde](traitement_source_donnees_ircom_03.png)

  4. Le fichier `impots_revenus.csv` produit doit avoir cet aspect :

     ```bash
     head impots_revenus.csv -n 4
     "code_departement","INSEE","nom_commune","tranche_fiscale","nombre_foyers_fiscaux","revenu_fiscal_reference","impot_net_total","nombre_foyers_fiscaux_imposes","revenu_fiscal_reference_foyers_fiscaux_imposes","traitement_salaires_nombre_foyers","traitements_salaires_montant","retraites_pensions_nombre_foyers","retraites_pensions_montant"
     "010","001","L'Abergement-Clémenciat","Total",466,16446,801,247,11691,316,11711,176,4915
     "010","002","L'Abergement-de-Varey","Total",143,4975,247,80,3620,101,3498,40,1269
     "010","004","Ambérieu-en-Bugey","0 à 10 000",1846,7216,-80,14,92,792,5261,492,4432     
     ```

     Il est alors prêt à être pris en charge par l'application.
