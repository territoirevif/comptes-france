# Liste tous les tests failsafe échoués
find . -name '*IT.txt' -type f -exec grep -LF "Failures: 0, Errors: 0" {} \; | grep -vF 'MIT.txt' | xargs cat

