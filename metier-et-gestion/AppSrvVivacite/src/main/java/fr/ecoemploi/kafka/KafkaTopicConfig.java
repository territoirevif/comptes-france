package fr.ecoemploi.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.kafka.config.TopicBuilder;

/**
 * Configuration des topics Kafka
 * @author Marc Le Bihan
 */
@Configuration
public class KafkaTopicConfig {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(KafkaTopicConfig.class);

   /** Nom du topic vie */
   @Value("ecoemploi-statut")
   private String topicVie;

   /**
    * Création du topic destiné au suivi de l'initialisation et de la vivacité des composants de l'application.
    * @return Topic vie.
    */
   @Bean
   public NewTopic topicVie() {
      LOGGER.info("Configuration du topic Kafka {} (Bean prêt).", this.topicVie);
      return TopicBuilder.name(this.topicVie).build();
   }
}
