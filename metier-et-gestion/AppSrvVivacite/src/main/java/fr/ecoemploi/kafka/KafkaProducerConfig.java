package fr.ecoemploi.kafka;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.kafka.core.*;

import java.util.*;

/**
 * Configuration des producteurs Kafka.
 * @author Marc Le Bihan
 */
@Configuration
public class KafkaProducerConfig {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerConfig.class);

   /** Les serveurs Kafka. */
   @Value("${spring.kafka.bootstrap-servers}")
   private String bootstrapServers;

   /**
    * Configuration du producteur Kafka
    * @return Sérialiseur/Désérialiseur par défaut : String
    */
   @Bean
   public Map<String, Object> producerConfigs() {
      Map<String, Object> props = new HashMap<>();
      props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, this.bootstrapServers);
      props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
      props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

      LOGGER.info("Configuration des producteurs Kafka (Bean prêt).");
      return props;
   }

   /**
    * Factory du producteur Kafka.
    * @return Factory.
    */
   @Bean
   public ProducerFactory<String, String> producerFactory() {
      return new DefaultKafkaProducerFactory<>(producerConfigs());
   }

   /**
    * Template de configuration du producteur Kafka.
    * @return Kafka Template
    */
   @Bean
   public KafkaTemplate<String, String> kafkaTemplate() {
      return new KafkaTemplate<>(producerFactory());
   }
}
