package fr.ecoemploi.kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.kafka.config.*;
import org.springframework.kafka.core.*;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;

import java.util.*;

/**
 * Configuration des consommateurs Kafka.
 */
@Configuration
public class KafkaConsumerConfig {
   /** Les serveurs Kafka */
   @Value("${spring.kafka.bootstrap-servers}")
   private String bootstrapServers;

   /**
    * Configuration du consomateur Kafka
    * @return Désérialiseur par défaut : String
    */
   @Bean
   public Map<String, Object> consumerConfigs() {
      Map<String, Object> props = new HashMap<>();
      props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, this.bootstrapServers);
      props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
      return props;
   }

   /**
    * Factory du consommateur Kafka.
    * @return Factory.
    */
   @Bean
   public ConsumerFactory<String, String> consumerFactory() {
      return new DefaultKafkaConsumerFactory<>(consumerConfigs());
   }

   /**
    * Template de configuration du consommateur Kafka.
    * @return Kafka Template
    */
   @Bean
   public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> kafkaListenerContainerFactory() {
      ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
      factory.setConsumerFactory(consumerFactory());
      return factory;
   }
}
