/**
 * Accès à Kafka pour l'initialisation des applications, et le suivi des travaux.
 * @author Marc Le Bihan
 */
package fr.ecoemploi.kafka;