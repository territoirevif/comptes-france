package fr.ecoemploi.service.vivacite;

/**
 * Phase d'installation/configuration/démarrage/arrêt d'un composant de l'application.
 * @author Marc Le Bihan
 */
public enum VivacitePhase {
   /** Initialisation du moteur de suivi */
   INIT("init"),

   /** Phase de démarrage. */
   START("start"),

   /** Configuration */
   CONFIG("config"),

   /** Phase d'arrêt */
   STOP("stop");

   /** Mot-clef indicatif. */
   private final String motClef;

   /**
    * Construire une phase de suivi.
    * @param motClef Mot-clef.
    */
   VivacitePhase(String motClef) {
      this.motClef = motClef;
   }

   /**
    * Renvoyer le mot-clef associé.
    * @return Mot-clef.
    */
   public String getMotClef() {
      return this.motClef;
   }
}
