package fr.ecoemploi.service.vivacite;

import java.io.*;
import java.text.MessageFormat;
import java.util.Objects;

/**
 * Vivacité de l'application
 */
public class Vivacite implements Serializable {
   /** Serial ID */
   @Serial
   private static final long serialVersionUID = -8607170347957904553L;

   /** Composant observé. */
   private String composant;

   /**  Phase d'initialisation, démarrage, fonctionnement, arrêt... */
   private String phase;

   /** Statut dans la phase en question, ou en général. */
   private String status;

   /** Description de son état. */
   private String description;

   /** Message additionnel. */
   private String message;

   /**
    * Construire une information de vivacité.
    */
   public Vivacite() {
   }

   /**
    * Construire une information de vivacité.
    * @param composant Composant observé
    * @param phase Phase d'initialisation, démarrage, arrêt, fonctionnement... dans laquelle se trouve le composant.
    * @param status Statut du composant, pour cette phase en particulier, ou en général.
    * @param description Description de l'étape ou la situation dans laquelle se trouve le composant.
    * @param message Message additionnel qui précise le statut.
    */
   public Vivacite(VivaciteComposant composant, VivacitePhase phase, VivaciteStatut status, String description, String message) {
      Objects.requireNonNull(composant, "Le composant ne peut pas valoir null");
      Objects.requireNonNull(phase, "La phase ne peut pas valoir null");
      Objects.requireNonNull(status, "Le statut ne peut pas valoir null");

      this.composant = composant.getMotClef();
      this.phase = phase.getMotClef();
      this.status = status.getMotClef();
      this.description = description;
      this.message = message;
   }

   /**
    * Construire une information de vivacité.
    * @param composant Composant observé
    * @param phase Phase d'initialisation, démarrage, arrêt, fonctionnement... dans laquelle se trouve le composant.
    * @param status Statut du composant, pour cette phase en particulier, ou en général.
    * @param description Description de l'étape ou la situation dans laquelle se trouve le composant.
    * @param message Message additionnel qui précise le statut.
    */
   public Vivacite(String composant, String phase, String status, String description, String message) {
      this.composant = composant;
      this.phase = phase;
      this.status = status;
      this.description = description;
      this.message = message;
   }

   /**
    * Renvoyer le composant observé.
    * @return Composant.
    */
   public String getComposant() {
      return this.composant;
   }

   /**
    * Fixer le composant observé.
    * @param composant Composant.
    */
   public void setComposant(String composant) {
      this.composant = composant;
   }

   /**
    * Renvoyer la phase d'initialisation, démarrage, arrêt, fonctionnement... dans laquelle se trouve le composant.
    * @return Phase du composant.
    */
   public String getPhase() {
      return this.phase;
   }

   /**
    * Fixer la phase d'initialisation, démarrage, arrêt, fonctionnement... dans laquelle se trouve le composant.
    * @param phase Phase du composant.
    */
   public void setPhase(String phase) {
      this.phase = phase;
   }

   /**
    * Renvoyer le statut du composant, pour cette phase en particulier, ou en général.
    * @return statut du composant.
    */
   public String getStatus() {
      return this.status;
   }

   /**
    * Fixer le statut du composant, pour cette phase en particulier, ou en général.
    * @param status statut du composant.
    */
   public void setStatus(String status) {
      this.status = status;
   }

   /**
    * Renvoyer la description de l'étape ou la situation dans laquelle se trouve le composant.
    * @return Description.
    */
   public String getDescription() {
      return this.description;
   }

   /**
    * Fixer la description de l'étape ou la situation dans laquelle se trouve le composant.
    * @param description  Description.
    */
   public void setDescription(String description) {
      this.description = description;
   }

   /**
    * Renvoyer le message additionnel qui précise le statut.
    * @return Message additionnel.
    */
   public String getMessage() {
      return this.message;
   }

   /**
    * Fixer le message additionnel qui précise le statut.
    * @param message Message additionnel.
    */
   public void setMessage(String message) {
      this.message = message;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      String format = "composant : {0}, phase : {1}, statut : {2}, description : {3}, message : {4}";
      return MessageFormat.format(format, this.composant, this.phase, this.status, this.description, this.message);
   }
}
