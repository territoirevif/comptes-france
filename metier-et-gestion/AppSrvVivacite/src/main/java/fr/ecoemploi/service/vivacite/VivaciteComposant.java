package fr.ecoemploi.service.vivacite;

/**
 * Composant de l'application impliqué dans une notification de vivacité.
 * @author Marc Le Bihan
 */
public enum VivaciteComposant {
   /** L'application principale eco-emploi */
   APPLICATION("application"),

   /** Imprécis : au tout début de l'inialisation, typiquement. */
   GENERAL("general"),

   /** Geoserver. */
   GEOSERVER("geoserver"),

   /** SGBD Postgis. */
   SGBD("sgbd"),

   /** Elastic / Logstash / Kibana */
   ELK("elk");

   /** Mot-clef indicatif. */
   private final String motClef;

   /**
    * Construire une phase de suivi.
    * @param motClef Mot-clef.
    */
   VivaciteComposant(String motClef) {
      this.motClef = motClef;
   }

   /**
    * Renvoyer le mot-clef associé.
    * @return Mot-clef.
    */
   public String getMotClef() {
      return this.motClef;
   }
}
