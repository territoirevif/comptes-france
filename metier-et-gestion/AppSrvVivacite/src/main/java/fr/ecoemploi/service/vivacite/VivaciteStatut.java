package fr.ecoemploi.service.vivacite;

/**
 * Statut d'une phase installation/configuration/démarrage/arrêt d'un composant de l'application.
 * @author Marc Le Bihan
 */
public enum VivaciteStatut {
   /** Démarrage. */
   START("start"),

   /** Démarrage en cours. */
   STARTING("starting"),

   /** Vérification en cours. */
   VERIFICATION("check"),

   /** Arrêt en cours */
   ENDING("ending"),

   /** A échoué. */
   FAILED("fail"),

   /** Réussi. */
   SUCCESS("success"),

   /** Arrêt */
   STOP("stop");

   /** Mot-clef indicatif. */
   private final String motClef;

   /**
    * Construire une phase de suivi.
    * @param motClef Mot-clef.
    */
   VivaciteStatut(String motClef) {
      this.motClef = motClef;
   }

   /**
    * Renvoyer le mot-clef associé.
    * @return Mot-clef.
    */
   public String getMotClef() {
      return this.motClef;
   }
}
