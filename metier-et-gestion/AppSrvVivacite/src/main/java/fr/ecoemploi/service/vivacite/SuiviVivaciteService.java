package fr.ecoemploi.service.vivacite;

import java.util.Objects;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import static fr.ecoemploi.domain.utils.console.ConsoleEscapeCodes.*;
import fr.ecoemploi.domain.utils.objets.PersistenceException;

/**
 * Service d'observation de la vivacité des composants de l'application.
 * @author Marc Le Bihan
 */
@Service
public class SuiviVivaciteService {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(SuiviVivaciteService.class);

   /** Template Kafka. */
   @Autowired
   private KafkaTemplate<String, String> kafkaTemplate;

   /** Topic de suivi de statut des composants. */
   private static final String TOPIC_STATUT = "ecoemploi-statut";

   /**
    * Emettre un message pour annoncer que nous sommes avons démarré.
    */
   public void sendHello() {
      Vivacite hello = new Vivacite("application", "start", "started", "Ecoemploi, backend métier Java/Spark (Spring-Boot)", "Son serveur Spring-Boot est prêt");
      send(hello);

      LOGGER.info(BGREEN.color("L'Application principale a émis le message, sur Kafka, qu'elle est maintenant prête."));
   }

   /**
    * Envoyer un message de vivacité
    * @param vivacite Message de vivacité
    */
   public void send(Vivacite vivacite) {
      Objects.requireNonNull(vivacite, "Le message de vivacité ou d'état à envoyer ne peut pas valoir null");

      String notificationJson = toJson(vivacite);
      this.kafkaTemplate.send(TOPIC_STATUT, notificationJson);
   }

   /**
    * Convertir en json une notification à destination de Kafka.
    * @param vivacite Objet à convertir en JSON.
    * @return Conversion en json.
    */
   private String toJson(Vivacite vivacite) {
      ObjectMapper objectMapper = new ObjectMapper();

      try {
         return objectMapper.writeValueAsString(vivacite);
      }
      catch(JsonProcessingException e) {
         throw new PersistenceException(e.getMessage(), e);
      }
   }
}
