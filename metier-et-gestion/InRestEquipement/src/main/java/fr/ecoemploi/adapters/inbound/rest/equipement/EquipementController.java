package fr.ecoemploi.adapters.inbound.rest.equipement;

import java.io.Serial;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.*;
import io.swagger.v3.oas.annotations.tags.Tag;

import fr.ecoemploi.adapters.inbound.port.equipement.EquipementApi;
import fr.ecoemploi.adapters.inbound.rest.core.AbstractRestController;
import fr.ecoemploi.application.port.equipement.EquipementPort;

/**
 * Contrôleur REST pour la gestion des équipements.
 * @author Marc LE BIHAN
 */
@RestController
@RequestMapping(value = "/equipements", name="equipements")
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:4200", "http://localhost:9091"})
@Tag(name = "Equipement", description = "Gestion des équipements présents sur le territoire")
public class EquipementController extends AbstractRestController implements EquipementApi {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 5688363440484082609L;

   /** Dataset des équipements. */
   @Autowired
   private EquipementPort equipementPort;

   /**
    * Charger la base équipement et les mettre en cache.
    * @param anneeEquipement Année de la base équipement à charger.<br>
    * 0 si toutes les années connues doivent l'être (à partir de 2018 pour la base équipement).
    */
   @Operation(description = "Charger la  dans les fichiers de cache")
   @GetMapping(value = "/chargerEquipements")
   @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Bases équipements chargées en cache."),
        @ApiResponse(responseCode = "403", description = "Si la génération de données par l'application est interdite."),
        @ApiResponse(responseCode = "500", description = "Un incident durant le chargement.")
      }
   )   
   public void chargerEquipements(
      @Parameter(name = "anneeEquipement", description = "Année de la base équipement : 0 pour toutes les années connues.", examples = {
         @ExampleObject(value = "2023", name="précise", summary = "une année de base équipement précise"),
         @ExampleObject(value = "0", name="relative", summary = "Toutes les années de la base équipement disponibles")
      })
      @RequestParam(name="anneeEquipement") int anneeEquipement) {
      refusSiGenerationInterdite();
      this.equipementPort.chargerEquipements(anneeEquipement);
   }
}
