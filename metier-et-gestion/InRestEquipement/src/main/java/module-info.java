/**
 * Inbound Rest de la base équipement
 */
module fr.ecoemploi.inbound.rest.equipement {
   // requires jakarta.ws.rs;
   requires java.ws.rs;
   requires spring.beans;
   requires spring.web;
   requires spring.context;
   requires io.swagger.v3.oas.annotations;

   requires fr.ecoemploi.domain.model;
   requires fr.ecoemploi.inbound.rest.core;

   requires fr.ecoemploi.application.port.equipement;
   requires fr.ecoemploi.inbound.port.equipement;

   exports fr.ecoemploi.adapters.inbound.rest.equipement;
}
