/**
 * Inbound Port des Comptes
 */
module fr.ecoemploi.inbound.port.compte {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.adapters.inbound.port.compte;
}
