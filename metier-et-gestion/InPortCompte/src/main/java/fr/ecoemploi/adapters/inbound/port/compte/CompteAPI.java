package fr.ecoemploi.adapters.inbound.port.compte;

import java.io.Serializable;

import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.comptabilite.ComptesIndividuelsCommune;

/**
 * API de la balance des comptes et des comptes individuels.
 */
public interface CompteAPI extends Serializable {
   /**
    * Charger les comptes des communes et des intercommunalités.
    * @param anneeCOG Année de référence du Code Officiel Géographique à charger.<br>
    * 0 si toutes les années connues doivent être chargées (à partir de 2016).
    */
   void chargerReferentiels(int anneeCOG);

   /**
    * Obtenir les comptes individuels d'une commune.
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param anneeExercice Année.
    * @param codeCommune Code commune.
    * @return Comptes individuels de la commune.
    */
   ComptesIndividuelsCommune obtenirComptesIndividuels(int anneeCOG, int anneeExercice, CodeCommune codeCommune);
}
