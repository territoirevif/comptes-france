<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>fr.comptes.france</groupId>
	<artifactId>parent</artifactId>
	<version>0.0.13-SNAPSHOT</version>
	<packaging>pom</packaging>

	<name>POM Parent</name>
	<description>Definitions générales du parent.</description>

	<properties>
		<!-- Plugins Maven -->
		<maven-compiler.memory>-Xms2048M -Xmx8192M</maven-compiler.memory>
		<openapi-generator-maven-plugin.version>7.10.0</openapi-generator-maven-plugin.version>

		<!-- Sonar et couverture de tests -->
		<jacoco.version>0.8.10</jacoco.version>
		<sonar.java.coveragePlugin>jacoco</sonar.java.coveragePlugin>
		<sonar.dynamicAnalysis>reuseReports</sonar.dynamicAnalysis>
		<sonar.coverage.jacoco.xmlReportPaths>${project.basedir}/../target/jacoco.exec</sonar.coverage.jacoco.xmlReportPaths>
		<sonar.language>java</sonar.language>

		<!-- Propriétés de construction -->
		<java.version>17</java.version>
		<maven.compiler.release>${java.version}</maven.compiler.release>

		<spring-boot.version>2.7.14</spring-boot.version>
		<surefire-version>3.1.2</surefire-version>

		<maven-wagon-ssh.version>3.5.2</maven-wagon-ssh.version>

		<maven.test.compile.encoding>UTF-8</maven.test.compile.encoding>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<downloadSources>true</downloadSources>
		<downloadJavadocs>true</downloadJavadocs>
	</properties>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.7.14</version>
		<!-- <version>3.1.5</version> -->
		<relativePath />
	</parent>

	<!-- SCM Git -->
	<scm>
		<developerConnection>scm:git:https://gitlab.com/territoirevif/comptes-france</developerConnection>
		<tag>ecoemploi-0.0.9</tag>
	</scm>

	<!-- Déploiement -->
	<distributionManagement>
		<!-- Dépôt local (en l'absence d'un Nexus/Artifactory) -->
		<repository>
			<id>integration</id>
			<name>Dépôt local</name>
			<url>file:///data/artifacts/comptes-france/comptes-france.integration</url>
		</repository>
	</distributionManagement>

	<dependencies>
		<!-- Requis le temps du passage à Spring boot 2.x -->
		<dependency>
			<groupId>org.javassist</groupId>
			<artifactId>javassist</artifactId>
			<version>3.23.1-GA</version>
		</dependency>

		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>

			<exclusions>
				<exclusion>
					<groupId>org.apache.logging.log4j</groupId>
					<artifactId>log4j-to-slf4j</artifactId>
				</exclusion>

				<exclusion>
					<groupId>log4j</groupId>
					<artifactId>log4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>

			<exclusions>
				<exclusion>
					<groupId>junit</groupId>
					<artifactId>junit</artifactId>
				</exclusion>

				<exclusion>
					<groupId>org.apache.logging.log4j</groupId>
					<artifactId>log4j-to-slf4j</artifactId>
				</exclusion>

				<exclusion>
					<groupId>log4j</groupId>
					<artifactId>log4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- Kafka 3.x pour Spring -->
		<dependency>
			<groupId>org.springframework.kafka</groupId>
			<artifactId>spring-kafka</artifactId>
		</dependency>

		<!-- Kafka 3.x pour Spring, test -->
		<dependency>
			<groupId>org.springframework.kafka</groupId>
			<artifactId>spring-kafka-test</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- JUnit 5 -->
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-api</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-engine</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-suite</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-starter-parent</artifactId>
				<version>${spring-boot.version}</version>

				<exclusions>
					<exclusion>
						<groupId>org.apache.logging.log4j</groupId>
						<artifactId>log4j-to-slf4j</artifactId>
					</exclusion>

					<exclusion>
						<groupId>ch.qos.logback</groupId>
						<artifactId>logback-classic</artifactId>
					</exclusion>

					<exclusion>
						<groupId>log4j</groupId>
						<artifactId>log4j</artifactId>
					</exclusion>
				</exclusions>
			</dependency>

			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-starter</artifactId>
				<version>${spring-boot.version}</version>

				<exclusions>
					<exclusion>
						<groupId>org.apache.logging.log4j</groupId>
						<artifactId>log4j-to-slf4j</artifactId>
					</exclusion>

					<exclusion>
						<groupId>ch.qos.logback</groupId>
						<artifactId>logback-classic</artifactId>
					</exclusion>

					<exclusion>
						<groupId>log4j</groupId>
						<artifactId>log4j</artifactId>
					</exclusion>
				</exclusions>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<profiles>
		<!-- Construction normale -->
		<profile>
			<id>default</id>

			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>

			<properties>
				<!-- Tests unitaires activés par défaut, et tests d'intégration désactivés -->
				<skip.integration.tests>true</skip.integration.tests>
				<skip.unit.tests>false</skip.unit.tests>

				<argLine>
					--add-exports java.base/sun.nio.ch=ALL-UNNAMED --add-opens java.base/java.util=ALL-UNNAMED --add-opens java.base/java.io=ALL-UNNAMED --add-opens java.base/java.nio=ALL-UNNAMED --add-opens java.base/java.lang=ALL-UNNAMED --add-opens java.base/java.lang.invoke=ALL-UNNAMED --add-opens java.base/sun.security.util=ALL-UNNAMED --add-opens java.base/sun.security.action=ALL-UNNAMED
				</argLine>
			</properties>
		</profile>

		<!-- Construction avec tests d'assemblage -->
		<profile>
			<id>assemblage</id>

			<properties>
				<!-- Tests unitaires et tests d'intégration activés en Intégration -->
				<skip.integration.tests>false</skip.integration.tests>
				<skip.unit.tests>false</skip.unit.tests>

				<argLine>
					--add-exports java.base/sun.nio.ch=ALL-UNNAMED --add-opens java.base/java.util=ALL-UNNAMED --add-opens java.base/java.io=ALL-UNNAMED --add-opens java.base/java.nio=ALL-UNNAMED --add-opens java.base/java.lang=ALL-UNNAMED --add-opens java.base/java.lang.invoke=ALL-UNNAMED --add-opens java.base/sun.security.util=ALL-UNNAMED --add-opens java.base/sun.security.action=ALL-UNNAMED
				</argLine>
			</properties>

			<build>
				<plugins>
					<!-- Plugin Failsafe : exécution de tests d'intégration -->
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-failsafe-plugin</artifactId>

						<executions>
							<execution>
								<id>integration-test</id>

								<configuration>
									<argLine>
										--add-exports java.base/sun.nio.ch=ALL-UNNAMED --add-opens java.base/java.util=ALL-UNNAMED --add-opens java.base/java.io=ALL-UNNAMED --add-opens java.base/java.nio=ALL-UNNAMED --add-opens java.base/java.lang=ALL-UNNAMED --add-opens java.base/java.lang.invoke=ALL-UNNAMED --add-opens java.base/sun.security.util=ALL-UNNAMED --add-opens java.base/sun.security.action=ALL-UNNAMED -Xmx20g
									</argLine>
								</configuration>
							</execution>
						</executions>

						<configuration>
							<skipTests>${skip.integration.tests}</skipTests>
							<trimStackTrace>false</trimStackTrace>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>

		<!-- Construction avec couverture de tests -->
		<profile>
			<id>coverage</id>

			<properties>
				<argLine>
					--add-exports java.base/sun.nio.ch=ALL-UNNAMED --add-opens java.base/java.util=ALL-UNNAMED --add-opens java.base/java.io=ALL-UNNAMED --add-opens java.base/java.nio=ALL-UNNAMED --add-opens java.base/java.lang=ALL-UNNAMED --add-opens java.base/java.lang.invoke=ALL-UNNAMED --add-opens java.base/sun.security.util=ALL-UNNAMED --add-opens java.base/sun.security.action=ALL-UNNAMED
				</argLine>
			</properties>

			<build>
				<plugins>
					<!-- Coverage de tests Jacoco et Sonar -->
					<plugin>
						<groupId>org.jacoco</groupId>
						<artifactId>jacoco-maven-plugin</artifactId>

						<executions>
							<execution>
								<id>prepare-agent</id>
								<goals>
									<goal>prepare-agent</goal>
								</goals>
							</execution>

							<execution>
								<id>report</id>
								<goals>
									<goal>report</goal>
								</goals>

								<configuration>
								</configuration>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>

		<!-- Construction avec tests d'assemblage et modules -->
		<profile>
			<id>assemblage-modules</id>

			<properties>
				<!-- Tests unitaires et tests d'intégration activés en Intégration -->
				<skip.integration.tests>false</skip.integration.tests>
				<skip.unit.tests>false</skip.unit.tests>
			</properties>
		</profile>

		<!-- Génération d'une release -->
		<profile>
			<id>release</id>

			<properties>
				<!-- Tests unitaires et tests d'intégration sont activés -->
				<skip.integration.tests>false</skip.integration.tests>
				<skip.unit.tests>false</skip.unit.tests>
				<argLine>
					--add-exports java.base/sun.nio.ch=ALL-UNNAMED --add-opens java.base/java.util=ALL-UNNAMED --add-opens java.base/java.io=ALL-UNNAMED --add-opens java.base/java.nio=ALL-UNNAMED --add-opens java.base/java.lang=ALL-UNNAMED --add-opens java.base/java.lang.invoke=ALL-UNNAMED --add-opens java.base/sun.security.util=ALL-UNNAMED --add-opens java.base/sun.security.action=ALL-UNNAMED
				</argLine>
			</properties>

			<build>
				<plugins>
					<!-- Génération de la Javadoc -->
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-javadoc-plugin</artifactId>

						<configuration>
							<docfilessubdirs>true</docfilessubdirs>
						</configuration>

						<executions>
							<execution>
								<id>attach-javadocs</id>
								<goals>
									<goal>jar</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

	<build>
		<plugins>
			<!-- Bug Eclipse temporaire -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
			</plugin>

			<!-- Compilation classique -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>

				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>

					<!--
					<compilerArgs>
						<arg>add-exports</arg>
						<arg>java.base/sun.nio.ch=ALL-UNNAMED</arg>
					</compilerArgs>
					-->
				</configuration>
			</plugin>

			<!-- Plugin Surefire : exécution tests unitaires -->
			<!-- Tous les tests sauf ceux débutant par IT* -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>${surefire-version}</version>

				<configuration>
					<skipTests>${skip.unit.tests}</skipTests>

					<excludes>
						<exclude>**/IT*.java</exclude>
					</excludes>

					<argLine>
						--add-exports java.base/sun.nio.ch=ALL-UNNAMED --add-opens java.base/java.util=ALL-UNNAMED --add-opens java.base/java.io=ALL-UNNAMED --add-opens java.base/java.nio=ALL-UNNAMED --add-opens java.base/java.lang=ALL-UNNAMED --add-opens java.base/java.lang.invoke=ALL-UNNAMED --add-opens java.base/sun.security.util=ALL-UNNAMED --add-opens java.base/sun.security.action=ALL-UNNAMED -Xmx12g
					</argLine>

					<trimStackTrace>false</trimStackTrace>
				</configuration>
			</plugin>

			<!-- Javadoc plugin -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>

				<configuration>
					<charset>UTF-8</charset>
					<docencoding>UTF-8</docencoding>
					<encoding>UTF-8</encoding>
				</configuration>
			</plugin>

			<!-- Source plugin -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
			</plugin>

			<!-- Release -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
			</plugin>

			<!-- Deploy plugin -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-deploy-plugin</artifactId>
			</plugin>
		</plugins>

		<extensions>
			<extension>
				<groupId>org.apache.maven.wagon</groupId>
				<artifactId>wagon-ssh</artifactId>
				<version>${maven-wagon-ssh.version}</version>
			</extension>
		</extensions>

		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.openapitools</groupId>
					<artifactId>openapi-generator-maven-plugin</artifactId>
					<version>${openapi-generator-maven-plugin.version}</version>
				</plugin>

				<!-- Jacoco pour la couverture de code (Sonar) -->
				<plugin>
					<groupId>org.jacoco</groupId>
					<artifactId>jacoco-maven-plugin</artifactId>
					<version>${jacoco.version}</version>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<repositories>
		<repository>
			<id>integration</id>
			<name>Integration</name>
			<url>file:///data/artifacts/comptes-france/comptes-france.integration</url>
		</repository>
	</repositories>
</project>
