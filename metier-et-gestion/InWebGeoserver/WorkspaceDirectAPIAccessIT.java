package fr.ecoemploi.adapters.inbound.rest.geoserver.configurateur;

import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.*;
import org.springframework.http.*;
import org.springframework.web.client.*;

import fr.ecoemploi.adapters.inbound.rest.geoserver.swagger.api.*;
import fr.ecoemploi.adapters.inbound.rest.geoserver.swagger.invoker.ApiClient;
import fr.ecoemploi.adapters.inbound.rest.geoserver.swagger.invoker.auth.HttpBasicAuth;
import fr.ecoemploi.adapters.inbound.rest.geoserver.swagger.model.*;

/**
 * Testing workspace REST API (Direct Access).
 * @author Marc Le Bihan
 */
@SpringBootTest(classes = GeoserverTestApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class WorkspaceDirectAPIAccessIT {
   /** Client OpenAPI. */
   private ApiClient apiClient;
   
   /** Client workspace API. */
   private WorkspacesApi workspacesApi;
   
   /** Test workspace name. */
   private static String WORKSPACE_NAME = "integration_test_workspace";
   
   /** User login. */
   private String userLogin = "admin";

   /** User login. */
   private String userPassword = "geoserver";
   
   /**
    * Remove a workspace : prepare the test suite to ensure that the workspace creation will work. 
    * It's a test by itself.
    */
   @Test
   @Order(10)
   @DisplayName("Remove a workspace (test prepare)")
   void removeWorkspaceBegin() {
      try {
         this.workspacesApi.deleteWorkspace(WORKSPACE_NAME, true);
         HttpStatus status = this.apiClient.getStatusCode();

         assertTrue(HttpStatus.OK.equals(status), "The workspace should have been removed or not found.");
      }
      catch(HttpClientErrorException e) {
         assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode(), "The workspace should have been removed or not found : another Status Code has been returned.");
      }
   }
   
   /**
    * Add a workspace.
    */
   @Test
   @Order(20)
   @DisplayName("Add a workspace")
   void addWorkspace() {
      Workspace workspace = new Workspace();
      workspace.setName(WORKSPACE_NAME);
      
      WorkspaceRequest workspaceRequest = new WorkspaceRequest();
      workspaceRequest.setWorkspace(workspace);
      this.workspacesApi.postWorkspaces(workspaceRequest, false);
      HttpStatus status = this.apiClient.getStatusCode();
      
      assertEquals(HttpStatus.CREATED, status, "The workspace wasn't created.");
   }
   
   /**
    * Détect the existence of a workspace, by a getWorkspaces() and a reseach in a list.
    */
   @Test
   @Order(30)
   @DisplayName("Searching for a workspace by asking list of workspaces")
   void checkWorkspaceByList() {
      List<WorkspaceResponse> workspaces = this.workspacesApi.getWorkspaces() // Operation
           .getWorkspaces().getWorkspace();  // Result extraction.
      
      boolean exist = workspaces.stream().anyMatch(w -> WORKSPACE_NAME.equals(w.getName()));
      assertTrue(exist, "The workspace should exist in the list of workspaces.");
   }
   
   /**
    * Detect the existence of a workspace, par a direct getWorkspace().
    */
   @Test
   @Order(40)
   @DisplayName("Searching for a workspace, directly")
   void checkWorkspace() {
      WorkspaceResponse workspace = this.workspacesApi.getWorkspace(WORKSPACE_NAME, true) // Operation
          .getWorkspace();  // Result extraction.
      
      HttpStatus status = this.apiClient.getStatusCode(); 
      
      assertEquals(HttpStatus.OK, status, "The workspace should exist (cas 2).");
      assertEquals(WORKSPACE_NAME, workspace.getName(), "The workspace should have the same name that we put earlier.");
   }
   
   /**
    * Check of absence of a not existing workspace.
    */
   @Test
   @Order(45)
   @DisplayName("Check that a non-existing workspace is notified as such")
   void checkUnexistingWorkspace() {
      try {
         this.workspacesApi.getWorkspace("i_dont_exist", true);
         fail("The workspace presence checking should lead to a failure");
      }
      catch(HttpClientErrorException e) {
         assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode(), "This status code notifying the absence of the workspace isn't the expected one.");
      }
   }
   
   /**
    * Remove the test workspace.
    */
   @Test
   @Order(50)
   @DisplayName("Remove the test workspace")
   void deleteWorkspaceEnd() {
      this.workspacesApi.deleteWorkspace(WORKSPACE_NAME, true);
      HttpStatus status = this.apiClient.getStatusCode();
      
      assertTrue(HttpStatus.OK.equals(status), "The work space should have been removed.");
   }
   
   /**
    * Préparer l'API et son initialisation sur le geoserver de test.
    */
   @BeforeEach
   public void afterPropertiesSet() {
      // Initialiser l'API Client.
      this.apiClient = new ApiClient();
      this.apiClient.setBasePath("http://localhost:8080/geoserver/rest");
      
      HttpBasicAuth basicAuthentication = (HttpBasicAuth)this.apiClient.getAuthentication("basic");
      basicAuthentication.setUsername(this.userLogin);
      basicAuthentication.setPassword(this.userPassword);
      
      // Et les autres API utilisées.
      this.workspacesApi = new WorkspacesApi(this.apiClient);
   }
}
