/**
 * Aide à la manipulation de la configuration Geoserver.
 * @author Marc Le Bihan
 */
package fr.ecoemploi.adapters.inbound.rest.geoserver.configurateur;