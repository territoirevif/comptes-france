package fr.ecoemploi.adapters.inbound.rest.geoserver.configurateur;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.TechniqueException;

/**
 * Espace de travail Geoserver non trouvé.
 * @author Marc Le Bihan
 */
public class WorkspaceNotFoundException extends TechniqueException {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 5822981284147086596L;

   /** Le nom de l'espace de travail qui n'a pas été trouvé. */
   private String workspaceName;

   /**
    * Construire une exception
    * @param message Message accompagnant l'exception
    */
   public WorkspaceNotFoundException(String message) {
      super(message);
   }

   /**
    * Construire une exception
    * @param message Message accompagnant l'exception
    * @param cause Cause racine de l'exception
    */
   public WorkspaceNotFoundException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Construire une exception
    * @param message Message accompagnant l'exception
    * @param workspaceName Le nom du workspace qui n'a pas été trouvé
    */
   public WorkspaceNotFoundException(String message, String workspaceName) {
      this(message);
      this.workspaceName = workspaceName;
   }

   /**
    * Construire une exception
    * @param message Message accompagnant l'exception
    * @param cause Cause racine de l'exception
    * @param workspaceName Le nom du workspace qui n'a pas été trouvé
    */
   public WorkspaceNotFoundException(String message, Throwable cause, String workspaceName) {
      this(message, cause);
      this.workspaceName = workspaceName;
   }

   /**
    * Renvoyer le nom du workspace qui n'a pas été trouvé.
    * @return Nom du workspace absent.
    */
   public String getWorkspaceName() {
      return this.workspaceName;
   }
}
