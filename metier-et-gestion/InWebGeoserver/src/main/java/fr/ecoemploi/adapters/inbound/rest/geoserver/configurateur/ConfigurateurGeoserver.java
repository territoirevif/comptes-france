package fr.ecoemploi.adapters.inbound.rest.geoserver.configurateur;

import java.text.MessageFormat;
import java.util.*;

import org.slf4j.*;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.stereotype.*;
import org.springframework.web.client.*;

import fr.ecoemploi.adapters.inbound.rest.geoserver.swagger.api.*;
import fr.ecoemploi.adapters.inbound.rest.geoserver.swagger.invoker.ApiClient;
import fr.ecoemploi.adapters.inbound.rest.geoserver.swagger.invoker.auth.HttpBasicAuth;
import fr.ecoemploi.adapters.inbound.rest.geoserver.swagger.model.*;
import fr.ecoemploi.service.vivacite.*;

/**
 * Configurateur geoserver. 
 * @author Marc Le Bihan
 */
@Component
public class ConfigurateurGeoserver implements InitializingBean {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurateurGeoserver.class);

   /** Suivi de la vivacité des composants de l'application. */
   @Autowired
   private SuiviVivaciteService vivaciteService;

   /** User login. */
   @Value("${geoserver.user}")
   protected String userLogin;

   /** User login. */
   @Value("${geoserver.password}")
   protected String userPassword;

   /** Rest URL. */
   @Value("${geoserver.rest.url}")
   protected String rest;

   /** Quiet mode */
   @Value("${geoserver.quiet:true}")
   protected boolean quiet;

   /** Datasource Postgis (host). */
   @Value("${geoserver.postgis.host}")
   private String datasourceHost;

   /** Datasource Postgis (port). */
   @Value("${geoserver.postgis.port}")
   private String datasourcePort;

   /** Datasource Postgis (Username). */
   @Value("${spring.datasource.username}")
   private String datasourceUsername;

   /** Datasource Postgis (Password). */
   @Value("${spring.datasource.password}")
   private String datasourcePassword;

   /** Nom du workspace geoserver. */
   @Value("${geoserver.workspace.name}")
   private String geoserverWorkspaceName;

   /** Nom de la datasource geoserver. */
   @Value("${geoserver.datasource.name}")
   private String geoserverDatasourceName;

   /** Description de la datasource. */
   @Value("${geoserver.datasource.description}")
   private String geoserverDatasourceDescription;

   /** Database que désigne la datasource geoserver. */
   @Value("${geoserver.datasource.database}")
   private String geoserverDatasourceDatabase;

   /** Workspace API */
   private WorkspacesApi workspacesApi;

   /** Datastore API */
   private DatastoresApi datastoresApi;

   /**
    * Créer un workspace.
    * @param workspaceName Nom du workspace.
    * @param asDefault true : le créer comme workspace par défaut.
    * @return true, s'il a été créé. false, s'il existe déjà.
    */
   public boolean createWorkspace(String workspaceName, boolean asDefault) {
      WorkspaceInfo workspaceInfo = new WorkspaceInfo().name(workspaceName);
      WorkspaceWrapper workspaceWrapper = new WorkspaceWrapper().workspace(workspaceInfo);

      try {
         ResponseEntity<Void> entity = this.workspacesApi.createWorkspaceWithHttpInfo(workspaceWrapper, asDefault);

         boolean cree = HttpStatus.CREATED.equals(entity.getStatusCode());

         if (cree) {
            LOGGER.info("Workspace {} créé", workspaceName);
         }

         return cree;
      }
      catch(HttpClientErrorException e) {
         // S'il existe déjà l'on passe silencieusement.
         if (HttpStatus.CONFLICT.equals(e.getStatusCode())) {
            LOGGER.info("Le workspace {} existe déjà", workspaceName);
            return false;
         }

         throw e;
      }
   }
   
   /**
    * Supprimer un workspace.
    * @param workspaceName Nom du workspace.
    * @param recurse true, pour supprimer aussi ses éléments.
    * @return true si le workspace a été supprimé avec succès.
    * <br>false, s'il n'existait pas.
    */
   public boolean deleteWorkspace(String workspaceName, boolean recurse) {
      try {
         ResponseEntity<Void> entity = this.workspacesApi.deleteWorkspaceWithHttpInfo(workspaceName, recurse);
         boolean supprime = HttpStatus.OK.equals(entity.getStatusCode());

         if (supprime) {
            LOGGER.info("Workspace {} supprimé : {}", workspaceName, entity);
            return true;
         }

         return false;
      }
      catch(HttpClientErrorException e) {
         if (HttpStatus.NOT_FOUND.equals(e.getStatusCode()) == false) {
            throw e;
         }
         
         if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Le workspace {} n'existe pas.", workspaceName);
         }
         
         return false;
      }
   }

   /**
    * Obtenir le détail d'un workspace.
    * @param workspaceName Nom du workspace.
    * @return détail du workspace.
    * @throws WorkspaceNotFoundException si la recherche devait réussir, mais qu'il est absent.
    */
   public WorkspaceSummary getWorkspace(String workspaceName) {
      try {
         ResponseEntity<GetWorkspaceResponse> wsr = this.workspacesApi.getWorkspaceWithHttpInfo(workspaceName, this.quiet);
         GetWorkspaceResponse response = (wsr != null) ? wsr.getBody() : null;

         if (response == null) {
            throw new RuntimeException("getWorkspaceWithHttpInfo a renvoyé une réponse nulle");
         }

         return response.getWorkspace();
      }
      catch(HttpClientErrorException e) {
         if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())) {
            String format = "Le workspace {0} n''a pas été trouvé : {1}.";
            String message = MessageFormat.format(format, workspaceName, e.getMessage());

            WorkspaceNotFoundException ex = new WorkspaceNotFoundException(message, workspaceName);
            LOGGER.warn(ex.getMessage());
            throw ex;
         }

         String format = "La recherche du workspace {0} a recontré un incident (code statut HTTP {1}) : {2}.";
         String message = MessageFormat.format(format, workspaceName, e.getStatusCode(), e.getMessage());

         RuntimeException ex = new RuntimeException(message);
         LOGGER.error(ex.getMessage(), ex);
         throw ex;
      }
   }

   /**
    * Créer un datastore.
    * @param workspaceName Nom du workspace où il doit être placé
    * @param name Nom du datastore
    * @param description Description du datastore
    * @param connectionParameters Paramètres de connexion
    * @return true si le Datastore a été créé.
    */
   public boolean createDatastore(String workspaceName, String name, String description, Map<String, String> connectionParameters) {
      Objects.requireNonNull(workspaceName, "Le nom du workspace ne peut pas valoir null");
      Objects.requireNonNull(name, "Le nom du datastore ne peut pas valoir null");
      Objects.requireNonNull(description, "La description du datastore ne peut pas valoir null");
      Objects.requireNonNull(connectionParameters, "La map porteuse des paramètres connexion ne peut pas valoir null");

      DataStoreInfo datastore = new DataStoreInfo()
         .name(name)
         .workspace(new WorkspaceInfo().name(workspaceName).isolated(false))
         .enabled(true)
         .description(description)
         .connectionParameters(connectionParameters);

      DataStoreInfoWrapper dataStoreInfoWrapper = new DataStoreInfoWrapper().dataStore(datastore);

      ResponseEntity<Void> response = this.datastoresApi.createDatastoreWithHttpInfo(workspaceName, dataStoreInfoWrapper);
      boolean cree = HttpStatus.CREATED.equals(response.getStatusCode());

      if (cree) {
         LOGGER.info("Datastore {} créé dans le workspace {}", name, workspaceName);
      }

      return cree;
   }

   /**
    * Créer un datastore vers une database PostGIS
    * @param workspaceName Nom du workspace où il doit être placé
    * @param name Nom du datastore
    * @param description Description du datastore
    * @param host Serveur où se trouve l'instance PostGIS
    * @param port Port de connexion
    * @param database Nom de la base de données
    * @param user User de connexion
    * @param password Mot de passe de connexion
    * @return true s'il a été créé, false s'il existait déjà.
    *
    * <br>
    * Dixit : <a href="https://docs.geoserver.org/stable/en/user/rest/stores.html">Geoserver Stores</a>
    * <pre>{@code
    *    <dataStore>
    *       <name>nyc</name>
    *
    *       <connectionParameters>
    *          <host>localhost</host>
    *          <port>5432</port>
    *
    *         <database>nyc</database>
    *         <user>bob</user>
    *         <passwd>postgres</passwd>
    *         <dbtype>postgis</dbtype>
    *      </connectionParameters>
    *    </dataStore>
    * </pre>}
    */
   public boolean createPostGISDatastore(String workspaceName, String name, String description,
        String host, String port, String database, String user, String password) {
      Objects.requireNonNull(workspaceName, "Le nom du workspace ne peut pas valoir null");
      Objects.requireNonNull(name, "Le nom du datastore ne peut pas valoir null");
      Objects.requireNonNull(description, "La description du datastore ne peut pas valoir null");
      Objects.requireNonNull(host, "Le serveur de connexion PostGIS ne peut pas valoir null");
      Objects.requireNonNull(port, "Le port de connexion PostGIS ne peut pas valoir null");
      Objects.requireNonNull(database, "La database de connexion PostGIS ne peut pas valoir null");
      Objects.requireNonNull(user, "Le profil utilisateur de connexion PostGIS ne peut pas valoir null");
      Objects.requireNonNull(password, "Le mot de passe de connexion PostGIS ne peut pas valoir null");

      Map<String, String> connexion = new HashMap<>();
      connexion.put("host", host);
      connexion.put("port", port);
      connexion.put("database", database);
      connexion.put("user", user);
      connexion.put("passwd", password);
      connexion.put("dbtype", "postgis");

      return createDatastore(workspaceName, name, description, connexion);
   }

   /**
    * Obtenir un datastore.
    * @param workspaceName Workspace où se trouve le datastore.
    * @param storeName Nom du datastore.
    * @return Description du Datastore.
    */
   public DataStoreResponse getDatastore(String workspaceName, String storeName) {
      try {
         ResponseEntity<DataStoreWrapper> wrapper = this.datastoresApi.getDataStoreWithHttpInfo(workspaceName, storeName, false);
         DataStoreWrapper body = wrapper.getBody();

         if (body != null) {
            return body.getDataStore();
         }

         return null;
      }
      catch (HttpClientErrorException e) {
         if (HttpStatus.NOT_FOUND.equals(e.getStatusCode()) == false) {
            throw e;
         }

         if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Le datastore {} n'existe pas dans le workspace {}.", storeName, workspaceName);
         }

         return null;
      }
   }

   /**
    * Supprimer un datastore.
    * @param workspaceName Workspace où se trouve le datastore.
    * @param storeName Nom du datastore.
    * @return true s'il a été supprimé. false, s'il n'existait pas.
    */
   public boolean deleteDatastore(String workspaceName, String storeName) {
      try {
         this.datastoresApi.deleteDatastore(workspaceName, storeName, false);
         return true;
      }
      catch (HttpClientErrorException e) {
         if (HttpStatus.NOT_FOUND.equals(e.getStatusCode()) == false) {
            throw e;
         }

         if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Le datastore {} n'existe pas dans le workspace {}.", storeName, workspaceName);
         }

         return false;
      }
   }

   /**
    * Démarrer geoserver et vérifier qu'il est correctement configuré.
    */
   public void startGeoserver() {
      String format = "L''application vérifie la présence sur Geoserver du workspace ''{0}'' et de l''entrepôt ''{1}''";
      String action = MessageFormat.format(format, this.geoserverWorkspaceName, this.geoserverDatasourceName);

      this.vivaciteService.send(new Vivacite(VivaciteComposant.GEOSERVER, VivacitePhase.CONFIG, VivaciteStatut.VERIFICATION, action, ""));

      // Si le workspace ecoemploi n'existe pas, le créer.
      try {
         getWorkspace(this.geoserverWorkspaceName);
      }
      catch (WorkspaceNotFoundException ex) {
         createWorkspace(this.geoserverWorkspaceName, true);
      }

      // Créer la datasource si elle ne l'a pas encore été.
      if (getDatastore(this.geoserverWorkspaceName, this.geoserverDatasourceName) == null) {
         createPostGISDatastore(this.geoserverWorkspaceName,
            this.geoserverDatasourceName, this.geoserverDatasourceDescription,
            this.datasourceHost, this.datasourcePort, this.geoserverDatasourceDatabase,
            this.datasourceUsername, this.datasourcePassword);
      }

      final String geoServerPret = "Geoserver est prêt à recevoir les requêtes de l'application";
      this.vivaciteService.send(new Vivacite(VivaciteComposant.GEOSERVER, VivacitePhase.CONFIG, VivaciteStatut.SUCCESS, action, geoServerPret));
      LOGGER.info(geoServerPret);
   }

   /**
    * Préparer l'API et son initialisation sur le geoserver de test.
    */
   @Override
   public void afterPropertiesSet() {
      // Initialiser l'API Client.
      ApiClient apiClient = new ApiClient();
      apiClient.setBasePath(this.rest);
      
      HttpBasicAuth basicAuthentication = (HttpBasicAuth)apiClient.getAuthentication("basicAuth");
      basicAuthentication.setUsername(this.userLogin);
      basicAuthentication.setPassword(this.userPassword);
      
      // Et les autres API utilisées.
      this.workspacesApi = new WorkspacesApi(apiClient);
      this.datastoresApi = new DatastoresApi(apiClient);
   }

   /**
    * REST Template pour le démarrage de SwaggerAPI (OpenAPI).
    * @return REST Template.
    */
   @Bean
   public RestTemplate getRestTemplate() {
      return new RestTemplate();
   }
}
