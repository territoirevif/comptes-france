package fr.ecoemploi.adapters.inbound.rest.geoserver.configurateur;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.ActiveProfiles;

import fr.ecoemploi.adapters.inbound.rest.geoserver.swagger.model.WorkspaceSummary;

/**
 * Test du configurateur Geoserver.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = GeoserverTestApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ConfigurateurIT {
   /** Configurateur Geoserver. */
   @Autowired
   private ConfigurateurGeoserver configurateur;
   
   /** Nom du workspace de test. */
   private static final String WORKSPACE_NAME = "configurateur_test_workspace";

   /** Nom du datastore de test. */
   private static final String DATASTORE_NAME = "configurateur_test_datastore";

   /** User Postgis. */
   @Value("${spring.datasource.username}")
   private String databaseUser;

   /** Password Postgis. */
   @Value("${spring.datasource.password}")
   private String databasePassword;

   /**
    * Supprimer initialement le workspace de test pour s'assurer que la suite des tests fonctionnera. 
    * C'est un test à part entière.
    */
   @Test
   @Order(10)
   @DisplayName("Supprimer un workspace (préparation du test)")
   void removeWorkspaceBegin() {
      assertDoesNotThrow(() -> this.configurateur.deleteWorkspace(WORKSPACE_NAME, true));
   }
   
   /**
    * Créer un workspace.
    */
   @Test
   @Order(20)
   @DisplayName("Créer un workspace")
   void addWorkspace() {
      assertTrue(this.configurateur.createWorkspace(WORKSPACE_NAME, false), "Le workspace n'a pas été créé.");
   }

   /**
    * Détecter l'existence directe d'un workspace, par un getWorkspace().
    */
   @Test
   @Order(40)
   @DisplayName("Recherche d'un workspace")
   void checkWorkspace() {
      WorkspaceSummary workspace = this.configurateur.getWorkspace(WORKSPACE_NAME);
      assertEquals(WORKSPACE_NAME, workspace.getName(), "Le workspace devrait avoir le même nom que celui placé auparavant.");
   }
   
   /**
    * Vérifier l'absence d'un workspace, mode levée d'exception.
    */
   @Test
   @Order(45)
   @DisplayName("Un workspace inexistant lève une exception")
   void checkUnexistingWorkspaceException() {
      assertThrows(WorkspaceNotFoundException.class, () -> this.configurateur.getWorkspace("i_dont_exist"),
         "L'absence d'un workspace n'a pas conduit à la levée d'une exception, ou de la bonne exception");
   }

   /**
    * Supprimer le workspace de test.
    */
   @Test
   @Order(50)
   @DisplayName("Supprimer un workspace (fin du test)")
   void deleteWorkspaceEnd() {
      assertTrue(this.configurateur.deleteWorkspace(WORKSPACE_NAME, true), "Le workspace aurait dû être supprimé.");
   }

   /**
    * Créer un datastore PostGIS
    */
   @Test
   @Order(60)
   @DisplayName("Créer un datastore PostGIS")
   void createPostGISDatastore() {
      this.configurateur.createWorkspace(WORKSPACE_NAME, true);

      try {
         assertDoesNotThrow(() ->
            this.configurateur.createPostGISDatastore(WORKSPACE_NAME, DATASTORE_NAME, "test : créer un datastore",
               "localhost", "5434", "comptesfrance", this.databaseUser, this.databasePassword),
            "La création du datastore PostGIS a échoué");
      }
      finally {
         this.configurateur.deleteDatastore(WORKSPACE_NAME, DATASTORE_NAME);
         this.configurateur.deleteWorkspace(WORKSPACE_NAME, true);
      }
   }
}
