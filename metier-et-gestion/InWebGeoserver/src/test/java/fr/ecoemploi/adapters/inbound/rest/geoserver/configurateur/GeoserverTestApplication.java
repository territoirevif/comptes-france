package fr.ecoemploi.adapters.inbound.rest.geoserver.configurateur;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * Application de test pour Geoserver
 * @author Marc LE BIHAN
 */
@SpringBootApplication
@ComponentScan(basePackages={"fr", "com"})
public class GeoserverTestApplication {
}
