module fr.ecoemploi.application.service.association {
   requires fr.ecoemploi.application.service.core;
   requires fr.ecoemploi.application.port.association;
   requires fr.ecoemploi.outbound.port.association;
   requires fr.ecoemploi.domain.model;

   requires spring.beans;
   requires spring.context;

   requires org.slf4j;

   exports fr.ecoemploi.application.service;
}
