package fr.ecoemploi.application.service;

import java.io.Serial;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ecoemploi.adapters.outbound.port.association.*;
import fr.ecoemploi.application.port.association.AssociationPort;
import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.association.*;

/**
 * Contrôleur REST pour la gestion des associations.
 * @author Marc LE BIHAN
 */
@Service
public class AssociationService implements AssociationPort {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -3532501071770365592L;

   /** Repository des associations présentes sur le territoire. */
   @Autowired
   private AssociationRepository associationRepository;

   /** Repository des associations présentes sur le territoire. */
   @Autowired
   private AssociationExportRepository associationExportRepository;

   /**
    * Charger le ou les référentiels des associations.
    * @param anneeRNA Année du Registre National des Associations<br>
    *     0: pour toutes les années disponibles
    * @param anneeCOG Année du Code Officiel Géographique.
    */
   public void chargerAssociations(int anneeRNA, int anneeCOG) {
      this.associationRepository.chargerAssociations(anneeRNA, anneeCOG);
   }

   /**
    * Obtenir la liste des associations d'une commune.
    * @param anneeRNA Année du Registre National des Associations
    * @param anneeCOG Année du Code Officiel Géographique.
    * @param codeCommune Code Commune.
    * @return Liste des associations.
    */
   public List<Association> obtenirAssociations(int anneeRNA, int anneeCOG, CodeCommune codeCommune) {
      return this.associationRepository.obtenirAssociations(anneeRNA, anneeCOG, codeCommune);
   }

   /**
    * Exporter les associations touristique présentes en France.
    * @param anneeRNA Année de référence des associations.
    * @param anneeCOG Année du COG à considérer.
    * @param themeObjetSocial Thème de l'objet social associatif.
    * @return Fichier d'exportation CSV des associations.
    */
   public String exporterAssociationsCSV(ThemeObjetSocial themeObjetSocial, int anneeRNA, int anneeCOG) {
      return this.associationExportRepository.exporterAssociationsCSV(themeObjetSocial, anneeRNA, anneeCOG);
   }
}
