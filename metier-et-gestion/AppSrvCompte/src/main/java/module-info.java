module fr.ecoemploi.application.service.compte {
   requires fr.ecoemploi.application.service.core;
   requires fr.ecoemploi.application.port.compte;
   requires fr.ecoemploi.outbound.port.compte;
   requires fr.ecoemploi.domain.model;

   requires spring.beans;
   requires spring.context;

   requires org.slf4j;

   exports fr.ecoemploi.application.service.compte;
}
