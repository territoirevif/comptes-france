package fr.ecoemploi.application.service.compte;

import java.io.Serial;
import java.util.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import fr.ecoemploi.adapters.outbound.port.compte.*;
import fr.ecoemploi.application.port.compte.*;
import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.*;

/**
 * Acquisition des balances de comptes.
 * @author Marc LE BIHAN
 */
@Service
public class BalanceCompteService implements BalanceComptePort, BalanceCompteIndividuelIntercommunalitePort {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 5106436284652219205L;

   /** Repository des comptes. */
   @Autowired
   private BalanceCompteRepository comptesRepository;

   /** Repository des comptes de communes. */
   @Autowired
   private BalanceCompteCommuneRepository comptesCommuneRepository;

   /** Repository des comptes individuels de communes. */
   @Autowired
   private BalanceCompteIndividuelCommuneRepository comptesIndividuelsCommuneRepository;

   /** Repository des comptes individuels de communes. */
   @Autowired
   private BalanceCompteIndividuelIntercommunaliteRepository comptesIndividuelsIntercommunaliteRepository;

   /**
    * {@inheritDoc}
    */
   @Override
   public void chargerReferentiels(int anneeCOG) {
      this.comptesRepository.chargerReferentiels(anneeCOG);
   }

   /**
    * {@inheritDoc}
    */
   public Map<SIRET, List<Compte>> obtenirBalanceComptesCommune(int anneeCOG, int anneeExercice, CodeCommune codeCommune) {
      return this.comptesCommuneRepository.obtenirBalanceComptesCommune(anneeCOG, anneeExercice, codeCommune);
   }

   /**
    * {@inheritDoc}
    */
   public ComptesIndividuelsCommune obtenirComptesIndividuels(int anneeCOG, int anneeExercice, CodeCommune codeCommune) {
      return this.comptesIndividuelsCommuneRepository.obtenirComptesIndividuels(anneeCOG, anneeExercice, codeCommune);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public List<ComptesIndividuelsCommune> obtenirComptesIndividuels(int anneeCOG, int anneeExercice, SIREN codeEPCI) {
      return this.comptesIndividuelsIntercommunaliteRepository.obtenirComptesIndividuels(anneeCOG, anneeExercice, codeEPCI);
   }
}
