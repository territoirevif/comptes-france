/**
 * Inbound Rest des revenus et imposition
 */
module fr.ecoemploi.inbound.rest.revenus {
   // requires jakarta.ws.rs;
   requires java.ws.rs;
   requires spring.beans;
   requires spring.web;
   requires spring.context;
   requires io.swagger.v3.oas.annotations;

   requires fr.ecoemploi.domain.model;
   requires fr.ecoemploi.inbound.rest.core;

   requires fr.ecoemploi.application.port.revenus;
   requires fr.ecoemploi.inbound.port.revenus;

   exports fr.ecoemploi.adapters.inbound.rest.revenus;
}
