package fr.ecoemploi.adapters.inbound.rest.revenus;

import java.io.Serial;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import fr.ecoemploi.adapters.inbound.port.revenus.RevenusApi;
import fr.ecoemploi.adapters.inbound.rest.core.AbstractRestController;
import fr.ecoemploi.application.port.revenus.RevenusImpositionPort;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.*;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * Contrôleur REST pour la gestion des entreprises.
 * @author Marc LE BIHAN
 */
@RestController
@RequestMapping(value = "/revenus-imposition", name="revenus-imposition")
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:4200", "http://localhost:9091"})
@Tag(name = "Revenus et imposition", description = "Revenus et imposition des ménages")
public class RevenusImpositionController extends AbstractRestController implements RevenusApi {
   @Serial
   private static final long serialVersionUID = 1404930361045941611L;

   /** Service de recherche des entreprises présentes sur le territoire. */
   @Autowired
   private RevenusImpositionPort revenusImpositionPort;

   /**
    * Charger le référentiel des entreprises.
    * @param anneeCOG Année du Code Officiel Géographique, pour les communes de référence
    *     Si toutes les années d'imposition sont chargées, placer dans ce champ un nombre relatif (0, -1...).
    * @param anneeImposition Année d'imposition à charger.<br>
    * 0 si toutes les années connues doivent être chargées (à partir de 2018).
    */
   @Operation(description = "Charger les référentiels de revenus et d'imposition des ménages (DGFIP) dans les fichiers de cache")
   @GetMapping(value = "/chargerRevenusEtImpositionMenages")
   @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Référentiel chargé."),
      @ApiResponse(responseCode = "403", description = "Si la génération de données par l'application est interdite."),
      @ApiResponse(responseCode = "500", description = "Un incident durant le chargement.")
   })
   @Override
   public void chargerRevenusEtImpositionMenages(
      @Parameter(name = "anneeImposition", description = "Année d'imposition et de revenus des ménages : 0 pour toutes les années connues.", examples = {
         @ExampleObject(value = "2021", name = "Une année précise", summary = "Une année d'imposition particulière"),
         @ExampleObject(value = "0", name = "Une année relative", summary = "Toutes les années d'imposition disponibles")
      })

      @RequestParam(name= "anneeImposition") int anneeImposition,

      @Parameter(name = "anneeCOG", description = "Année du code officiel géographique de référence, pour les communes.<br>" +
         "Si vous chargez toutes les années d'imposition disponibles, placez ici un nombre relatif : 0, -1... : <br>" +
         "l'année du code officiel géographique sera alors calculée d'après l'année d'imposition.",
         examples = {
            @ExampleObject(value = "2021", name="Une année précise", summary = "Une année du COG précise",
               description = "Une année du code officiel géographique précise."),

            @ExampleObject(value = "-1", name="Une année relative", summary = "Une année du COG relative à celle de l'année d'imposition choisie",
               description = "Ici pour une année d'imposition 2021, par exemple,<br>" +
                  "2021 - 1 = 2020 sera l'année du code officiel géographique associé.")
         })

      @RequestParam(name="anneeCOG") int anneeCOG) {
      refusSiGenerationInterdite();
      this.revenusImpositionPort.chargerRevenusEtImpositionMenages(anneeImposition, anneeCOG);
   }
}
