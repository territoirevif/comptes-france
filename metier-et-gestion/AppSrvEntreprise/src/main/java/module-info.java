module fr.ecoemploi.application.service.entreprise {
   requires fr.ecoemploi.application.service.core;
   requires fr.ecoemploi.application.port.entreprise;
   requires fr.ecoemploi.outbound.port.entreprise;
   requires fr.ecoemploi.domain.model;

   requires spring.beans;
   requires spring.context;

   requires org.slf4j;

   exports fr.ecoemploi.application.service.entreprise;
}
