package fr.ecoemploi.application.service.entreprise;

import java.io.Serial;
import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import fr.ecoemploi.adapters.outbound.port.entreprise.ActiviteCommuneRepository;
import fr.ecoemploi.application.port.entreprise.ActiviteCommunePort;
import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.entreprise.ActiviteCommune;

/**
 * Service de gestion des activités.
 * @author Marc LE BIHAN
 */
@Service
public class ActivitesService implements ActiviteCommunePort {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 4140939904807576994L;

   /** Repository d'activités de communes. */
   @Autowired
   private ActiviteCommuneRepository activiteCommuneRepository;

   /**
    * Obtenir les activités d'une commune.
    * @param anneeCOG Année du COG.
    * @param anneeSIRENE Année SIRENE des entreprises à consulter.
    * @param codeCommune Code commune.
    * @return Activités de la commune.
    */
   public List<ActiviteCommune> obtenirActivitesCommune(int anneeCOG, int anneeSIRENE, CodeCommune codeCommune) {
      return this.activiteCommuneRepository.obtenirActivitesCommune(anneeCOG, anneeSIRENE, codeCommune);
   }

   /**
    * Exporter les activités des communes en CSV.
    * @param anneeCOG Année du COG.
    * @param anneeSIRENE Année SIRENE de la base entreprise.
    * @param codeEPCI Code EPCI de filrage, si non null.
    * @param codeCommune Code Commune de filrage, si non null.
    */
   public void exporterActivitesCommuneCSV(int anneeCOG, int anneeSIRENE, String codeEPCI, String codeCommune) {
      this.activiteCommuneRepository.exporterActivitesCommuneCSV(anneeCOG, anneeSIRENE, codeEPCI, codeCommune);
   }
}
