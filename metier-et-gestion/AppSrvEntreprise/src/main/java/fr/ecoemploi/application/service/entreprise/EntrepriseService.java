package fr.ecoemploi.application.service.entreprise;

import java.io.Serial;
import java.util.Locale;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import fr.ecoemploi.adapters.outbound.port.entreprise.EntrepriseRepository;
import fr.ecoemploi.application.port.entreprise.EntreprisePort;
import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.entreprise.Entreprises;

/**
 * Service de gestion des entreprises.
 * @author Marc LE BIHAN
 */
@Service
public class EntrepriseService implements EntreprisePort {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -1701730393833027893L;

   /** Repository des entreprises présentes sur le territoire. */
   @Autowired
   private EntrepriseRepository entrepriseRepository;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EntrepriseService.class);

   /** Année Sirene début. */
   @Value("${annee.sirene.debut}")
   private int annneSireneDebut;

   /** Année Sirene début. */
   @Value("${annee.sirene.fin}")
   private int annneSireneFin;

   /** Source de messages. */
   @Autowired
   private transient MessageSource messageSource;

   /**
    * Charger le référentiel des entreprises
    * @param anneeCOG Année du Code Officiel Géographique, pour les communes de référence
    *     Si toutes les années sirene sont chargées, placer dans ce champ un nombre relatif (0, -1...).
    * @param anneeSirene année désirée ou 0 pour toutes les années applicables.
    */
   @Override
   public void chargerReferentiel(int anneeCOG, int anneeSirene) {
      // Si le paramètre SIRENE n'est pas valide, lever une exception.
      if (anneeSirene != 0 && anneeSirene < this.annneSireneDebut || anneeSirene > this.annneSireneFin) {
         String message = messageSource.getMessage("validation.annee.sirene.invalide", new Object[] {anneeSirene, this.annneSireneDebut, this.annneSireneFin}, Locale.getDefault());
         IllegalArgumentException ex = new IllegalArgumentException(message);
         LOGGER.warn(message);
         throw ex;
      }

      this.entrepriseRepository.chargerReferentiel(anneeCOG, anneeSirene);
   }

   /**
    * Obtenir les entreprises et établissements d'une commune.
    * @param anneeCOG Année du Code Officiel Géographique de la commune.
    * @param anneeSirene Année du SIREN des entreprises.
    * @param codeCommune Code de la commune désirée.
    * @return Entreprises et établissements de cette commune.
    */
   public Entreprises obtenirEntreprisesEtEtablissements(int anneeCOG, int anneeSirene, CodeCommune codeCommune) {
      return this.entrepriseRepository.obtenirEntreprisesEtEtablissements(anneeCOG, anneeSirene, codeCommune);
   }
}
