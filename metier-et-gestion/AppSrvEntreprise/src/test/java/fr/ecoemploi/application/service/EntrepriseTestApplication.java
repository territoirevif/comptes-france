package fr.ecoemploi.application.service;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * Application de test
 * @author Marc LE BIHAN
 */
@SpringBootApplication
@ComponentScan(basePackages={"fr.ecoemploi"})
public class EntrepriseTestApplication {
}
