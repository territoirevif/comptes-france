package fr.ecoemploi.adapters.outbound.port.catalogue;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

/**
 * Interface d'accès aux données des catalogues de jeux de données de data.gouv.fr
 * @author Marc Le Bihan
 */
@Repository
public interface CatalogueRessourcesRepository extends Serializable {
   /**
    * Charger les jeux de données.
    */
   void chargerRessourcesDatagouv();
}
