package fr.ecoemploi.adapters.outbound.port.catalogue;

import java.io.Serializable;
import java.util.*;

import org.springframework.stereotype.Repository;

import fr.ecoemploi.domain.model.catalogue.JeuDeDonneesElastic;

/**
 * Interface d'accès aux données des catalogues de jeux de données de data.gouv.fr
 * @author Marc Le Bihan
 */
@Repository
public interface CatalogueJeuxDonneesRepository extends Serializable {
   /**
    * Charger les jeux de données.
    */
   void chargerJeuxDeDonneesDatagouv();

   /**
    * Enumérer les valeurs distinctes d'un champ du catalogue datagouv.<br>
    * "frequence_catalogue", "licence_catalogue", "granularite_spatiale_catalogue", "zones_spatiales_catalogue", "tags_catalogue"<br>
    * ont des valeurs énumérées.
    * @param champ nom du champ dont ont veut l'énumération des valeurs distinctes
    * @return Map de valeurs que peut prendre ce champ et le nombre d'occurrences qu'on lui a trouvées.<br>
    *    - les valeurs sont converties en chaînes de caractères
    *    - les valeurs sont classés par nombre d'occurences décroissantes.
    */
   Map<String, Long> enumererValeursDistinctes(String champ);

   /**
    * Rechercher dans Elastic
    * @param searchText Texte à rechercher
    * @param field Champ où rechercher, spécifiquement. Peut valoir null, et alors la recherche est globale.
    * @return Liste de résultats de recherche
    */
    List<JeuDeDonneesElastic> recherche(String searchText, String field);
}
