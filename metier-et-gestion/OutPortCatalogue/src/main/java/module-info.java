/**
 * Outbound Port du catalogue datagouv.fr
 */
module fr.ecoemploi.outbound.port.catalogue {
   requires fr.ecoemploi.domain.model;
   requires spring.context;

   exports fr.ecoemploi.adapters.outbound.port.catalogue;
}
