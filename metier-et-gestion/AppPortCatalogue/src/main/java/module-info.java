/**
 * Application Port du catalogue
 */
module fr.ecoemploi.application.port.catalogue {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.application.port.catalogue;
}
