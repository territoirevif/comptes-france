package fr.ecoemploi.application.port.catalogue;

import java.io.Serializable;
import java.util.*;

import fr.ecoemploi.domain.model.catalogue.JeuDeDonneesElastic;

/**
 * Actions possibles sur les catalogues de données data.gouv.fr
 * @author Marc Le Bihan
 */
public interface CataloguePort extends Serializable {
   /**
    * Charger le catalogue datagouv.fr
    */
   void chargerCatalogueDataGouvFr();

   /**
    * Rechercher dans Elastic
    * @param searchText Texte à rechercher
    * @param field Champ où rechercher, spécifiquement. Peut valoir null, et alors la recherche est globale.
    * @return Liste de résultats de recherche
    */
   List<JeuDeDonneesElastic> recherche(String searchText, String field);

   /**
    * Enumérer les valeurs distinctes d'un champ du catalogue datagouv.<br>
    * "frequence_catalogue", "licence_catalogue", "granularite_spatiale_catalogue", "zones_spatiales_catalogue", "tags_catalogue", "score_qualite_catalogue"<br>
    * ont des valeurs énumérées.
    * @param champ nom du champ dont ont veut l'énumération des valeurs distinctes
    * @return Map de valeurs que peut prendre ce champ et le nombre d'occurrences qu'on lui a trouvées.<br>
    *    - les valeurs sont converties en chaînes de caractères
    *    - les valeurs sont classés par nombre d'occurences décroissantes.
    */
   Map<String, Long> enumererValeursDistinctes(String champ);
}
