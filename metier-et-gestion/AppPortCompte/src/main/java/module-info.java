/**
 * Application Port des comptes
 */
module fr.ecoemploi.application.port.compte {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.application.port.compte;
}
