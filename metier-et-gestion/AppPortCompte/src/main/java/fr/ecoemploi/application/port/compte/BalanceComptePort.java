package fr.ecoemploi.application.port.compte;

import java.io.Serializable;
import java.util.*;

import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.*;

/**
 * Port de la balance des comptes et des comptes individuels.
 */
public interface BalanceComptePort extends Serializable {
   /**
    * Charger les comptes des communes et des intercommunalités.
    * @param anneeCOG Année de référence du Code Officiel Géographique à charger.<br>
    * 0 si toutes les années connues doivent être chargées (à partir de 2016).
    */
   void chargerReferentiels(int anneeCOG);

   /**
    * Obtenir la balance des comptes des d'une commune (Budget Principal et annexes).
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param anneeExercice Année.
    * @param codeCommune Code commune.
    * @return Liste des comptes par SIRET.
    */
   Map<SIRET, List<Compte>> obtenirBalanceComptesCommune(int anneeCOG, int anneeExercice, CodeCommune codeCommune);

   /**
    * Obtenir les comptes individuels d'une commune.
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param anneeExercice Année.
    * @param codeCommune Code commune.
    * @return Comptes individuels de la commune.
    */
    ComptesIndividuelsCommune obtenirComptesIndividuels(int anneeCOG, int anneeExercice, CodeCommune codeCommune);
}
