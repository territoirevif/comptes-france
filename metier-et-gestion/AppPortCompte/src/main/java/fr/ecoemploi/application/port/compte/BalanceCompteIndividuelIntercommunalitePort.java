package fr.ecoemploi.application.port.compte;

import java.io.Serializable;
import java.util.List;

import fr.ecoemploi.domain.model.territoire.SIREN;
import fr.ecoemploi.domain.model.territoire.comptabilite.ComptesIndividuelsCommune;

/**
 * Port des comptes individuels d'intercommunalités.
 */
public interface BalanceCompteIndividuelIntercommunalitePort extends Serializable {
   /**
    * Obtenir les comptes individuels d'une intercommunalité.
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param anneeExercice Année.
    * @param codeEPCI Code de l'intercommunalité.
    * @return Liste des comptes.
    */
   List<ComptesIndividuelsCommune> obtenirComptesIndividuels(int anneeCOG, int anneeExercice, SIREN codeEPCI);
}
