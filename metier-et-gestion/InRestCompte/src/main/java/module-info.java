/**
 * Inbound Rest des comptes
 */
module fr.ecoemploi.inbound.rest.compte {
   // requires jakarta.ws.rs;
   requires java.ws.rs;
   requires spring.beans;
   requires spring.web;
   requires spring.context;
   requires io.swagger.v3.oas.annotations;

   requires fr.ecoemploi.domain.utils;
   requires fr.ecoemploi.domain.model;
   requires fr.ecoemploi.inbound.rest.core;

   requires fr.ecoemploi.application.port.compte;
   requires fr.ecoemploi.inbound.port.compte;
   requires fr.ecoemploi.postgis;

   exports fr.ecoemploi.adapters.inbound.rest.compte;
}
