package fr.ecoemploi.adapters.inbound.rest.compte;

import java.io.Serial;
import java.util.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.info.*;
import io.swagger.v3.oas.annotations.media.*;
import io.swagger.v3.oas.annotations.responses.*;

import fr.ecoemploi.adapters.inbound.port.compte.CompteAPI;
import fr.ecoemploi.adapters.inbound.rest.core.AbstractRestController;
import fr.ecoemploi.application.port.compte.*;

import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.*;

/**
 * Services REST des comptes des communes. 
 * @author Marc LE BIHAN
 */
@RestController
@RequestMapping(value = "/comptesCommunes", name="comptesCommunes")
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:4200", "http://localhost:9091"})
@OpenAPIDefinition(info = @Info(title = "Comptes des communes (balance et individuels)"))
public class ComptesCollectivitesController extends AbstractRestController implements CompteAPI {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -2646150458437284719L;

   /** Service d'acquisition des comptes de comptabilité. */
   @Autowired
   private BalanceComptePort balanceComptePort;

   /** Service d'acquisition des comptes individuels d'intercommunalité. */
   @Autowired
   private BalanceCompteIndividuelIntercommunalitePort balanceCompteIndividuelIntercommunalitePort;

   /**
    * Charger les comptes des communes et des intercommunalités.
    * @param anneeCOG Année de référence du Code Officiel Géographique à charger.<br>
    * 0 si toutes les années connues doivent être chargées (à partir de 2016).
    */
   @Operation(description = "Charger les référentiels territoriaux dans les fichiers de cache")
   @GetMapping(value = "/chargerReferentiel")
   @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Référentiel chargé."),
        @ApiResponse(responseCode = "403", description = "Si la génération de données par l'application est interdite."),
        @ApiResponse(responseCode = "500", description = "Un incident durant le chargement.")
      }
   )   
   public void chargerReferentiels(
      @Parameter(name = "anneeCOG", description = "Année du Code Officiel Géographique : 0 pour toutes les années connues.", example = "2022")
      @RequestParam(name="anneeCOG") int anneeCOG) {
      refusSiGenerationInterdite();
      this.balanceComptePort.chargerReferentiels(anneeCOG);
   }
   
   /**
    * Renvoyer les comptes d'une commune.
    * @param anneeCOG Année du code officiel géograhique de référence.
    * @param annee Année recherchée.
    * @param codeCommune Code de la commune.
    * @return Liste de ses comptes.
    */
   @GetMapping(value="/codeCommune")
   @Operation(description = "Retourne les comptes d'une commune")
   @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Liste des comptes de la commune obtenus, par numéro de SIRET d'établissement.", 
              content = {@Content(schema = @Schema(implementation = List.class, description = "Les comptes M14 ou M14A sont retournés pour un exercice comptable."))}),
        @ApiResponse(responseCode = "403", description = "- Si l'exécution de méthodes REST renvoyant des données volumineuses n'est pas autorisée.\n" +
           "- Si les données raffinées requises pour l'exécution de cette méthode, avec ces paramètres, n'ont pas été générées, et que l'application n'a pas le droit de les produire."),
      }
   )   
   public Map<SIRET, List<Compte>> comptesComptablesCommune(
      @Parameter(name = "anneeCOG", description = "Année du Code Officiel Géographique.", example = "2022")
      @RequestParam(name="anneeCOG") int anneeCOG,
      
      @Parameter(name = "annee", description = "Année de la balance des comptes M14 ou M14A à extraire.", example = "2022")
      @RequestParam(name="annee") int annee,

      @Parameter(name = "codeCommune", description = "Code de la commune dont on veut les comptes.", example = "29046")
      @RequestParam(name="codeCommune") String codeCommune) {
      Objects.requireNonNull(codeCommune, "Le numéro de commune ne peut pas valoir null.");
      return this.balanceComptePort.obtenirBalanceComptesCommune(anneeCOG, annee, new CodeCommune(codeCommune));
   }
   
   /**
    * Renvoyer les comptes individuels d'une commune.
    * @param anneeCOG Année du code officiel géograhique de référence.
    * @param annee Année recherchée.
    * @param codeCommune Code de la commune.
    * @return Liste de ses comptes individuels.
    */
   @GetMapping(value="/comptesIndividuelsCommune")
   @Operation(description = "Retourne les comptes individuels d'une commune")
   @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Liste des comptes individuels de la commune obtenus.", 
            content = {
               @Content(schema = @Schema(implementation = ComptesIndividuelsCommune.class, description = "Les comptes individuels sont retournés pour un exercice comptable."))
            }),
        @ApiResponse(responseCode = "403", description = "- Si l'exécution de méthodes REST renvoyant des données volumineuses n'est pas autorisée.\n" +
            "- Si les données raffinées requises pour l'exécution de cette méthode, avec ces paramètres, n'ont pas été générées, et que l'application n'a pas le droit de les produire.")
      }
   )   
   public ComptesIndividuelsCommune obtenirComptesIndividuels(
      @Parameter(name = "anneeCOG", description = "Année du Code Officiel Géographique.", example = "2019")
      @RequestParam(name="anneeCOG") int anneeCOG,
      
      @Parameter(name = "annee", description = "Année de la balance des comptes individuels à extraire.", example = "2019")
      @RequestParam(name="annee") int annee,

      @Parameter(name = "codeCommune", description = "Code de la commune dont on veut les comptes individuels.", example = "29046")
      @RequestParam(name="codeCommune") CodeCommune codeCommune) {
      Objects.requireNonNull(codeCommune, "Le numéro de commune ne peut pas valoir null.");
      return this.balanceComptePort.obtenirComptesIndividuels(anneeCOG, annee, codeCommune);
   }   

   /**
    * Renvoyer les comptes individuels d'une intercommunalité.
    * @param anneeCOG Année du code officiel géograhique de référence.
    * @param annee Année recherchée.
    * @param codeEPCI Code de l'intercommunalité.
    * @return Liste de ses comptes individuels.
    */
   @GetMapping(value="/comptesIndividuelsIntercommunalite")
   @Operation(description = "Retourne les comptes individuels d'une commune")
   @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Liste des comptes individuels de la commune obtenus.",
              content = {@Content(schema = @Schema(implementation = List.class, description = "Les comptes individuels sont retournés pour un exercice comptable."))}),
          @ApiResponse(responseCode = "403", description = "- Si l'exécution de méthodes REST renvoyant des données volumineuses n'est pas autorisée.\n" +
            "- Si les données raffinées requises pour l'exécution de cette méthode, avec ces paramètres, n'ont pas été générées, et que l'application n'a pas le droit de les produire.")
      }
   )   
   public List<ComptesIndividuelsCommune> comptesIndividuelsIntercommunalite(
      @Parameter(name = "anneeCOG", description = "Année du Code Officiel Géographique.", example = "2019")
      @RequestParam(name="anneeCOG") int anneeCOG,
      
      @Parameter(name = "annee", description = "Année de la balance des comptes individuels à extraire.", example = "2019")
      @RequestParam(name="annee") int annee,

      @Parameter(name = "codeEPCI", description = "Code de l'intercommunalité dont on veut les comptes individuels.", example = "200039808")
      @RequestParam(name="codeEPCI") String codeEPCI) {
      Objects.requireNonNull(codeEPCI, "Le code EPCI de l'intercommunalité ne peut pas valoir null.");
      return this.balanceCompteIndividuelIntercommunalitePort.obtenirComptesIndividuels(anneeCOG, annee, new SIREN(codeEPCI));
   }   
}
