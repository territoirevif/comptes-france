#!/bin/bash
#
# ************************************************************************
# * Démarrrage, en mode développement, du moteur métier de l'application *
# ************************************************************************

# Le lancement en --add-exports et --add-opens ne devrait plus être requis depuis la ligne de commande
#
# exec java --add-exports java.base/sun.nio.ch=ALL-UNNAMED \
#   --add-opens java.base/java.util=ALL-UNNAMED \
#   --add-opens java.base/java.io=ALL-UNNAMED \
#   --add-opens java.base/java.nio=ALL-UNNAMED \
#   --add-opens java.base/java.lang=ALL-UNNAMED \
#   --add-opens java.base/java.lang.invoke=ALL-UNNAMED \
#   --add-opens java.base/sun.security.util=ALL-UNNAMED \
#   --add-opens java.base/sun.security.action=ALL-UNNAMED \
#   -jar target/application-metier-et-gestion.jar

exec java -jar target/application-metier-et-gestion.jar
