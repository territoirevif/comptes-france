package fr.ecoemploi.run;

import java.util.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.*;
import org.springframework.context.annotation.*;
import org.springframework.core.env.*;

import static fr.ecoemploi.domain.utils.console.ConsoleEscapeCodes.*;
import fr.ecoemploi.adapters.inbound.rest.geoserver.configurateur.ConfigurateurGeoserver;
import fr.ecoemploi.service.vivacite.SuiviVivaciteService;

/**
 * Application de démarrage Spring-boot.
 * @author Marc LE BIHAN
 */
@SpringBootApplication
@ComponentScan(basePackages={"fr.ecoemploi"})
public class Application implements ApplicationContextAware, ApplicationRunner {
   /** LOGGER. */
   private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

   /** Suivi de la vivacité des composants de l'application. */
   @Autowired
   private SuiviVivaciteService suiviVivaciteService;

   /** Configurateur de geoserver. */
   @Autowired
   private ConfigurateurGeoserver geoserver;

   /**
    * Démarrage de l'application.
    * @param args Arguments.
    */
   public static void main(String[] args) {
      SpringApplication.run(Application.class, args);
   }

   /**
    * Traitement initial de l'application.
    * @param args arguments de la ligne de commande.
    */
   @Override
   public void run(ApplicationArguments args) {
      // Configuration de geoserver et émission d'un message, sur Kafka, que l'application est prête.
      this.geoserver.startGeoserver();
      this.suiviVivaciteService.sendHello();
   }

   /**
    * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
    */
   @Override
   public void setApplicationContext(ApplicationContext ctx) {
      Environment env = ctx.getEnvironment();
      
      LOGGER.info("\nL'application démarre avec ces profils : {}\n", Arrays.asList(env.getActiveProfiles()));
      
      if (env.getActiveProfiles().length == 0) {
         LOGGER.warn(BYELLOW.color("Vous n'avez pas précisé de profil actif. L'application va fonctionner avec des valeurs de développement très spécifiques."));
      }
   }
}
