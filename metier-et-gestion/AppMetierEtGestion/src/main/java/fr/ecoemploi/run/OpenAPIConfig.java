package fr.ecoemploi.run;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

import io.swagger.v3.oas.models.*;
import io.swagger.v3.oas.models.info.*;

/**
 * Configuration d'OpenAPI.
 * @author Marc Le Bihan
 */
@Configuration
public class OpenAPIConfig {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(OpenAPIConfig.class);

   /**
    * Configuration d'OpenAPI.
    * @param appVersion Version de l'application.
    * @return OpenAPI.
    */
   @Bean
   public OpenAPI customOpenAPI(@Value("${project.version}") String appVersion) {
       Contact contact = new Contact();
       contact.setName("Marc Le Bihan");
       
       License license = new License();
       license.setName("Apache 2.0");
       license.setUrl("http://www.apache.org/licenses/LICENSE-2.0.html");

       LOGGER.info("Configuration OpenAPI (Bean prêt).");

       return new OpenAPI()
         .components(new Components())
         .info(new Info()
            .title("API OpenData Economie-Territoire")
            .description("Services REST Economie, emploi, tourisme du territoire")
            .contact(contact)
            .license(license)
            .version(appVersion));
   }
}
