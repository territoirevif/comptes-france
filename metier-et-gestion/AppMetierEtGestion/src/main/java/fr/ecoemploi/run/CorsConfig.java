package fr.ecoemploi.run;
 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.web.servlet.config.annotation.*;

/**
 * Web Configuration.
 */
@Configuration
@EnableWebMvc
public class CorsConfig implements WebMvcConfigurer {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CorsConfig.class);

   /** Mapping pour CORS. */
   @Value("${backend.cors.mapping}")
   private String corsMapping;

   /** Origine autorisée pour CORS. */
   @Value("${backend.cors.allowedOrigins:null}")
   private String corsAllowedOrigins;

   /** Méthodes HTTP autorisées pour CORS. */
   @Value("${backend.cors.allowedMethods:null}")
   private String corsAllowedMethods;

   /** Exposed headers pour CORS. */
   @Value("${backend.cors.exposedHeaders:null}")
   private String corsExposedHeaders;

   /**
    * {@inheritDoc}
    */
   /*@Override
   public void addCorsMappings(CorsRegistry registry) {
      CorsRegistration registration = registry.addMapping(this.corsMapping);
      LOGGER.info("Les règles CORS du backend métier autorisent pour pour le mapping {} :", this.corsMapping);

      if (this.corsAllowedOrigins != null) {
         registration.allowedOrigins(this.corsAllowedOrigins);
         LOGGER.info("\t- AllowedOrigins : {}", this.corsAllowedOrigins);
      }

      if (this.corsAllowedMethods != null) {
         registration.allowedMethods(this.corsAllowedMethods);
         LOGGER.info("\t- AllowedMethods : {}", this.corsAllowedMethods);
      }

      if (this.corsExposedHeaders != null) {
         registration.exposedHeaders(this.corsExposedHeaders);
         LOGGER.info("\t- ExposedHeaders : {}", this.corsExposedHeaders);
      }
   } */
}
