# Entreprises

Problème de coût et de disponibilité du foncier à proximité de Paris

Une entreprise qui dépose un brevet, un modèle (Propriété Intellectuelle)

   - génère en moyenne 20% de CA en plus
   - versent des rémunération supérieures de 19%
   - réputation, crédibilité, visibilité, partenariats, meilleure distribution

Objectif : freiner la disparition de la sous-traitance industrielle

   - par la R&D francilienne

Fillières stratégiques :

   - automobile
   - santé
   - bio thérapie
   - bio production

Objectif : identifier le foncier disponible (à la vente, à la location)

   - Cartographie des friches

. En Île-de-France, Les TPE/PME ont perdu -13.3 milliards de chiffres d'affaires durant la crise du Covid.

### Attractivté des capitaux étrangers

En Île-de-France, 2020, 336 opérations de capitaux étrangers ont créé 11000 emplois.

Leur contrepartie, c'est un risque de perte de contrôle.

### Cession des entreprises 

103000 entreprises employant au moins un salarié, ou près d'une entreprise sur 3, en Île-de-France,
ont un dirigeant âgé de 55 ans ou plus => transmission à moyen ou long terme

Il faut identifier les sociétés susceptibles d'être cédées => Identifier les acteurs de cession/reprise. 

# Industrie

Un dispositif _relance industrie_ a été mis en place par la région en 2020

## Lieux industriels de référence et évènements

   - Additive Factory Hub, à Saclay
   - Semaine des métiers de l'industrie en Île-de-France
   - Club ETI Île-de-France

## Objectifs industrie 

### Objectifs industrie Majeurs

   - Cartographie des compétences
   - Cartographie des machines
   - Cartographie des fournisseurs

### Objectifs industrie Généraux

   - Décarbonation
   - Modèle circulaire (=?) plus sobre en ressources
   - Davantage de ressources locales
   - Plus d'indépendances dans l'approvisionnement des matières
   - Création de centres d'usinage et de prototypage de proximité
   - Favoriser les Entreprises du secteur Social et Solidaire (ESS) dans les filières industrielles stratégiques

### Objectifs industrie Enseignement

   - Développer des "écoles de production"
   - Mise en visibilité des formations existantes
   - Participation des industriels à la promotion de leurs métiers dans les lycées.


# Logistique

    - important, car lié au contexte urbain dense de l'IdF.  
    - l'e-commerce fait migrer les commerces physiques vers la logistique.

## Objectif logistiques

   - sanctuariser des sites logistiques (notamment pour le dernier kilomètre)
   - ports fluviaux
   - gares
   - aéroports

# Emploi

Sur 15 ans, l'Idf a perdu 100000 emplois

Difficulté de recruteemnt en :

   - BTP
   - aéronautique
   - santé

# Biens critiques

   - masques et blouses
   - anesthésiques
   - anticancéreux
   - antibiotiques

Approvisionnements en :

   - polymères
   - bois
   - zinc
   - PVC (pour le secteur du bâtiment)

# Agro alimentaire

Renforcer l'autonomie
   - circuits courts
   - vente directe
   - approvisionnements locaux

=> Multiplication par 5 de la part des produits locaux dans l'alimentation des francilliens



# Logement, Habitat



# Autres documents

Small Business Act (Commande publique bénéficie aux entreprises industrielles franciliennes)
Conseil National des Achats
Dispositif _relance industrie_ a été mis en place par la région en 2020
