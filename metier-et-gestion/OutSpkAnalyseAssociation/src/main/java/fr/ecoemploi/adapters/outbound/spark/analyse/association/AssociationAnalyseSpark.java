package fr.ecoemploi.adapters.outbound.spark.analyse.association;

import static org.apache.spark.sql.functions.*;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;

import org.apache.spark.sql.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import fr.ecoemploi.adapters.outbound.port.association.AssociationExportRepository;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.association.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.OptionsCreationLecture;
import fr.ecoemploi.adapters.outbound.spark.dataset.entreprise.*;
import fr.ecoemploi.domain.model.territoire.association.*;

/**
 * Service de recherche et d'analyse des associations.
 * @author Marc Le Bihan
 */
@Service
public class AssociationAnalyseSpark extends AbstractSparkDataset implements AssociationExportRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -7893119272048290362L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AssociationAnalyseSpark.class);

   /** Dataset des diplômes et formations. */
   @Autowired
   private AssociationWaldecDataset associationsDataset;
   
   /** Dataset des entreprises. */
   @Autowired
   private EntrepriseDataset entreprisesDataset;
   
   /** Dataset des établissements. */
   @Autowired
   private EtablissementDataset etablissementsDataset;

   /**
    * Exporter les associations ayant un thème social particulier dans un fichier CSV, en joignant les entreprises auxquelles elles se réfèrent si c'est le cas.
    * @param anneeRNA Année de lecture des associations.
    * @param anneeCOG Année du COG.
    * @param themeObjetSocial Thème de l'objet social associatif.
    * @return Fichier d'export CSV.
    */
   @SuppressWarnings("java:S1192") // Avertissement pour duplication de noms de colonnes
   public String exporterAssociationsCSV(ThemeObjetSocial themeObjetSocial, int anneeRNA, int anneeCOG) {
      Objects.requireNonNull(themeObjetSocial, "Le thème de l'objet social, pour une demande d'exportation d'associations en CSV, ne peut pas valoir null.");

      LOGGER.info("Demande d'export CSV des associations de thème social {} ({}) de France en {}, communes : {}", themeObjetSocial.getLibelle(), themeObjetSocial.name(), anneeRNA, anneeCOG);

      Dataset<Row> associations = this.associationsDataset.rowAssociationsThematiques(null, null, themeObjetSocial.getCodesObjetsSociaux(), anneeRNA, anneeCOG, true, AssociationWaldecTri.CODE_COMMUNE);
      associations = joinEntreprises(associations, anneeCOG, anneeRNA);
      associations = associations.orderBy("codeRegion", "nomEPCI", "codeDepartement", "nomCommune", "libelle_objet_social1", "libelle_objet_social2", "titre");

      associations = associations.select("codeRegion", "nomEPCI", "codeDepartement", "nomCommune", "populationTotale", 
         "titre", "objet", "site_web", "libelle_objet_social1", "libelle_objet_social2", 
         "nombreAnneesExistence", "libelleCategorieJuridique","libelle","libelleNAF","denominationEntreprise","denominationUsuelle1","denominationUsuelle2",
         "denominationUsuelle3","sigle","enseigne1","enseigne2","enseigne3","denominationEtablissement", "economieSocialeSolidaire",         
         "siret", "nom_declarant", "civilite_declarant", "date_creation", 
         "nombreAnneesDerniereDeclaration", "date_derniere_declaration", "position_activite", "date_dissolution", "codeCommune", 
         "adresse_siege_complement", "adresse_siege_numero_voie", "adresse_siege_type_voie", "adresse_siege_libelle_voie", "adresse_siege_distribution", 
         "adresse_siege_code_postal", "adresse_gestion_complement_association", "adresse_gestion_complement_geo", "adresse_gestion_libelle_voie", "adresse_gestion_distribution_facturation", 
         "adresse_gestion_code_postal", "adresse_gestion_achemine", "adresse_gestion_pays",
         "complementAdresse","numeroVoie","indiceRepetition","typeDeVoie",
         "libelleVoie","codePostal","distributionSpeciale","cedex","libelleCedex", "codeEPCI", "numero_waldec","diffusable"
      );

      associations = associations.withColumnRenamed("codeRegion", "Région") 
         .withColumnRenamed("nomEPCI", "Nom de l'intercommunalité")
         .withColumnRenamed("codeDepartement", "Département") 
         .withColumnRenamed("nomCommune", "Nom de la commune")
         .withColumnRenamed("populationTotale", "Population communale")
         .withColumnRenamed("titre", "Nom de l'association")
         .withColumnRenamed("objet", "Objet social de l'association")
         .withColumnRenamed("site_web", "Site web")
         .withColumnRenamed("libelle_objet_social1", "Catégorie d'objet social n°1")
         .withColumnRenamed("libelle_objet_social2", "Catégorie d'objet social n°2") 
         .withColumnRenamed("siret", "Siret")
         .withColumnRenamed("numero_waldec", "Numéro WALDEC/RNA")
         .withColumnRenamed("nombreAnneesExistence", "Nombre d'années d'existence") 
         .withColumnRenamed("date_creation", "Date de création")
         .withColumnRenamed("nombreAnneesDerniereDeclaration", "Nombre d'années depuis la dernière déclaration") 
         .withColumnRenamed("date_derniere_declaration", "Date de dernière déclaration") 
         .withColumnRenamed("position_activite", "Active")
         .withColumnRenamed("date_dissolution", "Date de dissolution") 
         .withColumnRenamed("codeCommune", "Code de la commune")
         .withColumnRenamed("adresse_siege_complement", "Complément de voie (siège)")
         .withColumnRenamed("adresse_siege_numero_voie", "Numéro de voie (siège)")
         .withColumnRenamed("adresse_siege_type_voie", "Type de voie (siège)")
         .withColumnRenamed("adresse_siege_libelle_voie", "Libellé de voie (siège)")
         .withColumnRenamed("adresse_siege_distribution", "Distribution (siège)") 
         .withColumnRenamed("adresse_siege_code_postal", "Code postal (siège)") 
         .withColumnRenamed("nom_declarant", "Nom du déclarant")
         .withColumnRenamed("adresse_gestion_complement_association", "Complément association (gestion)") 
         .withColumnRenamed("adresse_gestion_complement_geo", "Complément géographique (gestion)")
         .withColumnRenamed("adresse_gestion_libelle_voie", "Libellé de voie (gestion)")
         .withColumnRenamed("adresse_gestion_distribution_facturation", "Distribution facturation (gestion)")
         .withColumnRenamed("adresse_gestion_code_postal", "Code postal (gestion)")
         .withColumnRenamed("adresse_gestion_achemine", "Acheminement (gestion)")
         .withColumnRenamed("adresse_gestion_pays", "Pays (gestion)")
         .withColumnRenamed("civilite_declarant","Civilité déclarant (gestion)") 
         .withColumnRenamed("codeEPCI", "Code de l'intercommunalité (code EPCI)")

         .withColumnRenamed("sigle", "Signe")
         .withColumnRenamed("libelleCategorieJuridique", "Catégorie Juridique")
         .withColumnRenamed("libelle", "Libellé")
         .withColumnRenamed("libelleNAF", "Activité (APE)")
         .withColumnRenamed("denominationEntreprise","Dénomination entreprise")
         .withColumnRenamed("denominationUsuelle1","Dénomination usuelle 1")
         .withColumnRenamed("denominationUsuelle2","Dénomination usuelle 2")
         .withColumnRenamed("denominationUsuelle3","Dénomination usuelle 3")
         .withColumnRenamed("economieSocialeSolidaire", "Domaine Economie Sociale et Solidaire")
         .withColumnRenamed("diffusable","Diffusable")
         .withColumnRenamed("complementAdresse","Complément adresse (établissement)")
         .withColumnRenamed("numeroVoie","Numéro de voie (établissement)")
         .withColumnRenamed("indiceRepetition","Répétition voie (établissement)")
         .withColumnRenamed("typeDeVoie","Type de voie (établissement)")
         .withColumnRenamed("libelleVoie","Libellé de voie (établissement)")
         .withColumnRenamed("codePostal","Code postal (établissement)")
         .withColumnRenamed("distributionSpeciale","Distribution (établissement)")
         .withColumnRenamed("cedex","Cedex (établissement)")
         .withColumnRenamed("libelleCedex","Libellé cedex (établissement)")
         .withColumnRenamed("enseigne1","Enseigne 1")
         .withColumnRenamed("enseigne2","Enseigne 2")
         .withColumnRenamed("enseigne3","Enseigne 3")
         .withColumnRenamed("denominationEtablissement","Dénomination établissement");

      String prefixeFichierStore = MessageFormat.format("associations_{0}_", themeObjetSocial.name().toLowerCase());
      return exportCSV().exporterCSV(associations, prefixeFichierStore, true).getAbsolutePath();
   }

   /**
    * Joindre à un Dataset Row d'associations les établissements qui lui sont rattachés.
    * @param associations Dataset Row d'associations
    * @param anneeCOG Année du COG
    * @param anneeSirene Année Sirene des établissements
    * @return Dataset complété des établissements
    */
   @SuppressWarnings("java:S1192") // Avertissement pour duplication de noms de colonnes
   public Dataset<Row> joinEntreprises(Dataset<Row> associations, int anneeCOG, int anneeSirene) {
      OptionsCreationLecture options = optionsCreationLecture();

      Dataset<Row> entreprises = this.entreprisesDataset.rowEntreprises(options, null, anneeSirene, true, new EntrepriseTriSiren())
         .drop(col("active"))
         .drop(col("etablissements"))
         .drop(col("caractereEmployeur"))
         .drop(col("anneeValiditeEffectifSalarie"))
         .drop(col("trancheEffectifSalarie"))
         .drop(col("nomenclatureActivitePrincipale"))
         .drop(col("activitePrincipale"))
         .drop(col("dateDebutHistorisation"))
         .drop(col("dateDernierTraitement"))
         .drop(col("nombrePeriodes"));

      associations = associations.join(entreprises, entreprises.col("rna").equalTo(associations.col("numero_waldec")), "left_outer");

      Dataset<Row> etablissements = this.etablissementsDataset.rowEtablissements(options, null, new EtablissementTriSirenSiret(), anneeCOG, anneeSirene, true, true, true);

      return associations.join(etablissements, etablissements.col("siren").equalTo(associations.col("siren")), "left_outer")
         .drop(etablissements.col("codeRegion"))
         .drop(etablissements.col("codeDepartement"))
         .drop(etablissements.col("codeCommune"))
         .drop(etablissements.col("nomCommune"))
         .drop(etablissements.col("populationTotale"))
         .drop(etablissements.col("siret"))
         .drop(etablissements.col("codeEPCI"))
         .drop(etablissements.col("nomEPCI"))
         .drop(etablissements.col("nombreperiodes"))
         .drop(etablissements.col("siren"))
         .drop(etablissements.col("active"));
   }
}
