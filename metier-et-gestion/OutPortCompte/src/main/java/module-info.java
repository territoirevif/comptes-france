/**
 * Outbound Port des comptes
 */
module fr.ecoemploi.outbound.port.compte {
   requires fr.ecoemploi.domain.model;
   requires spring.context;

   exports fr.ecoemploi.adapters.outbound.port.compte;
}
