package fr.ecoemploi.adapters.outbound.port.compte;

import java.io.Serializable;

/**
 * Repository de la balance des comptes.
 */
public interface BalanceCompteRepository extends Serializable {
   /**
    * Charger les comptes des communes et des intercommunalités.
    * @param anneeCOG Année de référence du Code Officiel Géographique à charger.<br>
    * 0 si toutes les années connues doivent être chargées (à partir de 2016).
    */
   void chargerReferentiels(int anneeCOG);
}
