package fr.ecoemploi.adapters.outbound.port.compte;

import java.io.Serializable;

import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.ComptesIndividuelsCommune;

/**
 * Repository des comptes individuels de commune.
 */
public interface BalanceCompteIndividuelCommuneRepository extends Serializable {
   /**
    * Obtenir les comptes individuels d'une commune.
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param anneeExercice Année.
    * @param codeCommune Code commune.
    * @return Comptes individuels de la commune.
    */
   ComptesIndividuelsCommune obtenirComptesIndividuels(int anneeCOG, int anneeExercice, CodeCommune codeCommune);
}
