package fr.ecoemploi.adapters.outbound.port.compte;

import java.io.Serializable;
import java.util.*;

import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.Compte;

/**
 * Repository des comptes de communes
 */
public interface BalanceCompteCommuneRepository extends Serializable {
   /**
    * Obtenir la balance des comptes des d'une commune (Budget Principal et annexes).
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param anneeExercice Année.
    * @param codeCommune Code commune.
    * @return Liste des comptes par SIRET.
    */
   Map<SIRET, List<Compte>> obtenirBalanceComptesCommune(int anneeCOG, int anneeExercice, CodeCommune codeCommune);
}
