package fr.ecoemploi.adapters.inbound.rest.cog;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.Serial;
import java.util.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.info.*;
import io.swagger.v3.oas.annotations.media.*;
import io.swagger.v3.oas.annotations.responses.*;
import io.swagger.v3.oas.annotations.tags.Tag;

import fr.ecoemploi.adapters.inbound.port.cog.CogAPI;
import fr.ecoemploi.adapters.inbound.rest.core.AbstractRestController;
import fr.ecoemploi.application.port.cog.CogPort;
import fr.ecoemploi.domain.model.territoire.*;

/**
 * Services REST du code officiel géographique
 * @author Marc Le Bihan
 */
@RestController
@RequestMapping(value="/cog", name="cog")
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:4200", "http://localhost:9091"})
@OpenAPIDefinition(info =
   @Info(title = "Extraction et analyse des données open data souveraines", version = "${project.version}")
)
@Tag(name = "Code Officiel Géographique", description = "Maillage territorial du Code Officiel Géographique")
public class COGController extends AbstractRestController implements CogAPI {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 6976436398273195009L;

   /** Service du code officiel géographique. */
   private final CogPort cogPort;

   /** Source de messages. */
   private final transient MessageSource messageSource;

   /**
    * Construire un contrôleur REST présentant les fonctions autour du Code Officiel Géographique
    * @param cogPort Services du COG.
    * @param messageSource Bundle de messages
    */
   @Autowired
   public COGController(CogPort cogPort, MessageSource messageSource) {
      this.cogPort = cogPort;
      this.messageSource = messageSource;
   }

   /**
    * Charger le référentiel territorial dans les fichiers de cache.
    * @param anneeCOG Année de référence du Code Officiel Géographique à charger.<br>
    * 0 si toutes les années connues doivent être chargées (à partir de 2016).
    */
   @Operation(description = "Charger les référentiels territoriaux dans les fichiers de cache")
   @GetMapping(value = "/chargerReferentiel")
   @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Référentiel chargé."),
        @ApiResponse(responseCode = "403", description = "Si la génération de données par l'application est interdite."),
        @ApiResponse(responseCode = "500", description = "Un incident durant le chargement.")
      }
   )   
   public void chargerReferentiels(
      @Parameter(name = "anneeCOG", description = "Année du Code Officiel Géographique : 0 pour toutes les années connues.", example = "2024")
      @RequestParam(name="anneeCOG") int anneeCOG) {
      refusSiGenerationInterdite();
      this.cogPort.chargerReferentiels(anneeCOG);
   }

   /**
    * Obtenir la liste des communes.
    * @param anneeCOG Année de référence du Code Officiel Géographique.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *                                          false si seules les communes de type COM sont lues.
    * @return Liste des communes.
    */
   @Operation(description = "Retourne les communes du Code Officiel Geographique en vigueur une année particulière sous forme de Map.")
   @GetMapping(value = "/communes")
   @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Communes présentes sur le territoire cette année là, sous la forme d'une <code>Map<String, Commune></code>."),
        @ApiResponse(responseCode = "403", description = "- Si l'exécution de méthodes REST renvoyant des données volumineuses n'est pas autorisée.\n" +
         "- Si les données raffinées requises pour l'exécution de cette méthode, avec ces paramètres, n'ont pas été générées, et que l'application n'a pas le droit de les produire."),
        @ApiResponse(responseCode = "500", description = "Un incident est survenu durant l'extraction des communes.")
      }
   )   
   public Map<String, Commune> obtenirCommunes(
      @Parameter(name = "anneeCOG", description = "Année du Code Officiel Géographique.", example = "2024")
      @RequestParam(name="anneeCOG") int anneeCOG,

      @Parameter(name = "inclureCommunesDelegueesAssociees", description = "Inclure communes déléguées ou associées.", example = "true")
      @RequestParam(name="inclureCommunesDelegueesAssociees") boolean inclureCommunesDelegueesAssociees) {
      assertGenerationAutorisee(true);
      return this.cogPort.obtenirCommunes(anneeCOG, inclureCommunesDelegueesAssociees);
   }

   /**
    * Obtenir la liste des communes.
    * @param anneeCOG Année de référence du Code Officiel Géographique.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *                                          false si seules les communes de type COM sont lues.
    * @return Liste des communes.
    */
   @Operation(description = "Retourne les communes du Code Officiel Geographique en vigueur une année particulière.")
   @GetMapping(value = "/communesTriLocale")
   @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Communes présentes sur le territoire cette année là, sous la forme d'une liste triée par nom de commune, dans l'ordre d'une locale."),
        @ApiResponse(responseCode = "403", description = "- Si l'exécution de méthodes REST renvoyant des données volumineuses n'est pas autorisée.\n" +
            "- Si les données raffinées requises pour l'exécution de cette méthode, avec ces paramètres, n'ont pas été générées, et que l'application n'a pas le droit de les produire."),
        @ApiResponse(responseCode = "500", description = "Un incident est survenu durant l'extraction des communes.")
      }
   )   
   public List<Commune> obtenirCommunesTriParLocale(
      @Parameter(name = "anneeCOG", description = "Année du Code Officiel Géographique.", example = "2024")
      @RequestParam(name="anneeCOG") int anneeCOG,

      @Parameter(name = "inclureCommunesDelegueesAssociees", description = "Inclure communes déléguées ou associées.", example = "true")
      @RequestParam(name="inclureCommunesDelegueesAssociees") boolean inclureCommunesDelegueesAssociees) {
      assertGenerationAutorisee(true);
      return this.cogPort.obtenirCommunesTriParLocale(anneeCOG, inclureCommunesDelegueesAssociees, Locale.FRENCH.toString());
   }

   /**
    * Déterminer ce qu'est devenue une commune au 1er Janvier d'une année.
    * @param codeCommune Code de la commune possiblement devenue historique.
    * @param annee Année considérée.
    * @return Commune résultante ou null, s'il n'y a aucun changement.
    */
   @Operation(description = "Indique si une commune a changé.<br>" +
      "Chaque année, des communes fusionnent ou défusionnent. Si vous faites façe à une commune inconnue, soumettez-là à cette fonction.<br>" +
      "Elle vous indiquera si au 1er Janvier de l'année que vous examinez, quelle commune la remplace.<br>" +
      "<u>Limitation :</u> L'examen des mouvements de communes se fait sur la période [01/01/2010 ; 01/01/annee] et pas dès les premiers historiques de 1943.")
   @GetMapping(value = "/estDevenue")
   @ApiResponses(value = {
         @ApiResponse(responseCode = "200", description = "L'examen s'est déroulé sans incident. Null si aucun changement n'a été détecté pour cette commune. Sinon, son détail.",
            content = {@Content(schema = @Schema(implementation = CommuneHistorique.class),
               examples = {
                  @ExampleObject(name = "Arboys en Bugey, anciennement Saint-Bois",
                     description = "La commune historique de code '01340' (Saint-Bois) est devenue celle de code '01015' : Arboys en Bugey, de type COM." +
                        "<br> Son dernier évènement connu date du 01/01/2016.",
                     value = """
                     {
                       "dateEffetEvenement": [
                         2016,
                         1,
                         1
                       ],
                       "typeCommune": "COM",
                       "codeCommuneHistorique": "01340",
                       "nomCommuneHistorique": null,
                       "typeCommuneHistorique": null,
                       "codeCommune": "01015",
                       "typeDeNom": "1",
                       "nomCommuneMajuscules": "ARBOYS EN BUGEY",
                       "nomCommune": "Arboys en Bugey",
                       "nomCommuneAvecArticle": "Arboys en Bugey",
                       "typeEvenement": "CREATION_DE_COMMUNE_NOUVELLE"
                     }
                     )}
                     """)
               }
            )}),

         @ApiResponse(responseCode = "403", description = "S'il n'est pas permis de produire des données raffinées n'ont pas été générées, et que l'application n'a pas le droit de les produire"),
         @ApiResponse(responseCode = "500", description = "Un incident est survenu durant l'examen.")
      }
   )
   public CommuneHistorique estDevenue(
      @Parameter(name = "codeCommune", description = "Code de la commune", example = "01340")
      @RequestParam(name="codeCommune") String codeCommune,

      @Parameter(name = "annee", description = "Année de réification. Qu'est devenue la commune au 01/01/annee ?", example = "2024")
      @RequestParam(name="annee") int annee) {
      return this.cogPort.estDevenue(codeCommune, annee);
   }

   /**
    * Afficher les communes d'une intercommunalité.
    * @param anneeCOG Année du code officiel géographique.
    * @param codeEPCI Code EPCI de l'intercommunalité recherchée.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *                                          false si seules les communes de type COM sont lues.
    * @return Communes formattées.
    */
   @Operation(description = "Affiche en sortie HTML les communes et compétences d'une intercommunalité une année particulière.")
   @GetMapping(value = "/communes/communesIntercoHtml")
   @Produces(MediaType.TEXT_HTML)

   @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "La liste des communes et compétences de l'intercommunalité ont été renvoyées."),
      @ApiResponse(responseCode = "403", description = "Si les données raffinées requises pour l'exécution de cette méthode, avec ces paramètres, n'ont pas été générées, et que l'application n'a pas le droit de les produire."),
      @ApiResponse(responseCode = "500", description = "Un incident est survenu durant l'extraction des données de l'intercommunalité.")
   })
   public String communesIntercoHtml(
      @Parameter(name = "anneeCOG", description = "Année du Code Officiel Géographique.", example = "2024")
      @RequestParam(name="anneeCOG") int anneeCOG,

      @Parameter(name = "codeEPCI", description = "Code SIREN de l'intercommunalité.", examples = @ExampleObject(name ="CC Écueillé - Valençay (Indre)", value = "200040558"))
      @RequestParam(name="codeEPCI") String codeEPCI,

      @Parameter(name = "inclureCommunesDelegueesAssociees", description = "Indique s'il faut inclure dans cette liste les communes déléguées ou associées.", example = "false")
      @RequestParam(name="inclureCommunesDelegueesAssociees") boolean inclureCommunesDelegueesAssociees) {
      // Charger le code officiel géographique sur l'année voulue.
      CodeOfficielGeographique cog = this.cogPort.obtenirCodeOfficielGeographique(anneeCOG, inclureCommunesDelegueesAssociees);
      Intercommunalites intercommunalites = cog.getIntercommunalites();

      // Rechercher l'intercommunalité désirée.
      StringBuffer html = new StringBuffer();
      html(html, "html.debut");

      try {
         Communes communes = cog.getCommunesMembres(new SIRENCommune(codeEPCI));
         Intercommunalite intercommunalite = intercommunalites.get(codeEPCI);
         html(html, "html.titre", intercommunalite.getNatureJuridique().getLibelle(), intercommunalite.getNomGroupement(), intercommunalite.getSiren());
         html(html, "html.body.debut");

         communes = communes.triParNomDeCommune(Locale.FRENCH);
         communes(html, intercommunalite, communes);
         competences(html, intercommunalite);
      }
      catch(@SuppressWarnings("unused") IntercommunaliteAbsenteDansCommuneSiegeException e) {
         html(html, "html.intercommunalite_inconnue", codeEPCI, anneeCOG);
      }

      html(html, "html.body.fin");
      html(html, "html.fin");
      return html.toString();
   }

   /**
    * Bloc de communes.
    * @param html HTML.
    * @param intercommunalite Intercommunalité dont les communes doivent être représentées.
    * @param communesMembres Communes membres de l'intercommunalité.
    */
   private void communes(StringBuffer html, Intercommunalite intercommunalite, Communes communesMembres) {
      html(html, "html.commune.titre", intercommunalite.getNomGroupement());
      html(html, "html.commune_interco_header");

      for(Commune commune : communesMembres.values()) {
         html(html, "html.commune_interco_element", commune.getCodeCommune(), commune.getNomCommune(), commune.getPopulation(), commune.getCodeDepartement(), commune.getSirenCommune());
      }

      html(html, "html.commune_interco_footer");
   }

   /**
    * Bloc de compétences.
    * @param html HTML.
    * @param intercommunalite Intercommunalité dont les communes doivent être représentées.
    */
   private void competences(StringBuffer html, Intercommunalite intercommunalite) {
      html(html, "html.competences.titre");

      for(TypeCompetence type : TypeCompetence.values()) {
         html(html, "html.competences_type", type.name());
         html(html, "html.competences_header");

         for(CompetenceGroupement competence : intercommunalite.getCompetencesTriees(type)) {
            html(html, "html.competences_element", competence.name(), competence.getLibelle());
         }

         html(html, "html.competences_footer");
      }
   }

   /**
    * Ajouter de l'HTML au buffer de page.
    * @param html HTML à compléter, peut valoir null.
    * @param clef Clef de ressources.
    * @param args Arguments.
    */
   private void html(StringBuffer html, String clef, Object... args) {
      String texte = this.messageSource.getMessage(clef, args, Locale.getDefault());
      texte = texte.replace("\u00A0", "&nbsp;");
      texte = texte.replace("\u202F", "&nbsp;");
      html.append(texte);
   }
}
