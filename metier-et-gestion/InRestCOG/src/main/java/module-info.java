/**
 * Inbound Rest du catalogue data.gouv.fr
 */
module fr.ecoemploi.inbound.rest.cog {
   // requires jakarta.ws.rs;
   requires java.ws.rs;
   requires spring.beans;
   requires spring.web;
   requires spring.context;
   requires io.swagger.v3.oas.annotations;

   requires fr.ecoemploi.domain.model;
   requires fr.ecoemploi.inbound.rest.core;

   requires fr.ecoemploi.application.port.cog;
   requires fr.ecoemploi.inbound.port.cog;

   exports fr.ecoemploi.adapters.inbound.rest.cog;
}
