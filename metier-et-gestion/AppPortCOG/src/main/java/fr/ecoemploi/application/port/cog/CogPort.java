package fr.ecoemploi.application.port.cog;

import java.io.Serializable;
import java.util.*;

import fr.ecoemploi.domain.model.territoire.*;

/**
 * Port du Code Officiel Géographique
 */
public interface CogPort extends Serializable {
   /**
    * Charger le référentiel territorial dans les fichiers de cache.
    * @param anneeCOG Année de référence du Code Officiel Géographique à charger.<br>
    * 0 si toutes les années connues doivent être chargées (à partir de 2016).
    */
   void chargerReferentiels(int anneeCOG);

   /**
    * Déterminer ce qu'est devenue une commune au 1er Janvier d'une année.
    * @param codeCommune Code de la commune initiale.
    * @param annee Année considérée.
    * @return Commune résultante ou null, s'il n'y a aucun changement.
    */
   CommuneHistorique estDevenue(String codeCommune, int annee);

   /**
    * Obtenir le code officiel géographique.
    * @param anneeCOG année du COG recherché.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *                                          false si seules les communes de type COM sont lues.
    * @return Code Officiel Géographique.
    */
   CodeOfficielGeographique obtenirCodeOfficielGeographique(int anneeCOG, boolean inclureCommunesDelegueesAssociees);

   /**
    * Obtenir la liste des communes.
    * @param anneeCOG Année de référence du Code Officiel Géographique.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *                                          false si seules les communes de type COM sont lues.
    * @return Liste des communes.
    */
   Map<String, Commune> obtenirCommunes(int anneeCOG, boolean inclureCommunesDelegueesAssociees);

   /**
    * Obtenir la liste des communes, triées par une locale.
    * @param anneeCOG Année de référence du Code Officiel Géographique.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *                                          false si seules les communes de type COM sont lues.
    * @param locale Locale à utiliser pour le tri.
    * @return Liste des communes.
    */
   List<Commune> obtenirCommunesTriParLocale(int anneeCOG, boolean inclureCommunesDelegueesAssociees, String locale);
}
