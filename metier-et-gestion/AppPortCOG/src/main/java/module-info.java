/**
 * Application Port du Code Officiel Géographique (COG)
 */
module fr.ecoemploi.application.port.cog {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.application.port.cog;
}
