package fr.ecoemploi.service;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.TechniqueException;

/**
 * Exception levée si un fichier géographique n'existe pas.
 * @author Marc LE BIHAN
 */
public class FichierGeographiqueAbsentException extends TechniqueException {
   /** Serial ID */
   @Serial
   private static final long serialVersionUID = -1809793600521388778L;

   /** Le fichier géographique qui n'existe pas. */
   private final String fichierAbsent;

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param fichier Fichier géographique qui n'existe pas.
    */
   public FichierGeographiqueAbsentException(String message, String fichier) {
      super(message);
      this.fichierAbsent = fichier;
   }

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception.
    * @param fichier Fichier géographique qui n'existe pas.
    */
   public FichierGeographiqueAbsentException(String message, Throwable cause, String fichier) {
      super(message, cause);
      this.fichierAbsent = fichier;
   }

   /**
    * Renvoyer le fichier géographique qui n'a pas été trouvé.
    * @return Fichier géographique qui n'existe pas.
    */
   public String getFichierFeatureAbsent() {
      return this.fichierAbsent;
   }
}
