package fr.ecoemploi.service;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.TechniqueException;

/**
 * Exception levée si un répertoire recherché n'existe pas.
 * @author Marc LE BIHAN
 */
public class RepertoireInexistantException extends TechniqueException {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -583849011503323296L;
   
   /** Chemin d'accès vers le répertoire manquant. */
   private final String repertoireManquant;
   
   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param repertoireManquant Répertoire qui est manquant.
    */
   public RepertoireInexistantException(String message, String repertoireManquant) {
      super(message);
      this.repertoireManquant = repertoireManquant;
   }
   
   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception.
    * @param repertoireManquant Répertoire qui est manquant.
    */
   public RepertoireInexistantException(String message, Throwable cause, String repertoireManquant) {
      super(message, cause);
      this.repertoireManquant = repertoireManquant;
   }

   /**
    * Renvoyer le nom du répertoire manquant.
    * @return Répertoire manquant, peut valoir null, s'il n'est pas connu.
    */
   public String getRepertoireManquant() {
      return this.repertoireManquant;
   }
}
