package fr.ecoemploi.service;

import java.io.*;
import java.security.*;

/**
 * Génération d'un SHA256 sur un fichier.
 * @author Marc Le Bihan
 */
public class SHA256Checksum {
   /** Taille du Buffer. */
   static final int BUFFER_SIZE = 10000000;

   /**
    * Renvoyer un checksum en Sha256
    * @param filename Nom du fichier.
    * @return Checksum.
    */
   public static String getMD5Checksum(String filename) {
      try {
         byte[] b = createChecksum(filename);
         StringBuilder result = new StringBuilder();

         for (byte value : b) {
            result.append(Integer.toString((value & 0xff) + 0x100, 16).substring(1));
         }
         return result.toString();
      }
      catch(Exception e) {
         throw new RuntimeException(e.getMessage(), e);
      }
   }
   
   /**
    * Génerer le checksum.
    * @param filename Nom du fichier.
    * @return Checksum, en octets.
    * @throws IOException si le fichier n'existe pas ou ne peut être lu.
    * @throws NoSuchAlgorithmException si l'algorithme SHA-256 n'a pu être initialisé.
    */
   private static byte[] createChecksum(String filename) throws IOException, NoSuchAlgorithmException {
      byte[] buffer = new byte[BUFFER_SIZE];
      MessageDigest complete = MessageDigest.getInstance("SHA-256");
      int numRead;

      try(InputStream fis = new FileInputStream(filename)) {
         do {
            numRead = fis.read(buffer);
            
            if (numRead > 0) {
               complete.update(buffer, 0, numRead);
            }
         } 
         while(numRead != -1);
      } 
       
      return complete.digest();
   }
}
