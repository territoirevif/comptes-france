module fr.ecoemploi.application.service.core {
   requires fr.ecoemploi.domain.utils;
   requires org.slf4j;

   exports fr.ecoemploi.service;
}