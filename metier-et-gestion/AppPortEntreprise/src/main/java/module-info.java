/**
 * Application Port des entreprises et établissements
 */
module fr.ecoemploi.application.port.entreprise {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.application.port.entreprise;
}
