package fr.ecoemploi.application.port.entreprise;

import java.io.Serializable;

import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.entreprise.Entreprises;

/**
 * Actions possibles sur les entreprises.
 * @author Marc Le Bihan
 */
public interface EntreprisePort extends Serializable {
   /**
    * Charger le référentiel des entreprises pour une année.
    * @param anneeCOG Année du Code Officiel Géographique, pour les communes de référence
    *     Si toutes les années sirene sont chargées, placer dans ce champ un nombre relatif (0, -1...).
    * @param anneeSirene année désirée ou 0 pour toutes les années applicables.
    */
   void chargerReferentiel(int anneeCOG, int anneeSirene);

   /**
    * Obtenir les entreprises et établissements d'une commune.
    * @param anneeCOG Année du Code Officiel Géographique de la commune.
    * @param anneeSirene Année du SIREN des entreprises.
    * @param codeCommune Code de la commune désirée.
    * @return Entreprises et établissements de cette commune.
    */
   Entreprises obtenirEntreprisesEtEtablissements(int anneeCOG, int anneeSirene, CodeCommune codeCommune);
}
