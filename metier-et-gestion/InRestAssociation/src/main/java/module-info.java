/**
 * Inbound Rest des associations
 */
module fr.ecoemploi.inbound.rest.association {
   // requires jakarta.ws.rs;
   requires java.ws.rs;
   requires spring.beans;
   requires spring.web;
   requires spring.context;
   requires io.swagger.v3.oas.annotations;

   requires fr.ecoemploi.domain.model;
   requires fr.ecoemploi.inbound.rest.core;

   requires fr.ecoemploi.application.port.association;
   requires fr.ecoemploi.inbound.port.association;

   exports fr.ecoemploi.adapters.inbound.rest.association;
}
