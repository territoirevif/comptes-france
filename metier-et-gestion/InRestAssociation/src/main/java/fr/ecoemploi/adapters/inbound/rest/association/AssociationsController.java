package fr.ecoemploi.adapters.inbound.rest.association;

import java.io.Serial;
import java.util.*;
// import jakarta.ws.rs.*;
import javax.ws.rs.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.media.*;
import io.swagger.v3.oas.annotations.responses.*;
import io.swagger.v3.oas.annotations.tags.Tag;

import fr.ecoemploi.adapters.inbound.port.association.AssociationApi;
import fr.ecoemploi.adapters.inbound.rest.core.AbstractRestController;
import fr.ecoemploi.application.port.association.AssociationPort;
import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.association.*;

/**
 * Contrôleur REST pour la gestion des associations.
 * @author Marc LE BIHAN
 */
@RestController
@RequestMapping(value = "/associations", name="associations")
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:4200", "http://localhost:9091"})
@Tag(name = "Association", description = "Gestion des associations")
public class AssociationsController extends AbstractRestController implements AssociationApi {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 5394587189534374747L;

   /** Service de recherche des associations présentes sur le territoire. */
   @Autowired
   private AssociationPort associationPort;

   /**
    * Charger le référentiel des associations.
    * @param anneeRNA Année de référence SIREN à charger.<br>
    * 0 si toutes les années connues doivent être chargées (à partir de 2018).
    * @param anneeCOG Année du Code Officiel Géographique, pour les communes de référence
    *     Si toutes les années sirene sont chargées, placer dans ce champ un nombre relatif (0, -1...).
    */
   @Operation(description = "Charger le référentiel des associations dans les fichiers de cache")
   @GetMapping(value = "/chargerAssociations")
   @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Référentiel chargé."),
      @ApiResponse(responseCode = "403", description = "Si la génération de données par l'application est interdite."),
      @ApiResponse(responseCode = "500", description = "Un incident durant le chargement.")
   })
   @Override
   public void chargerAssociations(
      @Parameter(name = "anneeRNA", description = "Année du Registre National des Associations (RNA) : 0 pour toutes les années connues.", examples = {
         @ExampleObject(value = "2024", name = "précise", summary = "une année RNA particulière"),
         @ExampleObject(value = "0", name = "relative", summary = "Toutes les années RNA disponibles")
      })
      @RequestParam(name= "anneeRNA") int anneeRNA,

      @Parameter(name = "anneeCOG", description = "Année du code officiel géographique de référence, pour les communes.<br>" +
         "Si vous chargez toutes les années SIRENE disponibles, placez ici un nombre relatif : 0, -1... : <br>" +
         "l'année du code officiel géographique sera alors calculée d'après l'année SIRENE.",
         examples = {
            @ExampleObject(value = "2024", name="précise", summary = "une année du COG précise",
               description = "Une année du code officiel géographique précise."),

            @ExampleObject(value = "-1", name="relative", summary = "une année du COG relative à celle du RNA choisi",
               description = "Ici pour une année RNA 2023, par exemple,<br>" +
                  "2023 - 1 = 2022 sera l'année du code officiel géographique associé.")
         }
      )
      @RequestParam(name="anneeCOG") int anneeCOG) {
      refusSiGenerationInterdite();
      this.associationPort.chargerAssociations(anneeRNA, anneeCOG);
   }

   /**
    * Obtenir la liste des associations d'une commune.
    * @param anneeRNA Année du Registre National des Associations
    * @param anneeCOG Année du Code Officiel Géographique.
    * @param codeCommune Code Commune.
    * @return Liste des associations.
    */
   @GetMapping(value="/obtenirAssociations")
   @Operation(description = "Obtenir les associations d'une commune")
   @ApiResponses(value = {
      @ApiResponse(responseCode="200", description = "Associations présentes dans une commune.",
         content = {@Content(schema = @Schema(implementation = Association.class))})
   })
   public List<Association> obtenirAssociations(
      @Parameter(name = "anneeRNA", description = "Année de référence du Registre National des Associations pour la sélection.", example = "2024")
      @RequestParam(name="anneeRNA")
      int anneeRNA,

      @Parameter(name = "anneeCOG", description = "Année du code officiel géographique, pour le maillage territorial.", example = "2024")
      @RequestParam(name="anneeCOG")
      int anneeCOG,

      @Parameter(name = "codeCommune", description = "Code de la commune", example = "29046")
      @RequestParam(name="codeCommune")
      String codeCommune) {
      assertGenerationAutorisee(true);
      return this.associationPort.obtenirAssociations(anneeRNA, anneeCOG, new CodeCommune(codeCommune));
   }

   /**
    * Exporter les associations ayant un thème social particulier.
    * @param anneeRNA Année de référence des associations.
    * @param anneeCOG Année du COG à considérer.
    * @param themeObjetSocial Thème de l'objet social associatif.
    * @return Fichier d'exportation CSV des associations.
    */
   @GetMapping(value="/exporterAssociationsThemeSocial")
   @Produces("text/csv")
   
   @Operation(description = "Exporte toutes les associations présentes en France ayant un thème social particulier en CSV")
   @ApiResponses(value = {
        @ApiResponse(responseCode="200", description = "Chemin d'accès vers le fichier CSV des associations produit.",
              content = {@Content(schema = @Schema(implementation = String.class))})
      }
   )   
   public String exporterAssociationsCSV(
      @Parameter(name = "themeObjetSocial", description = "Thème des objets sociaux associatifs.", example = "DEVELOPPEMENT_TOURISTIQUE")
      @RequestParam(name="themeObjetSocial") ThemeObjetSocial themeObjetSocial,

      @Parameter(name = "anneeRNA", description = "Année de référence du Registre National des Associations pour la sélection.", example = "2024")
      @RequestParam(name="anneeRNA") int anneeRNA,

      @Parameter(name = "anneeCOG", description = "Année du COG pour les communes et régions.", example = "2024")
      @RequestParam(name="anneeCOG") int anneeCOG) {
      assertGenerationAutorisee(true);
      return this.associationPort.exporterAssociationsCSV(themeObjetSocial, anneeRNA, anneeCOG);
   }
}
