/**
 * Inbound Port des Associations
 */
module fr.ecoemploi.inbound.port.association {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.adapters.inbound.port.association;
}
