package fr.ecoemploi.adapters.inbound.port.association;

import java.io.Serializable;
import java.util.List;

import fr.ecoemploi.domain.model.territoire.association.*;

/**
 * Actions possibles sur les associations.
 */
public interface AssociationApi extends Serializable {
   /**
    * Charger le ou les référentiels des associations.
    * @param anneeRNA Année du Registre National des Associations<br>
    *     0: pour toutes les années disponibles
    * @param anneeCOG Année du Code Officiel Géographique.
    */
   void chargerAssociations(int anneeRNA, int anneeCOG);

   /**
    * Obtenir la liste des associations d'une commune.
    * @param anneeRNA Année du Registre National des Associations
    * @param anneeCOG Année du Code Officiel Géographique.
    * @param codeCommune Code Commune.
    * @return Liste des associations.
    */
   List<Association> obtenirAssociations(int anneeRNA, int anneeCOG, String codeCommune);

   /**
    * Exporter les associations touristique présentes en France.
    * @param anneeRNA Année de référence des associations.
    * @param anneeCOG Année du COG à considérer.
    * @param themeObjetSocial Thème de l'objet social associatif.
    * @return Fichier d'exportation CSV des associations.
    */
   String exporterAssociationsCSV(ThemeObjetSocial themeObjetSocial, int anneeRNA, int anneeCOG);
}
