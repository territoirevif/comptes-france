package fr.ecoemploi.adapters.outbound.spark.dataset.association;

import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;


/**
 * Application de test pour Spark
 * @author Marc LE BIHAN
 */
@SpringBootApplication
@ComponentScan(basePackages={"fr", "com"})
public class SparkAssociationTestApplication {
}
