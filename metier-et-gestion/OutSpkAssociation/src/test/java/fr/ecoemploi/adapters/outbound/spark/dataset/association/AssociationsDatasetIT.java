package fr.ecoemploi.adapters.outbound.spark.dataset.association;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.stream.Paginateur;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.verifications.Verification;
import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.association.Association;

import org.apache.spark.sql.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

/**
 * Tests sur le dataset des associations.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkAssociationTestApplication.class)
class AssociationsDatasetIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 8907755776581833882L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AssociationsDatasetIT.class);

   /** Dataset des associations. */
   @Autowired
   private AssociationWaldecDataset associationsDataset;

   /** Dataset des objets sociaux des associations. */
   @Autowired
   private AssociationObjetsSociauxDataset objetsSociauxDataset;

   /** Année du cog pour les tests. */
   private static final int COG = 2024;
   
   /** Année du RNA pour les tests. */
   private static final int ANNEE_RNA = 2024;
   
   /** Vérifications par défaut. */
   private Verification[] verifications;

   /**
    * Options globales des tests.
    */
   @BeforeEach
   public void optionsGlobales() {
      this.verifications = new Verification[] {};
   }

   /**
    * Lecture des associations.
    */
   @DisplayName("Row Associations du RNA Waldec")
   @ParameterizedTest(name = "Associations {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   void associationsRegistreNational(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.associationsDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      LOGGER.info("Lecture des associations pour l'année {}, cog {}, restriction : {} → {}, {}", ANNEE_RNA, COG, typeRestriction, communes, annee);

      Dataset<Row> parRNAwaldec = this.associationsDataset.rowAssociations(options, new HistoriqueExecution(), ANNEE_RNA, COG, true, AssociationWaldecTri.RNA_WALDEC, this.verifications);
      parRNAwaldec.printSchema();
      parRNAwaldec.show(500, false);
      assertNotEquals(0, parRNAwaldec.count(), "Une association au moins aurait du être lue, par RNA Waldec");
      
      if (tousTris()) {
         Dataset<Row> parCodeCommune = this.associationsDataset.rowAssociations(options, new HistoriqueExecution(), ANNEE_RNA, COG, true, AssociationWaldecTri.CODE_COMMUNE, this.verifications);
         parCodeCommune.select("numero_waldec", "codeEPCI", "codeDepartement", "codeCommune", "titre").show(500, false);
         assertNotEquals(0, parCodeCommune.count(), "Une association au moins aurait du être lue, par Code Commune");
   
         Dataset<Row> parEPCI = this.associationsDataset.rowAssociations(options, new HistoriqueExecution(), ANNEE_RNA, COG, true, AssociationWaldecTri.EPCI, this.verifications);
         parEPCI.select("numero_waldec", "codeEPCI", "codeDepartement", "codeCommune", "titre").show(500, false);
         assertNotEquals(0, parEPCI.count(), "Une association au moins aurait du être lue, par code EPCI");
      }
   }

   /**
    * Lecture des associations de Douarnenez.
    */
   @Test
   @DisplayName("Trouver des associations à Douarnenez")
   void associationsDouarnenez() {
      List<Association> associations = this.associationsDataset.obtenirAssociations(ANNEE_RNA, COG, new CodeCommune("29046"));
      assertNotEquals(0, associations.size(), "Une ou des associations auraient du être retournées.");

      associations.stream().limit(10).forEach(association -> LOGGER.info(association.toString()));
   }

   /**
    * Lecture d'associations, sous forme d'objets métiers, liste.
    */
   @Test
   @DisplayName("Associations en liste d'objets métiers")
   void associationListeObjetsMetiers() {
      Dataset<Row> rowAssociations = this.associationsDataset.rowAssociations(null, null, ANNEE_RNA, COG, true, AssociationWaldecTri.RNA_WALDEC, this.verifications)
         .limit(100);

      List<Association> associations = this.associationsDataset.toObjetsMetiersList(rowAssociations);
      assertNotEquals(0, associations.size(), "Un objet métier association au moins aurait du être obtenue");
   }

   /**
    * Lecture d'associations, sous forme d'objets métiers, Stream.
    */
   @Test
   @DisplayName("Associations en flux d'objets métiers")
   void associationStreamObjetsMetiers() {
      Dataset<Row> rowAssociations = this.associationsDataset.rowAssociations(null, null, ANNEE_RNA, COG, true, AssociationWaldecTri.RNA_WALDEC, this.verifications);
      DatasetRowToObjetMetierInterface<Association> rowToObjetMetier = this.associationsDataset;

      this.associationsDataset.paginateur().setNombreObjetsParPageStream(500000);
      Map<String, Integer> hashs = new HashMap<>();

      assertDoesNotThrow(() -> {
         Stream<Association> associations = rowToObjetMetier.toObjetsMetiersStream(rowAssociations);
         Set<String> departementsAvecEpts = Set.of("75", "91", "92", "93", "94", "95");

         // Vérifier qu'on peut bien parcourir toute la liste, et qu'il n'y a pas de doublons
         associations.forEach(association -> {
            // Les associations d'un département qui fait partie d'un établissement public territorial peuvent apparaître deux fois :
            // une fois pour la métropole de Paris, une autre pour leur intercommunalité de type EPT, et on ne peut pas les inclure dans le test
            boolean dansEPT = false;

            if (association.getCodeDepartement() != null && association.getCodeDepartement().numeroDepartement() != null) {
               dansEPT = departementsAvecEpts.contains(association.getCodeDepartement().numeroDepartement());
            } else {
               LOGGER.warn("Une association a un code département invalide : {}", association.getCodeDepartement());
            }

            String numeroWaldec = association.getNumeroWaldec() != null ? association.getNumeroWaldec().getNumero() : null;

            if (numeroWaldec != null && dansEPT == false) {
               Integer valeur = hashs.get(numeroWaldec);

               if (valeur != null && valeur.longValue() == 1) {
                  LOGGER.error("Une association est en doublon sur le numéro waldec {} : {}",  association.getNumeroWaldec(), association);
               }

               hashs.put(numeroWaldec, valeur ==  null ? 1 :  valeur+1);
            }
         });
      });

      // Déterminer le nombre d'associations qui ont été lues plusieurs fois.
      long nombreDepassement = hashs.entrySet().stream().filter(comptage -> comptage.getValue() > 1).count();
      assertEquals(0, nombreDepassement, MessageFormat.format("{0} associations ont été lues plusieurs fois, sur {1} que porte le dataset", nombreDepassement, rowAssociations.count()));
   }

   /**
    * Test que le stream lit les associations dans le bon sens
    */
   @Test
   @DisplayName("Bon sens du stream")
   void bonSens() {
      Dataset<Row> dixAssociations = this.associationsDataset.rowAssociations(null, null, ANNEE_RNA, COG, true, AssociationWaldecTri.RNA_WALDEC, this.verifications)
         .limit(10).orderBy("numero_waldec");

      String[] reference = toWaldecArray(dixAssociations);
      LOGGER.info("La référence du jeu d'essai est cet ordre : {}", (Object)reference);

      // Pagination par des List
      List<Dataset<Association>> associationDatasetsList = Paginateur.paginer(dixAssociations, p -> this.associationsDataset.toDatasetObjetsMetiers(p), 2);
      assertEquals(5, associationDatasetsList.size(), "Il n'y a pas le nombre de pages escompté");

      List<Association> rassembleList = new ArrayList<>();
      associationDatasetsList.forEach(associationDatasetList -> rassembleList.addAll(associationDatasetList.collectAsList()));

      String[] parDivisionEnListe = toWaldecArray(rassembleList);
      assertArrayEquals(reference, parDivisionEnListe, "La division en liste ne respecte pas l'ordre original");

      // Pagination par un stream
      Stream<Association> associationStream = Paginateur.paginerEnStream(dixAssociations, p -> this.associationsDataset.toDatasetObjetsMetiers(p), 2);
      String[] parStream = toWaldecArray(associationStream.toList());
      assertArrayEquals(reference, parStream, "La division en stream ne respecte pas l'ordre original");
   }

   /**
    * Extraire un tableau d'identifiants waldecs des rows du jeu d'essai
    * @param rows Rows
    * @return tableau d'identifiants waldecs.
    */
   private String[] toWaldecArray(Dataset<Row> rows) {
      String[] waldecs = new String[10];
      int index=0;

      for(Row row : rows.select("numero_waldec").collectAsList()) {
         waldecs[index ++] = row.getString(0);
      }

      return waldecs;
   }

   /**
    * Extraire un tableau d'identifiants waldecs des rows du jeu d'essai
    * @param associations Rows
    * @return tableau d'identifiants waldecs.
    */
   private String[] toWaldecArray(List<Association> associations) {
      String[] waldecs = new String[10];
      int index=0;

      for(Association association : associations) {
         waldecs[index ++] = association.getNumeroWaldec().getNumero();
      }

      return waldecs;
   }

   /**
    * Lecture d'associations, sous forme d'objets métiers.
    */
   @Test
   @DisplayName("Row en objet métier")
   void associationObjetMetier() {
      Row rowAssociation = this.associationsDataset.rowAssociations(null, null, ANNEE_RNA, COG, true, AssociationWaldecTri.RNA_WALDEC, this.verifications).first();

      assertDoesNotThrow(() -> {
         Association association = this.associationsDataset.toObjetMetier(rowAssociation);
         LOGGER.info("Association lue  par conversion de Row en objet métier : {}", association);
      });
   }

   /**
    * Présenter la liste des objets sociaux
    */
   @Test
   @DisplayName("Liste des objets sociaux")
   void objetsSociaux() {
      Dataset<Row> objetsSociaux = this.objetsSociauxDataset.rowObjetsSociaux(2023);
      assertNotEquals(0L, objetsSociaux.count(), "Un objet social d'association au moins aurait dû être lu");

      objetsSociaux.show(false);
   }
}
