package fr.ecoemploi.adapters.outbound.spark.dataset.association;

import java.io.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.domain.utils.objets.TechniqueException;

import org.apache.spark.sql.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

/**
 * Dataset des objets sociaux des associations.
 * @author Marc Le Bihan
 */
@Service
public class AssociationObjetsSociauxDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 6225747325084029875L;

   /** Chargeur d'objets sociaux d'associations. */
   @Autowired
   private AssociationObjetsSociauxRowCsvLoader loader;

   /**
    * Obtenir un Dataset Row des objets sociaux des associations.
    * @param annee Année du registre.
    * @return Dataset des objets sociaux des associations.
    * @throws TechniqueException si un incident survient.
    */
   public Dataset<Row> rowObjetsSociaux(int annee) throws TechniqueException {
      return this.loader.loadOpenData(annee);
   }
}
