package fr.ecoemploi.adapters.outbound.spark.dataset.association;

import java.io.*;
import java.util.*;
import java.util.function.Supplier;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.api.java.function.*;
import static org.apache.spark.sql.functions.*;
import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.port.association.AssociationRepository;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.verifications.Verification;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.CogDataset;

import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.association.Association;

/**
 * Dataset du Répertoire National des Associations (RNA), Waldec.
 * @author Marc Le Bihan
 */
@Service
public class AssociationWaldecDataset extends AbstractSparkObjetMetierDataset<Association> implements AssociationRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 8147964646336369586L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AssociationWaldecDataset.class);

   /** Année RNA début. */
   private final int annneWaldecDebut;

   /** Année RNA fin. */
   private final int annneWaldecFin;

   /** Chargeur de données brutes open data */
   private final AssociationWaldecRowCsvLoader rowLoader;

   /** Validateur de Row */
   private final AssociationWaldecRowValidator rowValidator;

   /** Encodeur d'objets métiers depuis un Row. */
   private final AssociationWaldecEncoder objetMetierEncoder = new AssociationWaldecEncoder();

   /** Objets sociaux des associations. */
   private final AssociationObjetsSociauxDataset objetsSociauxDataset;

   /** Dataset du code officiel géographique. */
   private final CogDataset cogDataset;

   /**
    * Construire un générateur de datasets d'associations
    * @param annneWaldecDebut Année RNA début
    * @param annneWaldecFin Année RNA fin
    * @param objetsSociauxDataset Objets sociaux des associations
    * @param cogDataset Dataset du code officiel géographique
    * @param rowLoader Chargeur de données brutes open data
    * @param rowValidator Validateur de Row
    */
   @Autowired
   public AssociationWaldecDataset(@Value("${annee.association.debut}") int annneWaldecDebut, @Value("${annee.association.fin}") int annneWaldecFin,
       AssociationObjetsSociauxDataset objetsSociauxDataset, CogDataset cogDataset,
       AssociationWaldecRowCsvLoader rowLoader, AssociationWaldecRowValidator rowValidator) {
      this.annneWaldecDebut = annneWaldecDebut;
      this.annneWaldecFin = annneWaldecFin;
      this.objetsSociauxDataset = objetsSociauxDataset;
      this.cogDataset = cogDataset;
      this.rowLoader = rowLoader;
      this.rowValidator = rowValidator;
   }

   /**
    * Charger le ou les référentiels des associations.
    * @param anneeRNA Année du Registre National des Associations<br>
    *     0: pour toutes les années disponibles
    * @param anneeCOG Année du Code Officiel Géographique.
    */
   @Override
   public void chargerAssociations(int anneeRNA, int anneeCOG) {
      if (anneeRNA != 0) {
         chargement(anneeRNA, anneeCOG);
      }
      else {
         for (int annee = this.annneWaldecDebut; annee <= this.annneWaldecFin; annee ++) {
            int anneeCommunes = anneeCOG;

            if (Math.abs(anneeCOG) < 10) {
               anneeCommunes = annee + anneeCOG;
            }

            chargement(annee, anneeCommunes);
         }
      }
   }

   /**
    * Charger un référentiel d'associations.
    * @param anneeRNA Année du Registre National des Associations
    * @param anneeCOG Année du Code Officiel Géographique.
    */
   private void chargement(int anneeRNA, int anneeCOG) {
      Arrays.stream(AssociationWaldecTri.values()).forEach(tri -> rowAssociations(null, null, anneeRNA, anneeCOG, true, tri));
   }

   /**
    * Obtenir un Dataset Row du registre national des associations actives.
    * @param anneeRNA Année du registre.
    * @param anneeCOG Année du Code Officiel Géographique.
    * @param actives true si seules les associations actives doivent être retenues.
    * @param tri Tri à sélectionner.
    * @param verifications Vérifications optionnelles à mener.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historique Historique d'exécution.
    * @return Dataset des associations.
    */
   @SuppressWarnings("java:S1192") // Avertissement pour duplication de noms de colonnes
   public Dataset<Row> rowAssociations(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeRNA, int anneeCOG, boolean actives, AssociationWaldecTri tri, @SuppressWarnings("unused") Verification... verifications) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.addReglesValidation(this.session, this.rowValidator);
      }

      Supplier<Dataset<Row>> worker = () -> {
         LOGGER.info("Début de l'acquisition des associations pour l'année {}, cog {}, actives : {}...", anneeRNA, anneeCOG, actives);
         Dataset<Row> associations = this.rowLoader.loadOpenData(anneeRNA, anneeCOG);

         // Ejecter les associations qui n'ont ni nom, ni objet social.
         associations = associations.filter((FilterFunction<Row>)association -> {
            if (this.rowValidator.validerAssociation(historique, association, anneeRNA, anneeCOG) == false) {
               return false;
            }

            // Il peut être demandé que seules les associations actives soient retenues.
            if (actives) {
               return "A".equals(association.getAs("position_activite")) != false;
            }

            return true;
         });

         // Joindre les objets sociaux, en outer.
         Dataset<Row> objetsSociaux = this.objetsSociauxDataset.rowObjetsSociaux(anneeRNA);

         associations = associations.join(objetsSociaux, associations.col("code_objet_social1").equalTo(objetsSociaux.col("code_objet_social")), "outer")
            .withColumnRenamed("libelle_objet_social", "libelle_objet_social1")
            .drop("code_objet_social");

         associations = associations.join(objetsSociaux, associations.col("code_objet_social2").equalTo(objetsSociaux.col("code_objet_social")), "outer")
            .withColumnRenamed("libelle_objet_social", "libelle_objet_social2")
            .drop("code_objet_social");

         // Remplacer les noms de communes par celles officielles.
         Dataset<Row> communes = this.cogDataset.rowCommunes(null, null, anneeCOG, false).select("codeCommune", "nomCommune", "codeRegion", "codeDepartement", "codeEPCI", "nomEPCI", "populationTotale");

         associations = associations.join(communes, associations.col("adresse_siege_code_commune").equalTo(communes.col("codeCommune")), "inner")
            .drop("adresse_siege_code_commune", "adresse_siege_nom_commune");

         // Supprimer les caractères parasites majeurs des noms d'associations, objets sociaux, adresses.
         associations = this.rowValidator.correctionDonneesInvalides(associations, true);
         associations = associations.persist(); // Important : optimise beaucoup.

         // Calculer l'ancienneté d'existence des associations qui ont une date de création.
         Column nombreAnneesExistence = when(col("date_creation").equalTo(null), null).
            otherwise(datediff(current_date(), col("date_creation")).$div(lit(365)));

         associations = associations.withColumn("nombreAnneesExistence", nombreAnneesExistence);

         // Calculer l'ancienneté de la dernière déclaration quand la date est connue.
         Column nombreAnneesDerniereDeclaration = when(col("date_derniere_declaration").equalTo(null), null).
            otherwise(datediff(current_date(), col("date_derniere_declaration")).$div(lit(365)));

         associations = associations.withColumn("nombreAnneesDerniereDeclaration", nombreAnneesDerniereDeclaration);

         return associations;
      };

      Column postFiltre = options.hasRestrictionAuxCommunes() ?
         (CODE_COMMUNE.col().isin(options.restrictionAuxCommunes().toArray())) : null;

      boolean anneeEligibleCache = options.anneeEligibleMiseEnCache(anneeRNA);

      return constitutionStandard(options, historique, worker, anneeEligibleCache, new ConditionPostFiltrageConstante(postFiltre),
         new CacheParqueteur<>(options, this.session, exportCSV(), "associations_RNA", "FILTRE-annee_{0,number,#0}-cog_{1,number,#0}-actives_{2}", tri, anneeRNA, anneeCOG, actives));
   }
   
   /**
    * Enumération des associations présentes sur le territoire.
    * @param codesSociaux Filtrage optionnel sur des codes sociaux.
    * @param anneeRNA Année de recherche des associations dans le Registre National des Associations.
    * @param anneeCOG Année du Code Officiel Géographique.
    * @param actives true si seules les associations actives doivent être retenues.
    * @param tri Tri à sélectionner.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historique Historique d'exécution.
    * @return Associations actives en 2020.
    */
   public Dataset<Row> rowAssociationsThematiques(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, String[] codesSociaux, int anneeRNA, int anneeCOG, boolean actives, AssociationWaldecTri tri) {
      Dataset<Row> associations = rowAssociations(optionsCreationLecture, historique, anneeRNA, anneeCOG, actives, tri);

      if (historique != null) {
         historique.addReglesValidation(this.session, this.rowValidator);
      }

      // Si une liste de codes sociaux est transmise, s'y restreindre.
      if (codesSociaux != null) {
         associations = associations.filter((FilterFunction<Row>)a ->
            // Une association est porteuse de deux codes objet. Il suffit que l'un d'eux corresponde.
            Arrays.stream(codesSociaux).anyMatch(c -> c.equals(a.getAs("code_objet_social1"))) ||
               Arrays.stream(codesSociaux).anyMatch(c -> c.equals(a.getAs("code_objet_social2")))
         );
      }
      
      return associations;
   }

   /**
    * Obtenir la liste des associations d'une commune.
    * @param anneeRNA Année du Registre National des Associations
    * @param anneeCOG Année du Code Officiel Géographique.
    * @param codeCommune Code Commune.
    * @return Liste des associations.
    */
   public List<Association> obtenirAssociations(int anneeRNA, int anneeCOG, CodeCommune codeCommune) {
      Objects.requireNonNull(codeCommune, "Le code commune où rechercher des associations ne peut pas valoir null.");

      LOGGER.info("Obtention des associations présentes en {} pour la commune de code {} du COG {}", anneeRNA, codeCommune, anneeCOG);

      Dataset<Row> rowAssociations = rowAssociations(null, null, anneeRNA, anneeCOG, true, AssociationWaldecTri.CODE_COMMUNE)
         .where(CODE_DEPARTEMENT.col().equalTo(codeCommune.numeroDepartement())
            .and(CODE_COMMUNE.col().equalTo(codeCommune.departementEtCommune())));

      return toObjetsMetiersList(rowAssociations);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected RowToObjetMetierInterface<Association> objetMetierEncoder() {
      return this.objetMetierEncoder;
   }
}
