package fr.ecoemploi.adapters.outbound.spark.dataset.association;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.TriParqueteurIF;

/**
 * Tris disponibles pour les associations Waldec.
 * @author Marc Le Bihan
 */
@SuppressWarnings("java:S1192") // Avertissement pour duplication de noms de colonnes
public enum AssociationWaldecTri implements TriParqueteurIF {
   /** Par code WALDEC (RNA). */
   RNA_WALDEC("_par_rnaWaldec", true, "codeDepartement", "numero_waldec"),

   /** Par code commune, puis WALDEC/RNA. */
   CODE_COMMUNE("_par_codeCommune_rnaWaldec", true, "codeDepartement", "codeCommune", "numero_waldec"),

   /** Par code EPCI, code commune, et WALDEC/RNA. */
   EPCI("_par_epci_codeCommune_rnaWaldec", true, "codeDepartement", "codeEPCI", "codeCommune", "numero_waldec");

   /** Suffixe du fichier trié. */
   private final String suffixeFichier;

   /** Nom de la colonne de partitionnement. */
   private final String nomColonnePartionnement;

   /** Nom des colonnes de tri. */
   private final String[] nomsColonnesTri;

   /** Déterminer s'il faut trier au sein des partitions. */
   private final boolean sortWithinPartition;

   /**
    * Constuire un tri possible.
    * @param suffixeFichier            Suffixe du fichier trié.
    * @param sortWithinPartition       true s'il faut faire un tri au sein des partitions,<br>
    *                                  false si c'est un tri global.
    * @param nomColonnePartitionnement Nom de la colonne de partitionnement, s'il y en a une.
    * @param nomColonnesTri            Nom des colonnes de tri.
    */
   AssociationWaldecTri(String suffixeFichier, boolean sortWithinPartition, String nomColonnePartitionnement, String... nomColonnesTri) {
      this.suffixeFichier = suffixeFichier;
      this.sortWithinPartition = sortWithinPartition;
      this.nomColonnePartionnement = nomColonnePartitionnement;
      this.nomsColonnesTri = nomColonnesTri;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getNomColonnePartionnement() {
      return this.nomColonnePartionnement;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String[] getNomsColonnesTri() {
      return this.nomsColonnesTri;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getSuffixeFichier() {
      return this.suffixeFichier;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean sortWithinPartition() {
      return this.sortWithinPartition;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean partitionByRange() {
      return false;
   }
}
