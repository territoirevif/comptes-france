package fr.ecoemploi.adapters.outbound.spark.dataset.association;

import java.io.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.types.DataTypes.StringType;

/**
 * Chargeur d'objets sociaux depuis un fichier CSV.
 * @author Marc Le Bihan
 */
@Component
public class AssociationObjetsSociauxRowCsvLoader implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -7092700134033623131L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AssociationObjetsSociauxRowCsvLoader.class);

   /** Nom du fichier CSV. */
   @Value("${association-objets-sociaux.fichier.csv.nom}")
   private String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${association.dir}")
   private String repertoireFichier;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Charger le fichier CSV des objets sociaux des associations.
    * @param annee Année du registre.
    * @return Dataset des objets sociaux des associations.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> loadOpenData(int annee) {
      LOGGER.info("Acquisition des objets sociaux d'associations de {}...", annee);

      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, annee, this.nomFichier);

      StructType schema;

      if (annee >= 2022) {
         schema = new StructType()
            .add("objet_social_parent_id", StringType, false)  // Code de l'objet social parent
            .add("objet_social_parent_lib", StringType, false) // Libellé de l'objet social parent
            .add("objet_social_id", StringType, false)         // Code de l'objet social
            .add("objet_social_lib", StringType, false);       // Libellé de l'objet social
      }
      else {
         schema = new StructType()
            .add("code_objet_social", StringType, false)   // Code de l'objet social
            .add("libelle_objet_social", StringType, false); // Libellé de l'objet social
      }

      String delimiter = annee <= 2021 ? "," : ";";

      Dataset<Row> objetsSociaux = this.session.read().schema(schema).format("csv")
         .option("encoding", "UTF-8")
         .option("header","true")
         .option("delimiter", delimiter)
         .option("quote", "\"")
         .option("escape", "\"")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath());

      objetsSociaux = objetsSociaux
         .withColumnRenamed("objet_social_id", "code_objet_social")
         .withColumnRenamed("objet_social_lib", "libelle_objet_social")
         .orderBy(col("code_objet_social"))
         .cache();

      return objetsSociaux.select("code_objet_social", "libelle_objet_social");
   }
}
