package fr.ecoemploi.adapters.outbound.spark.dataset.association;

import java.io.*;
import java.util.*;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.RowToObjetMetierInterface;
import fr.ecoemploi.domain.model.territoire.association.Association;

/**
 * Encodeur de dataset Row en objets métiers Association.
 */
public class AssociationWaldecEncoder implements RowToObjetMetierInterface<Association>, Serializable {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 503364021236948186L;

   /**
    * Convertir un Dataset Row en Dataset d'associations.
    * @param rows Dataset Row à convertir.
    * @return Dataset d'associations.
    */
   public Dataset<Association> toDatasetObjetsMetiers(Dataset<Row> rows) {
      Objects.requireNonNull(rows, "Le dataset des associations ne peut pas valoir null.");

      return rows.map((MapFunction<Row, Association>)this::toObjetMetier, Encoders.bean(Association.class));
   }

   /**
    * Convertir un Row en objet métier.
    * @param r Row.
    * @return Objet métier.
    */
   public Association toObjetMetier(Row r) {
      return new Association(r.getAs("numero_waldec"),
         r.getAs("ancien_numero_waldec"),
         r.getAs("siret"),
         r.getAs("numero_rup"),
         r.getAs("gestion"),

         r.getAs("nature"),
         r.getAs("groupement"),
         r.getAs("titre"),
         r.getAs("titre_court"),
         r.getAs("objet"),

         r.getAs("code_objet_social1"),
         r.getAs("code_objet_social2"),
         r.getAs("nom_declarant"),
         r.getAs("civilite_declarant"),
         r.getAs("site_web"),

         r.getAs("autorisation_publication_web"),
         r.getAs("observation"),
         r.getAs("position_activite"),
         r.getAs("maj_time"),
         r.getAs("libelle_objet_social1"),

         r.getAs("libelle_objet_social2"),
         r.getAs("codeCommune"),
         r.getAs("nomCommune"),
         r.getAs("codeRegion"),
         r.getAs("codeDepartement"),

         r.getAs("codeEPCI"),
         r.getAs("nomEPCI"),
         r.getAs("populationTotale"),
         r.getAs("date_creation"),
         r.getAs("date_derniere_declaration"),

         r.getAs("date_publication_JO_creation"),
         r.getAs("date_dissolution"),
         r.getAs("nombreAnneesExistence"),
         r.getAs("nombreAnneesDerniereDeclaration"),
         r.getAs("adresse_siege_complement"),

         r.getAs("adresse_siege_numero_voie"),
         r.getAs("adresse_siege_type_voie"),
         r.getAs("adresse_siege_libelle_voie"),
         r.getAs("adresse_siege_distribution"),
         r.getAs("adresse_siege_code_postal"),

         r.getAs("adresse_gestion_complement_association"),
         r.getAs("adresse_gestion_complement_geo"),
         r.getAs("adresse_gestion_libelle_voie"),
         r.getAs("adresse_gestion_distribution_facturation"),
         r.getAs("adresse_gestion_code_postal"),

         r.getAs("adresse_gestion_achemine"),
         r.getAs("adresse_gestion_pays"));
   }
}
