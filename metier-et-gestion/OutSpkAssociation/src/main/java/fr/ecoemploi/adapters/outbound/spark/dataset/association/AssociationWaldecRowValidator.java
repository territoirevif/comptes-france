package fr.ecoemploi.adapters.outbound.spark.dataset.association;

import java.io.*;
import java.text.*;
import java.time.*;
import java.util.*;
import java.util.function.BooleanSupplier;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.*;
import org.springframework.beans.BeansException;
import org.springframework.context.*;
import org.springframework.stereotype.Component;

import org.apache.spark.api.java.function.MapPartitionsFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.*;
import org.apache.spark.sql.types.StructType;
import static org.apache.spark.sql.types.DataTypes.DateType;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.HistoriqueExecution;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.validation.AbstractRowValidator;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Validateur d'enregistrements Row durant le chargement du dataset des associations
 */
@Component
public class AssociationWaldecRowValidator extends AbstractRowValidator implements ApplicationContextAware {
   /** Serialization UUID. */
   @Serial
   private static final long serialVersionUID = 778519243454957819L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AssociationWaldecRowValidator.class);

   /** Une association n'a pas d'objet */
   private static final String VLD_ASSOCIATION_SANS_OBJET = "ASSOCIATION_SANS_OBJET";

   /** Une association n'a pas d'identifant waldec */
   private static final String VLD_ASSOCIATION_SANS_WALDEC = "ASSOCIATION_SANS_WALDEC";

   /**
    * Valider une association
    * @param historique Historique d'exécution
    * @param association Association à valider
    * @param anneeRNA Année RNA
    * @param anneeCOG Année du COG
    * @return true si l'association est valide
    */
   public boolean validerAssociation(HistoriqueExecution historique, Row association, int anneeRNA, int anneeCOG) {
      List<BooleanSupplier> validations = new ArrayList<>();

      String titre = TITRE_ASSOCIATION.getAs(association);
      String objetSocial = OBJET_SOCIAL_ASSOCIATION.getAs(association);
      String numeroWaldec = NUMERO_WALDEC_ASSOCIATION.getAs(association);

      // Le nom comme le thème social doivent être alimentés
      validations.add(() -> invalideSi(VLD_ASSOCIATION_SANS_OBJET, historique, association,
         p -> titre == null &&  objetSocial == null,
         () -> new Serializable[] {numeroWaldec, association.getAs("adresse_siege_nom_commune"), association.getAs("adresse_siege_code_commune")}));


      // L'association doit avoir un identifiant waldec
      validations.add(() -> invalideSi(VLD_ASSOCIATION_SANS_WALDEC, historique, association,
         p -> StringUtils.isBlank(numeroWaldec) || numeroWaldec.length() != 10,
         () -> new Serializable[] {association.getAs("adresse_siege_nom_commune"), association.getAs("adresse_siege_code_commune")}));

      return validations.stream().allMatch(BooleanSupplier::getAsBoolean);
   }

   /**
    * Supprimer les caractères parasites et corriger les dates invalides.
    * @param associations Dataset d'associations.
    * @param guillementsEnApostrophes s'il faut transformer aussi les guillements en apostrophes (pour fichiers CSV aux chaînes de caractères délimités par des guillements).
    * @return Tabulations, " transformés en '.
    */
   Dataset<Row> correctionDonneesInvalides(Dataset<Row> associations, boolean guillementsEnApostrophes) {
      Dataset<Row> cible = associations;

      StructType schema = cible.schema();

      // Les champs textes verront leurs caractères parasites supprimés.
      String[] nomChampsTexte = {
         "titre", "objet", "adresse_siege_libelle_voie", "adresse_gestion_libelle_voie", "adresse_siege_complement",
         "adresse_gestion_complement_association", "adresse_gestion_complement_geo", "adresse_siege_numero_voie", "adresse_siege_distribution",
         "adresse_siege_type_voie", "adresse_gestion_distribution_facturation", "adresse_siege_code_postal", "adresse_gestion_code_postal"
      };

      int[] indexChampsTexte = new int[nomChampsTexte.length];

      for(int index=0; index < nomChampsTexte.length; index ++) {
         indexChampsTexte[index] = schema.fieldIndex(nomChampsTexte[index]);
      }

      // Les champs date au format texte seront convertis en dates, et s'ils ne le peuvent pas, seront corrigés.
      String[] nomChampsDates = {"date_creation", "date_derniere_declaration", "date_publication_JO_creation", "date_dissolution"};

      int[] indexChampsDate = new int[nomChampsDates.length];

      for(int index=0; index < indexChampsDate.length; index ++) {
         indexChampsDate[index] = schema.fieldIndex(nomChampsDates[index] + "_toCheck"); // Les champs à vérifier sont suffixés par _toCheck.
         schema = schema.add(nomChampsDates[index], DateType);
      }

      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      LocalDate dateLimiteInferieure;

      try {
         dateLimiteInferieure = Instant.ofEpochMilli(format.parse("1901-01-01").getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
      }
      catch (ParseException ex) {
         // Inattendu.
         throw new RuntimeException(ex.getMessage(), ex);
      }

      LocalDate dateLimiteSuperieure = LocalDate.now();

      ExpressionEncoder<Row> encoder = ExpressionEncoder.apply(schema);

      cible = cible.mapPartitions((MapPartitionsFunction<Row, Row>) it -> {
         List<Row> rows = new LinkedList<>();

         while(it.hasNext()) {
            Row row = it.next();

            // Copier tous les champs du row dans une liste.
            List<Object> valeurs = new ArrayList<>();

            for(int indexColonne=0; indexColonne < row.size(); indexColonne ++) {
               valeurs.add(row.get(indexColonne));
            }

            // Corriger les champs textes en enlevant leurs caractères parasites et en restituant leurs sauts de ligne,
            // et corriger les dates invalides.
            correctionChampsTexte(nomChampsTexte, indexChampsTexte, valeurs,guillementsEnApostrophes);
            correctionDatesInvalides(nomChampsDates, indexChampsDate, valeurs, dateLimiteInferieure, dateLimiteSuperieure, format);

            rows.add(RowFactory.create(valeurs.toArray()));
         }

         return rows.iterator();
      }, encoder);

      return cible.drop("date_creation_toCheck", "date_derniere_declaration_toCheck", "date_publication_JO_creation_toCheck", "date_dissolution_toCheck");
   }

   /**
    * Correction d'une série de champ texte (descriptifs) dans un enregistrement, pour en enlever les caractères parasites et rétablir les sauts de ligne.
    * @param nomChampsTexte Nom des champs textes à rectifier dans l'enregistrement.
    * @param indexChampsTexte Index des champs texte.
    * @param valeurs Valeurs des champs.
    * @param guillementsEnApostrophes true, s'il faut convertir les guillemets en apostrophes.
    */
   void correctionChampsTexte(String[] nomChampsTexte, int[] indexChampsTexte, List<Object> valeurs, boolean guillementsEnApostrophes) {
      for(int indexControle=0; indexControle < nomChampsTexte.length; indexControle ++) {
         int indexColonne = indexChampsTexte[indexControle];
         String candidat = (String)valeurs.get(indexColonne); // L'on a mis que des String dans la liste des champs.

         String correction;

         if (candidat == null) {
            correction = null;
         }
         else {
            correction = candidat.trim();
            correction = correction.replace('\t', ' ')
               .replace("«", "\"")
               .replace("»", "\"")
               .replace("“", "\"")
               .replace("”", "\"")
               .replace("’", "'")
               .replace("\u0092", "'");

            // Il peut être demandé (pour les exports CSV) de remplacer les guillemets par des apostrophes.
            if (guillementsEnApostrophes) {
               correction = correction.replace( '\"', '\'');
            }

            // Les points virgules sont à remplacer par des sauts de ligne, dans l'objet social.
            if (nomChampsTexte[indexControle].equals("objet")) {
               correction = correction.replace(';', '\n');
            }
         }

         valeurs.set(indexColonne, correction);
      }
   }

   /**
    * Corriger une série de dates invalides.
    * @param nomChampsDates Nom des champs date à corriger.
    * @param indexChampsDate Index des champs date.
    * @param valeurs Valeurs des champs.
    * @param dateLimiteInferieure Date limite inférieure.
    * @param dateLimiteSuperieure Date limite supérieure.
    * @param format Format de la date.
    */
   private void correctionDatesInvalides(String[] nomChampsDates, int[] indexChampsDate, List<Object> valeurs, LocalDate dateLimiteInferieure, LocalDate dateLimiteSuperieure, SimpleDateFormat format) {
      for(int indexControle=0; indexControle < nomChampsDates.length; indexControle ++) {
         int indexColonne = indexChampsDate[indexControle];
         String dateCandidate = (String)valeurs.get(indexColonne); // L'on a mis que des String dans la liste des champs.

         correctionDateInvalide(dateCandidate, valeurs, dateLimiteInferieure, dateLimiteSuperieure, format);
      }
   }

   /**
    * Corriger une date invalide.
    * @param dateCandidate Date candidate au contrôle.
    * @param dateLimiteInferieure Date limite inférieure.
    * @param dateLimiteSuperieure Date limite supérieure.
    * @param format Format de la date.
    */
   private void correctionDateInvalide(String dateCandidate, List<Object> valeurs, LocalDate dateLimiteInferieure, LocalDate dateLimiteSuperieure, SimpleDateFormat format) {
      if (dateCandidate == null)
      {
         valeurs.add(null);
         return;
      }

      LocalDate dateCorrigee;

      if (dateCandidate.length() < 10) {
         LOGGER.warn("Une date invalide, {}, a été renvoyée en 1901-01-01.", dateCandidate);
         dateCorrigee = dateLimiteInferieure;
      }
      else {
         try {
            dateCorrigee = Instant.ofEpochMilli(format.parse(dateCandidate).getTime()).atZone(ZoneId.systemDefault()).toLocalDate();

            if (dateCorrigee.isBefore(dateLimiteInferieure)) {
               dateCorrigee = dateLimiteInferieure;
            }
            else {
               if (dateCorrigee.isAfter(dateLimiteSuperieure)) {
                  dateCorrigee = dateLimiteSuperieure;
               }
            }
         }
         catch(@SuppressWarnings("unused") ParseException e) {
            LOGGER.warn("Une date invalide, {}, a été renvoyée en 1901-01-01.", dateCandidate);
            dateCorrigee = dateLimiteInferieure;
         }
      }

      valeurs.add(dateCorrigee);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
      declareRegle(VLD_ASSOCIATION_SANS_OBJET, this.messageSource,
         "Une association n'a pas d'objet social", String.class, String.class , String.class);

      declareRegle(VLD_ASSOCIATION_SANS_WALDEC, this.messageSource,
         "Une assocition n'a pas de code Waldec", String.class, String.class);
   }
}
