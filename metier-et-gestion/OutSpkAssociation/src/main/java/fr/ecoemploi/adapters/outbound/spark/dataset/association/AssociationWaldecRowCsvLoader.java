package fr.ecoemploi.adapters.outbound.spark.dataset.association;

import java.io.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;

import static org.apache.spark.sql.types.DataTypes.*;

/**
 * Chargeur de données open data de département.
 * @author Marc Le Bihan
 */
@Component
public class AssociationWaldecRowCsvLoader implements Serializable {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -6048398085034288640L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AssociationWaldecRowCsvLoader.class);

   /** Nom du fichier CSV. */
   @Value("${association-rna-waldec.fichier.csv.nom}")
   private String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${association.dir}")
   private String repertoireFichier;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Lire et renommer les champs du CSV des associations.
    * @param anneeRNA Année du registre.
    * @param anneeCOG Année du Code Officiel Géographique.
    * @return Dataset des associations.
    */
   @SuppressWarnings("java:S1192") // Avertissement pour duplication de noms de colonnes
   public Dataset<Row> loadOpenData(int anneeRNA, int anneeCOG) {
      LOGGER.info("Lecture du fichier CSV des associations inscrites au registre national de {} et année COG {}...", anneeRNA, anneeCOG);

      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, anneeRNA, this.nomFichier);

      StructType schema = new StructType()
         .add("id", StringType, true)             // Numéro Waldec national unique de l’association
         .add("id_ex", StringType, true)           // Ancien numéro de l’association
         .add("siret", StringType, true)           // N° siret (facultatif)
         .add("rup_mi", StringType, true)         // N° de RUP attribué par le Ministère
         .add("gestion", StringType, true)        // Code du site gestionnaire de l’association
         .add("date_creat", StringType, true)     // Date de déclaration de création (date de dépôt du dossier en préfecture)
         .add("date_decla", StringType, true)      // Date de la dernière déclaration
         .add("date_publi", StringType, true)      // Date de publication JO de l’avis de création
         .add("date_disso", StringType, true)      // Date de déclaration de dissolution de l’association
         .add("nature", StringType, true)         // Simplement déclarée 1901 ou autre
         .add("groupement", StringType, true)     // Simple ou union ou fédération (S, U, F)
         .add("titre", StringType, true)          // Titre
         .add("titre_court", StringType, true)    // Titre court
         .add("objet", StringType, true)          // Texte descriptif de l'association
         .add("objet_social1", StringType, true)  // Code obligatoire dans la nomenclature nationale
         .add("objet_social2", StringType, true)   // Code secondaire, facultatif dans la nomenclature nationale
         .add("adrs_complement", StringType, true) // Adresse du siège de l’association
         .add("adrs_numvoie", StringType, true)
         .add("adrs_repetition", StringType, true) // (B, T, Q) n’est plus utlisé : regroupé avec adrs_numvoie
         .add("adrs_typevoie", StringType, true)
         .add("adrs_libvoie", StringType, true)
         .add("adrs_distrib", StringType, true)    // Adresse de facturation
         .add("adrs_codeinsee", StringType, true)
         .add("adrs_codepostal", StringType, true)
         .add("adrs_libcommune", StringType, true)
         .add("adrg_declarant", StringType, true)  // Nom patronymique du déclarant (présence temporaire)
         .add("adrg_complemid", StringType, true)  // Adresse de gestion de l’association
         .add("adrg_complemgeo", StringType, true)
         .add("adrg_libvoie", StringType, true)
         .add("adrg_distrib", StringType, true)
         .add("adrg_codepostal", StringType, true)
         .add("adrg_achemine", StringType, true)
         .add("adrg_pays", StringType, true)
         .add("dir_civilite", StringType, true)    // Code de la civilié du dirigeant principal – seule information d’ordre patronymique
         .add("siteweb", StringType, true)
         .add("publiweb", StringType, true)        // Indicateur d’autorisation de publication sur le WEB
         .add("observation", StringType, true)
         .add("position", StringType, true)       // Position d’activité de l’association (A = Active, D = Dissoute, S = Supprimée)
         .add("maj_time", TimestampType, true);   // Date de mise à jour de l’article

      Dataset<Row> associations = this.session.read().schema(schema).format("csv")
         .option("encoding", "ISO-8859-1")
         .option("header","true")
         .option("delimiter", ";")
         .option("quote", "\"")
         .option("escape", "\"")
         .option("multiLine",true)
         .option("enforceSchema", false)
         .load(source.getAbsolutePath())
         .selectExpr("*");

      associations = associations.withColumnRenamed("id", "numero_waldec")
         .withColumnRenamed("id_ex", "ancien_numero_waldec")
         .withColumnRenamed("rup_mi", "numero_rup")
         .withColumnRenamed("date_creat", "date_creation_toCheck")
         .withColumnRenamed("date_decla", "date_derniere_declaration_toCheck")
         .withColumnRenamed("date_publi", "date_publication_JO_creation_toCheck")
         .withColumnRenamed("date_disso", "date_dissolution_toCheck")
         .withColumnRenamed("objet_social1", "code_objet_social1")
         .withColumnRenamed("objet_social2", "code_objet_social2")
         .withColumnRenamed("adrs_complement", "adresse_siege_complement")
         .withColumnRenamed("adrs_numvoie", "adresse_siege_numero_voie")
         .withColumnRenamed("adrs_repetition", "obsolete_adresse_siege_repetition")
         .withColumnRenamed("adrs_typevoie", "adresse_siege_type_voie")
         .withColumnRenamed("adrs_libvoie", "adresse_siege_libelle_voie")
         .withColumnRenamed("adrs_distrib", "adresse_siege_distribution")
         .withColumnRenamed("adrs_codeinsee", "adresse_siege_code_commune")
         .withColumnRenamed("adrs_codepostal", "adresse_siege_code_postal")
         .withColumnRenamed("adrs_libcommune", "adresse_siege_nom_commune")
         .withColumnRenamed("adrg_declarant", "nom_declarant")
         .withColumnRenamed("adrg_complemid", "adresse_gestion_complement_association")
         .withColumnRenamed("adrg_complemgeo", "adresse_gestion_complement_geo")
         .withColumnRenamed("adrg_libvoie", "adresse_gestion_libelle_voie")
         .withColumnRenamed("adrg_distrib", "adresse_gestion_distribution_facturation")
         .withColumnRenamed("adrg_codepostal", "adresse_gestion_code_postal")
         .withColumnRenamed("adrg_achemine", "adresse_gestion_achemine")
         .withColumnRenamed("adrg_pays", "adresse_gestion_pays")
         .withColumnRenamed("dir_civilite", "civilite_declarant")
         .withColumnRenamed("siteweb", "site_web")
         .withColumnRenamed("publiweb", "autorisation_publication_web")
         .withColumnRenamed("position", "position_activite");

      return associations;
   }
}
