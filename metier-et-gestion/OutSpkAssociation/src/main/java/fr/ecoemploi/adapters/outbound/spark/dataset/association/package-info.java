/**
 * Associations (Répertoire National des Assocations, Journal Officiel des Associations)
 * @author Marc Le Bihan
 */
package fr.ecoemploi.adapters.outbound.spark.dataset.association;
