package fr.ecoemploi.adapters.outbound.spark.dataset.datagouv;

import java.io.*;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import fr.ecoemploi.adapters.outbound.port.catalogue.CatalogueJeuxDonneesRepository;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.domain.model.catalogue.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;

/**
 * Dataset d'acquisition du catalogue des données open data de data.gouv.fr.
 * Pour chaque jeu de données, il est notamment indiqué son numéro d’identification, son titre, son url,
 * son rattachement à une organisation (quand il existe), sa description, sa fréquence de mise à jour, sa licence associée,
 * sa couverture temporelle et spatiale, ses dates de création et de dernière mise à jour, son statut de publication (privé ou public),
 * ses tags attribués et quelques indicateurs d’audience.
 * @author Marc Le Bihan
 */
@Service
@Qualifier("spark")
public class CatalogueDatagouvJeuxDeDonneesDataset extends AbstractSparkObjetMetierDataset<JeuDeDonnees> implements CatalogueJeuxDonneesRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 6924916870105216688L;

   /** Encodeur de Row en JeuDeDonnees. */
   private final CatalogueDatagouvJeuxDeDonneesEncoder encoder = new CatalogueDatagouvJeuxDeDonneesEncoder();

   /** Chargeur de données open data. */
   @Autowired
   private CatalogueDatagouvJeuxDeDonneesCsvLoader catalogueDatagouvCsvLoader;

   /*
    La liste des ressources publiées sur data.gouv.fr : pour chaque ressource publiée, il est notamment indiqué son rattachement à un jeu de données, son titre, sa description, son url, son type, son format, sa taille, ses dates de création et de dernière mise à jour et son nombre de téléchargement.
    La liste des réutilisations publiées sur data.gouv.fr : pour chaque réutilisation publiée, il est notamment indiqué son numéro d’identification, son titre, son url, son type, sa description, son rattachement à une organisation, ses dates de création et de dernière mise à jour, ses tags attribués, son rattachement au jeu de données réutilisé et quelques indicateurs d’audience.
    La liste des organisations créées sur data.gouv.fr : pour chaque organisation créée, il est notamment indiqué son identification, son nom, son url, sa description, son logo, ses dates de création et de dernière modification et quelques indicateurs d’audience.
    La liste des tags créés sur data.gouv.fr : pour chaque tag créé est indiqué le nombre de fois où il a été attribué à un jeu de données et à une réutilisation.
    La liste des discussions ouvertes sur data.gouv.fr : pour chaque discussion ouverte, il est notamment indiqué son numéro d’identification, le nom de son utilisateur lié, son titre, son nombre de messages, le contenu de ses messages, ses dates de création et de fermeture.
    La liste des moissoneurs sur data.gouv.fr : pour chaque moissoneur, il est notamment indiqué son status (validé ou en attente), le nom de l'organisation et la technologie concernée.

      Pour les suivants
      @Value("${datagouv.fichier.harvest.csv}") String fichierHarvest,
      @Value("${datagouv.fichier.organization.csv}") String fichierOrganization,
      @Value("${datagouv.fichier.resource.csv}") String fichierResource,
      @Value("${datagouv.fichier.reuse.csv}") String fichierReuse,
      @Value("${datagouv.fichier.link.csv}") String fichierLink,
      @Value("${datagouv.fichier.tags.csv}") String fichierTags,
    */

   /**
    * Charger les jeux de données.
    */
   @Override
   public void chargerJeuxDeDonneesDatagouv() {
      rowCatalogueDataset(null, new HistoriqueExecution());
   }

   /**
    * Charger la partie Dataset du catalogue.
    * @param optionsCreationLecture Options de lecture et de création de dataset.
    * @param historique Historique d'exécution
    * @return Sous-composant Ressource du Catalogue.
    */
   public Dataset<Row> rowCatalogueDataset(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.aucuneDeRegleValidation(this.session);
      }

      Supplier<Dataset<Row>> worker = () -> this.catalogueDatagouvCsvLoader.loadOpenData();

      return constitutionStandard(options, historique, worker,
         new CacheParqueteur<>(options, this.session, this.exportCSV(),
            "catalogue_dataset", "FILTRE-bruts", new CatalogueTriIdJeuDeDonnees()));
   }

   /**
    * Renvoyer la partie Dataset du catalogue sous la forme d'un objet métier.
    * @param optionsCreationLecture Options de lecture et de création de dataset.
    * @param historique Historique d'exécution
    * @return Dataset de jeux de données.
    */
   public Dataset<JeuDeDonnees> catalogueDataset(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique) {
      return catalogueDataset(rowCatalogueDataset(optionsCreationLecture, historique));
   }

   /**
    * Renvoyer la partie Dataset du catalogue sous la forme d'un objet métier.
    * @param datasets Dataset Row des datasets de data.gouv.fr.
    * @return Dataset de jeux de données.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<JeuDeDonnees> catalogueDataset(Dataset<Row> datasets) {
      Objects.requireNonNull(datasets, "Le Dataset en entrée ne peut pas valoir null.");

      return datasets.map((MapFunction<Row, JeuDeDonnees>)this::toObjetMetier, Encoders.bean(JeuDeDonnees.class));
   }

   /**
    * Obtenir les ressources liés à leurs jeux de données.
    * @param dsJeuxDeDonnees Dataset de jeux de données.
    * @param dsRessources Dataset de ressources.
    * @return Jeux de données déclinées avec leur ressources.
    */
   public JeuxDeDonnees jeuxDeDonnees(Dataset<JeuDeDonnees> dsJeuxDeDonnees, Dataset<Ressource> dsRessources) {
      return super.declinaison(new JeuxDeDonnees(),
         dsJeuxDeDonnees, dsJeuxDeDonnees.col("id"), JeuDeDonnees::getId,
         dsRessources, dsRessources.col("catalogueId"), Ressource::getId,
         JeuDeDonnees::getRessources);
   }

   /**
    * Enumérer les valeurs distinctes d'un champ du catalogue datagouv.<br>
    * "frequence_catalogue", "licence_catalogue", "granularite_spatiale_catalogue", "zones_spatiales_catalogue", "tags_catalogue"<br>
    * ont des valeurs énumérées.
    * @param champ nom du champ dont ont veut l'énumération des valeurs distinctes.
    * @return Map de valeurs que peut prendre ce champ et le nombre d'occurrences qu'on lui a trouvées.<br>
    *    - les valeurs sont converties en chaînes de caractères
    *    - les valeurs sont classés par nombre d'occurences décroissantes.
    */
   public Map<String, Long> enumererValeursDistinctes(String champ) {
      Objects.requireNonNull(champ, "Le nom du champ, dont on veut énumérer les valeurs distinctes dans le catalogue datagouv, ne peut pas valoir null");

      // Faire un distinct avec count sur le champ demandé
      RelationalGroupedDataset group = rowCatalogueDataset(null, null).groupBy(col(champ));
      Dataset<Row> resume = group.agg(count(champ).as("nombre"));

      return enumerationValeursDistinctes(resume.selectExpr(champ, "nombre").collectAsList(), champ);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public List<JeuDeDonneesElastic> recherche(String searchText, String field) {
      throw new UnsupportedOperationException("recherche (Elastic) n'est pas admise sur un Catalogue de jeux de données Spark");
   }

   /**
    * Retourner sous forme de Map (valeur, nombre d'occurrences) le résultat.
    * @param comptages Liste des Row des valeurs distinctes obtenues du dataset catalogue par Spark
    * @param champ Nom du champ demandé
    */
   private Map<String, Long> enumerationValeursDistinctes(List<Row> comptages, String champ) {
      Map<String, Long> valeurs = new LinkedHashMap<>();

      comptages.forEach(comptage -> {
         Object valeurDistincteObjet = comptage.get(0); // La valeur énumérée n'est pas toujours un String
         String valeurDistincteString = valeurDistincteObjet != null ? valeurDistincteObjet.toString() : "null";

         // Le champ tags_catalogue doit être décomposé.
         if ("tags_catalogue".equals(champ)) {
            String[] tags = valeurDistincteString.split(",");

            for(String tag : tags) {
               valeurs.compute(tag, (k, value) -> (value == null) ? 1 : value + 1);
            }
         }
         else {
            valeurs.put(valeurDistincteString, comptage.getLong(1));
         }
      });

      Comparator<? super Map.Entry<String, Long>> triDecroissant = Map.Entry.comparingByValue((x, y) -> - Long.compare(x, y));

      return valeurs.entrySet()
         .stream()
         .sorted(triDecroissant)

         .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
            (oldValue, newValue) -> oldValue, LinkedHashMap::new));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected RowToObjetMetierInterface<JeuDeDonnees> objetMetierEncoder() {
      return this.encoder;
   }
}
