package fr.ecoemploi.adapters.outbound.spark.dataset.datagouv;

import java.io.Serial;

import org.apache.spark.sql.Column;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.TriDataset;

import static org.apache.spark.sql.functions.col;

/**
 * Tri de dataset : tri par id jeu de données
 * @author Marc Le Bihan
 */
public class CatalogueTriIdJeuDeDonnees extends TriDataset {
   @Serial
   private static final long serialVersionUID = 320878343919497233L;

   /**
    * Constuire un tri.
    */
   public CatalogueTriIdJeuDeDonnees() {
      super("TRI-id_jeu_de_donnees", false, null, false,
         "id_catalogue");
   }

   /**
    * Renvoyer une condition equalTo sur l'identifiant.
    * @param id Identifiant. Si null, isNull() servira au test.
    * @return Colonne contenant la condition.
    */
   public Column equalTo(String id) {
      return id != null ? col("id_catalogue").equalTo(id) : col("id_catalogue").isNull();
   }
}
