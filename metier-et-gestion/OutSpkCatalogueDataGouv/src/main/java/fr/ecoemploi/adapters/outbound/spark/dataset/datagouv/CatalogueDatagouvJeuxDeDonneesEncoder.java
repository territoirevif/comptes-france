package fr.ecoemploi.adapters.outbound.spark.dataset.datagouv;

import java.io.*;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.slf4j.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.RowToObjetMetierInterface;
import fr.ecoemploi.domain.model.catalogue.*;

/**
 * Conversion d'un Row en objet métier JeuDeDonnees.
 */
public class CatalogueDatagouvJeuxDeDonneesEncoder implements RowToObjetMetierInterface<JeuDeDonnees>, Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -1493696977461940253L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CatalogueDatagouvJeuxDeDonneesEncoder.class);

   /**
    * {@inheritDoc}
    */
   @Override
   public Dataset<JeuDeDonnees> toDatasetObjetsMetiers(Dataset<Row> rows) {
      return rows.map((MapFunction<Row, JeuDeDonnees>)this::toObjetMetier, Encoders.bean(JeuDeDonnees.class));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public JeuDeDonnees toObjetMetier(Row row) {
      try {
         String catId = row.getAs("id_catalogue");
         CatalogueId catalogueId = catId != null ? new CatalogueId(catId) : null;

         String titre = row.getAs("titre_catalogue");
         String slug = row.getAs("slug_catalogue");
         String acronyme = row.getAs("acronyme_catalogue");
         String url = row.getAs("url_catalogue");

         String orgId = row.getAs("id_organisation_catalogue");
         OrganisationId organisationId = orgId != null ? new OrganisationId(orgId) : null;

         String organisation = row.getAs("organisation_catalogue");
         String description = row.getAs("description_catalogue");
         String frequence = row.getAs("frequence_catalogue");
         String licence = row.getAs("licence_catalogue");

         LocalDate dateDebut = row.getAs("date_debut_couverture_catalogue");
         LocalDate dateFin = row.getAs("date_fin_couverture_catalogue");
         String granulariteSpatiale = row.getAs("granularite_spatiale_catalogue");
         String zonesSpatiales = row.getAs("zones_spatiales_catalogue");

         Boolean featured = row.getAs("featured_catalogue");
         LocalDate dateCreation = row.getAs("date_creation_catalogue");
         LocalDate dateModification = row.getAs("date_modification_catalogue");

         // Décliner le champ tags en ses éléments
         String tags = row.getAs("tags_catalogue");

         Set<String> etiquettes = new HashSet<>();

         if (StringUtils.isBlank(tags) == false) {
            etiquettes.addAll(Arrays.asList(tags.split(",")));
         }

         Boolean archive = row.getAs("archive_catalogue");

         Integer nombreMoissonneurs = row.getAs("nombre_moissonneurs_catalogue");
         String moissonneurDomaine = row.getAs("moissonneur_domaine_catalogue");
         LocalDate moissonneurDateCreation = row.getAs("moissonneur_date_creation_catalogue");
         LocalDate moissonneurDateModification = row.getAs("moissonneur_date_modification_catalogue");
         Double scoreQualite = row.getAs("score_qualite_catalogue");

         Integer nombreDeRessources = row.getAs("nombre_de_ressources_catalogue");
         Integer metriqueDiscussions = row.getAs("metrique_discussions_catalogue");
         Integer metriqueReutilisation = row.getAs("metrique_reutilisation_catalogue");
         Integer metriqueSuiveurs = row.getAs("metrique_suiveurs_catalogue");
         Integer metriqueVues = row.getAs("metrique_vues_catalogue");

         return new JeuDeDonnees(catalogueId, titre, slug, acronyme, url,
            organisationId, organisation, description, frequence, licence,
            dateDebut, dateFin, granulariteSpatiale, zonesSpatiales,
            featured, dateCreation, dateModification, etiquettes, archive,
            nombreMoissonneurs, moissonneurDomaine, moissonneurDateCreation, moissonneurDateModification, scoreQualite,
            nombreDeRessources, metriqueDiscussions, metriqueReutilisation, metriqueSuiveurs, metriqueVues);
      }
      catch(RuntimeException e) {
         String format = "Un row durant le mapCatalogueDataset(..) a provoqué un incident : {0}. Contenu du row : \n{1}";
         String message = MessageFormat.format(format, e.getMessage(), row.mkString());
         RuntimeException ex = new RuntimeException(message, e);

         LOGGER.error(ex.getMessage(), ex);
         throw ex;
      }
   }
}
