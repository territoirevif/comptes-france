package fr.ecoemploi.adapters.outbound.spark.dataset.datagouv;

import java.io.Serial;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;

import org.springframework.stereotype.*;

/**
 * Service d'acquisition du catalogue des données open data de data.gouv.fr
 * @author Marc Le Bihan
 */
@Service
public class CatalogueDatagouvService extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 6612968871828427791L;

   /**
    * Construire un service d'accès aux catalogues des données open data de data.gouv.fr.
    */
   public CatalogueDatagouvService() {
   }
}
