package fr.ecoemploi.adapters.outbound.spark.dataset.datagouv;

import java.io.Serial;
import java.util.Objects;
import java.util.function.Supplier;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

import fr.ecoemploi.adapters.outbound.port.catalogue.CatalogueRessourcesRepository;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.domain.model.catalogue.*;

/**
 * Ressources simplifiées (sans données parentes du jeu de données (dataset)) du catalogue open data (data.gouv.fr) pour un jeu de données particulier.
 * @author Marc Le Bihan
 */
@Service
@Qualifier("spark")
public class CatalogueDatagouvRessourceDataset extends AbstractSparkObjetMetierDataset<Ressource> implements CatalogueRessourcesRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 2317708338284310256L;

   /** Encodeur de Row en Ressource. */
   private final CatalogueDatagouvRessourceEncoder encoder = new CatalogueDatagouvRessourceEncoder();

   /** Chargeur de données open data. */
   @Autowired
   private CatalogueDatagouvRessourcesCsvLoader catalogueRessourcesCsvLoader;

   /**
    * Charger les ressources associées aux jeux de données de Datagouv.fr
    */
   @Override
   public void chargerRessourcesDatagouv() {
      rowCatalogueRessources(null, new HistoriqueExecution());
   }

   /**
    * Charger la partie Ressources du catalogue.
    * @param optionsCreationLecture Options de lecture et de création de dataset.
    * @param historique Historique de création du dataset
    * @return Sous-composant Ressource du Catalogue.
    */
   public Dataset<Row> rowCatalogueRessources(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.aucuneDeRegleValidation(this.session);
      }

      Supplier<Dataset<Row>> worker = () -> this.catalogueRessourcesCsvLoader.loadOpenData();

      return constitutionStandard(options, historique, worker,
         new CacheParqueteur<>(options, this.session, exportCSV(),
            "catalogue_ressource", "FILTRE-bruts", new CatalogueTriIdRessource()));
   }

   /**
    * Renvoyer la partie Dataset du catalogue sous la forme d'un objet métier.
    * @param optionsCreationLecture Options de lecture et de création de dataset.
    * @param historique Historique de création du dataset
    * @return Dataset de ressources de jeux de données.
    */
   public Dataset<Ressource> catalogueRessources(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique) {
      return catalogueRessources(rowCatalogueRessources(optionsCreationLecture, historique));
   }

   /**
    * Renvoyer la partie Dataset du catalogue sous la forme d'un objet métier.
    * @param datasets Dataset Row des datasets de data.gouv.fr.
    * @return Dataset de ressources de jeux de données.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Ressource> catalogueRessources(Dataset<Row> datasets) {
      Objects.requireNonNull(datasets, "Le Dataset en entrée ne peut pas valoir null.");

      return datasets.map((MapFunction<Row, Ressource>)this::toObjetMetier, Encoders.bean(Ressource.class));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected RowToObjetMetierInterface<Ressource> objetMetierEncoder() {
      return this.encoder;
   }
}
