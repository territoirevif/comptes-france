package fr.ecoemploi.adapters.outbound.spark.dataset.datagouv;

import java.io.*;
import java.text.MessageFormat;
import java.time.LocalDate;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.slf4j.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.RowToObjetMetierInterface;
import fr.ecoemploi.domain.model.catalogue.*;

/**
 * Conversion d'un Row en objet métier RessourceJeuDeDonnees.
 */
public class CatalogueDatagouvRessourceJeuDeDonneesEncoder implements RowToObjetMetierInterface<RessourceJeuDeDonnees>, Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -3591391381126161117L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CatalogueDatagouvRessourceJeuDeDonneesEncoder.class);

   /**
    * {@inheritDoc}
    */
   @Override
   public Dataset<RessourceJeuDeDonnees> toDatasetObjetsMetiers(Dataset<Row> rows) {
      return rows.map((MapFunction<Row, RessourceJeuDeDonnees>)this::toObjetMetier, Encoders.bean(RessourceJeuDeDonnees.class));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public RessourceJeuDeDonnees toObjetMetier(Row row) {
      try {
         String catId = row.getAs("id_catalogue");
         CatalogueId catalogueId = catId != null ? new CatalogueId(catId) : null;

         String titreCatalogue = row.getAs("titre_catalogue");
         String slugCatalogue = row.getAs("slug_catalogue");
         String urlCatalogue = row.getAs("url_catalogue");
         String organisationCatalogue = row.getAs("organisation_catalogue");

         String orgId = row.getAs("id_organisation_catalogue");
         OrganisationId organisationCatalogueId = orgId != null ? new OrganisationId(orgId) : null;
         String licenceCatalogue = row.getAs("licence_catalogue");
         Boolean isCataloguePrive = row.getAs("prive_catalogue");
         Boolean isCatalogueArchive = row.getAs("archive_catalogue");

         String ressourceId = row.getAs("id_ressource");
         RessourceJeuDeDonneesId ressourceJeuDeDonneesId = ressourceId != null ? new RessourceJeuDeDonneesId(ressourceId) : null;

         String url = row.getAs("url_ressource");
         String titre = row.getAs("titre_ressource");
         String description = row.getAs("description_ressource");
         String typeDeFichier = row.getAs("filetype_ressource");
         String format = row.getAs("format_ressource");

         String mime = row.getAs("mime_ressource");
         Integer tailleDuFichier = row.getAs("filesize_ressource");
         String typeDeChecksum = row.getAs("checksum_type_ressource");
         String valeurChecksum = row.getAs("checksum_valeur_ressource");
         LocalDate dateCreation = row.getAs("date_creation_ressource");

         LocalDate dateModification = row.getAs("date_modification_ressource");
         Integer nombreDeTelechargements = row.getAs("nombre_downloads_ressource");
         LocalDate moissonneurDateCreation = row.getAs("moissonneur_date_creation_ressource");
         LocalDate moissonneurDateModification = row.getAs("moissonneur_date_modification_ressource");

         return new RessourceJeuDeDonnees(
            catalogueId, titreCatalogue, slugCatalogue, urlCatalogue, organisationCatalogueId,
            organisationCatalogue, licenceCatalogue, isCataloguePrive, isCatalogueArchive, ressourceJeuDeDonneesId,
            url, titre, description, typeDeFichier, format,
            mime, tailleDuFichier, typeDeChecksum, valeurChecksum, dateCreation,
            dateModification, nombreDeTelechargements, moissonneurDateCreation, moissonneurDateModification);
      }
      catch(RuntimeException e) {
         String format = "Un row durant la conversion en {0} a provoqué un incident : {1}. Contenu du row : \n{2}";
         String message = MessageFormat.format(format, RessourceJeuDeDonnees.class.getSimpleName(), e.getMessage(), row.mkString(";"));
         RuntimeException ex = new RuntimeException(message, e);

         LOGGER.error(ex.getMessage(), ex);
         throw ex;
      }
   }
}
