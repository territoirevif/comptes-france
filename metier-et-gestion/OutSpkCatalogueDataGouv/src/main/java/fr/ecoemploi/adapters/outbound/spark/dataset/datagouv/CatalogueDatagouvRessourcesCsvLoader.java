package fr.ecoemploi.adapters.outbound.spark.dataset.datagouv;

import java.io.*;

import org.apache.spark.sql.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

/**
 * Chargement de données Open data du catalogue datagouv, partie ressources associées à un jeu de données, mais sans ses données parentes de dataset, depuis un fichier CSV
 * @author Marc Le Bihan
 */
@Component
public class CatalogueDatagouvRessourcesCsvLoader implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 7270228559308638827L;

   /** Chargeur de dataset complet. */
   @Autowired
   private CatalogueDatagouvRessourcesJeuDeDonneesCsvLoader catalogueDatagouvRessourcesJeuDeDonneesCsvLoader;

   /**
    * Charger le fichier CSV du catalogue.
    * @return Dataset du catalogue, partie ressource (Row).
    * @throws IllegalArgumentException si le catalogue n'a pas été trouvé.
    */
   public Dataset<Row> loadOpenData() {
      return this.catalogueDatagouvRessourcesJeuDeDonneesCsvLoader.loadOpenData().select(
         "id_ressource", "id_catalogue", "url_ressource", "titre_ressource", "description_ressource",
         "filetype_ressource", "format_ressource", "mime_ressource", "filesize_ressource", "checksum_type_ressource",
         "checksum_valeur_ressource", "date_creation_ressource", "date_modification_ressource", "nombre_downloads_ressource", "moissonneur_date_creation_ressource",
         "moissonneur_date_modification_ressource"
      );
   }
}
