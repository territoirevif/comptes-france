package fr.ecoemploi.adapters.outbound.spark.dataset.datagouv;

import java.io.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;

import static org.apache.spark.sql.types.DataTypes.*;

/**
 * Chargement de données Open data du catalogue datagouv, partie Jeux de données (Dataset), depuis un fichier CSV
 * @author Marc Le Bihan
 */
@Component
public class CatalogueDatagouvJeuxDeDonneesCsvLoader implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 7155586807017882408L;

   /** Nom du fichier CSV. */
   @Value("${datagouv.fichier.dataset.csv}")
   private String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${catalogue-datagouv.dir}")
   private String repertoireFichier;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Charger le fichier CSV du catalogue.
    * @return Dataset du catalogue, partie ressource (Row).
    * @throws IllegalArgumentException si le catalogue n'a pas été trouvé.
    */
   public Dataset<Row> loadOpenData() {
      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, 0, this.nomFichier);

      /* "id";                      "title";                                                                            "slug";
         "66ac2368b873ff24eba06f29";"Zone de dissipation de l'Energie (ZDE) : PPRi Val de Cisse approuvé le 16/05/2023";"zone-de-dissipation-de-lenergie-zde-ppri-val-de-cisse-approuve-le-16-05-2023";

         "acronym";"url";                                                                                                              "organization";
         "";       "http://www.data.gouv.fr/fr/datasets/zone-de-dissipation-de-lenergie-zde-ppri-val-de-cisse-approuve-le-16-05-2023/";"Do.TeRR . Géo-Centre";

         "organization_id";         "owner";"owner_id";"description";                                                                     "frequency";"license";
         "5a83f81fc751df6f8573eb8a";""     ;""        ;"Carte des aléas : Zone de dissipation de l'Energie (ZDE), après rupture de digue";""         ;"License Not Specified";

         "temporal_coverage.start";"temporal_coverage.end";"spatial.granularity";"spatial.zones";"featured";"created_at";                "last_modified";
         "";                       "";                     "";                   "";             False;     "2024-08-02T00:08:08.136000";"2024-08-02T00:08:08.136000";

         "tags";                                                                                                                                          "archived";
         "donnees-ouvertes,risque-inondation,risque-zonages-risque-naturel,zones-de-gestion-de-restriction-ou-de-reglementation-et-unites-de-declaration";False;

         "resources_count";"main_resources_count";"downloads";"harvest.backend";"harvest.domain";     "harvest.created_at";"harvest.modified_at";"quality_score";
         6;                6;                      0;          "DCAT";          "catalogue.doterr.fr";"";                  "";                   "0.33";

         "metric.discussions";"metric.reuses";"metric.followers";"metric.views";"metric.resources_downloads"
         0;                   0;              0;                 0;             0
      */

      StructType schema = new StructType()
         .add("id", StringType, true)
         .add("title", StringType, true)
         .add("slug", StringType, true)
         .add("acronym", StringType, true)
         .add("url", StringType, true)

         .add("organization", StringType, true) // TODO NEW
         .add("organization_id", StringType, true) // TODO NEW
         .add("owner", StringType, true)
         .add("owner_id", StringType, true)
         .add("description", StringType, true)

         .add("frequency", StringType, true)
         .add("license", StringType, true)
         .add("temporal_coverage.start", DateType, true)
         .add("temporal_coverage.end", DateType, true)
         .add("spatial.granularity", StringType, true)

         .add("spatial.zones", StringType, true)
         .add("featured", BooleanType, true) //NOSONAR
         .add("created_at", DateType, true)
         .add("last_modified", DateType, true)
         .add("tags", StringType, true)

         .add("archived", BooleanType, true)
         .add("resources_count", IntegerType, true)
         .add("main_resources_count", IntegerType, true) // TODO NEW
         .add("downloads", IntegerType, true) // TODO NEW
         .add("harvest.backend", IntegerType, true)

         .add("harvest.domain", StringType, true)
         .add("harvest.created_at", DateType, true)
         .add("harvest.modified_at", DateType, true)
         .add("quality_score", DoubleType, true)
         .add("metric.discussions", IntegerType, true)

         .add("metric.reuses", IntegerType, true)
         .add("metric.followers", IntegerType, true)
         .add("metric.views", IntegerType, true)
         .add("metric.resources_downloads", IntegerType, true); // TODO NEW

      Dataset<Row> csv = this.session.read().schema(schema).format("csv")
         .option("header","true")
         .option("quote", "\"")
         .option("escape", "\"")
         .option("sep", ";")
         .option("multiline", "true")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath());

      return csv.withColumnRenamed("id", "id_catalogue")
         .withColumnRenamed("title", "titre_catalogue")
         .withColumnRenamed("slug", "slug_catalogue")
         .withColumnRenamed("acronym", "acronyme_catalogue")
         .withColumnRenamed("url", "url_catalogue")

         .withColumnRenamed("organization", "organisation_catalogue")
         .withColumnRenamed("organization_id", "id_organisation_catalogue")
         .withColumnRenamed("description", "description_catalogue")
         .withColumnRenamed("frequency", "frequence_catalogue")
         .withColumnRenamed("license", "licence_catalogue")

         .withColumnRenamed("temporal_coverage.start", "date_debut_couverture_catalogue")
         .withColumnRenamed("temporal_coverage.end", "date_fin_couverture_catalogue")
         .withColumnRenamed("spatial.granularity", "granularite_spatiale_catalogue")
         .withColumnRenamed("spatial.zones", "zones_spatiales_catalogue")
         .withColumnRenamed("private", "prive_catalogue")

         .withColumnRenamed("featured", "featured_catalogue")
         .withColumnRenamed("created_at", "date_creation_catalogue")
         .withColumnRenamed("last_modified", "date_modification_catalogue")
         .withColumnRenamed("tags", "tags_catalogue")
         .withColumnRenamed("archived", "archive_catalogue")

         .withColumnRenamed("harvest.backend", "nombre_moissonneurs_catalogue")
         .withColumnRenamed("harvest.domain", "moissonneur_domaine_catalogue")
         .withColumnRenamed("harvest.created_at", "moissonneur_date_creation_catalogue")
         .withColumnRenamed("harvest.modified_at", "moissonneur_date_modification_catalogue")
         .withColumnRenamed("quality_score", "score_qualite_catalogue")

         .withColumnRenamed("resources_count", "nombre_de_ressources_catalogue")
         .withColumnRenamed("metric.discussions", "metrique_discussions_catalogue")
         .withColumnRenamed("metric.reuses", "metrique_reutilisation_catalogue")
         .withColumnRenamed("metric.followers", "metrique_suiveurs_catalogue")
         .withColumnRenamed("metric.views", "metrique_vues_catalogue");
   }
}
