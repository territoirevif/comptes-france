package fr.ecoemploi.adapters.outbound.spark.dataset.datagouv;

import java.io.Serial;

import org.apache.spark.sql.Column;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.TriDataset;

/**
 * Tri de dataset : tri par id ressource
 * @author Marc Le Bihan
 */
public class CatalogueTriIdRessource extends TriDataset {
   @Serial
   private static final long serialVersionUID = -1042552882992984511L;

   /**
    * Constuire un tri.
    */
   public CatalogueTriIdRessource() {
      super("TRI-id_ressource", false, null, false,
         "id_ressource");
   }

   /**
    * Renvoyer une condition equalTo sur l'identifiant.
    * @param id Identifiant. Si null, isNull() servira au test.
    * @return Colonne contenant la condition.
    */
   public Column equalTo(String id) {
      return id != null ? col("id_ressource").equalTo(id) : col("id_ressource").isNull();
   }
}
