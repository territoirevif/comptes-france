package fr.ecoemploi.adapters.outbound.spark.dataset.datagouv;

import java.io.*;
import java.text.MessageFormat;
import java.time.LocalDate;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.slf4j.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.RowToObjetMetierInterface;
import fr.ecoemploi.domain.model.catalogue.*;

/**
 * Conversion d'un Row en objet métier RessourceJeuDeDonnees.
 */
public class CatalogueDatagouvRessourceEncoder implements RowToObjetMetierInterface<Ressource>, Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -8829076222119282661L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CatalogueDatagouvRessourceEncoder.class);

   /**
    * {@inheritDoc}
    */
   @Override
   public Dataset<Ressource> toDatasetObjetsMetiers(Dataset<Row> rows) {
      return rows.map((MapFunction<Row, Ressource>)this::toObjetMetier, Encoders.bean(Ressource.class));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Ressource toObjetMetier(Row row) {
      try {
         String ressourceId = row.getAs("id_ressource");
         RessourceJeuDeDonneesId ressourceJeuDeDonneesId = ressourceId != null ? new RessourceJeuDeDonneesId(ressourceId) : null;

         String catalogueId = row.getAs("id_catalogue");
         CatalogueId jeuDeDonneesId = new CatalogueId(catalogueId);

         String url = row.getAs("url_ressource");
         String titre = row.getAs("titre_ressource");
         String description = row.getAs("description_ressource");
         String typeDeFichier = row.getAs("filetype_ressource");
         String format = row.getAs("format_ressource");

         String mime = row.getAs("mime_ressource");
         Integer tailleDuFichier = row.getAs("filesize_ressource");
         String typeDeChecksum = row.getAs("checksum_type_ressource");
         String valeurChecksum = row.getAs("checksum_valeur_ressource");
         LocalDate dateCreation = row.getAs("date_creation_ressource");

         LocalDate dateModification = row.getAs("date_modification_ressource");
         Integer nombreDeTelechargements = row.getAs("nombre_downloads_ressource");
         LocalDate moissonneurDateCreation = row.getAs("moissonneur_date_creation_ressource");
         LocalDate moissonneurDateModification = row.getAs("moissonneur_date_modification_ressource");

         return new Ressource(jeuDeDonneesId, ressourceJeuDeDonneesId, url, titre, description, typeDeFichier, format,
            mime, tailleDuFichier, typeDeChecksum, valeurChecksum, dateCreation,
            dateModification, nombreDeTelechargements, moissonneurDateCreation, moissonneurDateModification);
      }
      catch(RuntimeException e) {
         String format = "Un row durant la conversion en {0} a provoqué un incident : {1}. Contenu du row : \n{2}";
         String message = MessageFormat.format(format, Ressource.class.getSimpleName(), e.getMessage(), row.mkString(";"));
         RuntimeException ex = new RuntimeException(message, e);

         LOGGER.error(ex.getMessage(), ex);
         throw ex;
      }
   }
}
