package fr.ecoemploi.adapters.outbound.spark.dataset.datagouv;

import java.io.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;

import static org.apache.spark.sql.types.DataTypes.*;

/**
 * Chargement de données Open data du catalogue datagouv, partie ressources associées à un jeu de données, depuis un fichier CSV
 * @author Marc Le Bihan
 */
@Component
public class CatalogueDatagouvRessourcesJeuDeDonneesCsvLoader implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 6542289137795916755L;

   /** Nom du fichier CSV. */
   @Value("${datagouv.fichier.resource.csv}")
   private String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${catalogue-datagouv.dir}")
   private String repertoireFichier;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Charger le fichier CSV du catalogue.
    * @return Dataset du catalogue, partie ressource (Row).
    * @throws IllegalArgumentException si le catalogue n'a pas été trouvé.
    */
   public Dataset<Row> loadOpenData() {
      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, 0, this.nomFichier);

      /* "dataset.id";              "dataset.title";
         "66ac2368b873ff24eba06f29";"Zone de dissipation de l'Energie (ZDE) : PPRi Val de Cisse approuvé le 16/05/2023";

         "dataset.slug";                                                                "dataset.url";
         "zone-de-dissipation-de-lenergie-zde-ppri-val-de-cisse-approuve-le-16-05-2023";"http://www.data.gouv.fr/fr/datasets/zone-de-dissipation-de-lenergie-zde-ppri-val-de-cisse-approuve-le-16-05-2023/";

         "dataset.organization";"dataset.organization_id"; "dataset.license";      "dataset.private";"dataset.archived";"id";
         "Do.TeRR . Géo-Centre";"5a83f81fc751df6f8573eb8a";"License Not Specified";False;            False;              "ed8a04fc-f2d3-47c8-857c-d935f2a2b97a";

         "url";
         "http://catalogue.geo-ide.developpement-durable.gouv.fr/catalogue/srv/fre/catalog.search#/metadata/fr-120066022-jdd-1b5a2e8d-d1b2-4852-8576-ae1693e586dd";

         "title";                                "description";"filetype";"type";"format";                         "mime";"filesize";"checksum.type";"checksum.value";
         "Vue HTML des métadonnées sur internet";"";           "remote";  "main";"www:download-1.0-http--download";"";    "";        "";             "";

         "created_at";                "modified";                  "downloads";"harvest.created_at";"harvest.modified_at";"schema_name";"schema_version";"preview_url"
         "2024-08-02T00:08:08.137000";"2024-08-02T00:08:08.137000";0;           "";                 "";                   "";           "";              False
      */

      StructType schema = new StructType()
         .add("dataset.id", StringType, false)        // 6283678588d916a28f655439
         .add("dataset.title", StringType, false)     // Infrastructures de recharge pour véhicules électriques (organisation MEA ENERGIES)
         .add("dataset.slug", StringType, false)      // infrastructures-de-recharge-pour-vehicules-electriques-organisation-mea-energies-12
         .add("dataset.url", StringType, false)       // http://www.data.gouv.fr/fr/datasets/infrastructures-de-recharge-pour-vehicules-electriques-organisation-mea-energies-12/
         .add("dataset.organization", StringType, true)  // MEA ENERGIES

         .add("dataset.organization_id", StringType, false)  // 6109024c0c69700429c90af3
         .add("dataset.license", StringType, true)    // null
         .add("dataset.private", BooleanType, false)  // false
         .add("dataset.archived", BooleanType, true)  // false
         .add("id", StringType, true)                 // 291e6936-82dc-4cd3-9494-72ebc4880676

         .add("url", StringType, false)               // https://static.data.gouv.fr/resources/infrastructures-de-recharge-pour-vehicules-electriques-organisation-mea-energies-12/20220517-111445/data.csv
         .add("title", StringType, true)              // Infrastructures de recharge pour véhicules électriques (organisation MEA ENERGIES)
         .add("description", StringType, true)        // null
         .add("filetype", StringType, false)          // file
         .add("type", StringType, false)

         .add("format", StringType, true)             // csv
         .add("mime", StringType, true)               // text/csv
         .add("filesize", IntegerType, true)          // 1696
         .add("checksum.type", StringType, true)      // sha1
         .add("checksum.value", StringType, true)     // 92c335587fcbc6deee18fe097fe197b9bc705d42

         .add("created_at", DateType, false)          // 2022-05-17
         .add("modified", DateType, false)            // 2023-03-28
         .add("downloads", IntegerType, false)        // 0
         .add("harvest.created_at", DateType, true)   // null
         .add("harvest.modified_at", DateType, true)  // null

         .add("schema_name", StringType, true)
         .add("schema_version", StringType, true)
         .add("preview_url", BooleanType, true);

      Dataset<Row> csv = this.session.read().schema(schema).format("csv")
         .option("header","true")
         .option("quote", "\"")
         .option("escape", "\"")
         .option("sep", ";")
         .option("multiline", "true")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath());

      return csv.withColumnRenamed("dataset.id", "id_catalogue")
         .withColumnRenamed("dataset.title", "titre_catalogue")
         .withColumnRenamed("dataset.slug", "slug_catalogue")
         .withColumnRenamed("dataset.url", "url_catalogue")
         .withColumnRenamed("dataset.organization", "organisation_catalogue")

         .withColumnRenamed("dataset.organization_id", "id_organisation_catalogue")
         .withColumnRenamed("dataset.license", "licence_catalogue")
         .withColumnRenamed("dataset.private", "prive_catalogue")
         .withColumnRenamed("dataset.archived", "archive_catalogue")
         .withColumnRenamed("id", "id_ressource")

         .withColumnRenamed("url", "url_ressource")
         .withColumnRenamed("title", "titre_ressource")
         .withColumnRenamed("description", "description_ressource")
         .withColumnRenamed("filetype", "filetype_ressource")
         .withColumnRenamed("format", "format_ressource")

         .withColumnRenamed("mime", "mime_ressource")
         .withColumnRenamed("filesize", "filesize_ressource")
         .withColumnRenamed("checksum.type", "checksum_type_ressource")
         .withColumnRenamed("checksum.value", "checksum_valeur_ressource")
         .withColumnRenamed("created_at", "date_creation_ressource")

         .withColumnRenamed("modified", "date_modification_ressource")
         .withColumnRenamed("downloads", "nombre_downloads_ressource")
         .withColumnRenamed("harvest.created_at", "moissonneur_date_creation_ressource")
         .withColumnRenamed("harvest.modified_at", "moissonneur_date_modification_ressource");
   }
}
