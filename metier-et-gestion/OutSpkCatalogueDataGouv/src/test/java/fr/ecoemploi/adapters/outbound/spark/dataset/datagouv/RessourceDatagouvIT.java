package fr.ecoemploi.adapters.outbound.spark.dataset.datagouv;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;

import org.junit.jupiter.api.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

import fr.ecoemploi.adapters.outbound.spark.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.domain.model.catalogue.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test sur le catalogue datagouv, partie Ressource.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkCatalogueTestApplication.class)
@ComponentScan(basePackages={"fr", "com"})
class RessourceDatagouvIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -8643751080852923199L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(RessourceDatagouvIT.class);

   /** Dataset du catalogue datagouv (ressources). */
   @Autowired
   private CatalogueDatagouvRessourceJeuDeDonneesDataset ressourceJeuDeDonneesDataset;

   /** Dataset du catalogue datagouv (ressources), simplifié. */
   @Autowired
   private CatalogueDatagouvRessourceDataset ressourceDataset;

   /**
    * Obtention des Dataset de row.
    */
   @Test
   @DisplayName("Row ressource complète avec jeu de donnée parent.")
   void rowRessourceDatasets() {
      LOGGER.info("Lecture des ressources des jeux de données datagouv en Row...");
      OptionsCreationLecture options = this.ressourceDataset.optionsCreationLecture();

      Dataset<Row> catalogue = this.ressourceJeuDeDonneesDataset.rowCatalogueRessources(options, new HistoriqueExecution());
      assertNotEquals(0, catalogue.count(), "Plusieurs entrées du catalogue (datasets) auraient du être lues.");

      catalogue.show(20, false);
      LOGGER.info("{} datasets de ressources complètes existent.", catalogue.count());
   }

   /**
    * Obtention des Dataset de row.
    */
   @Test
   @DisplayName("Row ressource simplifiée catalogue.")
   void rowRessourceSimplifieeDatasets() {
      LOGGER.info("Lecture des ressources simplifiées des jeux de données datagouv en Row...");
      OptionsCreationLecture options = this.ressourceDataset.optionsCreationLecture();

      Dataset<Row> catalogue = this.ressourceDataset.rowCatalogueRessources(options, new HistoriqueExecution());
      assertNotEquals(0, catalogue.count(), "Plusieurs entrées du catalogue (datasets) auraient du être lues.");

      catalogue.show(20, false);
      LOGGER.info("{} datasets ressources simplifiées existent.", catalogue.count());
   }

   /**
    * Obtention des Dataset d'objets métiers complet (ressource + données parentes).
    */
   @Test
   @DisplayName("Ressource avec données parentes du jeu de données.")
   void catalogueRessourcesAvecJeuDeDonnees() {
      LOGGER.info("Lecture du catalogue datagouv des ressources sous forme d'objets métiers, avec données parentes...");
      OptionsCreationLecture options = this.ressourceDataset.optionsCreationLecture();

      Dataset<RessourceJeuDeDonnees> catalogue = this.ressourceJeuDeDonneesDataset.catalogueRessources(options, new HistoriqueExecution());
      assertNotEquals(0, catalogue.count(), "Plusieurs entrées du catalogue (ressources) auraient du être lues.");

      final int nombre = 100;
      List<RessourceJeuDeDonnees> ressources = catalogue.takeAsList(nombre);
      assertEquals(nombre, ressources.size(), MessageFormat.format("{0} rows auraient dû être convertis en objets métiers de ressources de jeu de données", nombre));

      ressources.forEach(rsc -> LOGGER.info(rsc.toString()));
   }

   /**
    * Trouver une ressource particulière dans le fichier des ressources.
    */
   @Test
   @DisplayName("Trouver la ressource : fichier des communes du cog")
   void trouverRessourceCommunesCOG() {
      // Remarque : ce n'est pas une belle manière de faire une recherche.
      Column cog = (col("catalogueId").cast(StringType)).equalTo("{58c984b088ee386cdb1261f3}");
      Column communes = (col("id").cast(StringType)).equalTo("{8262de72-138f-4596-ad2f-10079e5f4d7c}");

      OptionsCreationLecture options = this.ressourceJeuDeDonneesDataset.optionsCreationLecture();
      Dataset<RessourceJeuDeDonnees> ressources = this.ressourceJeuDeDonneesDataset.catalogueRessources(options, new HistoriqueExecution()).where(cog.and(communes));
      long count = ressources.count();

      ressources.show(1000, false);
      assertEquals(1L, count, "Une seule ressource aurait dû être lue pour le fichier communes du COG");
   }

   /**
    * Trouver un jeu de données particulier dans le fichier des ressources.
    */
   @Test
   @DisplayName("Trouver un jeu de données")
   void trouverJeu() {
      String jeuId = "{58c984b088ee386cdb1261f3}";

      // Remarque : ce n'est pas une belle manière de faire une recherche.
      Column jeu = (col("catalogueId").cast(StringType)).equalTo(jeuId);

      OptionsCreationLecture options = this.ressourceJeuDeDonneesDataset.optionsCreationLecture();
      Dataset<RessourceJeuDeDonnees> ressources = this.ressourceJeuDeDonneesDataset.catalogueRessources(options, new HistoriqueExecution()).where(jeu);
      long count = ressources.count();

      ressources.show(1000, false);
      assertNotEquals(0L, count, "Au moins une ressource aurait dû être lue pour le jeu de données " + jeuId);
   }

   /**
    * Obtention des Dataset d'objets métiers (ressource seule, sans données parentes).
    */
   @Test
   @DisplayName("Ressource sans données parentes.")
   void catalogueRessourcesSansJeuDeDonnees() {
      LOGGER.info("Lecture du catalogue datagouv des ressources sous forme d'objets métiers, sans données parentes...");
      OptionsCreationLecture options = this.ressourceDataset.optionsCreationLecture();

      Dataset<Ressource> catalogue = this.ressourceDataset.catalogueRessources(options, new HistoriqueExecution());
      assertNotEquals(0, catalogue.count(), "Plusieurs entrées du catalogue (ressources) auraient du être lues.");

      final int nombre = 100;
      List<Ressource> ressources = catalogue.takeAsList(nombre);
      assertEquals(nombre, ressources.size(), MessageFormat.format("{0} rows auraient dû être convertis en objets métiers de ressources de jeu de données", nombre));

      ressources.forEach(rsc -> LOGGER.info(rsc.toString()));
   }

   /**
    * Démarrer le chonomètre.
    */
   @BeforeEach
   public void desactiverCaches() {
      this.chrono.start();
   }
}
