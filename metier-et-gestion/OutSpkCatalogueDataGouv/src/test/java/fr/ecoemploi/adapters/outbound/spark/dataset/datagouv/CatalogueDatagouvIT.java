package fr.ecoemploi.adapters.outbound.spark.dataset.datagouv;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;

import org.apache.spark.sql.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.stream.Paginateur;
import fr.ecoemploi.domain.model.catalogue.*;

/**
 * Test sur le catalogue datagouv, partie jeu de données.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkCatalogueTestApplication.class)
@ComponentScan(basePackages={"fr.ecoemploi", "com"})
class CatalogueDatagouvIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5878582886504124454L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CatalogueDatagouvIT.class);
   
   /** Dataset du catalogue datagouv. */
   @Autowired
   private CatalogueDatagouvJeuxDeDonneesDataset catalogueDataset;

   /** Dataset des ressources datagouv. */
   @Autowired
   private CatalogueDatagouvRessourceDataset ressourceDataset;

   /**
    * Obtention des Dataset de row.
    */
   @Test
   @DisplayName("Row de jeux de données")
   void rowCatalogueDatasets() {
      LOGGER.info("Lecture du catalogue datagouv en Row...");
      OptionsCreationLecture options = this.ressourceDataset.optionsCreationLecture();

      Dataset<Row> catalogue = this.catalogueDataset.rowCatalogueDataset(options, new HistoriqueExecution());
      assertNotEquals(0, catalogue.count(), "Plusieurs entrées du catalogue (datasets) auraient du être lues.");

      catalogue.show(20, false);
      LOGGER.info("{} datasets existent.", catalogue.count());
   }

   /**
    * Obtention des Dataset de row, paginés.
    */
   @Test
   @DisplayName("Row de jeux de données, paginés")
   void rowCatalogueDatasetsPagines() {
      LOGGER.info("Lecture du catalogue datagouv en Row, paginés...");
      OptionsCreationLecture options = this.ressourceDataset.optionsCreationLecture();

      Dataset<Row> catalogue = this.catalogueDataset.rowCatalogueDataset(options, new HistoriqueExecution()).limit(70);

      List<Dataset<Row>> pagines = Paginateur.paginer(catalogue, 20);
      assertEquals(4, pagines.size(), "Le nombre de sous-datasets n'est pas celui attendu");

      for(int i=0; i < 3; i ++) {
         pagines.get(i).show(1000, false);
         assertEquals(20, pagines.get(i).count(), MessageFormat.format("Le nombre d''enregistrements du sous-datasets d''index {0}, {1}, n''est pas celui attendu", i, pagines.get(i).count()));
      }

      pagines.get(3).show(1000, false);
      assertEquals(10, pagines.get(3).count(), MessageFormat.format("Le dernier sous-dataset n''a pas le bon nombre d''enregitrements, mais {0}", pagines.get(3).count()));
   }

   /**
    * Obtention des Dataset d'objets métiers paginés.
    */
   @Test
   @DisplayName("Jeux de données paginés, non streamés")
   void catalogueDatasetsObjetsMetiersPagines() {
      LOGGER.info("Lecture du catalogue datagouv en objets métiers, paginés...");
      OptionsCreationLecture options = this.ressourceDataset.optionsCreationLecture();

      Dataset<Row> catalogue = this.catalogueDataset.rowCatalogueDataset(options, new HistoriqueExecution()).limit(70);

      List<Dataset<JeuDeDonnees>> pagines = Paginateur.paginer(catalogue, p -> this.catalogueDataset.catalogueDataset(p), 20);
      assertEquals(4, pagines.size(), "Le nombre de sous-datasets d'objets métiers n'est pas celui attendu");

      for(int i=0; i < 3; i ++) {
         pagines.get(i).collectAsList().forEach(j -> LOGGER.info(j.toString()));
         assertEquals(20, pagines.get(i).count(), MessageFormat.format("Le nombre d''objets métiers du sous-datasets d''index {0}, {1}, n''est pas celui attendu", i, pagines.get(i).count()));
      }

      pagines.get(3).collectAsList().forEach(j -> LOGGER.info(j.toString()));
      assertEquals(10, pagines.get(3).count(), MessageFormat.format("Le dernier sous-dataset n''a pas le bon nombre d''objets métiers, mais {0}", pagines.get(3).count()));
   }

   /**
    * Obtention des Dataset d'objets métiers paginés, sous forme de stream.
    */
   @Test
   @DisplayName("Jeux de données paginés et streamés")
   void catalogueDatasetsObjetsMetiersPaginesStreames() {
      LOGGER.info("Lecture du catalogue datagouv en objets métiers, paginés et streamés...");
      OptionsCreationLecture options = this.ressourceDataset.optionsCreationLecture();

      Dataset<Row> catalogue = this.catalogueDataset.rowCatalogueDataset(options, new HistoriqueExecution()).limit(70);

      assertDoesNotThrow(() -> {
         Stream<JeuDeDonnees> jeuxDeDonnees = Paginateur.paginerEnStream(catalogue, p -> this.catalogueDataset.catalogueDataset(p), 20);

         List<JeuDeDonnees> extrait = jeuxDeDonnees.limit(22).toList();
         assertEquals(22, extrait.size(), "Le flux n'a pas apporté le nombre de jeux de données attendu");
         LOGGER.info(extrait.toString());
      });
   }

   /**
    * Obtention des Dataset de jeux de données et conversion en objet jeu de données.
    */
   @Test
   @DisplayName("Jeux de données, simples, sans ressources.")
   void catalogueJeuxDeDonnees() {
      LOGGER.info("Lecture du catalogue datagouv sous forme d'objets métiers...");
      OptionsCreationLecture options = this.ressourceDataset.optionsCreationLecture();

      Dataset<JeuDeDonnees> catalogue = this.catalogueDataset.catalogueDataset(options, new HistoriqueExecution());
      long count = catalogue.count();
      assertNotEquals(0, count, "Plusieurs entrées du catalogue (datasets) auraient du être lues.");
      LOGGER.info("{} enregistrements ont été placés dans le dataset", count);

      final int nombre = 100;
      List<JeuDeDonnees> jeuxDeDonnees = catalogue.takeAsList(nombre);
      assertEquals(nombre, jeuxDeDonnees.size(), MessageFormat.format("{0} rows auraient dû être convertis en objets métiers jeu de données", nombre));

      jeuxDeDonnees.forEach(jeuDeDonnees -> LOGGER.info(jeuDeDonnees.toString()));
   }

   /**
    * Obtention des jeux de données avec leurs ressouces associées.
    */
   @Test
   @DisplayName("Jeux de données complets, avec ressources.")
   void catalogueJeuxDeDonneesEtRessources() {
      LOGGER.info("Lecture du catalogue datagouv en jeux de données avec ses ressources...");
      OptionsCreationLecture options = this.ressourceDataset.optionsCreationLecture();

      Dataset<JeuDeDonnees> catalogue = this.catalogueDataset.catalogueDataset(options, new HistoriqueExecution());
      catalogue.printSchema();
      assertNotEquals(0, catalogue.count(), "Plusieurs entrées du catalogue (datasets) auraient du être lues.");

      Dataset<Ressource> ressources = this.ressourceDataset.catalogueRessources(options, new HistoriqueExecution());
      catalogue.printSchema();
      assertNotEquals(0, ressources.count(), "Plusieurs entrées des ressources auraient du être lues.");

      JeuxDeDonnees jeuxDeDonnees = this.catalogueDataset.jeuxDeDonnees(catalogue, ressources);
      assertNotEquals(0, jeuxDeDonnees.size(), "Aucune jointure n'a réussi pour former des jeux de données complets");

      jeuxDeDonnees.values().stream().limit(100).forEach(jeuDeDonnees -> {
         LOGGER.info(jeuDeDonnees.toString());
         jeuDeDonnees.getRessources().forEach((id, ressource) -> LOGGER.info("\t" + ressource.toString()));

         assertNotEquals(0, jeuDeDonnees.getRessources().size(), "Un jeu de données vient sans ressources");
      });
   }

   /**
    * Test utilitaire pour énumerer les valeurs uniques de certains champs.
    */
   @Test // @Disabled("utilitaire : examen des valeurs énumérées")
   @DisplayName("Dataset catalogue : examen des valeurs énumérées")
   void examenValeursEnumerees() {
      assertDoesNotThrow(() -> {
         String[] champs = {"frequence_catalogue", "licence_catalogue", "granularite_spatiale_catalogue", "zones_spatiales_catalogue", "tags_catalogue", "score_qualite_catalogue"};

         for(String champ : champs) {
            Map<String, Long> valeurs = this.catalogueDataset.enumererValeursDistinctes(champ);
            valeurs.forEach((key, value) -> LOGGER.info("{}\t{}", key, value));
         }
      });
   }

   /**
    * Démarrer le chonomètre.
    */
   @BeforeEach
   public void desactiverCaches() {
      this.chrono.start();
   }
}
