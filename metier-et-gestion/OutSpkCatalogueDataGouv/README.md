---
header-includes:
- \usepackage{tcolorbox}
- \usepackage{fvextra}
- \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,breakanywhere,breaksymbol=,breakanywheresymbolpre=,commandchars=\\\{\}}

title: "Catalogue data.gouv.fr"
subtitle: "ressources, organisations, liens, réutilisations, étiquettes, moissonneurs"
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

Fournisseur : INSEE

Lien: [sur data.gouv.fr](https://www.data.gouv.fr/fr/datasets/catalogue-des-donnees-de-data-gouv-fr/#/resources)

# Jeu de données

Pour chaque jeu de données publié :

   - numéro d’identification
   - titre
   - url
   - rattachement à une organisation (quand il existe)
   - description
   - sa fréquence de mise à jour
   - sa licence associée
   - couverture temporelle et spatiale
   - date de création et de dernière mise à jour
   - son statut de publication (privé ou public)
   - ses tags attribués
   - quelques indicateurs d’audience

# Ressources

Pour chaque ressource publiée :

   - son rattachement à un jeu de données,
   - titre,
   - description,
   - url,
   - type,
   - format,
   - taille,
   - dates de création
   - de dernière mise à jour
   - nombre de téléchargements

# Organisations

Pour chaque organisation créée :

   - son identification,
   - nom,
   - url,
   - description,
   - logo,
   - dates de création et de dernière modification
   - quelques indicateurs d’audience.

# Étiquettes

Pour chaque tag créé est indiqué :

   - le nombre de fois où il a été attribué à un jeu de données
   - et à une réutilisation.
 
# Moissonneurs

Pour chaque moissoneur :

   - son status (validé ou en attente),
   - le nom de l’organisation
   - la technologie concernée.

# Réutilisations

Pour chaque réutilisation publiée :

   - son numéro d’identification,
   - titre,
   - url,
   - type,
   - description,
   - rattachement à une organisation,
   - dates de création
   - de dernière mise à jour,
   - tags attribués,
   - rattachement au jeu de données réutilisé
   - quelques indicateurs d’audience.

# Discussions

Pour chaque discussion ouverte :

   - numéro d’identification
   - nom de son utilisateur lié
   - titre
   - nombre de messages
   - contenu de ses messages
   - ses dates de création
   - de fermeture

