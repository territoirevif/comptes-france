package fr.ecoemploi.domain.utils.objets;

import java.io.Serial;

/**
 * Exception liée à la persistance des données.
 * @author Marc LE BIHAN
 */
public class PersistenceException extends TechniqueException {
   /**  */
   @Serial
   private static final long serialVersionUID = -8416039991998055614L;

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception. 
    */
   public PersistenceException(String message) {
      super(message);
   }

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception. 
    */
   public PersistenceException(String message, Throwable cause) {
      super(message, cause);
   }
}
