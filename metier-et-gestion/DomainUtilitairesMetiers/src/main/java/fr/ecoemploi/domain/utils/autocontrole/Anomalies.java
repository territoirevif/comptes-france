package fr.ecoemploi.domain.utils.autocontrole;

import static fr.ecoemploi.domain.utils.autocontrole.SeveriteAnomalie.*;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.*;

/**
 * Une série d'anomalies.
 * @author Marc LE BIHAN
 */
public class Anomalies implements Iterable<Anomalie>, Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3789574731307945254L;

   /** Anomalies détectées. */
   private final List<Anomalie> anomaliesDetectees = new ArrayList<>();

   /** Visites. */
   private final Set<Object> visites = new HashSet<>();

   /**
    * Création d'une liste d'anomalies vides.
    */
   public Anomalies() {
   }

   /**
    * Construire une liste d'anomalies par copie.
    * @param anomalies Liste d'anomalies à copier.
    */
   public Anomalies(Anomalies anomalies) {
      anomalies.forEach(anomalie -> this.add(new Anomalie(anomalie)));
   }

   /**
    * Débute l'examen d'un objet vérifiable.
    * @param objetVerifiable Objet vérifiable.
    * @return Résultat de l'examen.
    */
   public static Anomalies anomalies(ObjetVerifiable objetVerifiable) {
      Anomalies anomalies = new Anomalies();
      anomalies.examine(objetVerifiable);
      return anomalies;
   }

   /**
    * Ajouter une anomalie à cette liste d'anomalies.
    * @param anomalie Anomalie à ajouter.
    */
   public void add(Anomalie anomalie) {
      Objects.requireNonNull(anomalie, "L'anomalie que l'on souhaite rajouter à la liste d'anomalies ne peut pas valoir null.");
      this.anomaliesDetectees.add(anomalie);
   }

   /**
    * Ajouter une anomalie à cette liste d'anomalies.
    * @param anomalies Anomalies à ajouter. 
    */
   public void addAll(Anomalies anomalies) {
      Objects.requireNonNull(anomalies, "La liste des anomalies que l'on souhaite rajouter à la notre peut être vide mais ne peut pas valoir null.");
      
      for(Anomalie anomalie : anomalies) {
         this.anomaliesDetectees.add(anomalie);
      }
   }

   /**
    * Procéder à l'examen et ajouter les anomalies d'un objet vérifiable.
    * @param objetVerifiable Objet vérifiable.
    */
   public void examine(ObjetVerifiable objetVerifiable) {
      // Si l'objet vérifiable vaut null, on part sans faire d'histoires. Notre but n'est pas de rajouter de la complexité dans les développements.
      // Si l'objet a déjà été examiné (d'après sa référence), c'est qu'il est embarqué dans un cycle : on ne procède pas de nouveau à son examen.
      if (objetVerifiable == null || toReference(objetVerifiable))
         return;

      // En prévisite, l'objet est ajouté à la liste des visites pour ne pas risquer d'être contrôlé une deuxième fois lors de l'examen en profondeur qui va maintenant avoir lieu.
      this.visites.add(objetVerifiable);

      // Procéder à la recherche effective des anomalies dans l'objet cible.
      objetVerifiable.anomalies(this);
   }

   /**
    * Ajouter des anomalies relatives à une liste d'objets vérifiables.
    * @param objetsVerifiables Liste des objets à vérifier.
    */
   public void examine(ObjetVerifiable... objetsVerifiables) {
      // Si la liste des objets vérifiables vaut null, on part sans faire d'histoires. Notre but n'est pas de rajouter de la complexité dans les développemnts.
      if (objetsVerifiables == null)
         return;

      for(ObjetVerifiable objetVerifiable : objetsVerifiables)
         examine(objetVerifiable);
   }

   /**
    * Helper pour déclarer rapidement une anomalie décrite par une exception.
    * @param severite La sévérité de l'anomalie à placer.
    * @param e Exception décrivant l'incident.
    * @return Message de l'anomalie.
    */
   public String declare(SeveriteAnomalie severite, Exception e) {
      Anomalie anomalie = new Anomalie(Locale.getDefault(), severite, e);
      this.anomaliesDetectees.add(anomalie);      
      return anomalie.getMessage();
   }
   
   /**
    * Helper pour déclarer rapidement une anomalie décrite par un texte brut.
    * @param severite La sévérité de l'anomalie à placer.
    * @param texte Texte descriptif.
    * @return Message de l'anomalie.
    */
   public String declare(SeveriteAnomalie severite, String texte) {
      Anomalie anomalie = new Anomalie(severite, texte);
      this.anomaliesDetectees.add(anomalie);
      return anomalie.getMessage();
   }

   /**
    * Helper pour déclarer rapidement une anomalie décrite par un texte brut.
    * @param severite La sévérité de l'anomalie à placer.
    * @param format Format à appliquer.
    * @param args Argument du message de l'anomalie.
    * @return Message de l'anomalie.
    */
   public String declare(SeveriteAnomalie severite, String format, Object... args) {
      Anomalie anomalie = new Anomalie(severite, MessageFormat.format(format, args));
      this.anomaliesDetectees.add(anomalie);
      return anomalie.getMessage();
   }

   /**
    * Déterminer s'il existe dans la liste une anomalie d'une gravité donnée (ou option : supérieure).
    * @param severite Gravité recherchée.
    * @param strict true, si l'on veut un test au sens strict (ce degré seulement),
    * <br>false, si l'on veut celle-ci ou une anomalie ayant une gravité plus forte.
    * @return true, si une anomalie ayant cette gravité (ou option : une plus forte) existe.
    */
   public boolean hasAnomalie(SeveriteAnomalie severite, boolean strict) {
      Objects.requireNonNull(severite, "La sévérité d'anomalie recherchée ne peut pas valoir null.");

      Anomalie recherche = this.anomaliesDetectees.stream().filter(anomalie -> strict ? anomalie.getSeverite().getGravite() == severite.getGravite()
            : anomalie.getSeverite().getGravite() >= severite.getGravite()).findFirst().orElse(null);

      return recherche != null;
   }

   /**
    * Déterminer s'il existe dans la liste une erreur ou une anomalie supérieure.
    * @return true, si une anomalie ayant cette gravité ou plus forte existe.
    */
   public boolean hasErreur() {
      return hasAnomalie(ERREUR, false);
   }

   /**
    * Renvoyer la plus grande sévérité présente dans une liste d'anomalies.
    * @return La sévérité la plus forte rencontrée.
    */
   public SeveriteAnomalie plusGrandeSeverite() {
      // Examiner les sévérités d'anomalies et retenir la plus grande.
      return this.anomaliesDetectees.stream().map(Anomalie::getSeverite)
         .max(Comparator.comparingInt(SeveriteAnomalie::getGravite)).orElse(AUCUNE);
   }

   /**
    * Renvoyer un sous-ensemble d'anomalies d'une gravité donnée ou supérieure.
    * @param severite Gravité recherchée.
    * @param strict true, si l'on veut un test au sens strict (ce degré seulement),
    * <br>false, si l'on veut celle-ci ou une anomalie ayant une gravité plus forte.
    * @return Liste des anomalies ayant cette gravité ou une plus forte.
    */
   public Anomalies anomalies(SeveriteAnomalie severite, boolean strict) {
      Objects.requireNonNull(severite, "La sévérité d'anomalie recherchée ne peut pas valoir null.");

      Anomalies anomalies = new Anomalies();

      for(Anomalie anomalie : this) {
         boolean eligible = strict && (anomalie.getSeverite().getGravite() == severite.getGravite())
               || !strict && (anomalie.getSeverite().getGravite() >= severite.getGravite());

         if (eligible)
            anomalies.add(anomalie);
      }

      return anomalies;
   }

   /**
    * Renvoyer la taille de cette liste d'anomalies.
    * @return Taille de la liste.
    */
   public int size() {
      return this.anomaliesDetectees.size();
   }

   /**
    * Renvoyer la référence d'un objet.
    * @param o Référence.
    * @return Référence.
    */
   private boolean toReference(Object o) {
      if (o != null) {
         return this.visites.contains(o);
      }
      
      return false;
   }
   
   /**
    * Renvoyer la liste des messages contenus dans cette liste d'anomalies.
    * @param separateur Séparateur.
    * @return Texte.
    */
   public String getMessages(String separateur) {
      if (this.anomaliesDetectees.isEmpty()) {
         return "";
      }
      
      return this.anomaliesDetectees.stream().map(Anomalie::getMessage).collect(Collectors.joining(separateur));
   }

   /**
    * @see java.lang.Iterable#iterator()
    */
   @Override
   public Iterator<Anomalie> iterator() {
      return this.anomaliesDetectees.iterator();
   }

   /**
    * @see java.util.AbstractCollection#toString()
    */
   @Override
   public String toString() {
      // Les anomalies sont les seules à attendre que l'intitulé de leur contenu soit mentionné par l'objet qui les affiche.
      // Sinon, il faudrait prendre en charge la localisation de cet objet également.
      if (this.anomaliesDetectees.isEmpty())
         return "[]";

      return this.anomaliesDetectees.stream().map(Object::toString).collect(Collectors.joining("\n", "[\n", "\n]"));
   }
}
