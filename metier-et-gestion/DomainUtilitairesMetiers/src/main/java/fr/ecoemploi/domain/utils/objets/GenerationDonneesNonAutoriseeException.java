package fr.ecoemploi.domain.utils.objets;

import java.io.Serial;

/**
 * Exception levée lorsque l'on cherche à débuter la génération de données Apache Parquet ou autres
 * alors que l'application n'est pas autorisée à le faire.
 *
 * @author Marc Le Bihan
 */
public class GenerationDonneesNonAutoriseeException extends TechniqueException {
   @Serial
   private static final long serialVersionUID = -4439595351560096941L;

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    */
   public GenerationDonneesNonAutoriseeException(String message) {
      super(message);
   }

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception.
    */
   public GenerationDonneesNonAutoriseeException(String message, Throwable cause) {
      super(message, cause);
   }
}
