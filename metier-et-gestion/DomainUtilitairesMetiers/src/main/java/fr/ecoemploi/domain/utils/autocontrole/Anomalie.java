package fr.ecoemploi.domain.utils.autocontrole;

import java.io.*;
import java.text.*;
import java.util.*;

/**
 * Description d'une anomalie.
 * @author Marc LE BIHAN
 */
public class Anomalie implements Serializable {
   /** Serial ID.  */
   @Serial
   private static final long serialVersionUID = -1091540557558793735L;

   /** Sévérité de l'anomalie. */
   private final SeveriteAnomalie severite;

   /** Message de l'anomalie. */
   private final String message;

   /** Renvoyer un numéro typant l'anomalie. */
   private int type;

   /** Locale dans laquelle a été composée cette anomalie. */
   private final Locale locale;

   /** Message d'assertion sur la sévérité d'une anomalie. */
   private static final String ASSERT_SEVERITE_ANOMALIE = "La sévérité d'une anomalie ne peut pas valoir null.";

   /**
    * Construire une anomalie à partir d'un texte brut.
    * @param severite Sévérité de l'anomalie.
    * @param texte Format du message.
    */
   public Anomalie(SeveriteAnomalie severite, String texte) {
      Objects.requireNonNull(severite, ASSERT_SEVERITE_ANOMALIE);
      
      this.severite = severite;
      this.locale = Locale.getDefault();
      this.message = texte;
   }

   /**
    * Constructeur d'une anomalie par copie
    * @param anomalie Anomalie à copier.
    */
   public Anomalie(Anomalie anomalie) {
      this.locale = anomalie.locale;
      this.message = anomalie.message;
      this.severite = anomalie.severite;
      this.type = anomalie.type;
   }

   /**
    * Construire une anomalie.
    * @param severite Sévérité de l'anomalie.
    * @param locale Locale à employer pour formatter le message.
    * @param format Format du message.
    * @param args Arguments du message.
    */
   public Anomalie(Locale locale, SeveriteAnomalie severite, String format, Object... args) {
      this(locale, format.hashCode(), severite, format, args);
   }

   /**
    * Construire une anomalie.
    * @param severite Sévérité de l'anomalie.
    * @param locale Locale à employer pour formatter le message.
    * @param type Type de l'anomalie (un nombre permettant de la classer).
    * @param format Format du message.
    * @param args Arguments du message.
    */
   public Anomalie(Locale locale, int type, SeveriteAnomalie severite, String format, Object... args) {
      Objects.requireNonNull(locale, "La locale ne peut pas valoir null.");
      Objects.requireNonNull(severite, ASSERT_SEVERITE_ANOMALIE);
      Objects.requireNonNull(format, "Le format du message d'une anomalie ne peut pas valoir null.");

      this.severite = severite;
      this.type = type;
      this.locale = locale;

      MessageFormat msgf = new MessageFormat(format, locale);
      this.message = msgf.format(args);
   }

   /**
    * Construire une anomalie à partir de message d'une exception.
    * @param severite Sévérité de l'anomalie.
    * @param locale Locale à employer pour formatter le message.
    * @param e Exception dont le contenu doit être présenté.
    */
   public Anomalie(Locale locale, SeveriteAnomalie severite, Exception e) {
      Objects.requireNonNull(locale, "La locale ne peut pas valoir null.");
      Objects.requireNonNull(severite, ASSERT_SEVERITE_ANOMALIE);
      Objects.requireNonNull(e, "L'exception ne peut pas valoir null.");

      this.severite = severite;
      this.type = e.getClass().getName().hashCode();
      this.locale = locale;

      this.message = e.getMessage();
   }

   /**
    * Renvoyer la locale associée à l'anomalie.
    * @return Locale.
    */
   public Locale getLocale() {
      return this.locale;
   }

   /**
    * Renvoyer le message.
    * @return Message.
    */
   public String getMessage() {
      return this.message;
   }

   /**
    * Renvoyer la sévérité de l'anomalie.
    * @return Sévérité de l'anomalie.
    */
   public SeveriteAnomalie getSeverite() {
      return this.severite;
   }

   /**
    * Renvoyer le type de l'anomalie.
    * @return Type de l'anomalie.
    */
   public int getType() {
      return this.type;
   }

   /**
    * Renvoyer le pointeur de l'objet.
    * @param o Objet.
    * @return Pointeur.
    */
   public static String toPointer(Object o) {
      return o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o));
   }

   /**
    * @see java.lang.Object#toString()
    */
   @Override
   public String toString() {
      String format = "{0} ({1}, {2,number,#0}, {3})";
      return MessageFormat.format(format, this.message, this.severite.name(), this.type, this.locale);
   }
}
