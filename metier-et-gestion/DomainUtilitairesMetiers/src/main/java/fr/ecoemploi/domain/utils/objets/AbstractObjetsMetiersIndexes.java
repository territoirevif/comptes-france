package fr.ecoemploi.domain.utils.objets;

import java.io.Serial;
import java.util.*;

import fr.ecoemploi.domain.utils.autocontrole.*;

/**
 * Classe de base des collections d'objets métiers indexés.
 * @param <Code> Code d'index de l'objet métier.
 * @param <OM> Objet métier.
 * @author Marc LE BIHAN
 */
public abstract class AbstractObjetsMetiersIndexes<Code, OM extends ObjetVerifiable> extends LinkedHashMap<Code, OM> implements Iterable<OM>, ObjetVerifiable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 6502573257902268270L;

   /**
    * Construire une liste d'objets indexés
    */
   protected AbstractObjetsMetiersIndexes() {
   }

   /**
    * Provoquer le tri d'une LinkedHashMap selon un comparateur.
    * @param liste Liste à trier.
    * @param comparateur Comparateur.
    */
   protected void sort(AbstractObjetsMetiersIndexes<Code, OM> liste, final Comparator<? super OM> comparateur) {
      Objects.requireNonNull(liste, "La liste à trier ne peut pas valoir null.");
      Objects.requireNonNull(comparateur, "Le comparateur ne peut pas valoir null.");

      List<Map.Entry<Code, OM>> entries = new ArrayList<>(liste.entrySet());
      entries.sort((lhs, rhs) -> comparateur.compare(lhs.getValue(), rhs.getValue()));

      liste.clear();
      for(Map.Entry<Code, OM> e : entries) {
         liste.put(e.getKey(), e.getValue());
      }
   }

   /**
    * @see ObjetVerifiable#anomalies(Anomalies)
    */
   @Override
   public void anomalies(Anomalies anomalies) {
      for(OM objet : this) {
         anomalies.examine(objet);
      }
   }

   /**
    * @see ObjetVerifiable#anomaliesNonDynamiques()
    */
   @Override
   public Anomalies anomaliesNonDynamiques() {
      Anomalies anomalies = new Anomalies();
      forEach(objet -> anomalies.addAll(objet.anomaliesNonDynamiques()));
      return anomalies;
   }

   /**
    * Déterminer si cet objet a une anomalie d'un certain niveau de gravité. 
    * @param severite Sévérité de l'anomalie.
    * @param strict true, s'il faut chercher ce niveau strictement,
    * <br>false, si un niveau égal ou supérieur convient.
    * @return true si une anomalie ou plus avec cette sévérité existe.
    */
   @Override
   public boolean hasAnomalie(SeveriteAnomalie severite, boolean strict) {
      Anomalies anomalies = new Anomalies();
      anomalies(anomalies);
      
      return anomalies.hasAnomalie(severite, strict);
   }

   /**
    * Renvoyer la plus grande sévérité d'anomalie rencontrée.
    * @return Séverité maximale.
    */
   @Override
   public SeveriteAnomalie plusGrandeSeverite() {
      Anomalies anomalies = new Anomalies();
      anomalies(anomalies);
      
      return anomalies.plusGrandeSeverite();
   }

   /**
    * @see java.lang.Iterable#iterator()
    */
   @Override
   public Iterator<OM> iterator() {
      return values().iterator();
   }
}
