package fr.ecoemploi.domain.utils.objets;

import java.text.*;
import java.util.*;

/**
 * Classe abstraite pour un comparateur avec Locale.
 */
public abstract class AbstractComparatorLocale {
   /** Collator. */
   protected final Collator collator;

   /**
    * Construire un comparateur.
    * @param locale Locale.
    */
   protected AbstractComparatorLocale(Locale locale) {
      this.collator = Collator.getInstance(locale);
   }
}
