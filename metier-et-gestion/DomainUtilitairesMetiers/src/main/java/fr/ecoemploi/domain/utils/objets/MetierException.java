package fr.ecoemploi.domain.utils.objets;

import java.io.Serial;

/**
 * Classe de base des exceptions métier.
 * @author Marc LE BIHAN
 */
public abstract class MetierException extends Exception {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 4234453773219910253L;

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    */
   protected MetierException(String message) {
      super(message);
   }

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception.
    */
   protected MetierException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Construire une exception.
    */
   protected MetierException() {
      super();
   }
}
