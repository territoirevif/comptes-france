/**
 * Classes de base pour les objets métiers capables de s'auto-contrôler.
 */
package fr.ecoemploi.domain.utils.objets;