package fr.ecoemploi.domain.utils.objets;

import org.apache.commons.lang3.builder.*;

import java.io.Serial;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;

import fr.ecoemploi.domain.utils.autocontrole.*;

/**
 * Classe de base de tous les objets métiers qui offrent la possibilité d'être
 * identifiés individuellement par un moyen quelconque.
 * @param <OMI_ID> Identifiant par lequel l'objet métier est repéré.
 * @author Marc LE BIHAN.
 */
public class ObjetMetierIdentifiable<OMI_ID extends Id> extends ObjetMetier {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -6138983703481100147L;

   /** Identifiant de l'objet métier. */
   private OMI_ID id;

   /**
    * Construire un objet métier identifiable.
    */
   public ObjetMetierIdentifiable() {
   }

   /**
    * Constructeur par copie.
    * @param c Objet à copier.
    */
   public ObjetMetierIdentifiable(ObjetMetierIdentifiable<OMI_ID> c) {
      super(c);

      if (c.id == null) {
         this.id = null;
      }
      else {
         try {
            Class<OMI_ID> classe = (Class<OMI_ID>)c.id.getClass();
            this.id = classe.getDeclaredConstructor().newInstance();
            this.id.setId(c.id.getId());
         } catch(InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            throw new RuntimeException(e);
         } catch (InvocationTargetException e) {
            throw new RuntimeException(e.getTargetException());
         }
      }
   }

   /**
    * Renvoyer l'identifiant de l'objet métier.
    * @return Identifiant.
    */
   public OMI_ID getId() {
      return this.id;
   }

   /**
    * Fixer l'identifiant de l'objet métier.
    * @param id Identifiant.
    */
   public void setId(OMI_ID id) {
      this.id = id;
   }

   /**
    * @see java.lang.Object#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object o) {
      if (o instanceof ObjetMetierIdentifiable == false)
         return false;

      ObjetMetierIdentifiable<?> oi = (ObjetMetierIdentifiable<?>)o;

      // La liste des anomalies n'est pas discriminante dans le test de l'égalité et ne participe pas au test.
      EqualsBuilder equals = new EqualsBuilder();
      equals.append(this.id, oi.id);
      return equals.isEquals();
   }

   /**
    * @see java.lang.Object#hashCode()
    */
   @Override
   public int hashCode() {
      // La liste des anomalies n'est pas discriminante dans le test de l'égalité et ne participe pas au calcul du hashcode.
      HashCodeBuilder hashcode = new HashCodeBuilder();
      hashcode.append(this.id);
      return hashcode.toHashCode();
   }

   /**
    * @see ObjetMetier#anomalies(Anomalies)
    */
   @Override
   public void anomalies(Anomalies anomalies) {
      super.anomalies(anomalies);

      // Un objet métier identifiable doit avoir un identifiant.
      if (this.id == null)
         declareAnomalie(anomalies, SeveriteAnomalie.ERREUR, "Un identifiant est obligatoire.");
      else {
         // Et cet identifiant doit être valide.
         anomalies.examine(this.id);
      }
   }

   /**
    * @see ObjetMetier#toUserString()
    */
   @Override
   public String toUserString() {
      return MessageFormat.format("identifiant : {0}", this.id);
   }

   /**
    * @see ObjetMetier#toString()
    */
   @Override
   public String toString() {
      return MessageFormat.format("id : {0}, {1}", this.id, super.toString());
   }
}
