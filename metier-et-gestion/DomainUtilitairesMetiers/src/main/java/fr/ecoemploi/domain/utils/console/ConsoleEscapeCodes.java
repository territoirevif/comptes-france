package fr.ecoemploi.domain.utils.console;

/**
 * Escape codes pour console et logger
 */
public enum ConsoleEscapeCodes {
   /** Text Reset */
   COLOR_OFF("\u001B[0m"),

   /** Black */
   BLACK("\u001B[0;30m"),

   /** Red */
   RED("\u001B[0;31m"),

   /** Green */
   GREEN("\u001B[0;32m"),

   /** Yellow */
   YELLOW("\u001B[0;33m"),

   /** Blue */
   BLUE("\u001B[0;34m"),

   /** Purple */
   PURPLE("\u001B[0;35m"),

   /** Cyan */
   CYAN("\u001B[0;36m"),

   /** White */
   WHITE("\u001B[0;37m"),

   /** Black */
   BBLACK("\u001B[1;30m"),

   /** Red */
   BRED("\u001B[1;31m"),

   /** Green */
   BGREEN("\u001B[1;32m"),

   /** Yellow */
   BYELLOW("\u001B[1;33m"),

   /** Blue */
   BBLUE("\u001B[1;34m"),

   /** Purple */
   BPURPLE("\u001B[1;35m"),

   /** Cyan */
   BCYAN("\u001B[1;36m"),

   /** White */
   BWHITE("\u001B[1;37m");

   /** Escape code pour la console */
   private final String escapeCode;

   ConsoleEscapeCodes(String escapeCode) {
      this.escapeCode = escapeCode;
   }

   /**
    * Obtenir un l'escape code console d'une couleur ou mise en forme
    * @return Escape code
    */
   public String getEscapeCode() {
      return this.escapeCode;
   }

   /**
    * Placer un texte sous escape code
    * @param texte Texte à placer sous escape code
    * @return Texte sous la forme ESCAPE_CODE texte ESCAPE_CODE_RESET
    */
   public String color(String texte) {
      return this.escapeCode + texte + COLOR_OFF.escapeCode;
   }
}
