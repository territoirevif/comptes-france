package fr.ecoemploi.domain.utils.objets;

import java.io.Serial;

/**
 * Classe de base de toutes les exceptions techniques (en général on les considère comme transitoires 
 * ou liées à l'environnement de travail : un mauvais paramétrage, par exemple, ou une connexion qui temporairement ne se fait plus).
 * @author Marc LE BIHAN
 */
public abstract class TechniqueException extends RuntimeException {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5631933862663868801L;

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception. 
    */
   protected TechniqueException(String message) {
      super(message);
   }

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception. 
    */
   protected TechniqueException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Construire une exception.
    */
   protected TechniqueException() {
      super();
   }
}
