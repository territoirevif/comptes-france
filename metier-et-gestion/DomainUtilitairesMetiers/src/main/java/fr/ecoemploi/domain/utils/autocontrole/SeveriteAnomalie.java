package fr.ecoemploi.domain.utils.autocontrole;

/**
 * Sévérité d'une anomalie.
 * @author Marc LE BIHAN
 */
public enum SeveriteAnomalie {
   /** Aucune ou ignorée. */
   AUCUNE(0),

   /** Debug. */
   DEBUG(20),

   /** Historique. */
   HISTORIQUE(50),

   /** Information. */
   INFORMATION(100),

   /** Avertissement. */
   AVERTISSEMENT(200),

   /** Avertissement système. */
   AVERTISSEMENT_SYSTEME(210),

   /** Erreur. */
   ERREUR(300),

   /** Erreur système. */
   ERREUR_SYSTEME(310),

   /** Erreur fatale. */
   FATALE(400);

   /** Gravité de l'anomalie sous forme numérique. */
   private final int gravite;

   /**
    * Construire une sévérité d'anomalie.
    * @param gravite Gravité relative de l'anomalie.
    */
   SeveriteAnomalie(int gravite) {
      this.gravite = gravite;
   }

   /**
    * Renvoyer la liste des sévérités par gravité.
    * @return Liste des sévérités.
    */
   public static SeveriteAnomalie[] parGravite() {
      return new SeveriteAnomalie[] {AUCUNE, DEBUG, HISTORIQUE, INFORMATION, AVERTISSEMENT, AVERTISSEMENT_SYSTEME, ERREUR, ERREUR_SYSTEME, FATALE};
   }

   /**
    * Renvoyer la gravité des erreurs sous forme numérique.
    * @return Gravité des anomalies.
    */
   public int getGravite() {
      return this.gravite;
   }
}
