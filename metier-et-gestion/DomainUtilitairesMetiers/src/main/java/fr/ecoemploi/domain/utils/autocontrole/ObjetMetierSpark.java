package fr.ecoemploi.domain.utils.autocontrole;

import java.io.*;
import java.time.*;
import java.time.format.*;

import org.apache.commons.lang3.*;

/**
 * Objet métier sérialisable pour Spark. 
 * @author Marc Le Bihan
 */
public abstract class ObjetMetierSpark implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -1692513835174124655L;

   /**
    * Parser une date INSEE.
    * @param date Date candidate.
    * @return Date.
    */
   public static LocalDate fromINSEEtoLocalDate(String date) {
      if (StringUtils.isBlank(date)) {
         return null;
      }
      
      return LocalDate.parse(date, DateTimeFormatter.ISO_DATE);
   }
   
   /**
    * Parser une date time INSEE.
    * @param date Date candidate.
    * @return Date.
    */
   public static LocalDateTime fromINSEEtoLocalDateTime(String date) {
      if (StringUtils.isBlank(date)) {
         return null;
      }
      
      return LocalDateTime.parse(date, DateTimeFormatter.ISO_DATE_TIME);
   }

   /**
    * Convertir un texte en boolean.
    * @param bool Booléen.
    * @return true (s'il vaut "true", "1", la lettre "O", "Y", sinon false.
    */
   public static boolean toBoolean(String bool) {
      return switch (bool) {
         case "true", "1", "O", "Y" -> true;
         default -> false;
      };
   }
}
