package fr.ecoemploi.domain.utils.autocontrole;

import java.io.*;

import org.apache.commons.lang3.*;

/**
 * Un identifiant. Constitué d'un unique mot sans espaces, il ne peut être null ou
 * vide, mais n'est pas sensible à la casse.
 * @author Marc Le Bihan.
 */
public abstract class Id implements ObjetVerifiable, Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -6962689342895602796L;

   /** Valeur de l'identifiant. */
   private String id; //NOSONAR
   
   /** Liste des anomalies non dynamiques observées sur l'objet. */
   private Anomalies anomaliesNonDynamiques = new Anomalies();

   /**
    * Construire un identifiant.
    */
   public Id() { //NOSONAR Doit pouvoir être instancié dynamiquement
   }

   /**
    * Constructeur par copie.
    * @param id Identifiant.
    */
   public Id(Id id) { //NOSONAR Doit pouvoir être instancié dynamiquement
      if (id == null) {
         this.id = null;
      }
      else {
         this.id = id.getId();
         this.anomaliesNonDynamiques = new Anomalies(id.anomaliesNonDynamiques);
      }
   }

   /**
    * Construire un identifiant.
    * @param id Chaine de l'identifiant à associer.
    */
   public Id(String id) {  //NOSONAR
      setId(id);
   }

   /**
    * Renvoyer l'identifiant.
    * @return Chaine de caractères représentant l'identifiant.
    */
   public String getId() {
      return this.id;
   }

   /**
    * Fixer la valeur de l'identifiant.
    * @param id Chaine de l'identifiant.
    */
   public void setId(String id) {
      this.id = id;
   }

   /**
    * Renvoyer l'identifiant sous la forme d'une chaine de caractère.
    * @return Chaine de caractères.
    */
   @Override
   public String toString() {
      return this.id;
   }

   /**
    * Déterminer si cet objet a une anomalie d'un certain niveau de gravité. 
    * @param severite Sévérité de l'anomalie.
    * @param strict true, s'il faut chercher ce niveau strictement,
    * <br>false, si un niveau égal ou supérieur convient.
    * @return true si une anomalie ou plus avec cette sévérité existe.
    */
   @Override
   public boolean hasAnomalie(SeveriteAnomalie severite, boolean strict) {
      Anomalies anomalies = new Anomalies();
      anomalies(anomalies);
      
      return anomalies.hasAnomalie(severite, strict);
   }

   /**
    * Renvoyer la plus grande sévérité d'anomalie rencontrée.
    * @return Séverité maximale.
    */
   @Override
   public SeveriteAnomalie plusGrandeSeverite() {
      Anomalies anomalies = new Anomalies();
      anomalies(anomalies);
      
      return anomalies.plusGrandeSeverite();
   }

   /**
    * Déterminer si deux identifiants sont identiques.
    * @param o Objet à comparer:<br>
    * - Identifiant.<br>
    * - Chaine de caractères.
    * @return <em>true</em>si les identifiants sont identiques.<br>
    * <em>false</em> sinon.
    */
   @Override
   public boolean equals(Object o) {
      // Si nous sommes comparés à un objet null, nous renvoyons false.
      if (o == null)
         return false;

      // Si nous sommes comparés à nous-mêmes, nous renvoyons true.
      if (o == this)
         return true;

      if (o instanceof Id identifiant) {
         // Si deux identifiants ont des chaînes de caractères nulles, ils sont invalides mais égaux.
         if (identifiant.getId() == null && getId() == null)
            return true;

         // Si l'un des d'eux repose sur une chaîne de caractères nulle mais pas l'autre, l'un d'entre-eux
         // est certaiment invalide, mais en premier lieu : ils ne sont plus égaux.
         if ((identifiant.getId() != null && getId() == null) || (identifiant.getId() == null && getId() != null))
            return false;

         return identifiant.getId().equalsIgnoreCase(getId());
      }

      if (o instanceof String s) {
         // Si notre identifiant vaut null, il est invalide, et en tout cas : il ne peut plus être égal au paramètre.
         if (getId() == null)
            return false;

         return getId().equalsIgnoreCase(s);
      }
      
      return false;
   }

   /**
    * Renvoyer un hashcode.
    * @return Hashcode.
    */
   @Override
   public int hashCode() {
      return this.id != null ? this.id.hashCode() : -1;
   }

   /**
    * Renvoyer la liste des anomalies non dynamiques de l'objet.
    * @return Liste des anomalies.
    */
   @Override
   public Anomalies anomaliesNonDynamiques() {
      return this.anomaliesNonDynamiques;
   }

   /**
    * @see ObjetVerifiable#anomalies(Anomalies)
    */
   @Override
   public void anomalies(Anomalies anomalies) {
      // Un identifiant ne doit pas valoir null ou être à blanc.
      if (StringUtils.isBlank(this.id)) {
         anomalies.declare(SeveriteAnomalie.ERREUR, "L''identifiant ne peut pas être vide.");
      }
      else {
         // Cet identifiant ne doit pas comporter d'espaces en son sein.
         if (this.id.contains(" ")) {
            anomalies.declare(SeveriteAnomalie.ERREUR, "L'identifiant ne peut pas comporter d'espaces.");
         }
      }
   }
}
