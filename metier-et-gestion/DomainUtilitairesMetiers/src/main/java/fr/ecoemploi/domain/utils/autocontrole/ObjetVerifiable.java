package fr.ecoemploi.domain.utils.autocontrole;

/**
 * Marque le fait qu'un objet est considéré comme un objet vérifiable:
 * il est doté d'un invariant.
 * @author Marc Le Bihan.
 * @version 1.07c Refactorisé (assert, variables, JDK 1.6).
 */
public interface ObjetVerifiable {
   /**
    * Examiner l'objet à la recherche des anomalies qu'il aurait.
    * @param anomalies Anomalies à compléter par celles détectées.
    */
   void anomalies(Anomalies anomalies);
   
   /**
    * Renvoyer la liste des anomalies non dynamiques de l'objet.
    * @return Liste des anomalies.
    */
   Anomalies anomaliesNonDynamiques();
   
   /**
    * Déterminer si cet objet a une anomalie d'un certain niveau de gravité. 
    * @param severite Sévérité de l'anomalie.
    * @param strict true, s'il faut chercher ce niveau strictement,
    * <br>false, si un niveau égal ou supérieur convient.
    * @return true si une anomalie ou plus avec cette sévérité existe.
    */
   boolean hasAnomalie(SeveriteAnomalie severite, boolean strict);

   /**
    * Renvoyer la plus grande sévérité d'anomalie rencontrée.
    * @return Séverité maximale.
    */
   SeveriteAnomalie plusGrandeSeverite();
}
