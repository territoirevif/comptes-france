package fr.ecoemploi.domain.utils.objets;

import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.text.*;
import java.time.*;
import java.util.*;

import org.slf4j.*;

import fr.ecoemploi.domain.utils.autocontrole.*;

/**
 * Classe de base de tous les objets métiers.
 * Les objets métiers sont capables de produire deux types d'anomalies :
 * <br>- Celles, dynamiques, d'autocontrôle.
 * <br>- Celles, non dynamiques, qui leur ont été imposées depuis l'extérieur.
 * @author Marc Le Bihan.
 */
public class ObjetMetier implements ObjetVerifiable, Serializable {
   /** Serial ID */
   @Serial
   private static final long serialVersionUID = -8055985242554481965L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ObjetMetier.class);

   /** Liste des anomalies non dynamiques observées sur l'objet. */
   private Anomalies anomaliesNonDynamiques = null;

   /**
    * Construire un objet métier.
    */
   public ObjetMetier() {
      // Peut être créé entièrement vide (pour des opérations de désérialisation)
   }

   /**
    * Constructeur par copie.
    * @param om Objet à copier.
    */
   public ObjetMetier(ObjetMetier om) {
      this.anomaliesNonDynamiques = om.anomaliesNonDynamiques != null ? new Anomalies(om.anomaliesNonDynamiques) : null;
   }

   /**
    * Déclarer une anomalie au nom de cet objet.
    * @param anomalies Liste des anomalies à compléter.
    * @param severite La sévérité de l'anomalie à placer.
    * @param format Format du message.
    * @param args Arguments optionnels.
    */
   public void declareAnomalie(Anomalies anomalies, SeveriteAnomalie severite, String format, Object... args) {
      anomalies.declare(severite, format, args);
   }

   /**
    * Ajouter à une liste d'anomalies toutes les anomalies d'un autre objet.
    * @param anomalies Liste des anomalies de destination.
    * @param objetVerifiable Objet dont il faut obtenir les anomalies.
    */
   public void declareAnomalies(Anomalies anomalies, ObjetVerifiable objetVerifiable) {
      Objects.requireNonNull(anomalies, "La liste des anomalies dans laquelle ajouter le message ne peut pas valoir null.");
      Objects.requireNonNull(objetVerifiable, "L''objet vérifiable dont il faut extraire les éventuelles anomalies ne peut pas valoir null.");
      anomalies.examine(objetVerifiable);
   }

   /**
    * Renvoyer la liste des anomalies non dynamiques de l'objet.
    * @return Liste des anomalies.
    */
   @Override
   public Anomalies anomaliesNonDynamiques() {
      if (this.anomaliesNonDynamiques == null) {
         this.anomaliesNonDynamiques = new Anomalies();
      }

      return this.anomaliesNonDynamiques;
   }

   /**
    * @see ObjetVerifiable#anomalies(Anomalies)
    */
   @Override
   public void anomalies(Anomalies anomalies) {
      // Renvoyer une copie de cette liste d'anomalies, car ce qu'ajoutera à la détection
      // l'objet qui appelle cette méthode ne devra avoir pour durée de vie que le temps
      // d'appel de sa méthode.
      // Sans copie, les anomalies découvertes deviendraient non dynamiques.
      // Et pour obtenir ces dernières, il faut utiliser la méthode anomaliesNonDynamiques() qui renvoie la listes des anomalies non clonées.
      if (this.anomaliesNonDynamiques != null) {
         for (Anomalie anomalie : this.anomaliesNonDynamiques)
            anomalies.add(new Anomalie(anomalie));
      }
   }

   /**
    * Déterminer si cet objet a une anomalie d'un certain niveau de gravité. 
    * @param severite Sévérité de l'anomalie.
    * @param strict true, s'il faut chercher ce niveau strictement,
    * <br>false, si un niveau égal ou supérieur convient.
    * @return true si une anomalie ou plus avec cette sévérité existe.
    */
   @Override
   public boolean hasAnomalie(SeveriteAnomalie severite, boolean strict) {
      Anomalies anomalies = new Anomalies();
      anomalies(anomalies);
      
      return anomalies.hasAnomalie(severite, strict);
   }

   /**
    * Renvoyer la plus grande sévérité d'anomalie rencontrée.
    * @return Séverité maximale.
    */
   @Override
   public SeveriteAnomalie plusGrandeSeverite() {
      Anomalies anomalies = new Anomalies();
      anomalies(anomalies);
      
      return anomalies.plusGrandeSeverite();
   }
   /**
    * Alimenter un objet avec des valeurs par défaut.
    * @param objet Objet.
    */
   public static <T> T alimenter(T objet) {
      double nDouble = 1.57818;
      int nInt = 1;
      long nLong = 10000L;

      Class<?> classe = objet.getClass();

      while(classe.equals(Object.class) == false && classe.getName().startsWith("java.") == false) {
         for(Field field : classe.getDeclaredFields()) {
            // Ignorer certains types
            if (field.getType().equals(Class.class) || field.getType().equals(ArrayList.class)
               || field.getType().equals(List.class)|| field.getType().equals(Set.class)
               || field.getType().equals(HashSet.class) || field.getType().equals(HashMap.class)) {
               continue;
            }

            // Ecarter les champs statiques.
            if (Modifier.isStatic(field.getModifiers())) {
               continue;
            }

            // Champs de type texte.
            if (field.getType().equals(String.class)) {
               setValue(field, objet, "*" + field.getName() + "*");
               continue;
            }

            // Champs de type LocalDate.
            if (field.getType().equals(LocalDate.class)) {
               setValue(field, objet, LocalDate.now());
               continue;
            }

            // Champs de type LocalDateTime.
            if (field.getType().equals(LocalDateTime.class)) {
               setValue(field, objet, LocalDateTime.now());
               continue;
            }

            // Champs de type URL.
            if (field.getType().equals(URL.class)) {
               try {
                  URL url = new URL("http://a.b.c/test");
                  setValue(field, objet, url);
               }
               catch(MalformedURLException e) {
                  throw new RuntimeException("Mauvaise préparation d'URL pour test toString : " + e.getMessage());
               }

               continue;
            }

            // Champs de type ZonedDateTime.
            if (field.getType().equals(ZonedDateTime.class)) {
               setValue(field, objet, ZonedDateTime.now());
               continue;
            }

            // Champs de type texte.
            if (field.getType().equals(Double.class) || field.getType().equals(Double.TYPE)) {
               setValue(field, objet, nDouble ++);
               continue;
            }

            // Champs de type integer.
            if (field.getType().equals(Integer.class) || field.getType().equals(Integer.TYPE)) {
               setValue(field, objet, nInt ++);
               continue;
            }


            // Champs de type boolean.
            if (field.getType().equals(Boolean.class) || field.getType().equals(Boolean.TYPE)) {
               setValue(field, objet, true);
               continue;
            }

            // Champs de type long.
            if (field.getType().equals(Long.class) || field.getType().equals(Long.TYPE)) {
               setValue(field, objet, nLong ++);
               continue;
            }

            // Champs de type énumération
            if (Enum.class.isAssignableFrom(field.getType())) {
               @SuppressWarnings("unchecked")
               Class<Enum<?>> enumeration = (Class<Enum<?>>)field.getType();
               Enum<?>[] constantes = enumeration.getEnumConstants();
               setValue(field, objet, constantes[0]);

               continue;
            }

            if (field.getType().getName().startsWith("java.") == false) {
               try {
                  field.setAccessible(true); //NOSONAR
                  Object membre = field.get(objet);

                  // Ecarter les champs statiques et abstraits.
                  if ((Modifier.isStatic(field.getModifiers()) && Modifier.isAbstract(field.getModifiers())) == false) {
                     // Si l'objet n'est pas initialisé, tenter de le faire.
                     if (membre == null) {
                        try {
                           membre = field.getType().getDeclaredConstructor().newInstance();
                        }
                        catch(@SuppressWarnings("unused") InstantiationException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
                           // On laisse passer, on ne pourra  pas l'attribuer.
                        }
                     }

                     // Si l'on a obtenu le membre ou si on l'a créé, on l'alimente et on l'affecte.
                     if (membre != null && membre != objet) {
                        alimenter(membre);
                        setValue(field, objet, membre);
                     }
                  }

                  continue;
               }
               catch(IllegalArgumentException | IllegalAccessException e) {
                  LOGGER.error("Le champ de l'objet {}.{}.{} n'a pas pu être assigné : {}", objet.getClass().getName(), field.getName(), field.getType().getName(), e.getMessage());
               }
            }

            // Indiquer les champs que l'on a pas pu assigner.
            LOGGER.warn("Le champ de l'objet de classe {} non assigné : {}.{}", objet.getClass().getName(), field.getName(), field.getType().getName());
         }

         classe = classe.getSuperclass();
      }

      return objet;
  }

   /**
    * Fixer une valeur à un champ.
    * @param field Champ dans l'objet cible.
    * @param objet Objet cible.
    * @param valeur Valeur à attribuer.
    */
   private static void setValue(Field field, Object objet, Object valeur) {
      field.setAccessible(true); //NOSONAR

      try {
         field.set(objet, valeur); //NOSONAR
      }
      catch(IllegalArgumentException | IllegalAccessException e) {
         LOGGER.error("{}.{} n'a pas pu être assigné : {}", objet.getClass().getName(), field.getName(), e.getMessage());
      }
   }

   /**
    * @see java.lang.Object#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object o) {
      // Deux objets métiers sont égaux dans leurs classes abstraites, car ils n'ont aucun membre de valeur.
      return o instanceof ObjetMetier;
   }

   /**
    * @see java.lang.Object#hashCode()
    */
   @Override
   public int hashCode() {
      return 417;
   }

   /**
    * Renvoyer une description de l'objet sous un format compréhensible par un utilisateur.
    * @return Chaîne de caractères lui permettant de comprendre ce que désigne l'objet. Idéalement, de manière unique.
    */
   public String toUserString() {
      return "";
   }

   /**
    * @see java.lang.Object#toString()
    */
   @Override
   public String toString() {
      try {
         return MessageFormat.format("anomalies : {0}", Anomalies.anomalies(this));
      }
      catch(RuntimeException e) {
         // Situation où l'exécution de la méthode anomalies() n'a pas pu aboutir : 
         // dumper l'objet plus restrictivement en rapportant l'incident.
         return MessageFormat.format("anomalies : !! {0} !!", e.getMessage());
      }
   }
}
