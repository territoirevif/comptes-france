/**
 * Module des utilitaires de bas niveau.
 */
module fr.ecoemploi.domain.utils {
   requires org.slf4j;
   requires org.apache.commons.lang3;

   exports fr.ecoemploi.domain.utils.autocontrole;
   exports fr.ecoemploi.domain.utils.console;
   exports fr.ecoemploi.domain.utils.objets;
}
