package fr.ecoemploi.domain.utils.objets;

import java.io.Serial;
import java.text.MessageFormat;

import fr.ecoemploi.domain.utils.objets.ObjetMetier;

/**
 * Une montre.
 * @author Marc LE BIHAN
 */
public class Montre extends ObjetMetier {
   /** Serial ID */
   @Serial
   private static final long serialVersionUID = 6984607313150025645L;

   /** Heure. */
   private int heure = 15;

   /** Minute. */
   private int minute = 30;

   /**
    * Renvoyer l'heure.
    * @return Heure.
    */
   public int getHeure() {
      return this.heure;
   }

   /**
    * Renvoyer les minutes.
    * @return Minutes.
    */
   public int getMinute() {
      return this.minute;
   }

   /**
    * Fixer l'heure.
    * @param heure Heure.
    */
   public void setHeure(int heure) {
      this.heure = heure;
   }

   /**
    * Fixer les minutes.
    * @param minute Minutes.
    */
   public void setMinute(int minute) {
      this.minute = minute;
   }

   /**
    * @see ObjetMetier#toString()
    */
   @Override
   public String toString() {
      return MessageFormat.format("{0}:{1}", this.heure, this.minute);
   }
}
