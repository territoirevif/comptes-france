package fr.ecoemploi.domain.utils.objets;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test des objets métiers.
 * @author Marc LE BIHAN
 */
class TestObjetsMetiers extends AbstractTestObjetsMetiers {
   /** Objet parent b1. */
   private Bof b1;

   /** Objet enfant p1 */
   private Pof p1;

   /** Objet parent b2. */
   private Bof b2;

   /** Objet enfant p2 */
   private Pof p2;

   /**
    * Test des chaînes de caractères.
    */
   @Test
   void toStrings() {
      Assertions.assertTrue(this.b1.toString().contains("AAAA"), "le texte de b1 ne contient pas le AAAA attendu.");
      Assertions.assertTrue(this.b1.toUserString().contains("AAAA"), "le texte utilisateur de b1 ne contient pas le AAAA attendu.");
      Assertions.assertFalse(this.b1.toUserString().contains("{"), "le texte utilisateur de b1 contient une accolade qui n''existe que dans toString().");

      Assertions.assertTrue(this.p1.toString().contains("BBBB"), "le texte de p1 ne contient pas le BBBB attendu.");
      Assertions.assertTrue(this.p1.toUserString().contains("BBBB"), "le texte utilisateur de p1 ne contient pas le AAAA attendu.");
      Assertions.assertFalse(this.p1.toUserString().contains("{"), "le texte utilisateur de p1 contient une accolade qui n''existe que dans toString().");

      Assertions.assertTrue(this.b2.toString().contains("DDDD"), "le texte de b2 ne contient pas le DDDD attendu.");
      Assertions.assertTrue(this.b2.toUserString().contains("DDDD"), "le texte utilisateur de b2 ne contient pas le AAAA attendu.");
      Assertions.assertFalse(this.b2.toUserString().contains("{"), "le texte utilisateur de b2 contient une accolade qui n''existe que dans toString().");

      Assertions.assertTrue(this.p2.toString().contains("CCCC"), "le texte de p2 ne contient pas le BBBB attendu.");
      Assertions.assertTrue(this.p2.toUserString().contains("CCCC"), "le texte utilisateur de p2 ne contient pas le AAAA attendu.");
      Assertions.assertFalse(this.p2.toUserString().contains("{"), "le texte utilisateur de p2 contient une accolade qui n''existe que dans toString().");
   }

   /**
    * Test de l'héritage de ressources.
    */
   @Test
   void heritageRessources() {
      // Aucun héritage (la clef 'toString' est dans le fichier de properties de la montre).
      Montre m = new Montre();
      Assertions.assertEquals("15:30", m.toString(), "L'heure de la montre est incorrecte.");

      // Héritage (la clef 'toString' est dans le fichier properties de la montre, mais utilisée par le biais Réveil qui hérite de Montre). 
      Reveil r = new Reveil();
      Assertions.assertEquals("15:30-16", r.toString(), "L'heure du réveil est incorrecte.");
   }

   /**
    * Test des clones. 
    */
   @Test
   void clones() {
      Bof b1c = new Bof(this.b1);
      Assertions.assertEquals(this.b1, b1c, "clones : b1c n'est pas égal à b1");
      Assertions.assertNotSame(this.b1, b1c, "clones : b1c n'a pas le même pointeur que b1");

      Bof b2c = new Bof(this.b2);
      Assertions.assertEquals(this.b2, b2c, "clones : b2c n'est pas égal à b2");
      Assertions.assertNotSame(this.b2, b2c, "clones : b2c n'a pas le même pointeur que b2");

      Pof p1c = new Pof(this.p1);
      Assertions.assertEquals(this.p1, p1c, "clones : p1c n'est pas égal à p1");
      Assertions.assertNotSame(this.p1, p1c, "clones : p1c n'a pas le même pointeur que p1");

      Pof p2c = new Pof(this.p2);
      Assertions.assertEquals(this.p2, p2c, "clones : p2c n'est pas égal à p2");
      Assertions.assertNotSame(this.p2, p2c, "clones : p2c n'a pas le même pointeur que p2");
   }

   /**
    * Test de sérialisation d'un objet métier.
    */
   @Test
   void serialisation() {
      assertDoesNotThrow(() -> serialisationEtRelecture(this.b1));
   }

   /**
    * Setup.
    */
   @BeforeEach
   public void setup() {
      this.p1 = new Pof();
      this.p1.setPensee("BBBB");

      this.b1 = new Bof();
      this.b1.setNom("AAAA");

      this.p2 = new Pof();
      this.p2.setPensee("CCCC");

      this.b2 = new Bof();
      this.b2.setNom("DDDD");
      this.b2.setPof(this.p2);
   }
}
