package fr.ecoemploi.domain.utils.autocontrole.atelier;

import fr.ecoemploi.domain.utils.autocontrole.*;

/**
 * Une roue.
 * @author Marc LE BIHAN
 */
public class Roue implements ObjetVerifiable {
   /** Le vélo qui a cette roue. */
   private final Velo velo;

   /**
    * Construire une roue.
    * @param velo Vélo à laquelle appartient la roue.
    */
   public Roue(Velo velo) {
      this.velo = velo;
   }

   /**
    * @see ObjetVerifiable#anomalies(Anomalies)
    */
   @Override
   public void anomalies(Anomalies anomalies) {
      anomalies.examine(this.velo);
   }

   /**
    * @see ObjetVerifiable#hasAnomalie(SeveriteAnomalie, boolean)
    */
   @Override
   public boolean hasAnomalie(@SuppressWarnings("unused") SeveriteAnomalie severite, @SuppressWarnings("unused") boolean strict) {
      return false;
   }

   /**
    * @see ObjetVerifiable#plusGrandeSeverite()
    */
   @Override
   public SeveriteAnomalie plusGrandeSeverite() {
      return null;
   }

   /**
    * @see ObjetVerifiable#anomaliesNonDynamiques()
    */
   @Override
   public Anomalies anomaliesNonDynamiques() {
      return new Anomalies();
   }
}
