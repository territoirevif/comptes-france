package fr.ecoemploi.domain.utils.objets;

import java.io.Serial;
import java.text.MessageFormat;

/**
 * Un objet métier quelconque n°2.
 * @author Marc LE BIHAN
 */
public class Pof extends ObjetMetier {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5943043102536616831L;

   /** Une pensée. */
   private String pensee;

   /**
    * Construire un objet.
    */
   public Pof() {
   }

   /**
    * Construire un objet par copie.
    * @param pof Objet à copier.
    */
   public Pof(Pof pof) {
      super(pof);
      this.pensee = pof.pensee;
   }

   /**
    * Renvoyer une pensée.
    * @return Pensée.
    */
   public String getPensee() {
      return this.pensee;
   }

   /**
    * Fixer une pensée.
    * @param pensee Pensée.
    */
   public void setPensee(String pensee) {
      this.pensee = pensee;
   }

   /**
    * @see ObjetMetier#toString()
    */
   @Override
   public String toUserString() {
      return MessageFormat.format("{0}", this.pensee);
   }

   /**
    * @see ObjetMetier#toString()
    */
   @Override
   public String toString() {
      return MessageFormat.format("'{'pensée : {0}'}', {1}", this.pensee, super.toString());
   }
}
