package fr.ecoemploi.domain.utils.objets;

import java.io.Serial;
import java.text.MessageFormat;

import fr.ecoemploi.domain.utils.autocontrole.*;

/**
 * Un objet métier quelconque n°1.
 * @author Marc LE BIHAN
 */
public class Bof extends ObjetMetier {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 9214113994637632695L;

   /** Un nom. */
   private String nom;

   /** Un objet membre pof */
   private Pof pof;

   /**
    * Constructeur.
    */
   public Bof() {
   }

   /**
    * Constructeur par copie.
    * @param bof Objet à copier.
    */
   public Bof(Bof bof) {
      super(bof);
      this.nom = bof.nom;
      this.pof = bof.pof != null ? new Pof(bof.pof) : null;
   }

   /**
    * Renvoyer le nom.
    * @return Nom.
    */
   public String getNom() {
      return this.nom;
   }

   /**
    * Renvoyer la variable membre pof.
    * @return Pof.
    */
   public Pof getPof() {
      return this.pof;
   }

   /**
    * Fixer le nom.
    * @param nom Nom.
    */
   public void setNom(String nom) {
      this.nom = nom;
   }

   /**
    * Fixer la variable membre pof.
    * @param pof Pof.
    */
   public void setPof(Pof pof) {
      this.pof = pof;
   }

   /**
    * @see ObjetMetier#anomalies(Anomalies)
    */
   @Override
   public void anomalies(Anomalies anomalies) {
      super.anomalies(anomalies);
      declareAnomalie(anomalies, SeveriteAnomalie.ERREUR, "Insatisfait", new Object[] {});
   }

   /**
    * @see ObjetMetier#toString()
    */
   @Override
   public String toUserString() {
      return MessageFormat.format("{0} {1}", this.nom, this.pof != null ? this.pof.toUserString() : "");
   }

   /**
    * @see ObjetMetier#toString()
    */
   @Override
   public String toString() {
      return MessageFormat.format("'{'nom : {0}, Pof : {1}'}', {2}", this.nom, this.pof, super.toString());
   }
}
