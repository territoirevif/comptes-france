package fr.ecoemploi.domain.utils.autocontrole;

import java.util.*;
import static java.util.Locale.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import fr.ecoemploi.domain.utils.autocontrole.atelier.Velo;
import static fr.ecoemploi.domain.utils.autocontrole.SeveriteAnomalie.*;

/**
 * Test des anomalies.
 * @author Marc LE BIHAN
 */
class TestAnomalies {
   /** Une date, pour tester la bonne localisation des paramètres. */
   private final Date date = new GregorianCalendar(2013, Calendar.SEPTEMBER, 27).getTime(); // Le 27/09/2013

   /** Une liste d'anomalies. */
   private final Anomalies anomalies1 = new Anomalies();

   /** Un message d'erreur de France. */
   private final Anomalie franceErreur1 = new Anomalie(FRANCE, ERREUR, "Un message d'erreur en français.");

   /** Un message d'erreur en anglais. */
   private final Anomalie anglaisErreur1 = new Anomalie(ENGLISH, ERREUR, "Un message d'erreur en anglais.");

   /** Un message d'avertissement de France, avec une date. */
   private final Anomalie franceAvertissement1 = new Anomalie(FRANCE, AVERTISSEMENT, "Bonjour, nous sommes le {0,date,short}.", this.date);

   /** Un message d'avertissement d'italie, avec un nombre. */
   private final Anomalie italieAvertissement1 = new Anomalie(ITALY, AVERTISSEMENT, "{0,number}.", 123456);

   /** Un message d'avertissement du Royaume-Uni, avec une date. */
   private final Anomalie ukAvertissement1 = new Anomalie(UK, AVERTISSEMENT, "Hello, we are the {0,date,short}.", this.date);

   /**
    * Test de cycle.
    */
   @Test
   void cycle() {
      // Un vélo a deux roues qui référencent elles-mêmes leur vélo parent.
      // Cela peut générer une boucle sans fin...
      assertDoesNotThrow(() -> {
         Velo velo = new Velo();

         Anomalies anomalies = new Anomalies();
         velo.anomalies(anomalies);
      });
   }

   /**
    * Test des sévérités.
    */
   @Test
   void severites() {
      assertTrue(this.anomalies1.hasAnomalie(INFORMATION, false), "La plus basse sévérité est AVERTISSEMENT > INFORMATION, donc il y a des anomalies plus fortes qu'INFORMATION.");
      assertFalse(this.anomalies1.hasAnomalie(INFORMATION, true), "La plus basse sévérité est AVERTISSEMENT, il n'y a pas d'INFORMATION spécifiquement dans la liste.");
      assertTrue(this.anomalies1.hasAnomalie(AVERTISSEMENT, true), "Il devrait être détecté des AVERTISSEMENT dans la liste, au sens strict.");
      assertTrue(this.anomalies1.hasAnomalie(AVERTISSEMENT, false), "Il devrait être détecté des AVERTISSEMENT dans la liste, au sens large.");
      assertTrue(this.anomalies1.hasAnomalie(ERREUR, true), "Il devrait être détecté des ERREUR dans la liste, au sens strict.");
      assertTrue(this.anomalies1.hasAnomalie(ERREUR, false), "Il devrait être détecté des ERREUR dans la liste, au sens large.");
      assertTrue(this.anomalies1.hasErreur(), "Il y a des ERREUR dans la liste, au sens général.");
      assertFalse(this.anomalies1.hasAnomalie(FATALE, true), "Il ne devrait pas y avoir d'anomalie FATALE repérée dans la liste, au sens strict.");
      assertFalse(this.anomalies1.hasAnomalie(FATALE, false), "Il ne devrait pas y avoir d'anomalie FATALE repérée dans la liste, au sens large.");
      assertEquals(ERREUR, this.anomalies1.plusGrandeSeverite(), "La plus grande sévérité devrait être une ERREUR.");

      assertEquals(2, this.anomalies1.anomalies(ERREUR, true).size(), "Le nombre d'erreurs dans la liste n'est pas correct (A)");
      assertEquals(2, this.anomalies1.anomalies(ERREUR, false).size(), "Le nombre d'erreurs dans la liste n'est pas correct (B)");
      assertEquals(3, this.anomalies1.anomalies(AVERTISSEMENT, true).size(), "Le nombre d'avertissements dans la liste n'est pas correct (A)");
      assertEquals(5, this.anomalies1.anomalies(AVERTISSEMENT, false).size(), "Le nombre d'avertissements dans la liste n'est pas correct (B)");
   }

   /**
    * Préparer les anomalies pour les tests.
    */
   @BeforeEach
   public void preparerListesAnomalies() {
      this.anomalies1.add(this.franceErreur1);
      this.anomalies1.add(this.anglaisErreur1);
      this.anomalies1.add(this.franceAvertissement1);
      this.anomalies1.add(this.ukAvertissement1);
      this.anomalies1.add(this.italieAvertissement1);
   }
}
