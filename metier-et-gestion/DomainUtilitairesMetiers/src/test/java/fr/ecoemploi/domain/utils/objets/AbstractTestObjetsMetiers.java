package fr.ecoemploi.domain.utils.objets;

import java.lang.reflect.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test abstrait pour tester quelques points habituels des objets métiers.
 * @author Marc LE BIHAN
 */
public abstract class AbstractTestObjetsMetiers extends AbstractTestUtilitaire {
   /**
    * Vérification du bon clonage d'un objet.
    * @param o Objet original.
    * @param clone Objet qui se dit clone du premier.
    * @param champsExclus Champs qu'il faut exclure de la vérification d'assertNotSame().
    */
   public void controlerClonage(Object o, Object clone, String... champsExclus) {
      assertNotNull(o, MessageFormat.format("L''objet de classe {0} dont le clonage doit être contrôlé ne peut pas valoir null.", o.getClass().getName()));
      assertNotNull(clone, MessageFormat.format("L''objet de classe {0} dont le clonage doit être contrôlé ne peut pas valoir null.", o.getClass().getName()));

      // Cloner l'objet puis vérifier qu'il est égal à son original.
      assertEquals(o, clone, MessageFormat.format("L''objet cloné {0} et son original devraient être égaux.", o.getClass().getSimpleName()));
      assertNotSame(o, clone, MessageFormat.format("L''objet cloné {0} et son original ne devraient pas partager le même pointeur.", o.getClass().getSimpleName()));

      // Enumérer toutes les variables membres de l'objet.
      Field[] champs = o.getClass().getDeclaredFields();

      for(Field champ : champs) {
         // Si le champ est parmi la liste de ceux à exclure, ne pas en tenir compte.
         if (isChampExclu(champ, champsExclus)) {
            continue;
         }

         // Si l'objet est un type primitif ou un String, l'assertNotSame() ne pourra pas s'appliquer.
         // En revanche, l'assertEquals(), lui, toujours.
         champ.setAccessible(true);

         // Lecture de la valeur originale.
         Object valeurOriginal = null;

         try {
            valeurOriginal = champ.get(o);
         }
         catch(IllegalArgumentException | IllegalAccessException e) {
            fail(MessageFormat.format("L''obtention de la valeur du champ {0} dans l''objet original {1} a échoué : {2}.", champ.getName(),
                  o.getClass().getSimpleName(), e.getMessage()));
         }

         // Lecture de la valeur clonée.
         Object valeurClone = null;

         try {
            valeurClone = champ.get(clone);
         }
         catch(IllegalArgumentException | IllegalAccessException e) {
            fail(MessageFormat.format("L''obtention de la valeur du champ {0} dans l''objet cloné {1} a échoué : {2}.", champ.getName(),
                  clone.getClass().getSimpleName(), e.getMessage()));
         }

         assertEquals(valeurOriginal, valeurClone, MessageFormat.format("La valeur de la variable membre {0} de l''objet {1} et de son clone devrait être égales.", champ.getName(), clone.getClass().getSimpleName()));

         // Les types primitifs, les chaînes de caractères et les énumérations, ne voient pas leurs pointeurs vérifiés.
         // Et cela n'a de sens que si les valeurs de ces pointeurs sont non nuls.
         if (valeurOriginal != null && valeurClone != null) {
            if (champ.getType().isPrimitive() == false && champ.getType().equals(String.class) == false
                  && Enum.class.isAssignableFrom(champ.getType()) == false)
               assertNotSame(valeurOriginal, valeurClone, MessageFormat.format("La variable membre {0} de l''objet {1} et de son clone ne devraient pas partager le même pointeur.", champ.getName(), clone.getClass().getSimpleName()));
         }
      }
   }

   /**
    * Déterminer si un champ fait partie d'une liste de champs exclus.
    * @param champ Champ.
    * @param champsExclus Liste de champs exclus.
    * @return true, si c'est le cas.
    */
   private boolean isChampExclu(Field champ, String[] champsExclus) {
      for (String exclus : champsExclus) {
         if (exclus.equals(champ.getName()))
            return true;
      }

      return false;
   }

   /**
    * Contrôler les toStrings de toutes les classes d'un package.
    * @param classesCandidates Liste des classes candidates au contrôle.  
    */
   public void controlerToStrings(List<Class<?>> classesCandidates) {
      Objects.requireNonNull(classesCandidates, "La liste des classes dont les toString() doivent être contrôlés peut être vide mais ne doit pas être nulle.");
      
      // Récupérer toutes les classes.
      List<Class<?>> classes = filtrer(classesCandidates);
      System.out.println(MessageFormat.format("{0} classes vont être examinées par toString().", classes.size()));

      // Pour rechercher les "{0", "{1" qui seraient demeurés et montreraient une mauvaise conversion.
      Pattern mauvaisFormats = Pattern.compile("^\\{[0-9]{1,3}$");
      boolean echecConversions = false;
      
      for(Class<?> classe : classes) {
         try {
            Object instance = classe.getDeclaredConstructor().newInstance();
            
            // Expérimentation d'un toString avec des variables membres non initialisées.
            instance.toString();
            
            // Puis avec des variables initialisées
            ObjetMetier.alimenter(instance);
            String contenu = instance.toString();
            
            if (contenu == null) {
               System.out.println(MessageFormat.format("La classe {0} produit un toString() null", classe.getName()));
               continue;
            }
            
            if (mauvaisFormats.matcher(contenu).matches()) {
               System.err.println(MessageFormat.format("toString({0}) a produit une chaîne incorrecte : {1}", classe, contenu));
               echecConversions = true;
            }
            else {
               System.out.println(MessageFormat.format("toString({0}) = {1}", classe, contenu));
            }
         }
         catch(InstantiationException | IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException e) {
            System.err.println(MessageFormat.format("L''istanciation de la classe {0} a échoué sur une exception de classe {2} : {1}", classe.getName(), e.getMessage(), e.getClass().getName()));
         }
         catch(InvocationTargetException e) {
            System.err.println(MessageFormat.format("L''istanciation de la classe {0} a échoué sur une exception de classe {2} : {1}", classe.getName(), e.getTargetException().getMessage(), e.getClass().getName()));
         }
      }
      
      if (echecConversions) {
         fail("Des échecs de conversions d'objets en toString() on eu lieu");
      }
   }

   /**
    * Filter les classes candidates.
    * @param candidates Classes candidates.
    * @return Liste des classes retenues.
    */
   private List<Class<?>> filtrer(List<Class<?>> candidates) {
      List<Class<?>> retenues = new ArrayList<>();
      
      for(Class<?> classe : candidates) {
         // Ecarter les classes abstraites.
         if (Modifier.isAbstract(classe.getModifiers())) {
            continue;
         }
         
         // Ecarter les classes que l'on ne peut instancier avec un constructeur sans argument.
         try {
            classe.getDeclaredConstructor(new Class[] {});
         }
         catch(@SuppressWarnings("unused") NoSuchMethodException | SecurityException e) {
            continue;
         }
         
         retenues.add(classe);
      }
      
      return retenues;
   }
}
