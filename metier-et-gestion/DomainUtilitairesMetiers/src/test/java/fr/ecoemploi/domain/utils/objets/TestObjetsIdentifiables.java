package fr.ecoemploi.domain.utils.objets;

import java.util.*;

import org.junit.jupiter.api.*;

import fr.ecoemploi.domain.utils.autocontrole.SeveriteAnomalie;

import static org.junit.jupiter.api.Assertions.*;

import static fr.ecoemploi.domain.utils.autocontrole.Anomalies.anomalies;

/**
 * Test sur les identifiants et les objets métiers identifiables.
 * @author Marc LE BIHAN.
 */
class TestObjetsIdentifiables extends AbstractTestObjetsMetiers {
   /**
    * Test de l'identifiant d'un objet identifiable.
    */
   @Test
   void objetIdentifiable() {
      // Un objet correctement identifié ne rapporte pas d'anomalie.
      ObjetIdentifiable o1 = new ObjetIdentifiable();
      o1.setId(new Identifiant("A"));
      assertFalse(anomalies(o1).hasErreur(), "Un objet identifiable avec un bon identifiant aurait dû être accepté.");
   }

   /**
    * Test d'un objet non identifiable.
    */
   @Test
   void objetNonIdentifiable() {
      // Un objet sans aucun identifiant est refusé.
      ObjetIdentifiable o1 = new ObjetIdentifiable();
      assertTrue(anomalies(o1).hasErreur(), "Un objet sans identifiant doit être refusé.");

      // Un objet avec un identifiant est invalide doit être refusé.
      ObjetIdentifiable o2 = new ObjetIdentifiable();
      o2.setId(new Identifiant(" "));
      assertTrue(anomalies(o2).hasErreur(), "Un objet avec un identifiant invalide doit être refusé (A).");

      ObjetIdentifiable o3 = new ObjetIdentifiable();
      o3.setId(new Identifiant(null));
      assertTrue(anomalies(o3).hasErreur(), "Un objet avec un identifiant invalide doit être refusé (B).");
   }

   /**
    * Vérifier qu'une anomalie non dynamique est conservée tout le long de l'existence d'un objet,
    * et que celles dynamiques (d'autocontrôle) ne viennent pas s'ajouter aux anomalies non dynamiques définitivement.
    */
   @Test
   void conservationDesAnomaliesNonDynamiques() {
      // On déclare une anomalie non dynamique à un objet et l'on s'assure qu'il l'a retient.
      ObjetIdentifiable oi1 = new ObjetIdentifiable(new Identifiant("A"));
      oi1.declareAnomalie(oi1.anomaliesNonDynamiques(), SeveriteAnomalie.ERREUR, "Cette anomalie non dynamique doit demeurer.", new Object[] {});
      assertTrue(anomalies(oi1).size() > 0, "L'objet aurait du renvoyer l'anomalie non dynamique.");

      // On déclare une anomalie dynamique (autocontrôle) à un objet et l'on s'assure qu'il la retient dynamiquement,
      // mais pas non dynamiquement.
      // L'identifiant X provoque la production d'une anomalie dans l'objet ObjetIdentifiable.
      ObjetIdentifiable oi2 = new ObjetIdentifiable(new Identifiant("X"));
      assertTrue(anomalies(oi2).size() > 0, "L'objet aurait du renvoyer une anomalie d'autocontrôle.");
      Assertions.assertEquals(0, oi2.anomaliesNonDynamiques().size(), "L'objet n'aurait pas du renvoyer d'anomalie non dynamique.");

      // On déclare une anomalie dynamique et non dynamique conjointement : l'une demeure en non dynamique,
      // et deux sont renvoyées lors de l'appel de l'autocontrôle.
      ObjetIdentifiable oi3 = new ObjetIdentifiable(new Identifiant("X"));
      oi1.declareAnomalie(oi3.anomaliesNonDynamiques(), SeveriteAnomalie.ERREUR, "Cette anomalie non dynamique doit demeurer.", new Object[] {});
      assertEquals(2, anomalies(oi3).size(), "L'objet aurait du renvoyer deux anomalies dynamiques.");
      Assertions.assertEquals(1, oi3.anomaliesNonDynamiques().size(), "L'objet aurait du conserver une seule anomalie non dynamique.");
   }

   /**
    * Test du clonage des listes d'objets identifiables.
    */
   @Test
   void clones() {
      // Clone d'un objet identifiable.
      ObjetIdentifiable oi1 = new ObjetIdentifiable(new Identifiant("A"));
      ObjetIdentifiable oi1c = new ObjetIdentifiable(oi1);
      Assertions.assertEquals(oi1, oi1c, "Un objet identifiable cloné aurait du valoir son original.");
      Assertions.assertNotSame(oi1, oi1c, "Un objet identifiable cloné ne devrait être pas avoir le même pointeur que son original.");
      Assertions.assertNotSame(oi1.getId(), oi1c.getId(), "Un objet identifiable cloné ne devrait pas avoir les mêmes pointeurs de variables membre que son original.");

      // Clone d'une liste d'objets identifiables, sous la forme d'ArrayList.
      ObjetIdentifiable oi2 = new ObjetIdentifiable(new Identifiant("B"));
      ObjetIdentifiable oi3 = new ObjetIdentifiable(new Identifiant("C"));
      ArrayList<ObjetIdentifiable> liste = new ArrayList<>();
      liste.add(oi1);
      liste.add(oi2);
      liste.add(oi3);

      @SuppressWarnings("unchecked") ArrayList<ObjetIdentifiable> listeClonee = (ArrayList<ObjetIdentifiable>)liste.clone();
      assertEquals(liste, listeClonee, "La liste clonée devrait être la même que celle d'origine (A).");
      assertTrue(listeClonee.containsAll(liste) && liste.containsAll(listeClonee), "La liste clonée devrait être la même que celle d'origine (B).");
   }
}
