package fr.ecoemploi.domain.utils.objets;

import java.io.Serial;

import fr.ecoemploi.domain.utils.autocontrole.*;

/**
 * Un objet identifiable.
 * @author Marc LE BIHAN
 */
public class ObjetIdentifiable extends ObjetMetierIdentifiable<Identifiant> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 8194699929033505023L;

   /**
    * Construire un objet sans identifiant. 
    */
   public ObjetIdentifiable() {
   }

   /**
    * Construire un objet avec un identifiant.
    * @param id Identifiant de l'objet. 
    */
   public ObjetIdentifiable(Identifiant id) {
      setId(id);
   }

   /**
    * Construire un objet identifiable par copie.
    * @param id Objet identifiable.
    */
   public ObjetIdentifiable(ObjetIdentifiable id) {
      super(id);
   }

   /**
    * @see ObjetMetierIdentifiable#anomalies(Anomalies)
    */
   @Override
   public void anomalies(Anomalies anomalies) {
      super.anomalies(anomalies);

      // Si l'identifiant vaut "X", cet objet déclarera une anomalie. 
      if (getId() != null && getId().getId() != null && getId().getId().equals("X"))
         declareAnomalie(anomalies, SeveriteAnomalie.ERREUR, "Une anomalie dynamique qui est fugitive.");
   }

   /**
    * Créer un objet identifiable.
    * @return Objet identifiable.
    */
   public static ObjetIdentifiable create() {
      return new ObjetIdentifiable();
   }
}
