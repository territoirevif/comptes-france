package fr.ecoemploi.domain.utils.autocontrole;

import java.io.Serial;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import static fr.ecoemploi.domain.utils.autocontrole.Anomalies.*;


/**
 * Test sur les identifiants et les objets métiers identifiables.
 * @author Marc LE BIHAN.
 */
class TestIdentifiant {
   /** Un identifiant. */
   static class Identifiant extends Id {
      /** Serial ID. */
      @Serial
      private static final long serialVersionUID = 1919085249177434402L;

      /**
       * Construire l'identifiant.
       */
      public Identifiant() {
      }

      /**
       * Construire l'identifiant.
       * @param id Valeur de l'identifiant.
       */
      public Identifiant(String id) {
         super(id);
      }

      /**
       * @see ObjetVerifiable#anomaliesNonDynamiques()
       */
      @Override
      public Anomalies anomaliesNonDynamiques() {
         return new Anomalies();
      }
   }

   /**
    * Identifiants invalides.
    */
   @Test
   @DisplayName("Détection d'identifiants invalides")
   void invalide() {
      // Un identifiant null doit être refusé.
      Identifiant i1 = new Identifiant(null);
      assertTrue(anomalies(i1).hasErreur(), "Un identifiant null aurait dû être refusé.");

      // Un identifiant vide aussi.
      Identifiant i2 = new Identifiant("");
      assertTrue(anomalies(i2).hasErreur(), "Un identifiant vide aurait dû être refusé.");

      // Les identifiants contenant des espaces également.
      Identifiant i3 = new Identifiant("a b");
      assertTrue(anomalies(i3).hasErreur(), "Un identifiant contenant des esapces aurait dû être refusé (A).");

      Identifiant i4 = new Identifiant("  ");
      assertTrue(anomalies(i4).hasErreur(), "Un identifiant contenant des esapces aurait dû être refusé (B).");
   }

   /**
    * Un identifiant valide.
    */
   @Test
   @DisplayName("Acceptation d'identifiants valides")
   void valide() {
      Identifiant identifiant = new Identifiant("valide");
      assertFalse(anomalies(identifiant).hasErreur(), "Un identifiant valide aurait dû être accepté.");
   }
}
