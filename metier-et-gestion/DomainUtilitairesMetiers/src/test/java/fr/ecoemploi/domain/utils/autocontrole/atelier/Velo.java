package fr.ecoemploi.domain.utils.autocontrole.atelier;

import fr.ecoemploi.domain.utils.autocontrole.*;

/**
 * Un vélo.
 * @author Marc LE BIHAN
 */
public class Velo implements ObjetVerifiable {
   /** Roue droite du vélo. */
   private final Roue roueDroite = new Roue(this);

   /** Roue gauche du vélo. */
   private final Roue roueGauche = new Roue(this);

   /**
    * @see ObjetVerifiable#anomalies(Anomalies)
    */
   @Override
   public void anomalies(Anomalies anomalies) {
      anomalies.examine(this.roueDroite);
      anomalies.examine(this.roueGauche);
   }

   /**
    * @see ObjetVerifiable#hasAnomalie(SeveriteAnomalie, boolean)
    */
   @Override
   public boolean hasAnomalie(@SuppressWarnings("unused") SeveriteAnomalie severite, @SuppressWarnings("unused") boolean strict) {
      return false;
   }

   /**
    * @see ObjetVerifiable#plusGrandeSeverite()
    */
   @Override
   public SeveriteAnomalie plusGrandeSeverite() {
      return null;
   }

   /**
    * @see ObjetVerifiable#anomaliesNonDynamiques()
    */
   @Override
   public Anomalies anomaliesNonDynamiques() {
      return new Anomalies();
   }
}
