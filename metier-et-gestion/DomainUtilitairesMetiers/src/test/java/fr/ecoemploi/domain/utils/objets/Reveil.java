package fr.ecoemploi.domain.utils.objets;

import java.io.Serial;
import java.text.MessageFormat;

/**
 * Un réveil.
 * @author Marc LE BIHAN
 */
public class Reveil extends Montre {
   /** Sérial ID */
   @Serial
   private static final long serialVersionUID = 1648783672785801187L;

   /** Heure d'alarme. */
   private int alarme = 16;

   /**
    * Renvoyer l'alarme.
    * @return Alarme.
    */
   public int getAlarme() {
      return this.alarme;
   }

   /**
    * Fixer l'alarme.
    * @param alarme Alarme.
    */
   public void setAlarme(int alarme) {
      this.alarme = alarme;
   }

   /**
    * @see ObjetMetier#toString()
    */
   @Override
   public String toString() {
      // Laisser le toString1 (et non pas toString()) : cela fait partie du test spécifique de ne pas aller 
      // systématiquement sur une clef de propriété toString.
      return MessageFormat.format("{0}-{1}", super.toString(), this.alarme);
   }
}
