package fr.ecoemploi.domain.utils.objets;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.text.*;
import java.util.*;

/**
 * Classe de base des tests utilitaires.
 * @author Marc LE BIHAN
 */
abstract public class AbstractTestUtilitaire {
   /**
    * Sérialiser et désérialiser un objet en vérifiant leur égalité ensuite.
    * @param candidat Objet à tester.
    */
   protected void serialisationEtRelecture(Object candidat) {
      Objects.requireNonNull(candidat, MessageFormat.format("Le {0} transmis ne peut pas valoir null.", candidat.getClass()));

      // 1) Sérialisation.
      byte[] octetsObjet = serialiser(candidat);

      // 2) Désérialisation.
      Object relectureCandidat = deserialiser(octetsObjet, candidat.getClass());
      assertEquals(candidat, relectureCandidat, MessageFormat.format("Le {0} relu n''est pas la même que celui sérialisé.", candidat.getClass()));
   }

   /**
    * Sérialiser un objet.
    * @param o Objet à sérialiser.
    * @return Octets représentant l'objet.
    */
   private byte[] serialiser(Object o) {
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      
      try {
         ObjectOutputStream oos = new ObjectOutputStream(bos);
         oos.writeObject(o);
         return bos.toByteArray();
      }
      catch(IOException e) {
         throw new RuntimeException(e.getMessage(), e);
      }
   }

   /**
    * Désérialiser un objet.
    * @param classe Classe de l'objet à désérialiser.
    * @param <O> Classe de l'objet à désérialiser.
    * @param bytes Octets à partir desquels reconstituer l'objet.
    * @return Objet désiré.
    */
   @SuppressWarnings("unchecked")
   private <O> O deserialiser(byte[] bytes, @SuppressWarnings("unused") Class<O> classe) {
      ByteArrayInputStream bis = new ByteArrayInputStream(bytes);

      try {
         ObjectInputStream ois = new ObjectInputStream(bis);
         return (O)ois.readObject();
      }
      catch(IOException | ClassNotFoundException e) {
         throw new RuntimeException(e.getMessage(), e);
      }
   }
}
