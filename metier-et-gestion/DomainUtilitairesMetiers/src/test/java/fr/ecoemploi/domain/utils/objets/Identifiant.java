package fr.ecoemploi.domain.utils.objets;

import java.io.Serial;

import fr.ecoemploi.domain.utils.autocontrole.*;

/** 
 * Un identifiant. 
 */
public class Identifiant extends Id {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -5279396361187018990L;

   /** 
    * Construire un identifiant.
    */
   public Identifiant() {
   }

   /** 
    * Construire un identifiant.
    * @param id Valeur de l'identifiant. 
    */
   public Identifiant(String id) {
      super(id);
   }

   /**
    * @see ObjetVerifiable#anomaliesNonDynamiques()
    */
   @Override
   public Anomalies anomaliesNonDynamiques() {
      return new Anomalies();
   }
}
