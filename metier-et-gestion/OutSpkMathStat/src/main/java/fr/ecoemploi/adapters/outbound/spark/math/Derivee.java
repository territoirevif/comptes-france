package fr.ecoemploi.adapters.outbound.spark.math;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.differentiation.*;
import org.slf4j.*;

/**
 * Dérivée
 * @author Marc Le Bihan
 */
public class Derivee {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(Derivee.class);

   /** Fonction à étudier */
   private final UnivariateFunction f;

   /**
    * Créer une dérivée basée sur une fonction.
    * @param f Fonction.
    */
   public Derivee(UnivariateFunction f) {
      this.f = f;
   }

   /**
    * Créer une dérivée basée sur une fonction.
    * @param f Fonction.
    */
   public Derivee(UnivariateDifferentiableFunction f) {
      this.f = f;
   }

   /**
    * Obtenir un nombre dérivé.
    * @param x Valeur x0 où l'on veut le nombre dérivé.
    * @param h Ecart pour calculer la limite, si la fonction est une univariée non différentiable.
    * @return Nombre dérivé.
    * @throws IllegalArgumentException si h vaut zéro.
    */
   public double getNombreDerive(double x, double h) {
      if (f instanceof UnivariateDifferentiableFunction differentiableFunction) {
         return differentiableFunction.value(new DerivativeStructure(1, 1, 0, x)).getPartialDerivative(1);
      }
      else {
         if (h == 0) {
            throw new IllegalArgumentException("L'écart pour calculer le nombre dérivé d'un polynôme ne peut pas valoir zéro.");
         }

         double nombreDerive = (f.value(x + h) - f.value(x)) / h;

         if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Le nombre dérivé en {} est {} pour p(x) = {}, mesuré avec h = {}.", x, nombreDerive, f, h);
         }

         return nombreDerive;
      }
   }

   /**
    * Déterminer si nous sommes devant un maximum local
    * @param x Valeur x0.
    * @param h Ecart pour comparer f'(x0 - h) avec f'(x0 + h)
    * @return true si c'est le cas (f'(x) = 0, et f croît avant x, et décroît ensuite).
    */
   public boolean isMaximumLocal(double x, double h) {
      // La fonction croît avant x, puis décroît ensuite.
      return getNombreDerive(x, h) == 0 && getNombreDerive(x - h, h) > 0 && getNombreDerive(x + h, h) < 0;
   }

   /**
    * Déterminer si nous sommes devant un minimum local
    * @param x Valeur x0.
    * @param h Ecart pour comparer f'(x0 - h) avec f'(x0 + h)
    * @return true si c'est le cas (f'(x) = 0, et f décroît avant x, et croît ensuite).
    */
   public boolean isMinimumLocal(double x, double h) {
      // La fonction décroît avant x, puis croît ensuite.
      return getNombreDerive(x, h) == 0 && getNombreDerive(x - h, h) < 0 && getNombreDerive(x + h, h) > 0;
   }

   /**
    * Déterminer si nous sommes devant une tangente horizontale
    * @param x Valeur x0.
    * @param h Ecart pour comparer f'(x0 - h) avec f'(x0 + h)
    * @return true si c'est le cas (f'(x) = 0, et f'(x0 - h) et f'(x0 + h) ne changent pas de signe).
    */
   public boolean isTangenteHorizontale(double x, double h) {
      double a = getNombreDerive(x - h, h);
      double b = getNombreDerive(x + h, h);
      return getNombreDerive(x, h) == 0 && ((a <= 0 && b <= 0) || (a >= 0 && b >= 0));
   }
}
