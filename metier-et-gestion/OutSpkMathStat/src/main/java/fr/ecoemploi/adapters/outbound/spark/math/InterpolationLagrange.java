package fr.ecoemploi.adapters.outbound.spark.math;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;

import org.slf4j.*;

/**
 * Interpolation de Lagrange
 * @author Marc Le Bihan
 */
public class InterpolationLagrange implements Serializable {
   @Serial
   private static final long serialVersionUID = -534311492720482408L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(InterpolationLagrange.class);

   /** Relations que le polynôme doit respecter. */
   private final List<Relation> relations;

   /** Le nombre de degrés du polynôme. */
   private final int n;

   /** Représente une relation y = f(x) que le polynôme doit vérifier. */
   public static class Relation implements Serializable {
      @Serial
      private static final long serialVersionUID = -3561313074829885497L;

      /** Point x */
      private final double x;

      /** Valeur souhaitée pour p(x) */
      private final double y;

      /**
       * Relation entre x et y.
       * @param x pour la valeur
       * @param y la valeur de y associée
       */
      public Relation(double x, double y) {
         this.x = x;
         this.y = y;
      }
   }

   /**
    * Construire une interpolation de Lagrange.
    * @param vx Valeurs de x recherchées.
    * @param vy Valeurs de y recherchées.
    */
   public InterpolationLagrange(List<Double> vx, List<Double> vy) {
      Objects.requireNonNull(vx, "La liste des valeurs de x à rechercher peut être vide, mais ne peut pas valoir null");
      Objects.requireNonNull(vy, "La liste des valeurs de y de p(x) peut être vide, mais ne peut pas valoir null");

      // Il faut autant de valeurs x à rechercher que de valeurs p(x) associées.
      if (vx.size() != vy.size()) {
         String format = "Il doit y avoir autant de valeurs de x à rechercher ({0} ici) que de valeurs y = p(x) associées (ici : {1})";
         String message = MessageFormat.format(format, vx.size(), vy.size());

         IllegalArgumentException ex = new IllegalArgumentException(message);
         LOGGER.error(message, ex);
         throw ex;
      }

      this.relations = new ArrayList<>();

      for(int index = 0; index < vx.size(); index++) {
         Relation r = new Relation(vx.get(index), vy.get(index));
         this.relations.add(r);
      }

      this.n = this.relations.size();
   }

   /**
    * Interpolation
    * @param x La valeur x qu'on recherche par p(x)
    * @return La valeur y = p(x)
    */
   public double interpolation(int x) {
      double result = 0;

      for (int k = 0; k < this.n; k++) {
         result += terme(k, x);
      }

      return result;
   }

   /**
    * Retourner la valeur du terme d'indice k pour la valeur x
    * @param k Indice k
    * @param x Valeur x
    * @return La valeur du terme
    */
   private double terme(int k, double x) {
      double term = this.relations.get(k).y;

      for (int j = 0; j < this.n; j++) {
         if (j != k) {
            term = term * baseDeLagrange(j, k, x);
         }
      }

      return term;
   }

   /**
    * Base de Lagrange pour le paramètre d'indice k, quand on recherche la valeur x et qu'on itère sur j
    * @param j Indice j du paramètre évalué
    * @param k Indice k de la base de Lagrange en cours de construction
    * @param x Valeur x recherchée
    * @return La valeur de la base<br>
    *    1 : si j = k<br>
    *    autre valeur si j != k
    */
   private double baseDeLagrange(int j, int k, double x) {
      if (j != k) {
        return (x - this.relations.get(j).x) / (this.relations.get(k).x - this.relations.get(j).x);
      }

      return 1.0;
   }
}
