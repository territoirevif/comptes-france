package fr.ecoemploi.adapters.outbound.spark.stat;

import java.io.Serial;
import java.util.*;
import static java.lang.Math.sqrt;

import org.apache.spark.api.java.function.MapFunction;

import org.apache.spark.sql.*;
import org.apache.spark.sql.api.java.*;
import org.apache.spark.sql.types.*;
import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.DoubleType;

import org.apache.spark.ml.feature.*;
import org.apache.spark.ml.linalg.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Expliquer les résultats d'une Analyse en Composantes principales (ACP)
 */
@Component
public class ExpliquerACP {
   /** Session Spark. */
   @Autowired
   protected SparkSession session;

   /** Plus grande valeur propre de chaque composante, sa corrélation avec la variable qu'elle résume, et la qualité globale du résumé qu'elle en fait */
   private Dataset<Row> valeursPropresEtQualite;

   /** Contributions individuelles et qualité de la représentation de chaque individu sur chaque axe */
   private Dataset<Row> contributionsEtQualiteProjectionIndividuelles;

   /** Nom du champ scaledFeatures */
   private static final String SCALED_FEATURES_FIELD = "scaledFeatures";

   /**
    * Calculer et expliquer les composantes principales
    * @param donneesQuantitatives Données quantitatives à soumettre à l'ACP
    * @param reduire true s'il faut centrer réduire les données, false s'il faut les centrer seulement
    * @param nombreComposantes Nombre de composantes principales
    */
   public void calculerEtExpliquerComposantesPrincipales(Dataset<Row> donneesQuantitatives, boolean reduire, int nombreComposantes) {
      // Centrer réduire les valeurs (Standardizing).

      // scaledFeatures
      // [3.665634766586407,3.156450874585141]
      // [2.711113911293206,0.8064410737434008]
      // [1.461309625945275,0.7194036737122256]
      // ...
      StandardScaler scaler = new StandardScaler().setInputCol("features").setOutputCol(SCALED_FEATURES_FIELD).setWithMean(true); // Centrer

      if (reduire) {
        scaler = scaler.setWithStd(true); // Réduire
      }

      Dataset<Row> centreReduit = scaler.fit(donneesQuantitatives).transform(donneesQuantitatives).select(SCALED_FEATURES_FIELD);

      // Faire calculer par ML les composantes principales (nombreComposantes extraites de p variables).
      //
      // scaledFeatures                            pca
      // [3.665634766586407,3.156450874585141]     [-4.823943018707778,-0.36004738290505234]
      // [2.711113911293206,0.8064410737434008]    [-2.4872869831159297,-1.3468070793732898]
      // [1.461309625945275,0.7194036737122256]    [-1.5419971620115105,-0.5246067298266516]
      // ...
      PCA pcaOperationDefinition = new PCA().setInputCol(SCALED_FEATURES_FIELD).setOutputCol("pca").setK(nombreComposantes);
      Dataset<Row> composantesPrincipalesCalculees = pcaOperationDefinition.fit(centreReduit).transform(centreReduit);

      expliquerComposantesPrincipales(pcaOperationDefinition, composantesPrincipalesCalculees, donneesQuantitatives.columns().length);
   }

   /**
    * Explication de composantes principales.
    * @param pcaOperationDefinition Définition de l'ACP
    * @param composantesPrincipalesCalculees ACP calculé sur les données d'entrée (contient le champ "pca")
    * @param p Nombre de variables qui ont été soumises à l'ACP
    */
   public void expliquerComposantesPrincipales(PCA pcaOperationDefinition, Dataset<Row> composantesPrincipalesCalculees, int p) {
      // Créer le dataset des features (des vecteurs) :
      long n = composantesPrincipalesCalculees.count();

      // Calculer la plus grande valeur propre de chaque composante, sa corrélation avec la variable qu'elle résume, et la qualité globale du résumé qu'elle en fait.
      //
      // valeurPropre         correlationComposanteAvecXi   qualiteResumeComposante
      // 1.5648493047635532   0.8845477106305665            0.7824246523817766
      // 0.3951506952364467   0.44449448547560555           0.19757534761822335
      this.valeursPropresEtQualite = valeurPropreEtQualiteGlobaleResume(pcaOperationDefinition, composantesPrincipalesCalculees.select(col("pca")), n, p);

      // Calculer les contributions individuelles et la qualité de la représentation de chaque individu sur chaque axe.
      //
      // xStdCarre           contributionIndividuelleAxe                  qualiteProjectionIndividuAxe
      // 23.400060365676286  [0.29741427723298436,0.006561249644731516]   [0.9944600947215109,0.00553990527848926]
      // 8.000485845427955   [0.07906955024417166,0.09180747147437675]    [0.773277605373604,0.22672239462639585]
      // 2.6529674686309654  [0.03038957477136533,0.013929481805149522]   [0.8962624969082515,0.10373750309174866]
      // ...
      this.contributionsEtQualiteProjectionIndividuelles = contributionsEtQuailiteIndividuelle(composantesPrincipalesCalculees, this.valeursPropresEtQualite, n);
   }

   /**
    * Renvoyer la plus grande valeur propre de chaque composante,<br>
    * sa corrélation avec la variable qu'elle résume,<br>
    * et la qualité globale du résumé qu'elle en fait
    * @return Dataset Row
    */
   public Dataset<Row> getValeursPropresEtQualite() {
      return this.valeursPropresEtQualite;
   }

   /**
    * Renvoyer les contributions individuelles<br>
    * et la qualité de la représentation de chaque individu sur chaque axe
    * @return Dataset Row
    */
   public Dataset<Row> getContributionsEtQualiteProjectionIndividuelles() {
      return this.contributionsEtQualiteProjectionIndividuelles;
   }

   /**
    * Renvoyer les valeurs propres (eigen values), la corrélation des composantes aux variables Xi, et la qualité globale du résumé qu'elle en donne.
    * @param pcaOperationDefinition Définition de l'opération d'ACP demandée.
    * @param pca Résultat de l'ACP.
    * @param n Nombre d'individus dans la matrice de départ.
    * @param p Nombre de variables.
    * @return Dataset contenant les valeurs propres, la corrélation des composantes aux variables Xi, et la qualité globale du résumé de chaque composante.
    */
   private Dataset<Row> valeurPropreEtQualiteGlobaleResume(PCA pcaOperationDefinition, Dataset<Row> pca, long n, int p) {
      int nombreComposantes = pcaOperationDefinition.getK();

      double[] valeursPropres = new double[nombreComposantes];                // Plus grande valeur propre de R pour une composante.
      double[] correlationsComposantesAvecXi = new double[nombreComposantes]; // Corrélation de la composante avec Xi.
      double[] qualiteComposantes = new double[nombreComposantes];            // Qualité globale du résumé proposé par la composante.

      List<Row> enregistrements = new ArrayList<>();
      List<Row> composantesTousIndividusUniversites = pca.select(col("pca")).collectAsList();

      for (Row composantesIndividuUniversite : composantesTousIndividusUniversites) {
         for (int numeroComposante = 1; numeroComposante <= nombreComposantes; numeroComposante++) {
            double composante = ((DenseVector) composantesIndividuUniversite.get(0)).values()[numeroComposante - 1]; // Φ[numero_composante]
            valeursPropres[numeroComposante - 1] += composante * composante;
         }
      }

      for (int numeroComposante = 1; numeroComposante <= nombreComposantes; numeroComposante++) {
         valeursPropres[numeroComposante - 1] = valeursPropres[numeroComposante - 1] / n;

         // Calcul de la qualité du résumé de la composante :
         correlationsComposantesAvecXi[numeroComposante - 1] = sqrt(valeursPropres[numeroComposante - 1]) * (1 / sqrt(p));
         qualiteComposantes[numeroComposante - 1] = valeursPropres[numeroComposante - 1] / p;
         Row row = RowFactory.create(valeursPropres[numeroComposante - 1], correlationsComposantesAvecXi[numeroComposante - 1], qualiteComposantes[numeroComposante - 1]);
         enregistrements.add(row);
      }

      // Construire le dataset avec le schéma [valeurPropre (double), qualiteResumeComposante (double)].
      StructType schema = new StructType(new StructField[]{
         new StructField("valeurPropre", DoubleType, false, Metadata.empty()),
         new StructField("correlationComposanteAvecXi", DoubleType, false, Metadata.empty()),
         new StructField("qualiteResumeComposante", DoubleType, false, Metadata.empty()),
      });

      return this.session.createDataFrame(enregistrements, schema);
   }

   /**
    * Calculer les contributions individuelles par axe et la qualité de la projection pour l'individu.
    * @param acp Individus centrés réduits et ACP.
    * @param valeursPropresEtQualitesComposantes Dataset des valeurs propres et qualité des composantes, par variable.
    * @param n Nombre d'individus.
    * @return Dataset d'ACP complété de la contribution individuelle et de la qualité des composantes.
    */
   private Dataset<Row> contributionsEtQuailiteIndividuelle(Dataset<Row> acp, Dataset<Row> valeursPropresEtQualitesComposantes, long n) {
      // Faire la somme, par individu, des carrés des valeurs centrées réduites x* de chaque variable se rapportant à un individu.
      Dataset<Row> ds = xStdCarre(acp);

      // Calculer les contributions individuelles par axe.
      List<Double> listeValeursPropres = valeursPropresEtQualitesComposantes.select("valeurPropre").map((MapFunction<Row, Double>) v -> v.getDouble(0), Encoders.DOUBLE()).collectAsList();
      DenseVector valeursPropres = (DenseVector) Vectors.dense(listeValeursPropres.stream().mapToDouble(Double::doubleValue).toArray());

      // Renvoyer la contribution individuelle d'un individu sur un axe.
      ds = contributionIndividuelle(ds, n, valeursPropres);

      // Calculer la qualité de la projection, par individu, sur les axes.
      return qualiteProjectionAxe(ds);
   }

   /**
    * Calculer la somme des x carrés centrés réduits des variables d'un individu.
    * @param acp Dataset ACP
    * @return Dataset avec la colonne xStdCarre ajoutée
    */
   private Dataset<Row> xStdCarre(Dataset<Row> acp) {
      this.session.udf().register("xCarresCentresReduitsUDF", new UDF1<DenseVector, Double>() {
         /** Serial ID. */
         @Serial
         private static final long serialVersionUID = 1L;

         /**
          * Renvoyer la somme des x carrés centrés réduits des variables d'un individu.
          * @param individuCentreReduit Individu aux variables centrées réduites.
          */
         @Override
         public Double call(DenseVector individuCentreReduit) {
            double sommeXCarreCentresReduits = 0.0;
            double[] variablesIndividuCentreesReduites = individuCentreReduit.values();

            for (int index = 0; index < individuCentreReduit.size(); index++) {
               sommeXCarreCentresReduits += variablesIndividuCentreesReduites[index] * variablesIndividuCentreesReduites[index];
            }

            return sommeXCarreCentresReduits;
         }
      }, DoubleType);

      // Faire la somme, par individu, des carrés des valeurs centrées réduites x* de chaque variable se rapportant à un individu.
      return acp.withColumn("xStdCarre", callUDF("xCarresCentresReduitsUDF", col(SCALED_FEATURES_FIELD)));
   }

   /**
    * Renvoyer les contributions indidividuelles
    * @param ds Dataset des individus
    * @param n Nombre de variables
    * @param valeursPropres Valeurs propres
    * @return Dataset avec la colonne contributionIndividuelleAxe ajoutée
    */
   private Dataset<Row> contributionIndividuelle(Dataset<Row> ds, long n, DenseVector valeursPropres) {
      this.session.udf().register("udfContributionIndividuelleAxe", new UDF1<DenseVector, DenseVector>() {
         /** Serial ID. */
         @Serial
         private static final long serialVersionUID = 1L;

         /**
          * Renvoyer la contribution individuelle d'un individu sur un axe.
          * @param composantes Composantes principales d'un individu.
          */
         @Override
         public DenseVector call(DenseVector composantes) {
            double[] contributions = new double[composantes.size()];

            for (int axe = 1; axe <= contributions.length; axe++) {
               double composante = composantes.values()[axe - 1];
               double valeurPropre = valeursPropres.values()[axe - 1];

               contributions[axe - 1] = ((composante * composante) / n) / valeurPropre;
            }

            return (DenseVector) Vectors.dense(contributions);
         }
      }, new VectorUDT());

      return ds.withColumn("contributionIndividuelleAxe", callUDF("udfContributionIndividuelleAxe", col("pca")));
   }

   /**
    * Qualité de la projection sur les axes
    * @param ds Dataset
    * @return Dataset avec la colonne qualiteProjectionIndividuAxe ajoutée
    */
   private Dataset<Row> qualiteProjectionAxe(Dataset<Row> ds) {
      // Calculer la qualité de la projection, par individu, sur les axes.
      this.session.udf().register("udfQualiteProjectionIndividuAxe", new UDF2<DenseVector, Double, DenseVector>() {
         /** Serial ID. */
         @Serial
         private static final long serialVersionUID = 1L;

         /**
          * Renvoyer la qualité de la projection, par individu, sur les axes.
          * @param composantes Individu aux variables centrées réduites.
          * @param xStdCarre valeurs centrées réduites, au carré, cumulées pour toutes les variables associées à l'individu.
          */
         @Override
         public DenseVector call(DenseVector composantes, Double xStdCarre) {
            double[] qualite = new double[composantes.size()];

            for (int axe = 1; axe <= qualite.length; axe++) {
               double composante = composantes.values()[axe - 1];
               qualite[axe - 1] = (composante * composante) / xStdCarre;
            }

            return (DenseVector) Vectors.dense(qualite);
         }
      }, new VectorUDT());

      return ds.withColumn("qualiteProjectionIndividuAxe", callUDF("udfQualiteProjectionIndividuAxe", col("pca"), col("xStdCarre")));
   }
}
