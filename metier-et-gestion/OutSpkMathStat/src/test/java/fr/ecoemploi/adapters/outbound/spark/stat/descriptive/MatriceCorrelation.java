package fr.ecoemploi.adapters.outbound.spark.stat.descriptive;

import java.math.*;
import java.text.DecimalFormat;
import java.util.function.*;

import org.apache.spark.ml.feature.*;
import org.apache.spark.ml.linalg.*;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.ml.stat.Correlation;
import org.apache.spark.sql.*;

import scala.collection.Iterator;

/**
 * Matrice de corrélation
 */
public class MatriceCorrelation {
   /** Matrice de corrélation. */
   private Double[][] matrice;

   /** Noms des colonnes de la matrice. */
   private String[] nomsColonnes;

   /** Nombre de places décimales. */
   private int decimalPlaces;

   /**
    * Créer une matrice de correlation à partir d'un Dataset.
    * @param matrice Matrice source.
    * @param decimalPlaces si supérieur à 0, provoque un arrondi des valeurs de corrélation
    * @return Matrice de corrélation.
    */
   public Double[][] matriceCorrelation(Dataset<Row> matrice, boolean reduire, int decimalPlaces) {
      VectorAssembler assembler = new VectorAssembler()
         .setInputCols(matrice.columns())
         .setOutputCol("features");

      StandardScaler scaler = new StandardScaler().setInputCol("features").setOutputCol("scaledFeatures") //NOSONAR
         .setWithMean(true);   // Centrer

      // Réduire si demandé
      if (reduire) {
         scaler = scaler.setWithStd(true);
      }

      Dataset<Row> centrageReduction = assembler.transform(matrice);
      centrageReduction = scaler.fit(centrageReduction).transform(centrageReduction).select("scaledFeatures"); //NOSONAR
      Dataset<Row> matriceCorrelationSpark = Correlation.corr(centrageReduction, "scaledFeatures");

      return matriceCorrelation(matriceCorrelationSpark, matrice.columns(), decimalPlaces);
   }

   /**
    * Renvoyer une matrice de corrélation p x p variables sous forme d'un double tableau de doubles
    * @param matriceCorrelation Matrice de corrélation
    * @param nomsColonnes Nom des colonnes
    * @param decimalPlaces si supérieur à 0, provoque un arrondi des valeurs de corrélation
    * @return tableau de doubles contenant la matrice
    */
   public Double[][] matriceCorrelation(Dataset<Row> matriceCorrelation, String[] nomsColonnes, int decimalPlaces) {
      int ligne = 0;

      this.matrice = new Double[nomsColonnes.length][nomsColonnes.length];
      this.nomsColonnes = nomsColonnes;
      this.decimalPlaces = decimalPlaces;

      DenseMatrix matrix = matriceCorrelation.head().getAs(0);
      Iterator<Vector> rowIter = matrix.rowIter();

      while(rowIter.hasNext()) {
         Vector row = rowIter.next();
         double[] valeurs = row.toArray();

         for(int colonne=0; colonne < valeurs.length; colonne ++) {
            double correlation = valeurs[colonne];

            // Afficher la corrélation si elle été résolue (pas de NaN)
            if (!Double.isNaN(correlation)) {
               if (decimalPlaces > 0) {
                  BigDecimal bd = BigDecimal.valueOf(correlation);
                  bd = bd.setScale(3, RoundingMode.HALF_UP);
                  matrice[ligne][colonne] = bd.doubleValue();
               }
               else {
                  matrice[ligne][colonne] = correlation;
               }
            }
         }

         ligne ++;
      }

      return this.matrice;
   }

   /**
    * {@inheritDoc}
    */
   public String toString(int nombreTabsEntete, Function<String, Integer> nombreTabulations) {
      StringBuilder representation = new StringBuilder();
      if (nombreTabsEntete > 0) {
         representation.append("\t".repeat(nombreTabsEntete - 1));
      }

      // Première ligne : les noms des variables
      for(String nomColonne : this.nomsColonnes) {
         representation.append("\t");
         representation.append(toString(nomColonne, nombreTabulations));
      }

      representation.append("\n");

      // Lignes suivantes : nom de la variable en ligne, puis les corrélations de cette variable avec les autres
      DecimalFormat decimalFormat = new DecimalFormat();
      decimalFormat.setMinimumFractionDigits(this.decimalPlaces);

      for(int ligne = 0; ligne < this.nomsColonnes.length; ligne ++) {
         if (ligne > 0)
            representation.append("\n");

         representation.append(toString(this.nomsColonnes[ligne], nombreTabulations));

         for(int colonne = 0; colonne < this.nomsColonnes.length; colonne ++) {
            representation.append("\t");

            // Ajouter un espace si le signe du nombre est positif, pour que les colonnes soient bien alignées, avant d'afficher le nombre
            double valeur = this.matrice[ligne][colonne];

            if (valeur >= 0) {
               representation.append(" ");
            }

            String stringValue = decimalFormat.format(valeur);
            representation.append(stringValue);
         }
      }

      return representation.toString();
   }

   /**
    * {@inheritDoc}
    */
   public String toString() {
      return toString(2, null);
   }

   /**
    * Retourne un nom de colonne complété d'autant de tabulation que demandé par une fonction
    * @param nomColonne Nom de la colonne
    * @param nombreTabulations Fonction donnant le nombre de tabulations d'après une chaîne de caractères.
    * @return Chaine avec d'éventuelles tabulations finales
    */
   private String toString(String nomColonne, Function<String, Integer> nombreTabulations) {
      if (nombreTabulations == null) {
         return nomColonne;
      }

      return nomColonne + "\t".repeat(nombreTabulations.apply(nomColonne));
   }
}
