package fr.ecoemploi.adapters.outbound.spark.math;

import org.apache.commons.math3.analysis.*;
import org.apache.commons.math3.analysis.differentiation.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/**
 * Tests sur les dérivées.
 */
class DeriveeTest {
   /** Une fonction simple. */
   private final UnivariateFunction f = (x -> (5 * x - 2)/(1 - 3 * x));

   /** La même, mais sous une autre forme. */
   private final UnivariateDifferentiableFunction g = new UnivariateDifferentiableFunction() {
      @Override
      public double value(double x) {
         return (5 * x - 2)/(1 - 3 * x);
      }

      @Override
      public DerivativeStructure value(DerivativeStructure x) {
         var numerator = x.multiply(5).subtract(2);
         var denominator = x.multiply(-3).add(1);
         return numerator.divide(denominator);
      }
   };

   /** Dérivée de la forme univariée simple de la fonction. */
   private final Derivee d1 = new Derivee(this.f);

   /** Dérivée de la forme univariée simple de la fonction, avec un écart invalide pour le calcul de la limite. */
   private final Derivee d2 = new Derivee(this.f);

   /** Dérivée de la forme univariée différentiable de la fonction. */
   private final Derivee d3 = new Derivee(this.g);

   /**
    * Nombres dérivés.
    */
   @Test @DisplayName("nombres dérivés")
   void nombresDerives() {
      assertEquals(-0.01561, this.d1.getNombreDerive(3.0, 0.0001), 0.0001, "Le nombre dérivé n'est pas celui attendu (A)");
      assertThrows(IllegalArgumentException.class, () -> this.d2.getNombreDerive(3.0, 0), "Un h = 0 a été accepté : il aurait dû être refusé");
      assertEquals(-0.01561, this.d3.getNombreDerive(3.0, 0.0001), 0.0001, "Le nombre dérivé n'est pas celui attendu (B)");
   }

   @Test @DisplayName("Minimum, maximum locaux, et tangente horizontale")
   void minimumMaximumLocauxEtTangenteHorizontale() {
      // f n'a ni un minimum local ni un maximum local ni de tangente horizontale en x0 = 3
      Derivee df = new Derivee(this.f);
      assertFalse(df.isMaximumLocal(3.0, 0.0001), "f ne doit pas avoir de maximum local");
      assertFalse(df.isMinimumLocal(3.0, 0.0001), "f ne doit pas avoir de minimum local");
      assertFalse(df.isTangenteHorizontale(3.0, 0.0001), "f ne doit pas avoir de tangente horizontale");
   }
}
