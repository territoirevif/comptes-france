package fr.ecoemploi.adapters.outbound.spark.stat.univariee;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;
import static org.junit.jupiter.api.Assertions.*;

import java.io.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import fr.ecoemploi.adapters.outbound.spark.stat.*;

/**
 * Tests sur les étudiants. 
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkMathStatsTestApplication.class)
class EtudiantsIT extends AbstractStatsTest implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -5457915368407251746L;

   /**
    * Test sur les étudiants : présentation de l'énoncé.
    * C1 C2 C3 C4 C5 C6 C7
    * --------------------
    * 52 47 51 69 83 76 24
    * 23 44 19 67 24 75 23
    * 83 58 63 77 85 83 27
    * 75 51 43 85 86 80 30
    *  4 46 25 33 27 14 19
    * 35 56 31 47 77 77 21
    * 67 49 27 75 79 78 29
    * 92 57 73 83 87 84 93
    * 12 42 23 59 21 79 18
    * 46 54 48 73 29 81 25
    */
   @Test
   @DisplayName("Enoncé : les notes de dix étudiants dans sept matières")
   void enonce() {
      Dataset<Row> etudiants = load();
      
      // Calcul des moyennes des valeurs des lignes, dans une colonne additionnelle.
      Column somme = col("c1").plus(col("c2")).plus(col("c3")).plus(col("c4")).plus(col("c5")).plus(col("c6")).plus(col("c7"));
      Column moyenne = round(somme.divide(etudiants.columns().length), 1);
      
      Dataset<Row> moyennesLignes = etudiants.withColumn("moyennes", moyenne);
      moyennesLignes.show();
      assertEquals(57.4, moyennesLignes.head().getDouble(etudiants.columns().length), "La moyenne de la première ligne n'est pas celle attendue.");
      
      // Calcul des moyennes des colonnes.
      Dataset<Row> moyennesColonnes = etudiants.select(avg("c1"), avg("c2"), avg("c3"), avg("c4"), avg("c5"), avg("c6"), avg("c7"));
      moyennesColonnes.show();
      assertEquals(48.9, moyennesColonnes.head().getDouble(0), "La moyenne de la première colonne n'est pas celle attendue.");
   }
   
   /**
    * Charger le jeu de test.
    * @return Jeu de test des étudiants.
    */
   private Dataset<Row> load() {
      StructType schema = new StructType();
      
      schema = schema.add("C1", IntegerType, false)
         .add("C2", IntegerType, false)
         .add("C3", IntegerType, false)
         .add("C4", IntegerType, false)
         .add("C5", IntegerType, false)
         .add("C6", IntegerType, false)
         .add("C7", IntegerType, false);
         
      return this.session.read().schema(schema).format("csv").option("header","true").load(new File("src/test/resources/etudiants.csv").getAbsolutePath());
   }
}
