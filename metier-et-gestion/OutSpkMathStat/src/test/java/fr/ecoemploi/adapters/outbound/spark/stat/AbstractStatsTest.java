package fr.ecoemploi.adapters.outbound.spark.stat;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

import java.util.*;

import org.apache.spark.api.java.function.*;
import org.apache.spark.ml.linalg.*;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;
import org.springframework.beans.factory.annotation.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.cog.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.HistoriqueExecution;
import fr.ecoemploi.adapters.outbound.spark.dataset.groupement.*;
import fr.ecoemploi.domain.model.territoire.NatureJuridiqueIntercommunalite;
import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;

/**
 * Classe de base pour les tests statistiques.
 * @author Marc Le Bihan
 */
public abstract class AbstractStatsTest extends AbstractRestIT {
   /** Session Spark. */
   @Autowired
   protected SparkSession session;

   /** Dataset du code officiel géographique. */
   @Autowired
   private CogDataset cogDataset;

   /** Dataset des périmètres. */
   @Autowired
   private EPCIPerimetreDataset perimetresDataset;

   /**
    * Renvoyer un Dataset depuis une série de valeurs entières.
    * @param valeurs Valeurs.
    * @return Dataset avec une colonne x, entière, contenant les valeurs.
    */
   protected Dataset<Row> fromDouble(double[] valeurs) {
      StructType schema = new StructType().add("x", DoubleType, false);
      
      List<Row> rows = new ArrayList<>();

      for(double valeur : valeurs) {
         rows.add(RowFactory.create(valeur));
      }

      return this.session.createDataFrame(rows, schema);
   }
   
   /**
    * Renvoyer un Dataset depuis une série de valeurs entières à probabilités.
    * @param valeurs Valeurs.
    * @param probabilites Probabilités.
    * @return Dataset avec une colonne x, entière, contenant les valeurs<br>
    * et une colonne Px, décimale, contenant les probabilités.
    */
   protected Dataset<Row> fromDouble(double[] valeurs, double[] probabilites) {
      StructType schema = new StructType()
         .add("x", DoubleType, false)
         .add("Px", DoubleType, false);
      
      List<Row> rows = new ArrayList<>();
      
      for(int index=0; index < valeurs.length; index ++) {
         rows.add(RowFactory.create(valeurs[index], probabilites[index]));
      }

      return this.session.createDataFrame(rows, schema);
   }
   
   /**
    * Calculer l'espérance sur un Dataset avec valeurs et probabilités.
    * @param ds Dataset avec colonnes : <br>
    * x  : valeur<br>
    * Px : fréquence<br>
    * @return espérance.
    */
   protected double esperance(Dataset<Row> ds) {
      return ds.agg(sum(col("x").multiply(col("Px")))).first().getDouble(0);
   }
   
   /**
    * Calculer la variance sur un Dataset avec valeurs et probabilités.
    * @param ds Dataset avec colonnes : <br>
    * x  : valeur<br>
    * Px : fréquence<br>
    * @return espérance.
    */
   protected double variance(Dataset<Row> ds) {
      Column variation = col("x").minus(esperance(ds));
      Column variationCarre = variation.multiply(variation);
      Column termeCalculVariance = col("Px").multiply(variationCarre);
      
      return ds.agg(sum(termeCalculVariance)).first().getDouble(0);
   }
   
   /**
    * Créer un Dataset Row par des vecteurs denses constitués à partir d'upplets de valeurs.
    * @param listeUppletsValeurs liste de n-upplets.
    * @return Dataset Dense.
    */
   protected Dataset<Row> denseRowWithVectors(double[]... listeUppletsValeurs) {
      List<Row> data = new ArrayList<>();

      for(double[] upplet : listeUppletsValeurs) {
         Vector vecteur = Vectors.dense(upplet);
         data.add(RowFactory.create(vecteur));
      }

       StructType schema = new StructType(new StructField[] {
         new StructField("features", new VectorUDT(), false, Metadata.empty()),
       });

       return this.session.createDataFrame(data, schema);
   }
   
   /**
    * Renvoyer un ensemble de valeurs doubles sous la forme d'un tableau.
    * @param valeurs Valeurs.
    * @return Tableau de doubles.
    */
   protected double[] d(double... valeurs) {
      return valeurs;
   }
   
   /**
    * Obtenir l'ensemble des individus.
    * @return Individus.
    */
   protected Dataset<Row> individus() {
      // Ne pas retenir les arrondissements, car la commune est déjà présente elle même sous un type "COM".
      // Les arrondissements gardent, en regard, la population totale de leur commune, et ils fausseraient la population totale.
      return this.cogDataset.rowCommunes(null, null, 2019, false).filter("typeCommune <> 'ARM'");
   }

   /**
    * Renvoyer la liste des intercommunalités à fiscalité propre.
    * @return EPCI.
    */
   protected Dataset<Row> intercommunalites() {
      Dataset<Row> ds = this.perimetresDataset.rowPerimetres(null, new HistoriqueExecution(), 2019, true, new EPCIPerimetreTriSirenSiegeCommuneMembreSirenMembre());
      
      return ds.filter((FilterFunction<Row>)groupement
         -> NatureJuridiqueIntercommunalite.fromCode(groupement.getAs("natureJuridique")).isFiscalitePropre());
   }
}
