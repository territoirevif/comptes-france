package fr.ecoemploi.adapters.outbound.spark.stat.inferentielle;

import java.io.*;

import org.apache.commons.math3.distribution.*;
import org.slf4j.*;

import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;
import org.apache.spark.sql.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import fr.ecoemploi.adapters.outbound.spark.stat.*;

/**
 * Les distributions et les lois qu'elles suivent.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkMathStatsTestApplication.class)
class LoisIT extends AbstractStatsTest implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -5336138759791541882L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(LoisIT.class);

   /**
    * Déterminer la distribution de probabilité d'une variable aléatoire X.
    */
   @Test
   @DisplayName("Espérance et variance d'une v.a. X ayant une distribution de probabilité spécifique (la fonction de densité est connue).")
   void distributionVariableAleatoireX() {
      Dataset<Row> ds = fromDouble(new double[] {1, 2, 3, 4, 5, 6}, new double[] {0.1, 0.4, 0.2, 0.1, 0.1, 0.1});
      
      double esperance = esperance(ds);
      double variance = variance(ds);
      
      LOGGER.info("E(X) = {}, V(X) = {}", esperance, variance);
      assertEquals(3.0, esperance, 0.01, "L'espérance de la variable aléatoire est incorrecte.");
      assertEquals(2.2, variance, 0.01, "La variance de la variable aléatoire est incorrecte.");
   }
   
   /**
    * Distribution uniforme.
    */
   @Test
   @DisplayName("E(X) et V(X) d'une v.a. ayant une distribution de probabilité uniforme.")
   void distributionUniformeVA() {
      Dataset<Row> ds = fromDouble(new double[] {5, 6, 7, 8, 9});
      
      double esperance = ds.selectExpr("avg(x)").first().getDouble(0);
      double variance = ds.selectExpr("var_pop(x)").first().getDouble(0);
      
      LOGGER.info("E(X) = {}, V(X) = {}", esperance, variance);
      assertEquals(7, esperance, "L'espérance de la probabilité uniforme est incorrecte.");
      assertEquals(2, variance, "La variance de la probabilité uniforme est incorrecte.");
   }

   /**
    * On lance trois dés. Probabilité d'un six obtenu ?
    * Suit une loi binomiale B(3, 1/6)
    * Par calcul : F(0) = 125/216, F(1) = 125/216 + 75/216, F(2) = (125 + 75 + 15)/216, F(3) = (125 + 75 + 15 + 1)/216 = 1  
    */
   @Test
   @DisplayName("On lance trois dés. Probabilité d'un six obtenu ? Caractéristiques de la loi B(3, 1/6)")
   void troisDesDonnentUnSix() {
      int n = 3;                    // Nombre de lancés de dés.
      double[] v = {0, 1, 2, 3};    // Valeurs possibles de la variable aléatoire.
      double[] f = new double[n+1]; // Fréquences cumulées.
      double[] p = new double[n+1]; // Probabilités.

      // Calculer les probabilités et les fréquences.
      BinomialDistribution loiBinomale = new BinomialDistribution(n, 1.0/6.0);
      
      for(int i=0; i <= n; i++) {
         p[i] = loiBinomale.probability(i);
         f[i] = loiBinomale.cumulativeProbability(i);   
      }

      LOGGER.info("P(x) = {}", p);
      LOGGER.info("F(x) = {}", f);
      
      Dataset<Row> ds = fromDouble(v, p);
      assertNotEquals(0L, ds.count(), "Caractéristiques de la loi B(3, 1/6) aurait dû renvoyer un résultat");

      LOGGER.info("E(X) = {}, V(X) = {}", esperance(ds), variance(ds));
   }
}
