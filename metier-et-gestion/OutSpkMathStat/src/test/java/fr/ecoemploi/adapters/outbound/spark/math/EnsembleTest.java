package fr.ecoemploi.adapters.outbound.spark.math;

import static org.apache.commons.collections4.SetUtils.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

import org.apache.commons.collections4.*;
import org.junit.jupiter.api.*;
import org.slf4j.*;

/**
 * Test sur les ensembles
 * <br>Albèbre (1)
 * <ul>
 * <li>Inclusion</li>
 * <li>Intersection</li>
 * <li>Réunion</li>
 * <li>Ensemble complémentaire</li>
 * </ul>
 * @author Marc Le Bihan
 */
class EnsembleTest {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EnsembleTest.class);

   /**
    * Cardinalités : Card(A) = 10
    */
   @Test
   @DisplayName("Cardinalité : Card(A) = 10")
   void cardinalite() {
      int[] A = {0,1,2,3,4,5,6,7,8,9};
      LOGGER.info("Card({}) = {}", A, A.length);

      assertEquals(10, A.length, "Mauvaise cardinalité de A.");
   }

   /**
    * Appartenance : x ∈ A
    */
   @Test
   @DisplayName("Appartenance : x ∈ A")
   void appartient() {
      Set<Integer> A = new HashSet<>(Arrays.asList(0,1,2,3,4,5,6,7,8,9));
      LOGGER.info("{} ∈ {} : {}", 1, A, A.contains(1));

      assertTrue(A.contains(1), "1 appartient à A.");
   }

   /**
    * C est inclu dans E (C ⊂ E).
    */
   @Test
   @DisplayName("C ⊂ E : C est inclu dans E")
   void inclusion() {
      List<Integer> C = Arrays.asList(0,2,4,6);
      List<Integer> E = Arrays.asList(0,2,3,4,6,8,10);

      boolean C_inclus_dans_E = E.containsAll(C);

      LOGGER.info("{} ⊂ {} : {}", C, E, C_inclus_dans_E);
      assertTrue(C_inclus_dans_E, "C aurait dû être inclu dans E.");
   }

   /**
    * Intersection de A et B (A ∩ B).
    */
   @Test
   @DisplayName("A ∩ B : intersection de A et de B")
   void intersection() {
      Set<Integer> A = new HashSet<>(Arrays.asList(0,1,2));
      Set<Integer> B = new HashSet<>(Arrays.asList(1,3,5));

      Set<Integer> A_intersect_B = SetUtils.intersection(A, B);
      LOGGER.info("{} ∩ {} = {}", A, B, A_intersect_B);

      assertArrayEquals(new Integer[] {1}, A_intersect_B.toArray(A_intersect_B.toArray(new Integer[0])), "Seul l'élément {1} est l'intersection de A et de B.");
   }

   /**
    * Réunion de A et B (A ∪ B).
    */
   @Test
   @DisplayName("A ∪ B : réunion de A et de B")
   void reunion() {
      Set<Integer> A = new HashSet<>(Arrays.asList(0,1,2));
      Set<Integer> B = new HashSet<>(Arrays.asList(1,3,5));

      Set<Integer> A_union_B = union(A, B);
      LOGGER.info("{} ∪ {} = {}", A, B, A_union_B);

      assertArrayEquals(new Integer[] {0,1,2,3,5}, A_union_B.toArray(A_union_B.toArray(new Integer[0])), "Tous les éléments de A et de B auraient dû se trouver dans l'ensemble.");
   }

   /**
    * Le complémentaire de B dans A (CAB).
    */
   @Test
   @DisplayName("∁AB : complémentaire de B dans A.")
   void complementaire() {
      Set<Integer> A = new HashSet<>(Arrays.asList(1,2,3,4));
      Set<Integer> B = new HashSet<>(Arrays.asList(1,2));

      Set<Integer> complementaire_de_B_dans_A = difference(A, B);
      LOGGER.info("C{} {} = {}", A, B, complementaire_de_B_dans_A);

      assertArrayEquals(new Integer[] {3,4}, complementaire_de_B_dans_A.toArray(complementaire_de_B_dans_A.toArray(new Integer[0])), "Le complémentaire de B dans A devrait être {3,4}.");
   }

   /**
    * Exercice 1 : quelles inclusions peut-on écrire ?
    */
   @Test
   @DisplayName("Exercice 1 : Quelles inclusions peut-on écrire ?")
   void exercice1() {
      Set<Integer> A = new HashSet<>(Arrays.asList(3,8,9));
      Set<Integer> B = new HashSet<>(Arrays.asList(6,9,10,8,11));
      Set<Integer> C = new HashSet<>(Arrays.asList(8,10,9,3,11,7));
      assertTrue(C.containsAll(A), "A ⊂ C");
      assertFalse(C.containsAll(B), "B ⊄ C");
   }

   /**
    * Exercice 2 : E est l'ensemble des entiers naturels inférieurs à 10 ; A = {1,4,8,6,2}
    * <ol>
    * <li>Pourquoi a t-on A ⊂ E ?</li>
    * <li>Quel est le complémentaire de A dans E ?</li>
    * <li>Déterminer A ∪ CA et A ∩ CA</li>
    * </ol>
    */
   @Test
   @DisplayName("Exercice 2 : Inclusion et complémentaires")
   void exercice2() {
      Set<Integer> E = new HashSet<>(Arrays.asList(0,1,2,3,4,5,6,7,8,9));
      Set<Integer> A = new HashSet<>(Arrays.asList(1,4,8,6,2));

      assertTrue(E.containsAll(A), "A ⊂ E");
      assertArrayEquals(new HashSet<>(Arrays.asList(0,3,5,7,9)).toArray(new Integer[0]), difference(E, A).toArray(new Integer[0]), "A ⊂ E");
      LOGGER.info("A ∪ CA = {}", union(A, difference(E, A)));
      LOGGER.info("A ∩ CA = {}", SetUtils.intersection(A, difference(E, A)));
   }
}
