/**
 * Tests sur les statistiques descriptives univariées. 
 * @author Marc Le Bihan
 */
package fr.ecoemploi.adapters.outbound.spark.stat.univariee;