package fr.ecoemploi.adapters.outbound.spark.math;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * Test sur les interpolations de Lagrange
 */
class InterpolationLagrangeTest {
   /**
    * Recherche d'un polynôme tel que p(-1) = 8, p(0) = 3, p(1) = 6
    */
   @Test
   void test() {
      InterpolationLagrange p = new InterpolationLagrange(List.of(-1.0, 0.0, 1.0), List.of(8.0, 3.0, 6.0));

      assertEquals(17.0, p.interpolation(2), 0.001, "mauvaise interpolation pour y = 2");
      assertEquals(36.0, p.interpolation(3), 0.001, "mauvaise interpolation pour x = 3");
   }
}
