package fr.ecoemploi.adapters.outbound.spark.stat.inferentielle;

import org.apache.commons.math3.distribution.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import org.slf4j.*;
import org.springframework.test.context.ActiveProfiles;

/**
 * Estimation ponctuelle et intervalle de confiance
 * (depuis Statistique Inférentielle, Chapitre 1 p.9 à 16, R. Abdesselam, Ellipse 2020)
 */
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // Pour suivre l'ordre des exercices
class EstimationPonctuelleEtIntervalleDeConfianceTest {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EstimationPonctuelleEtIntervalleDeConfianceTest.class);

   /**
    * Recherche de valeurs dans une table normale... (exercice 1, question a)<br>
    * P(X > 8.5), P(X ≦ 7.5), P(6.5 < X ≦ 10), P[(X > 6)/(X > 5)]
    */
   @Test @Order(1)
   @DisplayName("Emploi de la table normale")
   void tableNormale() {
      LOGGER.info("==> Exercice 1 : Lecture de table normale, question a");

      // X ∼ N(μ=8, σ²=16)
      NormalDistribution N = new NormalDistribution(8, 4); // μ=8, σ=4
      LOGGER.info("Quand on suit une loi normale N(μ=8, σ=4)");

      // P(X > 8.5)
      double p1 = 1 - N.cumulativeProbability(8.5);
      LOGGER.info("\tP(X > 8.5) vaut {}", p1);
      assertEquals(0.45, p1, 0.01, "Mauvaise probabilité de P(X > 8.5)");

      // P(X ≦ 7.5)
      double p2 = N.cumulativeProbability(7.5);
      LOGGER.info("\tP(X ≦ 7.5) vaut {}", p2);
      assertEquals(0.45, p2, 0.01, "Mauvaise probabilité de P(X ≦ 7.5)");

      // P(6.5 < X ≦ 10)
      double p3 = N.probability(6.5, 10);
      LOGGER.info("\tP(6.5 < X ≦ 10) vaut {}", p3);
      assertEquals(0.338, p3, 0.01, "Mauvaise probabilité de P(6.5 < X ≦ 10)");

      // P[(X > 6)/(X > 5)]
      double xSup5 = 1 - N.cumulativeProbability(5);
      double xSup6 = 1 - N.cumulativeProbability(6);

      // Remarque : une probabilité conditionnelle aurait été :
      // double p4 = (xSup5 * xSup6) / xSup5; // P(B | A) = P(A ∩ B) / P(A)

      double p4 = xSup6 / xSup5;
      LOGGER.info("\tP[(X > 6)/(X > 5)] vaut {}", p4);
      assertEquals(0.894, p4, 0.01, "Mauvaise probabilité pour P[(X > 6)/(X > 5)]");
   }

   /**
    * estimation de l'espérance mathématique m et de variance (σ²) d'une loi normale depuis deux valeurs (exercice 1, questions b et c) :<br>
    * P(X > 3) = 0.0912 et P(X ≦ -1) = 0.0912<br><br>
    *
    * Puis calcul de la probabilité de P(X > 0)<br>
    * enfin de x, tel que P(X < x) = 0.036<br>
    */
   @Test @Order(2)
   @DisplayName("Estimation moyenne, variance loi normale depuis P")
   void estimationMoyenneVarianceLoiNormaleDepuisP() {
      LOGGER.info("==> Exercice 1 : Lecture de table normale, questions b et c");

      // On va se servir de la valeur centrée réduite qui suit une loi N(0,1)
      // qui permet de substituer à X une valeur Y = (X - µ) / σ
      NormalDistribution N = new NormalDistribution(0, 1);    // μ=0, σ=1
      double t1 = N.inverseCumulativeProbability(0.0912);     // pour P(X ≦ -1) = 0.0912
      double t2 = N.inverseCumulativeProbability(1 - 0.0912); // pour P(X > 3) = 0.0912

      // Les deux équations qui ressortent de ces calculs sont :
      //    3 - µ =  1.334σ (L1)
      //   -1 - µ = -1.334σ (L2)
      // et l'on résout ce système en calculant L1 - L2 qui supprime µ et donne la valeur de σ :
      double sigma = (3.0 - (-1.0)) / (t1 - t2);
      double sigmaCarre = sigma * sigma;
      double mu = 3.0 - t1 * sigma;

      LOGGER.info("Si P(X ≦ -1) = P(X > 3) = 9.12%, alors c'est que X suit une loi normale d'espérance µ = {} et σ² = {}", mu, sigma);
      assertEquals(1, mu, 0.01, "Mauvaise espérance pour une loi normale qui respecterait P(X ≦ -1) = P(X > 3) = 9.12%"); // µ = 1
      assertEquals(1.5 * 1.5, sigmaCarre, 0.01, "Mauvaise variance pour l'espérance mᵧ et variance σ² de X, sachant que P(X ≦ -1) = P(X > 3) = 9.12%"); // σ² = 1.5²

      // Probabilité que P(X > 0) [sachant donc que X ~ N(µ=1, σ²=1.5²)]
      // C'est 1 - F(0)
      LOGGER.info("Maintenant nous savons que X ~ N(µ = 1, σ = 1.5)");
      N = new NormalDistribution(1, 1.5); // μ=1, σ=1.5
      double pSuperieur0 = 1 - N.cumulativeProbability(0);
      LOGGER.info("\tP(X > 0) = {}", pSuperieur0);
      assertEquals(0.7475, pSuperieur0, 0.01, "Mauvaise P(X > 0) [sachant que X ~ N(µ=1, σ²=1.5²)]");

      // Et x, tel que P(X < x) = 0.036
      double x = N.inverseCumulativeProbability(0.036);
      LOGGER.info("\tP(X < {}) = 0.036", x);
      assertEquals(-1.7, x, 0.01, "Mauvais x tel que P(X < x) = 0.036 [sachant que X ~ N(µ=1, σ²=1.5²)]");

      // Remarque : le livre "Statistique Inférentielle", Abdesslam, ellipses 2020 dit -2, mais c'est visiblement -1.7
      // (cf. https://stats.stackexchange.com/questions/581932/amazed-by-a-calculation-of-x-such-as-px-x-3-60-when-x-sim-n%c2%b5-1/581950#581950)
   }

   /**
    * Estimation d'un Khi-Deux : χ(ddl=2, non-centralité=15)...(exercice 1, question d)
    * Le Khi-deux sert aux tests d'indépendance, tests d'ajustement (un échantillon suit-il une distribution particulière ?)...<br>
    *
    * P(Z ≦ z) = 0.99, encadrer au mieux P(Z ≦ 20)<br>
    */
   @Test @Order(3)
   @DisplayName("Emploi de la table du Khi-Deux")
   void tableKhiDeux() {
      LOGGER.info("==> Exercice 1 : Lecture de table du Khi-Deux, question d");
      LOGGER.warn("Ce test est mal maîtrisé, et devrait être considéré comme faux");

      // Z ~ χ(ddl=15, non-centralité=2)
      ChiSquaredDistribution KhiDeux = new ChiSquaredDistribution(15, 2);
      LOGGER.info("Quand on suit une loi de Khi-deux, Z ~ χ(ddl=15, non-centralité=2)");

      double z = KhiDeux.inverseCumulativeProbability(0.99);
      LOGGER.info("\tP(Z ≦ {}) = 0.99", z);
      assertEquals(30.58, z, 0.2, "Mauvais z tel que P(Z ≦ z) = 0.99, quand Z suit χ(ddl=15, non-centralité=2)");

      // double z1, z2;

      // Encadrer au mieux P(Z ≦ 20)
      double z3 = KhiDeux.cumulativeProbability(20.0);
      LOGGER.info("\tP(Z ≦ 20) = {}", z3);
      assertEquals(0.85, z3, 0.05, "Mauvais P(Z ≦ 20), quand Z suit χ(ddl=15, non-centralité=2)");
   }

   /**
    * Emploi de la table de Student : T ~ T(10)... (exercice 1, question e)<br>
    * P(|T| > t) = 0.05, P(T > t1] = 0.95, P(T < t2) = 0.99<br>
    */
   @Test @Order(4)
   @DisplayName("Emploi de la table de Student")
   void tableStudent() {
      LOGGER.info("==> Exercice 1 : Lecture de table de Student, question e");

      TDistribution Student = new TDistribution(10); // T ~ T(10)
      LOGGER.info("Quand on suit une loi de Student, T ~ T(10)");

      // P(|T| > t) = 0.05
      //   à résoudre en P(T > t) = 0.025 et P(T ≦ -t) = 0.025
      double ta = Student.inverseCumulativeProbability(1 - 0.025);
      double tb = - Student.inverseCumulativeProbability(0.025);

      LOGGER.info("\tP(|T| > t) = 0.05 ⟺ P(T > {}) = 0.025 et P(T ≦ {}) = 0.025", ta, tb);
      assertEquals(2.228, ta, 0.01, "Mauvais P(|T| > t) = 0.05, quand T suit T(10)");

      // P(T > t1] = 0.95
      double t1 = Student.inverseCumulativeProbability(1 - 0.95);
      LOGGER.info("\tP(T > {}] = 0.95", t1);
      assertEquals(-1.812, t1, 0.01, "Mauvais P(T > t1] = 0.95, quand T suit T(10)");

      // P(T < t2) = 0.99
      double t2 = Student.inverseCumulativeProbability(0.99);
      LOGGER.info("\tP(T ≦ {}) = 0.99", t2);
      assertEquals(2.764, t2, 0.01, "Mauvais P(T ≦ t2) = 0.99, quand T suit T(10)");
   }

   /**
    * Emploi de la table de Fisher-Snédécor (exercice 2)<br>
    * Pourvue de deux degrés de liberté pour paramètres, elle est utilisée :<br>
    *    - pour l'analyse ou la comparaison de variances<br><br>
    *
    * P[F ≦ f1] = 0.975, P[F > f2] = 0.975, P[f3 ≦ F ≦ f4]<br>
    */
   @Test @Order(5) @Disabled("Ma compréhension du test de Fisher-Snédécor n'est pas bonne") // FIXME Exercice 1.5 non réussi
   @DisplayName("Emploi de la table de Fisher-Snédécor")
   void tableFisherSenedecor() {
      LOGGER.info("==> Exercice 2 : Lecture de table de Fisher-Snédécor, question e");

      FDistribution F = new FDistribution(4, 10); // degrés de libertés : 4 et 10
      LOGGER.info("Quand on suit une loi de Fisher-Senedecor F ~ F(4, 10)");

      // P[F ≦ f1] = 0.975 : test unilatéral à droite
      double f1 = F.inverseCumulativeProbability(0.975);
      LOGGER.info("\tLe test unilatéral à droite, P[F ≦ {}] = 0.975", f1);
      assertEquals(4.47, f1, 0.01, "Mauvais P[F ≦ f] = 0.975 pour une loi de Fisher-Snédécor(4, 10)");

      // P[F > f2] = 0.975 : test unilatéral à gauche
      double f2 = F.inverseCumulativeProbability(1 - 0.975);
      LOGGER.info("\tLe test unilatéral à gauche, P[F > {}] = 0.975", f2);
      assertEquals(0.1131, f2, 0.01, "Mauvais P[F > {}] = 0.975 pour une loi de Fisher-Snédécor(4, 10)");

      // P[f3 ≦ F ≦ f4] = 0.98
      double f3 = - F.inverseCumulativeProbability(1 - (0.98/2.0));
      double f4 = F.inverseCumulativeProbability(0.98 / 2.0);
      LOGGER.info("\tLe test bilatéral symétrique, P[{} ≦ F ≦ {}] = 0.98", f3, f4);
      assertEquals(0.069, f3, 0.01, "Mauvaise valeur de f3 pour P[f3 ≦ F ≦ f4] = 0.98 pour une loi de Fisher-Snédécor(4, 10)");
      assertEquals(0.599, f4, 0.01, "Mauvaise valeur de f4 pour P[f3 ≦ F ≦ f4] = 0.98 pour une loi de Fisher-Snédécor(4, 10)");
   }

   /**
    * X une variable aléatoire associée au poids en grammes d'un paquet de chips, X ~ N(µ=150g, σ=20g)... (exercice 6)<br>
    * <pre>
    *    - poids moyen d'un échantillon, d'après sa taille
    *    - chance que son poids soit inférieure à 140g
    *    - que le poids total des échantillons soit compris entre 2095 et 2405 grammes
    *    - qu'un paquet pèse moins de 140 grammes
    *    - et le nombre de paquets de chips qu'il faut contrôler pour avoir un poids moyen compris entre 144.5 et 155.5 grammes, dans 90% des cas
    * </pre>
    */
   @Test @Order(6)
   @DisplayName("Autour du poids en grammes d'un paquet de chips : estimation d'une moyenne")
   void paquetDeChips() {
      LOGGER.info("==> Exercice 6 : le poids moyen d'un paquet de chips dans un échantillon");

      NormalDistribution N = new NormalDistribution(150, 20);
      LOGGER.info("X est une v.a. associée au poids en grammes d'un paquet de chips, et suit N(µ=150g, σ=20g),");
      LOGGER.info("et l'on prélève un échantillon de 15 paquets.");

      // Prendre en compte la taille de l'échantillon, de 15 paquets, pour dire quel sera ̅X, le poids moyen d'un paquet de chips

      // n est petit, σ est connu (X ~ N(µ=150g, σ=20g))
      // ̅X ∼ N(µ, σ/√n) : variance σ divisée par la racine de l'effectif de l'échantillon.
      double ecartTypeEchantillon15 = 20.0 / Math.sqrt(15.0);
      LOGGER.info("\tLe poids moyen de l'échantillon de 15 paquets, ̅X, suit une loi N(150, {})", ecartTypeEchantillon15);
      assertEquals(5.16398, ecartTypeEchantillon15, 0.01, "mauvais écart-type pour le poids moyen ̅X que suit un d'échantillon de 15 paquets");

      NormalDistribution NX = new NormalDistribution(150, ecartTypeEchantillon15);

      // Quand on choisit un échantillon de 15 paquets, quelle est la chance que leur poids moyen soit inférieur à 140g ?
      double poidsInferieurA140g = NX.cumulativeProbability(140);
      LOGGER.info("\tQuand on choisit un échantillon de 15 paquets, la chance que leur poids moyen soit inférieur à 140g est de {}", poidsInferieurA140g);
      assertEquals(0.0262, poidsInferieurA140g, 0.01, "mauvaise probabilité qu'un échantillon de 15 paquets de chips ait un poids moyen ̅X inférieur à 140g");

      // Quelle est la chance que le poids total de ces paquets de chips soit comprise entre 2095 et 2405 grammes ?
      double poidsMinimal = 2095.0 / 15.0;
      double poidsMaximal = 2405.0 / 15.0;
      double poidsInferieurAuMaximal = NX.cumulativeProbability(poidsMaximal); // ̅X < 2405 / 15 : on voudra le retenir.
      double poidsInferieurAuMinimal = NX.cumulativeProbability(poidsMinimal); // ̅X < 2095 / 15 : on voudra l'écarter.
      double probabilitePoidsComprisEntre2095et2405grammes = poidsInferieurAuMaximal - poidsInferieurAuMinimal;
      LOGGER.info("\tLa chance que la sommes des 15 paquets de chips soit comprise entre 2095 et 2405 grammes est de {}", probabilitePoidsComprisEntre2095et2405grammes);
      assertEquals(0.9544, probabilitePoidsComprisEntre2095et2405grammes, 0.01, "mauvaise probabilité que la somme des poids d'échantillon de 15 paquets de chips soit comprise entre 2095 et 2405 grammes");

      // Quelle est la chance qu'un paquet pèse moins de 140 grammes ?
      double probabiliteUnPoidsInferieurA140g = N.cumulativeProbability(140);
      LOGGER.info("\tLa probabilité qu'un paquet de chips pèse moins de 140 grammes est de {}", probabiliteUnPoidsInferieurA140g);
      assertEquals(0.3086, probabiliteUnPoidsInferieurA140g, 0.01, "mauvaise probabilité qu'un paquet de chips pèse moins de 140 grammes");

      // Combien de paquets de chips faut-il contrôler pour avoir un poids moyen compris entre 144.5 et 155.5 grammes dans 90% des cas?
      LOGGER.info("Avoir un poids moyen compris entre 144.5 et 155 grammes, c'est chercher un écart de 5.5 au maximum");
      LOGGER.info("Dans cette question, on veut que ce soit le cas dans 90% (=0.9) des cas");

      // m = ̅x ± k.(σ / √n)
      // on doit trouver k (par exemple, k = 2 pour 0.95 (=95%) de chances, mais là c'est 0.9 que l'on veut.
      // ̅x on connait, c'est 150, et σ aussi : c'est le 20 grammes de la loi normale de départ.
      // on calculera n depuis soit : 144.5 = 150 - k(20 / √n) ou 155.5 = 150 + k(20 / √n)

      // La fonction inverse normale de 0.9 sur N(0,1), elle va donner l'aire qu'il y aurait de -∞ à 0.9, ce qui ne nous arrange pas
      // parce que l'on veut un intervalle centré en zéro.
      // et lui il se calcule sur la base de P(-t < X < t) = 2ϕ(t) - 1, c'est à dire que si on cherche 2ϕ(t) - 1 = 0.9 ça fait ϕ(t) = 0.95
      // et ϕ(t) c'est "la fonction inverse de la normale N(0,1)" à appliquer
      double phi = (1 + 0.9) / 2;

      NormalDistribution NCentreeReduite = new NormalDistribution(0, 1);
      double k = NCentreeReduite.inverseCumulativeProbability(phi);
      LOGGER.info("\tL'intervalle centré en 0 dans N(0,1) qui correspond à une probablité de 0.9, se repère avec k = {}", k);
      LOGGER.info("\tc'est à dire que m = 150 ± {}(20 / √n)", k);

      // Maintenant on peut résoudre 155.5 = 150 + k(20 / √n), par exemple
      double effectif = Math.pow((k * 20) / (155.5 - 150), 2);
      int effectifArrondiSuperieur = (int)Math.ceil(effectif);
      LOGGER.info("\tIl faudra un échantillon d'éffectif d'au moins {} unités pour être assurés que la moyenne des poids des paquets ait 90% de chances d'être comprise entre 144.5 et 155.5 grammes.", effectifArrondiSuperieur);
      assertEquals(36, effectifArrondiSuperieur, "mauvaise taille d'échantillon pour s'assurer que les paquets ait 90% de chances d'avoir une moyenne de poids comprise entre 144.5 et 155.5 grammes");
   }

   /**
    * La société croissant chaud veut lancer un produit dont une enquête montre 18% d'intéressés... (exercice 7)<br>
    * (Étude autour d'un intervalle de confiance basé sur une proportion)<br>
    * <pre>
    *    - calcul d'un intervalle de confiance à 95% autour d'une proportion
    *    - calcul de l'effectif qu'il faudrait pour conserver cette confiance à 95%, mais réduire l'amplitude de l'intervalle de moitié
    * </pre>
    */
   @Test @Order(7)
   @DisplayName("La société croissant chaud : estimation d'une proportion")
   void croissantChaud() {
      LOGGER.info("==> Exercice 7 : un intervalle de confiance autour de la proportion des clients qui apprécieraient un nouveau type de croissants");

      LOGGER.info("La société croissant chaud veut lancer un produit dont une enquête montre 18% d'intéressés.");
      LOGGER.info("Ce qui nous est donné, ici, c'est une proportion.");
      LOGGER.info("On sait que l'échantillon des sondés fait 2500 personnes. Il suit une loi normale dont on ne connaît ni µ ni σ.");

      double phi = (1 + 0.95) / 2;
      NormalDistribution NCentreeReduite = new NormalDistribution(0, 1);
      double u = NCentreeReduite.inverseCumulativeProbability(phi);

      LOGGER.info("\tUn intervalle de confiance à 95% va se reprérer dans une loi normale N(0,1) avec un u = {}", u);
      assertEquals(1.96, u, 0.01, "mauvaise borne pour le choix de l'intervalle centré en zéro, u, dans N(0,1)");

      // l'estimation peut varier dans l'intevalle p = f ± u.sqrt[(f.(1 − f)) / n]
      // et l'on sait que f = 0.18 (18%), u = 1.96, et n = 2500.
      double amplitudeIntervalleConfiance = u * Math.sqrt((0.18 * (1-0.18))/2500);
      LOGGER.info("\tL'intervalle de confiance de la proportion 18% est : 0.18 ± {}", amplitudeIntervalleConfiance);
      LOGGER.info("\tsoit compris dans l'intevalle [{}, {}]", 0.18 - amplitudeIntervalleConfiance, 0.18 + amplitudeIntervalleConfiance);
      assertEquals(0.165, 0.18 - amplitudeIntervalleConfiance, 0.01, "Mauvaise borne basse d'intervalle de confiance autour de 18% pour un échantillon de 2500 personnes");
      assertEquals(0.195, 0.18 + amplitudeIntervalleConfiance, 0.01, "Mauvaise borne haute d'intervalle de confiance autour de 18% pour un échantillon de 2500 personnes");

      LOGGER.info("Quelle taille d'échantillon choisir pour réduire cet intervalle de confiance de moitié ?");
      LOGGER.info("\tOn ne peut pas changer notre précision, et u va rester égal à {} (95%)", u);

      // Tout à l'heure, nous avons trouvé que u.sqrt[(f.(1 − f)) / n] en valeur absolue, c'est à dire 1.96.sqrt[(0.18.(1 − 0.18)) / 2500] valait 0.015,
      // et l'on veut qu'il vale la moitié de cela, à présent, sachant qu'on ne peut jouer que sur n.
      amplitudeIntervalleConfiance = amplitudeIntervalleConfiance / 2;
      double a = 0.18 * (1.0 - 0.18);
      double b = Math.pow(amplitudeIntervalleConfiance / u, 2);
      double n = Math.ceil(a / b);
      LOGGER.info("\tIl faudra un échantillon de {} personnes au moins pour réduire l'amplitude de cet intervalle de confiance de moitié tout en gardant 95% de chances qu'il soit juste.", n);
      assertEquals(10000, n, 1, "Mauvais échantillon pour réduire de moitié l'amplitude de cet intervalle de confiance tout en gardant 95% de chances qu'il soit juste");
   }

   /**
    * Un atelier fabrique des pièces de bois destinées au montage des meubles en pin... (exercice 8)<br>
    * (étude autour de la variance)<br>
    * <pre>
    *    - a) quelle est la probabilité que la hauteur moyenne d'un échantillon de 49 pièces fasse plus de 22.3 mm ?
    *    - b) comment modifier l'écart-type de la hauteur d'une pièce pour que cette hauteur moyenne soit comprise entre 21.72 et 22.28 mm ?
    * </pre>
    */
   @Test @Order(8)
   @DisplayName("Dans un atelier qui fabrique des pièces : estimation d'une variance")
   void dansUnAtelier() {
      LOGGER.info("==> Exercice 8 : Dans un atelier, on fabrique des pièces de bois d'espérance mathématique 22 mm et d'écart-type 2 mm.");
      // NormalDistribution N = new NormalDistribution(22, 2);

      LOGGER.info("On prélève un échantillon de 49 pièces.");
      LOGGER.info("a) Quelle est la probabilité que la hauteur moyenne sur cet échantillon dépasse 22.3 mm ?");

      // On sait que la moyenne est centrée sur 22 mm. Mais quel est son écart-type ?
      // ̅X ∼ N(µ, σ/√n) avec µ = 22 mm, σ = 2 mm, et n = 49
      double ecartTypeMoyenne = 2.0 / Math.sqrt(49.0);

      LOGGER.info("\tLa moyenne de cet échantillon de 49 pièces suit une loi ~ ̅X ∼ N(µ, {})", ecartTypeMoyenne);
      NormalDistribution NX = new NormalDistribution(22, ecartTypeMoyenne);

      // P(m > 22.3) = 1 - P(m ≦ 22.3)
      double hauteurMoyenneSuperieure = 1 - NX.cumulativeProbability(22.3);
      LOGGER.info("\tLa probabilité que la hauteur moyenne de cet échantillon dépasse 22.3 est de {}", hauteurMoyenneSuperieure);
      assertEquals(0.1469, hauteurMoyenneSuperieure, 0.01, "Mauvaise probabilité que P(m > 22.3)");

      // Quel écart-type pour une hauteur moyenne comprise entre 21.72 et 22.28 mm ?
      LOGGER.info("Le chef d'atelier souhaite que 95% des échantillons de 49 pièces donnent une hauteur moyenne comprise entre 21.72 et 22.28 mm");
      LOGGER.info("b) Comment faut-il modifier l'écart-type de la hauteur d'une pièce pour obtenir ce résultat ?");

      double phi = (1 + 0.95) / 2; // L'on recherche un intervalle de confiance à 95%
      NormalDistribution NCentreeReduite = new NormalDistribution(0, 1); // Sur la loi normale centrée réduite
      double u = NCentreeReduite.inverseCumulativeProbability(phi);      // u est à lire sur la courbe
      double borneInfActuelle = 22.0 - u * ecartTypeMoyenne;
      double borneSupActuelle = 22.0 + u * ecartTypeMoyenne;
      LOGGER.info("\tL'actuel intervalle de confiance est, à 95%, de m = 22 ± {} × {} = 22 ± {}, soit [{},{}]",
         u, ecartTypeMoyenne, u * ecartTypeMoyenne, borneInfActuelle, borneSupActuelle);

      LOGGER.info("\tIl suffit de déterminer la valeur pour 22 - {} × σ = 21.72 et 22 + {} × σ = 22.28", u, u);
      LOGGER.info("\tau lieu de 22 - {} × {} = {} et 22 + {} × {} = {} comme actuellement.", u, ecartTypeMoyenne, borneInfActuelle, u, ecartTypeMoyenne, borneSupActuelle);

      double ecartTypeMoyenneCible = borneInfActuelle / (22 - u);
      LOGGER.info("\tσ = {22 - {}} = {}. ", u, ecartTypeMoyenneCible);

      LOGGER.info("L'écart-type qui convient pour une hauteur moyenne de pièce comprise entre [21.72, 22.28] mm est σ = {}", ecartTypeMoyenneCible);
      assertEquals(1, ecartTypeMoyenneCible, 0.1, "Mauvais écart-type pour une hauteur moyenne de pièce comprise entre [21.72, 22.28] mm");

      /* Cette partie était inexacte, avec une tentative échouée par le Khi-Deux :

         // à 95% de confiance, on aura α = 0.05 => a = α/2 = 0.025 et b = 1 - α/2 =& 0.975 dans la courbe du χ²(48)
         LOGGER.info("\tà 95% de confiance, on aura α = {} => a = α/2 = {} et b = 1 - α/2 = {} dans la courbe du χ²({})", 0.05, 0.05/2.0, (1-0.05/2.0), 49-1);
         ChiSquaredDistribution Chi = new ChiSquaredDistribution(48);
         double a = Chi.inverseCumulativeProbability(0.025);
         double b =  Chi.inverseCumulativeProbability(0.975);
         LOGGER.info("\tc'est à dire : a = {} et b = {}", a, b); */
   }

   /**
    * Un confiseur vend des boites de chocolats... (exercice 9)
    * <pre>
    *    a) donner un intervalle de confiance de niveau 1 - α = 95% de m, le poids moyen d'une boite
    *    b) Sur la boite, est affiché un poids net de 1 kg. Peut-on conclure, au risque de 5%, à une publicité mensongère ?
    *    c) En supposant que la variance de X soit connue : celle que nous avons observée, donner un intervalle de confiance à 1 - alpha = 95% et le comparer au précédent.
    * </pre>
    */
   @Test @Order(9)
   @DisplayName("Un confiseur vend des boites de chocolats")
   void unConfiseurVendDesChocolats() {
      LOGGER.info("==> Exercice 9 : Un confiseur vend des boites de chocolats de poids normalement distribué.");
      LOGGER.info("Les pesées de 8 boites donnent un poids moyen de 0.995 et un écart-type de 0.015.");

      // a) Donner un intervalle de confiance de niveau 1 - α = 95% de m, le poids moyen d'une boite

      // la moyenne suit ̅X ∼ N(µ, σ/√n) : variance σ divisée par la racine de l'effectif de l'échantillon
      double ecartTypeEchantillon8 = 0.015 / Math.sqrt(8);
      LOGGER.info("\tLe poids moyen d'un l'échantillon de 8 boites de chocolats, ̅X, suit une loi N(0.095, {})", ecartTypeEchantillon8);

      // pour trouver un intervalle à 95% de confiance, m = ̅x ± u.(σ / √n), il faut déterminer u
      // l'inverse de 0.95 sur N(0,1) donnerait la probabilité d'une aire de 0.95, mais non centrée sur zéro, alors que c'est ce dont on a besoin.
      // ce que nous cherchons, c'est P(-t < X < t) = 2ϕ(t) - 1, c.à.d 2ϕ(t) - 1 = 0.95 => ϕ(t) = 0.975
      // et ϕ(t) c'est "la fonction inverse de la normale N(0,1)" à appliquer
      double phi = (1 + 0.95) / 2;
      NormalDistribution NCentreeReduite = new NormalDistribution(0, 1);
      double u = NCentreeReduite.inverseCumulativeProbability(phi);

      // maintenant on peut appliquer m = ̅x ± u.(σ / √n)
      LOGGER.info("\ta) En résolvant par une loi de student, l'intervalle à 95% de confiance du poids moyen d'une boite de chocolats d'un échantillon de huit boites");

      // On va adresser une distribution de Student de 7 degrés de liberté (effectif de 8 boites dans l'échantillon)
      // avec phi = (1 - 0.95) / 2 = 0.25
      TDistribution Student = new TDistribution(7);
      // phi = (1 + 0.95) / 2; reste le même
      double t = Student.inverseCumulativeProbability(phi);
      LOGGER.info("\tEn résolvant par une loi de Student (n petit, sigma inconnu), l'intervalle à 95% de confiance du poids moyen d'une boite de chocolats d'un échantillon de huit boites");
      double borneInfIntervalleConfianceMoyenneBoiteChocolat = 0.995 - t * ecartTypeEchantillon8;
      double borneSupIntervalleConfianceMoyenneBoiteChocolat = 0.995 + t * ecartTypeEchantillon8;
      LOGGER.info("\test m = 0.995 ± 0.{}.{}, c'est à dire [{}, {}]", ecartTypeEchantillon8, t, borneInfIntervalleConfianceMoyenneBoiteChocolat, borneSupIntervalleConfianceMoyenneBoiteChocolat);
      assertEquals(0.982, borneInfIntervalleConfianceMoyenneBoiteChocolat, 0.01, "Mauvaise borne inféreure de l'intervalle à 95% de confiance du poids moyen d'un échantillon de huit boites de chocolats, par un calcul via la loi de Student");
      assertEquals(1.008, borneSupIntervalleConfianceMoyenneBoiteChocolat, 0.01, "Mauvaise borne supérieure de l'intervalle à 95% de confiance du poids moyen d'un échantillon de huit boites de chocolats, par un calcul via la loi de Student");

      // b) Sur la boite, est affiché un poids net de 1 kg. Peut-on conclure, au risque de 5%, à une publicité mensongère ?

      LOGGER.info("\tb) Sur la boite, est affiché un poids net de 1 kg. Peut-on conclure, au risque de 5%, à une publicité mensongère ?");
      LOGGER.info("\t1 appartient à l'intervalle de confiance précédemment calculé à 5% d'erreur. Il n'y a donc pas tromperie");

      // c) En supposant que la variance de X soit connue : celle que nous avons observée, donner un intervalle de confiance à 1 - alpha = 95% et le comparer au précédent.

      LOGGER.info("\tc) En supposant que la variance de X soit connue : celle que nous avons observée, donner un intervalle de confiance à 1 - alpha = 95% et le comparer au précédent.");

      // maintenant on appliquer m = ̅x ± u.(σ / √n)
      LOGGER.info("\tEn résolvant par une loi normale, l'intervalle à 95% de confiance du poids moyen d'une boite de chocolats d'un échantillon de huit boites");
      borneInfIntervalleConfianceMoyenneBoiteChocolat = 0.995 - u * ecartTypeEchantillon8;
      borneSupIntervalleConfianceMoyenneBoiteChocolat = 0.995 + u * ecartTypeEchantillon8;

      LOGGER.info("\test m = 0.995 ± {}.{}, c'est à dire [{}, {}]", ecartTypeEchantillon8, u, borneInfIntervalleConfianceMoyenneBoiteChocolat, borneSupIntervalleConfianceMoyenneBoiteChocolat);
      assertEquals(0.982, borneInfIntervalleConfianceMoyenneBoiteChocolat, 0.01, "Mauvaise borne inféreure de l'intervalle à 95% de confiance du poids moyen d'un échantillon de huit boites de chocolats, par un calcul via la loi Normale");
      assertEquals(1.008, borneSupIntervalleConfianceMoyenneBoiteChocolat, 0.01, "Mauvaise borne supérieure de l'intervalle à 95% de confiance du poids moyen d'un échantillon de huit boites de chocolats, par un calcul via la loi Normale");
   }
}
