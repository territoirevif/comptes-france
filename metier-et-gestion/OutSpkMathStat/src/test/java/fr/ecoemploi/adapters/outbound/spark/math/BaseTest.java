package fr.ecoemploi.adapters.outbound.spark.math;

import org.junit.jupiter.api.*;
import org.slf4j.*;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Bases
 */
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // Pour suivre l'ordre des exercices
class BaseTest {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(BaseTest.class);

   /**
    * Conversions de base
    */
   @Test @Order(1)
   @DisplayName("Conversions de bases")
   void bases() {
      String depart = "259";
      String base16 = conversion(depart, 10, 16);

      LOGGER.info("{} décimal en base 16 = {}", depart, base16);
      assertEquals("103", base16, "Mauvaise conversion hexadécimale");

      // 259 = 8 x 29 + 27 = 8R (A = 10, B = 11, C = 12 ... R = 27)
      String base29 = conversion(base16, 16, 29);
      LOGGER.info("{} base 16 en base 29 = {}", base16, base29);
      assertEquals("8r", conversion("103", 16, 29), "Mauvaise conversion de base 16 en base 29");
   }

   /**
    * Conversion d'une base en une autre.
    * @param nombre Nombre à convertir.
    * @param baseDepart Base de départ.
    * @param baseArrivee Base d'arrivée.
    * @return Nombre en base d'arrivée.
    */
   private String conversion(String nombre, int baseDepart, int baseArrivee)
   {
      return Integer.toString(Integer.parseInt(nombre, baseDepart), baseArrivee);
   }
}
