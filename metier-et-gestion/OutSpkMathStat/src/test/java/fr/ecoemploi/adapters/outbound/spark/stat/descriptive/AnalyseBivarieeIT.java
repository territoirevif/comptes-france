package fr.ecoemploi.adapters.outbound.spark.stat.descriptive;

import java.io.*;

import org.slf4j.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import org.apache.spark.ml.stat.*;
import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.adapters.outbound.spark.stat.*;

/**
 * Analyse bivariée.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkMathStatsTestApplication.class)
class AnalyseBivarieeIT extends AbstractStatsTest implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3509391026632353044L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AnalyseBivarieeIT.class);

   /**
    * Le nombre de compétences exercées par une intercommunalité a t-il un lien avec sa population ?
    */
   @Test
   @DisplayName("Lien entre population d'une intercommunalité et nombre de compétences exercées")
   void competencesEtTailleEPCI() {
      double correlation = intercommunalites().stat().corr("nombreDeCompetencesExercees", "populationGroupement", "pearson");

      Dataset<Row> intercommunalites = intercommunalites().selectExpr("nomGroupement", "nombreDeCompetencesExercees", "populationGroupement");
      intercommunalites.show(false);

      long nombreIntercommunalites = intercommunalites.count();
      assertNotEquals(0L, nombreIntercommunalites, "Une intercommunalité au moins aurait dû être lue pour les compétences exercées");
      
      LOGGER.info("Correlation entre le nombre de compétences des {} intercommunalités et leur poulation : {}", nombreIntercommunalites, correlation);
   }

   /**
    * Le nombre de communes d'une intercommunalité est-elle en rapport avec sa population ?
    */
   @Test
   @DisplayName("Lien entre le nombre de communes d'une intercommunalité et sa population")
   void nombreCommunesEtpopulationEPCI() {
      assertDoesNotThrow(() -> {
         double correlation = intercommunalites().stat().corr("nombreCommunesMembres", "populationGroupement", "pearson");
         intercommunalites().selectExpr("nomGroupement", "nombreCommunesMembres", "populationGroupement").show(false);

         LOGGER.info("Correlation entre le nombre de communes des intercommunalités et leur population : {}", correlation);
      }, "Corrélation entre nombre de communes d'une intercommunalité et sa population échouée");
   }

   /**
    * Le nombre de communes d'une intercommunalité est-elle en rapport avec son nombre de compétences ?
    */
   @Test
   @DisplayName("Lien entre le nombre de compétences et nombre de communes d'une intercommunalité")
   void nombreCompetencesEtNombreCommunesEPCI() {
      assertDoesNotThrow(() -> {
         double correlation = intercommunalites().stat().corr("nombreDeCompetencesExercees", "nombreCommunesMembres", "pearson");
            intercommunalites().selectExpr("nomGroupement", "nombreDeCompetencesExercees", "nombreCommunesMembres").show(false);

         LOGGER.info("Correlation entre le nombre de compétences exercées et le nombre de communes d'une intercommunalité : {}", correlation);
      }, "Corrélation entre nombre de communes d'une intercommunalité et son nombre de compétences exercées échouée");
   }
   
   /**
    * Corrélation entre les notes attribuées par deux juges à des sportifs (Valeurs ordonnées, Spearman).
    */
   @Test
   @DisplayName("Ordonné, Spearman : deux juges notent des sportifs")
   void ordonneSpearmanDeuxJugesNotentDesSportifs() {
      Dataset<Row> df = denseRowWithVectors(d(8.3, 7.9), d(7.6, 7.4), d(9.1, 9.1), d(9.5, 9.3), d(8.4, 8.4),
         d(6.9, 7.5), d(9.2, 9.0), d(7.8, 7.2), d(8.6, 8.2), d(8.2, 8.1));

      Row r = Correlation.corr(df, "features", "spearman").head();
      assertNotNull(r, "Une corrélation aurait dû être donnée dans 'deux juges notent des sportifs'");

      LOGGER.info("Correlation entre les notes des deux juges pour les sportifs : {}", r.get(0).toString());
   }

   /**
    * Régression linéaire : détermination des coefficients a et b de la droite par la méthode des moindre carrés
    */
   @Test
   @DisplayName("Régression linéaire : détermination des coefficients a et b par la méthode des moindre carrés")
   void regressionLineaireMethodeMoindreCarres() {
      // Deux colonnes : glucid et insulin
      Dataset<Row> glucideEtInsuline = session.read().format("csv")
         .option("encoding", "ISO-8859-1")
         .option("header","true")
         .option("inferSchema", "true")
         .option("delimiter", " ")
         .load("src/test/resources/glucides-aliments-ingeres.tsv");
      assertNotEquals(0L, glucideEtInsuline.count(), "Le dataset glucides-aliments-ingeres aurait dû renvoyer au moins un enregistrement");

      double cov = glucideEtInsuline.stat().cov("glucid", "insulin");
      LOGGER.info("covariance (glucides, insuline) = {}", cov);

      double sd = glucideEtInsuline.selectExpr("stddev_samp(glucid)").head().getDouble(0);
      double varianceX = sd * sd;
      LOGGER.info("Ecart-type en x (glucides) = {} => variance = {}", sd, varianceX);

      double a = cov / varianceX;

      double moyenneGlucides = glucideEtInsuline.selectExpr("mean(glucid)").head().getDouble(0);
      double moyenneInsuline = glucideEtInsuline.selectExpr("mean(insulin)").head().getDouble(0);
      double b = moyenneInsuline - a * moyenneGlucides;

      LOGGER.info("La droite de régression linéaire par la méthode des moindres carrés est : y = {}x + {}", a, b);

      // Calcul du coefficient de corrélation et de la variance expliquée
      double cor = glucideEtInsuline.stat().corr("glucid", "insulin", "pearson");
      double r2 = cor * cor;
      LOGGER.info("Le coefficient de correlation : r^2 = {} (r = {})", r2, cor);

      // Calcul des résidus
      double varianceTotaleY = glucideEtInsuline.selectExpr("var_samp(insulin)").head().getDouble(0);

      Column fonctionPrediction = (col("glucid").multiply(lit(a))).plus(b);
      glucideEtInsuline = glucideEtInsuline.withColumn("prediction", fonctionPrediction);
      double variancePrediction = glucideEtInsuline.selectExpr("var_samp(prediction)").head().getDouble(0);

      double residus = varianceTotaleY - variancePrediction;
      LOGGER.info("Les résidus sont de {} sur une variance totale de {} qu'explique {} de X.", residus, varianceTotaleY, variancePrediction);
      LOGGER.info("et {} / {} = {} la variance expliquée trouvée précédemment.", variancePrediction, varianceTotaleY, variancePrediction / varianceTotaleY);
   }
}
