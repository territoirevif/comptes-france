package fr.ecoemploi.adapters.outbound.spark.stat;

import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import org.apache.spark.ml.feature.StringIndexer; // A1. One hot-encoding : transforme les modalités en nombres
import org.apache.spark.ml.feature.VectorAssembler; // A2. Assemble plusieurs colonnes en une feature
import org.apache.spark.sql.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test de l'API Machine learning
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkMathStatsTestApplication.class)
class MachineLearingIT {
    /**
     * Enumération des tests, classifications, etc.
     */
    @Test @Disabled("Expérimental") // Expérimental
    @DisplayName("Enumération des tests, classifications, etc.")
    void testsClassifications() {
        new StringIndexer();
        Dataset<Row> datasetApprentissage = null;

        VectorAssembler assembler = new VectorAssembler();
        assembler.setInputCols(new String[]{"x1", "x2", "x3"});
        assembler.setOutputCol("features");
        Dataset<Row> features = assembler.transform(datasetApprentissage);

        assembler.getInputCols();

        Dataset<Row>[] jeux = datasetApprentissage.randomSplit(new double[]{0.7, 0.3});

        for(Dataset<Row> jeu : jeux) {
            assertNotEquals(0L, jeu.count(), "Un des jeux d'apprentissage pour les classifications est vide");
        }
    }
}
