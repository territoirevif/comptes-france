package fr.ecoemploi.adapters.outbound.spark.stat.descriptive;

import java.io.*;
import java.util.*;
import java.util.function.Function;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;
import fr.ecoemploi.adapters.outbound.spark.stat.*;

/**
 * Analyse en composantes principales.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkMathStatsTestApplication.class)
class AnalyseComposantesPrincipalesIT extends AbstractStatsTest implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 7301395393891047373L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AnalyseComposantesPrincipalesIT.class);

   /** Expliquer ACP */
   @Autowired
   private ExpliquerACP expliquerACP;

   /**
    * Recherche de composantes principales.
    */
   @Test
   @DisplayName("ACP : le classement des universités américaines")
   void composantePrincipale() {
      // Le classement (variables HiCi et SCi des universités américaines).

      // Créer le dataset des features (des vecteurs) :

      // features
      // [100.0,100.0]
      // [86.1,70.3]
      // [67.9,69.2] 
      // ...
      Dataset<Row> dsHiCiSCi = denseRowWithVectors(
         d(100, 100), d(86.1, 70.3), d(67.9, 69.2), d(54.0, 65.4), d(65.9, 61.7),
         d(58.4, 50.3), d(56.5, 69.6), d(59.3, 46.5), d(50.8, 54.1), d(46.3, 65.4),
         d(57.9, 63.2), d(54.5, 65.1), d(57.4, 75.9), d(59.3, 64.6), d(56.9, 70.8),
         d(52.4, 74.1), d(52.9, 67.2), d(54.0, 59.8), d(41.3, 67.9), d(41.9, 80.9),
         d(60.7, 77.1), d(38.5, 68.6), d(40.6, 62.2), d(39.2, 77.6), d(38.5, 63.2),

         d(44.5, 57.6), d(35.5, 38.4), d(39.2, 53.4), d(46.9, 57.0), d(41.3, 53.9),
         d(27.7, 23.2), d(46.9, 62.0), d(48.6, 67.0), d(39.9, 45.7), d(42.6, 42.7),
         d(31.4, 63.1), d(40.6, 53.3), d(46.9, 54.8), d(23.4, 54.2), d(30.6, 38.0),
         d(31.4, 51.0), d(27.7, 56.6), d(45.1, 58.0), d(46.9, 64.2), d(35.5, 48.9),
         d(25.7, 51.7), d(39.9, 44.8), d(24.6, 56.9), d(39.9, 65.6), d(37.1, 52.7));

      dsHiCiSCi.show(100, false);

      assertDoesNotThrow(() -> {
         this.expliquerACP.calculerEtExpliquerComposantesPrincipales(dsHiCiSCi, false, 2);
         this.expliquerACP.getValeursPropresEtQualite().show(false);
         this.expliquerACP.getContributionsEtQualiteProjectionIndividuelles().show(100, false);
      });
   }

   /**
    * Notes de collégiens
    */
   @Test
   @DisplayName("ACP : Notes de collégiens")
   void classeCollege() {
      Dataset<Row> notesCollege = loadClasseCollege();

      long nombreIndividus = notesCollege.count();
      int nombreVariables = notesCollege.columns().length;
      LOGGER.info("{} élèves (individus), {} matières (variables)", nombreIndividus, nombreVariables);

      notesCollege.show(30, false);
      assertNotEquals(0, nombreIndividus, "Le fichier des collégiens n'a pas été bien chargé");

      Dataset<Row> moyennes = onAllFields(notesCollege, field -> round(mean(new Column(field)), 3), "mean_%s");
      moyennes.show(nombreVariables, 0, true);

      Dataset<Row> ecartsTypes = onAllFields(notesCollege, field -> round(stddev_pop(new Column(field)), 4), "std_%s");
      ecartsTypes.show(nombreVariables, 0, true);

      MatriceCorrelation matriceCorrelation = new MatriceCorrelation();
      matriceCorrelation.matriceCorrelation(notesCollege, false, 3);

      LOGGER.info("Matrice de corrélation :\n{}", matriceCorrelation.toString(2, nomColonne -> (7 - nomColonne.length()) / 4));

      //DataFrameStatFunctions statsUnivariees = notesCollege.stat();
      //LOGGER.info(statsUnivariees.toString());
   }


   /**
    * Applique une opération sur chaque colonne d'un dataset
    * @param ds Dataset cible
    * @param fcn Fonction (nom du champ, opération sur colonne) à appliquer
    * @param destFieldPatternName pattern avec un %s désignant le nom du champ examiné, du nom à donner au champ final
    * @return Dataset résultat
    */
   private Dataset<Row> onAllFields(Dataset<Row> ds, Function<String, Column> fcn, String destFieldPatternName) {
      List<Column> columns = new ArrayList<>();

      for(String fieldName : ds.columns()) {
         columns.add(fcn.apply(fieldName)
            .as(String.format(destFieldPatternName, fieldName)));
      }

      return ds.select(columns.toArray(new Column[]{}));
   }

   /**
    * Charger le fichier CSV des notes de collégiens
    * @return Dataset
    */
   private Dataset<Row> loadClasseCollege() {
      return this.session.read().format("csv")
         .option("header","true")
         .option("inferSchema", "true")
         .load(new File("src/test/resources/college.csv").getAbsolutePath());
   }
}
