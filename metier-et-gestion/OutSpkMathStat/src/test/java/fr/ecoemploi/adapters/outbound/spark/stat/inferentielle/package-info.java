/**
 * Tests sur la statistique inférentielle. 
 * @author Marc Le Bihan
 */
package fr.ecoemploi.adapters.outbound.spark.stat.inferentielle;