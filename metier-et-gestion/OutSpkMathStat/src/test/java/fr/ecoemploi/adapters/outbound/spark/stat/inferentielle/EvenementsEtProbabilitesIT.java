package fr.ecoemploi.adapters.outbound.spark.stat.inferentielle;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;

import org.apache.spark.sql.*;
import org.junit.jupiter.api.*;
import org.slf4j.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import fr.ecoemploi.adapters.outbound.spark.stat.*;
import fr.ecoemploi.domain.utils.objets.TechniqueException;

/**
 * Evènements et probabilités.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkMathStatsTestApplication.class)
class EvenementsEtProbabilitesIT extends AbstractStatsTest implements Serializable {
   /** Serial ID.  */
   @Serial
   private static final long serialVersionUID = 6762193806675818229L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EvenementsEtProbabilitesIT.class);
   
   /**
    * Obtenir deux caractéristiques d'un individu "commune".
    * @throws TechniqueException si un incident survient.
    */
   @Test
   @DisplayName("Obtenir deux caractéristiques d'un individu et observer la réalisation d'un évènement.")
   void obtenirCaracteristiquesIndividuEtObservationEvenement() throws TechniqueException {
      // Des individus, nous ne voulons que trois caractéristiques.
      Dataset<Row> dsIndividu = individus().selectExpr("nomCommune", "populationTotale");
      
      // On vérifie le premier des individus.
      Row individu = dsIndividu.head();
      
      // On attend : deux caractéristiques, une population totale qui est un nombre strictement positif.
      assertEquals(2, individu.size(), "2 caractéristiques de l'individu auraient du être extraites.");
      
      assertTrue(individu.getAs("populationTotale") instanceof Integer, 
            "La caractéristique 'populationTotale' de l'individu devrait être un nombre.");
      
      int populationTotale = individu.getAs("populationTotale");
      assertTrue(populationTotale > 0, 
            "La caractérisque 'populationTotale' de l'individu devrait être strictement positive.");
      
      LOGGER.info("Les deux caractéristiques désirée du premier individu 'commune' sont : {}", individu);
      
      // Un évènement qui nous interesse : que la population de la commune soit 500 ou plus.
      Dataset<Row> dsIndividuAvecEvenement = individus()
            .selectExpr("nomCommune", "populationTotale", "populationTotale >= 500 as population1000ouPlus");
      
      individu = dsIndividuAvecEvenement.head();

      assertTrue(individu.getAs("population1000ouPlus") instanceof Boolean, 
            "La caractéristique 'population1000ouPlus' de l'individu devrait être un booléen.");
      
      assertEquals((Integer)individu.getAs("populationTotale") >= 500, individu.getAs("population1000ouPlus"), 
            "La population totale de la commune aurait dû mener à une autre décision sur l'évènement.");
      
      LOGGER.info("Les caractéristiques du premier individu 'commune' (2 précédentes + évènement) sont : {}", individu);
      
      Dataset<Row> ds = individus().selectExpr("nomCommune", "populationTotale", "strateCommune");
      ds.show();
   }
}
