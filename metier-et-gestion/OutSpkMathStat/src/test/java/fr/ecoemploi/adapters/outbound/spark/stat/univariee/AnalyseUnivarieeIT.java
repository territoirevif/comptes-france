package fr.ecoemploi.adapters.outbound.spark.stat.univariee;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.text.*;

import org.apache.spark.ml.feature.*;
import org.apache.spark.sql.*;
import org.junit.jupiter.api.*;
import org.slf4j.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import fr.ecoemploi.adapters.outbound.spark.stat.*;

/**
 * Petits tests de statistiques (Cours et PDF).
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkMathStatsTestApplication.class)
class AnalyseUnivarieeIT extends AbstractStatsTest implements Serializable {
   /** Serial ID.  */
   @Serial
   private static final long serialVersionUID = 6762193806675818229L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AnalyseUnivarieeIT.class);
   
   /**
    * Statistiques univariées sur la population communale Française.
    */
   @Test
   @DisplayName("Statistiques univariées sur la population communale française")
   void univarie() {
      Dataset<Row> populationCommunes = individus().selectExpr("populationTotale");
      
      // Obtenir les quantiles 1, 2, 3 et les déciles 1 et 9.
      double[] quantiles = populationCommunes.stat().approxQuantile("populationTotale", new double[] {0.1, 0.25, 0.5, 0.75, 0.9}, 0);
      
      LOGGER.info("Population des communes françaises :\n Q(10%) = {}, Q(25%) = {}, Q(50%) = {}, Q(75%) = {}, Q(90%) = {}", 
         quantiles[0], quantiles[1], quantiles[2], quantiles[3], quantiles[4]);
      
      for(int index=0; index < 4; index ++) {
         assertTrue(quantiles[index] <= quantiles[index+1], MessageFormat.format("Un quantile de population communale est faux : {0} > {1}", quantiles[index], quantiles[index+1]));
      }
      
      LOGGER.info("Ecart interquartile (50% des valeurs, si gaussien) : Q3 - Q1 = {}", quantiles[3] - quantiles[1]);
      LOGGER.info("Ecart interdécile (80% des valeurs, si gaussien) : x0.9 - x0.1 = {}", quantiles[4] - quantiles[0]);

      double pivotGauche = quantiles[1] - 1.5 * (quantiles[3] - quantiles[1]);
      double pivotDroit = quantiles[3] + 1.5 * (quantiles[3] - quantiles[1]);

      double pivotGaucheAlternatif = quantiles[1] - 2 * (quantiles[3] - quantiles[1]);
      double pivotDroitAlternatif = quantiles[3] + 2 * (quantiles[3] - quantiles[1]);

      LOGGER.info("Valeurs pivots (détection de valeurs aberrantes, si gaussien) : [{}, {}], alternatif [{}, {}].", 
         pivotGauche, pivotDroit, pivotGaucheAlternatif, pivotDroitAlternatif);

      // Autre descripteur.
      Dataset<Row> describe = populationCommunes.describe("populationTotale");
      describe.show();

      // summary renvoie les lignes : aux titres de colonnes (situées dans le champ 'summary') : count, mean, stddev, min, 25%, 50%, 75%, max.
      Dataset<Row> dsSummary = individus().summary().selectExpr("summary", "populationTotale");
      dsSummary.show();
      
      // Moyenne, écart-type de population, assymétrie, applatissement, coefficient de variation.
      Row univarie = individus().selectExpr("mean(populationTotale)", "stddev_pop(populationTotale)", "skewness(populationTotale)", "kurtosis(populationTotale)").first();
      double moyenne = univarie.getDouble(0);
      double ecartType = univarie.getDouble(1);
      double assymetrie = univarie.getDouble(2);
      double assymetrie2 = 3 * ((moyenne - quantiles[2]) / ecartType);
      double applatissement = univarie.getDouble(3);
      double mediane = quantiles[2];
      double coefficientVariation = ecartType / moyenne;

      String format = "Moyenne : {0,number,#0.0}, ecart-type : {1,number,#0},  assymétrie : {2,number,#0.00} ou {3,number,#0.00}, applatissement : {4,number,#0.0}, coefficient de variation = {5,number,#0.0}, médiane = {6,number,#0.0}.";
      LOGGER.info(MessageFormat.format(format, moyenne, ecartType, assymetrie, assymetrie2, applatissement, coefficientVariation, mediane));
      
      // Centrage - réduction
      Dataset<Row> populationsTotalesDenses = new VectorAssembler().setInputCols(new String[]{"populationTotale"}).setOutputCol("populationTotaleDense").transform(individus());      

      Dataset<Row> centreReduit = new StandardScaler().setWithMean(true).setWithStd(true).setInputCol("populationTotaleDense").setOutputCol("population_centree_reduite").fit(populationsTotalesDenses).transform(populationsTotalesDenses);
      centreReduit = centreReduit.select("nomCommune", "populationTotale","population_centree_reduite");
      centreReduit.show(false);
   }
}
