package fr.ecoemploi.dao;

//import jakarta.persistence.*;
import javax.persistence.*;
import javax.sql.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.*;
import org.springframework.orm.jpa.*;
import org.springframework.orm.jpa.vendor.*;
import org.springframework.transaction.*;
import org.springframework.transaction.annotation.*;

/**
 * Configuration de la source de données.
 * @author Marc LE BIHAN
 */
@Configuration
@EnableTransactionManagement
public class PersistenceConfig {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(PersistenceConfig.class);

   /** Driver. */
   @Value("${spring.datasource.driver}")
   private String driver;
   
   /** Username. */
   @Value("${spring.datasource.username}")
   private String username;
   
   /** Password. */
   @Value("${spring.datasource.password}")
   private String password;
   
   /** URL. */
   @Value("${spring.datasource.url}")
   private String url;

   /**
    * Source de données.
    * @return Datasource.
    */
   @Bean
   public DataSource dataSource(){
      DriverManagerDataSource dataSource = new DriverManagerDataSource();
      dataSource.setDriverClassName(this.driver);
      dataSource.setUrl(this.url);
      dataSource.setUsername(this.username);
      dataSource.setPassword(this.password);

      LOGGER.info("Configuration de la datasource PostGIS (Bean prêt).");
      return dataSource;
   }

   /**
    * Producteur de factory d'entity manager.
    * @return Factory d'entity manager.
    */
   @Bean
   public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
      LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
      em.setDataSource(dataSource());
      em.setPackagesToScan("fr.fondation", "fr.ecoemploi");

      JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
      em.setJpaVendorAdapter(vendorAdapter);

      return em;
   }

   /**
    * Producteur de gestionnaire de transactions.
    * @param emf Entity manager factory.
    * @return Gestionnaire de transactions.
    */
   @Bean
   public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
      JpaTransactionManager transactionManager = new JpaTransactionManager();
      transactionManager.setEntityManagerFactory(emf);

      return transactionManager;
   }
}
