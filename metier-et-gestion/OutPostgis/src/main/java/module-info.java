/**
 * Outbound port Posgis
 */
module fr.ecoemploi.postgis {
   requires fr.ecoemploi.domain.model;

   requires java.sql;
   //requires jakarta.persistence;
   requires java.persistence;

   requires org.slf4j;

   requires spring.beans;
   requires spring.context;
   requires spring.jdbc;
   requires spring.orm;
   requires spring.tx;
}
