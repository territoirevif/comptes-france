package fr.ecoemploi.adapters.inbound.port.emploi;

import java.io.Serializable;

/**
 * API des besoins en main d'oeuvre.
 * @author Marc Le Bihan
 */
public interface BesoinsEnMainOeuvreAPI extends Serializable {
   /**
    * Charger le ou les Référentiels des Besoins en Main d'Oeuvre (BMO).
    * @param anneeBMO Année des besoins en main d'oeuvre<br>
    *     0: pour toutes les années disponibles
    */
   void chargerBesoinsEnMainOeuvre(int anneeBMO);
}
