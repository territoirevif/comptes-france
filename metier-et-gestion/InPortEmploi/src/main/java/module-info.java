/**
 * Inbound Port de l'emploi
 */
module fr.ecoemploi.inbound.port.emploi {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.adapters.inbound.port.emploi;
}
