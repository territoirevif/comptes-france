/**
 * Application Port des Associations
 */
module fr.ecoemploi.application.port.association {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.application.port.association;
}
