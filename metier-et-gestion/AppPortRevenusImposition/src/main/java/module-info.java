/**
 * Application Port des revenus et imposition
 */
module fr.ecoemploi.application.port.revenus {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.application.port.revenus;
}
