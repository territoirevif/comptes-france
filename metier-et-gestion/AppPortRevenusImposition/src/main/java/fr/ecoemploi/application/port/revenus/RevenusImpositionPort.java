package fr.ecoemploi.application.port.revenus;

import java.io.Serializable;

/**
 * Actions possibles sur les revenus et imposition des ménages
 * @author Marc Le Bihan
 */
public interface RevenusImpositionPort extends Serializable {
   /**
    * Charger les revenus et imposition des ménages.
    * @param anneeImposition Année d'imposition (exemple : 2018 = déclaration d'impôt 2019).
    * @param anneeCOG Année du code officiel géographique.
    */
   void chargerRevenusEtImpositionMenages(int anneeImposition, int anneeCOG);
}
