/**
 * Inbound Port du Code Officiel Géographique
 */
module fr.ecoemploi.inbound.port.cog {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.adapters.inbound.port.cog;
}
