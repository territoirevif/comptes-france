package fr.ecoemploi.adapters.inbound.port.cog;

import java.io.Serializable;

/**
 * API du Code Officiel Géographique
 */
public interface CogAPI extends Serializable {
   /**
    * Charger le référentiel territorial dans les fichiers de cache.
    * @param anneeCOG Année de référence du Code Officiel Géographique à charger.<br>
    * 0 si toutes les années connues doivent être chargées (à partir de 2016).
    */
   public void chargerReferentiels(int anneeCOG);
}
