package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.activite;

import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;


/**
 * Application de test pour Spark
 * @author Marc LE BIHAN
 */
@SpringBootApplication
@ComponentScan(basePackages={"fr", "test", "com"})
public class SparkEmploiTestApplication {
}
