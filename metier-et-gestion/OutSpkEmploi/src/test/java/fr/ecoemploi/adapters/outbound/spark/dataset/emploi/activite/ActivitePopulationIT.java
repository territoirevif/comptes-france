package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.activite;

import java.io.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.CogDataset;

import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

/**
 * Tests sur les types d'activités des populations communales.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkEmploiTestApplication.class)
class ActivitePopulationIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -7124571238000038692L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ActivitePopulationIT.class);

   /** Dataset du code officiel géographique. */
   @Autowired
   private CogDataset cogDataset;

   /** Dataset des types d'acctivité par tranche d'âge. */
   @Autowired
   private ActivitePopulationParAgeDataset activiteAgeDataset;
   
   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Lecture des activités par tranche d'âge de la population.
    */
   @Test
   @DisplayName("Types activités par tranche d'âge")
   void activitesParTrancheAge() {
      Dataset<Row> ds = this.activiteAgeDataset.rowActivitesParTrancheAgeEtCommunes(2016);
      ds.where("codeCommune = '22272'").show(false);
      
      assertNotEquals(0L, ds.count(), "Une activité au moins aurait du être lue");
   }
   
   /** 
    * Vérifier la structure de la population de plus de 15 ans en 2016. 
    */
   @Test
   @DisplayName("Structure de la population de plus de 15 ans en 2016 dans Douanenez-Communauté")
   void structurePopulationActive() {
      Dataset<Row> communes = this.cogDataset.rowCommunes(null, null, 2020, false).filter((FilterFunction<Row>)candidat -> "242900645".equals(candidat.getAs("codeEPCI")));
      assertNotEquals(0L, communes.count(), "Une commune au moins aurait dû être lue pour Douanenez-Communauté");
      
      LOGGER.info("\n Toutes données : ");
      assertNotEquals(0L, this.activiteAgeDataset.populationActivite(this.session, communes, false, false, 2016).count(),
         "Des données d'activité de la population pour Douanenez-Communauté auraient dû être obtenues");
      
      LOGGER.info("\n Données sexe fusionnées : ");
      Dataset<Row> detailSexes = this.activiteAgeDataset.populationActivite(this.session, communes, true, false, 2016);
      assertNotEquals(0L, detailSexes.count(), "Les données sexe fusionnées pour Douanenez-Communauté auraient dû être obtenues");

      detailSexes.show(300, false);
      
      LOGGER.info("\n Données âge fusionnées : ");
      Dataset<Row> detailAges = this.activiteAgeDataset.populationActivite(this.session, communes, false, true, 2016);
      assertNotEquals(0L, detailAges.count(), "Les données âges fusionnées pour Douanenez-Communauté auraient dû être obtenues");
      detailAges.show(300, false);
      
      LOGGER.info("\n Données âge et sexe fusionnées : ");
      Dataset<Row> detailAgesSexes = this.activiteAgeDataset.populationActivite(this.session, communes, true, true, 2016);
      assertNotEquals(0L, detailAgesSexes.count(), "Les données âges et sexes fusionnées pour Douanenez-Communauté auraient dû être obtenues");
      detailAgesSexes.show(300, false);
   }
   
   /** 
    * Vérifier la structure de la population de plus de 15 ans en 2016. 
    */
   @Test
   @DisplayName("Statistique sur la population active de plus de 15 ans en 2016")
   void statistiquesPopActiveFrancaise() {
      Dataset<Row> communes = this.cogDataset.rowCommunes(null, null, 2020, false)
         .where("strateCommune = 7");
      assertNotEquals(0L, communes.count(), "Au moins une commune de strate 7 aurait dû être lue");

      Dataset<Row> actifsAyantUnEmploi = this.activiteAgeDataset.populationActivite(this.session, communes, true, true, 2016)
         .where(col("typeActivite").equalTo(lit(ActivitePopulationParAgeDataset.ACTIFS_AYANT_UN_EMPLOI)));
      assertNotEquals(0L, actifsAyantUnEmploi.count(), "Au moins un actif ayant un emploi aurait dû être trouvé dans les communes de strate 7");
      actifsAyantUnEmploi.describe("pctSurPopActive").show();

      Dataset<Row> chomeurs = this.activiteAgeDataset.populationActivite(this.session, communes, true, true, 2016)
         .where(col("typeActivite").equalTo(lit(ActivitePopulationParAgeDataset.CHOMEURS)));
      assertNotEquals(0L, chomeurs.count(), "Au moins un chômeur aurait dû être trouvé dans les communes de strate 7");
      chomeurs.describe("pctSurPopActive").show();
   }
}
