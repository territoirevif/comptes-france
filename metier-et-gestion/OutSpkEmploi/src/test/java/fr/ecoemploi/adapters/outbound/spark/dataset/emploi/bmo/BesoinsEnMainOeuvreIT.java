package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.*;
import java.text.MessageFormat;
import java.util.Set;

import static org.apache.spark.sql.functions.*;
import static org.junit.jupiter.api.Assertions.*;

import org.apache.spark.sql.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.platform.commons.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.emploi.activite.SparkEmploiTestApplication;

/**
 * Tests sur les besoins en main d'oeuvre. 
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkEmploiTestApplication.class)
class BesoinsEnMainOeuvreIT  extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -7500393500223156164L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(BesoinsEnMainOeuvreIT.class);

   /** Dataset des besoins en main d'oeuvre. */
   @Autowired
   private BesoinsEnMainOeuvreDataset besoinsEnMainOeuvreDataset;

   /** Dataset des périmètres des bassins d'emploi. */
   @Autowired
   private PerimetresBassinsEmploiDataset perimetresBassinsEmploiDataset;

   /** Dataset des périmètres des zones d'emploi. */
   @Autowired
   private PerimetresZonesEmploiDataset perimetresZonesEmploiDataset;

   /** Année BMO début. */
   @Value("${annee.bmo.debut}")
   private int annneBMODebut;

   /** Année BMO début. */
   @Value("${annee.bmo.fin}")
   private int annneBMOFin;

   /**
    * Obtenir les beoins en main d'oeuvre.
    */
   @DisplayName("Dresser une liste de besoins en main d'oeuvre")
   @ParameterizedTest(name = "BMO {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   void lireBMO(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.besoinsEnMainOeuvreDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      HistoriqueExecution historique = new HistoriqueExecution();

      for(int anneeBMO = this.annneBMODebut; anneeBMO <= this.annneBMOFin; anneeBMO ++) {
         if (options.hasRestrictionAnnee() && options.restrictionAnnee() != anneeBMO) {
            continue;
         }

         LOGGER.info("Lecture des besoins en main d'oeuvre pour l'année {}, restriction : {} → {}, {}", anneeBMO, typeRestriction, communes, annee);

         Dataset<Row> bmo = this.besoinsEnMainOeuvreDataset.obtenirBesoinsEnMainOeuvre(options, historique, anneeBMO, true, new BesoinsEnMainOeuvreTriDepartementBassinMetier())
            .orderBy(col("codeBassinEmploi"), col("nombreProjetsRecrutement").desc());

         assertNotEquals(0L, bmo.count(), MessageFormat.format("Un besoin en main d''oeuvre, au moins, aurait dû être trouvé pour l''année {0}.", anneeBMO));
         bmo.show(500, false);
      }
   }
   
   /**
    * Obtenir les pérmiètres des bassins d'emploi.
    */
   @DisplayName("Lire les périmètres des bassins d'emploi")
   @ParameterizedTest(name = "Périmètres bassins d''emploi BMO {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   void lirePerimetresBassinsEmploi(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.perimetresBassinsEmploiDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      HistoriqueExecution historique = new HistoriqueExecution();

      for(int anneeBMO = this.annneBMODebut; anneeBMO <= this.annneBMOFin; anneeBMO ++) {
         if (options.hasRestrictionAnnee() && options.restrictionAnnee() != anneeBMO) {
            continue;
         }

         PerimetresBassinsEmploiTri[] tris = new PerimetresBassinsEmploiTri[] {new PerimetresBassinsEmploiTriDepartementBassinCommune(), new PerimetresBassinsEmploiTriDepartementCommuneBassin()};

         for(PerimetresBassinsEmploiTri tri : tris) {
            LOGGER.info("Lecture des périmètres des bassins d'emplois pour l'année {}, restriction : {} → {}, {}", anneeBMO, typeRestriction, communes, annee);

            // Le BMO est une année antérieur à l'année en cours, le COG est pris une année antérieur.
            Dataset<Row> perimetres =  this.perimetresBassinsEmploiDataset.obtenirPerimetresBassinsEmploi(options, historique, anneeBMO, anneeBMO - 1, tri);

            assertNotEquals(0L, perimetres.count(), MessageFormat.format("Un périmètre de bassin d''emploi, au moins, aurait dû être trouvé pour l''année {0,number,#0} et le tri {1}.", anneeBMO, tri.getClass().getSimpleName()));
            perimetres.show(300, false);
         }
      }
   }
   
   /**
    * Obtenir les pérmiètres des zones d'emploi.
    */
   @DisplayName("Lire les périmètres des zones d'emploi")
   @ParameterizedTest(name = "Périmètres zones d''emploi BMO {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   void lirePerimetresZonesEmploi(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.perimetresZonesEmploiDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      HistoriqueExecution historique = new HistoriqueExecution();

      for(int anneeBMO = this.annneBMODebut; anneeBMO <= this.annneBMOFin; anneeBMO ++) {
         if (options.hasRestrictionAnnee() && options.restrictionAnnee() != anneeBMO) {
            continue;
         }

         PerimetresZonesEmploiTri[] tris = new PerimetresZonesEmploiTri[] {new PerimetresZonesEmploiTriDepartementCommuneZone(), new PerimetresZonesEmploiTriDepartementZoneCommune()};

         for(PerimetresZonesEmploiTri tri : tris) {
            LOGGER.info("Lecture des périmètres des zones d'emploi pour l'année {}, restriction : {} → {}, {}", anneeBMO, typeRestriction, communes, annee);
            Dataset<Row> perimetres = this.perimetresZonesEmploiDataset.obtenirPerimetresZonesEmploi(options, historique, anneeBMO, anneeBMO - 1, tri);

            assertNotEquals(0L, perimetres.count(), MessageFormat.format("Un périmètre de zone d''emploi, au moins, aurait dû être trouvé pour l''année {0} et le tri {1}.", anneeBMO, tri));
            perimetres.show(300, false);
         }
      }
   }
   
   /**
    * Obtenir les beoins en main d'oeuvre de la zone d'emploi de Douarnenez.
    */
   @Test
   @DisplayName("Dresser les besoins en main d'oeuvre de Douarnenez")
   void lireBMODouarnenez() {
      for(int anneeBMO = this.annneBMODebut; anneeBMO <= this.annneBMOFin; anneeBMO ++) {
         PerimetresBassinsEmploiTri[] tris = new PerimetresBassinsEmploiTri[] {new PerimetresBassinsEmploiTriDepartementBassinCommune(), new PerimetresBassinsEmploiTriDepartementCommuneBassin()};

         for(PerimetresBassinsEmploiTri tri : tris) {
            // Le BMO est une année antérieur à l'année en cours, le COG est pris une année antérieur.
            String codeBassinEmploi = this.perimetresBassinsEmploiDataset.obtenirCodeBassinEmploiCommune(null, null, "29046", anneeBMO, anneeBMO - 1, tri);
            assertNotNull(codeBassinEmploi, "Il n'a pas été trouvé de bassin d'emploi pour Douarnenez");

            LOGGER.info("Le code bassin d'emploi rattaché à Douarnenez est : {}", codeBassinEmploi);

            Dataset<Row> bmo = this.besoinsEnMainOeuvreDataset.obtenirBesoinsEnMainOeuvre(null, null, anneeBMO, true, new BesoinsEnMainOeuvreTriDepartementBassinMetier())
               .where(col("codeBassinEmploi").equalTo(lit(codeBassinEmploi)))
               .orderBy(col("codeBassinEmploi"), col("nombreProjetsRecrutement").desc());

            assertNotEquals(0L, bmo.count(), MessageFormat.format("Un besoin en main d''oeuvre à Douarnenez, au moins, aurait dû être trouvé pour l''année {0} et le tri {1}.", anneeBMO, tri));
            bmo.show(500, false);
         }
      }
   }

   /**
    * Obtenir le code zone d'emploi d'une commune
    */
   @Test
   @DisplayName("Obtenir le code zone d'emploi d'une commune")
   void obtenirCodeZoneEmploiCommune() {
      for(int anneeBMO = this.annneBMODebut; anneeBMO <= this.annneBMOFin; anneeBMO ++) {
         String codeZone = this.perimetresZonesEmploiDataset.obtenirCodeZoneEmploiCommune(null, null, "29046", anneeBMO, anneeBMO - 1, new PerimetresZonesEmploiTriDepartementCommuneZone());
         assertFalse(StringUtils.isBlank(codeZone), "Le code de zone d''emploi de Douarnenez aurait dû être trouvé.");
         LOGGER.info("Le code zone d'emploi de Douarnenez est : {}", codeZone);
      }
   }
}
