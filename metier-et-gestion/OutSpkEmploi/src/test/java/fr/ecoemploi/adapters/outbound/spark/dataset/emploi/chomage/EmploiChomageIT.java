package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.chomage;

import java.io.*;
import static org.junit.jupiter.api.Assertions.*;

import org.apache.spark.sql.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.emploi.activite.SparkEmploiTestApplication;
import fr.ecoemploi.domain.utils.objets.TechniqueException;

/**
 * Tests sur les données d'emploi/chômage.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkEmploiTestApplication.class)
class EmploiChomageIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -533927524045870445L;

   /** Dataset sur l'emploi et la population active. */
   @Autowired
   private EmploiPopulationActiveDataset emploiPopulationActiveDataset;
   
   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Lecture des données sur l'emploi et la population active.
    * @throws TechniqueException si un incident survient.
    */
   @Test
   @DisplayName("Emploi et population active")
   void emploiEtPopulationActive() throws TechniqueException {
      Dataset<Row> ds = this.emploiPopulationActiveDataset.rowEmploiPopulationActive(this.session, 2016);
      
      ds.show(false);
      assertNotEquals(0L, ds.count(), "Une information de diplôme/formation au moins aurait du être lue");
   }
}
