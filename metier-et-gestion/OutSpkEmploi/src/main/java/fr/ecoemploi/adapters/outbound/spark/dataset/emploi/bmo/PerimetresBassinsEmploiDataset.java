package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.*;
import java.util.*;
import java.util.function.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import static org.apache.spark.sql.functions.*;
import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.CogDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Dataset des périmètres des bassins d'emploi.
 * @author Marc Le Bihan
 */
@Service
public class PerimetresBassinsEmploiDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -4833476781111118755L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(PerimetresBassinsEmploiDataset.class);

   /** Chargeur de fichier open data csv. */
   private final PerimetresBassinsEmploiCsvLoader loader;

   /** Validateur de code officiel géographique. */
   private final PerimetresBassinsEmploiRowValidator validator;

   /** Dataset du Code Officiel Géographique. */
   private final CogDataset cogDataset;

   /**
    * Construire un générateur de dataset de périmètres de bassins d'emplois.
    * @param loader Chargeur du dataset.
    * @param validator Validateur du dataset.
    * @param cogDataset Dataset de code officiel géographique.
    */
   @Autowired
   public PerimetresBassinsEmploiDataset(PerimetresBassinsEmploiCsvLoader loader, PerimetresBassinsEmploiRowValidator validator, CogDataset cogDataset) {
      this.loader = loader;
      this.validator = validator;
      this.cogDataset = cogDataset;
   }

   /**
    * Obtenir les périmètres des bassins d'emploi.
    * @param anneeDefinition année des définitions.
    * @param anneeCOG Année du Code Officiel Géographique à associer, pour contrôle et filtrage.
    * @param tri Tri et partitionnement du dataset.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historique Historique d'exécution.
    * @return Dataset des besoins en main d'oeuvre, et ses notifications d'incidents.
    * <br>
    * <pre>|codeRegion|codeBassinEmploi|nomBassinEmploi    |codeCommuneBassinEmploi|nomCommuneBassinEmploi   |codeDepartement|<br>
    * |32        |3227            |LENS               |62001                  |Ablain-Saint-Nazaire     |62             |
    * |32        |3224            |ARTOIS TERNOIS     |62002                  |Ablainzevelle            |62             |</pre>
    * <br>
    * Un champ <code>codeCommuneRattachementBassinEmploi</code> est ajouté avant 2021 :<br>
    * <pre>|codeRegion|codeBassinEmploi|nomBassinEmploi|codeCommuneBassinEmploi|codeCommuneRattachementBassinEmploi|codeDepartement|
    * |32        |3224            |ARTOIS TERNOIS |62002                  |62002                              |62             |
    * |32        |3224            |ARTOIS TERNOIS |62004                  |62004                              |62             |</pre>
    */
   public Dataset<Row> obtenirPerimetresBassinsEmploi(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeDefinition, int anneeCOG, PerimetresBassinsEmploiTri tri) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.addReglesValidation(this.session, this.validator);
      }

      Supplier<Dataset<Row>> worker = () -> {
         LOGGER.info("Début de l'acquisition des périmètres des bassins d'emploi {} et contrôle d'après le COG {}...", anneeDefinition, anneeCOG);

         Dataset<Row> communes = this.cogDataset.rowCommunes(options, historique, anneeCOG, false);
         Dataset<Row> perimetres = this.loader.loadOpenData(anneeDefinition);

         perimetres = perimetres.join(communes, col("codeCommuneBassinEmploi").equalTo(CODE_COMMUNE.col()), "left_outer")
            .filter((FilterFunction<Row>)commune ->  this.validator.validerPerimetreBassinEmploi(historique,
               commune, anneeDefinition, anneeCOG));

         perimetres = perimetres
            .drop(CODE_REGION.col(communes))
            .drop(CODE_DEPARTEMENT.col(communes));

         return select(perimetres, anneeDefinition);
      };

      ConditionPostFiltrage postFiltre = new PerimetresBassinsEmploiPostFiltre();
      boolean anneeEligibleCache = options.anneeEligibleMiseEnCache(anneeDefinition);

      return constitutionStandard(options, historique, worker, anneeEligibleCache, postFiltre,
         new CacheParqueteur<>(options, this.session, exportCSV(), "besoins_en_main_oeuvre-perimetres_bassins", "FILTRE-annee_{0,number,#0}-cog_{1,number,#0}-bruts", tri, anneeDefinition, anneeCOG));
   }

   /**
    * Obtenir le code du bassin d'emploi d'une commune.
    * @param codeCommune Code de la commune.
    * @param anneeDefinition année des définitions.
    * @param anneeCOG Année du Code Officiel Géographique à associer, pour contrôle et filtrage.
    * @param tri Tri et partionnement des périmètres de bassin d'emploi.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historique Historique d'exécution.
    * @return Code du bassin d'emploi de la commune.
    */
   public String obtenirCodeBassinEmploiCommune(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, String codeCommune, int anneeDefinition, int anneeCOG, PerimetresBassinsEmploiTri tri) {
      Objects.requireNonNull(codeCommune, "Le code commune ne peut pas valoir null.");

      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.addReglesValidation(this.session, this.validator);
      }

      List<Row> rowCommunes = obtenirPerimetresBassinsEmploi(options, historique, anneeDefinition, anneeCOG, tri)
         .where(col("codeCommuneBassinEmploi").equalTo(lit(codeCommune))).collectAsList();
      
      if (rowCommunes.isEmpty()) {
         LOGGER.warn("Il n'y a pas de bassin d'emploi incluant la commune de code {} en {}.", codeCommune, anneeDefinition);
         return null;
      }
      
      if (rowCommunes.size() > 1) {
         LOGGER.error("Il y a plus d'un bassin d'emploi incluant la commune de code {} en {}. les deux premiers : {} et {}. Le premier code est renvoyé.", 
            codeCommune, anneeDefinition, CODE_BASSIN_EMPLOI.getAs(rowCommunes.get(0)), CODE_BASSIN_EMPLOI.getAs(rowCommunes.get(1)));
      }
      
      return CODE_BASSIN_EMPLOI.getAs(rowCommunes.get(0));
   }

   /**
    * Renvoyer un select des colonnes du dataset.
    * @param perimetres Perimètres.
    * @param anneeDefinition Année de définition.
    * @return Select de colonnes sur le dataset.
    */
   public Dataset<Row> select(Dataset<Row> perimetres, int anneeDefinition) {
      List<String> champs = new ArrayList<>(new ArrayList<>(List.of(
         CODE_REGION.champ(), CODE_DEPARTEMENT.champ(), CODE_BASSIN_EMPLOI.champ(), NOM_BASSIN_EMPLOI.champ(), CODE_COMMUNE_BASSIN_EMPLOI.champ()
      )));

      if (anneeDefinition < 2021) {
         champs.add("codeCommuneRattachementBassinEmploi");
      }

      if (anneeDefinition >= 2022) {
         champs.add(NOM_COMMUNE_BASSIN_EMPLOI.champ());
      }

      return perimetres.selectExpr(champs.toArray(new String[]{}));
   }
}
