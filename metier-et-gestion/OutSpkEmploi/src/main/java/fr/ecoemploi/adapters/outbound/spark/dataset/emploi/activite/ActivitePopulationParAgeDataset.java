package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.activite;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

import java.io.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;

import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

/**
 * Type d'activité de la population par âge. 
 * @author Marc Le Bihan
 */
@Service
public class ActivitePopulationParAgeDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -3727265852435409883L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ActivitePopulationParAgeDataset.class);

   /** Nom du fichier CSV. */
   private final String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   private final String repertoireFichier;

   /** Actifs ayant un emploi */
   public static final String ACTIFS_AYANT_UN_EMPLOI = "11";
   
   /** Chômeurs */
   public static final String CHOMEURS = "12";
   
   /** Retraités ou préretraités */
   public static String RETRAITES_OU_PRERETRAITES = "21";
   
   /** Elèves, étudiants, stagiaires non rémunérés */
   public static String ELEVES_ETUDIANTS_STAGIAIRES_NON_REMUNERES = "22";
   
   /** Femmes ou hommes au foyer */
   public static String FEMMES_OU_HOMMES_AU_FOYER = "24";
   
   /** Autres inactifs */
   public static String AUTRES_INACTIFS = "26";

   /**
    * Construire un dataset de lecture des types d'activités de population par âge.
    * @param fichier Nom du fichier de recensement.
    * @param repertoire Répertoire du fichier.
    */
   @Autowired
   public ActivitePopulationParAgeDataset(@Value("${type-activite-population-par-age.fichier.csv.nom}") String fichier, 
      @Value("${emploi.dir}") String repertoire) {
      this.repertoireFichier = repertoire;
      this.nomFichier = fichier;
   }

   /**
    * Obtenir un Dataset Row des types d'activités par population et âge.
    * @param anneeRecensement Année du recensement.
    * @return Dataset des types d'activité.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> rowActivitesParTrancheAgeEtCommunes(int anneeRecensement) {
      LOGGER.info("Acquisition des activités par tranche d'âge et communes du recensement {}...", anneeRecensement);

      File repertoireParent = assertExistenceRepertoire(this.repertoireFichier);
      File source = assertExistenceFichierAnnuel(repertoireParent, anneeRecensement, this.nomFichier);
      
      return this.session.read().schema(schema(false)).format("csv")
         .option("header","true")
         .option("delimiter", ";")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath())
            .withColumnRenamed("NIVGEO", "typeCommune")
            .withColumnRenamed("CODGEO", "codeCommune")
            .withColumnRenamed("LIBGEO", "nomCommune")
            .withColumnRenamed("SEXE", "sexe")
            .withColumnRenamed("AGEQ65", "trancheAge") 
            .withColumnRenamed("TACTR", "typeActivite")
            .withColumnRenamed("NB", "nombre")
         .selectExpr("*")
         .filter("typeCommune <> 'COMA' AND typeCommune <> 'COMD'"); // Si la commune est une commune déléguée ou associée, elle est éjectée.
   }
   
   /**
    * Renvoyer un dataset sur la population active.
    * @param session Session Spark.
    * @param communes Dataset de communes (avec population) dont il faut déterminer l'activité de la population.
    * @param fusionnerSexes true s'il faut fusionner les données sur les sexes.
    * @param fusionnerAges true s'il faut fusionner les données sur les âges.
    * @param anneeRecensementActivitesParTrancheAge Année du recensement d'activités par tranche d'âge.
    * @return Population active.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> populationActivite(SparkSession session, Dataset<Row> communes, boolean fusionnerSexes, boolean fusionnerAges, int anneeRecensementActivitesParTrancheAge) {
      Dataset<Row> activite = rowActivitesParTrancheAgeEtCommunes(anneeRecensementActivitesParTrancheAge);
      
      activite = activite.join(communes, communes.col("codeCommune").equalTo(activite.col("codeCommune")), "inner");
      
      activite = activite.select(communes.col("codeCommune"), communes.col("nomCommune"), communes.col("sirenCommune"), communes.col("populationTotale"),
         activite.col("sexe"), activite.col("trancheAge"), activite.col("typeActivite"), activite.col("nombre"), communes.col("strateCommune"), communes.col("codeEPCI"));

      // Faire un cumul total de la population de plus de 15 recensée pour avoir les effectifs par commune.
      RelationalGroupedDataset cumulEffectifs = activite.groupBy(communes.col("codeCommune"));
      Dataset<Row> effectifs = cumulEffectifs.sum("nombre");
      effectifs = effectifs.withColumnRenamed("sum(nombre)", "effectifs");
      effectifs = effectifs.withColumnRenamed("codeCommune", "codeCommuneEffectifs");
      
      // Faire le ou les fusions des données par sexe ou tranches d'âges, si demandé.
      RelationalGroupedDataset group;
      
      if (fusionnerSexes && fusionnerAges) {
         group =  activite.groupBy(communes.col("codeEPCI"), communes.col("codeCommune"), communes.col("nomCommune"), communes.col("sirenCommune"), communes.col("populationTotale"), communes.col("strateCommune"), 
               activite.col("typeActivite"));
         activite = group.sum("nombre");
         activite = activite.withColumnRenamed("sum(nombre)", "nombre");
      }
      else {
         if (fusionnerSexes) {
            group =  activite.groupBy(communes.col("codeEPCI"), communes.col("codeCommune"), communes.col("nomCommune"), communes.col("sirenCommune"), communes.col("populationTotale"), communes.col("strateCommune"), 
                  activite.col("trancheAge"), activite.col("typeActivite"));
            activite = group.sum("nombre");
            activite = activite.withColumnRenamed("sum(nombre)", "nombre");
         }
         else {
            if (fusionnerAges) {
               group =  activite.groupBy(communes.col("codeEPCI"), communes.col("codeCommune"), communes.col("nomCommune"), communes.col("sirenCommune"), communes.col("populationTotale"), communes.col("strateCommune"), 
                     activite.col("sexe"), activite.col("typeActivite"));
               activite = group.sum("nombre");
               activite = activite.withColumnRenamed("sum(nombre)", "nombre");
            }
         }
      }
      
      activite = activite.join(effectifs, activite.col("codeCommune").equalTo(effectifs.col("codeCommuneEffectifs"))).drop(effectifs.col("codeCommuneEffectifs"));
      
      // Certaines communes ont des totalisations nulles, toutes catégories confondues, et doivent être écartés.
      activite = activite.filter((FilterFunction<Row>)candidat -> {
         if ((Double)candidat.getAs("effectifs") == 0.0) {
            LOGGER.warn("La commune {} ({}) a des données de population de plus de 15 en {} invalides.", candidat.getAs("nomCommune"), candidat.getAs("codeCommune"), anneeRecensementActivitesParTrancheAge);
            return false;
         }
         
         return true;
      });

      // Ajouter des calculs de pourcentages.
      activite = activite.withColumn("pctSurPopActive", activite.col("nombre").multiply(lit(100)).divide(activite.col("effectifs")));
      activite = activite.withColumn("pctSurPopTotale", activite.col("nombre").multiply(lit(100)).divide(activite.col("populationTotale")));
      
      return activite;
   }

   /**
    * Renvoyer le schéma du Dataset.
    * @param renamed true s'il faut renvoyer les colonnes renommées, false : d'origine du fichier csv.
    * @return Schéma.
    */
   public StructType schema(boolean renamed) {
      /*
         SEXE : Sexe
         1 : Hommes
         2 : Femmes
         
         AGEQ65 : Âge quinquennal
         015 : 15 à 19 ans
         020 : 20 à 24 ans
         025 : 25 à 29 ans
         030 : 30 à 34 ans
         035 : 35 à 39 ans
         040 : 40 à 44 ans
         045 : 45 à 49 ans
         050 : 50 à 54 ans
         055 : 55 à 59 ans
         060 : 60 à 64 ans
         065 : 65 ans ou plus
         
         TACTR : Type d'activité
         11 : Actifs ayant un emploi
         12 : Chômeurs
         21 : Retraités ou préretraités
         22 : Elèves, étudiants, stagiaires non rémunérés
         24 : Femmes ou hommes au foyer
         26 : Autres inactifs
       */
      
      /*
       * NIVGEO;CODGEO;LIBGEO;SEXE;AGEQ65;TACTR;NB
       * ARM;13201;Marseille 1er Arrondissement;1;015;11;70.267790555
       */
      StructType schema = new StructType();
      schema = schema.add(renamed ? "typeCommune" : "NIVGEO", StringType, false)
         .add(renamed ? "codeCommune" : "CODGEO", StringType, false)
         .add(renamed ? "nomCommune" : "LIBGEO", StringType, false)
         .add(renamed ? "sexe" : "SEXE", StringType, false)
         .add(renamed ? "trancheAge" : "AGEQ65", IntegerType, false)
         .add(renamed ? "typeActivite" : "TACTR", StringType, false)
         .add(renamed ? "nombre" : "NB", DoubleType, false);
      
      return schema;
   }
}
