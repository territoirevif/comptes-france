package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Tri des périmètres de bassins d'emploi : partitionnement par département, tri par code bassin puis code commune du bassin.
 * @author Marc Le Bihan
 */
public class PerimetresBassinsEmploiTriDepartementBassinCommune extends PerimetresBassinsEmploiTri {
   @Serial
   private static final long serialVersionUID = 7801698826783622831L;

   /**
    * {@inheritDoc}
    */
   PerimetresBassinsEmploiTriDepartementBassinCommune() {
      super("PARTITION-departement-TRI-bassin-commune", true, CODE_DEPARTEMENT.champ(), false,
         CODE_DEPARTEMENT.champ(), CODE_BASSIN_EMPLOI.champ(), CODE_COMMUNE_BASSIN_EMPLOI.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @param codeBassin Code bassin d'emploi, si null sera testé avec <code>isNull()</code>
    * @param codeCommuneBassinEmploi Code commune du bassin d'emploi, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeBassin, String codeCommuneBassinEmploi) {
      return equalTo(codeDepartement, codeBassin)
         .and(codeCommuneBassinEmploi != null ? CODE_COMMUNE_BASSIN_EMPLOI.col().equalTo(codeCommuneBassinEmploi) : CODE_COMMUNE_BASSIN_EMPLOI.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @param codeBassin Code bassin d'emploi, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeBassin) {
      return equalTo(codeDepartement)
         .and(codeBassin != null ? CODE_BASSIN_EMPLOI.col().equalTo(codeBassin) : CODE_BASSIN_EMPLOI.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
