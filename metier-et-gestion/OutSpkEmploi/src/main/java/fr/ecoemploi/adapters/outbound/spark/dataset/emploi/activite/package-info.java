/**
 * Type d'activité de la population
 * @author Marc Le Bihan
 */
package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.activite;