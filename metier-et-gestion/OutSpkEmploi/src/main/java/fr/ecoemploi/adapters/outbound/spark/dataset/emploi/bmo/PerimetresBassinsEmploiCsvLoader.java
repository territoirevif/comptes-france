package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.StringType;

/**
 * Chargeur de fichier CSV de périmètres de bassin d'emploi
 * @author Marc Le Bihan
 */
@Component
public class PerimetresBassinsEmploiCsvLoader implements Serializable {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -8326617752632456601L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(PerimetresBassinsEmploiCsvLoader.class);

   /** Nom du fichier CSV. */
   @Value("${perimetres_bassins_emploi.fichier.csv.nom}")
   private String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${emploi.dir}")
   private String repertoireFichier;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Charger le fichier CSV opendata d'une année.
    * @param annee Année de définition
    * @return Dataset du fichier CSV, aux champs ordonnés et renommés.
    */
   @SuppressWarnings("java:S1192") // Avertissement pour duplication de noms de colonnes
   public Dataset<Row> loadOpenData(int annee) {
      LOGGER.info("Lecture du fichier des périmètres de bassins d'emploi de l'année {}...", annee);

      Dataset<Row> perimetres = rename(load(annee), annee);
      List<String> champs = new ArrayList<>(List.of("codeRegion", "codeDepartement", "codeBassinEmploi", "nomBassinEmploi", "codeCommuneBassinEmploi"));

      if (annee != 2021) {
         champs.add("nomCommuneBassinEmploi");
      }

      if (annee < 2021) {
         champs.add("codeCommuneRattachementBassinEmploi");
      }

      return perimetres.selectExpr(champs.toArray(new String[] {}));
   }

   /**
    * Renvoyer le schéma du Dataset.
    * @param anneeDefinition année pour le suffixe des champs.
    * @return Schéma du Dataset.
    */
   private StructType schema(int anneeDefinition) {
      StructType schema = new StructType()
         .add("reg", StringType)   // Code géographique de la région
         .add("dep", StringType);  // Département Code géographique du département

      if (anneeDefinition >= 2021) {
         schema = schema.add("code_commune_insee", StringType); // Code de la commune
      }

      if (anneeDefinition >= 2022) {
         schema = schema.add("lib_commune", StringType); // Nom de la commune
      }

      String champCodeBassinEmploi = MessageFormat.format("code_bassin_BMO{0,number,#0}", anneeDefinition >= 2020 ? 2020 : 2019);
      String champLibelleBassinEmploi = MessageFormat.format("lib_bassin_BMO{0,number,#0}", anneeDefinition >= 2020 ? 2020 : 2019);

      schema = schema
         .add(champCodeBassinEmploi, StringType)         // Code du bassin d'emploi
         .add(champLibelleBassinEmploi, StringType);     // Libellé du bassin d'emploi

      if (anneeDefinition < 2021) {
         /* "reg","dep","code_bassin_BMO2020","lib_bassin_BMO2020","CODGEO","LIBGEO",  "code_commune_ratt"
            "84" ,"01", "8437",               "TREVOUX",           "01003", "AMAREINS","01165" */
         schema = schema
            .add("CODGEO", StringType)             // Code de la commune
            .add("LIBGEO", StringType)             // Nom de la commune
            .add("code_commune_ratt", StringType); // Code géopgrahique de la commune de rattachement.
      }

      return schema;
   }

   /**
    * Charger le fichier csv.
    * @param anneeDefinition Année de définition
    * @return Dataset.
    */
   private Dataset<Row> load(int anneeDefinition) {
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier), anneeDefinition, this.nomFichier);

      return this.session.read().schema(schema(anneeDefinition)).format("csv")
         .option("header","true")
         .option("delimiter", ",")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath());
   }

   /**
    * Renommer les champs.
    * @param csv Dataset du fichier CSV
    * @param anneeDefinition année de définition.
    * @return Dataset aux champs renommés.
    */
   private Dataset<Row> rename(Dataset<Row> csv, int anneeDefinition) {
      String nomChampBassinEmploi = MessageFormat.format("code_bassin_BMO{0,number,#0}", anneeDefinition >= 2020 ? 2020 : 2019);
      String nomChampLibelleBassinEmploi = MessageFormat.format("lib_bassin_BMO{0,number,#0}", anneeDefinition >= 2020 ? 2020 : 2019);

      Dataset<Row> rename = csv
         .withColumn("codeRegion", when(length(col("reg")).equalTo(lit(1)), concat(lit("0"), col("reg"))).otherwise(col("reg")))
         .withColumn("codeDepartement", when(length(col("dep")).equalTo(lit(1)), concat(lit("0"), col("dep"))).otherwise(col("dep")))
         .withColumnRenamed(nomChampBassinEmploi, "codeBassinEmploi")
         .withColumnRenamed(nomChampLibelleBassinEmploi, "nomBassinEmploi");

      if (anneeDefinition < 2021) {
         rename = rename
            .withColumnRenamed("CODGEO", "codeCommuneBassinEmploi")
            .withColumnRenamed("LIBGEO", "nomCommuneBassinEmploi")
            .withColumnRenamed("code_commune_ratt", "codeCommuneRattachementBassinEmploi");
      }

      if (anneeDefinition >= 2021) {
         rename = rename.withColumnRenamed("code_commune_insee", "codeCommuneBassinEmploi");
      }

      if (anneeDefinition >= 2022) {
         rename = rename.withColumnRenamed("lib_commune", "nomCommuneBassinEmploi");
      }

      return rename;
   }
}
