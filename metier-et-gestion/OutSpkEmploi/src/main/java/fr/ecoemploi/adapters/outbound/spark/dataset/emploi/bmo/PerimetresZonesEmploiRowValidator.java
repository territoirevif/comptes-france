package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.*;
import java.util.*;
import java.util.function.BooleanSupplier;

import org.apache.spark.sql.Row;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.HistoriqueExecution;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.validation.AbstractRowValidator;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Validateur de zones d'emploi
 * @author Marc Le Bihan
 */
@Component
public class PerimetresZonesEmploiRowValidator extends AbstractRowValidator implements ApplicationContextAware {
   @Serial
   private static final long serialVersionUID = -6474276254756987202L;

   /** Le code commune de la zone d'emploi ou le code de la zone d'emploi sont nuls */
   public static final String VLD_ZONE_EMPLOI_OU_CODE_COMMUNE_VIDE = "ZONE_EMPLOI_OU_CODE_COMMUNE_VIDE";

   /** La commune est inconnue du COG de cette année-là */
   public static final String VLD_CODE_COMMUNE_ZONE_EMPLOI_INCONNUE = "CODE_COMMUNE_ZONE_EMPLOI_INVALIDE";

   /**
    * Valider un périmètre de zone d'emploi.
    * @param historique Historique d'exécution
    * @param perimetre Périmètre de zone d'emploi auquel est adjoint sa commune en left_outer join
    * @param anneeDefinition Année de définition du périmètre de la zonee d'emploi
    * @param anneeCOG Année du Code Officiel Géographique
    * @return true si le périmètre est valide.
    */
   public boolean validerPerimetreZoneEmploi(HistoriqueExecution historique, Row perimetre, int anneeDefinition, int anneeCOG) {
      List<BooleanSupplier> validations = new ArrayList<>();

      String codeZoneEmploi = CODE_ZONE_EMPLOI.getAs(perimetre);
      String codeCommuneZoneEmploi = CODE_COMMUNE_ZONE_EMPLOI.getAs(perimetre);
      String codeCommune = CODE_COMMUNE.getAs(perimetre);

      // Si le code commune de la zone d'emploi ou le code de la zone d'emploi sont nuls, l'enregistrement est invalide.
      validations.add(() -> invalideSi(VLD_ZONE_EMPLOI_OU_CODE_COMMUNE_VIDE, historique, perimetre,
         p -> codeZoneEmploi == null || codeCommuneZoneEmploi == null,
         () -> new Serializable[] {codeZoneEmploi, codeCommuneZoneEmploi}));

      // La commune doit être connue du COG de cette année-là.
      validations.add(() -> invalideSi(VLD_CODE_COMMUNE_ZONE_EMPLOI_INCONNUE, historique, perimetre,
         p -> codeCommune == null,
         () -> new Serializable[] {codeZoneEmploi, anneeDefinition, codeCommuneZoneEmploi, anneeCOG}));

      return validations.stream().allMatch(BooleanSupplier::getAsBoolean);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
      declareRegle(VLD_ZONE_EMPLOI_OU_CODE_COMMUNE_VIDE, this.messageSource,
         "Le code d'un périmètre de bassin d'emploi est vide ou son code commune de rattachement l'est", String.class, String.class);

      declareRegle(VLD_CODE_COMMUNE_ZONE_EMPLOI_INCONNUE, this.messageSource,
         "Un bassin d'emploi se réfère à une commune qui n'existe pas", String.class, Integer.class, String.class, Integer.class);
   }
}
