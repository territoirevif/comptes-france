package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.*;
import java.util.*;
import java.util.function.Supplier;

import org.slf4j.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.cog.CogDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Dataset des périmètres des zones d'emploi. 
 * @author Marc Le Bihan
 */
@Service
public class PerimetresZonesEmploiDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 1316287802783446858L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(PerimetresZonesEmploiDataset.class);

   /** Chargeur de fichier CSV de périmètres de zones d'emploi. */
   private final PerimetresZonesEmploiCsvLoader loader;

   /** Validateur de périmètres de zones d'emploi. */
   private final PerimetresZonesEmploiRowValidator validator;

   /** Dataset du Code Officiel Géographique. */
   private final CogDataset cogDataset;

   /**
    * Construire un générateur de dataset de périmètres de zones d'emplois.
    * @param loader Chargeur du dataset.
    * @param validator Validateur du dataset.
    * @param cogDataset Dataset de code officiel géographique.
    */
   @Autowired
   public PerimetresZonesEmploiDataset(PerimetresZonesEmploiCsvLoader loader, PerimetresZonesEmploiRowValidator validator, CogDataset cogDataset) {
      this.loader = loader;
      this.validator = validator;
      this.cogDataset = cogDataset;
   }

   /**
    * Obtenir les périmètres des zones d'emploi.
    * @param anneeDefinition année des définitions.
    * @param anneeCOG Année du Code Officiel Géographique à associer, pour contrôle et filtrage.
    * @param tri Tri et partitionnement sélectionné.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historique Historique d'exécution.
    * @return Dataset des besoins en main d'oeuvre.
    */
   public Dataset<Row> obtenirPerimetresZonesEmploi(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeDefinition, int anneeCOG, PerimetresZonesEmploiTri tri) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.addReglesValidation(this.session, this.validator);
      }

      Supplier<Dataset<Row>> worker = () -> {
         LOGGER.info("Début de l'acquisition des périmètres des zones d'emploi {} et contrôle d'après le COG {}...", anneeDefinition, anneeCOG);

         Dataset<Row> communes = this.cogDataset.rowCommunes(optionsCreationLecture, historique, anneeCOG, false);
         Dataset<Row> perimetres = this.loader.loadOpenData(anneeDefinition);

         perimetres = perimetres.join(communes, CODE_COMMUNE_ZONE_EMPLOI.col().equalTo(CODE_COMMUNE.col()), "left_outer")
            .filter((FilterFunction<Row>)commune ->  this.validator.validerPerimetreZoneEmploi(historique,
               commune, anneeDefinition, anneeCOG));

         perimetres = perimetres
            .drop(CODE_REGION.col(communes))
            .drop(CODE_DEPARTEMENT.col(communes));

         return select(perimetres, anneeDefinition);
      };

      ConditionPostFiltrage postFiltre = new PerimetresZonesEmploiPostFiltre();
      boolean anneeEligibleCache = options.anneeEligibleMiseEnCache(anneeDefinition);

      return constitutionStandard(options, historique, worker, anneeEligibleCache, postFiltre,
         new CacheParqueteur<>(options, this.session, exportCSV(), "besoins_en_main_oeuvre-perimetres_zones", "FILTRE-annee_{0,number,#0}-cog_{1,number,#0}-bruts", tri, anneeDefinition, anneeCOG));
   }

   /**
    * Obtenir le code de la zone d'emploi d'une commune.
    * @param codeCommune Code de la commune.
    * @param anneeDefinition année des définitions.
    * @param anneeCOG Année du Code Officiel Géographique à associer, pour contrôle et filtrage.
    * @param tri Tri et partitionnement sélectionné.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historique Historique d'exécution.
    * @return Code de la zone d'emploi de la commune.
    */
   public String obtenirCodeZoneEmploiCommune(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, String codeCommune, int anneeDefinition, int anneeCOG, PerimetresZonesEmploiTri tri) {
      Objects.requireNonNull(codeCommune, "Le code commune ne peut pas valoir null.");

      List<Row> rowCommunes = obtenirPerimetresZonesEmploi(optionsCreationLecture, historique, anneeDefinition, anneeCOG, tri)
         .where(col("codeCommuneZoneEmploi").equalTo(lit(codeCommune))).collectAsList();
      
      if (rowCommunes.isEmpty()) {
         LOGGER.warn("Il n'y a pas de zone d'emploi incluant la commune de code {} en {}.", codeCommune, anneeDefinition);
         return null;
      }
      
      if (rowCommunes.size() > 1) {
         LOGGER.error("Il y a plus d'une zone d'emploi incluant la commune de code {} en {}. les deux premiers : {} et {}. Le premier code est renvoyé.", 
            codeCommune, anneeDefinition, CODE_ZONE_EMPLOI.getAs(rowCommunes.get(0)), CODE_ZONE_EMPLOI.getAs(rowCommunes.get(1)));
      }
      
      return rowCommunes.get(0).getAs("codeZoneEmploi");
   }

   /**
    * Renvoyer un select des colonnes du dataset.
    * @param perimetres Perimètres.
    * @param anneeDefinition Année de définition.
    * @return Select de colonnes sur le dataset.
    */
   public Dataset<Row> select(Dataset<Row> perimetres, @SuppressWarnings("unused") int anneeDefinition) {
      List<String> champs = new ArrayList<>(new ArrayList<>(List.of(
         CODE_REGION.champ(), CODE_DEPARTEMENT.champ(), CODE_ZONE_EMPLOI.champ(), NOM_ZONE_EMPLOI.champ(), CODE_COMMUNE_ZONE_EMPLOI.champ(),
         NOM_COMMUNE_ZONE_EMPLOI.champ(), "codePartieTransRegionale"
      )));

      return perimetres.selectExpr(champs.toArray(new String[]{}));
   }
}
