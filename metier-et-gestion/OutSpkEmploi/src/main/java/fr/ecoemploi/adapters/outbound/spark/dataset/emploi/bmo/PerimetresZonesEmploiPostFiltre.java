package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.util.*;
import java.util.function.Supplier;

import org.apache.spark.sql.*;
import org.slf4j.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Post-filtrage de périmètres de zones d'emploi<br>
 * Apporte toutes les communes des zones d'emploi entières où se trouve une commune de la restriction
 * @author Marc Le Bihan
 */
public class PerimetresZonesEmploiPostFiltre extends AbstractConditionPostFiltrageCommune {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(PerimetresZonesEmploiPostFiltre.class);

   /**
    * Constituer le filtre de restriction, s'il y a lieu
    * @param worker Supplier du dataset à restreindre
    * @param options Options de création et de lecture du dataset.
    */
   @Override
   public Column apply(OptionsCreationLecture options, Supplier<Dataset<Row>> worker) {
      // Ne s'applique que si des restrictions aux communes ont été demandées
      if (options.hasRestrictionAuxCommunes() == false) {
         return null;
      }

      Dataset<Row> dataset = worker.get();

      if (dataset == null) {
         LOGGER.error("Un dataset null inattendu a été renvoyé en post-filtrage de dataset");
         return null;
      }

      // Retrouver les zones d'emplois auxquels correspondent ces communes
      Column conditionRechercheCodesZonesDesCommunes = conditionInitiale(options, dataset,
         FiltrageInitial.DEPARTEMENTS_ET_COMMUNES, CODE_DEPARTEMENT.champ(), CODE_COMMUNE_ZONE_EMPLOI.champ());

      Set<String> codesZones = new HashSet<>();

      dataset.where(conditionRechercheCodesZonesDesCommunes)
         .select(CODE_ZONE_EMPLOI.champ()).distinct().collectAsList()
         .forEach(rowCodeZones -> codesZones.add(rowCodeZones.getString(0)));

      LOGGER.info("La restriction aux codes communes demandés réduit la sélection aux zones d'emploi : {}", codesZones);

      return CODE_DEPARTEMENT.col().isin(restrictionCodesDepartementsAppliquees().toArray())
         .and(CODE_ZONE_EMPLOI.col().isin(codesZones.toArray()));
   }
}
