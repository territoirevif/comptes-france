package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Tri des périmètres de zones d'emploi : partitionnement par département, tri par code commune de la zone d'emploi puis code zone d'emploi.
 * @author Marc Le Bihan
 */
public class PerimetresZonesEmploiTriDepartementCommuneZone extends PerimetresZonesEmploiTri {
   @Serial
   private static final long serialVersionUID = 5526771807415744898L;

   /**
    * {@inheritDoc}
    */
   PerimetresZonesEmploiTriDepartementCommuneZone() {
      super("PARTITION-departement-TRI-commune-zone", true, CODE_DEPARTEMENT.champ(), false,
         CODE_DEPARTEMENT.champ(), CODE_COMMUNE_ZONE_EMPLOI.champ(), CODE_ZONE_EMPLOI.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @param codeCommuneZoneEmploi Code commune du bassin d'emploi, si null sera testé avec <code>isNull()</code>
    * @param codeZoneEmploi Code de la zone d'emploi, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommuneZoneEmploi, String codeZoneEmploi) {
      return equalTo(codeDepartement, codeCommuneZoneEmploi)
         .and(codeZoneEmploi != null ? CODE_ZONE_EMPLOI.col().equalTo(codeZoneEmploi) : CODE_ZONE_EMPLOI.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @param codeCommuneZoneEmploi Code commune du bassin d'emploi, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommuneZoneEmploi) {
      return equalTo(codeDepartement)
         .and(codeCommuneZoneEmploi != null ? CODE_COMMUNE_ZONE_EMPLOI.col().equalTo(codeCommuneZoneEmploi) : CODE_COMMUNE_ZONE_EMPLOI.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
