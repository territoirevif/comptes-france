package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Tri des besoins en main d'oeuvre : partitionnement par département, tri par code bassin puis code métier.
 * @author Marc Le Bihan
 */
public class BesoinsEnMainOeuvreTriDepartementBassinMetier extends BesoinsEnMainOeuvreTri {
   @Serial
   private static final long serialVersionUID = 8929459767454192516L;

   /**
    * {@inheritDoc}
    */
   BesoinsEnMainOeuvreTriDepartementBassinMetier() {
      super("PARTITION-departement-TRI-bassin-metier", true, CODE_DEPARTEMENT.champ(), false,
         CODE_DEPARTEMENT.champ(), CODE_BASSIN_EMPLOI.champ(), CODE_METIER.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @param codeBassin Code bassin d'emploi, si null sera testé avec <code>isNull()</code>
    * @param codeMetier Code métier, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeBassin, String codeMetier) {
      return equalTo(codeDepartement, codeBassin)
         .and(codeMetier != null ? CODE_METIER.col().equalTo(codeMetier) : CODE_METIER.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @param codeBassin Code bassin d'emploi, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeBassin) {
      return equalTo(codeDepartement)
         .and(codeBassin != null ? CODE_BASSIN_EMPLOI.col().equalTo(codeBassin) : CODE_BASSIN_EMPLOI.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codesDepartements Codes départements, si null sera testé avec <code>isNull()</code>
    * @param codesBassins Codes bassins d'emploi, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column isin(String[] codesDepartements, String[] codesBassins) {
      return isin(codesDepartements)
         .and(codesBassins != null ? CODE_BASSIN_EMPLOI.col().isin((Object[])codesBassins) : CODE_BASSIN_EMPLOI.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codesDepartements Codes départements, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column isin(String[] codesDepartements) {
      return codesDepartements != null ? CODE_DEPARTEMENT.col().isin((Object[])codesDepartements) : CODE_DEPARTEMENT.col().isNull();
   }
}
