package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.util.*;
import java.util.function.*;

import org.slf4j.*;

import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Post-filtrage de périmètres de bassin d'emploi<br>
 * Apporte toutes les communes des bassins entiers où se trouve une commune de la restriction
 * @author Marc Le Bihan
 */
public class PerimetresBassinsEmploiPostFiltre extends AbstractConditionPostFiltrageCommune {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(PerimetresBassinsEmploiPostFiltre.class);

   /**
    * Constituer le filtre de restriction, s'il y a lieu
    * @param worker Supplier du dataset à restreindre
    * @param options Options de création et de lecture du dataset.
    */
   @Override
   public Column apply(OptionsCreationLecture options, Supplier<Dataset<Row>> worker) {
      // Ne s'applique que si des restrictions aux communes ont été demandées
      if (options.hasRestrictionAuxCommunes() == false) {
         return null;
      }

      Dataset<Row> dataset = worker.get();

      if (dataset == null) {
         LOGGER.error("Un dataset null inattendu a été renvoyé en post-filtrage de dataset");
         return null;
      }

      // Retrouver les bassins d'emplois auxquels correspondent ces communes
      Column conditionRechercheCodesBassinsDesCommunes = conditionInitiale(options, dataset,
         FiltrageInitial.DEPARTEMENTS_ET_COMMUNES, CODE_DEPARTEMENT.champ(), CODE_COMMUNE_BASSIN_EMPLOI.champ());

      Set<String> codesBassins = new HashSet<>();

      dataset.where(conditionRechercheCodesBassinsDesCommunes)
         .select(CODE_BASSIN_EMPLOI.champ()).distinct().collectAsList()
         .forEach(rowCodeBassin -> codesBassins.add(rowCodeBassin.getString(0)));

      LOGGER.info("La restriction aux codes communes demandés réduit la sélection aux bassins d'emploi : {}", codesBassins);

      return CODE_DEPARTEMENT.col().isin(restrictionCodesDepartementsAppliquees().toArray())
         .and(CODE_BASSIN_EMPLOI.col().isin(codesBassins.toArray()));
   }
}
