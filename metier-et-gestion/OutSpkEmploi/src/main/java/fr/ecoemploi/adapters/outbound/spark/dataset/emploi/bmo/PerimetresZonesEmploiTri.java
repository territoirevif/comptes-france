package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.Serial;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

/**
 * Tris disponibles pour les périmètres des zones d'emploi.
 * @author Marc Le Bihan
 */
public class PerimetresZonesEmploiTri extends TriDataset {
   @Serial
   private static final long serialVersionUID = 3800800003054032139L;

   /**
    * Constuire un tri possible.
    * @param suffixeFichier            Suffixe du fichier trié.
    * @param sortWithinPartition       true s'il faut faire un tri au sein des partitions,<br>
    *                                  false si c'est un tri global.
    * @param partitionByRange          true s'il y a un partitionnement par range, false sinon.
    * @param nomColonnePartitionnement Nom de la colonne de partitionnement, s'il y en a une.
    * @param nomColonnesTri            Nom des colonnes de tri.
    */
   PerimetresZonesEmploiTri(String suffixeFichier, boolean sortWithinPartition, String nomColonnePartitionnement, boolean partitionByRange, String... nomColonnesTri) {
      super(suffixeFichier, sortWithinPartition, nomColonnePartitionnement, partitionByRange, nomColonnesTri);
   }
}
