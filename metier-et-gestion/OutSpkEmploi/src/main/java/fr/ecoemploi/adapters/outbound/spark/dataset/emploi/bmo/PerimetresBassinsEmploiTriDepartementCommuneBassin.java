package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Tri des périmètres de bassins d'emploi : partitionnement par département, tri par  code commune du bassin puis code bassin.
 * @author Marc Le Bihan
 */
public class PerimetresBassinsEmploiTriDepartementCommuneBassin extends PerimetresBassinsEmploiTri {
   @Serial
   private static final long serialVersionUID = -7727697104882005651L;

   /**
    * {@inheritDoc}
    */
   PerimetresBassinsEmploiTriDepartementCommuneBassin() {
      super("PARTITION-departement-TRI-commune-bassin", true, CODE_DEPARTEMENT.champ(), false,
         CODE_DEPARTEMENT.champ(), CODE_COMMUNE_BASSIN_EMPLOI.champ(), CODE_BASSIN_EMPLOI.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @param codeCommuneBassinEmploi Code commune du bassin d'emploi, si null sera testé avec <code>isNull()</code>
    * @param codeBassin Code bassin d'emploi, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommuneBassinEmploi, String codeBassin) {
      return equalTo(codeDepartement, codeCommuneBassinEmploi)
         .and(codeBassin != null ? CODE_BASSIN_EMPLOI.col().equalTo(codeBassin) : CODE_BASSIN_EMPLOI.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @param codeCommuneBassinEmploi Code commune du bassin d'emploi, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommuneBassinEmploi) {
      return equalTo(codeDepartement)
         .and(codeCommuneBassinEmploi != null ? CODE_COMMUNE_BASSIN_EMPLOI.col().equalTo(codeCommuneBassinEmploi) : CODE_COMMUNE_BASSIN_EMPLOI.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codesDepartements Codes départements, si null sera testé avec <code>isNull()</code>
    * @param codesCommunesBassinEmploi Codes communes des bassins d'emplois à rechercher, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column isin(String[] codesDepartements, String[] codesCommunesBassinEmploi) {
      return isin(codesDepartements)
         .and(codesCommunesBassinEmploi != null ? CODE_COMMUNE_BASSIN_EMPLOI.col().isin((Object[])codesCommunesBassinEmploi) : CODE_COMMUNE_BASSIN_EMPLOI.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codesDepartements Codes départements, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column isin(String[] codesDepartements) {
      return codesDepartements != null ? CODE_DEPARTEMENT.col().isin((Object[])codesDepartements) : CODE_DEPARTEMENT.col().isNull();
   }
}
