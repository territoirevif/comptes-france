package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.chomage;

import static org.apache.spark.sql.types.DataTypes.*;

import java.io.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

/**
 * Données sur l'emploi et la population active.
 * @author Marc Le Bihan
 */
@Service
public class EmploiPopulationActiveDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -618167941241713998L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EmploiPopulationActiveDataset.class);

   /** Nom du fichier CSV. */
   private final String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   private final String repertoireFichier;

   /**
    * Construire un dataset de données sur l'emploi et la population active.
    * @param fichier Nom du fichier de recensement.
    * @param repertoire Répertoire du fichier.
    */
   @Autowired
   public EmploiPopulationActiveDataset(@Value("${emploi-population-active.fichier.csv.nom}") String fichier, 
      @Value("${emploi.dir}") String repertoire) {
      this.repertoireFichier = repertoire;
      this.nomFichier = fichier;
   }
   
   /**
    * Obtenir un Dataset Row de l'emploi et de la population active.
    * @param session Session Spark.
    * @param anneeRecensement Année du recensement.
    * @return Dataset sur l'emploi et la population active.
    */
   public Dataset<Row> rowEmploiPopulationActive(SparkSession session, int anneeRecensement) {
      LOGGER.info("Acquisition des diplômes et formations du recensement {}...", anneeRecensement);

      File repertoireParent = assertExistenceRepertoire(this.repertoireFichier);
      File source = assertExistenceFichierAnnuel(repertoireParent, anneeRecensement, this.nomFichier);
      
      return session.read().schema(schema(false)).format("csv")
         .option("header","true")
         .option("delimiter", ";")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath())
            /*.withColumnRenamed("NIVGEO", "typeCommune")
            .withColumnRenamed("CODGEO", "codeCommune")
            .withColumnRenamed("LIBGEO", "nomCommune")
            .withColumnRenamed("SEXE", "sexe")
            .withColumnRenamed("AGEQ65", "trancheAge") 
            .withColumnRenamed("TACTR", "typeActivite")
            .withColumnRenamed("NB", "nombre") */
         .selectExpr("*");
         //.filter("typeCommune <> 'COMA' AND typeCommune <> 'COMD'"); // Si la commune est une commune déléguée ou associée, elle est éjectée.
   }
   
   /**
    * Renvoyer le schéma du dataset.
    * @param renamed true si les champs doivent être renommés ou false s'ils doivent garder le nom qu'ils ont dans le CSV.
    * @return Schéma.
    */
   public StructType schema(@SuppressWarnings("unused") boolean renamed) {
      StructType schema = new StructType();
      
      schema = schema.add("CODGEO", StringType, false)
        .add("P16_POP1564", DoubleType, false)
        .add("P16_POP1524", DoubleType, false)
        .add("P16_POP2554", DoubleType, false)
        .add("P16_POP5564", DoubleType, false)
        .add("P16_H1564", DoubleType, false)
        .add("P16_H1524", DoubleType, false)
        .add("P16_H2554", DoubleType, false)
        .add("P16_H5564", DoubleType, false)
        .add("P16_F1564", DoubleType, false)
        .add("P16_F1524", DoubleType, false)
        .add("P16_F2554", DoubleType, false)
        .add("P16_F5564", DoubleType, false)
        .add("P16_ACT1564", DoubleType, false)
        .add("P16_ACT1524", DoubleType, false)
        .add("P16_ACT2554", DoubleType, false)
        .add("P16_ACT5564", DoubleType, false)
        .add("P16_HACT1564", DoubleType, false)
        .add("P16_HACT1524", DoubleType, false)
        .add("P16_HACT2554", DoubleType, false)
        .add("P16_HACT5564", DoubleType, false)
        .add("P16_FACT1564", DoubleType, false)
        .add("P16_FACT1524", DoubleType, false)
        .add("P16_FACT2554", DoubleType, false)
        .add("P16_FACT5564", DoubleType, false)
        .add("P16_ACTOCC1564", DoubleType, false)
        .add("P16_ACTOCC1524", DoubleType, false)
        .add("P16_ACTOCC2554", DoubleType, false)
        .add("P16_ACTOCC5564", DoubleType, false)
        .add("P16_HACTOCC1564", DoubleType, false)
        .add("P16_HACTOCC1524", DoubleType, false)
        .add("P16_HACTOCC2554", DoubleType, false)
        .add("P16_HACTOCC5564", DoubleType, false)
        .add("P16_FACTOCC1564", DoubleType, false)
        .add("P16_FACTOCC1524", DoubleType, false)
        .add("P16_FACTOCC2554", DoubleType, false)
        .add("P16_FACTOCC5564", DoubleType, false)
        .add("P16_CHOM1564", DoubleType, false)
        .add("P16_HCHOM1564", DoubleType, false)
        .add("P16_HCHOM1524", DoubleType, false)
        .add("P16_HCHOM2554", DoubleType, false)
        .add("P16_HCHOM5564", DoubleType, false)
        .add("P16_FCHOM1564", DoubleType, false)
        .add("P16_FCHOM1524", DoubleType, false)
        .add("P16_FCHOM2554", DoubleType, false)
        .add("P16_FCHOM5564", DoubleType, false)
        .add("P16_INACT1564", DoubleType, false)
        .add("P16_ETUD1564", DoubleType, false)
        .add("P16_RETR1564", DoubleType, false)
        .add("P16_AINACT1564", DoubleType, false)
        .add("C16_ACT1564", DoubleType, false)
        .add("C16_ACT1564_CS1", DoubleType, false)
        .add("C16_ACT1564_CS2", DoubleType, false)
        .add("C16_ACT1564_CS3", DoubleType, false)
        .add("C16_ACT1564_CS4", DoubleType, false)
        .add("C16_ACT1564_CS5", DoubleType, false)
        .add("C16_ACT1564_CS6", DoubleType, false)
        .add("C16_ACTOCC1564", DoubleType, false)
        .add("C16_ACTOCC1564_CS1", DoubleType, false)
        .add("C16_ACTOCC1564_CS2", DoubleType, false)
        .add("C16_ACTOCC1564_CS3", DoubleType, false)
        .add("C16_ACTOCC1564_CS4", DoubleType, false)
        .add("C16_ACTOCC1564_CS5", DoubleType, false)
        .add("C16_ACTOCC1564_CS6", DoubleType, false)
        .add("P16_EMPLT", DoubleType, false)
        .add("P16_ACTOCC", DoubleType, false)
        .add("P16_POP15P", DoubleType, false)
        .add("P16_ACT15P", DoubleType, false)
        .add("P16_EMPLT_SAL", DoubleType, false)
        .add("P16_EMPLT_FSAL", DoubleType, false)
        .add("P16_EMPLT_SALTP", DoubleType, false)
        .add("P16_EMPLT_NSAL", DoubleType, false)
        .add("P16_EMPLT_FNSAL", DoubleType, false)
        .add("P16_EMPLT_NSALTP", DoubleType, false)
        .add("C16_EMPLT", DoubleType, false)
        .add("C16_EMPLT_CS1", DoubleType, false)
        .add("C16_EMPLT_CS2", DoubleType, false)
        .add("C16_EMPLT_CS3", DoubleType, false)
        .add("C16_EMPLT_CS4", DoubleType, false)
        .add("C16_EMPLT_CS5", DoubleType, false)
        .add("C16_EMPLT_CS6", DoubleType, false)
        .add("C16_EMPLT_AGRI", DoubleType, false)
        .add("C16_EMPLT_INDUS", DoubleType, false)
        .add("C16_EMPLT_CONST", DoubleType, false)
        .add("C16_EMPLT_CTS", DoubleType, false)
        .add("C16_EMPLT_APESAS", DoubleType, false)
        .add("C16_EMPLT_F", DoubleType, false)
        .add("C16_AGRILT_F", DoubleType, false)
        .add("C16_INDUSLT_F", DoubleType, false)
        .add("C16_CONSTLT_F", DoubleType, false)
        .add("C16_CTSLT_F", DoubleType, false)
        .add("C16_APESASLT_F", DoubleType, false)
        .add("C16_EMPLT_SAL", DoubleType, false)
        .add("C16_AGRILT_SAL", DoubleType, false)
        .add("C16_INDUSLT_SAL", DoubleType, false)
        .add("C16_CONSTLT_SAL", DoubleType, false)
        .add("C16_CTSLT_SAL", DoubleType, false)
        .add("C16_APESASLT_SAL", DoubleType, false)
        .add("C16_AGRILT_FSAL", DoubleType, false)
        .add("C16_INDUSLT_FSAL", DoubleType, false)
        .add("C16_CONSTLT_FSAL", DoubleType, false)
        .add("C16_CTSLT_FSAL", DoubleType, false)
        .add("C16_APESASLT_FSAL", DoubleType, false)
        .add("C16_AGRILT_NSAL", DoubleType, false)
        .add("C16_INDUSLT_NSAL", DoubleType, false)
        .add("C16_CONSTLT_NSAL", DoubleType, false)
        .add("C16_CTSLT_NSAL", DoubleType, false)
        .add("C16_APESASLT_NSAL", DoubleType, false)
        .add("C16_AGRILT_FNSAL", DoubleType, false)
        .add("C16_INDUSLT_FNSAL", DoubleType, false)
        .add("C16_CONSTLT_FNSAL", DoubleType, false)
        .add("C16_CTSLT_FNSAL", DoubleType, false)
        .add("C16_APESASLT_FNSAL", DoubleType, false)
        .add("P11_POP1564", DoubleType, false)
        .add("P11_POP1524", DoubleType, false)
        .add("P11_POP2554", DoubleType, false)
        .add("P11_POP5564", DoubleType, false)
        .add("P11_H1564", DoubleType, false)
        .add("P11_H1524", DoubleType, false)
        .add("P11_H2554", DoubleType, false)
        .add("P11_H5564", DoubleType, false)
        .add("P11_F1564", DoubleType, false)
        .add("P11_F1524", DoubleType, false)
        .add("P11_F2554", DoubleType, false)
        .add("P11_F5564", DoubleType, false)
        .add("P11_ACT1564", DoubleType, false)
        .add("P11_ACT1524", DoubleType, false)
        .add("P11_ACT2554", DoubleType, false)
        .add("P11_ACT5564", DoubleType, false)
        .add("P11_HACT1564", DoubleType, false)
        .add("P11_HACT1524", DoubleType, false)
        .add("P11_HACT2554", DoubleType, false)
        .add("P11_HACT5564", DoubleType, false)
        .add("P11_FACT1564", DoubleType, false)
        .add("P11_FACT1524", DoubleType, false)
        .add("P11_FACT2554", DoubleType, false)
        .add("P11_FACT5564", DoubleType, false)
        .add("P11_ACTOCC1564", DoubleType, false)
        .add("P11_ACTOCC1524", DoubleType, false)
        .add("P11_ACTOCC2554", DoubleType, false)
        .add("P11_ACTOCC5564", DoubleType, false)
        .add("P11_HACTOCC1564", DoubleType, false)
        .add("P11_HACTOCC1524", DoubleType, false)
        .add("P11_HACTOCC2554", DoubleType, false)
        .add("P11_HACTOCC5564", DoubleType, false)
        .add("P11_FACTOCC1564", DoubleType, false)
        .add("P11_FACTOCC1524", DoubleType, false)
        .add("P11_FACTOCC2554", DoubleType, false)
        .add("P11_FACTOCC5564", DoubleType, false)
        .add("P11_CHOM1564", DoubleType, false)
        .add("P11_HCHOM1564", DoubleType, false)
        .add("P11_HCHOM1524", DoubleType, false)
        .add("P11_HCHOM2554", DoubleType, false)
        .add("P11_HCHOM5564", DoubleType, false)
        .add("P11_FCHOM1564", DoubleType, false)
        .add("P11_FCHOM1524", DoubleType, false)
        .add("P11_FCHOM2554", DoubleType, false)
        .add("P11_FCHOM5564", DoubleType, false)
        .add("P11_INACT1564", DoubleType, false)
        .add("P11_ETUD1564", DoubleType, false)
        .add("P11_RETR1564", DoubleType, false)
        .add("P11_AINACT1564", DoubleType, false)
        .add("C11_ACT1564", DoubleType, false)
        .add("C11_ACT1564_CS1", DoubleType, false)
        .add("C11_ACT1564_CS2", DoubleType, false)
        .add("C11_ACT1564_CS3", DoubleType, false)
        .add("C11_ACT1564_CS4", DoubleType, false)
        .add("C11_ACT1564_CS5", DoubleType, false)
        .add("C11_ACT1564_CS6", DoubleType, false)
        .add("C11_ACTOCC1564", DoubleType, false)
        .add("C11_ACTOCC1564_CS1", DoubleType, false)
        .add("C11_ACTOCC1564_CS2", DoubleType, false)
        .add("C11_ACTOCC1564_CS3", DoubleType, false)
        .add("C11_ACTOCC1564_CS4", DoubleType, false)
        .add("C11_ACTOCC1564_CS5", DoubleType, false)
        .add("C11_ACTOCC1564_CS6", DoubleType, false)
        .add("P11_EMPLT", DoubleType, false)
        .add("P11_ACTOCC", DoubleType, false)
        .add("P11_POP15P", DoubleType, false)
        .add("P11_ACT15P", DoubleType, false)
        .add("P11_EMPLT_SAL", DoubleType, false)
        .add("P11_EMPLT_FSAL", DoubleType, false)
        .add("P11_EMPLT_SALTP", DoubleType, false)
        .add("P11_EMPLT_NSAL", DoubleType, false)
        .add("P11_EMPLT_FNSAL", DoubleType, false)
        .add("P11_EMPLT_NSALTP", DoubleType, false)
        .add("C11_EMPLT", DoubleType, false)
        .add("C11_EMPLT_CS1", DoubleType, false)
        .add("C11_EMPLT_CS2", DoubleType, false)
        .add("C11_EMPLT_CS3", DoubleType, false)
        .add("C11_EMPLT_CS4", DoubleType, false)
        .add("C11_EMPLT_CS5", DoubleType, false)
        .add("C11_EMPLT_CS6", DoubleType, false)
        .add("C11_EMPLT_AGRI", DoubleType, false)
        .add("C11_EMPLT_INDUS", DoubleType, false)
        .add("C11_EMPLT_CONST", DoubleType, false)
        .add("C11_EMPLT_CTS", DoubleType, false)
        .add("C11_EMPLT_APESAS", DoubleType, false)
        .add("C11_EMPLT_F", DoubleType, false)
        .add("C11_AGRILT_F", DoubleType, false)
        .add("C11_INDUSLT_F", DoubleType, false)
        .add("C11_CONSTLT_F", DoubleType, false)
        .add("C11_CTSLT_F", DoubleType, false)
        .add("C11_APESASLT_F", DoubleType, false)
        .add("C11_EMPLT_SAL", DoubleType, false)
        .add("C11_AGRILT_SAL", DoubleType, false)
        .add("C11_INDUSLT_SAL", DoubleType, false)
        .add("C11_CONSTLT_SAL", DoubleType, false)
        .add("C11_CTSLT_SAL", DoubleType, false)
        .add("C11_APESASLT_SAL", DoubleType, false)
        .add("C11_AGRILT_FSAL", DoubleType, false)
        .add("C11_INDUSLT_FSAL", DoubleType, false)
        .add("C11_CONSTLT_FSAL", DoubleType, false)
        .add("C11_CTSLT_FSAL", DoubleType, false)
        .add("C11_APESASLT_FSAL", DoubleType, false)
        .add("C11_AGRILT_NSAL", DoubleType, false)
        .add("C11_INDUSLT_NSAL", DoubleType, false)
        .add("C11_CONSTLT_NSAL", DoubleType, false)
        .add("C11_CTSLT_NSAL", DoubleType, false)
        .add("C11_APESASLT_NSAL", DoubleType, false)
        .add("C11_AGRILT_FNSAL", DoubleType, false)
        .add("C11_INDUSLT_FNSAL", DoubleType, false)
        .add("C11_CONSTLT_FNSAL", DoubleType, false)
        .add("C11_CTSLT_FNSAL", DoubleType, false)
        .add("C11_APESASLT_FNSAL", DoubleType, false);
      
      return schema;
   }
}
