package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.*;
import java.util.*;
import java.util.function.Supplier;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.sql.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.adapters.outbound.port.emploi.BesoinsEnMainOeuvreRepository;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.domain.model.territoire.CodeDepartement;

/**
 * Dataset des Besoins en Main d'Oeuvre.
 * @author Marc Le Bihan
 */
@Service
public class BesoinsEnMainOeuvreDataset extends AbstractSparkDataset implements BesoinsEnMainOeuvreRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 7031981509880624748L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(BesoinsEnMainOeuvreDataset.class);

   /** Année BMO début. */
   private final int annneBMODebut;

   /** Année BMO fin. */
   private final int annneBMOFin;

   /** Chargeur de fichier CSV de données open data. */
   private final BesoinsEnMainOeuvreCsvLoader loader;

   /** Périmètres de bassins d'emploi. */
   private final PerimetresBassinsEmploiDataset perimetresBassinsEmploiDataset;

   /** Périmètres de zones d'emploi. */
   private final PerimetresZonesEmploiDataset perimetresZonesEmploiDataset;

   /**
    * Construire un générateur de dataset de besoins en main d'oeuvre.
    * @param annneBMODebut Année BMO début
    * @param annneBMOFin Année BMO fin
    * @param loader Chargeur de fichier CSV de données open data
    * @param perimetresBassinsEmploiDataset Périmètres de bassins d'emploi
    * @param perimetresZonesEmploiDataset Périmètres de zones d'emploi
    */
   @Autowired
   public BesoinsEnMainOeuvreDataset(@Value("${annee.bmo.debut}") int annneBMODebut, @Value("${annee.bmo.fin}") int annneBMOFin,
       BesoinsEnMainOeuvreCsvLoader loader, PerimetresBassinsEmploiDataset perimetresBassinsEmploiDataset, PerimetresZonesEmploiDataset perimetresZonesEmploiDataset) {
      this.annneBMODebut = annneBMODebut;
      this.annneBMOFin = annneBMOFin;
      this.loader = loader;
      this.perimetresBassinsEmploiDataset = perimetresBassinsEmploiDataset;
      this.perimetresZonesEmploiDataset = perimetresZonesEmploiDataset;
   }

   /**
    * Charger le ou les Référentiels des Besoins en Main d'Oeuvre (BMO).
    * @param anneeBMO Année des besoins en main d'oeuvre<br>
    *     0: pour toutes les années disponibles
    */
   @Override
   public void chargerBesoinsEnMainOeuvre(int anneeBMO) {
      if (anneeBMO != 0) {
         chargement(anneeBMO);
      }
      else {
         for (int annee = this.annneBMODebut; annee <= this.annneBMOFin; annee ++) {
            chargement(annee);
         }
      }
   }

   /**
    * Charger un référentiel des besoin en main d'oeuvre.
    * @param anneeBMO Année des besoins en main d'oeuvre
    */
   private void chargement(int anneeBMO) {
      OptionsCreationLecture options = optionsCreationLecture();
      HistoriqueExecution historique = new HistoriqueExecution();

      obtenirBesoinsEnMainOeuvre(options, historique, anneeBMO, false, new BesoinsEnMainOeuvreTriDepartementMetierBassin());
      obtenirBesoinsEnMainOeuvre(options, historique, anneeBMO, false, new BesoinsEnMainOeuvreTriDepartementBassinMetier());

      this.perimetresBassinsEmploiDataset.obtenirPerimetresBassinsEmploi(options, historique, anneeBMO, anneeBMO-1, new PerimetresBassinsEmploiTriDepartementBassinCommune());
      this.perimetresBassinsEmploiDataset.obtenirPerimetresBassinsEmploi(options, historique, anneeBMO, anneeBMO-1, new PerimetresBassinsEmploiTriDepartementCommuneBassin());

      this.perimetresZonesEmploiDataset.obtenirPerimetresZonesEmploi(options, historique, anneeBMO, anneeBMO-1, new PerimetresZonesEmploiTriDepartementCommuneZone());
      this.perimetresZonesEmploiDataset.obtenirPerimetresZonesEmploi(options, historique, anneeBMO, anneeBMO-1, new PerimetresZonesEmploiTriDepartementZoneCommune());
   }

   /**
    * Obtenir les besoins en main d'oeuvre d'une année.
    * @param anneeBMO année des besoins en main d'oeuvre.
    * @param ejecterSansAucuneInformation true s'il faut éjecter du dataset les enregistrements qui ont conjointement
    *    le nombre de projets de recrutement, recrutement difficiles et recrutements saisonniers à null.
    * @param tri Tri et partionnement du dataset.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historique Historique d'exécution.
    * @return Dataset des besoins en main d'oeuvre.
    */
   public Dataset<Row> obtenirBesoinsEnMainOeuvre(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeBMO, boolean ejecterSansAucuneInformation, BesoinsEnMainOeuvreTri tri) {
      LOGGER.info("Acquisition des besoins en main d'oeuvre {}...", anneeBMO);

      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.aucuneDeRegleValidation(this.session);
      }

      Supplier<Dataset<Row>> worker = () -> {
         LOGGER.info("Début de l'acquisition des Besoins en Main d'Oeuvre non filtrés pour l'année {}...", anneeBMO);
         Dataset<Row> bmo = this.loader.loadOpenData(anneeBMO);

         // Si le nombre de projets de recrutement n'est pas nul ou égal à zéro, et qu'il y a un nombre de projets de recrutement difficiles,
         // il est possible de calculer un pourcentage des projets de recrutement difficiles.
         Column pourcentageDifficulteRecrutement = when(
            NOMBRE_PROJETS_RECRUTEMENT.col().isNotNull()
               .and(NOMBRE_PROJETS_RECRUTEMENT_DIFFICILES.col().isNotNull()
                  .and(NOMBRE_PROJETS_RECRUTEMENT.col().notEqual(lit(0)))),
            NOMBRE_PROJETS_RECRUTEMENT_DIFFICILES.col().multiply(100).divide(NOMBRE_PROJETS_RECRUTEMENT.col()))
            .otherwise(null);

         // Même chose pour le pourcentage des projets saisonniers.
         Column pourcentageRecrutementSaisonniers = when(
            NOMBRE_PROJETS_RECRUTEMENT.col().isNotNull()
               .and(NOMBRE_PROJETS_RECRUTEMENT_SAISONNIERS.col().isNotNull()
                  .and(NOMBRE_PROJETS_RECRUTEMENT.col().notEqual(lit(0)))),
            NOMBRE_PROJETS_RECRUTEMENT_SAISONNIERS.col().multiply(100).divide(NOMBRE_PROJETS_RECRUTEMENT.col()))
            .otherwise(null);

         bmo = bmo.withColumn(POURCENTAGE_PROJETS_RECRUTEMENT_DIFFICILES.champ(), pourcentageDifficulteRecrutement)
            .withColumn(POURCENTAGE_PROJETS_RECRUTEMENT_SAISONNIERS.champ(), pourcentageRecrutementSaisonniers);

         return bmo;
      };

      Column postFiltre = options.hasRestrictionAuxCommunes() ? postFiltre(options, historique, anneeBMO, anneeBMO -1) : null;
      boolean anneeEligibleCache = options.anneeEligibleMiseEnCache(anneeBMO);

      Dataset<Row> bmo = constitutionStandard(options, historique, worker, anneeEligibleCache, new ConditionPostFiltrageConstante(postFiltre),
         new CacheParqueteur<>(options, this.session, exportCSV(), "besoins_en_main_oeuvre", "FILTRE-annee_{0,number,#0}-bruts", tri, anneeBMO));

      // S'il nous est demandé d'éjecter les lignes sans aucune information,
      // retirer celles qui ont null pour valeur dans les colonnes :
      // nombreProjetsRecrutement, nombreProjetsRecrutementDifficiles, nombreProjetsRecrutementSaisonniers, en même temps.
      if (ejecterSansAucuneInformation) {
         Column conditionAucuneInformation = not(NOMBRE_PROJETS_RECRUTEMENT.col().isNull()
            .and(NOMBRE_PROJETS_RECRUTEMENT_DIFFICILES.col().isNull())
            .and(NOMBRE_PROJETS_RECRUTEMENT_SAISONNIERS.col().isNull()));

         bmo = bmo.filter(conditionAucuneInformation);
      }

      return bmo;
   }

   /**
    * Constituer le filtre de restriction, s'il y a lieu
    * @return Filtre de restriction
    */
   private Column postFiltre(OptionsCreationLecture options, HistoriqueExecution historique, int anneeBMO, int anneeCOG) {
      // Ne s'applique que si des restrictions aux communes ont été demandées
      if (options.hasRestrictionAuxCommunes() == false) {
         return null;
      }

      // Retrouver les bassins d'emplois auxquels correspondent ces communes
      String[] codesCommunes = options.restrictionAuxCommunes().toArray(new String[0]);
      String[] codesDepartements = CodeDepartement.codesDepartements(codesCommunes).toArray(new String[0]);

      PerimetresBassinsEmploiTriDepartementCommuneBassin tri = new PerimetresBassinsEmploiTriDepartementCommuneBassin();
      Column conditionBassinsDesCommunes = tri.isin(codesDepartements, codesCommunes);

      OptionsCreationLecture optionsSansRestrictions = new OptionsCreationLecture(options).restrictionsIgnorees(true);
      Dataset<Row> perimetresDataset = this.perimetresBassinsEmploiDataset.obtenirPerimetresBassinsEmploi(optionsSansRestrictions, historique, anneeBMO, anneeCOG, tri);
      List<Row> rowCodesBassins = perimetresDataset.where(conditionBassinsDesCommunes).select(CODE_BASSIN_EMPLOI.champ()).distinct().collectAsList();

      Set<String> codesBassins = new HashSet<>();

      for(Row rowCodeBassin : rowCodesBassins) {
         codesBassins.add(rowCodeBassin.getString(0));
      }

      LOGGER.info("La restriction aux codes communes demandés réduit la sélection aux bassins d'emploi : {}", codesBassins);

      // Le tri ci-dessous ne va pas être employé, mais il a le générateur de la condition.
      return new BesoinsEnMainOeuvreTriDepartementBassinMetier().isin(codesDepartements, codesBassins.toArray(new String[0]));
   }
}
