package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Tri des besoins en main d'oeuvre : partitionnement par département, tri par code métier puis code bassin.
 * @author Marc Le Bihan
 */
public class BesoinsEnMainOeuvreTriDepartementMetierBassin extends BesoinsEnMainOeuvreTri {
   @Serial
   private static final long serialVersionUID = -7776838712408344175L;

   /**
    * {@inheritDoc}
    */
   BesoinsEnMainOeuvreTriDepartementMetierBassin() {
      super("PARTITION-departement-TRI-metier-bassin", true, CODE_DEPARTEMENT.champ(), false,
         CODE_DEPARTEMENT.champ(), CODE_METIER.champ(), CODE_BASSIN_EMPLOI.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec isNull
    * @param codeBassin Code bassin d'emploi, si null sera testé avec <code>isNull()</code>
    * @param codeMetier Code métier, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeMetier, String codeBassin) {
      return equalTo(codeDepartement, codeMetier)
         .and(codeBassin != null ? CODE_BASSIN_EMPLOI.col().equalTo(codeBassin) : CODE_BASSIN_EMPLOI.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @param codeMetier Code métier, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeMetier) {
      return equalTo(codeDepartement)
         .and(codeMetier != null ? CODE_METIER.col().equalTo(codeMetier) : CODE_METIER.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
