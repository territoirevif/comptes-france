package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.*;
import java.text.MessageFormat;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

/**
 * Chargeur de fichier CSV open data pour les besoins en main d'oeuvre.
 * @author Marc Le Bihan
 */
@Component
public class BesoinsEnMainOeuvreCsvLoader implements Serializable {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -836977914451939356L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(BesoinsEnMainOeuvreCsvLoader.class);

   /** Nom du fichier CSV. */
   @Value("${besoins_en_main_oeuvre.fichier.csv.nom}")
   private String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${emploi.dir}")
   private String repertoireFichier;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Charger le fichier CSV opendata d'une année.
    * @param annee Année de définition
    * @return Dataset du fichier CSV, aux champs ordonnés et renommés.
    */
   @SuppressWarnings("java:S1192") // Avertissement pour duplication de noms de colonnes
   public Dataset<Row> loadOpenData(int annee) {
      LOGGER.info("Lecture du fichier des besoins en main d'oeuvre de l'année {}...", annee);

      return rename(load(annee), annee);
   }

   /**
    * Renvoyer le schéma du Dataset.
    * @param anneeBMO année des zones d'emploi.
    * @return Schéma du Dataset.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   private StructType schema(int anneeBMO) {
      String suffixeAnnee = String.valueOf(anneeBMO - 2000);
      String nomChampCodeBassinEmploi = MessageFormat.format("BE{0}", suffixeAnnee);
      String nomChampNomBassinEmploi = MessageFormat.format("NOMBE{0}", suffixeAnnee);

      if (anneeBMO < 2021) {
         /* "annee","BE20","NOMBE20",           "Famille_met", "Lbl_fam_met",    "metier","nommetier",            "Dept","NomDept",   "met","xmet","smet","REG","NOM_REG"
            2020,   "0101","BASSIN BASSE-TERRE","Z",           "Autres métiers", "A0Z40", "Agriculteurs salariés","971", "GUADELOUPE","234","130", "96",  "1",  "Guadeloupe" */
         return new StructType()
            .add("annee", IntegerType)       // Année de l'examen.
            .add(nomChampCodeBassinEmploi, StringType)   // Code bassin d'emploi
            .add(nomChampNomBassinEmploi, StringType)    // Nom du bassin d'emploi
            .add("Famille_met", StringType)  // Code famille de métier
            .add("Lbl_fam_met", StringType)  // Libellé famille de métier
            .add("metier", StringType)       // Code métier BMO (FAP)
            .add("nommetier", StringType)    // Libellé métier BMO
            .add("Dept", StringType)         // N° de département
            .add("NomDept", StringType)      // Nom du département
            .add("met", StringType)          // Nombre de projet de recrutement
            .add("xmet", StringType)         // Nombre de projets de recrutement jugés difficiles
            .add("smet", StringType)         // Nombre de projets de recrutement saisonniers
            .add("REG", StringType)          // Code région Insee
            .add("NOM_REG", StringType);     // Nom de la région
      }

      /* "annee","Code métier BMO","Nom métier BMO",       "Famille_met","Lbl_fam_met",   "BE21","NOMBE21",           "Dept","NomDept",   "REG","NOM_REG",   "met","xmet","smet"
          2021,  "A0Z40",          "Agriculteurs salariés","Z",          "Autres métiers","0101","BASSIN BASSE-TERRE","971", "Guadeloupe","01", "Guadeloupe","252","189", "159" */
      if (anneeBMO < 2024) {
         return new StructType()
            .add("annee", IntegerType)       // Année de l'examen.
            .add("Code métier BMO", StringType)   // Code métier BMO (FAP)
            .add("Nom métier BMO", StringType)    // Libellé métier BMO
            .add("Famille_met", StringType)  // Code famille de métier
            .add("Lbl_fam_met", StringType)  // Libellé famille de métier
            .add(nomChampCodeBassinEmploi, StringType)   // Code bassin d'emploi
            .add(nomChampNomBassinEmploi, StringType)    // Nom du bassin d'emploi
            .add("Dept", StringType)         // N° de département
            .add("NomDept", StringType)      // Nom du département
            .add("REG", StringType)          // Code région Insee
            .add("NOM_REG", StringType)      // Nom de la région
            .add("met", StringType)          // Nombre de projet de recrutement
            .add("xmet", StringType)         // Nombre de projets de recrutement jugés difficiles
            .add("smet", StringType);        // Nombre de projets de recrutement saisonniers
      }

      /* "annee","Code métier BMO","Nom métier BMO","Famille_met","Lbl_fam_met",   "REG","NOM_REG",  "Dept","NomDept",   "BE24","NOMBE24",           "met","xmet","smet"
         "2024", "A0X40",          "Agriculteurs",  "Z",          "Autres métiers","01", "Guadeloupe","971","Guadeloupe","101", "BASSIN BASSE-TERRE","95", "31",  "44" */
      return new StructType()
         .add("annee", IntegerType)       // Année de l'examen.
         .add("Code métier BMO", StringType)   // Code métier BMO (FAP)
         .add("Nom métier BMO", StringType)    // Libellé métier BMO
         .add("Famille_met", StringType)  // Code famille de métier
         .add("Lbl_fam_met", StringType)  // Libellé famille de métier
         .add("REG", StringType)          // Code région Insee
         .add("NOM_REG", StringType)      // Nom de la région
         .add("Dept", StringType)         // N° de département
         .add("NomDept", StringType)      // Nom du département
         .add(nomChampCodeBassinEmploi, StringType)   // Code bassin d'emploi
         .add(nomChampNomBassinEmploi, StringType)    // Nom du bassin d'emploi
         .add("met", StringType)          // Nombre de projet de recrutement
         .add("xmet", StringType)         // Nombre de projets de recrutement jugés difficiles
         .add("smet", StringType);        // Nombre de projets de recrutement saisonniers
   }

   /**
    * Charger le fichier csv.
    * @param anneeBMO Année des besoins en main d'ouevre
    * @return Dataset.
    */
   private Dataset<Row> load(int anneeBMO) {
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier), anneeBMO, this.nomFichier);

      return this.session.read().schema(schema(anneeBMO)).format("csv")
         .option("header","true")
         .option("delimiter", ",")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath());
   }

   /**
    * Renommer les champs.
    * @param csv Dataset du fichier CSV
    * @param anneeBMO année des besoins en main d'oeuvre.
    * @return Dataset aux champs renommés.
    */
   private Dataset<Row> rename(Dataset<Row> csv, int anneeBMO) {
      String suffixeAnnee = String.valueOf(anneeBMO - 2000);
      String nomChampCodeBassinEmploi = MessageFormat.format("BE{0}", suffixeAnnee);
      String nomChampNomBassinEmploi = MessageFormat.format("NOMBE{0}", suffixeAnnee);

      csv = csv
         .withColumnRenamed("annee", "anneeBMO")
         .withColumnRenamed(nomChampCodeBassinEmploi, "codeBassinEmploi")
         .withColumnRenamed(nomChampNomBassinEmploi, "nomBassinEmploi")
         .withColumnRenamed("Famille_met", "codeFamilleMetier")
         .withColumnRenamed("Lbl_fam_met", "libelleFamilleMetier")
         .withColumn("codeDepartement", when(length(col("Dept")).equalTo(lit(1)), concat(lit("0"), col("Dept"))).otherwise(col("Dept")))
         .withColumnRenamed("NomDept", "nomDepartement")
         .withColumn("nombreProjetsRecrutement", col("met").cast(IntegerType))
         .withColumn("nombreProjetsRecrutementDifficiles", col("xmet").cast(IntegerType))
         .withColumn("nombreProjetsRecrutementSaisonniers", col("smet").cast(IntegerType))
         .withColumn("codeRegion", when(length(col("REG")).equalTo(lit(1)), concat(lit("0"), col("REG"))).otherwise(col("REG")))
         .withColumnRenamed("NOM_REG", "nomRegion")
         .drop("Dept", "REG", "met", "xmet", "smet");

      if (anneeBMO < 2021) {
         csv = csv
            .withColumnRenamed("metier", "codeMetier")
            .withColumnRenamed("nommetier", "nomMetier");
      }
      else {
         csv = csv
            .withColumnRenamed("Code métier BMO", "codeMetier")
            .withColumnRenamed("Nom métier BMO", "nomMetier");
      }

      return csv;
   }
}
