package fr.ecoemploi.adapters.outbound.spark.dataset.emploi.bmo;

import java.io.*;
import java.text.MessageFormat;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.StringType;

/**
 * Chargeur de fichier CSV open data de périmètres zones d'emploi.
 * @author Marc Le Bihan
 */
@Component
public class PerimetresZonesEmploiCsvLoader implements Serializable {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -4329023294659518905L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(PerimetresZonesEmploiCsvLoader.class);

   /** Nom du fichier CSV. */
   @Value("${perimetres_zones_emploi.fichier.csv.nom}")
   private String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${emploi.dir}")
   private String repertoireFichier;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Charger le fichier CSV opendata d'une année.
    * @param annee Année de définition
    * @return Dataset du fichier CSV, aux champs ordonnés et renommés.
    */
   @SuppressWarnings("java:S1192") // Avertissement pour duplication de noms de colonnes
   public Dataset<Row> loadOpenData(int annee) {
      LOGGER.info("Lecture du fichier des périmètres de zones d'emploi de l'année {}...", annee);

      return rename(load(annee), annee)
         .select("codeRegion", "codeDepartement", "codeZoneEmploi", "nomZoneEmploi", "codeCommuneZoneEmploi",
            "nomCommuneZoneEmploi", "codePartieTransRegionale");
   }

   /**
    * Renvoyer le schéma du Dataset.
    * @param annee année pour le suffixe des champs.
    * @return Schéma du Dataset.
    */
   private StructType schema(int annee) {
      String nomChampZoneEmploi = MessageFormat.format("ZE{0,number,#0}", annee);
      String nomChampLibelleGeographique = MessageFormat.format("LIBZE{0,number,#0}", annee);
      String nomChampPartieTransregionale = MessageFormat.format("ZE{0,number,#0}_PARTIE_REG", annee);

      return new StructType()
         .add("CODGEO", StringType)                      // Code géographique communal
         .add("LIBGEO", StringType)                      // Libellé géographique communal
         .add(nomChampZoneEmploi, StringType)            // Code géographique de la zone d'emploi
         .add(nomChampLibelleGeographique, StringType)   // Libellé géographique de la zone d'emploi
         .add(nomChampPartieTransregionale, StringType)  // Code géopgrahique de la partie régionale de la zone d’emploi trans-régionale
         .add("DEP", StringType)                         // Département Code géographique du département
         .add("REG", StringType);                        // Code géographique de la région
   }

   /**
    * Charger le fichier csv.
    * @param anneeDefinition Année de définition
    * @return Dataset.
    */
   private Dataset<Row> load(int anneeDefinition) {
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier), anneeDefinition, this.nomFichier);

      return this.session.read().schema(schema(anneeDefinition)).format("csv")
         .option("header","true")
         .option("delimiter", ",")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath());
   }

   /**
    * Renommer les champs.
    * @param csv Dataset du fichier CSV
    * @param anneeDefinition année de définition.
    * @return Dataset aux champs renommés.
    */
   private Dataset<Row> rename(Dataset<Row> csv, int anneeDefinition) {
      String nomChampZoneEmploi = MessageFormat.format("ZE{0,number,#0}", anneeDefinition);
      String nomChampLibelleGeographique = MessageFormat.format("LIBZE{0,number,#0}", anneeDefinition);
      String nomChampPartieTransregionale = MessageFormat.format("ZE{0,number,#0}_PARTIE_REG", anneeDefinition);

      return csv
         .withColumnRenamed(nomChampZoneEmploi, "codeZoneEmploi")
         .withColumnRenamed(nomChampLibelleGeographique, "nomZoneEmploi")
         .withColumnRenamed("CODGEO", "codeCommuneZoneEmploi")
         .withColumnRenamed("LIBGEO", "nomCommuneZoneEmploi")
         .withColumnRenamed(nomChampPartieTransregionale, "codePartieTransRegionale")
         .withColumn("codeDepartement", when(length(col("DEP")).equalTo(lit(1)), concat(lit("0"), col("DEP"))).otherwise(col("DEP")))
         .withColumn("codeRegion", when(length(col("REG")).equalTo(lit(1)), concat(lit("0"), col("REG"))).otherwise(col("REG")));
   }
}
