package fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;
import static org.apache.spark.sql.types.DataTypes.*;

/**
 * Nom des champs
 */
public enum NomChamp {
   /** Arrondissement d'une commune */
   ARRONDISSEMENT("arrondissement", StringType, "Arrondissement"),

   /** Arrondissement dans une commune siège */
   ARRONDISSEMENT_SIEGE("arrondissementSiege", StringType, "Arrondissement dans la ville siège"),

   /** Catégorie de compétence de groupement ou communale */
   CATEGORIE_COMPETENCE("categorieCompetence", StringType, "Catégorie d'une compétence de groupement ou communale"),

   /** Code activité */
   CODE_ACTIVITE("codeActivite", StringType, "Code Activité"),

   /** Code d'un bassin de vie */
   CODE_BASSIN_VIE("codeBassinVie", StringType, "Code d'un bassin de vie"),

   /** Code d'un bassin d'emploi */
   CODE_BASSIN_EMPLOI("codeBassinEmploi", StringType, "Code d'un bassin d'emploi"),

   /** Code d'un canton */
   CODE_CANTON("codeCanton", StringType, "Code d'un canton"),

   /** Code commune */
   CODE_COMMUNE("codeCommune", StringType, "Code commune"),

   /** Code commune avant un évènement communal */
   CODE_COMMUNE_AVANT_EVENEMENT("codeCommuneAvantEvenement", StringType, "Code commune avant un évènement communal (fusion, changement de nom...)"),

   /** Code commune après un évènement communal */
   CODE_COMMUNE_APRES_EVENEMENT("codeCommuneApresEvenement", StringType, "Code commune après un évènement communal (fusion, changement de nom...)"),

   /** Code commune membre d'un bassin d'emploi */
   CODE_COMMUNE_BASSIN_EMPLOI("codeCommuneBassinEmploi", StringType, "Code commune membre d'un bassin d'emploi"),

   /** Code commune membre d'une intercommunalité */
   CODE_COMMUNE_MEMBRE("codeCommuneMembre", StringType, "Code commune membre d'une intercommunalité"),

   /** Code commune parente */
   CODE_COMMUNE_PARENTE("codeCommuneParente", StringType, "Code commune parente d'une commune déléguée ou associée"),

   /** Code commune secondaire */
   CODE_COMMUNE_SECONDAIRE("codeCommuneSecondaire", StringType, "Code commune secondaire (pour des établissements d'entreprises, par exemple)"),

   /** Code commune siège d'une intercommunalité */
   CODE_COMMUNE_SIEGE("codeCommuneSiege", StringType, "Code commune siège d'une intercommunalité"),

   /** Code commune membre d'une zone d'emploi */
   CODE_COMMUNE_ZONE_EMPLOI("codeCommuneZoneEmploi", StringType, "Code commune membre d'une zone d'emploi"),

   /** Code d'une compétence de groupement ou communale */
   CODE_COMPETENCE("codeCompetence", StringType, "Code compétence"),

   /** Code département */
   CODE_DEPARTEMENT("codeDepartement", StringType, "Code département"),

   /** Code EPCI (intercommunalité) */
   CODE_EPCI("codeEPCI", StringType, "Code d'une intercommunalité"),

   /** Code métier */
   CODE_METIER("codeMetier", StringType, "Code métier"),

   /** Code postal */
   CODE_POSTAL("codePostal", StringType, "Code postal"),

   /** Code région */
   CODE_REGION("codeRegion", StringType, "Code région"),

   /** Code unité urbaine */
   CODE_UNITE_URBAINE("codeUniteUrbaine", StringType, "Code unité urbaine"),

   /** Code d'une zone d'emploi */
   CODE_ZONE_EMPLOI("codeZoneEmploi", StringType, "Code d'une zone d'emploi"),

   /** Complement d'adresse */
   COMPLEMENT_ADRESSE("complementAdresse", StringType, "Complément d'adresse"),

   /** Complement de nom */
   COMPLEMENT_NOM("complementNom", StringType, "Complément de nom"),

   /** Date de création d'entreprise */
   DATE_CREATION_ENTREPRISE("dateCreationEntreprise", DateType, "Date de création d'entreprise"),

   /** Date de création d'établissement */
   DATE_CREATION_ETABLISSEMENT("dateCreationEtablissement", DateType, "Date de création d'établissement"),

   /** Date début d'historisation d'entreprise */
   DATE_DEBUT_HISTORISATION_ENTREPRISE("dateDebutHistorisation", DateType, "Date de début d'historisation d'entreprise"),

   /** Date de dernier traitement de l'entreprise */
   DATE_DERNIER_TRAITEMENT_ENTREPRISE("dateCreationEntreprise", DateType, "Date de dernier traitement de l'entreprise"),

   /** Date d'effet d'un évènement communal */
   DATE_EFFET_EVENEMENT_COMMUNAL("dateEffetEvenementCommunal", DateType, "Date d'effet d'un évènement communal (fusion, changement de nom...)"),

   /** Si les informations sont diffusables */
   DIFFUSABLE("diffusable", BooleanType, "Statut diffusable (SIRENE)"),

   /** Compétence en vigueur une année donnée (existence légale nationale) */
   EN_VIGUEUR_COMPETENCE("competenceEnVigueur", StringType, "Compétence en vigueur une année donnée : inconnue [à ce moment-là], active [groupements et communes peuvent la sélectionner], obsolete [n'est plus prise en charge, peut-être : remplacée]"),

   /** Indice de répétition du numéro dans la voie */
   INDICE_REPETITION("indiceRepetition", StringType, "Indice de répétition du numéro dans la voie"),

   /** Latitude */
   LATITUDE("latitude", DoubleType, "Latitude"),

   /** Position Lambert X */
   LAMBERT_X("lambertX", DoubleType, "Position Lambert X"),

   /** Position Lambert X */
   LAMBERT_Y("lambertY", DoubleType, "Position Lambert Y"),

   /** Libelle d'un type d'équipement */
   LIBELLE_TYPE_EQUIPEMENT("libelleTypeEquipement", StringType, "Libelle d'un type d'équipement"),

   /** Libelle de la voie */
   LIBELLE_VOIE("libelleVoie", StringType, "Libelle de la voie"),

   /** Longitude */
   LONGITUDE("longitude", DoubleType, "Longitude"),

   /** Nom d'adhésion d'un groupement à un syndicat mixte fermé (SMF) */
   NOM_ADHESION("nomAdhesion", StringType, "Nom du syndicat mixte auquel adhère un groupement"),

   /** Nom d'un de bassin d'emploi */
   NOM_BASSIN_EMPLOI("nomBassinEmploi", StringType, "Nom d'un bassin d'emploi"),

   /** Nom commune */
   NOM_COMMUNE("nomCommune", StringType, "Nom commune"),

   /** Nom d'une commune d'un bassin d'emploi */
   NOM_COMMUNE_BASSIN_EMPLOI("nomCommuneBassinEmploi", StringType, "Nom d'une commune d'un bassin d'emploi"),

   /** Nom d'une commune membre */
   NOM_COMMUNE_MEMBRE("nomCommuneMembre", StringType, "Nom d'une commune membre"),

   /** Nom commune siège */
   NOM_COMMUNE_SIEGE("nomCommuneSiege", StringType, "Nom d'une commune siège"),

   /** Nom d'une commune d'une zone d'emploi */
   NOM_COMMUNE_ZONE_EMPLOI("nomCommuneZoneEmploi", StringType, "Nom d'une commune d'une zone d'emploi"),

   /** Nom d'une compétence de groupement */
   NOM_COMPETENCE("nomCompetence", StringType, "Nom d'une compétence"),

   /** Nom d'un département */
   NOM_DEPARTEMENT("nomDepartement", StringType, "Nom d'un departement"),

   /** Nom d'une intercommunalité */
   NOM_EPCI("nomEPCI", StringType, "Nom d'une intercommunalité (EPCI)"),

   /** Nom d'un groupement */
   NOM_GROUPEMENT("nomGroupement", StringType, "Nom d'un groupement"),

   /** Nom d'un membre (sans qu'on sache son type : commune, groupement, autre organisme) */
   NOM_MEMBRE("nomMembre", StringType, "Nom d'un membre"),

   /** Nom ou raison sociale */
   NOM_OU_RAISON_SOCIALE("nomOuRaisonSociale", StringType, "Nom d'un ou raison sociale"),

   /** Nom d'une zone d'emploi */
   NOM_ZONE_EMPLOI("nomZoneEmploi", StringType, "Nom d'un bassin d'emploi"),

   /** Nombre de projets de recrutement (Besoins en main d'oeuvre) */
   NOMBRE_PROJETS_RECRUTEMENT("nombreProjetsRecrutement", IntegerType, "Nombre de projets de recrutement (Besoins en main d'oeuvre)"),

   /** Nombre de projets de recrutement difficiles (Besoins en main d'oeuvre) */
   NOMBRE_PROJETS_RECRUTEMENT_DIFFICILES("nombreProjetsRecrutementDifficiles", IntegerType, "Nombre de projets de recrutement difficiles (Besoins en main d'oeuvre)"),

   /** Nombre de projets de recrutement saisonniers (Besoins en main d'oeuvre) */
   NOMBRE_PROJETS_RECRUTEMENT_SAISONNIERS("nombreProjetsRecrutementSaisonniers", IntegerType, "Nombre de projets de recrutement saisonniers (Besoins en main d'oeuvre)"),

   /** Numéro de compte de comptabilité */
   NUMERO_COMPTE("numeroCompte", StringType, "Numéro de compte de comptabilité"),

   /** Numéro de voie */
   NUMERO_VOIE("numeroVoie", StringType, "Numéro de voie"),

   /** Numéro waldec d'une association */
   NUMERO_WALDEC_ASSOCIATION("numero_waldec", StringType, "Numéro waldec d'une association"),

   /** Nature juridique */
   NATURE_JURIDIQUE("natureJuridique", StringType, "Nature juridique"),

   /** Objet social d'une association */
   OBJET_SOCIAL_ASSOCIATION("objet", StringType, "Objet social d'une association"),

   /** Population du syndicat mixte auquel adhère un groupement */
   POPULATION_ADHESION("populationAdhesion", IntegerType, "Population d'un syndicat mixte auquel adhère un groupement"),

   /** Population comptée à part */
   POPULATION_COMPTEE_A_PART("populationCompteApart", IntegerType, "Population comptée à part"),

   /** Population d'un groupement */
   POPULATION_GROUPEMENT("populationGroupement", IntegerType, "Population d'un groupement"),

   /** Population d'un membre */
   POPULATION_MEMBRE("populationMembre", IntegerType, "Population d'un membre d'un groupement"),

   /** Population de la commune membre d'un groupement comptée à part */
   POPULATION_COMMUNE_MEMBRE_COMPTEE_A_PART("populationCommuneMembreCompteApart", IntegerType, "Population de la commune membre d'un groupement, comptée à part"),

   /** Population municipale de la commune membre d'un groupement */
   POPULATION_COMMUNE_MEMBRE_MUNICIPALE("populationCommuneMembreMunicipale", IntegerType, "Population municipale de la commune membre d'un groupement"),

   /** Population totale de la commune membre d'un groupement */
   POPULATION_COMMUNE_MEMBRE_TOTALE("populationCommuneMembreTotale", IntegerType, "population Totale (municipale + comptée à part) de la commune membre d'un groupement"),

   /** Population de la commune siège d'un groupement comptée à part */
   POPULATION_COMMUNE_SIEGE_COMPTEE_A_PART("populationCommuneSiegeCompteApart", IntegerType, "Population de la commune siège d'un groupement, comptée à part"),

   /** Population municipale de la commune siège d'un groupement */
   POPULATION_COMMUNE_SIEGE_MUNICIPALE("populationCommuneSiegeMunicipale", IntegerType, "Population municipale de la commune siège d'un groupement"),

   /** Population totale de la commune siège d'un groupement */
   POPULATION_COMMUNE_SIEGE_TOTALE("populationCommuneSiegeTotale", IntegerType, "Population totale (municipale + comptée à part) de la commune siège d'un groupement"),

   /** Population municipale */
   POPULATION_MUNICIPALE("populationMunicipale", IntegerType, "Population municipale"),

   /** Population totale */
   POPULATION_TOTALE("populationTotale", IntegerType, "Population totale (municipale + comptée à part)"),

   /** Pourcentage de projets de recrutement difficiles (Besoins en main d'oeuvre) */
   POURCENTAGE_PROJETS_RECRUTEMENT_DIFFICILES("pourcentageProjetsRecrutementDifficiles", DoubleType, "Pourcentage de projets de recrutement difficiles (Besoins en main d'oeuvre)"),

   /** Pourcentage de projets de recrutement saisonniers (Besoins en main d'oeuvre) */
   POURCENTAGE_PROJETS_RECRUTEMENT_SAISONNIERS("pourcentageProjetsRecrutementSaisonniers", DoubleType, "Pourcentage de projets de recrutement saisonniers (Besoins en main d'oeuvre)"),

   /** Qualité de la géolocalisation */
   QUALITE_GEOLOCALISATION("qualiteGeolocalisation", IntegerType, "Qualité de la géolocalisation"),

   /** Qualité du positionnement X, Y */
   QUALITE_POSITIONNEMENT_XY("qualitePositionnementXY", StringType, "Qualité du positionnement XY",
      """
         La QUALITE_XY est calculée à partir de deux variables : un indicateur de qualité de la géolocalisation (QUALITE_GEOLOC) et une tranche de distance de précision de la géolocalisation (TR_DIST_PRECISION).
            une QUALITE_XY “Bonne” correspond à TR_DIST_PRECISION = “< 100” (De moins de 100 mètres) et soit QUALITE_GEOLOC = “11”, soit QUALITE_GEOLOC = “12”, soit QUALITE_GEOLOC = “_Z” ;
            une QUALITE_XY “Acceptable” correspond à QUALITE_GEOLOC = “12” et TR_DIST_PRECISION = “[100 - 500[” (De 100 à moins de 500 mètres) ;
            une QUALITE_XY “Mauvaise” correspond à QUALITE_GEOLOC = “12” et TR_DIST_PRECISION = “>= 500” (500 mètres ou plus) ou à QUALITE_GEOLOC = “21”, “22”, “33”.
            une QUALITE_XY “Non-géolocalisé” correspond à QUALITE_GEOLOC = “_U” et TR_DIST_PRECISION = “_U” (Indéterminée).
            une QUALITE_XY “Indéterminée” correspond à TR_DIST_PRECISION = “_U” (Indéterminée) et soit QUALITE_GEOLOC = “12”, soit QUALITE_GEOLOC = “_Z” ;
            pour environ 10 000 équipements, une QUALITE_XY “Bonne” ou “Acceptable” correspond à la combinaison QUALITE_GEOLOC = “_Z” et TR_DIST_PRECISION = “_Z”.
         
         Modalité Libellé
         A  Acceptable
         B  Bonne
         M  Mauvaise
         _U Indéterminée
         _Z Non-géolocalisé
         """),

   /** Siren d'adhésion d'un groupement à un syndicat mixte fermé (SMF) */
   SIREN_ADHESION("sirenAdhesion", StringType, "Siren du syndicat mixte auquel adhère un groupement"),

   /** SIREN d'entreprise */
   SIREN_ENTREPRISE("siren", StringType, "SIREN d'entreprise"),

   /** SIREN de commune */
   SIREN_COMMUNE("sirenCommune", StringType, "SIREN de commune"),

   /** SIREN d'une commune membre */
   SIREN_COMMUNE_MEMBRE("sirenCommuneMembre", StringType, "SIREN d'une commune membre"),

   /** SIREN d'une commune siège */
   SIREN_COMMUNE_SIEGE("sirenCommuneSiege", StringType, "SIREN d'une commune siège"),

   /** SIREN d'un groupement (exemple: intercommunalité) */
   SIREN_GROUPEMENT("sirenGroupement", StringType, "SIREN d'un groupement (ex: celui d'une intercommunalité)"),

   /** SIRET d'établissement */
   SIRET_ETABLISSEMENT("siret", StringType, "SIRET d'établissement"),

   /** SIREN d'un membre (sans type précis du membre : commune, groupement, autre organisme...) */
   SIREN_MEMBRE("sirenMembre", StringType, "SIREN d'un membre"),

   /** Soust-type d'établissement */
   SOUS_TYPE_ETABLISSEMENT("sousTypeEtablissement", StringType, "Sous-type d'établissement"),

   /** Strate de la commune */
   STRATE_COMMUNE("strateCommune", IntegerType, "Strate commune"),

   /** Une surface ou aire */
   SURFACE("surface", DoubleType, "Surface ou aire"),

   /** Titre d'une association */
   TITRE_ASSOCIATION("titre", StringType, "Titre d'une association"),

   /** Type de budget (principal, annexe) */
   TYPE_BUDGET("typeBudget", StringType, "Type de budget (principal, annexe)"),

   /** Type de commune */
   TYPE_COMMUNE("typeCommune", StringType, "Type de commune : normale, déléguée, associée"),

   /** Type d'une compétence de groupement ou de commune */
   TYPE_COMPETENCE("typeCompetence", StringType, "Type d'une compétence : Obligatoire, Facultative, Optionnelle, NonApplicable [pour une nature juridique de groupement ou commune à qui elle ne s'adresse pas]"),

   /** Type d'équipement */
   TYPE_EQUIPEMENT("typeEquipement", StringType, "Type d'équipement"),

   /** Type d'établissement */
   TYPE_ETABLISSEMENT("typeEtablissement", StringType, "Type d'établissement"),

   /** Type de voie. */
   TYPE_DE_VOIE("typeDeVoie", StringType, "Type de voie"),

   /** Type de voie. */
   TYPE_DE_VOIE_SECONDAIRE("typeDeVoieSecondaire", StringType, "Type de voie secondaire"),

   /** Type d'évènement communal. */
   TYPE_EVENEMENT_COMMUNAL("typeEvenementCommunal", StringType, "Type d'évènement communal (fusion, changement de nom...)");

   /** Nom du champ. */
   private final String nomChampDataset;

   /** Description du champ. */
   private final String description;

   /** Type habituel de la donnée. */
   private final DataType dataType;

   /** Colonne générique Spark. */
   private final Column column;

   /** Description détaillée */
   private final String detail;

   /**
    * Construire une description de champ.
    * @param nomChamp Nom du champ, dans les datasets.
    * @param type Type habituel de sa donnée.
    * @param description Description.
    */
   NomChamp(String nomChamp, DataType type, String description, String detail) {
      this.nomChampDataset = nomChamp;
      this.description = description;
      this.dataType = type;
      this.column = org.apache.spark.sql.functions.col(this.nomChampDataset);
      this.detail = detail;
   }

   /**
    * Construire une description de champ.
    * @param nomChamp Nom du champ, dans les datasets.
    * @param type Type habituel de sa donnée.
    * @param description Description.
    */
   NomChamp(String nomChamp, DataType type, String description) {
      this(nomChamp, type, description, null);
   }

   /**
    * Retourner le nom du champ dans un dataset.
    * @return Nom du champ.
    */
   public String champ() {
      return this.nomChampDataset;
   }

   /**
    * Retourner la colonne générique Spark.
    * @return Colonne.
    */
   public Column col() {
      return this.column;
   }

   /**
    * Retourner la colonne Spark d'un dataset.
    * @param dataset Dataset.
    * @param <T> Type du dataset.
    * @return Colonne.
    */
   public <T> Column col(Dataset<T> dataset) {
      return dataset.col(champ());
   }

   /**
    * Retourner une colonne suffixée de Spark.
    * @param suffixe Suffixe à lui placer.
    * @return Colonne.
    */
   public Column col(String suffixe) {
      return org.apache.spark.sql.functions.col(champSuffixe(suffixe));
   }

   /**
    * Retourner la colonne générique Spark.
    * @param dataset Dataset.
    * @param suffixe Suffixe à lui placer.
    * @param <T> Type du dataset.
    * @return Colonne.
    */
   public <T> Column col(Dataset<T> dataset, String suffixe) {
      return dataset.col(champSuffixe(suffixe));
   }

   /**
    * Renvoyer le contenu d'un champ dans un Row.
    * @param row Row.
    * @return Contenu du champ.
    * @param <T> Type du champ
    */
   public <T> T getAs(Row row) {
      return row.getAs(champ());
   }

   /**
    * Renvoyer le contenu d'un champ dans un Row.
    * @param row Row.
    * @param suffixe Suffixe.
    * @return Contenu du champ.
    * @param <T> Type du champ
    */
   public <T> T getAs(Row row, String suffixe) {
      return row.getAs(champSuffixe(suffixe));
   }

   /**
    * Retourner le nom du champ dans un dataset, en lui ajoutant un suffixe.
    * @param suffixe Suffixe à lui placer.
    * @param verbatim true s'il doit concaténer ce qui est proposé tel quel, sans chercher à introduire une règle de nommage
    * @return Nom du champ au format nom_suffixe.
    */
   public String champSuffixe(String suffixe, boolean verbatim) {
      if (verbatim) {
         return this.nomChampDataset + suffixe;
      }

      boolean champAunderscore = this.nomChampDataset.contains("_");
      boolean champAunderscoreFinal = this.nomChampDataset.endsWith("_");

      boolean suffixeAunderscore = suffixe.contains("_");
      boolean suffixeAunderscoreDebut = suffixe.startsWith("_");

      // Si le champ se se termine par un underscore et que le suffixe débute par lui, ne pas y concaténer celui du suffixe
      if (champAunderscoreFinal && suffixeAunderscoreDebut) {
         return this.nomChampDataset + suffixe.substring(1);
      }

      // Si le suffixe a un underscore n'importe où
      if (suffixeAunderscore) {
         // S'il n'est pas au début du suffixe, et que le champ n'en a pas un terminal, lui en rajouter un
         if (champAunderscoreFinal == false && suffixeAunderscoreDebut == false) {
            return this.nomChampDataset + "_" + suffixe;
         }
         else {
            // Sinon, les concaténer tels quels
            return this.nomChampDataset + suffixe;
         }
      }

      // Si le champ présente des underscore, mais le suffixe pas un au début, en placer un entre lui et le suffixe
      if (champAunderscore && suffixeAunderscoreDebut == false) {
         return this.nomChampDataset + "_" + suffixe;
      }

      return this.nomChampDataset + suffixe;
   }

   /**
    * Retourner le nom du champ dans un dataset, en lui ajoutant un suffixe, suivant les règles de nommage par défaut.
    * @param suffixe Suffixe à lui placer.
    * @return Nom du champ au format nom_suffixe.
    */
   public String champSuffixe(String suffixe) {
      return champSuffixe(suffixe, true);
   }

   /**
    * Renvoyer la description du champ.
    * @return Description du champ.
    */
   public String description() {
      return this.description;
   }

   /**
    * Renvoyer le détail du champ.
    * @return Détail du champ.
    */
   public String detail() {
      return this.detail;
   }

   /**
    * Renvoyer le type du champ.
    * @return Type du champ.
    */
   public DataType dataType() {
      return this.dataType;
   }
}
