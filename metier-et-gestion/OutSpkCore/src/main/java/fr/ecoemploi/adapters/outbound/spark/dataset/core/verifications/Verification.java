package fr.ecoemploi.adapters.outbound.spark.dataset.core.verifications;

/**
 * Souhaits de vérifications durant la constitution du Dataset.
 *
 * @author Marc Le Bihan
 */
public enum Verification {
   /**
    * Les données sont comptées à des fins de statistiques.
    */
   COMPTAGES_ET_STATISTIQUES,

   /**
    * La liste des colonnes entièrement non nulles est déterminée sur l'ensemble des données.
    */
   DETERMINER_COLONNES_NON_NULLES,

   /**
    * Les données détectées mal typées par VERIFIER_TYPAGE_COLONNES sont persistées dans un Dataset String, avec un libellé d'incident.
    */
   DUMP_ENREGISTREMENTS_MAL_TYPES,

   /**
    * Les données rejetées, présentées par PRESENTER_REJETS, sont placées dans un Dataset et persistées.
    */
   DUMP_REJETS,

   /**
    * Présenter les premières entrées de chaque dataset, intermédiaire ou final, construit.
    */
   SHOW_DATASET,

   /**
    * Les données exclues sont présentées par un log de niveau approprié à sa sévérité.
    */
   SHOW_REJETS,

   /**
    * Présenter les schémas de chaque Dataset, intermédiaire ou final, construit.
    */
   SHOW_SCHEMAS,

   /**
    * Le contenu des colonnes est contrôlé face à leur type sur l'ensemble des données.
    */
   VERIFIER_TYPAGE_COLONNES
}
