package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.util.*;

import org.apache.spark.sql.*;

import fr.ecoemploi.domain.model.territoire.CodeDepartement;

/**
 * Condition de post-filtrage classique, qui reprend les colonnes les plus régulièrement filtrées : codes départements, codes communes
 * @author Marc Le Bihan
 */
public abstract class AbstractConditionPostFiltrageCommune implements ConditionPostFiltrage {
   /** Restriction aux codes communes */
   private Set<String> restrictionCommunes;

   /** Restriction aux codes départements */
   private Set<String> restrictionDepartements;

   /** Filtrage initial */
   public enum FiltrageInitial {
      /** Filtrage sur code département */
      DEPARTEMENTS,

      /** Filtrage sur code commune */
      COMMUNES,

      /** Filtrage sur code département et commune */
      DEPARTEMENTS_ET_COMMUNES,
   }

   /**
    * Préparer une condition de filtrage initial
    * @param filtrageInitial Style de filtrage : sur codes départements uniquement, codes communes, les deux...
    * @param dataset Dataset où appliquer la condition.
    * @param nomChampCodeDepartement Nom du champ code département dans ce dataset
    * @param nomChampCodeCommune Nom du champ code commune dans ce dataset
    * @param options Options de lecture et de création de dataset
    * @return Condition intiale
    */
   public Column conditionInitiale(OptionsCreationLecture options, Dataset<Row> dataset, FiltrageInitial filtrageInitial, String nomChampCodeDepartement, String nomChampCodeCommune) {
      // Ne s'applique que si des restrictions aux communes ont été demandées
      if (options == null || options.hasRestrictionAuxCommunes() == false) {
         return null;
      }

      this.restrictionCommunes = options.restrictionAuxCommunes();
      String[] codesCommunes = this.restrictionCommunes.toArray(new String[0]);

      this.restrictionDepartements = CodeDepartement.codesDepartements(codesCommunes);
      String[] codesDepartements = this.restrictionDepartements.toArray(new String[0]);

      return switch (filtrageInitial) {
         case DEPARTEMENTS -> dataset.col(nomChampCodeDepartement).isin((Object[])codesDepartements);

         case COMMUNES -> dataset.col(nomChampCodeCommune).isin((Object[])codesCommunes);

         case DEPARTEMENTS_ET_COMMUNES -> dataset.col(nomChampCodeDepartement).isin((Object[])codesDepartements)
            .and(dataset.col(nomChampCodeCommune).isin((Object[])codesCommunes));
      };
   }

   /**
    * Renvoyer les restrictions aux codes communes qui ont été appliquées
    * @return Codes communes
    */
   public Set<String> restrictionCodesCommunesAppliquees() {
      return this.restrictionCommunes;
   }

   /**
    * Renvoyer les restrictions aux codes départements qui ont été appliquées
    * @return Codes départements
    */
   public Set<String> restrictionCodesDepartementsAppliquees() {
      return this.restrictionDepartements;
   }
}
