package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Options de création ou de lecture d'un dataset
 * lors de l'exécution d'une de ses méthodes
 */
@Component
@Scope("prototype")
public class OptionsCreationLecture implements Serializable {
   @Serial
   private static final long serialVersionUID = 6305653576779988363L;

   /** Indique s'il faut remplacer le store, s'il existe. Effacera le répertoire parquet, s'il est trouvé, avant sa réécriture */
   private boolean remplacerStore;

   /** Indique s'il faut renvoyer le contenu du store (relu) à la fin du traitement du dataset, et pas celui constitué en mémoire.<br>
    *  Utile si un repartitionnement a lieu par un partitionBy lors de l'écriture, dont on veut tenir compte. Mais plus coûteux en temps. */
   private boolean toujoursRenvoyerContenuDuStore;

   /** Répertoire temporaire */
   private String tempDir;

   /** Indique s'il faut utiliser le cache ou non. */
   private boolean useCache;

   /** Indique s'il faut générer un CSV ou non, en même temps que le dataset Parquet principal */
   private boolean genererCSV;

   /** Si l'appelant a indiqué avoir déjà fait le tri (et qu'il ne faut pas le refaire) */
   private boolean triDejaFait;

   /** Paramétrage optionnel d'un sample, mis en oeuvre si différent de 0. */
   private double sampleFraction = 0.0;

   /** Paramétrage du seed associé : aléatoire si égal à 0. */
   private Long sampleSeed = 0L;

   /** Ignorer les restrictions */
   private boolean restrictionsIgnore;

   /** Restriction aux communes lors de la création ou l'emploi des datasets, si non null. */
   private Set<String> restrictionAuxCommunes;

   /** Restriction à l'année, lors de la création ou l'emploi des datasets, si non null. */
   private Integer restrictionAnnee;

   /** Restriction en écriture seulement (la lecture cherche à lire prioritairement la totalité des données) */
   private boolean restrictionEnEcritureSeulement;

   /**
    * Construire des options de création et lecture de dataset.
    * @param remplacerStore Indique s'il faut remplacer le store, s'il existe. Effacera le répertoire parquet, s'il est trouvé, avant sa réécriture
    * @param toujoursRenvoyerContenuDuStore Indique s'il faut renvoyer le contenu du store (relu) à la fin du traitement du dataset, et pas celui constitué en mémoire.<br>
    *     Utile si un repartitionnement a lieu par un partitionBy lors de l'écriture, dont on veut tenir compte. Mais plus coûteux en temps
    * @param tempDir Répertoire temporaire
    * @param useCache Indique s'il faut utiliser le cache ou non
    * @param restrictionsIgnore true, s'il faut ignorer les restrictions.
    * @param restrictionAuxCommunes Restriction aux communes, si non null.
    * @param restrictionAnnee Restriction à l'année, si non null.
    * @param restrictionEnEcritureSeulement Restriction en écriture seulement (la lecture cherche à lire prioritairement la totalité des données).
    */
   @Autowired
   public OptionsCreationLecture(@Value("${remplacer.store:true}") boolean remplacerStore,
      @Value("${toujours.renvoyer.contenu.store:true}") boolean toujoursRenvoyerContenuDuStore,
      @Value("${temp.dir}") String tempDir, @Value("${useCache:true}") boolean useCache,
      @Value("${genererCSV:true}") boolean genererCSV,
      @Value("${restriction.ignore:false}") boolean restrictionsIgnore,
      @Value("${restriction.communes:null}") Set<String> restrictionAuxCommunes, @Value("${restriction.annee:0}") Integer restrictionAnnee,
      @Value("${restriction.en.ecriture.seulement:true}") boolean restrictionEnEcritureSeulement) {
      this.remplacerStore = remplacerStore;
      this.toujoursRenvoyerContenuDuStore = toujoursRenvoyerContenuDuStore;
      this.tempDir = tempDir;
      this.useCache = useCache;
      this.genererCSV = genererCSV;

      this.restrictionsIgnore = restrictionsIgnore;
      this.restrictionAuxCommunes = restrictionAuxCommunes;
      this.restrictionAnnee = restrictionAnnee;
      this.restrictionEnEcritureSeulement = restrictionEnEcritureSeulement;
   }

   /**
    * Construire des options de création et lecture de dataset par copie
    * @param options Options à copier.
    */
   public OptionsCreationLecture(OptionsCreationLecture options) {
      this(options.remplacerStore, options.toujoursRenvoyerContenuDuStore, options.tempDir, options.useCache, options.genererCSV,
         options.restrictionsIgnore, options.restrictionAuxCommunes, options.restrictionAnnee, options.restrictionEnEcritureSeulement);
      this.triDejaFait = options.triDejaFait;
      this.sampleFraction = options.sampleFraction;
      this.sampleSeed = options.sampleSeed;
      this.restrictionsIgnore = options.restrictionsIgnore;
      this.restrictionAuxCommunes = options.restrictionAuxCommunes;
      this.restrictionAnnee = options.restrictionAnnee;
      this.restrictionEnEcritureSeulement = options.restrictionEnEcritureSeulement;
   }

   /**
    * Déterminer s'il y a une restriction à l'année à appliquer.
    * @return true s'il y a une année fournie par <code>restrictionAnnee()</code> pour limiter la création ou l'emploi des datasets, false s'il n'y a pas de filtre
    */
   public boolean hasRestrictionAnnee() {
      Integer annee = restrictionAnnee();
      return restrictionsIgnorees() == false && annee != null && annee != 0;
   }

   /**
    * Déterminer s'il y a une restriction aux communes à appliquer.
    * @return true s'il y a des codes communes fournis par <code>restrictionAuxCommunes()</code> auxquels limiter la création ou l'emploi des datasets, false s'il n'y a pas de filtre
    */
   public boolean hasRestrictionAuxCommunes() {
      return restrictionsIgnorees() == false && restrictionAuxCommunes() != null && restrictionAuxCommunes().isEmpty() == false && restrictionAuxCommunes().iterator().next().equals("null") == false;
   }

   /**
    * Déterminer si au moment de l'écriture d'un dataset parquet, son image en CSV est également produite ou non.
    * @return true si le fichier CSV est également produit.
    */
   public boolean hasRestrictionEnEcritureSeulement() {
      return restrictionEnEcritureSeulement();
   }

   /**
    * Déterminer si au moment de l'écriture d'un dataset parquet, son image en CSV est également produite ou non.
    * @return true si le fichier CSV est également produit.
    */
   public boolean genererCSV() {
      return this.genererCSV;
   }

   /**
    * Indiquer si au moment de l'écriture d'un dataset parquet son image en CSV est également produite ou non.
    * @param genererCSV true si le fichier CSV est également produit.
    * @return les options en cours de constitution
    */
   public OptionsCreationLecture genererCSV(boolean genererCSV) {
      this.genererCSV = genererCSV;
      return this;
   }

   /**
    * Déterminer s'il faut remplacer le store, s'il existe.<br>
    * Effacera le répertoire parquet, s'il est trouvé, avant sa réécriture.
    * @return true, s'il faut le supprimer pour le remplacer par l'instruction store qui suivra.
    */
   public boolean remplacerStore() {
      return this.remplacerStore;
   }

   /**
    * Indiquer s'il faut remplacer le store, s'il existe.<br>
    * Effacera le répertoire parquet, s'il est trouvé, avant sa réécriture.
    * @param remplacer true s'il faut le supprimer pour le remplacer par l'instruction store qui suivra.
    * @return les options en cours de constitution
    */
   public OptionsCreationLecture remplacerStore(boolean remplacer) {
      this.remplacerStore = remplacer;
      return this;
   }

   /**
    * Déterminer s'il a été demandé que les restrictions soient ignorées.
    * @return true s'il a été demandé que d'éventuelles restrictions présentes soient ignorées.
    */
   public boolean restrictionsIgnorees() {
      return this.restrictionsIgnore;
   }

   /**
    * Indiquer si l'on souhaite que les restrictions éventuellement présentes soient ignorées.
    * @param restrictionsIgnore true si l'on souhaite que les restrictions présentes soient ignorées.
    * @return les options en cours de constitution
    */
   public OptionsCreationLecture restrictionsIgnorees(boolean restrictionsIgnore) {
      this.restrictionsIgnore = restrictionsIgnore;
      return this;
   }

   /**
    * Renvoyer la restriction à une année à appliquer.
    * @return Année à laquelle restreindre l'écriture et éventuellement la lecture des datasets, si non null.
    */
   public Integer restrictionAnnee() {
      return this.restrictionAnnee;
   }

   /**
    * Déterminer si une année est éligible à la mise en cache pour un dataset (dans le cas où elle serait demandée).
    * @param annee Année pour laquelle déterminer si l'écriture en cache pourrait être empêchée.
    * @return true si l'année est (éventuellement sous autres conditions) éligible pour que des datasets soient écrits en cache.
    */
   public boolean anneeEligibleMiseEnCache(int annee) {
      return restrictionsIgnorees() || hasRestrictionAnnee() == false || annee == restrictionAnnee();
   }

   /**
    * Restreindre la création ou l'emploi des datasets à une liste de communes.
    * @param restrictionAnnee Listes des codes communes auxquels limiter la création ou l'emploi des datasets
    * @return les options en cours de constitution
    */
   public OptionsCreationLecture restrictionAnnee(Integer restrictionAnnee) {
      this.restrictionAnnee = restrictionAnnee;
      return this;
   }

   /**
    * Renvoyer la restriction aux communes à appliquer.
    * @return Listes des codes communes auxquels limiter la création et éventuellement l'emploi des datasets, si non null.
    */
   public Set<String> restrictionAuxCommunes() {
      return this.restrictionAuxCommunes;
   }

   /**
    * Restreindre la création ou l'emploi des datasets à une liste de communes.
    * @param restrictionAuxCommunes Listes des codes communes auxquels limiter la création ou l'emploi des datasets
    * @return les options en cours de constitution
    */
   public OptionsCreationLecture restrictionAuxCommunes(Set<String> restrictionAuxCommunes) {
      this.restrictionAuxCommunes = restrictionAuxCommunes;
      return this;
   }

   /**
    * Déterminer si les restrictions éventuelles à la création ou l'emploi des datasets, se fait en écriture seulement.
    * @return true si les restrictions éventuellement demandées ne doivent s'appliquer qu'en écriture de dataset, et que la lecture, elle,
    * cherche toujours à lire le plus vaste.
    */
   public boolean restrictionEnEcritureSeulement() {
      return this.restrictionEnEcritureSeulement;
   }

   /**
    * Restreindre la création ou l'emploi des datasets en écriture seulement.
    * @param restrictionEnEcritureSeulement true si les restrictions éventuellement demandées ne doivent s'appliquer qu'en écriture de dataset,
    *        et que la lecture, elle, cherche toujours à lire le plus vaste.
    * @return les options en cours de constitution
    */
   public OptionsCreationLecture restrictionEnEcritureSeulement(boolean restrictionEnEcritureSeulement) {
      this.restrictionEnEcritureSeulement = restrictionEnEcritureSeulement;
      return this;
   }

   /**
    * Renvoyer la fraction du dataset renvoyé par le sample<br>
    * @return fraction. Un sample est pris si la fraction est différente de 0 et de 1.
    */
   public double sampleFraction() {
      return this.sampleFraction;
   }

   /**
    * Fixer la fraction du dataset renvoyé par le sample<br>
    * @param fraction Un sample est pris si la fraction est différente de 0 et de 1.
    * @return les options en cours de constitution
    */
   public OptionsCreationLecture sampleFraction(double fraction) {
      this.sampleFraction = fraction;
      return this;
   }

   /**
    * Renvoyer le seed servant à générer un sample.
    * @return Seed. Le sample sera aléatoire s'il est égal à 0 ou null.
    */
   public Long sampleSeed() {
      return this.sampleSeed;
   }

   /**
    * Fixer le seed servant à générer un sample.
    * @param seed Le sample sera aléatoire s'il est égal à 0 ou null.
    * @return les options en cours de constitution
    */
   public OptionsCreationLecture sampleSeed(Long seed) {
      this.sampleSeed = seed;
      return this;
   }

   /**
    * Renvoyer le répertoire de stockage des fichiers temporaires (dont les fichiers parquet).<br>
    * Ce répertoire est le répertoire racine, parfois adapté s'il y a une restriction à des codes communes
    * dans la production des datasets.
    * @return Répertoire de stockage. Seul la première intercommunalité ou la première commune est prise comme sous-répertoire, s'il y a lieu.
    */
   public String tempDir() {
      String temp = tempDirRoot();

      StringBuilder format = new StringBuilder();
      List<Object> args = new ArrayList<>();

      // Restriction aux communes
      if (hasRestrictionAuxCommunes()) {
         format.append(restrictionAuxCommunes().size() > 1 ? "/Communes_{0}" : "/Commune_{0}");
         args.add(restrictionAuxCommunes().iterator().next());
      }
      else {
         args.add(null);
      }

      // Restriction sur une année
      if (hasRestrictionAnnee()) {
         format.append("/Annee_{1,number,#0}");
         args.add(restrictionAnnee());
      }
      else {
         args.add(null);
      }

      String sousRepertoire = MessageFormat.format(format.toString(), args.toArray());

      File restrictionDir = new File(temp, sousRepertoire);
      return restrictionDir.getAbsolutePath();
   }

   /**
    * Renvoyer le répertoire de stockage racine des fichiers temporaires (dont les fichiers parquet).
    * @return Répertoire de stockage.
    */
   public String tempDirRoot() {
      return this.tempDir;
   }

   /**
    * Fixer le répertoire de stockage des fichiers temporaires (dont les fichiers parquet).
    * @param directory Répertoire de stockage.
    * @return les options en cours de constitution
    */
   public OptionsCreationLecture tempDir(String directory) {
      this.tempDir = directory;
      return this;
   }

   /**
    * Déterminer s'il faut renvoyer le contenu du store (relu) à la fin du traitement du dataset, et pas celui constitué en mémoire.<br>
    * Utile si un repartitionnement a lieu par un partitionBy lors de l'écriture, dont on veut tenir compte. Mais plus coûteux en temps.
    * @return true s'il faut toujours le relire et le renvoyer une fois qu'il est constitué.
    */
   public boolean toujoursRenvoyerContenuDuStore() {
      return this.toujoursRenvoyerContenuDuStore;
   }

   /**
    * Indiquer s'il faut renvoyer le contenu du store (relu) à la fin du traitement du dataset, et pas celui constitué en mémoire.<br>
    * Utile si un repartitionnement a lieu par un partitionBy lors de l'écriture, dont on veut tenir compte. Mais plus coûteux en temps.
    * @param relireAvantDeRenvoyer true s'il faut toujours le relire et le renvoyer une fois qu'il est constitué<br>
    *    false s'il est renvoyé tel qu'il a été constitué en mémoire.
    * @return les options en cours de constitution
    */
   public OptionsCreationLecture toujoursRenvoyerContenuDuStore(boolean relireAvantDeRenvoyer) {
      this.toujoursRenvoyerContenuDuStore = relireAvantDeRenvoyer;
      return this;
   }

   /**
    * L'appelant indique qu'il a déjà fait le tri du dataset
    * @return true s'il l'a déjà fait, et qu'il ne faut pas le refaire.
    */
   public boolean triDejaFait() {
      return this.triDejaFait;
   }

   /**
    * Indiquer si l'appelant annonce qu'il a déjà fait le tri du dataset
    * @param triDejaFait true s'il l'a déjà fait, et qu'il ne faut pas le refaire.
    * @return Les options en cours de constitution
    */
   public OptionsCreationLecture triDejaFait(boolean triDejaFait) {
      this.triDejaFait = triDejaFait;
      return this;
   }

   /**
    * Déterminer si le cache est utilisé.
    * @return true s'il l'est<br>
    * false si le dataset sera recalculé à chaque fois.
    */
   public boolean useCache() {
      return this.useCache;
   }

   /**
    * Indiquer s'il faut utiliser le cache.
    * @param utiliseCache true s'il faut l'utiliser
    * <br>false si le dataset sera recalculé à chaque fois.
    * @return les options en cours de constitution
    */
   public OptionsCreationLecture useCache(boolean utiliseCache) {
      this.useCache = utiliseCache;
      return this;
   }
}
