package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.event.Level;

import org.springframework.context.support.ResourceBundleMessageSource;

import org.apache.spark.sql.*;
import org.apache.spark.util.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.validation.*;

/**
 * Renvoie un historique d'exécution.
 * @author Marc Le Bihan
 */
public class HistoriqueExecution implements Serializable {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -6518733758427422966L;

   /** Accumulateurs d'erreurs associés. */
   private final Map<String, LongAccumulator> accumulators = new HashMap<>();

   /** Accumulateur d'éléments invalides. */
   private CollectionAccumulator<ElementInvalide> elementsInvalidesAccumulator;

   /** Règles de validation utilisées. */
   private ReglesDeValidation reglesDeValidation;

   /** Message Source. */
   private transient ResourceBundleMessageSource messageSource;

   /**
    * Construire un historique d'exécution.
    */
   public HistoriqueExecution() {
   }

   /**
    * Logguer un message repris depuis le message source de Spring
    * @param log Logger
    * @param level Niveau de log
    * @param codeMessage Code du message
    * @param arg Argument
    */
   public void log(Logger log, Level level, String codeMessage, Object... arg) {
      if (this.messageSource == null) {
         this.messageSource = new ResourceBundleMessageSource();
      }

      String message = this.messageSource.getMessage(codeMessage, arg, Locale.getDefault());

      switch(level) {
         case ERROR -> log.error(message);
         case WARN -> log.warn(message);
         case INFO -> log.info(message);
         case DEBUG -> log.debug(message);
         case TRACE -> log.trace(message);
      }
   }

   /**
    * Ajouter un élément invalide.
    * @param codeRegleValidation Code de la règle de validation.
    * @param arguments Arguments associés.
    */
   public void incrementerOccurrences(String codeRegleValidation, Serializable[] arguments) {
      Objects.requireNonNull(codeRegleValidation, "Le code règle de validation ne peut pas valoir null");
      Objects.requireNonNull(this.elementsInvalidesAccumulator, "L'accumulateur n'est pas initialisé, il ne peut être incrémenté");

      LongAccumulator accumulator = this.accumulators.get(codeRegleValidation);

      if (accumulator != null) {
         accumulator.add(1);
      }

      this.elementsInvalidesAccumulator.add(new ElementInvalide(codeRegleValidation, arguments));
   }

   /**
    * Renvoyer les notifications.
    * @return Notifications.
    */
   protected Map<String, LongAccumulator> getNotifications() {
      return this.accumulators;
   }

   /**
    * Ajouter les codes des règles de validation qui pourront être émises.
    * @param session Session Spark.
    * @param validator Validateur de dataset.
    */
   public void addReglesValidation(SparkSession session, AbstractRowValidator validator) {
      aucuneDeRegleValidation(session);

      for(RegleDeValidation regleDeValidation : validator) {
         this.reglesDeValidation.addIfNew(regleDeValidation);

         String codeRegle = regleDeValidation.getCodeRegleValidation();
         this.accumulators.putIfAbsent(codeRegle, session.sparkContext().longAccumulator(codeRegle));
      }
   }

   /**
    * Ne déclarer aucune règle de validation.
    * @param session Session Spark.
    */
   public void aucuneDeRegleValidation(SparkSession session) {
      if (this.reglesDeValidation == null) {
         this.reglesDeValidation = new ReglesDeValidation();
      }

      // Le but de cette fonction est d'initialiser les accumulateurs Spark, qui ne peuvent l'être qu'à un moment précis.
      if (this.elementsInvalidesAccumulator == null) {
         this.elementsInvalidesAccumulator = session.sparkContext().collectionAccumulator();
      }
   }

   /**
    * Destination de dump
    */
   public abstract static class AbstractDumpDestination implements Consumer<Pair<String, Object[]>> {
   }

   /**
    * Destination de dump : Logger
    */
   public static class DumpDestinationLogger extends AbstractDumpDestination {
      private final Logger log;

      /**
       * Construire un dump vers un logger
       * @param log Logger
       */
      public DumpDestinationLogger(Logger log) {
         this.log = log;
      }

      @Override
      public void accept(Pair<String, Object[]> formatAndTheirArgs) {
         if (formatAndTheirArgs.getValue() == null || formatAndTheirArgs.getValue().length == 0) {
            this.log.info(formatAndTheirArgs.getKey());
         }
         else {
            String message = MessageFormat.format(formatAndTheirArgs.getKey(), formatAndTheirArgs.getValue());
            this.log.info(message);
         }
      }
   }

   /**
    * Destination de dump : Writer
    */
   public static class DumpDestinationWriter extends AbstractDumpDestination {
      private final PrintWriter prt;

      /**
       * Construire un dump vers un writer
       * @param wtr Writer
       */
      public DumpDestinationWriter(Writer wtr) {
         this.prt = new PrintWriter(wtr, true);
      }

      @Override
      public void accept(Pair<String, Object[]> formatAndTheirArgs) {
         if (formatAndTheirArgs.getValue() == null || formatAndTheirArgs.getValue().length == 0) {
            this.prt.println(formatAndTheirArgs.getKey());
         }
         else {
            this.prt.println(MessageFormat.format(formatAndTheirArgs.getKey(), formatAndTheirArgs.getValue()));
         }
      }
   }

   /**
    * Dumper sur une destination les notifications accumulées.
    * @param destination Destination.
    * @param alimentesSeulement true si l'on n'émet que ceux qui ont un compteur d'occurrences supérieur à zéro.
    * @return true si au moins une notification est présente dans cet historique. false, s'il est vide.
    */
   public boolean dumpNotifications(AbstractDumpDestination destination, boolean alimentesSeulement) {
      destination.accept(Pair.of(
         getNotifications().isEmpty() ? "Il n'y a aucune notification dans l'historique d'exécution" : "Notifications de l'historique d'exécution :", null)
      );

      ElementsInvalides elements = null;

      if (this.elementsInvalidesAccumulator != null) {
         elements = new ElementsInvalides(this.elementsInvalidesAccumulator.value());
      }

      destination.accept(Pair.of(
         elements == null || elements.isEmpty() ? "Il n'y a aucun paramètre associé aux messages dans l'historique d'exécution" : "Paramètres des notifications :", null
      ));

      for(Map.Entry<String, LongAccumulator> rapport : getNotifications().entrySet()) {
         dumpNotification(rapport, elements, destination, alimentesSeulement);
      }

      return getNotifications().isEmpty() == false;
   }

   /**
    * Dumper sur une destination une notification.
    * @param rapport La notification du rapport à dumper.
    * @param elements Eléments invalides
    * @param destination Destination.
    * @param alimentesSeulement true si l'on n'émet que ceux qui ont un compteur d'occurrences supérieur à zéro.
    */
   public void dumpNotification(Map.Entry<String, LongAccumulator> rapport, ElementsInvalides elements, AbstractDumpDestination destination, boolean alimentesSeulement) {
      long count = rapport.getValue().count();

      if (alimentesSeulement && count == 0) {
         return;
      }

      RegleDeValidation regleDeValidation = this.reglesDeValidation.get(rapport.getKey());

      if (regleDeValidation == null) {
         destination.accept(Pair.of(
            "Une règle de validation {0} n''a pas été retrouvée lors du dump des notifications d''erreur", new Object[]{rapport.getKey()})
         );

         return;
      }

      destination.accept(Pair.of(
         "{0} : {1} ({2}) : {3}", new Object[] {
            regleDeValidation.getCodeRegleValidation(), regleDeValidation.getDescriptif(), regleDeValidation.getCodeOuFormatMessage(), rapport.getValue().count()}
         )
      );

      if (elements != null) {
         for(ElementInvalide elementInvalide : elements) {
            if (elementInvalide.getCodeRegleValidation().equals(rapport.getKey())) {
               destination.accept(Pair.of(
                  "{0} : {1}", new Object[]{elementInvalide.getArguments(), elementInvalide.getOccurrences()})
               );
            }
         }
      }
   }
}
