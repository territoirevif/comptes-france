package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.io.Serial;
import java.text.MessageFormat;
import java.util.*;

import org.apache.spark.sql.*;
import org.slf4j.*;

/**
 * Classe de base des tris de dataset
 */
public class TriDataset implements TriParqueteurIF {
   /** Serial UID */
   @Serial
   private static final long serialVersionUID = 4079726104509734185L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(TriDataset.class);

   /** Suffixe du fichier trié. */
   private final String suffixeFichier;

   /** Nom de la colonne de partitionnement. */
   private final String nomColonnePartionnement;

   /** Nom des colonnes de tri. */
   private final String[] nomsColonnesTri;

   /** Déterminer s'il faut trier au sein des partitions. */
   private final boolean sortWithinPartition;

   /** Déterminer s'il faut faire un partition by Range. */
   private final boolean partitionByRange;

   /**
    * Constuire un tri possible.
    * @param suffixeFichier            Suffixe du fichier trié.
    * @param sortWithinPartition       true s'il faut faire un tri au sein des partitions,<br>
    *                                  false si c'est un tri global.
    * @param nomColonnePartitionnement Nom de la colonne de partitionnement, s'il y en a une.
    * @param nomColonnesTri            Nom des colonnes de tri.
    * @param partitionByRange          true s'il faut faire un partitionnement par range.
    */
   public TriDataset(String suffixeFichier, boolean sortWithinPartition, String nomColonnePartitionnement, boolean partitionByRange, String... nomColonnesTri) {
      this.suffixeFichier = suffixeFichier;
      this.nomColonnePartionnement = nomColonnePartitionnement;
      this.nomsColonnesTri = nomColonnesTri;
      this.sortWithinPartition = sortWithinPartition;
      this.partitionByRange = partitionByRange;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getNomColonnePartionnement() {
      return this.nomColonnePartionnement;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String[] getNomsColonnesTri() {
      return this.nomsColonnesTri;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getSuffixeFichier() {
      return this.suffixeFichier;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean sortWithinPartition() {
      return this.sortWithinPartition;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean partitionByRange() {
      return this.partitionByRange;
   }

   /**
    * Créer une condition de jointure entre deux datasets reliés par des<br>
    * <code>datasetA.col(champ1).equalTo(datasetB.col(champ1)).and(datasetA.col(champ2).equalTo(datasetB.col(champ2)))</code><br>
    * d'après leurs colonnes de tri et de partitionnement.
    * @param datasetA Premier dataset à joindre
    * @param datasetB Second dataset à joindre
    * @param mentionnerPartionnement true s'il faut faire participer la colonne de partitionnement dans la condition de jointure.
    * @return Condition de jointure
    * @param <A> Classe du premier dataset
    * @param <B> Classe du second dataset
    * @throws NoSuchElementException si une colonne de tri ou de partitionnement n'existe pas dans l'un des datasets.
    */
   public <A,B> Column joinCondition(Dataset<A> datasetA, Dataset<B> datasetB, boolean mentionnerPartionnement) {
      Column joinCondition = null;
      Set<String> colonnesTri = new HashSet<>(Arrays.asList(this.nomsColonnesTri));
      Set<String> colonnesA = new HashSet<>(Arrays.asList(datasetA.columns()));
      Set<String> colonnesB = new HashSet<>(Arrays.asList(datasetB.columns()));

      // Joindre par les colonnes de tri
      for(String nomColonneTri : this.nomsColonnesTri) {
         Column a = colonne(colonnesA, datasetA, nomColonneTri, "tri", "premier");
         Column b = colonne(colonnesB, datasetB, nomColonneTri, "tri", "second");
         Column equalTo = a.equalTo(b);

         joinCondition = (joinCondition == null) ? equalTo : joinCondition.and(equalTo);
      }

      // Et par celle de partitionnement, s'il y en a une, que cela a été demandé, et qu'elle n'a pas déjà été mentionnée.
      if (mentionnerPartionnement && this.nomColonnePartionnement != null && colonnesTri.contains(this.nomColonnePartionnement) == false) {
         Column a = colonne(colonnesA, datasetA, this.nomColonnePartionnement, "partitionnement", "premier");
         Column b = colonne(colonnesB, datasetB, this.nomColonnePartionnement, "partitionnement", "second");
         Column equalTo = a.equalTo(b);

         joinCondition = (joinCondition == null) ? equalTo : joinCondition.and(equalTo);
      }

      return joinCondition;
   }

   /**
    * Déterminer si un autre tri a des colonnes de tri identique au notre.
    * @param candidat Tri candidat.
    * @return true si c'est le cas.
    */
   public boolean memeTri(TriParqueteurIF candidat) {
      Set<String> nous = new HashSet<>(Arrays.asList(this.nomsColonnesTri));
      Set<String> lui = new HashSet<>(Arrays.asList(candidat.getNomsColonnesTri()));
      return nous.containsAll(lui);
   }

   /**
    * Déterminer si un autre tri a des colonnes de tri et un partitionnement identique au notre.
    * @param candidat Tri candidat.
    * @return true si c'est le cas.
    */
   public boolean memeTriEtPartitionnement(TriParqueteurIF candidat) {
      String lui = candidat.getNomColonnePartionnement();

      if (this.nomColonnePartionnement == null && lui == null) {
         return true;
      }

      if (this.nomColonnePartionnement == null || lui == null) {
         return false;
      }

      return this.nomColonnePartionnement.equals(lui) && memeTri(candidat);
   }

   /**
    * S'assurer qu'une colonne est présente dans un dataset, puis la donner.
    * @param colonnes Liste des colonnes du dataset.
    * @param dataset Dataset considéré.
    * @param nomColonne Nom de la colonne recherchée.
    * @param type Type de la colonne, informatif.
    * @param description Description du dataset dans l'opération, informatif.
    * @throws NoSuchElementException si la colonne recherchée n'existe pas dans le dataset.
    */
   private <T> Column colonne(Set<String> colonnes, Dataset<T> dataset, String nomColonne, String type, String description) {
      if (colonnes.contains(nomColonne) == false) {
         String format = "Le {3} Dataset à joindre (de type {0}) n''a pas de colonne de {2} {1}";
         String message = MessageFormat.format(format, dataset.getClass().getSimpleName(), nomColonne, type, description);
         LOGGER.error(message);
         throw new NoSuchElementException(message);
      }

      return dataset.col(nomColonne);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      String format = "Suffixe du fichier: {0}, colonne de partitionnement: {1}, colonnes de tri: {2}, trier au sein des partitions: {3}, faire des partitions par intervalles: {4}";
      return MessageFormat.format(format, this.suffixeFichier, this.nomColonnePartionnement, Arrays.toString(this.nomsColonnesTri), this.sortWithinPartition, this.partitionByRange);
   }
}
