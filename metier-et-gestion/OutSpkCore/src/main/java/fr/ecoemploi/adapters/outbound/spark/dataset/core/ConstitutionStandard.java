package fr.ecoemploi.adapters.outbound.spark.dataset.core;

import java.io.*;
import java.text.MessageFormat;
import java.util.function.Supplier;

import org.apache.spark.sql.*;
import org.slf4j.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.domain.utils.objets.GenerationDonneesNonAutoriseeException;

import static fr.ecoemploi.domain.utils.console.ConsoleEscapeCodes.BYELLOW;

/**
 * Constitution standard de dataset Row
 * @author Marc Le Bihan
 */
public class ConstitutionStandard implements Serializable {
   @Serial
   private static final long serialVersionUID = -7593115902985662875L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ConstitutionStandard.class);

   /**
    * Constituer un dataset selon la méthode standard : <br>
    *    - Lecture depuis le store (le cache)<br>
    *    - S'il n'existe pas, constitution<br>
    *    - et sauvegarde dans le store, avant renvoi.
    *
    * @param parent classe parente de ce délégué
    * @param optionsCreationLecture Option de lecture et création de dataset.
    * @param worker Worker pour constituer le dataset, s'il faut le générer.
    * @param historiqueExecution Historique d'exécution.
    * @param cacheParqueteur Gestionnaire de cache pour ce dataset.
    * @return Dataset constitué.
    * @deprecated Utiliser la méthode prenant <code>Column postFiltre</code> en argument.
    */
   @Deprecated(forRemoval = true)
   public Dataset<Row> constitutionStandard(AbstractSparkDataset parent, OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution,
     Supplier<Dataset<Row>> worker, CacheParqueteur<? extends TriParqueteurIF> cacheParqueteur) {
      return constitutionStandard(parent, optionsCreationLecture, historiqueExecution, worker, true, (ConditionPostFiltrage) null, cacheParqueteur);
   }

   /**
    * Constituer un dataset selon la méthode standard : <br>
    *    - Lecture depuis le store (le cache)<br>
    *    - S'il n'existe pas, constitution<br>
    *    - et sauvegarde dans le store, avant renvoi.
    *
    * @param parent classe parente de ce délégué
    * @param optionsCreationLecture Option de lecture et création de dataset.
    * @param worker Worker pour constituer le dataset, s'il faut le générer.
    * @param anneeEligibleSauvegarde true si l'année en cours de traitement (si applicable) rend ce dataset éligible à une possible sauvegarde (éventuellement, sous autres conditions),
    *    false si, pour une restriction sur année par exemple, ce dataset constitué ne devra pas être sauvegardé.
    * @param conditionPostFiltrage Fonction de post-filtrage <code>Column = postFiltrage(options, dataset_issu_du_worker)</code>, si non <code>null</code> une clause where avec cette condition sera appliquée avant l'écriture du dataset
    * @param cacheParqueteur Gestionnaire de cache pour ce dataset.
    * @param historiqueExecution Historique d'exécution à alimenter, si non null.
    * @return Dataset constitué.
    * @throws GenerationDonneesNonAutoriseeException s'il est nécessaire de produire les données générées, mais que l'application n'en a pas le droit
    */
   public Dataset<Row> constitutionStandard(AbstractSparkDataset parent, OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution,
      Supplier<Dataset<Row>> worker, boolean anneeEligibleSauvegarde, ConditionPostFiltrage conditionPostFiltrage, CacheParqueteur<? extends TriParqueteurIF> cacheParqueteur) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : parent.optionsCreationLecture();

      Dataset<Row> dataset = cacheParqueteur.call(options.useCache());
      boolean existeEnCache = dataset != null;
      Dataset<Row> workerDataset = null;

      if (existeEnCache == false) {
         // Vérifier que nous sommes autorisés à générer ces données
         if (parent.isGenerationDonneesAutorisee() == false) {
            String format = "Le fichier ou répertoire de données générées ''{0}'' n''existe pas, mais l''application n''est pas autorisée à le générer.";
            String message = MessageFormat.format(format, cacheParqueteur.getStore(false));
            LOGGER.warn(BYELLOW.color(message));

            throw new GenerationDonneesNonAutoriseeException(message);
         }

         workerDataset = worker.get();
      }

      if (existeEnCache == false) {
         dataset = cacheParqueteur.appliquer(workerDataset, options.triDejaFait());
      }

      // Le dataset peut avoir été obtenu du cache, mais c'est une restriction qui est demandée dessus, et qu'il faut sauvegarder.
      // C'est le cas si la restriction doit s'appliquer en écriture seulement, qu'il y a un filtre demandé, mais pas encore de cache restreint.
      boolean storeRestreintExiste = new File(cacheParqueteur.getStore(false)).exists();

      Column postFiltre = null;

      if (conditionPostFiltrage != null) {
         postFiltre = conditionPostFiltrage.apply(options, worker);
      }

      boolean postFiltrer = postFiltre != null && (existeEnCache == false || (options.restrictionEnEcritureSeulement() && storeRestreintExiste == false));
      Dataset<Row> datasetPostFiltre = null;

      if (postFiltrer) {
         datasetPostFiltre = dataset.where(postFiltre);
      }
      else {
         // S'il n'y a pas de post filtrage à faire, et qu'un dataset du cache nous a été donné, le renvoyer sans le sauvegarder de nouveau.
         if (existeEnCache) {
            return dataset;
         }
      }

      // Au retour de la fonction de constitution standard, le dataset restreint peut avoir été sauvegardé.
      // Mais, à moins que la propriété restriction.en.ecriture.seulement soit à true, c'est le dataset complet qui sera renvoyé
      // pour que les traitements qui suivent ne soient pas affectés par cette restriction.
      boolean renvoyerDatasetRestreint = postFiltre != null && options.restrictionEnEcritureSeulement() == true || datasetPostFiltre == null;

      if (anneeEligibleSauvegarde == false) {
         LOGGER.info("Les conditions de filtrage post-constitution du dataset ne rendent pas son instance {} pour cette année éligible à la sauvegarde. " +
            "Il est renvoyé sans être mis en cache.", renvoyerDatasetRestreint ? "restreinte" : "complète");

         return renvoyerDatasetRestreint ? datasetPostFiltre : dataset;
      }

      // Si un historique de création de dataset nous a été réclamé, et que nous avons bien passé les étapes de construction du dataset
      // (c'est à dire : que nous n'en avons pas repris un, en cache, pour l'exploiter)
      // nous pouvons maintenant le placer car la sauvegarde du dataset qu'il décrit va bien avoir lieu.
      // le dumper dans un fichier à côté.
      if (historiqueExecution != null && existeEnCache == false) {
         creerHistoriqueExecution(historiqueExecution, cacheParqueteur);
      }

      return cacheParqueteur.save(renvoyerDatasetRestreint && datasetPostFiltre != null ? datasetPostFiltre : dataset);
   }

   /**
    * Créer un fichier d'historique d'exécution.
    * @param historiqueExecution Historique d'exécution.
    * @param cacheParqueteur Cache parqueteur.
    */
   private void creerHistoriqueExecution(HistoriqueExecution historiqueExecution, CacheParqueteur<? extends TriParqueteurIF> cacheParqueteur) {
      String store = cacheParqueteur.getStore(false);
      String fichierHistorique = MessageFormat.format("{0}.historiqueCreation", store);

      try {
         try (FileWriter fw = new FileWriter(fichierHistorique)) {
            boolean notificationsPresentes = historiqueExecution.dumpNotifications(new HistoriqueExecution.DumpDestinationWriter(fw), true);

            LOGGER.info("Un fichier d'historique d'exécution, {}, a été créé : il {}.",
               new File(fichierHistorique).getName(), notificationsPresentes ? "porte des notifications" : "est vide");
         }
      } catch (IOException e) {
         LOGGER.warn("Le fichier de rapport d'historique d'exécution, {}, n'a pas été créé : {}.", fichierHistorique, e.getMessage());
      }
   }
}
