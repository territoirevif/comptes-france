package fr.ecoemploi.adapters.outbound.spark.dataset.core.validation;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;

import org.springframework.context.MessageSource;

/**
 * Règle de validation
 * @author Marc LE BIHAN
 */
public class RegleDeValidation implements Serializable {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 6110285913144342428L;

   /** Code de la règle de validation. */
   private String codeRegleValidation;

   /** Descriptif du message. */
   private String descriptif;

   /** Code ou format du message. */
   private String codeOuFormatMessage;

   /** Classe des arguments associés. */
   private List<Class<?>> classeArguments;

   /**
    * Construire une règle de validation.
    * @param codeRegleValidation Code de la règle de validation.
    * @param descriptif Descriptif du message.
    * @param messageSource Ressource bundle du message.
    * @param classeArguments Classe des arguments associés.
    */
   public RegleDeValidation(String codeRegleValidation, MessageSource messageSource, String descriptif, Class<?>... classeArguments) {
      this.codeRegleValidation = codeRegleValidation;
      this.descriptif = descriptif;
      this.codeOuFormatMessage = messageSource.getMessage(codeRegleValidation, new Object[] {}, Locale.getDefault());
      this.classeArguments = List.of(classeArguments);
   }

   /**
    * Construire une règle de validation.
    * @param codeRegleValidation Code de la règle de validation.
    * @param descriptif Descriptif du message.
    * @param codeOuFormatMessage Code ou format du message.
    * @param classeArguments Classe des arguments associés.
    */
   public RegleDeValidation(String codeRegleValidation, String descriptif, String codeOuFormatMessage, Class<?>... classeArguments) {
      this.codeRegleValidation = codeRegleValidation;
      this.descriptif = descriptif;
      this.codeOuFormatMessage = codeOuFormatMessage;
      this.classeArguments = List.of(classeArguments);
   }

   /**
    * Renvoyer le descriptif de cette règle de validation.
    * @return Règle de validation.
    */
   public String getDescriptif() {
      return this.descriptif;
   }

   /**
    * Fixer le descriptif de cette règle de validation.
    * @param descriptif Règle de validation.
    */
   public void setDescriptif(String descriptif) {
      this.descriptif = descriptif;
   }

   /**
    * Renvoyer le code ou le format du message associé.
    * @return Code ou format du message.
    */
   public String getCodeOuFormatMessage() {
      return this.codeOuFormatMessage;
   }

   /**
    * Fixer le code ou le format du message associé.
    * @param codeOuFormatMessage Code ou format du message.
    */
   public void setCodeOuFormatMessage(String codeOuFormatMessage) {
      this.codeOuFormatMessage = codeOuFormatMessage;
   }

   /**
    * Renvoyer la classe de chacun des arguments du message.
    * @return Classe des arguments du message.
    */
   public List<Class<?>> getClasseArguments() {
      return this.classeArguments;
   }

   /**
    * Fixer la classe de chacun des arguments du message.
    * @param classeArguments Classe des arguments du message.
    */
   public void setClasseArguments(List<Class<?>> classeArguments) {
      this.classeArguments = classeArguments;
   }

   /**
    * Renvoyer le code de la règle de validation.
    * @return Code de la règle de validation.
    */
   public String getCodeRegleValidation() {
      return this.codeRegleValidation;
   }

   /**
    * Fixer le code de la règle de validation.
    * @param  codeRegleValidation Code de la règle de validation.
    */
   public void setCodeRegleValidation(String codeRegleValidation) {
      this.codeRegleValidation = codeRegleValidation;
   }

   /**
    * {@inheritDoc}
    */
   public String toString() {
      return MessageFormat.format("{0} : {1} ({2,number,#})", this.codeRegleValidation, this.descriptif, this.classeArguments.size());
   }
}
