package fr.ecoemploi.adapters.outbound.spark.dataset.core.validation;

import java.io.*;
import java.util.*;

/**
 * Liste de règles de validations.
 * @author Marc Le Bihan
 */
public class ReglesDeValidation extends LinkedHashMap<String, RegleDeValidation> implements Iterable<RegleDeValidation>, Serializable {
   @Serial
   private static final long serialVersionUID = 9178785404924283015L;

   /**
    * Ajouter une règle à la liste.
    * @param regleDeValidation Règle de validation.
    */
   public void addIfNew(RegleDeValidation regleDeValidation) {
      super.putIfAbsent(regleDeValidation.getCodeRegleValidation(), regleDeValidation);
   }

   /**
    * @see java.lang.Iterable#iterator()
    */
   @Override
   public Iterator<RegleDeValidation> iterator() {
      return values().iterator();
   }
}
