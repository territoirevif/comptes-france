package fr.ecoemploi.adapters.outbound.spark.dataset.core;

import org.apache.spark.sql.*;

/**
 * Interface marquant ue classe capable de convertir un Row en objet métier.
 * @param <OM> Classe de l'objet métier cible.
 */
public interface RowToObjetMetierInterface<OM> {
   /**
    * Convertir un Dataset Row en Dataset d'objets métiers.
    * @param rows Dataset Row à convertir.
    * @return Dataset d'objets métiers.
    */
   Dataset<OM> toDatasetObjetsMetiers(Dataset<Row> rows);

   /**
    * Convertir un Row associations en objet métier.
    * @param row Row à convertir.
    * @return objet métier.
    */
   OM toObjetMetier(Row row);
}
