package fr.ecoemploi.adapters.outbound.spark.dataset.core.validation;

import java.io.*;

/**
 * Déclaration d'un Elément invalide
 * @author  Marc LE BIHAN
 */
public class ElementInvalide implements Serializable {
   /** Serial UUID */
   @Serial
   private static final long serialVersionUID = -2444632977439501350L;

   /** Code de la règle de validation. */
   private final String codeRegleValidation;

   /** Les arguments qui accompagnent le message, et le nombre de fois qu'ils ont été rencontrés. */
   private final ClefElementInvalide arguments;

   /** Le nombre d'occurences. */
   private long occurrences;

   /**
    * Construire un élément invalide.
    * @param codeRegleValidation Règle de validation.
    * @param arguments arguments.
    */
   public ElementInvalide(String codeRegleValidation, Serializable... arguments) {
      this.codeRegleValidation = codeRegleValidation;
      this.arguments = new ClefElementInvalide(arguments);
      this.occurrences = 1L;
   }

   /**
    * Incrémenter l'occurrence de cet élément invalide.
    */
   public void incrementerOccurrence() {
      this.occurrences ++;
   }

   /**
    * Renvoyer le code de la règle de validation.
    * @return Code de la règle de validation.
    */
   public String getCodeRegleValidation() {
      return this.codeRegleValidation;
   }

   /**
    * Renvoyer les arguments du message d'erreur.
    * @return Arguments.
    */
   public ClefElementInvalide getArguments() {
      return this.arguments;
   }

   /**
    * Renvoyer le nombre d'occurrences de cette erreur avec ces arguments.
    * @return Nombre d'occurences.
    */
   public long getOccurrences() {
      return this.occurrences;
   }
}
