package fr.ecoemploi.adapters.outbound.spark.dataset.core.validation;

import java.io.*;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.function.*;

import org.jetbrains.annotations.NotNull;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import org.apache.spark.sql.*;

import fr.ecoemploi.domain.utils.autocontrole.ObjetMetierSpark;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.HistoriqueExecution;

/**
 * Classe de base des validateurs de Row de Dataset
 * @author Marc Le Bihan
 */
public class AbstractRowValidator implements Serializable, Iterable<RegleDeValidation> {
   /** Serial UID. */
   @Serial
   private static final long serialVersionUID = 9211467540734806778L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRowValidator.class);

   /** Règles de validation. */
   private final ReglesDeValidation reglesDeValidation = new ReglesDeValidation();

   /** Source de messages. */
   @Autowired
   protected transient MessageSource messageSource;

   /**
    * Déclarer une règle de validation que propose ce validateur.
    * @param codeRegleValidation Code de la règle de validation.
    * @param descriptif Descriptif du message.
    * @param messageSource Ressource bundle du message.
    * @param classeDesArguments Classe des arguments associés.
    */
   public void declareRegle(String codeRegleValidation, MessageSource messageSource, String descriptif, Class<?>... classeDesArguments) {
      RegleDeValidation regle = new RegleDeValidation(codeRegleValidation, messageSource, descriptif, classeDesArguments);
      this.reglesDeValidation.addIfNew(regle);
   }

   /**
    * Réaliser un test.
    * @param codeRegleValidation Code de la règle de validation à appliquer.
    * @param conditionEchec Test à réaliser : s'il vaut vrai, le champ n'est pas valide.
    * @param arguments Arguments (lazy) à extraire.
    * @param historique Historique d'exécution. Peut valoir null.
    * @return booléen rapportant le succès du test.
    * @throws NoSuchElementException si la règle de validation demandée n'existe pas.
    */
   protected boolean valideSi(String codeRegleValidation, HistoriqueExecution historique, Row row, Predicate<Row> conditionEchec, Supplier<Serializable[]> arguments) {
      if (!conditionEchec.test(row)) {
         invalider(codeRegleValidation, historique, arguments);
         return false;
      }

      return true;
   }

   /**
    * Réaliser un test.
    * @param codeRegleValidation Code de la règle de validation à appliquer.
    * @param conditionEchec Test à réaliser : s'il vaut vrai, le champ n'est pas valide.
    * @param arguments Arguments (lazy) à extraire.
    * @param historique Historique d'exécution. Peut valoir null.
    * @return booléen rapportant le succès du test.
    * @throws NoSuchElementException si la règle de validation demandée n'existe pas.
    */
   protected boolean invalideSi(String codeRegleValidation, HistoriqueExecution historique, Row row, Predicate<Row> conditionEchec, Supplier<Serializable[]> arguments) {
      if (conditionEchec.test(row)) {
         invalider(codeRegleValidation, historique, arguments);
         return false;
      }

      return true;
   }

   /**
    * Invalider un test en émettant son message d'anomalie et en incrémentant les compteurs requis.
    * @param codeRegleValidation Code de la règle de validation à appliquer.
    * @param arguments Arguments (lazy) à extraire.
    * @param historique Historique d'exécution. Peut valoir null.
    * @throws NoSuchElementException si la règle de validation demandée n'existe pas.
    */
   protected void invalider(String codeRegleValidation, HistoriqueExecution historique, Supplier<Serializable[]> arguments) {
      RegleDeValidation regleDeValidation = this.reglesDeValidation.get(codeRegleValidation);

      // Si la règle n'existe pas, partir en exception.
      if (regleDeValidation == null) {
         String format = "Il n''y a pas de règle de validation de code {0} dans le validateur {1}.";
         String message = MessageFormat.format(format, codeRegleValidation, getClass().getSimpleName());

         LOGGER.error(message);
         throw new NoSuchElementException(message);
      }

      // Dans certaines situations, le test aura émis lui-même le message d'avertissement.
      if (regleDeValidation.getCodeOuFormatMessage() != null) {
         Object[] args = arguments.get();

         String format = regleDeValidation.getCodeOuFormatMessage();
         String message = MessageFormat.format(format, args);

         LOGGER.warn(message);
      }

      if (historique != null) {
         historique.incrementerOccurrences(regleDeValidation.getCodeRegleValidation(), arguments.get());
      }
   }

   /**
    * Renvoyer la liste des règles de validation utilisées par ce validateur.
    * @return Règles de validation.
    */
   public ReglesDeValidation getReglesDeValidation() {
      return this.reglesDeValidation;
   }

   /**
    * Déterminer si une date est invalide.
    * @param date Date à tester.
    * @return Message expliquant son invalidité, ou null si elle est valide.
    */
   protected String dateInvalide(String date) {
      try {
         ObjetMetierSpark.fromINSEEtoLocalDate(date);
         return null;
      }
      catch(@SuppressWarnings("unused") DateTimeParseException ex) {
         return ex.getMessage();
      }
   }

   /**
    * Déterminer si une date heure est invalide.
    * @param dateTime Date heure à tester.
    * @return Message expliquant son invalidité, ou null si elle est valide.
    */
   protected String dateTimeInvalide(String dateTime) {
      try {
         ObjetMetierSpark.fromINSEEtoLocalDateTime(dateTime);
         return null;
      }
      catch(@SuppressWarnings("unused") DateTimeParseException ex) {
         return ex.getMessage();
      }
   }

   /**
    * Renvoyer une valeur de champ d'un row sous forme de chaîne de caractères.
    * @param indexs Liste des indexs connus.
    * @param row Row pour acquisition des connaissance.
    * @param nomChamp Nom du champ.
    * @return Valeur du champ.
    */
   protected String getString(Map<String, Integer> indexs, Row row, String nomChamp) {
      return row.getString(rowIndex(indexs, row, nomChamp));
   }

   /**
    * Renvoyer une valeur de champ d'un row sous forme d'un booléen.
    * @param indexs Liste des indexs connus.
    * @param row Row pour acquisition des connaissance.
    * @param nomChamp Nom du champ.
    * @return Valeur du champ.
    */
   protected Boolean getBoolean(Map<String, Integer> indexs, Row row, String nomChamp) {
      return row.getBoolean(rowIndex(indexs, row, nomChamp));
   }

   /**
    * Renvoyer une valeur de champ d'un row sous forme d'une LocalDate.
    * @param indexs Liste des indexs connus.
    * @param row Row pour acquisition des connaissance.
    * @param nomChamp Nom du champ.
    * @return Valeur du champ.
    */
   protected LocalDate getLocalDate(Map<String, Integer> indexs, Row row, String nomChamp) {
      return row.getLocalDate(rowIndex(indexs, row, nomChamp));
   }

   /**
    * Renvoyer l'index d'un champ dans un row.
    * @param indexs Liste des indexs connus.
    * @param row Row pour acquisition des connaissance.
    * @param nomChamp Nom du champ.
    * @return Index du champ dans le row.
    */
   private int rowIndex(Map<String, Integer> indexs, Row row, String nomChamp) {
      return indexs.computeIfAbsent(nomChamp, row::fieldIndex);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public @NotNull Iterator<RegleDeValidation> iterator() {
      return this.reglesDeValidation.values().iterator();
   }

   /**
    * {@inheritDoc}
    */
   public String toString() {
      StringBuilder sortie = new StringBuilder();

      for(RegleDeValidation regleDeValidation : this) {
         if (sortie.isEmpty() == false) {
            sortie.append("\n");
         }

         sortie.append(regleDeValidation.toString());
      }

      return sortie.toString();
   }
}
