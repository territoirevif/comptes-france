package fr.ecoemploi.adapters.outbound.spark.dataset.core;

import java.util.List;
import java.util.stream.Stream;

import org.apache.spark.sql.*;

/**
 * Marque un Dataset qui est capable de donner des objets métiers.
 * @param <OM> Objet métier.
 */
public interface DatasetRowToObjetMetierInterface<OM> extends RowToObjetMetierInterface<OM> {
   /**
    * Convertir un Dataset Row d'associations en liste d'objets métiers.
    * @param rows Dataset Row à convertir.
    * @return Liste d'objets métiers.
    */
   List<OM> toObjetsMetiersList(Dataset<Row> rows);

   /**
    * Convertir un dataset Row en Stream d'objets métiers.
    * @param rows Dataset Row à convertir.
    * @return Flux d'objets métiers.
    */
   Stream<OM> toObjetsMetiersStream(Dataset<Row> rows);
}
