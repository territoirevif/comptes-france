package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.io.*;
import java.nio.file.*;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import org.apache.spark.sql.*;

import fr.ecoemploi.domain.utils.objets.*;

/**
 * Exportation d'un CSV depuis un dataset
 */
@Component
public class ExportCSV implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -6771804231732682502L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ExportCSV.class);

   /** Répertoire temporaire */
   @Value("${temp.dir}")
   protected String tempDir;

   /**
    * Exporter en CSV la sortie d'un Dataset.
    * @param ds Dataset à exporter.
    * @param path Chemin optionnel, si <code>null</code>, sera alimenté avec le chemin du répertoire temporaire
    * @param nom Nom du fichier à générer.
    * @param nombreAleatoireSuffixe true si un nombre aléatoire doit être suffixé pour distinguer les fichiers de même préfixe.
    * @return Fichier CSV.
    * @throws TechniqueException si un incident survient.
    */
   public File exporterCSV(Dataset<Row> ds, String path, String nom, boolean nombreAleatoireSuffixe) {
      String nomFichierGenere = (nom == null) ? "csv_" : nom;
      String suffixe = nombreAleatoireSuffixe ? MessageFormat.format("{0,number,#0}", System.currentTimeMillis()) : "";
      String cheminAccesCompletCSV = ((path != null) ? path : this.tempDir) + File.separatorChar + nomFichierGenere + suffixe;

      // Si le suffixe du nom ne contient pas .csv, le placer. Sinon, il y a risque d'écrasement du dataset parquet
      if (nom != null && cheminAccesCompletCSV.toLowerCase().endsWith(".csv") == false) {
         cheminAccesCompletCSV = cheminAccesCompletCSV + ".csv";
      }

      File sortieCSV = new File(cheminAccesCompletCSV);

      ds.coalesce(1).write().mode(SaveMode.Overwrite)
         .option("header", "true")                                // Mettre une entête au CSV.
         .option("quoteMode", "NON_NUMERIC")                      // Encadrer de " les valeurs non numériques. (Ne fonctionne pas avec Spark https://stackoverflow.com/questions/54730409/write-a-csv-file-in-quotemode-non-numeric-to-have-only-strings-and-non-numeric)
         .option("quote", "\"").csv(sortieCSV.getAbsolutePath()); // La quote étant "

      // Dresser la liste des fichiers d'extension .csv produits.
      try(Stream<Path> walker = Files.walk(sortieCSV.toPath())) {
         List<File> fichiersCSV = walker.map(Path::toFile)                                   // Rechercher dans le répertoire de sortie les Path convertis en File,
            .filter(c -> c.isDirectory() == false && c.getName().endsWith(".csv")).toList(); // qui sont des fichiers CSV et les renvoyer en liste.

         // S'il y en a un nombre différent d'un, nous sommes face à une anomalie.
         if (fichiersCSV.size() != 1) {
            String format = "Un fichier CSV aurait dû être produit dans le répertoire temporaire {0}, mais {1} s''y trouve(nt).";
            String message = MessageFormat.format(format, sortieCSV.getAbsolutePath(), fichiersCSV.size());

            PersistenceException ex = new PersistenceException(message);
            LOGGER.error(ex.getMessage());
            throw ex;
         }

         File csv = fichiersCSV.get(0);
         File renommage = new File(csv.getParentFile(), nomFichierGenere);

         boolean renomme = csv.renameTo(renommage);
         LOGGER.info("Le dataset a été exporté dans le fichier CSV {}.", renomme ? renommage.getAbsolutePath() : sortieCSV.getAbsolutePath());

         return fichiersCSV.get(0);
      }
      catch(IOException e) {
         String format = "Incident lors de la création du fichier CSV dans le répertoire tempoaire {0} : {1}.";
         String message = MessageFormat.format(format, sortieCSV.getAbsolutePath(), e.getMessage());

         PersistenceException ex = new PersistenceException(message, e);
         LOGGER.error(ex.getMessage(), e);
         throw ex;
      }
   }

   /**
    * Exporter en CSV la sortie d'un Dataset.
    * @param ds Dataset à exporter.
    * @param nom Nom du fichier à générer.
    * @param nombreAleatoireSuffixe true si un nombre aléatoire doit être suffixé pour distinguer les fichiers de même préfixe.
    * @return Fichier CSV.
    * @throws TechniqueException si un incident survient.
    */
   public File exporterCSV(Dataset<Row> ds, String nom, boolean nombreAleatoireSuffixe) {
      return exporterCSV(ds, null, nom, nombreAleatoireSuffixe);
   }

   /**
    * Exporter en CSV la sortie d'un Dataset.
    * @param ds Dataset à exporter.
    * @return Fichier CSV.
    * @throws TechniqueException si un incident survient.
    */
   public File exporterCSV(Dataset<Row> ds) {
      return exporterCSV(ds, null, null, true);
   }
}
