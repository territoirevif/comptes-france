package fr.ecoemploi.adapters.outbound.spark.dataset.core.validation;

import java.io.*;
import java.util.*;

/**
 * Liste d'éléments qui ont été invalidés par les règles de validations.
 * @author Marc Le Bihan
 */
public class ElementsInvalides extends LinkedHashMap<Integer, ElementInvalide> implements Iterable<ElementInvalide>, Serializable {
   @Serial
   private static final long serialVersionUID = -7750476355832694897L;

   /**
    * Construire une liste d'éléments invalide vide.
    */
   public ElementsInvalides() {
   }

   /**
    * Construire une liste d'éléments invalides.
    * @param elementInvalides Liste d'éléments invalides.
    */
   public ElementsInvalides(List<ElementInvalide> elementInvalides) {
      elementInvalides.forEach(this::add);
   }

   /**
    * Ajouter un élément qui a été invalidé.
    * @param elementInvalide Elément invalide.
    */
   public void add(ElementInvalide elementInvalide) {
      super.put(elementInvalide.getArguments().hashCode(), elementInvalide);
   }

   /**
    * @see Iterable#iterator()
    */
   @Override
   public Iterator<ElementInvalide> iterator() {
      return values().iterator();
   }
}
