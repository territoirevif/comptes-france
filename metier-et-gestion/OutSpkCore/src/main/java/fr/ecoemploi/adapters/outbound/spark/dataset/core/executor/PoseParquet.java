package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.io.*;

/**
 * Une instance de fichier parquet trié.
 * @param <Tri> Enumération des tris disponibles. 
 * 
 * @author Marc Le Bihan
 */
public class PoseParquet<Tri extends TriParqueteurIF> implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -2332672368481740964L;

   /** Tri correspondant, dans l'énumération. */
   private Tri tri;
   
   /** Nom de ce tri (apparaît dans le nom du fichier). */
   private String nomDuTri;

   /** Tri effectif, par colonnes. */
   private String[] sort;
   
   /** Indique si le tri doit être demandé au sein de la partition. */
   private boolean sortWithPartitions;
   
   /** Colonne de partitionnement, si applicable. */
   private String colonnePartitionnement;

   /**
    * Décrire une tri possible de fichier parquet.
    * @param tri Tri représenté.
    * @param nomDuTri Nom du tri : apparaît dans le nom du fichier.
    */
   public PoseParquet(Tri tri, String nomDuTri) {
      this.tri = tri;
      this.nomDuTri = nomDuTri;
   }

   /**
    * Renvoyer le tri représenté.
    * @return Tri.
    */
   public Tri getTri() {
      return this.tri;
   }

   /**
    * Fixer le tri représenté.
    * @param tri tri.
    */
   public void setTri(Tri tri) {
      this.tri = tri;
   }

   /**
    * Renvoyer le nom de ce tri (apparaît dans le nom du fichier).
    * @return Nom du tri.
    */
   public String getNomDuTri() {
      return this.nomDuTri;
   }

   /**
    * Fixer le nom de ce tri (apparaît dans le nom du fichier).
    * @param nomDuTri Nom du tri. 
    */
   public void setNomDuTri(String nomDuTri) {
      this.nomDuTri = nomDuTri;
   }

   /**
    * Renvoyer les colonnes de tri.
    * @return Colonnes de tri.
    */
   public String[] getSort() {
      return this.sort;
   }

   /**
    * Fixer les colonnes de tri.
    * @param sort Colonnes de tri. 
    */
   public void setSort(String[] sort) {
      this.sort = sort;
   }

   /**
    * Déterminer si ce tri doit se faire au sein des partitions.
    * @return true si c'est le cas, false s'il est global.
    */
   public boolean isSortWithPartitions() {
      return this.sortWithPartitions;
   }

   /**
    * indiquer si ce tri doit se faire au sein des partitions.
    * @param sortWithPartitions true si c'est le cas, false s'il est global. 
    */
   public void setSortWithPartitions(boolean sortWithPartitions) {
      this.sortWithPartitions = sortWithPartitions;
   }

   /**
    * Renvoyer la colonne de partitionnement, si applicable.
    * @return Colonne de partitionnement, ou null s'il n'y a pas de partitionnement.
    */
   public String getColonnePartitionnement() {
      return this.colonnePartitionnement;
   }

   /**
    * Fixer la colonne de partitionnement, si applicable.
    * @param colonnePartitionnement Colonne de partitionnement, ou null s'il n'y a pas de partitionnement.
    */
   public void setColonnePartitionnement(String colonnePartitionnement) {
      this.colonnePartitionnement = colonnePartitionnement;
   }
}
