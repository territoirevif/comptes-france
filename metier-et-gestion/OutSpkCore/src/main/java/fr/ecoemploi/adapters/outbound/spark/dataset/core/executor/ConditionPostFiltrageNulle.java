package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.util.function.Supplier;

import org.apache.spark.sql.*;

/**
 * Aucune condition de post-filtrage : renvoie une condition nulle
 * @author Marc Le Bihan
 */
public class ConditionPostFiltrageNulle implements ConditionPostFiltrage {
   @Override
   public Column apply(OptionsCreationLecture options, Supplier<Dataset<Row>> worker) {
      return null;
   }
}
