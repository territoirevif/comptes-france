package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;

import org.slf4j.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import org.apache.spark.sql.SparkSession;
import org.apache.spark.util.CollectionAccumulator;

/**
 * Emetteur de messages ou de variables broadcast
 */
@Component
public class Emetteur implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 221165993935676202L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(Emetteur.class);

   /** Avertir des incidents de sélection. */
   @Value("${avertir.incidents.spark:false}")
   private boolean avertirIncidents = false;

   /**
    * Convertir un accumulateur Collection<T> en Set<T>.
    * @param <T> Classe des objets portés par l'accumulateur.
    * @param accumulator Accumulateur de type collection.
    * @return Ensemble trié.
    */
   public <T> Set<T> toSetAccumulator(CollectionAccumulator<T> accumulator) {
      return new LinkedHashSet<>(accumulator.value());
   }

   /**
    * Convertir un accumulateur Collection<String> en Set<String>.
    * @param <T> Classe des objets portés par l'accumulateur.
    * @param classeObjetAccumulateur Classe des objets portés par l'accumulateur.
    * @param session Session Spark dont le contexte permettra de retrouver l'accumulateur.
    * @param nom Nom de l'accumulateur de type collection.
    * @return Ensemble trié.
    */
   public <T> Set<T> toSetAccumulator(SparkSession session, @SuppressWarnings("unused") Class<T> classeObjetAccumulateur, String nom) {
      CollectionAccumulator<T> accumulator = session.sparkContext().collectionAccumulator(nom);
      return toSetAccumulator(accumulator);
   }

   /**
    * Convertir un accumulateur Collection<String> en Set<String>.
    * @param session Spark où, dans son contexte, retrouver l'accumulateur.
    * @param nom Nom de l'accumulateur de type collection.
    * @return Ensemble trié.
    */
   public Set<String> toSetAccumulator(SparkSession session, String nom) {
      CollectionAccumulator<String> accumulator = session.sparkContext().collectionAccumulator(nom);
      return toSetAccumulator(accumulator);
   }

   /**
    * Emet un message de log information et fixe le nom du job Spark (action).
    * @param session Session Spark.
    * @param format Format de MessageFormat.
    * @param args Arguments.
    */
   public void jobDescription(SparkSession session, String format, Object... args) {
      String message = MessageFormat.format(format, args);
      LOGGER.info(message);

      session.sparkContext().setJobDescription(message);
   }

   /**
    * Indiquer s'il faut avertir des incidents d'analyse.
    * @return true s'il faut en informer par log<br>
    * false s'il faut utiliser les requêtes de sélection les plus rapides.
    */
   public boolean isAvertirIncidents() {
      return this.avertirIncidents;
   }

   /**
    * Déterminer s'il faut avertir des incidents d'analyse.
    * @param avertirIncidents true s'il faut en informer par log<br>
    * false s'il faut utiliser les requêtes de sélection les plus rapides.
    */
   public void setAvertirIncidents(boolean avertirIncidents) {
      this.avertirIncidents = avertirIncidents;
   }
}
