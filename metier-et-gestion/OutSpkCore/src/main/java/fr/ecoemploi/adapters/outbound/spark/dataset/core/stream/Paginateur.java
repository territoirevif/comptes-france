package fr.ecoemploi.adapters.outbound.spark.dataset.core.stream;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.sql.*;

/**
 * Paginateur de dataset
 */
@Component
public class Paginateur implements Serializable {
   /** Serial UUID */
   @Serial
   private static final long serialVersionUID = 1039491913798574994L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(Paginateur.class);

   /** Nombre d'objets métiers lus par page, pour un stream */
   @Value("${nombreObjetsParPageStream:100000}")
   private int nombreObjetsParPageStream;

   /**
    * Paginer en stream en permettant la conversion d'un Row en objet métier.
    * @param dataset Dataset à diviser.
    * @param encoder Fonction d'encodage.
    * @param nombreObjetsParPage Nombre d'objets par page.
    * @return Flux d'objets métiers.
    * @param <T> Classe des objets métiers renvoyés par le stream.
    */
   public static <T> Stream<T> paginerEnStream(Dataset<?> dataset, Function<Dataset<Row>, Dataset<T>> encoder, int nombreObjetsParPage) {
      Objects.requireNonNull(dataset, "Le dataset à diviser pour sa pagination en stream d'objets ne peut pas valoir null.");
      Objects.requireNonNull(encoder, "La fonction d'encodage, du type Dataset<Row> vers le type d'objet métier désiré pour la pagination en stream d'objets d'un dataset, ne peut pas valoir null.");

      DatasetsItemIterator<T> objetsMetiersIterator = new DatasetsItemIterator<>(paginer(dataset, encoder, nombreObjetsParPage));
      return StreamSupport.stream(Spliterators.spliteratorUnknownSize(objetsMetiersIterator, Spliterator.ORDERED), false);
   }

   /**
    * Paginer en stream un dataset tel qu'il est.
    * @param dataset Dataset à diviser.
    * @param nombreObjetsParPage Nombre d'objets par page.
    * @return Flux d'objets métiers.
    * @param <T> Classe des objets métiers renvoyés par le stream.
    */
   public static <T> Stream<T> paginerEnStream(Dataset<T> dataset, int nombreObjetsParPage) {
      Objects.requireNonNull(dataset, "Le dataset à diviser pour sa pagination en stream d'objets ne peut pas valoir null.");

      DatasetsItemIterator<T> objetsMetiersIterator = new DatasetsItemIterator<>(paginer(dataset, nombreObjetsParPage));
      return StreamSupport.stream(Spliterators.spliteratorUnknownSize(objetsMetiersIterator, Spliterator.ORDERED), false);
   }

   /**
    * Paginer un dataset en renvoyant plusieurs datasets d'un nombre maximal d'objets métiers.
    * @param dataset Dataset d'enregistrements à diviser.
    * @param encoder Fonction d'encodage en dataset d'objets métiers d'un Dataset<Row>.
    * @param nombreObjetsParPage Nombre d'objets maximal par dataset créé.
    * @param <T> Type de l'objet métier cible que l'on veut obtenir.
    * @return Liste des datasets d'objet métier créés.
    */
   public static <T> List<Dataset<T>> paginer(Dataset<?> dataset, Function<Dataset<Row>, Dataset<T>> encoder, int nombreObjetsParPage) {
      Objects.requireNonNull(dataset, "Le dataset à diviser pour sa pagination en objets ne peut pas valoir null.");
      Objects.requireNonNull(encoder, "La fonction d'encodage, du type Dataset<Row> vers le type d'objet métier désiré pour la pagination en objets d'un dataset, ne peut pas valoir null.");

      List<Dataset<Row>> blocsEnregistrements = paginer(dataset.toDF(), nombreObjetsParPage);
      return toObjetMetierDataset(blocsEnregistrements, encoder);
   }

   /**
    * Convertir un dataset Row en dataset d'objets métiers.
    * @param blocsRows Bloc de datasets Rows.
    * @param encoder Encodeur.
    * @return Liste de dataset d'objets métiers.
    * @param <T> Classe de l'objet métier.
    */
   private static <T> List<Dataset<T>> toObjetMetierDataset(List<Dataset<Row>> blocsRows, Function<Dataset<Row>, Dataset<T>> encoder) {
      List<Dataset<T>> blocsDatasetsObjets = new ArrayList<>();

      for(Dataset<Row> rowDataset : blocsRows) {
         Dataset<T> datasetObjets = encoder.apply(rowDataset);
         blocsDatasetsObjets.add(datasetObjets);
      }

      return blocsDatasetsObjets;
   }

   /**
    * Paginer un dataset en renvoyant plusieurs datasets d'un nombre maximal d'enregistrements.
    * @param dataset Dataset à diviser.
    * @param nombreObjetsParPage Nombre d'objets maximal par dataset créé.
    * @return Liste des datasets créés.
    */
   public static <T> List<Dataset<T>> paginer(Dataset<T> dataset, int nombreObjetsParPage) {
      Objects.requireNonNull(dataset, "Le dataset à diviser pour sa pagination ne peut pas valoir null.");

      List<Dataset<T>> sousDatasets = new ArrayList<>();

      // Si le nombre d'enregistrements demandés par page est supérieur ou égal au nombre qu'en a le dataset reçu en paramètre,
      // le renvoyer comme unique élément de la liste.
      long count = dataset.count();
      LOGGER.info("Le dataset à paginer a {} enregistrements : il sera découpé en pages de {} enregistrements", count, nombreObjetsParPage);

      if (nombreObjetsParPage >= count) {
         sousDatasets.add(dataset);
         LOGGER.info("Le dataset sera converti en une seule étape.");
         return sousDatasets;
      }

      diviserDataset(sousDatasets, dataset, nombreObjetsParPage, count);
      return sousDatasets;
   }

   /**
    * Diviser un dataset (depuis Spark 3.4.x)
    * @param sousDatasets Liste de datasets à compléter avec des sous datasets
    * @param dataset Dataset à diviser
    * @param nombreObjetsParPage nombre d'objets par page
    * @param count Nombre total d'enregistrements du dataset à diviser
    */
   private static <T> void diviserDataset(List<Dataset<T>> sousDatasets, Dataset<T> dataset, int nombreObjetsParPage, long count) {
      int start = 0;

      do {
         sousDatasets.add(dataset.offset(start).limit(nombreObjetsParPage));
         start += nombreObjetsParPage;
      }
      while(start < count);
   }

   /**
    * Ancienne méthode de division de dataset (avant Spark 3.4.x)
    * @param sousDatasets Liste de datasets à compléter avec des sous datasets
    * @param dataset Dataset à diviser
    * @param nombreObjetsParPage nombre d'objets par page
    * @param count Nombre total d'enregistrements du dataset à diviser
    */
   private static void diviserDatasetOld(List<Dataset<Row>> sousDatasets, Dataset<Row> dataset, int nombreObjetsParPage, long count) {
      int start = 0;
      int end = nombreObjetsParPage;

      JavaPairRDD<Row, Long> rdd = dataset.toJavaRDD().zipWithIndex();

      // Alimenter une liste de datasets, chacun prenant à son tour les indexs dans [start, end[
      do {
         final long a = start;
         final long b = end;

         JavaPairRDD<Row, Long> rddSousEnsemble = rdd.filter(f -> f._2() >= a && f._2() < b);
         LOGGER.info("Le RDD divisé a {} éléments [{}, {}]", rddSousEnsemble.count(), a, b);

         Dataset<Row> sousDataset = dataset.sparkSession().createDataFrame(rddSousEnsemble.keys(), dataset.schema());
         LOGGER.info("Le sous dataset divisé a {} éléments", sousDataset.count());
         sousDatasets.add(sousDataset);

         start += nombreObjetsParPage;
         end += nombreObjetsParPage;
      }
      while(start < count);
   }

   /**
    * Renvoyer le nombre d'objets par page lus quand le Dataset est accédé en Stream
    * @return Nombre d'objets lus par page.
    */
   public int getNombreObjetsParPageStream() {
      return this.nombreObjetsParPageStream;
   }

   /**
    * Fixer le nombre d'objets par page lus quand le Dataset est accédé en Stream
    * @param nombreObjetsParPageStream Nombre d'objets lus par page.
    */
   public void setNombreObjetsParPageStream(int nombreObjetsParPageStream) {
      this.nombreObjetsParPageStream = nombreObjetsParPageStream;
   }
}
