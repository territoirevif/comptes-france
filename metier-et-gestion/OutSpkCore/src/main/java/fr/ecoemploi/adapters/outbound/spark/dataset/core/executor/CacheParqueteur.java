package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.io.*;
import org.slf4j.*;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.*;

/**
 * Assistant pour la lecture et sauvegarde d'un cache de dataset en parquet.
 * @param <TriSpecifique> Tri spécifique demandé pour ce cache.
 */
public class CacheParqueteur<TriSpecifique extends TriParqueteurIF> implements Function<Boolean, Dataset<Row>>, Serializable {
   @Serial
   private static final long serialVersionUID = 7980725351567396516L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CacheParqueteur.class);

   /** Parqueteur. */
   private final Parqueteur<TriSpecifique> parqueteur;

   /** Tri spécifique demandé pour cette mise en cache. */
   private final transient TriSpecifique tri;

   /** Argument du fichier store lu, écrit. */
   private final transient Object[] args;

   /** Session Spark. */
   private final SparkSession session;

   /**
    * Créer un cache de fichier parquet.
    * @param optionsCreationLecture Options de création/lecture du dataset.
    * @param session Session Spark.
    * @param exportCSV Exportateur de fichier CSV.
    * @param nomStore Nom du store.
    * @param formatDeclinaison Format de la déclinaison de ce store (utilisé avec les arguments args).
    * @param tri Tri (et partitionnement) du store.
    * @param args Arguments de la sous-sélection de cette constitution de store.
    */
   public CacheParqueteur(OptionsCreationLecture optionsCreationLecture, SparkSession session, ExportCSV exportCSV, String nomStore, String formatDeclinaison, TriSpecifique tri, Object... args) {
      this.session = session;
      this.parqueteur = new Parqueteur<>(optionsCreationLecture, exportCSV, nomStore, formatDeclinaison, tri);
      this.tri = tri;
      this.args = args;
   }

   /**
    * Charger un store en forçant ou non l'utilisation du cache.
    * @param useCache true s'il faut utiliser le cache.
    * @return Dataset lu.
    */
   @Override
   public Dataset<Row> call(Boolean useCache) {
      this.parqueteur.optionsCreationLecture().useCache(useCache);
      return this.parqueteur.loadFromStore(this.session, this.tri, this.args);
   }

   /**
    * Appliquer les tris et partionnements demandés.
    * @param ds Dataset.
    * @param triDejaFait true si le tri est déjà marqué comme réalisé par l'appelant, et qu'il ne faut pas le refaire.
    * @return Dataset trié et partitionné.
    */
   public Dataset<Row> appliquer(Dataset<Row> ds, boolean triDejaFait) {
      return this.parqueteur.appliquer(this.tri, ds, triDejaFait);
   }

   /**
    * Renvoyer le chemin d'accès du store dans lequel s'apprête à écrire/lire (ou a écrit/lu) le parqueteur
    * @param pourLecture true si le store à pour destination la lecture, auquel cas s'il y a une restriction, elle ne s'appliquera que si l'option <code>restriction.en.ecriture.seulement</code> est active.
    * @return Chemin d'accès du store
    */
   public String getStore(boolean pourLecture) {
      return this.parqueteur.getStore(pourLecture, this.tri, this.args);
   }

   /**
    * Sauvegarder le dataset dans le store parquet (si les options l'autorisent).
    * @param ds Dataset à sauvegarder.
    * @return dataset sauvegardé, éventuellement relu ensuite depuis le store si les options l'on demandé.
    */
   public Dataset<Row> save(Dataset<Row> ds) {
      boolean sauvegardeAuCoursDeCetteSession = this.parqueteur.saveToStore(this.tri, ds, this.args);

      if (sauvegardeAuCoursDeCetteSession) {
         LOGGER.info("Le dataset au PARTITIONNEMENT {} TRI {} ARGUMENTS {} est constitué et socké.", this.tri.getNomColonnePartionnement(), this.tri.getNomsColonnesTri(), this.args);
         ds.printSchema();
      }

      // Honorer la demande de renvoi du contenu du store si :
      //   - C'est le cache global qui est renvoyé, sans demande de restriction
      //   - Il y a une demande de restriction (aux communes) mais l'on vient de constituer ce cache, justement.
      boolean renvoyerContenuDuStore = this.parqueteur.optionsCreationLecture().toujoursRenvoyerContenuDuStore()
         && (this.parqueteur.optionsCreationLecture().hasRestrictionAuxCommunes() == false || sauvegardeAuCoursDeCetteSession);

      if (renvoyerContenuDuStore) {
         this.parqueteur.optionsCreationLecture().useCache(true);
         Dataset<Row> dsStore = this.parqueteur.loadFromStore(this.session, false, this.tri, this.args);

         if (dsStore == null) {
            LOGGER.warn("Il a été demandé de relire depuis le store le contenu du dataset de PARTITIONNEMENT {} TRI {} ARGUMENTS {}, après sa sauvegarde. Mais cela n'est pas possible : il n'existe pas sauvegardé. Il est renvoyé tel qu'en mémoire.", this.tri.getNomColonnePartionnement(), this.tri.getNomsColonnesTri(), this.args);
            return ds;
         }

         return dsStore;
      }

      return ds;
   }
}
