package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.io.*;
import java.text.*;
import java.util.*;

import org.apache.spark.sql.*;
import org.slf4j.*;

/**
 * Gestionnaire de fichiers parquet. 
 * @param <Tri> Enumération des tris disponibles.
 *  
 * @author Marc Le Bihan
 */
public class Parqueteur<Tri extends TriParqueteurIF> implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 4283496183816826946L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(Parqueteur.class);

   /** Options de création et de lecture d'un dataset, quand une de ses méthodes est appelée. */
   private final OptionsCreationLecture optionsCreationLecture;

   /** Exportateur de fichier CSV */
   private final ExportCSV exportCSV;

   /** Nom du fichier store de base. */
   private final String nomStore;
   
   /** Format des déclinaisons du fichier. */
   private final String formatDeclinaisons;

   /** Liste des fichiers parquet possibles. */
   private final Map<Tri, PoseParquet<Tri>> poses;

   /**
    * Construire un parqueteur sans tris définis.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param exportCSV Exportateur de fichier CSV
    * @param nomStore Racine du nom du fichier store.
    * @param formatDeclinaisons Format des déclinaisons possibles de cette racine.
    */
   public Parqueteur(OptionsCreationLecture optionsCreationLecture, ExportCSV exportCSV, String nomStore, String formatDeclinaisons) {
      this.optionsCreationLecture = optionsCreationLecture;
      this.exportCSV = exportCSV;
      this.nomStore = nomStore;
      this.formatDeclinaisons = formatDeclinaisons;
      this.poses = new HashMap<>();
   }

   /**
    * Construire un parqueteur contenant tous les tris proposés par une énumération.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param exportCSV Exportateur de fichier CSV
    * @param nomStore Racine du nom du fichier store.
    * @param formatDeclinaisons Format des déclinaisons possibles de cette racine.
    * @param valeursEnumerationTri Toutes les valeurs des énumérations du tri.
    */
   @SafeVarargs
   public Parqueteur(OptionsCreationLecture optionsCreationLecture, ExportCSV exportCSV, String nomStore, String formatDeclinaisons, Tri... valeursEnumerationTri) {
      this(optionsCreationLecture, exportCSV, nomStore, formatDeclinaisons);
      declareAll(valeursEnumerationTri);
   }

   /**
    * Renvoyer les options de lecture / écriture de dataset que le parqueteur utilise en ce moment.
    * @return Options de lecture / écriture de dataset.
    */
   public OptionsCreationLecture optionsCreationLecture() {
      return this.optionsCreationLecture;
   }

   /**
    * Charger un fichier depuis parquet, s'il est disponible.
    * @param session Session Spark.
    * @param tri Tri désiré.
    * @param args Arguments variables.
    * @return Dataset non null si le fichier était présent, null sinon.
    */
   public Dataset<Row> loadFromStore(SparkSession session, Tri tri, Object... args) {
      String store = getStore(true, this.formatDeclinaisons, args, tri);
      return Loader.loadFromStoreParquet(this.optionsCreationLecture, session, store);
   }

   /**
    * Charger un fichier depuis parquet, s'il est disponible.
    * @param session Session Spark.
    * @param pourLecture true si le store à pour destination la lecture, auquel cas s'il y a une restriction, elle ne s'appliquera que si l'option <code>restriction.en.ecriture.seulement</code> est active.
    * @param tri Tri désiré.
    * @param args Arguments variables.
    * @return Dataset non null si le fichier était présent, null sinon.
    */
   Dataset<Row> loadFromStore(SparkSession session, boolean pourLecture, Tri tri, Object... args) {
      String store = getStore(pourLecture, this.formatDeclinaisons, args, tri);
      return Loader.loadFromStoreParquet(this.optionsCreationLecture, session, store);
   }

   /**
    * Sauvergarder les données dans un cache, si le paramétrage l'autorise.
    * @param tri Tri désiré.
    * @param ds Dataset dont le contenu doit être écrit dans parquet.
    * @param args arguments pour construire la destination finale.
    * @return true, si le cache a été modifié<br>
    * false si le paramétrage l'interdisait : (propriété : 'use.cache'). 
    */
   public boolean saveToStore(Tri tri, Dataset<Row> ds, Object... args) {
      PoseParquet<Tri> pose = null;
      
      if (tri != null) {
         pose = this.poses.get(tri);
         
         if (pose == null) {
            LOGGER.warn("Un tri {} est désiré pour la constitution du fichier parquet {}, mais sa description n'est pas connue. (observation 2)", tri, this.nomStore);
         }
      }
      
      String store = getStore(false, this.formatDeclinaisons, args, tri);

      if (pose == null) {
         return Loader.saveSerialisationToStore(this.optionsCreationLecture, ds, store);
      }

      String[] colonnesPartitionnement = pose.getColonnePartitionnement() != null ? new String[] {pose.getColonnePartitionnement()} : null;

      boolean stocke = Loader.saveDatasetToStore(this.optionsCreationLecture, ds, colonnesPartitionnement, store);

      // Si le dataset a bien été stocké, produire son CSV attenant si les options de constitution l'autorisent
      if (stocke) {
         if (optionsCreationLecture().genererCSV() == false) {
            LOGGER.info("Les options de constitution du dataset demandent qu'il n'y ait pas de fichier CSV attenant produit.");
         } else {
            String csvFileName = new File(store).getName() + ".csv";

            // Le répertoire d'écrire est le répertoire parent du store final
            String csvPath = new File(store).getParentFile().getAbsolutePath();
            this.exportCSV.exporterCSV(ds, csvPath, csvFileName, false);
         }
      }

      return stocke;
   }

   /**
    * Renvoyer le chemin d'accès du store dans lequel s'apprête ou a écrit le parqueteur
    * @param pourLecture true si le store à pour destination la lecture, auquel cas s'il y a une restriction, elle ne s'appliquera que si l'option <code>restriction.en.ecriture.seulement</code> est active.
    * @param tri Tri désiré.
    * @param args arguments pour construire la destination finale.
    * @return Chemin d'accès du store
    */
   public String getStore(boolean pourLecture, Tri tri, Object... args) {
      return getStore(pourLecture, this.formatDeclinaisons, args, tri);
   }

   /**
    * Appliquer le tri, à l'approche du moment où le tri (et le partitionnement) deviennent possibles.
    * @param <T> Type du Dataset.
    * @param tri Tri demandé par l'appelant.
    * @param candidat Dataset candidat au partitionnement et au tri.
    * @param orderByDejaFait true si l'appelant annonce que l'order by a déjà été réalisé, et qu'il n'est pas nécessaire que la fonction y procède.
    * @return Dataset partitionné (si demandé) et trié (si demandé).
    */
   public <T> Dataset<T> appliquer(Tri tri, Dataset<T> candidat, boolean orderByDejaFait) {
      if (tri == null) {
         return candidat;
      }

      boolean hasColonnesDeTri = tri.getNomsColonnesTri() != null && tri.getNomsColonnesTri().length != 0;
      boolean hasColonnePartitionnement = tri.getNomColonnePartionnement() != null;

      PoseParquet<Tri> pose = this.poses.get(tri);

      if (hasColonnesDeTri) {
         pose.setSort(tri.getNomsColonnesTri());
      }

      if (hasColonnePartitionnement) {
         pose.setColonnePartitionnement(tri.getNomColonnePartionnement());
      }

      boolean colonnesTriEtPartitionnementIdentiques = hasColonnePartitionnement && hasColonnesDeTri
         && tri.getNomsColonnesTri().length == 1 && tri.getNomsColonnesTri()[0].equals(tri.getNomColonnePartionnement());

      // Un orderBy est nécessaire si on a des colonnes de tri, et :
      //    - La colonne de partitionnement et celle de tri sont identiques
      //    - et qu'un tri au sein de la partition n'a pas été demandé
      boolean orderByNecessaire = orderByDejaFait == false && hasColonnesDeTri && tri.sortWithinPartition() == false;

      LOGGER.info("Le parqueteur évalue un partitionnement par {} et colonnes de tri : {}", tri.getNomColonnePartionnement(), tri.getNomsColonnesTri());
      LOGGER.info("\tA une colonne de partitionnement: {}, a des colonnes de tri: {}, identiques ? {}", hasColonnePartitionnement, hasColonnesDeTri, colonnesTriEtPartitionnementIdentiques);
      LOGGER.info("\torderBy nécessaire: {} (annoncé déjà exécuté: {}, écriture sur disque prévue: {} (en remplacement ? {}), sortWithPartition: {})",
         orderByNecessaire, orderByDejaFait, this.optionsCreationLecture.useCache(), this.optionsCreationLecture.remplacerStore(), tri.sortWithinPartition());

      LOGGER.info("\tpartionnement en mémoire nécessaire: {}", hasColonnePartitionnement);

      if (orderByNecessaire) {
         candidat = candidat.orderBy(sortColumns(tri, candidat));
      }

      if (hasColonnePartitionnement) {
         if (tri.partitionByRange()) {
            candidat = candidat.repartitionByRange(candidat.col(tri.getNomColonnePartionnement()));
         }
         else {
            candidat = candidat.repartition(candidat.col(tri.getNomColonnePartionnement()));
         }

         // Si les colonnes de tri et celles de répartitions sont identiques
         // Il n'est pas nécessaire de retrier de nouveau, ni au sein des partitions. Cela a été fait apparavant.
         if (orderByDejaFait == false && hasColonnesDeTri && colonnesTriEtPartitionnementIdentiques == false && tri.sortWithinPartition()) {
            candidat = candidat.sortWithinPartitions(sortColumns(tri, candidat));
         }
      }
      
      pose.setSortWithPartitions(tri.sortWithinPartition());
      return candidat;
   }

   /**
    * Retrouver les colonnes de tri dans le dataset
    * @param tri tri.
    * @param candidat Dataset candidat.
    * @return tableau de Column de tri
    * @param <T> Type du dataset.
    */
   private <T> Column[] sortColumns(TriParqueteurIF tri, Dataset<T> candidat) {
      Column[] sortColumns = new Column[tri.getNomsColonnesTri().length];

      for(int index=0; index < tri.getNomsColonnesTri().length; index ++) {
         sortColumns[index] = candidat.col(tri.getNomsColonnesTri()[index]);
      }

      return sortColumns;
   }

   /**
    * Déclarer un nouveau tri possible.
    * @param tri Tri.
    * @throws ClassCastException si le tri n'implémente pas l'interface TriParqueteurIF.
    */
   private void declare(Tri tri) {
      this.poses.put(tri, new PoseParquet<>(tri, tri.getSuffixeFichier()));
   }

   /**
    * Déclarer tous les tris possibles.
    * @param triValues Enumération contenant tous les tris : tous ses membres seront parcourus.
    * @throws ClassCastException si le tri n'implémente pas l'interface TriParqueteurIF.
    */
   private void declareAll(Tri[] triValues) {
      for(Tri tri : triValues) {
         declare(tri);
      }
   }

   /**
    * Retrouver le chemin d'accès complet vers le fichier de cache correspondant à des critères de contenu et tri : ce fichier peut ne pas encore exister.
    * @param pourLecture true si le store à pour destination la lecture, auquel cas s'il y a une restriction, elle ne s'appliquera que si l'option <code>restriction.en.ecriture.seulement</code> est active.
    * @param fmtDeclinaisons Format des déclinaisons de contenu.
    * @param argsDeclinaisons Argument des déclinaisons de contenu.
    * @param tri Tri et partionnement.
    * @return Nom du fichier de cache.
    */
   private String getStore(boolean pourLecture, String fmtDeclinaisons, Object[] argsDeclinaisons, Tri tri) {
      String nomFichierCache = getNomFichierCache(fmtDeclinaisons, argsDeclinaisons, tri);

      // Sur option, le demandeur peut souhaiter forcer la lecture du plus vaste dataset, et ignorer celui restreint.
      boolean ignorerRestriction = pourLecture && this.optionsCreationLecture.restrictionEnEcritureSeulement();

      if (ignorerRestriction) {
         LOGGER.info("Si une restriction a été demandée pour le dataset {}, elle ne s'appliquera pas en lecture (restriction.en.ecriture.seulement=true).", nomFichierCache);
      } else {
         if (pourLecture) {
            LOGGER.info("Si une restriction a été demandée pour le dataset {}, elle s'appliquera pour cette lecture.", nomFichierCache);
         }
      }

      String repertoire = ignorerRestriction ? this.optionsCreationLecture.tempDirRoot() : this.optionsCreationLecture.tempDir();
      return new File(repertoire, nomFichierCache).getAbsolutePath();
   }

   /**
    * Retrouver le nom du fichier de cache correspondant à des critères de contenu et tri.
    * @param fmtDeclinaisons Format des déclinaisons de contenu.
    * @param argsDeclinaisons Argument des déclinaisons de contenu.
    * @param tri Tri et partionnement.
    * @return Nom du fichier de cache.
    */
   private String getNomFichierCache(String fmtDeclinaisons, Object[] argsDeclinaisons, Tri tri) {
      String partieTri = "";

      if (tri != null) {
         PoseParquet<Tri> pose = this.poses.get(tri);

         if (pose != null) {
            partieTri = pose.getNomDuTri();
         }
         else {
            LOGGER.warn("Un tri {} est désiré pour la constitution du fichier parquet {}, mais sa description n'est pas connue. (observation 1)", tri, this.nomStore);
         }
      }

      String formatNomFichier = fmtDeclinaisons != null ?
         MessageFormat.format("{0}-{1}-{2}", this.nomStore, this.formatDeclinaisons, partieTri) :
         MessageFormat.format("{0}-{1}", this.nomStore, partieTri);

      return MessageFormat.format(formatNomFichier, argsDeclinaisons);
   }
}
