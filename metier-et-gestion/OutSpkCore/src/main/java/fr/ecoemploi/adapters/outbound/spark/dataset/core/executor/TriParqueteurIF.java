package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.io.Serializable;

/**
 * Interface marqueuse d'un tri de fichier parquet.
 * @author Marc Le Bihan
 */
public interface TriParqueteurIF extends Serializable {
   /**
    * Renvoyer le nom de la colonne de partitionnement.
    * @return Nom de la colonne de partionnement ou null, s'il n'y a pas à partionner.
    */
   String getNomColonnePartionnement();

   /**
    * Renvoyer la liste des colonnes de tri.
    * @return Nom des champs à trier.
    */
   String[] getNomsColonnesTri();
   
   /**
    * Renvoyer le suffixe du fichier trié.
    * @return Suffixe.
    */
   String getSuffixeFichier();
   
   /**
    * Déterminer si le tri doit avoir lieu au sein des partitions.
    * @return true s'il faut faire un tri au sein des partitions,<br>
    * false si c'est un tri global.
    */
   boolean sortWithinPartition();

   /**
    * Déterminer s'il faut faire un sort partition by range.
    * @return true, si c'est le cas, false si un repartition(colonne) classique sera fait.
    */
   boolean partitionByRange();
}
