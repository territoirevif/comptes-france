package fr.ecoemploi.adapters.outbound.spark.dataset.core;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.function.*;

import org.jetbrains.annotations.NotNull;
import org.slf4j.*;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;

import scala.Tuple2;
import org.apache.spark.sql.*;
import org.apache.spark.util.CallSite;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.stream.Paginateur;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.verifications.Verificateur;

import static fr.ecoemploi.domain.utils.console.ConsoleEscapeCodes.*;

import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.utils.objets.GenerationDonneesNonAutoriseeException;
import fr.ecoemploi.service.*;

/**
 * Classe de base des datasets s'appuyant sur Spark.
 * @author Marc LE BIHAN
 */
public abstract class AbstractSparkDataset implements Serializable, ApplicationContextAware {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 4767656843791472227L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AbstractSparkDataset.class);

   /** URL JDBC. */
   @Value("${spring.datasource.url}")
   protected String urlJDBC;
   
   /** Username JDBC. */
   @Value("${spring.datasource.username}")
   protected String usernameJDBC;
   
   /** Password JDBC. */
   @Value("${spring.datasource.password}")
   protected String passwordJDBC;

   /** Indique si la génération de données (Apache Parquet ou autre) est autorisée */
   @Value("${generation.donnees.autorise:true}")
   public boolean generationDonneesAutorisee;

   /** Spark Session. */
   @Autowired
   protected SparkSession session;

   /** Emetteur de variable broadcast et de suivi */
   @Autowired
   private Emetteur emetteur;

   /** Export CSV. */
   @Autowired
   private ExportCSV exportCSV;

   /** Chargeur / Sauvegardeur de store CSV. */
   @Autowired
   private Loader loader;

   /** Paginateur. */
   @Autowired
   private Paginateur paginateur;

   /** Source de messages. */
   @Autowired
   protected transient MessageSource messageSource;

   /** Contexte de l'application (Spring) */
   private transient ApplicationContext applicationContext;

   /** Référentiel. */
   private final Referentiel referentiel = new Referentiel();

   /** Vérificateur de dataset. */
   private final Verificateur verificateur = new Verificateur();

   /** Processus de constitution standard de dataset Row */
   private final ConstitutionStandard constitutionStandard = new ConstitutionStandard();

   /**
    * Renvoyer un émetteur de descrition d'étape, de suivi et de broadcast de variables.
    * @return Emetteur.
    */
   public Emetteur emetteur() {
      return this.emetteur;
   }

   /**
    * Renvoyer le gestionnaire d'export en CSV.
    * @return Gestionnaire d'export en CSV.
    */
   public ExportCSV exportCSV() {
      return this.exportCSV;
   }

   /**
    * Indique si la génération de données (Apache Parquet ou autre) est autorisée.
    * @return <code>false</code> si seuls les fichiers de cache déjà présents peuvent être exploités.
    */
   public boolean isGenerationDonneesAutorisee() {
      return this.generationDonneesAutorisee;
   }

   /**
    * Renvoyer le chargeur de store.
    * @return Chargeur / stockeur.
    */
   public Loader loader() {
      return this.loader;
   }

   /**
    * Renvoyer un paginateur.
    * @return Paginateur.
    */
   public Paginateur paginateur() {
      return this.paginateur;
   }

   /**
    * Renvoyer un gestionnaire de référentiel.
    * @return Gestionnaire de référentiel.
    */
   public Referentiel referentiel() {
      return this.referentiel;
   }

   /**
    * Renvoyer le module de vérification de contenu de dataset.
    * @return Module de vérification.
    */
   public Verificateur verificateur() {
      return this.verificateur;
   }

   /**
    * Décliner des objets enfants selon leurs objets parents (exemple : entreprises en établissements).
    * @param parents Dataset d'objets parents.
    * @param columnJoinP Colonne de jointure du dataset des parents.
    * @param obtenirClefDuParent Fonction qui retourne depuis un objet métier parent P, sa clef KP.
    * @param enfants Dataset d'objets enfants.
    * @param columnJoinE Colonne de jointure du dataset des enfants.
    * @param obtenirClefEnfant Fonction qui retourne depuis un objet métier enfant E, sa clef KE.
    * @param obtenirMapEnfants Fonction qui retourne depuis un objet métier parent, une Map de ses enfants.
    * @param ensemble Réceptacle de la déclinaison enfants par parents.
    * @return Entreprises alimentées avec leurs établissements.
    * @param <P> Classe de l'objet métier parent.
    * @param <KP> Clef de la classe de l'objet métier parent.
    * @param <E> Classe de l'objet métier enfant.
    * @param <KE> Clef de la classe de l'objet métier enfant.
    * @param <C> Classe de l'objet récepteur de la déclinaison enfants par parent.
    */
   public <P extends Serializable, KP, E extends Serializable, KE, C extends Map<KP, P>> C declinaison(C ensemble,
      Dataset<P> parents, Column columnJoinP, Function<P, KP> obtenirClefDuParent,
      Dataset<E> enfants, Column columnJoinE, Function<E, KE> obtenirClefEnfant,
      Function<P, Map<KE, E>> obtenirMapEnfants) {
      Objects.requireNonNull(parents, "Le dataset parents ne peut pas valoir null.");
      Objects.requireNonNull(obtenirClefDuParent, "La fonction d'obtention de la clef du parent ne peut pas valoir null.");
      Objects.requireNonNull(enfants, "Le dataset enfants ne peut pas valoir null.");
      Objects.requireNonNull(obtenirMapEnfants, "La fonction retournant la map des objets enfants ne peut pas valoir null.");

      Dataset<Tuple2<P, E>> ds = parents.joinWith(enfants, columnJoinP.equalTo(columnJoinE), "inner");

      for(Tuple2<P, E> tuple : ds.collectAsList()) {
         // Rechercher l'objet parent par KP, et l'ajouter à l'ensemble C, vide, s'il n'existe  pas.
         P source = tuple._1();
         KP clefSourceParent = obtenirClefDuParent.apply(source);
         P parent = ensemble.computeIfAbsent(clefSourceParent, clef -> tuple._1());

         // Dans cet objet parent P, rechercher la liste des enfants E, indexée par KE, et y ajouter notre instance de E.
         Map<KE, E> ensembleEnfants = obtenirMapEnfants.apply(parent);
         E nouvelEnfant = tuple._2();
         KE clefEnfant = obtenirClefEnfant.apply(nouvelEnfant);
         ensembleEnfants.put(clefEnfant, nouvelEnfant);
      }

      return ensemble;
   }

   /**
    * Renvoyer un lecteur de table préparé avec une requête de base de données (ou un nom de table).
    * @param session Session Spark.
    * @param sql Requête SQL ou nom de table (à lire en entier).
    * @return DataframeReader chargé avec la requête vers la base locale.
    */
   protected DataFrameReader sql(SparkSession session, String sql) {
      return session.read().format("jdbc")
        .option("url", this.urlJDBC)
        .option("user", this.usernameJDBC)
        .option("password", this.passwordJDBC)
        .option("dbtable", sql);
   }

   /**
    * Fixer le libellé descriptif du stage.
    * @param format Format.
    * @param args Arguments.
    * @return Texte du message produit.
    */
   public String setStageDescription(String format, Object... args) {
      return setStageDescription(this.session, format, args);
   }

   /**
    * Fixer le libellé descriptif du stage.
    * @param formatShort Format du libellé court.
    * @param formatLong Format du libellé long.
    * @param args Arguments.
    * @return Texte du message long produit.
    */
   public String setStageDescription(String formatShort, String formatLong, Object... args) {
      return setStageDescription(this.session, formatShort, formatLong, args);
   }

   /**
    * Fixer le libellé descriptif du stage.
    * @param messageSource Source des messages
    * @param keyShort Clef du message contenant le libellé court.
    * @param keyLong Clef du message contenant le libellé long.
    * @param args Arguments.
    * @return Texte du message long produit.
    */
   public String setStageDescription(MessageSource messageSource, String keyShort, String keyLong, Object... args) {
      return setStageDescription(this.session, messageSource, keyShort, keyLong, args);
   }

   /**
    * Fixer le libellé descriptif du stage.
    * @param session Session spark.
    * @param format Format.
    * @param args Arguments.
    * @return Texte du message produit.
    */
   public static String setStageDescription(SparkSession session, String format, Object... args) {
      String message = MessageFormat.format(format, args);
      session.sparkContext().setCallSite(MessageFormat.format(format, args));
      return message;
   }

   /**
    * Fixer le libellé descriptif du stage.
    * @param session Session spark.
    * @param formatShort Format du libellé court.
    * @param formatLong Format du libellé long.
    * @param args Arguments.
    * @return Texte du message long produit.
    */
   public static String setStageDescription(SparkSession session, String formatShort, String formatLong, Object... args) {
      String messageLong = MessageFormat.format(formatLong, args);
      session.sparkContext().setCallSite(new CallSite(MessageFormat.format(formatShort, args), messageLong));
      return messageLong;
   }

   /**
    * Fixer le libellé descriptif du stage.
    * @param session Session spark.
    * @param messageSource Source des messages
    * @param keyShort Clef du message contenant le libellé court.
    * @param keyLong Clef du message contenant le libellé long.
    * @param args Arguments.
    * @return Texte du message long produit.
    */
   public static String setStageDescription(SparkSession session, MessageSource messageSource, String keyShort, String keyLong, Object... args) {
      String messageShort = messageSource.getMessage(keyShort, args, Locale.getDefault());
      String messageLong = messageSource.getMessage(keyLong, args, Locale.getDefault());

      session.sparkContext().setCallSite(new CallSite(messageShort, messageLong));
      return messageLong;
   }

   /**
    * Appliquer un sample, si réclamé, sur un dataset.
    * @param dataset Dataset.
    * @return Dataset au sample appliqué, ou renvoyé tel quel selon le paramétrage du dataset.
    * @param sampleFraction Fraction de sample. Pas de sample réalisé si égal à 0.0
    * @param sampleSeed seed utilisé pour le sample. Pas de seed si égal à 0 ou null.
    * @param <T> Type du dataset.
    */
   protected <T> Dataset<T> applySample(Dataset<T> dataset, double sampleFraction, Long sampleSeed) {
      return AbstractSparkCsvLoader.applySample(dataset, sampleFraction, sampleSeed);
   }

   /**
    * Renvoyer des options par défaut de comportement pour la création / lecture d'un dataset durant l'exécution d'une de ses méthodes.
    * @return Options de création et de lecture.
    */
   public OptionsCreationLecture optionsCreationLecture() {
      return (OptionsCreationLecture)this.applicationContext.getBean("optionsCreationLecture");
   }

   /**
    * Obtenir une clause de restriction à des départements de communes demandées en options.
    * @param options Options où une restriction à des communes peut avoir été demandée.
    * @param champDepartement Nom du champ département à utiliser dans la condition de sélection.
    * @param conditionParente Condition parente à completer et à renvoyer complétée, si non nulle.
    * @param and true si la condition reliant ce nouveau critère avec la condition parente est un <code>and</code>. false si c'est un <code>or</code>.
    * @return Column portant une condition de sélection sur leurs départements ou null s'il n'y a aucune commune de restriction dans les options.
    */
   public static Column restrictionAuxDepartementsDeCommunes(OptionsCreationLecture options, NomChamp champDepartement, Column conditionParente, boolean and) {
      // Si une restriction à quelques communes a été demandée, prendre toutes celles de leur département
      Column postFiltre = null;

      if (options.hasRestrictionAuxCommunes()) {
         Set<String> departements = new HashSet<>();

         for(String commune : options.restrictionAuxCommunes()) {
            departements.add(new CodeCommune(commune).numeroDepartement());
         }

         postFiltre = champDepartement.col().isin(departements.toArray());
      }

      if (conditionParente == null) {
         return postFiltre;
      }

      return and ? conditionParente.and(postFiltre) : conditionParente.or(postFiltre);
   }

   /**
    * Obtenir une clause de restriction à une année demandée en options.
    * @param options Options où une restriction à une année peut avoir été demandée.
    * @param champAnnee Nom du champ année à utiliser dans la condition de sélection.
    * @param conditionParente Condition parente à completer et à renvoyer complétée, si non nulle.
    * @param and true si la condition reliant ce nouveau critère avec la condition parente est un <code>and</code>. false si c'est un <code>or</code>.
    * @return Column portant une condition de sélection une année ou null s'il n'y a aucune année de restriction dans les options.
    */
   public static Column restrictionAnnee(OptionsCreationLecture options, NomChamp champAnnee, Column conditionParente, boolean and) {
      // Si une restriction à une année a été demandée, la placer en clause
      Column postFiltre = null;

      if (options.hasRestrictionAnnee()) {
         postFiltre = champAnnee.col().equalTo(options.restrictionAnnee());
      }


      if (conditionParente == null) {
         return postFiltre;
      }

      return and ? conditionParente.and(postFiltre) : conditionParente.or(postFiltre);
   }

   /**
    * Constituer un dataset selon la méthode standard : <br>
    *    - Lecture depuis le store (le cache)<br>
    *    - S'il n'existe pas, constitution<br>
    *    - et sauvegarde dans le store, avant renvoi.
    *
    * @param optionsCreationLecture Option de lecture et création de dataset.
    * @param worker Worker pour constituer le dataset, s'il faut le générer.
    * @param historiqueExecution Historique d'exécution.
    * @param cacheParqueteur Gestionnaire de cache pour ce dataset.
    * @return Dataset constitué.
    * @deprecated Utiliser la méthode prenant <code>Column postFiltre</code> en argument.
    */
   @Deprecated(forRemoval = true)
   protected Dataset<Row> constitutionStandard(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution,
      Supplier<Dataset<Row>> worker, CacheParqueteur<? extends TriParqueteurIF> cacheParqueteur) {
      return this.constitutionStandard.constitutionStandard(this, optionsCreationLecture, historiqueExecution, worker, cacheParqueteur);
   }

   /**
    * Constituer un dataset selon la méthode standard : <br>
    *    - Lecture depuis le store (le cache)<br>
    *    - S'il n'existe pas, constitution<br>
    *    - et sauvegarde dans le store, avant renvoi.
    *
    * @param optionsCreationLecture Option de lecture et création de dataset.
    * @param worker Worker pour constituer le dataset, s'il faut le générer.
    * @param anneeEligibleSauvegarde true si l'année en cours de traitement (si applicable) rend ce dataset éligible à une possible sauvegarde (éventuellement, sous autres conditions),
    *    false si, pour une restriction sur année par exemple, ce dataset constitué ne devra pas être sauvegardé.
    * @param conditionPostFiltrage Fonction de post-filtrage <code>Column = postFiltrage(options, dataset_issu_du_worker)</code>, si non <code>null</code> une clause where avec cette condition sera appliquée avant l'écriture du dataset
    * @param cacheParqueteur Gestionnaire de cache pour ce dataset.
    * @param historiqueExecution Historique d'exécution à alimenter, si non null.
    * @return Dataset constitué.
    * @throws GenerationDonneesNonAutoriseeException s'il est nécessaire de produire les données générées, mais que l'application n'en a pas le droit
    */
   protected Dataset<Row> constitutionStandard(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution,
      Supplier<Dataset<Row>> worker, boolean anneeEligibleSauvegarde, ConditionPostFiltrage conditionPostFiltrage, CacheParqueteur<? extends TriParqueteurIF> cacheParqueteur) {
      return this.constitutionStandard.constitutionStandard(this, optionsCreationLecture, historiqueExecution,
         worker, anneeEligibleSauvegarde, conditionPostFiltrage, cacheParqueteur);
   }

   /**
    * Garantit l'existence d'un répertoire, et le renvoie.
    * @param repertoire Chemin d'accès au répertoire recherché.
    * @return Répertoire.
    * @throws RepertoireInexistantException si le répertoire n'existe pas.
    */
   public static File assertExistenceRepertoire(String repertoire) throws RepertoireInexistantException {
      Objects.requireNonNull(repertoire, "Le répertoire dont on veut vérifier l'existence ne peut pas valoir null.");

      File directory = new File(repertoire);

      // Si le répertoire n'existe pas, lever une exception.
      if (directory.exists() == false) {
         String message = MessageFormat.format("Le répertoire ''{0}'' n''existe pas.", directory.getAbsolutePath());
         LOGGER.warn(message);
         throw new RepertoireInexistantException(message, directory.getAbsolutePath());
      }

      // S'il désigne un fichier à la place, également.
      if (directory.isDirectory() == false) {
         String message = MessageFormat.format("Le répertoire ''{0}'' désigne un fichier et non un répertoire.", directory.getAbsolutePath());
         LOGGER.warn(message);
         throw new RepertoireInexistantException(message, directory.getAbsolutePath());
      }

      return directory;
   }

   /**
    * Garantit l'existence d'un fichier, et le renvoie.
    * @param repertoireParent Répertoire parent.
    * @param annee Année du fichier recherché, 0 pour un fichier global qui sera cherché dans le répertoire parent, directement.
    * @param nomFichier Nom du fichier à sélectionner dans cette année.
    * @param parametresNomFichier Paramètres optionnels du nom du fichier.
    * @return Fichier.
    * @throws RepertoireInexistantException si le répertoire parent ou le sous-répertoire n'existent pas.
    * @throws FichierGeographiqueAbsentException si le fichier géographique désiré n'existe pas.
    */
   public static File assertExistenceFichierAnnuel(File repertoireParent, int annee, String nomFichier, String... parametresNomFichier) throws RepertoireInexistantException, FichierGeographiqueAbsentException {
      Objects.requireNonNull(repertoireParent, "Le répertoire parent où rechercher le fichier annuel ne peut pas valoir null.");
      Objects.requireNonNull(nomFichier, "Le nom du fichier à accoler au répertoire annuel ne peut pas valoir null.");

      // Concaténer au répertoire parent le sous-répertoire correspondant à l'année recherchée et le valider à son tour.
      File directory = assertExistenceRepertoire(repertoireParent.getAbsolutePath());
      String repertoireAnnuel = annee != 0 ? MessageFormat.format("{0}{1}{2,number,#0}", repertoireParent, File.separator, annee) : MessageFormat.format("{0}", repertoireParent);
      File directoryAnnuel = assertExistenceRepertoire(repertoireAnnuel);

      // Ajoindre le fichier recherché, et vérifier également sa présence, et qu'il est bien un fichier et non un répertoire.
      String nomFichierFinal;

      if (parametresNomFichier != null && parametresNomFichier.length != 0) {
         nomFichierFinal = MessageFormat.format(nomFichier, (Object[])parametresNomFichier);
      }
      else {
         nomFichierFinal = nomFichier;
      }

      String fichierCible = MessageFormat.format("{0}{1}{2}", directoryAnnuel.getAbsolutePath(), File.separator, nomFichierFinal);
      File fileCible = new File(fichierCible);

      if (fileCible.exists() == false) {
         String message = MessageFormat.format("Le fichier ''{0}'' n''existe pas.", fileCible.getAbsolutePath());
         LOGGER.warn(message);
         throw new FichierGeographiqueAbsentException(message, fileCible.getAbsolutePath());
      }

      // S'il désigne un fichier à la place, également.
      if (fileCible.isFile() == false) {
         String message = MessageFormat.format("Le fichier ''{0}'' désigne un répertoire et non un fichier.", directory.getAbsolutePath());
         LOGGER.warn(message);
         throw new RepertoireInexistantException(message, directory.getAbsolutePath());
      }

      return fileCible;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
      this.applicationContext = applicationContext;

      LOGGER.debug(this.generationDonneesAutorisee ?
         "L'application et les appels de méthodes REST peuvent générer des données Apache Parquet" :
         BYELLOW.color("L'application et les appels de méthodes REST peuvent utiliser, mais pas générer, des données Apache Parquet"));
   }
}
