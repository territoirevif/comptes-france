package fr.ecoemploi.adapters.outbound.spark.dataset.core;

import java.io.Serial;
import java.util.List;
import java.util.stream.Stream;

import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.stream.Paginateur;

/**
 * Dataset se mappant sur un objet métier principal.
 * @param <OM> Objet métier représenté par le Dataset
 */
public abstract class AbstractSparkObjetMetierDataset<OM> extends AbstractSparkDataset implements DatasetRowToObjetMetierInterface<OM> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -3286729699220772329L;

   /**
    * Convertir un Dataset Row en Dataset d'objets métiers.
    * @param rows Dataset Row à convertir.
    * @return Dataset d'objets métiers.
    */
   @Override
   public Dataset<OM> toDatasetObjetsMetiers(Dataset<Row> rows) {
      return objetMetierEncoder().toDatasetObjetsMetiers(rows);
   }

   /**
    * Convertir un Dataset Row d'associations en liste d'objets métiers.
    * @param rows Dataset Row à convertir.
    * @return Liste d'objets métiers.
    */
   @Override
   public List<OM> toObjetsMetiersList(Dataset<Row> rows) {
      return toDatasetObjetsMetiers(rows).collectAsList();
   }

   /**
    * Convertir en Stream un dataset Row.
    * @param rows Dataset Row à convertir.
    * @return Flux.
    */
   @Override
   public Stream<OM> toObjetsMetiersStream(Dataset<Row> rows) {
      return Paginateur.paginerEnStream(rows, objetMetierEncoder()::toDatasetObjetsMetiers, this.paginateur().getNombreObjetsParPageStream());
   }

   /**
    * Convertir en Stream un dataset Row.
    * @param rows Dataset Row à convertir.
    * @param blockSize Nombre d'éléments à placer par bloc.
    * @return Flux.
    */
   public Stream<OM> toObjetsMetiersStream(Dataset<Row> rows, int blockSize) {
      return Paginateur.paginerEnStream(rows, objetMetierEncoder()::toDatasetObjetsMetiers, blockSize);
   }

   /**
    * Convertir en Stream un dataset.
    * @param dataset Dataset déjà converti en objets métiers à convertir.
    * @param blockSize Nombre d'éléments à placer par bloc.
    * @param <T> Type des objets transportés.
    * @return Flux.
    */
   public <T> Stream<T> toStream(Dataset<T> dataset, int blockSize) {
      return Paginateur.paginerEnStream(dataset, blockSize);
   }

   /**
    * Convertir un Row en objet métier.
    * @param row Row à convertir.
    * @return objet métier.
    */
   @Override
   public OM toObjetMetier(Row row) {
      return objetMetierEncoder().toObjetMetier(row);
   }

   /**
    * Renvoie le convertisseur de Row en objet métier.
    * @return Convertisseur.
    */
   protected abstract RowToObjetMetierInterface<OM> objetMetierEncoder();
}
