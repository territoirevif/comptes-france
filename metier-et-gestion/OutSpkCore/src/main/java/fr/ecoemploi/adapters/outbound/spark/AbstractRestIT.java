package fr.ecoemploi.adapters.outbound.spark;

import org.apache.commons.lang3.time.*;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Value;

import java.text.MessageFormat;
import java.util.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.verifications.Verificateur;

/**
 * Classe de base de test des services REST.
 * @author Marc LE BIHAN
 */
public abstract class AbstractRestIT {
   /** Si tous les tris doivent être activés et testés. */
   @Value("${test.tous.tris:false}")
   private boolean tousTris;
   
   /** Si toutes les années doivent être testées. */
   @Value("${test.toutes.annees:false}")
   private boolean toutesAnnees;
   
   /** Indique si un explain plan doit être réalisé (null si aucun). */
   @Value("${test.explain.plan:null}")
   private String explainPlan;

   /** Indique si le cache des dataset doit être utilisé. */
   @Value("${test.activer.cache:true}")
   private boolean activerCache;

   /** Nombre d'éléments du dataset à montrer, par défaut. */
   @Value("${test.show.take:200}")
   private int take;

   /** Chronomètre. */
   protected StopWatch chrono = new StopWatch();

   /** Mesure de la valeur du chrono précédent. */
   private long mesureChronoPrecedent = 0L;

   /**
    * Prendre une mesure du chronomètre, et l'afficher.
    * @param text Texte.
    * @param log Log.
    */
   public void chrono(String text, Logger log) {
      this.chrono.split();
      long mesureChronoActuel = this.chrono.getTime();

      String message = MessageFormat.format("{0} : {1} (+{2})",
         text, this.chrono.formatSplitTime(), DurationFormatUtils.formatDurationHMS(mesureChronoActuel - this.mesureChronoPrecedent));

      this.mesureChronoPrecedent = mesureChronoActuel;
      log.info(message);
   }

   /**
    * Déterminer s'il faut activer le cache des datasets durant les tests.
    * @return true s'il sera activé
    *    false s'ils seront intégralement reconstruits à chaque fois.
    */
   public boolean activerCache() {
      return this.activerCache;
   }

   /**
    * Return Explain plan à utiliser durant les tests.
    * @return Spark SQL Explain Plan
    */
   public String explainPlan() {
      return this.explainPlan;
   }

   /**
    * Renvoyer le nombre d'enregistrements à montrer d'un dataset.
    * @return Nombre d'enregistrements.
    */
   public int showTake() {
      return this.take;
   }

   /**
    * Déterminer s'il faut utiliser un explain plan durant les tests.
    * @return true si on le désire.
    */
   public boolean useExplainPlan() {
      return this.explainPlan != null && "null".equals(this.explainPlan) == false;
   }

   /**
    * Déterminer si l'on veut toutes les années.
    * @return true si on veut toutes les expérimenter.<br>
    *    false si seule celle mise en exergue dans le test le sera.
    */
   public boolean toutesAnnees() {
      return this.toutesAnnees;
   }

   /**
    * Déterminer si l'on veut tous les tris.
    * @return true si l'on veut tous les expérimenté.<br>
    *    false si seul le premier d'entre eux le sera.
    */
   public boolean tousTris() {
      return this.tousTris;
   }

   /**
    * Vérifier que les champs attendus sont présents, et dans le bon ordre.
    * @param schema Schéma à contrôler.
    * @param verificateur fonction du dataset renvoyant les champs manquants.
    * @param champsAttendus Champs obligatoire.
    * @param verifierOrdre true s'il faut vérifier l'ordre.
    * @throws NoSuchElementException si un champ obligatoire manque.
    * @throws IllegalStateException si un champ est mal placé (si le contrôle de l'ordre a été demandé).
    */
   public void assertFields(StructType schema, Verificateur verificateur, String[] champsAttendus, boolean verifierOrdre) {
      // Vérifier la présence de tous les champs attendus.
      List<String> manquants = verificateur.champsManquants(schema, champsAttendus);

      if (manquants.isEmpty() == false) {
         throw new NoSuchElementException(MessageFormat.format("{0} champs sont absents du Dataset : {1}.", manquants.size(), manquants));
      }

      // Vérifier leur bon ordre.
      if (verifierOrdre) {
         for (int index = 0; index < schema.size(); index++) {
            String attendu = champsAttendus[index];
            String present = schema.fields()[index].name();

            if (attendu.equals(present) == false) {
               throw new IllegalStateException(MessageFormat.format("à la position {0} (base index 1), le champ {1} est à la place de celui {2} attendu.", index + 1, present, attendu));
            }
         }
      }
   }

   /**
    * Dump d'un Dataset en show et log.
    * @param <X> Classe de l'objet du Dataset.
    * @param ds Dataset.
    * @param take Nombre d'éléments à présenter.
    * @param log Logger s'il faut faire un toString() de l'objet.
    */
   protected <X> void dump(Dataset<X> ds, int take, Logger log) {
      ds.show(take, false);
      
      if (log != null) {
         List<X> extrait = ds.takeAsList(take);

         if (extrait != null) {
            log.info(extrait.toString()); //NOSONAR
         }
      }
   }

   /**
    * Présenter les résultats de tests d'une manière standardisée.
    * @param ds Dataset.
    * @param take Nombre d'éléments à afficher.
    * @param compterPartitionsEtEnregistrements true s'il faut compter le nombre d'enregistrements du dataset et de ses partitions<br>
    *     attention, cela a un coût en temps.
    * @param explainPlanMode Mode de l'explain plan<ul>
    *    <li>simple: Print only a physical plan.</li>
    *    <li>extended: Print both logical and physical plans.</li>
    *    <li>codegen: Print a physical plan and generated codes if they are available.</li>
    *    <li>cost: Print a logical plan and statistics if they are available.</li>
    *    <li>formatted: Split explain output into two sections: a physical plan outline and node details.</li></ul>
    * @param toString s'il faut faire un toString sur le contenu des row ou objet du dataset.
    * @param log Logger de destination.
    * @param <X> Classe des objets contenus dans le dataset.
    */
   protected <X> void testShow(Dataset<X> ds, int take, String explainPlanMode, boolean compterPartitionsEtEnregistrements, boolean toString, Logger log) {
      if (toString)
         dump(ds, take, log);
      else
         ds.show(take, false);

      if (explainPlanMode != null && "null".equals(explainPlanMode) == false) {
         ds.explain(explainPlanMode);
      }

      ds.printSchema();

      if (compterPartitionsEtEnregistrements) {
         log.info("Il y a {} enregistrements non filtrés dans {} partitions.", ds.count(), ds.rdd().getNumPartitions());
      }
   }
}
