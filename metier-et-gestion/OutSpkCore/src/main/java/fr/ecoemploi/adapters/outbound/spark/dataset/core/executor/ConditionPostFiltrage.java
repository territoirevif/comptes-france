package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.util.function.*;

import org.apache.spark.sql.*;

/**
 * Fonction renvoyant une condition de post-filtrage<br>
 * <code>Column = conditionPostFiltrage(options, dataset_issu_du_worker)</code>
 *
 * @author Marc Le Bihan
 */
public interface ConditionPostFiltrage extends BiFunction<OptionsCreationLecture, Supplier<Dataset<Row>>, Column> {
}
