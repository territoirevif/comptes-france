package fr.ecoemploi.adapters.outbound.spark.dataset.core;

import java.io.*;
import java.util.Map;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;
import org.slf4j.*;

/**
 * Classe de base des chargeurs de fichier CSV.
 * @author Marc Le Bihan
 */
public abstract class AbstractSparkCsvLoader implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -9052056154284795445L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AbstractSparkCsvLoader.class);

   /**
    * Renvoyer l'identifiant du catalogue data.gouv.fr qui se rapporte au jeu de données qui possède ce fichier, s'il est connu.
    * @return Identifiant du catalogue ou vide s'il est inconnu.
    */
   public String getCatalogueId() {
      return "";
   }

   /**
    * Renvoyer l'identifiant d'organisation data.gouv.fr qui met à disposition ce jeu de données ou fichier, s'il est connu.
    * @return Identifiant de l'organisation ou vide s'il est inconnu.
    */
   public String getOrganisationId() {
      return "";
   }

   /**
    * Renvoyer l'identifiant de la ressource data.gouv.fr qui se rapporte à ce fichier, s'il est connu.
    * @return Identifiant de la ressource ou vide s'il est inconnu.
    */
   public String getResourceId() {
      return "";
   }

   /**
    * Renvoyer le schéma du CSV source tel qu'il est supposé être dans le fichier téléchargé<br>
    * (s'il advient qu'il est différent, la plupart des constructeurs de dataset s'interromperont)
    * @param annee Année du dataset (ou indicateur de sa version)
    * @return Schema Spark
    */
   protected StructType schema(int annee) {
      return new StructType();
   }

   /**
    * Provoquer le chargement effectif de la source de données.
    * @param annee Année du dataset (ou indicateur de sa version)
    * @param source Fichier source CSV à lire
    * @param schema Schéma
    */
   protected Dataset<Row> load(StructType schema, int annee, File source) {
      return null;
   }

   /**
    * Renommer les champs du dataset.
    * @param annee Année du dataset (ou indicateur de sa version)
    * @param dataset Dataset.
    */
   protected Dataset<Row> rename(Dataset<Row> dataset, int annee) {
      return dataset;
   }

   /**
    * Caster le contenu du dataset pour améliorer les types de ses champs.
    * @param dataset Dataset à caster.
    * @param annee Année du dataset (ou indicateur de sa version)
    * @return dataset aux types plus précis.
    */
   protected Dataset<Row> cast(Dataset<Row> dataset, int annee) {
      return dataset;
   }

   /**
    * Reclasser les champs pour que les colonnes apparaissent dans un ordre établi
    * @param dataset Dataset à reclasser.
    * @param annee Année du dataset (ou indicateur de sa version)
    * @return dataset reclassé.
    */
   protected Dataset<Row> select(Dataset<Row> dataset, int annee) {
      return dataset;
   }

   /**
    * Appliquer un sample, si réclamé, sur un dataset.
    * @param dataset Dataset.
    * @return Dataset au sample appliqué, ou renvoyé tel quel selon le paramétrage du dataset.
    * @param sampleFraction Fraction de sample. Pas de sample réalisé si égal à 0.0
    * @param sampleSeed seed utilisé pour le sample. Pas de seed si égal à 0 ou null.
    * @param <T> Type du dataset.
    */
   protected static <T> Dataset<T> applySample(Dataset<T> dataset, double sampleFraction, Long sampleSeed) {
      if (sampleFraction != 0.0) {
         boolean avecSeedFixe = sampleSeed != null && sampleSeed != 0;

         LOGGER.info("Un sample de fraction {}, seed {} est appliqué. Ce n'est pas le dataset complet.",
            sampleFraction, avecSeedFixe ? String.valueOf(sampleSeed) : "aléatoire");

         dataset = avecSeedFixe ? dataset.sample(sampleFraction, sampleSeed) : dataset.sample(sampleFraction);
      }

      return dataset;
   }

   /**
    * Renommer les champs d'un dataset, s'ils existent dans le schéma.
    * @param dataset Dataset candidat.
    * @param renommages Renommages à appliquer.
    * @return Dataset traité.
    */
   protected Dataset<Row> renameFields(Dataset<Row> dataset, Map<String, String> renommages) {
      StructType schema = dataset.schema();
      Dataset<Row> cible = dataset;

      for(StructField field : schema.fields()) {
         String renommer = renommages.get(field.name());

         if (renommer != null) {
            cible = cible.withColumnRenamed(field.name(), renommer);
         }
      }

      return cible;
   }
}
