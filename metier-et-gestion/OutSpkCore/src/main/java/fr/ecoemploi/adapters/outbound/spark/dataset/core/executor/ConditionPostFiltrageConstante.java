package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.util.function.Supplier;

import org.apache.spark.sql.*;

/**
 * Condition de post-filtrage constante : renvoie une <code>Column</code> pré-définie
 */
public class ConditionPostFiltrageConstante implements ConditionPostFiltrage {
   /** Condition prédéfinie */
   private final Column condition;

   /**
    * Construire une condition de post-filtrage prédéfinie
    * @param condition Condition de post-filtrage
    */
   public ConditionPostFiltrageConstante(Column condition) {
      this.condition = condition;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Column apply(OptionsCreationLecture options, Supplier<Dataset<Row>> worker) {
      return this.condition;
   }
}
