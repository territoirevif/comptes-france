package fr.ecoemploi.adapters.outbound.spark.dataset.core.validation;

import java.io.*;
import java.util.*;

/**
 * Clef d'un élément invalide
 * @author Marc Le Bihan
 */
public class ClefElementInvalide implements Serializable, Iterable<Serializable> {
   /** Serial UUID */
   @Serial
   private static final long serialVersionUID = 9132459461228934573L;

   /** La liste des arguments de l'élément invalidé. */
   private final List<Serializable> arguments = new ArrayList<>();

   /**
    * Construire une clef d'élement invalide.
    * @param arguments Arguments de l'anomalie.
    */
   public ClefElementInvalide(Serializable... arguments) {
      this.arguments.addAll(Arrays.asList(arguments));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Iterator<Serializable> iterator() {
      return this.arguments.iterator();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean equals(Object clef) {
      if (clef instanceof ClefElementInvalide candidat) {
         return candidat.arguments.equals(this.arguments);
      }

      return false;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public int hashCode() {
      return arguments.hashCode();
   }

   /**
    * {@inheritDoc}
    */
   public String toString() {
      return List.of(this.arguments).toString();
   }
}
