package fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata;

import java.io.*;
import java.util.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;

import static org.apache.spark.sql.types.DataTypes.StringType;

/**
 * Partie accès à un reférentiel d'un dataset
 */
public class Referentiel implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5191253666612271943L;

   /**
    * Créer un dataset de type référentiel sur le modèle (clef, valeur).
    * @param session Session Spark.
    * @param nomChampClef Nom du champ clef.
    * @param nomChampValeur1OuClef2 Nom du champ servant de deuxième clef ou de première valeur.
    * @param nomChampValeur2ou1 Nom du champ servant de première ou deuxième valeur.
    * @param tripletClefsValeurs Tableau de (clef, clef2/valeur1, valeur1/valeur2), tous trois des String.
    * @return Dataset référentiel.
    */
   public Dataset<Row> creerReferentiel3champs(SparkSession session, String nomChampClef, String nomChampValeur1OuClef2, String nomChampValeur2ou1, String[][] tripletClefsValeurs) {
      List<Row> contenuDataset = new ArrayList<>();
      Arrays.asList(tripletClefsValeurs).forEach(trippletClefValeur -> contenuDataset.add(RowFactory.create(trippletClefValeur[0], trippletClefValeur[1], trippletClefValeur[2])));

      return session.createDataFrame(contenuDataset, new StructType()
         .add(nomChampClef, StringType, false)
         .add(nomChampValeur1OuClef2, StringType, true)
         .add(nomChampValeur2ou1, StringType, true));
   }

   /**
    * Créer un dataset de type référentiel sur le modèle (clef, valeur).
    * @param session Session Spark.
    * @param nomChampClef Nom du champ clef.
    * @param nomChampValeur Nom du champ valeur.
    * @param pairesClefsValeurs Tableau de (clef, valeur), tous deux des String.
    * @return Dataset référentiel.
    */
   public Dataset<Row> creerReferentiel(SparkSession session, String nomChampClef, String nomChampValeur, String[][] pairesClefsValeurs) {
      List<Row> contenuDataset = new ArrayList<>();
      Arrays.asList(pairesClefsValeurs).forEach(paireClefValeur -> contenuDataset.add(RowFactory.create(paireClefValeur[0], paireClefValeur[1])));

      return session.createDataFrame(contenuDataset, new StructType()
         .add(nomChampClef, StringType, false)
         .add(nomChampValeur, StringType, true));
   }

   /**
    * Joindre un référentiel.
    * @param source Source à alimenter.
    * @param referentiel Référentiel pour la compléter.
    * @param clefSource Nom du champ servant de clef dans la source.
    * @param clefReferentiel Nom du champ servant de première clef dans le référentiel pour la jointure.
    * @return Dataset alimenté des champs du référentiel, moins un champ doublon éventuel.
    */
   public Dataset<Row> joinReferentiel(Dataset<Row> source, Dataset<Row> referentiel, String clefSource, String clefReferentiel) {
      Dataset<Row> ref = referentiel;

      boolean clefsEgales = clefSource.equals(clefReferentiel);
      ref = clefsEgales ? ref.withColumnRenamed(clefReferentiel, "jkey") : ref;
      Column joinClause = clefsEgales ? source.col(clefSource).equalTo(ref.col("jkey")) : source.col(clefSource).equalTo(ref.col(clefReferentiel));

      source = source.join(ref, joinClause, "left_outer");

      if (clefsEgales) {
         source = source.drop(source.col("jkey"));
      }

      return source;
   }

   /**
    * Joindre un référentiel.
    * @param source Source à alimenter.
    * @param referentiel Référentiel pour la compléter.
    * @param clefSource1 Nom du champ servant de première clef dans la source.
    * @param clefReferentiel1 Nom du champ servant de première clef dans le référentiel pour la jointure.
    * @param clefSource2 Nom du champ servant de deuxième clef dans la source.
    * @param clefReferentiel2 Nom du champ servant de deuxième clef dans le référentiel pour la jointure.
    * @return Dataset alimenté des champs du référentiel, moins un champ doublon éventuel.
    */
   public Dataset<Row> joinReferentielDeuxClefs(Dataset<Row> source, Dataset<Row> referentiel, String clefSource1, String clefReferentiel1, String clefSource2, String clefReferentiel2) {
      boolean clefs1Egales = clefSource1.equals(clefReferentiel1);
      boolean clefs2Egales = clefSource2.equals(clefReferentiel2);

      Dataset<Row> ref = referentiel;
      final String JKEY1 = "jkey1";
      final String JKEY2 = "jkey2";

      ref = clefs1Egales ? ref.withColumnRenamed(clefReferentiel1, JKEY1) : ref;
      ref = clefs2Egales ? ref.withColumnRenamed(clefReferentiel2, JKEY2) : ref;
      Column joinClause1 = clefs1Egales ? source.col(clefSource1).equalTo(ref.col(JKEY1)) : source.col(clefSource1).equalTo(ref.col(clefReferentiel1));
      Column joinClause2 = clefs2Egales ? source.col(clefSource2).equalTo(ref.col(JKEY2)) : source.col(clefSource2).equalTo(ref.col(clefReferentiel2));

      source = source.join(ref, joinClause1.and(joinClause2), "left_outer");

      if (clefs1Egales) {
         source = source.drop(source.col(JKEY1));
      }

      if (clefs2Egales) {
         source = source.drop(source.col(JKEY2));
      }

      return source;
   }

   /**
    * Joindre un référentiel.
    * @param source Source à alimenter.
    * @param referentiel Référentiel pour la compléter.
    * @param clef Nom du champ servant de clef dans la source et le référentiel.
    * @return Dataset alimenté des champs du référentiel, moins un champ doublon éventuel.
    */
   public Dataset<Row> joinReferentiel(Dataset<Row> source, Dataset<Row> referentiel, String clef) {
      return joinReferentiel(source, referentiel, clef, clef);
   }
}
