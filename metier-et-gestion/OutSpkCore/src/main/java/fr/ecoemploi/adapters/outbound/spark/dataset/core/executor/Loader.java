package fr.ecoemploi.adapters.outbound.spark.dataset.core.executor;

import java.io.*;
import java.text.MessageFormat;
import java.util.Objects;

import org.slf4j.*;
import org.springframework.stereotype.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;

/**
 * Chargeur depuis un store
 */
@Component
public class Loader implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -8400826670928153833L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(Loader.class);

   /**
    * Lire les données en cache depuis parquet.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param session Session spark.
    * @param store Chemin d'accès vers le fichier store à lire.
    * @return Dataset lu, ou null s'il n'y a pas de cache.
    */
   public static Dataset<Row> loadFromStoreParquet(OptionsCreationLecture optionsCreationLecture, SparkSession session, String store) {
      if (optionsCreationLecture.useCache() == false) {
         LOGGER.info("La lecture du store {} est demandée, mais avec useCache = false il ne sera pas lu et va renvoyer null.", store);
         return null;
      }

      if (new File(store).exists()) {
         LOGGER.info("Le cache est utilisé pour la lecture depuis le fichier parquet {}.", store);

         try {
            StructType schema = session.read().parquet(store).schema();
            return session.read().schema(schema).parquet(store);
         }
         catch(Exception e) {
            // Devrait être une AnalysisException, mais non catchable ?
            String fmt = "La lecture du fichier parquet {0} a échoué : {1}.";
            String message = MessageFormat.format(fmt, store, e.getMessage());

            LOGGER.error(message);
            throw new RuntimeException(message, e);
         }
      }

      LOGGER.info("Il n'existe pas encore de store {}.", store);
      return null;
   }

   /**
    * Lire un objet sérialisé.
    * @param <T> Classe de l'objet cible.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param classe Classe de l'objet cible.
    * @param store Chemin d'accès vers le fichier store à lire.
    * @return Objet cible, ou null s'il n'y a pas de cache pour cet objet.
    */
   public static <T> T loadFromStoreSerialise(OptionsCreationLecture optionsCreationLecture, Class<T> classe, String store) {
      Objects.requireNonNull(classe, "La classe de l'objet à lire ne peut pas valoir null.");

      if (optionsCreationLecture.useCache() && new File(store).exists()) {
         LOGGER.info("Le cache est utilisé pour la lecture depuis le fichier sérialisé {}.", store);

         try(FileInputStream fis = new FileInputStream(store); ObjectInputStream ois = new ObjectInputStream(fis)) {
            Object o = ois.readObject();

            if (classe.isAssignableFrom(o.getClass()) == false) {
               String message = MessageFormat.format("Le fichier sérialisé ''{0}'' contient une classe {1} et non pas celle {2} attendue.", store, o.getClass().getName(), classe.getName());
               LOGGER.error(message);
               throw new ClassCastException(message);
            }

            @SuppressWarnings("unchecked")
            T cible = (T)o;
            return cible;
         }
         catch(FileNotFoundException e) {
            // Le fichier que nous venons de détecter n'est plus présent : c'est un bug de l'application.
            String fmt = "Le fichier de stockage {0} qui avait été détecté n''a pas été retrouvé : {1}.";
            String message = MessageFormat.format(fmt, store, e.getMessage());

            LOGGER.error(message);
            throw new RuntimeException(message, e);
         }
         catch(IOException e) {
            // Le fichier que nous cherchons est corrompu, d'une autre version. Il est considéré comme non advenu.
            String fmt = "Le fichier de stockage {0} ne contient pas une version valide de l''objet {1}. Il est corrompu et abandonné : {2}.";
            String message = MessageFormat.format(fmt, store, classe.getName(), e.getMessage());

            LOGGER.warn(message);
            return null;
         }
         catch(ClassNotFoundException e) {
            // Le fichier de sérialisation évoque une classe que nous ne connaissons plus.
            // C'est une autre forme de bug, mais l'on peut l'ignorer en considérant le fichier comme non advenu.
            String fmt = "Le fichier de stockage {0} déclare contenir une classe {1} inconnue de l''application. Il est abandonné.";
            String message = MessageFormat.format(fmt, store, e.getMessage());

            LOGGER.warn(message);
            return null;
         }
      }

      return null;
   }

   /**
    * Sauvergarder les données dans un cache serialisé (.ser), si le paramétrage l'autorise.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param o Objet à sérialiser.
    * @param store Chemin d'accès vers le fichier store à écrire.
    * @return true, si le cache a été modifié<br>
    * false si le paramétrage l'interdisait : (propriété : 'use.cache').
    */
   public static boolean saveSerialisationToStore(OptionsCreationLecture optionsCreationLecture, Serializable o, String store) {
      Objects.requireNonNull(o, "L'objet à sérialiser ne peut pas valoir null.");

      if (optionsCreationLecture.remplacerStore()) {

         try(FileOutputStream fos = new FileOutputStream(store); ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(o);
            oos.flush();
         }
         catch(FileNotFoundException e) {
            String fmt = "Le fichier de stockage {0} ne peut pas être créé (disque protégé, répertoire ayant ce nom...) : {1}.";
            String message = MessageFormat.format(fmt, store, e.getMessage());

            LOGGER.error(message);
            throw new RuntimeException(message, e);
         }
         catch(IOException e) {
            // Le fichier que nous cherchons est corrompu, d'une autre version. Il est considéré comme non advenu.
            String fmt = "Incident d''écriture du fichier de stockage {0} : {1}.";
            String message = MessageFormat.format(fmt, store, e.getMessage());

            LOGGER.error(message);
            throw new RuntimeException(message, e);
         }

         return true;
      }

      return false;
   }

   /**
    * Sauvergarder les données d'un dataset dans un cache Parquet, si le paramétrage l'autorise.
    * @param <T> Type du dataset.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param ds Dataset dont le contenu doit être écrit dans parquet.
    * @param colonnesPartionnement Colonnes de partitionnement. S'il y en a, le fichier parquet sera partitionné avec, à l'écriture.
    * @param store Chemin d'accès vers le fichier store à écrire.
    * @return true, si le cache a été modifié<br>
    * false si le paramétrage l'interdisait : (propriété : 'use.cache').
    */
   public static <T> boolean saveDatasetToStore(OptionsCreationLecture optionsCreationLecture, Dataset<T> ds, String[] colonnesPartionnement, String store) {
      if (ds.isEmpty() == false) {
         DataFrameWriter<T> dfw = ds.write();

         if (optionsCreationLecture.remplacerStore()) {
            dfw = dfw.mode("overwrite");
         }

         if (colonnesPartionnement != null && colonnesPartionnement.length > 0) {
            dfw.partitionBy(colonnesPartionnement).parquet(store);
         }
         else {
            dfw.parquet(store);
         }

         LOGGER.info("Un dataset a été {} dans le fichier parquet {}.", optionsCreationLecture.remplacerStore() ? "créé ou remplacé" : "créé", store);
         return true;
      }

      LOGGER.warn("Le dataset est vide et n'a pas été sauvegardé dans un fichier parquet.");
      return false;
   }
}
