package fr.ecoemploi.adapters.outbound.spark.dataset.core.verifications;

import static org.apache.spark.sql.types.DataTypes.*;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.apache.commons.lang3.builder.*;
import org.slf4j.*;

import org.apache.spark.SparkContext;
import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;
import org.apache.spark.util.LongAccumulator;

/**
 * Vérificateur de contenu de Dataset. 
 * @author Marc Le Bihan
 */
public class DatasetChecker implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -748305755612397397L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(DatasetChecker.class);

   /**
    * Erreur détectée sur une colonne de dataset.
    * @author Marc Le Bihan
    */
   static class Erreur {
      /** Nom du champ en cause. */
      private final String nomChamp;
      
      /** Message d'erreur observé. */
      private final String message;
      
      /** Nombre d'occurence de cette erreur. */
      private long nombre;
      
      /** Log 10 de ce nombre. */
      private int puissance10 = -1; // Forcer la log à la première détection.
      
      /** Log 10 précédent de ce nombre. */
      private int precedentePuissance10;
      
      /**
       * Construire un accumulateur d'erreurs observées sur une colone.
       * @param nomChamp Nom du champ.
       * @param message Message d'erreur.
       */
      public Erreur(String nomChamp, String message) {
         this.nomChamp = nomChamp;
         this.message = message;
      }

      /**
       * Incrémenter le nombre d'occurence de cette erreur, et avertir quand elle franchit une puissance de dix.
       */
      public void incrementerOccurence() {
         this.nombre ++;
         this.precedentePuissance10 = this.puissance10;
         this.puissance10 = (int)Math.log10(this.nombre);
         logSiNecessaire();
      }
      
      /**
       * Afficher une log si une erreur franchit une puissance de dix (dont 10^0 = 1).
       */
      public void logSiNecessaire() {
         if (this.puissance10 != this.precedentePuissance10) {
            LOGGER.error("Plus de {} enregistrements sont défecteux par le champ {} qui rencontre l'incident : {}.", this.nombre, this.nomChamp, this.message);
         }
      }
      
      /**
       * {@inheritDoc}
       */
      @Override
      public boolean equals(Object o) {
         if (o instanceof Erreur == false) {
            return false;
         }
         
         Erreur candidat = (Erreur)o;
         
         EqualsBuilder equals = new EqualsBuilder();
         equals.append(this.nomChamp, candidat.nomChamp);
         equals.append(this.message, candidat.message);
         return equals.isEquals();
      }
      
      /**
       * {@inheritDoc}
       */
      @Override
      public int hashCode() {
         HashCodeBuilder hash = new HashCodeBuilder();
         hash.append(this.nomChamp);
         hash.append(this.message);
         hash.append(this.nombre);
         hash.append(this.puissance10);
         hash.append(this.precedentePuissance10);
         return hash.toHashCode();
      }
   }

   /**
    * Vérifier l'intégralité du contenu d'un dataset.
    * @param ds Dataset.
    * @param summary true, s'il faut résumer le problème
    * <br>false s'il faut en donner les détails (avec des logs).<br>
    */
   public void check(Dataset<Row> ds, boolean summary) {
      check(ds, ds.schema(), summary);
   }
   
   /**
    * Vérifier l'intégralité du contenu d'un dataset.
    * @param ds Dataset.
    * @param schema Schema vis-à-vis duquel l'opposer.
    * @param summary true, s'il faut résumer le problème
    * <br>false s'il faut en donner les détails (avec des logs).<br>
    */
   public void check(Dataset<Row> ds, StructType schema, boolean summary) {
      ds.printSchema();

      Map<String, Erreur> erreurs = new HashMap<>();
      StructField[] fields = schema.fields();
      
      ds.foreach((ForeachFunction<Row>)row -> check(row, fields, erreurs, summary));
      
      if (erreurs.isEmpty() == false) {
         LOGGER.error("Le dataset {} présente {} erreurs de natures distinctes.", ds.getClass().getSimpleName(), erreurs.size());
      
         for(Erreur erreur : erreurs.values()) {
            LOGGER.error("champ : {}\terreur : {}\toccurrences : {}", erreur.nomChamp, erreur.message, erreur.nombre);
         }
      }
   }

   /**
    * Déterminer si le champs annoncés comme non nullables le sont réellement.
    * @param rows Dataset candidat.
    * @param eject Noms des champs à exclure du contrôle.
    * @return true si tous les champs annoncés non nullables n'ont effectivement pas de valeurs nulles.
    */
   public static boolean validNonNullable(Dataset<Row> rows, String... eject) {
      Map<String, Double> tauxNullsParChamp = DatasetChecker.tauxNullsParChamp(rows, false);
      boolean valide = true;

      for(StructField champ : rows.schema().fields()) {
         // Si le champ est parmi ceux à éjecter, ne pas en tenir compte
         boolean exclu = false;

         for(String exclure : eject) {
            if (champ.name().equals(exclure)) {
               exclu = true;
               break;
            }
         }

         if (exclu) {
            continue;
         }

         Double taux = tauxNullsParChamp.get(champ.name());

         if (champ.nullable() == false && taux.equals(0.0) == false) {
            LOGGER.error("Le champ {} est marqué dans le dataset comme nullable, mais il a un taux de {} de données nulles.", champ.name(), taux);
            valide = false;
         }
         else {
            LOGGER.error("Le champ {} est {}, et a un taux de {}.", champ.name(), champ.nullable() ? "nullable" : "non nullable", taux);
         }
      }

      return valide;
   }

   /**
    * Renvoyer le taux de champs null par nom de champ.
    * @param candidat Dataset à analyser.
    * @param ignorerNonNullables true pour ignorer ceux qui sont déjà marqués comme non nullables dans le schéma.
    * @return Map de champs avec leur taux
    */
   public static Map<String, Double> tauxNullsParChamp(Dataset<Row> candidat, boolean ignorerNonNullables) {
      long count = candidat.count();
      Map<String, Double> tauxDeChampsNulls = new LinkedHashMap<>();

      if (count == 0L) {
         LOGGER.warn("Le dataset soumis pour comptage de champs nuls n'a aucun enregistrement : le comptage ne peut avoir lieu.");
         return tauxDeChampsNulls;
      }

      LOGGER.info("Il y a {} enregistrements dans ce Dataset.", count);
      StructField[] fields = candidat.schema().fields();

      SparkContext ctx = candidat.sparkSession().sparkContext();
      Map<String, LongAccumulator> accumulateurs = new HashMap<>();

      for(StructField field : fields) {
         accumulateurs.put(field.name(), ctx.longAccumulator(field.name()));
      }

      candidat.foreach((ForeachFunction<Row>)row -> {
         for(int index = 0; index < fields.length; index ++) {
            StructField field = fields[index];

            // Si le champ est nullable d'après le schéma et qu'on nous demande de l'ignorer, passer au suivant.
            if (ignorerNonNullables && field.nullable()) {
               continue;
            }

            // Détecter un champ null et le compter.
            if (row.isNullAt(index)) {
               accumulateurs.get(field.name()).add(1L);
            }
         }
      });

      // Convertir les nombres de champs nulls en pourcentages

      for(StructField champ : fields) {
         Long nombreDeNulsPourLeChamp = accumulateurs.get(champ.name()).value();
         tauxDeChampsNulls.put(champ.name(), (double)nombreDeNulsPourLeChamp / count);
      }

      // Trier par taux de nullité et ordre alphabétique
       tauxDeChampsNulls = tauxDeChampsNulls.entrySet().stream().sorted((a, b) -> {
         if (Objects.equals(a.getValue(), b.getValue()) == false) {
            return Double.compare(a.getValue(), b.getValue());
         }
         else {
            return a.getKey().compareTo(b.getKey());
         }
      }

      ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (x, y) -> y, LinkedHashMap::new));

      tauxDeChampsNulls.forEach((key, value) -> LOGGER.info("{} : {}", key, value));
      return tauxDeChampsNulls;
   }

   /**
    * Vérifier l'intégralité du contenu d'un dataset.
    * @param row Enregistrement à contrôler.
    * @param fields Dessin de la structure.
    * @param erreurs Liste des erreurs observées.
    * @param summary true, s'il faut résumer le problème
    * <br>false s'il faut en donner les détails (avec des logs).<br>
    * @return true Si l'enregistrement est valide d'après son schéma.
    */
   protected boolean check(Row row, StructField[] fields, Map<String, Erreur> erreurs, boolean summary) {
      boolean statut = true;
      
      try {
         for(int index = 0; index < fields.length; index ++) {
            StructField field = fields[index];
            DataType dataType = field.dataType();

            statut = checkField(row, field, erreurs, dataType, summary, index) && statut;
         }
      }
      catch(RuntimeException e) {
         if (summary) {
            ajouterErreur("row", "enregistrement entièrement invalide", erreurs);
         }
         else {
            String format = "Enregistrement dataset invalide : {0}. Contenu du row : \n{1}";
            String message = MessageFormat.format(format, e.getMessage(), row.mkString());
            LOGGER.error(message);
         }
         
         statut = false;
      }
      
      if (statut == false && summary == false) {
         LOGGER.info("\nStructure et valeurs lues : ");
         LOGGER.info("Index\tNom du champ\tType escompté\tValeur du champ");
         
         for(int i = 0; i < fields.length; i ++) {
            StructField f = fields[i];
            LOGGER.info("{}\t{}\t{}\t{}", i, f.name(), f.dataType(), row.get(i));
         }
      }
      
      return statut;
   }

   /**
    * Vérifier le type et le contenu d'un champ du dataset.
    * @param row Enregistrement à contrôler.
    * @param field Champ à vérifier.
    * @param erreurs Liste des erreurs observées.
    * @param summary true, s'il faut résumer le problème
    * <br>false s'il faut en donner les détails (avec des logs).<br>
    * @param index Index du champ dans le dataset.
    * @param dataType Type du champ.
    * @return true Si le champ est valide d'après son schéma.
    */
   private boolean checkField(Row row, StructField field, Map<String, Erreur> erreurs, DataType dataType, boolean summary, int index) {
      try {
         // Si le champ peut être null et vaut null, passer au suivant.
         if (field.nullable() && row.isNullAt(index)) {
            return true;
         }

         // S'il ne le peut pas, mais qu'il l'est, c'est une anomalie.
         if (field.nullable() == false && row.isNullAt(index)) {
            if (summary) {
               ajouterErreur(field.name(), "null alors qu'interdit", erreurs);
            }
            else {
               String format = "Enregistrement dataset invalide : champ {0} null alors qu''interdit. Contenu du row : \n{1}";
               String message = MessageFormat.format(format, field.name(), row.mkString());
               LOGGER.error(message);
            }

            return false;
         }

         // Sinon, il doit être du type attendu.
         /*
          * Validation d'un champ à réaliser, sous condition.
          * @param dataTypeCondition Condition que doit respecter le type du champ, pour que le test soit applicable.
          * @param datasetGetter Mise en application du test, par appel d'un getter sur l'enregistrement extrait du dataset.
          */
         record ValidationChamp(Predicate<DataType> dataTypeCondition, Supplier<?> datasetGetter) {
         }

         ValidationChamp[] validationsChamps = {
            new ValidationChamp(datatype -> dataType instanceof StringType, () -> row.getString(index)),
            new ValidationChamp(datatype -> dataType instanceof BinaryType, () -> row.get(index)),
            new ValidationChamp(datatype -> dataType instanceof BooleanType, () -> row.getBoolean(index)),
            new ValidationChamp(datatype -> dataType instanceof NullType, () -> row.get(index)),

            new ValidationChamp(datatype -> dataType instanceof ByteType, () -> row.getByte(index)),
            new ValidationChamp(datatype -> dataType instanceof DecimalType, () -> row.getDecimal(index)),
            new ValidationChamp(datatype -> dataType instanceof DoubleType, () -> row.getDouble(index)),
            new ValidationChamp(datatype -> dataType instanceof FloatType, () -> row.getFloat(index)),
            new ValidationChamp(datatype -> dataType instanceof IntegerType, () -> row.getInt(index)),
            new ValidationChamp(datatype -> dataType instanceof LongType, () -> row.getLong(index)),
            new ValidationChamp(datatype -> dataType instanceof ShortType, () -> row.getShort(index)),

            new ValidationChamp(datatype -> dataType instanceof DateType, () -> row.getDate(index)),
            new ValidationChamp(datatype -> dataType instanceof CalendarIntervalType, () -> row.get(index)),
            new ValidationChamp(datatype -> dataType instanceof TimestampType, () -> row.getTimestamp(index)),

            new ValidationChamp(datatype -> dataType instanceof ArrayType, () -> row.get(index)),
            new ValidationChamp(datatype -> dataType instanceof MapType, () -> row.getMap(index)),
            new ValidationChamp(datatype -> dataType instanceof StructType, () -> row.getStruct(index)),
            new ValidationChamp(datatype -> dataType instanceof ObjectType, () -> row.get(index)),
         };

         for(ValidationChamp validationChamp : validationsChamps) {
            // Exécution des validateurs jusqu'à ce que l'un s'applique, et réussisse (ou non) son test.
            if (test(validationChamp.dataTypeCondition.test(dataType), validationChamp.datasetGetter)) {
               break;
            }
         }

         return true;
      }
      catch(ClassCastException e) {
         if (summary) {
            String message = MessageFormat.format("type observé : {0}, type attendu : {1}", e.getMessage(), field.dataType());
            ajouterErreur(field.name(), message, erreurs);
         }
         else {
            String format = "Problème de typage de champ : le champ {0} et de valeur {1} et déclaré du type {4} n''est pas du type attendu : {2}. Contenu du row : \n{3}";
            String message = MessageFormat.format(format, field.name(), row.get(index), e.getMessage(), row.mkString(), field.dataType());
            LOGGER.error(message);
         }

         return false;
      }
   }

   /**
    * Réalise un test.
    * @param condition Condition à vérifier pour lancer le test.
    * @param datasetGetter Getter à appeler, du dataset Spark.
    * @return true si le test a été exécuté.
    */
   private boolean test(boolean condition, Supplier<?> datasetGetter) {
      if (condition)
         datasetGetter.get();

      return condition;
   }

   /**
    * Ajouter une erreur à l'accumulateur des erreurs observées.
    * @param nomChamp Nom du champ en cause.
    * @param message Message d'erreur.
    * @param erreurs Erreurs accumulées.
    */
   private void ajouterErreur(String nomChamp, String message, Map<String, Erreur> erreurs) {
      Erreur erreur = erreurs.computeIfAbsent(nomChamp, clef -> new Erreur(clef, message));
      erreur.incrementerOccurence();
   }

   /**
    * Renvoyer le schéma du Dataset des erreurs observées.
    * @return Schema.
    */
   public StructType schema() {
      StructType schema = new StructType();
      
      schema = schema 
         .add("nomChamp", StringType, false)
         .add("message", StringType, false);
      
      return schema;
   }
}
