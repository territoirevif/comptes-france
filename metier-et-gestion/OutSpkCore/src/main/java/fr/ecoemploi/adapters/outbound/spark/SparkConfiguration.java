package fr.ecoemploi.adapters.outbound.spark;

import org.apache.spark.*;
import org.apache.spark.api.java.*;
import org.apache.spark.sql.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.*;
import org.springframework.context.support.*;
import scala.Option;

/**
 * Configuration de Spark
 * @author Marc LE BIHAN
 */
@Configuration
public class SparkConfiguration {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(SparkConfiguration.class);

   /** Emplacement de spark. */
   @Value("${spark.home}")
   private String sparkHome;
   
   /** Nom de l'application Spark. */
   @Value("${spark.app.name:Comptes France}")
   private String sparkAppName;

   /** URL des services. */
   @Value("${master.uri:local}")
   private String masterUri;
   
   /** Répertoire temporaire. */
   @Value("${temp.dir}")
   private String tempDir;
   
   /** Mémoire allouée au driver Spark. */
   @Value("${spark.driver.memory}")
   private String driverMemory;
   
   /** Mémoire allouée aux executors Spark. */
   @Value("${spark.executor.memory}")
   private String executorMemory;

   /** Taille maximale d'un résultat sérialisé. */
   @Value("${spark.driver.maxResultSize}")
   private String maxResultSize;

   /** Accepter le MapType dans les Dataset. */
   @Value("${spark.sql.legacy.allowHashOnMapType}")
   private String allowHashOnMapType;
   
   /** Autoriser les java.time.* au lieu des java.sql.* pour les dates et heures. */
   @Value("${spark.sql.datetime.java8API.enabled:true}")
   private String java8APIenabled;

   /** Gestion des calendar time dans les fichiers parquet. */
   @Value("${spark.sql.parquet.datetimeRebaseModeInWrite}")
   private String datetimeRebaseModeInWrite;

   /** Autorise Spark à réduire le nombre de partitions demandé. */
   @Value("${spark.sql.adaptive.coalescePartitions.enabled:true}")
   private String coalescePartitions;

   /** Autorise Spark à adapter les requêtes. */
   @Value("${spark.sql.adaptive.enabled:true}")
   private String adaptive;

   /** Logger la configuration. */
   @Value("${spark.logConf:true}")
   private String logConf;

   /**
    * Renvoyer la configuration Spark.
    * @return Configuration;
    */
   @Bean
   public SparkConf sparkConf() {
      LOGGER.info("Configuration de Spark (Bean prêt).");

      return new SparkConf()
         .setAppName(this.sparkAppName)
         
         .setSparkHome(this.sparkHome)
         .setMaster(this.masterUri)
         
         .set("spark.driver.memory", this.driverMemory)
         .set("spark.executor.memory", this.executorMemory)
         .set("spark.driver.maxResultSize", this.maxResultSize)

         .set("spark.sql.legacy.allowHashOnMapType", this.allowHashOnMapType)
         .set("spark.sql.datetime.java8API.enabled", this.java8APIenabled)
         .set("spark.sql.parquet.datetimeRebaseModeInWrite", this.datetimeRebaseModeInWrite)
         .set("spark.sql.adaptive.coalescePartitions.enabled", this.coalescePartitions)
         .set("spark.sql.adaptive.enabled", this.adaptive)

         .set("spark.local.dir", this.tempDir)
         .set("java.io.tmpdir", this.tempDir)
         .set("spark.executor.extraJavaOptions", "-Djava.io.tmpdir=" + this.tempDir)
         .set("spark.driver.extraJavaOptions", "-Djava.io.tmpdir=" + this.tempDir)
         .set("hadoop.tmp.dir", this.tempDir)
      
         .set("spark.logConf", this.logConf);
   }

   private static int count = 0;

   /**
    * Obtenir une session Spark.
    * @return Session Spark.
    */
   @Bean @SuppressWarnings("resource") // Spark se chargera de fermer le contexte Java.
   public SparkSession sparkSession() {
      Option<SparkSession> option = SparkSession.getActiveSession();

      if (option.isDefined()) {
         return option.get();
      }

      count++; //NOSONAR
      LOGGER.warn("Instantiation du contexte Spark n°{}", count);

      SparkContext sc = new JavaSparkContext(sparkConf()).sc();
      return SparkSession.builder().sparkContext(sc).getOrCreate();
      // FIXME provoque un class cast exception depuis Spark 3.3.0 ou Java 17
      //sc.sparkContext().setLogLevel("WARN");
   }

   /**
    * Sources de propriétés.
    * @return Sources de propriétés.
    */
   @Bean
   public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
      return new PropertySourcesPlaceholderConfigurer();
   }
}
