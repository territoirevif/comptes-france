package fr.ecoemploi.adapters.outbound.spark.dataset.core.verifications;

import java.io.*;
import java.util.*;

import org.slf4j.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.verifications.Verification.*;

/**
 * Vérificateur de dataset.
 */
public class Verificateur implements Serializable {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 5343865907233167430L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(Verificateur.class);

   /**
    * S'assurer que les champs souhaités sont bien dans le dataset.
    * @param schema Schéma de dataset à contrôler.
    * @param fields Champs désirés.
    * @return Liste des champs manquants.
    */
   public List<String> champsManquants(StructType schema, String... fields) {
      List<String> champsManquants = new ArrayList<>();

      for(String field : fields) {
         if (schema.getFieldIndex(field).isEmpty()) {
            champsManquants.add(field);
         }
      }

      return champsManquants;
   }

   /**
    * Réaliser des vérifications et affichages optionnels.
    * @param etape Nom de l'étape (pour log).
    * @param ds Dataset à contrôler.
    * @param colonnesStatistiques Colonnes de groupBy pour statistiques.
    * @param otherJoin Dataset other pour analyse des rejets.
    * @param joinCondition joinCondition pour analyse des rejets.
    * @param reclamees Vérifications réclamées.
    * @param restrictions Restrictions sur les vérifications par défaut.
    */
   public void verifications(String etape, Dataset<Row> ds, String[] colonnesStatistiques, Dataset<Row> otherJoin, Column joinCondition, Verification[] reclamees, Verification... restrictions) {
      if (reclamees == null || reclamees.length == 0)
         return;

      LOGGER.debug("Etape {} : Début des vérifications", etape);

      for(Verification reclamee : reclamees) {
         // Une série de vérifications par défaut peuvent être restreintes temporairement à une sous-liste.
         boolean retenue = restrictions == null || restrictions.length == 0 || hasVerifications(reclamees, restrictions);

         if (retenue == false) {
            continue;
         }

         switch(reclamee) {
            case COMPTAGES_ET_STATISTIQUES:
               comptageEtStatistiques(etape, ds, colonnesStatistiques);
               break;

            case DETERMINER_COLONNES_NON_NULLES, DUMP_ENREGISTREMENTS_MAL_TYPES, DUMP_REJETS, VERIFIER_TYPAGE_COLONNES:
               break;

            case SHOW_SCHEMAS:
               showSchema(etape, ds);
               break;

            case SHOW_REJETS:
               showRejets(etape, ds, colonnesStatistiques, otherJoin, joinCondition, reclamees);
               break;

            case SHOW_DATASET:
               showDataset(etape, ds);
               break;
         }
      }

      LOGGER.debug("Etape {} : Fin des vérifications", etape);
   }

   /**
    * Réaliser des vérifications et affichages optionnels, focus : comptages et statistiques.
    * @param etape Nom de l'étape (pour log).
    * @param ds Dataset à contrôler.
    * @param reclamees Vérifications réclamées.
    * @param restrictions Restrictions sur les vérifications par défaut.
    */
   public void verifications(String etape, Dataset<Row> ds, Verification[] reclamees, Verification... restrictions) {
      verifications(etape, ds, null, null, null, reclamees, restrictions);
   }

   /**
    * Réaliser des vérifications et affichages optionnels, focus : anti-join.
    * @param etape Nom de l'étape (pour log).
    * @param ds Dataset à contrôler.
    * @param colonnesStatistiques Colonnes de groupBy pour statistiques.
    * @param reclamees Vérifications réclamées.
    * @param restrictions Restrictions sur les vérifications par défaut.
    */
   public void verifications(String etape, Dataset<Row> ds, String[] colonnesStatistiques, Verification[] reclamees, Verification... restrictions) {
      // Restreindre la vérification anti-join aux seules étapes SHOW_REJETS et COMPTAGES_ET_STATISTIQUES.
      List<Verification> retenues = new ArrayList<>();

      if ((restrictions == null || restrictions.length == 0 || hasVerifications(reclamees, SHOW_REJETS))) {
         retenues.add(SHOW_REJETS);
      }

      if ((restrictions == null || restrictions.length == 0 || hasVerifications(reclamees, COMPTAGES_ET_STATISTIQUES))) {
         retenues.add(COMPTAGES_ET_STATISTIQUES);
      }

      if (retenues.isEmpty()) {
         return;
      }

      verifications(etape, ds, colonnesStatistiques, null, null, reclamees, retenues.toArray(new Verification[] {}));
   }

   /**
    * Afficher des statistiques sur les colonnes.
    * @param ds Dataset.
    * @param colonnesStatistiques Colonnes à considérer pour les comptages.
    */
   public void statistiques(Dataset<Row> ds, String[] colonnesStatistiques) {
      String premiereColonne = colonnesStatistiques.length >= 1 ? colonnesStatistiques[0] : null;
      String[] colonnesSuivantes = colonnesStatistiques.length >= 2 ? Arrays.copyOfRange(colonnesStatistiques, 1, colonnesStatistiques.length) : new String[]{};

      if (premiereColonne != null) {
         ds.groupBy(premiereColonne, colonnesSuivantes).count().orderBy(premiereColonne, colonnesSuivantes).show(200, false);
      }
   }

   /**
    * Déterminer si une vérification particulière est demandée parmi celles passées à une fonction.
    * @param reclamees Vérifications réclamées par la fonction.
    * @param desirees Vérification désirée parmi celle-ci.
    * @return true si une au moins l'est.
    */
   public boolean hasVerifications(Verification[] reclamees, Verification... desirees) {
      for(Verification reclamee : reclamees) {
         for(Verification desiree :  desirees) {
            if (desiree.name().equals(reclamee.name())) {
               return true;
            }
         }
      }

      return false;
   }

   /**
    * Comptage et statistiques.
    * @param etape Nom de l'étape (pour log).
    * @param ds Dataset à contrôler.
    * @param colonnesStatistiques Colonnes de groupBy pour statistiques.
    */
   public void comptageEtStatistiques(String etape, Dataset<Row> ds, String[] colonnesStatistiques) {
      if (colonnesStatistiques == null)
         return;

      LOGGER.debug("Etape {} : Comptages et statistiques sur {}.", etape, Arrays.toString(colonnesStatistiques)); //NOSONAR
      LOGGER.debug("Etape {} : {} enregistrements.", etape, ds.count());
      statistiques(ds, colonnesStatistiques);
   }

   /**
    * Présenter les rejets.
    * @param etape Nom de l'étape (pour log).
    * @param ds Dataset à contrôler.
    * @param colonnesStatistiques Colonnes de groupBy pour statistiques.
    * @param otherJoin Dataset other pour analyse des rejets.
    * @param joinCondition joinCondition pour analyse des rejets.
    * @param reclamees Vérifications réclamées.
    */
   public void showRejets(String etape, Dataset<Row> ds, String[] colonnesStatistiques, Dataset<Row> otherJoin, Column joinCondition, Verification[] reclamees) {
      if (otherJoin == null || joinCondition == null)
         return;

      LOGGER.debug("Etape {} : Rejets. Anti-join sur {}.", etape, joinCondition);
      Dataset<Row> rejets = ds.join(otherJoin, joinCondition, "left_anti");
      rejets.show(false);

      if (hasVerifications(reclamees, Verification.COMPTAGES_ET_STATISTIQUES) && colonnesStatistiques != null) {
         LOGGER.debug("Etape {} : Comptages et statistiques sur {}", etape, Arrays.toString(colonnesStatistiques)); //NOSONAR
         LOGGER.debug("Etape {} : {} rejets.", etape, rejets.count());
         statistiques(rejets, colonnesStatistiques);
      }
   }

   /**
    * Présenter le schéma du dataset.
    * @param etape Nom de l'étape (pour log).
    * @param ds Dataset à contrôler.
    */
   public void showSchema(String etape, Dataset<Row> ds) {
      LOGGER.debug("Etape {} : schema.", etape);
      ds.printSchema();
   }

   /**
    * Présenter une partie du dataset.
    * @param etape Nom de l'étape (pour log).
    * @param ds Dataset à contrôler.
    */
   public void showDataset(String etape, Dataset<Row> ds) {
      LOGGER.debug("Etape {} : show.", etape);
      ds.show(false);
   }
}
