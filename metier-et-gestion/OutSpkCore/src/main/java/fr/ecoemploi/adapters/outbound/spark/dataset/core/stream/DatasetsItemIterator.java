package fr.ecoemploi.adapters.outbound.spark.dataset.core.stream;

import java.util.*;

import org.apache.spark.sql.Dataset;
import org.slf4j.*;

/**
 * Un iterateur sur les datasets qui n'extrait leur contenu que si c'est nécessaire.
 * @param <T> Les éléments enfants transportés par les Dataset itérés (Dataset<T>).
 */
public class DatasetsItemIterator<T> implements Iterator<T> {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(DatasetsItemIterator.class);

   /** Datasets à parcourir, et à supprimer au fil de l'eau. */
   private final Stack<Dataset<T>> datasets = new Stack<>();

   /** Objets métiers actuellement parcourus. */
   private final Stack<T> currentObjetsMetiers = new Stack<>();

   /** Page actuelle */
   private long currentPage = 0;

   /** Page maximale */
   private final long maxPage;

   /**
    * Construire un itérateur sur une liste de datasets.
    * @param datasets Datasets.
    */
   public DatasetsItemIterator(List<Dataset<T>> datasets) {
      this.datasets.addAll(datasets);
      Collections.reverse(this.datasets); // Car nous utilisons une Stack, par commodité technique, mais voulons conserver l'ordre original.
      this.maxPage = this.datasets.size();
   }

   @Override
   public boolean hasNext() {
      // Si aucun dataset n'a été encore observé, prendre le premier, s'il y en a un.
      if (this.currentObjetsMetiers.isEmpty() && nextDataset() == false) {
         return false;
      }

      // S'il reste des éléments extractibles, répondre Oui.
      if (this.currentObjetsMetiers.isEmpty() == false) {
         return true;
      }

      // Sinon, tenter de parcourir un nouveau dataset.
      return nextDataset();
   }

   @Override
   public T next() {
      if (hasNext()) {
         return this.currentObjetsMetiers.pop();
      }

      throw new NoSuchElementException("La famille de datasets n'a plus d'éléments à fournir.");
   }

   /**
    * Préparer l'itération du dataset suivant.
    * @return true s'il en existe un, et qu'il a des éléments disponibles.
    */
   private boolean nextDataset() {
      if (this.datasets.isEmpty()) {
         return false;
      }

      this.currentPage ++;
      LOGGER.info("Chargement de la page {}/{} du dataset paginé.", this.currentPage, this.maxPage);

      Dataset<T> currentDataset = this.datasets.pop();

      this.currentObjetsMetiers.addAll(currentDataset.collectAsList());
      Collections.reverse(this.currentObjetsMetiers); // Car nous utilisons une Stack, par commodité technique, mais voulons conserver l'ordre original.
      currentDataset.unpersist();

      return this.currentObjetsMetiers.isEmpty() == false;
   }
}
