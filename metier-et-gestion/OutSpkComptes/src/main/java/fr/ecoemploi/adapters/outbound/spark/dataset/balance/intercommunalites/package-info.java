/**
 * Dataset de balances des comptes intercommunaux.
 * @author Marc LE BIHAN
 */
package fr.ecoemploi.adapters.outbound.spark.dataset.balance.intercommunalites;
