/**
 * Comptes indidivuels des intercommunalités.
 * @author Marc Le Bihan
 */
package fr.ecoemploi.adapters.outbound.spark.dataset.individuel.intercommunalites;