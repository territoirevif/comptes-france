package fr.ecoemploi.adapters.outbound.spark.dataset.balance;

import java.io.*;
import java.util.Objects;

import org.apache.spark.sql.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.RowToObjetMetierInterface;
import fr.ecoemploi.domain.model.territoire.comptabilite.Compte;

/**
 * Encodeur d'objet de compte de comptabilité.
 * @author Marc Le Bihan
 */
@Component
public class CompteEncoder implements RowToObjetMetierInterface<Compte>, Serializable {
   @Serial
   private static final long serialVersionUID = -5317954310669697510L;

   @Override
   public Dataset<Compte> toDatasetObjetsMetiers(Dataset<Row> rows) {
      Objects.requireNonNull(rows, "Le dataset de comptes de communes ne peut pas valoir null.");
      return rows.as(Encoders.bean(Compte.class));
   }

   @Override
   public Compte toObjetMetier(Row row) {
      throw new UnsupportedOperationException("toObjetMetier(row -> new Compte n'est pas encore proposé pour new Compte()");
   }
}
