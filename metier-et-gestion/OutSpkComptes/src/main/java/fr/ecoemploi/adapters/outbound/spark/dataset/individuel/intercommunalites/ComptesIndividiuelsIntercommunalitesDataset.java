package fr.ecoemploi.adapters.outbound.spark.dataset.individuel.intercommunalites;

import java.io.Serial;
import java.text.*;
import java.util.*;
import java.util.function.Supplier;

import org.springframework.stereotype.*;

import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.CODE_COMMUNE;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.individuel.communes.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.RowToObjetMetierInterface;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.individuel.AbstractComptesIndividuelsDataset;

/**
 * Dataset des comptes individuels des intercommunalités.
 * @author Marc Le Bihan
 */
@Service
public class ComptesIndividiuelsIntercommunalitesDataset extends AbstractComptesIndividuelsDataset<ComptesAvecRatioStructure, ComptesAvecRatioStructure, ComptesAvecReductionVotee, ComptesAvecTauxVote, ComptesIndividuelsIntercommunalite> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -8395369822335860520L;

   /** Encodeur d'objets comptes individuels d'intercommunalité. */
   private final ComptesIndividuelsIntercommunalitesEncoder encoder;

   /**
    * Construire un dataset de comptes individuels de communes.
    * @param comptesIndividuelsRowCsvLoader Chargeur de fichier csv.
    * @param encoder Encodeur d'objets comptes individuels d'intercommunalité.
    */
   public ComptesIndividiuelsIntercommunalitesDataset(ComptesIndividuelsIntercommunalitesRowCsvLoader comptesIndividuelsRowCsvLoader, ComptesIndividuelsIntercommunalitesEncoder encoder) {
      super(comptesIndividuelsRowCsvLoader, "exer", "intercommunalite");
      this.encoder = encoder;
   }
   
   /**
    * Obtenir un Dataset Row des comptes individuels des intercommunalités.
    * @param anneeComptable Année comptable : si 0, toutes les années comptables sont rapatritées.
    * @param anneeCOG année du COG pour rectifier les communes.
    * @param optionsCreationLecture Options de constitution du dataset
    * @param historique Historique de création de dataset
    * @return Dataset des communes.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> rowComptesIntercommunaux(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeComptable, int anneeCOG) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.aucuneDeRegleValidation(this.session);
      }

      Supplier<Dataset<Row>> worker = () -> super.rowComptesIndividuels(anneeComptable);

      Column postFiltre = null;
      boolean anneeEligibleCache = true;

      return constitutionStandard(options, historique, worker, anneeEligibleCache, new ConditionPostFiltrageConstante(postFiltre),
         new CacheParqueteur<>(optionsCreationLecture, this.session, exportCSV(), "comptes-individuels-intercommunalites", "FILTRE-exercice_{0,number,#0}", new ComptesIndividuelsTriSiren(),
            anneeComptable));
   }

   /**
    * Alimenter la carte de désendettement des communes de France :<br>
    * Elles sera écrite dans une table nommmée desendettement_communes_{exercice comptable}, qui doit être présente en base de données.
    * @param exerciceComptable Exercice comptable.
    * @param anneeCOG Année de référence du Code Officiel Géographique.
    * @param optionsCreationLecture Options de constitution du dataset
    * @param historique Historique de création de dataset
    */
   public void alimenterCarteDesendettement(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int exerciceComptable, int anneeCOG) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      Dataset<Row> write = rowComptesIntercommunaux(options, historique, exerciceComptable, anneeCOG)
          .select(col("codeCommune").alias("codecommune"), col("nomCommune").alias("nomcommune"), col("capaciteDesendettementCorrigeeArrondie").alias("capaciteDesendettement"));
      
      write.write()
         .format("jdbc")
         .option("url", this.urlJDBC)
         .option("dbtable", MessageFormat.format("desendettement_communes_{0,number,#0}", exerciceComptable))
         .option("user", this.usernameJDBC)
         .option("password", this.passwordJDBC)
         .option("truncate", "true")
         .mode(SaveMode.Overwrite)
         .save();      
   }

   @Override
   protected RowToObjetMetierInterface<ComptesIndividuelsIntercommunalite> objetMetierEncoder() {
      return this.encoder;
   }
}
