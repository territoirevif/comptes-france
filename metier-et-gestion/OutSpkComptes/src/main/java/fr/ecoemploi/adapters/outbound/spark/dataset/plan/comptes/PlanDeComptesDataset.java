package fr.ecoemploi.adapters.outbound.spark.dataset.plan.comptes;

import java.io.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;
import org.apache.spark.sql.types.*;
import static org.apache.spark.sql.types.DataTypes.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;

/**
 * Dataset de lecture de plans de comptes. 
 * @author Marc Le Bihan
 */
@Service
public class PlanDeComptesDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 2615738151026466651L;

   /** Nom du fichier CSV des comptes M14. */
   @Value("${plan-de-comptes.m14.csv.nom}")
   private String nomFichierM14;
   
   /** Nom du fichier CSV des comptes M14A. */
   @Value("${plan-de-comptes.m14a.csv.nom}")
   private String nomFichierM14a;
   
   /** Nom du fichier CSV des comptes M22. */
   @Value("${plan-de-comptes.m22.csv.nom}")
   private String nomFichierM22;

   /** Nom du fichier CSV des comptes M4. */
   @Value("${plan-de-comptes.m4.csv.nom}")
   private String nomFichierM4;
   
   /** Nom du fichier CSV des comptes M41. */
   @Value("${plan-de-comptes.m41.csv.nom}")
   private String nomFichierM41;
   
   /** Nom du fichier CSV des comptes M42. */
   @Value("${plan-de-comptes.m42.csv.nom}")
   private String nomFichierM42;
   
   /** Nom du fichier CSV des comptes M43. */
   @Value("${plan-de-comptes.m43.csv.nom}")
   private String nomFichierM43;
   
   /** Nom du fichier CSV des comptes M43A. */
   @Value("${plan-de-comptes.m43a.csv.nom}")
   private String nomFichierM43a;
   
   /** Nom du fichier CSV des comptes M49. */
   @Value("${plan-de-comptes.m49.csv.nom}")
   private String nomFichierM49;
   
   /** Nom du fichier CSV des comptes M49A. */
   @Value("${plan-de-comptes.m49a.csv.nom}")
   private String nomFichierM49a;
   
   /** Nom du fichier CSV des comptes M5. */
   @Value("${plan-de-comptes.m57.csv.nom}")
   private String nomFichierM57;

   /** Chemin d'accès au répertoire des comptes. */
   @Value("${comptes.dir}")
   private String repertoireComptes;

   /**
    * Obtenir le dataset des nomenclatures comptables <br>
    * Elles sont fixes, et obtenues d'un document descriptif, accompagnant la balance des comptes.
    * @param session Session Spark.
    * @return Dataset des nomemclatures.
    */
   public Dataset<Row> rowNomenclatures(SparkSession session) {
      String[][] nomemclatures = {
         {"M14", "Communes et groupements à fiscalité propre (GFP)"},
         {"M14A", "Communes de moins de 500 habitants"},
         {"M21", "Comptabilité des établissements publics de santé"},
         {"M22", "Établissements sociaux et médico-sociaux (EPSMS)"},
         {"M4", "Services publics locaux à caractère industriel ou commercial"},
         {"M41", "Services publics locaux de production et distribution d'énergies"},
         {"M42", "Services publics locaux des abattoirs"},
         {"M43", "Services publics locaux de transport de personnes"},
         {"M43A", "Services publics de transport de personnes - Nomenclature M43 abrégée"},
         {"M44", "Établissements publics fonciers locaux"},
         {"M49", "Services publics d'assainissement et distribution d'eau potable"},
         {"M49A", "Services publics d'assainissement et distribution d'eau potable - Nomenclature M49 abrégée"},
         {"M52", "Départements"},
         {"M57", "Collectivités territoriales uniques et Métropoles"},
         {"M57A", "Collectivités territoriales uniques et Métropoles - Nomenclature M57 abrégée"},
         {"M61", "Services départementaux d'incendie et de secours (SDIS)"},
         {"M71", "Régions"},
         {"M832", "Centres départementaux de gestion de la fonction publique territoriale (CDFPT)"}
      };
      
      return referentiel().creerReferentiel(super.session, "nomemclature", "libelleNomemclature", nomemclatures)
         .cache();
   }
   
   /**
    * Ajouter les libellés du plan de compte :<br>
    * Observés dans les comptes des communes : M4, M14, M14A, M22, M42, M41, M43, M43A, M49, M49A, M57<br> 
    * Observés dans les groupements à fiscalité propres et les intercommunalité : M14, M14A, M22, M4, M41, M42, M43, M43A, M49, M49A, M57
    * @return Dataset complété des libellé de plan de compte.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> plansDeComptes() {
      Dataset<Row> planDeComptes = 
         planDeComptes(0, "M14", this.nomFichierM14)
         .union(planDeComptes(0, "M14A", this.nomFichierM14a))
         .union(planDeComptes(0, "M22", this.nomFichierM22))
         .union(planDeComptes(0, "M4", this.nomFichierM4))
         .union(planDeComptes(0, "M41", this.nomFichierM41))
         .union(planDeComptes(0, "M42", this.nomFichierM42))
         .union(planDeComptes(0, "M43", this.nomFichierM43))
         .union(planDeComptes(0, "M43A", this.nomFichierM43a))
         .union(planDeComptes(0, "M49", this.nomFichierM49))
         .union(planDeComptes(0, "M49A", this.nomFichierM49a))
         .union(planDeComptes(0, "M57", this.nomFichierM57))
         .orderBy("nomenclatureComptablePlan", "numeroComptePlan");
      
      return planDeComptes.cache();
   }
   
   /**
    * Obtenir un Dataset Row du plan de comptes M14.
    * @param anneeComptable Année pour laquelle rechercher le plan de comptes : n'est pas pris en compte.
    * @param nomenclatureComptable Nomenclature comptable.
    * @param nomFichierPlanComptable nom du fichier de plan comptable associé.
    * @return Ensemble des données attributaires accessibles sur le plan de comptes.
    */
   private Dataset<Row> planDeComptes(@SuppressWarnings("unused") int anneeComptable, String nomenclatureComptable, String nomFichierPlanComptable) {
      File repertoireParent = assertExistenceRepertoire(this.repertoireComptes);
      File source = assertExistenceFichierAnnuel(repertoireParent, 0, nomFichierPlanComptable);
      
      Dataset<Row> planDeComptePourUneNomemclature = session.read().format("csv").schema(new StructType()
         .add("numeroComptePlan", StringType, false)
         .add("libelleComptePlan", StringType, false)
         .add("nomenclatureComptablePlan", StringType, false))
         .option("header","false")
         .option("delimiter", ",")
         .option("quote", "\"")
         .option("escape", "\"")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath())
         .selectExpr("*");
      
      return planDeComptePourUneNomemclature.withColumn("nomenclatureComptablePlan", lit(nomenclatureComptable));
   }
}
