package fr.ecoemploi.adapters.outbound.spark.dataset.individuel.intercommunalites;

import java.io.Serial;

import org.apache.spark.sql.Column;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.TriDataset;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Tri de dataset : tri par code siren
 * @author Marc Le Bihan
 */
public class ComptesIndividuelsTriSiren extends TriDataset {
   @Serial
   private static final long serialVersionUID = 4813480557965990855L;

   /**
    * Constuire un tri.
    */
   public ComptesIndividuelsTriSiren() {
      super("TRI-siren", false, null, false,
         SIREN_ENTREPRISE.champ());
   }

   /**
    * Renvoyer une condition equalTo sur le siren.
    * @param siren siren. Si null, isNull() servira au test.
    * @return Colonne contenant la condition.
    */
   public Column equalTo(String siren) {
      return siren != null ? SIREN_ENTREPRISE.col().equalTo(siren) : SIREN_ENTREPRISE.col().isNull();
   }
}
