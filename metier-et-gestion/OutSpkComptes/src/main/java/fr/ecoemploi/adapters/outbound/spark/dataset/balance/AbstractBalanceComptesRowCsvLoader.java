package fr.ecoemploi.adapters.outbound.spark.dataset.balance;

import java.io.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

/**
 * Classe de base des chargeurs de CSV pour les comptes de comptabilité
 * @author Marc Le Bihan
 */
public abstract class AbstractBalanceComptesRowCsvLoader extends AbstractSparkCsvLoader {
   @Serial
   private static final long serialVersionUID = 8932738686579706491L;

   /** Nom du fichier CSV. */
   private final String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${comptes.dir}")
   protected String repertoireFichier;

   /** Session Spark. */
   @Autowired
   protected SparkSession session;

   /**
    * Construire un chargeur de comptes depuis un fichier CSV.
    * @param nomFichier Nom du fichier
    */
   protected AbstractBalanceComptesRowCsvLoader(String nomFichier) {
      this.nomFichier = nomFichier;
   }

   /**
    * Charger un Dataset Row des balances des comptes.
    * @param anneeComptable Année comptable.
    * @return Dataset des comptes des communes.
    */
   public Dataset<Row> loadOpenData(int anneeComptable) {
      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, anneeComptable, this.nomFichier);

      return rename(this.load(schema(anneeComptable), source), anneeComptable);
   }

   /**
    * Provoquer le chargement effectif de la source de données.
    * @param schema Schéma.
    */
   protected Dataset<Row> load(StructType schema, File source) {
      return this.session.read().schema(schema).format("csv")
         .option("header","true")
         .option("quote", "\"")
         .option("escape", "\"")
         .option("dec", ".")
         .option("sep", ";")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath());
   }

   /**
    * Renommer les champs du dataset.
    * @param dataset Dataset.
    */
   @Override
   public Dataset<Row> rename(Dataset<Row> dataset, int annee) {
      return dataset.withColumnRenamed("IDENT", "siret")
         .withColumnRenamed("NDEPT", "codeDepartementSur3")
         .withColumnRenamed("LBUDG", "libelleBudget")
         .withColumnRenamed("CBUDG", "typeBudget")

         .withColumnRenamed("CTYPE", "typeEtablissement")
         .withColumnRenamed("CSTYP", "sousTypeEtablissement")
         .withColumnRenamed("NOMEN", "nomenclatureComptable")
         .withColumnRenamed("SIREN", "siren")
         .withColumnRenamed("CREGI", "codeRegion")

         .withColumnRenamed("CACTI", "codeActivite")
         .withColumnRenamed("SECTEUR", "codeSecteur")
         .withColumnRenamed("FINESS", "numeroFINESS")
         .withColumnRenamed("CODBUD1", "codeBudget")
         .withColumnRenamed("CATEG", "categorieCollectivite")

         .withColumnRenamed("BAL", "typeBalance")
         .withColumnRenamed("COMPTE", "numeroCompte")
         .withColumnRenamed("BEDEB", "balanceEntreeDebit")
         .withColumnRenamed("BECRE", "balanceEntreeCredit")
         .withColumnRenamed("OBNETDEB", "operationBudgetaireDebit")

         .withColumnRenamed("OBNETCRE", "operationBudgetaireCredit")
         .withColumnRenamed("ONBDEB", "operationNonBudgetaireDebit")
         .withColumnRenamed("ONBCRE", "operationNonBudgetaireCredit")
         .withColumnRenamed("OOBDEB", "operationOrdreBudgetaireDebit")
         .withColumnRenamed("OOBCRE", "operationOrdreBudgetaireCredit")

         .withColumnRenamed("SD", "soldeDebiteur")
         .withColumnRenamed("SC", "soldeCrediteur");
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Dataset<Row> cast(Dataset<Row> dataset, int annee) {
      return dataset
         .withColumn("anneeExercice", col("EXER").cast(IntegerType))
         .drop("EXER")

         // Nouveaux champs.
         .withColumn("budgetPrincipal", col("typeBudget").eqNullSafe("1").cast(BooleanType))
         .withColumn("nombreChiffresNumeroCompte", length(col("numeroCompte")));
   }

   /**
    * Renvoyer le schéma initial du dataset
    * @param annee Année de référence
    * @return Schéma
    */
   @Override
   public StructType schema(int annee) {
      // EXER  IDENT           NDEPT LBUDG INSEE CBUDG CTYPE CSTYP NOMEN siren       CREGI CACTI       SECTEUR  FINESS   CODBUD1  CATEG    BAL
      // 2018  21410017400017  41    BINAS 17    1     101   0     M14   214100174   24    40                                     Commune  DEF
      //
      // COMPTE   BEDEB BECRE OBNETDEB OBNETCRE ONBDEB   ONBCRE   OOBDEB   OOBCRE   SD SC
      // 7788     0     0     0        1392,44  0        0        0        0        0  1392,44
      return new StructType()
         .add("EXER", StringType, false)     // EXER     Exercice de gestion         4     Texte
         .add("IDENT", StringType, false)    // IDENT    N° SIRET                    14    Texte
         .add("NDEPT", StringType, false)    // NDEPT    N° département              3     Texte   N° département précédé du chiffre "0" - Numérotation spécifique pour l' Outre Mer (cf. onglet codif données d'identification)
         .add("LBUDG", StringType, false)    // LBUDG    Libellé du budget           40    Texte
         .add("INSEE", StringType, false)    // INSEE    Code INSEE                  3     Texte

         .add("CBUDG", StringType, false)    // CBUDG    Type de budget              1     Texte   Budget principal("1") – Budget principal rattaché (CCAS, CDE…) ("2") - Budget annexe ("3")
         .add("CTYPE", StringType, false)    // CTYPE    Type d'établissement        3     Texte   Cf. onglet "Codif_données_d'identification"
         .add("CSTYP", StringType, false)    // CSTYP    Sous-type d'établissement   2     Texte   Cf. onglet "Codif_données_d'identification"
         .add("NOMEN", StringType, false)    // NOMEN    Nomenclature comptable      9     Texte   Cf. onglet "Codif_données_d'identification"
         .add("SIREN", StringType, false)    // SIREN    N° SIREN                    9     Texte   Les 9  premiers caractères du n° SIRET ; Le SIREN d'un budget principal X est identique au SIREN du/ou des Budget(s) annexe(s) du Budget principal X ; SIREN BPx = SIREN BA du BPx

         .add("CREGI", StringType, false)    // CREGI    Code région                 3     Texte   Cf. onglet "Code_Region_Dépt"
         .add("CACTI", StringType, true)     // CACTI    Code activité               2     Texte   Cf. onglet "Codif_données_d'identification"
         .add("SECTEUR", StringType, true)   // SECTEUR  Code secteur                1     Texte   Code 4 : Budgets annexes sociaux et médico-sociaux rattachés à une collectivité territoriale, EPCI ou EPL
         .add("FINESS", StringType, true)    // FINESS   N° FINESS                   9     Texte   Pour les budgets annexes sociaux et médico-sociaux rattachés à une collectivité territoriale, EPCI ou EPL
         .add("CODBUD1", StringType, true)   // CODBUD1  Code budget                 1     Texte   Pour les budgets annexes sociaux et médico-sociaux rattachés à une collectivité territoriale, EPCI ou EPL

         .add("CATEG", StringType, false)    // CATEG    Catégorie de collectivité   7     Texte   Commune  - CTU - DEPT -  EPL  - EPT -  - GFP -  ML - SYND - REG -                Cf. onglet "Codif_données_d'identification"
         .add("BAL", StringType, false)      // BAL      Type de balance             4     Texte   DEF : Définitive
         .add("COMPTE", StringType, false)   // COMPTE   N° du compte                8     Texte
         .add("BEDEB", DoubleType, false)    // BEDEB    Balance d'entrée débit      8     Numérique
         .add("BECRE", DoubleType, false)    // BECRE    Balance d'entrée crédit     8     Numérique

         .add("OBNETDEB", DoubleType, false) // OBNETDEB Opération budgétaire débit nette des annulations   8     Numérique   Opérations d'ordre budgétaires comprises
         .add("OBNETCRE", DoubleType, false) // OBNETCRE Opération budgétaire crédit nette des annulations  8     Numérique   Opérations d'ordre budgétaires comprises
         .add("ONBDEB", DoubleType, false)   // ONBDEB   Opération Non Budgétaire Débit (1)        8     Numérique
         .add("ONBCRE", DoubleType, false)   // ONBCRE   Opération Non Budgétaire Crédit (1)       8     Numérique
         .add("OOBDEB", DoubleType, false)   // OOBDEB   Opération d'Ordre Budgétaire Débit (2)    8     Numérique   Distinction des opérations d'ordre budgétaire

         .add("OOBCRE", DoubleType, false)   // OOBCRE   Opération d'Ordre Budgétaire Crédit (2)   8     Numérique   Distinction des opérations d'ordre budgétaire
         .add("SD", DoubleType, false)       // SD       Solde débiteur              8     Numérique     max(0,sum(BEDEB,-BECRE,OBNETDEB,-OBNETCRE,ONBDEB,-ONBCRE))
         .add("SC", DoubleType, false);      // SC       Solde créditeur             8     Numérique     max(0,sum(BECRE,-BEDEB,OBNETCRE,-OBNETDEB,ONBCRE,-ONBDEB))
   }
}
