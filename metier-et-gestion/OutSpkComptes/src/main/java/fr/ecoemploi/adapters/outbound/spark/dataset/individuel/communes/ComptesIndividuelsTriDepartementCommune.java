package fr.ecoemploi.adapters.outbound.spark.dataset.individuel.communes;

import java.io.Serial;

import org.apache.spark.sql.Column;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.TriDataset;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Tri de dataset : partitionnement par département, puis tri par commune
 * @author Marc Le Bihan
 */
public class ComptesIndividuelsTriDepartementCommune extends TriDataset {
   @Serial
   private static final long serialVersionUID = -9115769308031652920L;

   /**
    * Constuire un tri.
    */
   public ComptesIndividuelsTriDepartementCommune() {
      super("PARTITION-departement-TRI-commune", true, CODE_DEPARTEMENT.champ(), false,
         CODE_DEPARTEMENT.champ(), CODE_COMMUNE.champ());
   }

   /**
    * Renvoyer une condition equalTo sur le code département et le code commune.
    * @param codeDepartement Code département. Si null, isNull() servira au test.
    * @param codeCommune Code commune. Si null, isNull() servira au test.
    * @return Colonne contenant la condition.
    */
   public Column equalTo(String codeDepartement, String codeCommune) {
      return equalTo(codeDepartement)
         .and(codeCommune != null ? CODE_COMMUNE.col().equalTo(codeCommune) : CODE_COMMUNE.col().isNull());
   }

   /**
    * Renvoyer une condition equalTo sur le code département.
    * @param codeDepartement Code département. Si null, isNull() servira au test.
    * @return Colonne contenant la condition.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
