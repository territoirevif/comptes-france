/**
 * Comptes individuels des communes. 
 * @author Marc Le Bihan
 */
package fr.ecoemploi.adapters.outbound.spark.dataset.individuel.communes;