package fr.ecoemploi.adapters.outbound.spark.dataset.balance;

import static org.apache.spark.sql.functions.*;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.function.Supplier;

import org.slf4j.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.groupement.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.Compte;
import fr.ecoemploi.service.*;
import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.plan.comptes.PlanDeComptesDataset;

/**
 * Dataset de la balance des comptes de collectivités locales.
 * @author Marc Le Bihan
 */
@Service
public abstract class AbstractBalanceComptesDataset extends AbstractSparkObjetMetierDataset<Compte> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5085209099469324333L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AbstractBalanceComptesDataset.class);
   
   /** Dataset des SIREN communaux. */
   @Autowired
   private SirenCommunesDataset datasetSirenCommunaux;

   /** Dataset des périmètres d'EPCI. */
   @Autowired
   private EPCIPerimetreDataset groupementsDataset;

   /** Dataset des plans de comptes. */
   @Autowired
   private PlanDeComptesDataset planDeComptesDataset;

   /** Chargeur de CSV. */
   private final AbstractBalanceComptesRowCsvLoader rowCsvLoader;

   /** Validateur de Row. */
   private final AbstractCompteRowValidator rowValidator;

   /** Encodeur d'objet métier. */
   private final CompteEncoder encoder;

   /** Indique si le chargement et la transmission des groupements est requise. */
   private final boolean chargerGroupements;

   /**
    * Construire un dataset de lecture des balances des comptes
    * @param rowCsvLoader Chargeur de CSV en Row à utiliser
    * @param rowValidator Validateur de Row
    * @param encoder Encodeur d'objet métier depuis un Row
    * @param chargerGroupements true si le chargement et la transmission des groupements est requise
    */
   protected AbstractBalanceComptesDataset(AbstractBalanceComptesRowCsvLoader rowCsvLoader, AbstractCompteRowValidator rowValidator, CompteEncoder encoder,
       boolean chargerGroupements) {
      this.rowCsvLoader = rowCsvLoader;
      this.rowValidator = rowValidator;
      this.encoder = encoder;
      this.chargerGroupements = chargerGroupements;
   }

   /**
    * Obtenir un Dataset Row des balances des comptes des communes.
    * @param optionsCreationLecture Options d'écriture et de lecture du dataset.
    * @param historique Historique d'exécution
    * @param tri Tri et partitionnement du dataset
    * @param nomBaseStore Nom de base du fichier store à créer exemple : comptes-balance-communes
    * @param anneeComptable Année comptable.
    * @param anneeCOG année du COG pour rectifier les communes.
    * @return Dataset des comptes de communes.
    */
   protected Dataset<Row> rowComptes(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, TriDataset tri,
      String nomBaseStore,
      int anneeComptable, int anneeCOG) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.addReglesValidation(this.session, this.rowValidator);
      }

      Supplier<Dataset<Row>> worker = () -> {
         Dataset<Row> comptes = this.rowCsvLoader.loadOpenData(anneeComptable);

         // Obtention des SIREN des communes d'après le COG.
         Dataset<Row> sirenCommunes = cogSurTroisAnnees(options, historique, anneeCOG).cache();

         // Renommer les champs, puis ne retienir que l'excercice comptable demandé.
         comptes = renommerEtCreerChamps(options, historique, comptes, anneeComptable)
            .where(col("anneeExercice").equalTo(anneeComptable));

         comptes = comptes.repartition(col("codeDepartement"));

         Dataset<Row> groupements = null;

         if (this.chargerGroupements) {
            groupements = this.groupementsDataset.rowPerimetres(options, historique, anneeCOG, true, new EPCIPerimetreTriSirenSiegeCommuneMembreSirenMembre());
         }

         comptes = comptes.filter((FilterFunction<Row>)compte -> this.rowValidator.validerCompte(historique, compte));

         // Ajouter les libellés du plan de compte.
         comptes = appliquerPlansDeComptes(comptes);

         // Restreindre au COG.
         comptes = appliquerCOG(anneeCOG, sirenCommunes, comptes, groupements);

         // Exécuter les cumuls de comptes parents, pour les mettre en regard de chaque compte.
         comptes = cumulsSousComptes(comptes);

         // Partitionner par département.
         comptes = comptes.repartition(col("codeDepartement"));

         String[] avant = comptes.columns();

         comptes = comptes.select("anneeExercice", CODE_REGION.champ(), CODE_DEPARTEMENT.champ(), CODE_COMMUNE.champ(),
            NOM_COMMUNE.champ(), SIREN_ENTREPRISE.champ(), SIRET_ETABLISSEMENT.champ(), "budgetPrincipal", "libelleBudget",
            NUMERO_COMPTE.champ(), "libelleCompte", "nomenclatureComptable", "soldeDebiteur", "soldeCrediteur",
            TYPE_ETABLISSEMENT.champ(), "libelleTypeEtablissement", "natureEtablissement", SOUS_TYPE_ETABLISSEMENT.champ(),
            "libelleSousTypeEtablissement", CODE_ACTIVITE.champ(), "libelleActivite", "codeSecteur", "numeroFINESS",
            TYPE_BUDGET.champ(), "libelleTypeBudget", "codeBudget",

            "categorieCollectivite", "populationMunicipale", "typeBalance", "balanceEntreeDebit", "balanceEntreeCredit",
            "operationBudgetaireDebit", "operationBudgetaireCredit", "operationNonBudgetaireDebit", "operationNonBudgetaireCredit",
            "operationOrdreBudgetaireDebit", "operationOrdreBudgetaireCredit", "nombreChiffresNumeroCompte",
            "cumulSD3", "cumulSC3", "cumulSD4", "cumulSC4", "cumulSD5", "cumulSC5", "cumulSD6", "cumulSC6"
         );

         champsManquantsSelect(avant, comptes.columns());
         return comptes;
      };

      Column postFiltre = options.hasRestrictionAuxCommunes() ? CODE_COMMUNE.col().isin(options.restrictionAuxCommunes().toArray()) : null;
      boolean anneeEligibleCache = options.anneeEligibleMiseEnCache(anneeComptable);

      return constitutionStandard(options, historique, worker, anneeEligibleCache, new ConditionPostFiltrageConstante(postFiltre),
         new CacheParqueteur<>(options, this.session, exportCSV(),
            nomBaseStore, "FILTRE-annee_{0,number,#0}-cog-{1,number,#0}", tri, anneeComptable, anneeCOG));
   }

   /**
    * Renvoyer la liste des champs que le select a oublié de reprendre.
    * @param avant Colonnes qui existaient avant le select.
    * @param apres Colonnes, une fois réorganisées.
    */
   private void champsManquantsSelect(String[] avant, String[] apres) {
      if (avant.length != apres.length) {
         List<String> champsManquants = new ArrayList<>(Arrays.asList(avant));
         champsManquants.removeAll(Arrays.asList(apres));

         LOGGER.error("La dataset de la balance des comptes a vu un select de réorganisation de l'ordre des colonnes le faire passer de {} à {} champs : {} manquent.", avant.length, apres.length, champsManquants);
      }
   }
   
   /**
    * Join deux COG : le COG demandé, et celui de n-1 et n-2, en mineur.
    * @param anneeCOG année du COG.
    * @return COG n, n-1, n-2 joints.
    */
   private Dataset<Row> cogSurTroisAnnees(OptionsCreationLecture options, HistoriqueExecution historique, int anneeCOG) {
      Dataset<Row> cogCourant = this.datasetSirenCommunaux.rowSirenCommunes(options, historique, new SirenCommunesTriDepartementCommuneSiren(), anneeCOG);
      Dataset<Row> cogTroisAnnees = cogCourant;
      
      try {
         Dataset<Row> cogPrecedent = this.datasetSirenCommunaux.rowSirenCommunes(options, historique, new SirenCommunesTriDepartementCommuneSiren(), anneeCOG - 1);
         Dataset<Row> delta = cogPrecedent.join(cogCourant, CODE_COMMUNE.col(cogCourant).equalTo(CODE_COMMUNE.col(cogPrecedent)), "left_anti");
         cogTroisAnnees = cogTroisAnnees.union(delta);

         try {
            Dataset<Row> cogAnte = this.datasetSirenCommunaux.rowSirenCommunes(options, historique, new SirenCommunesTriDepartementCommuneSiren(), anneeCOG - 2);
            Dataset<Row> delta2 = cogAnte.join(cogPrecedent, CODE_COMMUNE.col(cogPrecedent).equalTo(CODE_COMMUNE.col(cogAnte)), "left_anti");
            cogTroisAnnees = cogTroisAnnees.union(delta2);
         }
         catch(RepertoireInexistantException | FichierGeographiqueAbsentException e) {
            LOGGER.debug(e.getMessage());
         }
      }
      catch(RepertoireInexistantException | FichierGeographiqueAbsentException e) {
         LOGGER.debug(e.getMessage());
      }
      
      cogTroisAnnees = cogTroisAnnees.orderBy(col("sirenCommune"));
      return cogTroisAnnees;
   }
   
   /**
    * Ajouter les libellés du plan de compte.
    * @param comptes Dataset de comptes.
    * @return Dataset complété des libellé de plan de compte.
    */
   private Dataset<Row> appliquerPlansDeComptes(Dataset<Row> comptes) {
      Dataset<Row> plansDeComptes = this.planDeComptesDataset.plansDeComptes();
      
      Column jointure = NUMERO_COMPTE.col(comptes).equalTo(plansDeComptes.col("numeroComptePlan"))
         .and(comptes.col("nomenclatureComptable").equalTo(plansDeComptes.col("nomenclatureComptablePlan")));
         
      Dataset<Row> comptesAvecLibelles = comptes.join(plansDeComptes, jointure, "left_outer");

      comptesAvecLibelles = comptesAvecLibelles
         .drop(col("nomenclatureComptablePlan"))
         .drop(col("numeroComptePlan"))
         .withColumnRenamed("libelleComptePlan", "libelleCompte");

      return comptesAvecLibelles;
   }

   /**
    * Cumuler les sous-comptes.
    * @param comptes Dataset de comptes.
    * @return Dataset aux cumuls de comptes à 3, 4, 5, 6, 7 chiffres réalisés, par commune.
    */
   private Dataset<Row> cumulsSousComptes(Dataset<Row> comptes) {
      Dataset<Row> comptesAvecCumuls = comptes; 
      
      for(int nombreChiffresNiveauCompte = 3; nombreChiffresNiveauCompte < 7; nombreChiffresNiveauCompte ++) {
         comptesAvecCumuls = cumulsCompteParent(comptesAvecCumuls, nombreChiffresNiveauCompte);
      }
      
      return comptesAvecCumuls;
   }
   
   /**
    * Cumul par un niveau de compte parent.
    * @param comptes Liste des comptes.
    * @param nombreChiffres Nombre de chiffres auquel réduire le compte à cummuler. Exemple 4 : 2041582 est cumulé sur 2041. 
    * @return cumuls par compte parent : dataset au format (cumul des soldes débiteurs, cumul des soldes créditeurs).
    */
   private Dataset<Row> cumulsCompteParent(Dataset<Row> comptes, int nombreChiffres) {
      // Cumuler pour un niveau de compte parent sur le préfixe de leurs comptes réduits à nombreChiffres.
      Column nombreChiffresCompte = comptes.col("nombreChiffresNumeroCompte");
      
      String aliasNumeroCompteRacine = MessageFormat.format("numeroCompteSur{0}", nombreChiffres);
      
      String nomChampCumulSD = MessageFormat.format("cumulSD{0}", nombreChiffres);
      String nomChampCumulSC = MessageFormat.format("cumulSC{0}", nombreChiffres);
      Column sd = sum(when(nombreChiffresCompte.$greater$eq(lit(nombreChiffres)), col("soldeDebiteur")).otherwise(lit(0.0))).as(nomChampCumulSD);
      Column sc = sum(when(nombreChiffresCompte.$greater$eq(lit(nombreChiffres)), col("soldeCrediteur")).otherwise(lit(0.0))).as(nomChampCumulSC);

      Dataset<Row> comptesAvecCumuls = cumulsParentsSpecifiques(comptes, sd, sc, nombreChiffres, aliasNumeroCompteRacine);      
      
      comptesAvecCumuls = comptesAvecCumuls
         .drop(comptesAvecCumuls.col(aliasNumeroCompteRacine))
         .withColumnRenamed("cumulSD", nomChampCumulSD)
         .withColumnRenamed("cumulSC", nomChampCumulSC)
         .withColumn(nomChampCumulSD, round(col(nomChampCumulSD), 2))
         .withColumn(nomChampCumulSC, round(col(nomChampCumulSC), 2));
      
      return comptesAvecCumuls;
   }
   
   /**
    * Exécuter des cumuls de comptes parents spécifiques à la nature du fichier de balances comptables traité.
    * @param comptes Liste des comptes lus.
    * @param sd Expression de colonne à utiliser pour le traitement des soldes débit.
    * @param sc Expression de colonne à utiliser pour le traitement des soldes crédit.
    * @param nombreChiffres Nombre de chiffres à retenir pour faire le cumul (racine du compte à prendre).
    * @param aliasNumeroCompteRacine Nom alias du nom de compte racine.
    * @return Comptes avec leurs cumuls.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   protected Dataset<Row> cumulsParentsSpecifiques(Dataset<Row> comptes, Column sd, Column sc, int nombreChiffres, String aliasNumeroCompteRacine) {
      RelationalGroupedDataset group = comptes.groupBy(col("codeCommune"), col("siret"), col("numeroCompte").substr(1,nombreChiffres).as(aliasNumeroCompteRacine));
      Dataset<Row> cumuls = group.agg(sd, sc);
      
      // Associer à chaque compte la colonne de cumuls de comptes parents, pour le niveau en question.
      Column jointure =  
         comptes.col("siret").equalTo(cumuls.col("siret"))
         .and(comptes.col("numeroCompte").substr(1, nombreChiffres).equalTo(cumuls.col(aliasNumeroCompteRacine)));

      comptes = comptes.join(cumuls, jointure, "left_outer")
         .drop(cumuls.col("siret"))
         .drop(cumuls.col("codeCommune"));

      setStageDescription("Persistence du dataset des comptes");
      comptes = comptes.persist(); // Très intéressant.
      return comptes; 
   }

   /**
    * Renvoyer le validateur de Row utilisé par ce générateur de dataset
    * @return Validateur de Row.
    */
   protected AbstractCompteRowValidator validator() {
      return this.rowValidator;
   }

   /**
    * Référentiel des activités.
    * @return référentiel.
    */
   public Dataset<Row> referentielActivites() {
      String[][] referentiel = { 
         {"01","Adduction ou distribution d'eau et assainissement "},
         {"0A","Assainissement "},
         {"0E","Adduction ou distribution d'eau "},
         {"02","Production et distribution d'énergie"},
         {"03","Activités scolaires (sauf les cantines scolaires)"},
         {"04","Ramassage scolaire"},
         {"05","Activités sanitaires (hors M21)"},
         {"06","Activités sociales (hors M21 M22)"},
         {"07","Activités culturelles"},
         {"08","Activités sportives (sauf les remontées mécaniques)"},
         {"09","Tourisme"},
         {"10","Collecte et traitement des ordures ménagères"},
         {"11","Pompes funèbres (cimetières et crématoriums)"},
         {"12","Abattoirs"},
         {"13","Transport hors ramassage scolaire si individualisé"},
         {"14","Entretien des voiries et du matériel de voirie"},
         {"15","Aménagement de zones industrielles et artisanat"},
         {"16","Activités agricoles et forestières"},
         {"17","Remontées mécaniques"},
         {"18","Exploitation de parc de stationnement"},
         {"19","Chauffage urbain"},
         {"20","Foires, halles et marchés"},
         {"21","Gestion des ports et aéroports"},
         {"22","Ateliers relais"},
         {"23","Cantines du 1° degré"},
         {"24","Laboratoires d'analyses"},
         {"25","Protection et mise en valeur de l'environnement"},
         {"26","Secours et lutte contre l'incendie"},
         {"27","Cantine administrative"},
         {"28","Lotissements d'habitation"},
         {"29","Camping (régime spic / M4)"},
         {"30","Camping à but social (régime spa / M14)"},
         {"31","Logements sociaux"},
         {"32","Enfance (garderie, crèche, petite enfance, centre aéré..)"},
         {"33","Personnes âgées (résidences, foyer logement pour personnes âgées, aides ménagères, repas à domicile..)"},
         {"34","Nouvelles Technologies de l'Information et de la Communication (NTIC)"},
         {"35","Panneaux photovoltaïques "},
         {"36","Habitat"},
         {"37","Commerces multi-services"},
         {"38","Activités diverses (sauf chauffage urbain)"},
         {"39","Si plusieurs activités non isolables (les différentes activités sont tenues dans le compte de gestion principal)"},
         {"40","Administration générale (toutes les activités sont retracées dans des budgets annexes)"},
      };
      
      return referentiel().creerReferentiel(this.session, CODE_ACTIVITE.champ(), "libelleActivite", referentiel).orderBy(CODE_ACTIVITE.champ());
   }
   
   /**
    * Référentiel des types d'établissements.
    * @return référentiel.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> referentielTypeEtablissement() {
      String[][] referentiel = {
         {"101","Communes (BP)","COLLECTIVITE_TERRITORIALE"},
         {"102","Communes associées ou déléguées (BP)","COLLECTIVITE_TERRITORIALE"},
         {"702","VILLE DE PARIS (BP)","COLLECTIVITE_TERRITORIALE"},
         {"201","Départements (BP)","COLLECTIVITE_TERRITORIALE"},
         {"301","Régions (BP)","COLLECTIVITE_TERRITORIALE"},
         {"701","CTU – Collectivités territoriales uniques (BP) (Guyane Martinique)","COLLECTIVITE_TERRITORIALE"},
         {"410","Communes (BA) ","COLLECTIVITE_TERRITORIALE"},
         {"497","VILLE DE PARIS (BA)","COLLECTIVITE_TERRITORIALE"},
         {"420","Départements (BA) ","COLLECTIVITE_TERRITORIALE"},
         {"430","Régions (BA)","COLLECTIVITE_TERRITORIALE"},
         {"490","CTU – Collectivités territoriales uniques (BA)","EPCI/GFP"},
         {"401","GFP - CU - Communautés Urbaines (BP)","EPCI/GFP"},
         {"402","GFP - CA - Communautés d’Agglomérations (BP)","EPCI/GFP"},
         {"403","GFP - CC – Communautés de Communes (BP)","EPCI/GFP"},
         {"404","GFP - SAN – Syndicats d’Agglomération Nouvelle (BP)","EPCI/GFP"},
         {"405","GFP - MET – Métropoles (BP)","EPCI/GFP"},
         {"705","ML  - Métropole de Lyon (BP)","EPCI/GFP"},
         {"421","SIVU – Syndicats à Vocation Unique (BP)","EPCI/GFP"},
         {"422","SIVOM – Syndicats à Vocation Multiple (BP)","EPCI/GFP"},
         {"423","Pôles métropolitains (BP)","EPCI/GFP"},
         {"424","Pôles d'équilibre territorial et rural (BP)","EPCI/GFP"},
         {"425","EPT – Établissements publics territoriaux (BP)","EPCI/GFP"},
         {"440","Groupements de communes à fiscalité propre (CU,CA, CC, SAN et MET) (BA)","EPCI/GFP"},
         {"450","Groupements de communes non fiscalisés  (Syndicats + Pôles) (BA)","EPCI/GFP"},
         {"495","ML - Métropole de Lyon (BA)","EPCI/GFP"},
         {"431","CCAS - CIAS - Centres communaux et intercommunaux d’action sanitaire et sociale  (BP)","EPL"},
         {"433","Régies personnalisées (BP)","EPL"},
         {"437","Établissements publics administratifs  (BP)","EPL"},
         {"438","Préfecture de Police de Paris (BP)","EPL"},
         {"901","Groupements d'intérêt public – sauf GIPMDPH (BP)","EPL"},
         {"460","EPL (CCAS, CIAS, CDE, régies, SDIS, CGFPT, ASSOC, EPA) (BA)","EPL"}
      };
      
      return referentiel().creerReferentiel3champs(this.session, TYPE_ETABLISSEMENT.champ(), "libelleTypeEtablissement", "natureEtablissement", referentiel).orderBy(TYPE_ETABLISSEMENT.champ());
   }
   
   /**
    * Construire un référentiel de sous-type d'établissement.
    * @return Dataset de référentiel.
    */
   public Dataset<Row> referentielSousTypeEtablissement() {
      String[][] referentiel = { 
         {"421","00","Sans sous-type"},
         {"421","10","SIVU - Syndicat à Vocation Unique"},
         {"421","20","SYNDMC - Syndicat Mixte Communal"},
         {"421","30","SIAEP - Syndicat intercommunal d'adduction d'eau potable"},
         {"421","40","ASYMIX - Autre Syndicat Mixte"},
         {"421","50","ENTIND - Entente Interdépartementale"},
         {"421","60","ENTINR - Entente Interrégionale"},
         {"421","70","SICTOM - Syndicat Intercommunal de Traitement des Ordures Ménagères"},
         {"421","80","BINDIV - Commission syndicale pour la gestion de Biens Indivis"},
         {"421","90","GIP-MDPH - Groupement d'Intérêt Public  - Maisons Départementales des Personnes Handicapées"},
   
         {"433","10","REGIEP - Régie personnalisée à caractère industriel et commercial / EPIC – Établissement Public local à caractère Industriel et Commercial"},
         {"433","20","EPCC - Établissement Public de Coopération Culturelle"},
      };
      
      return referentiel().creerReferentiel3champs(this.session, TYPE_ETABLISSEMENT.champ(), SOUS_TYPE_ETABLISSEMENT.champ(), "libelleSousTypeEtablissement", referentiel).orderBy(TYPE_ETABLISSEMENT.champ(), SOUS_TYPE_ETABLISSEMENT.champ());
   }

   /**
    * Construire un référentiel de types de budgets.
    * @return Dataset référentiel.
    */
   public Dataset<Row> referentielTypeBudget() {
      String[][] referentiel = { 
         {"1","Budget principal"},
         {"2","Budget principal rattaché"},
         {"3","Budget annexe"},
         {"4","Budget annexe"}
      };

      return referentiel().creerReferentiel(this.session, TYPE_BUDGET.champ(), "libelleTypeBudget", referentiel).orderBy(TYPE_BUDGET.champ());
   }

   /**
    * Renommer et créer des champs.
    * @param csv Dataset brut, issu du fichier CSV.
    * @param optionsCreationLecture Options de création et de lecture de dataset
    * @param historique Historique d'exécution
    * @return Dataset aux champs renommés.
    */
   protected abstract Dataset<Row> renommerEtCreerChamps(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, Dataset<Row> csv, int annee);

   /**
    * Appliquer et filtrer les données d'après le Code Officiel Géographique.
    * @param anneeCOG Année du code officiel géographique.
    * @param sirenCommunes Siren des communes, et population des communes.
    * @param comptes Liste des comptes lus.
    * @param groupements Groupements, si appplicable. Peut valoir null.
    * @return Dataset de comptes à l'issue du traitement, le renvoyer trié dans l'ordre optimal du traitement à venir, si possible.
    */
   protected abstract Dataset<Row> appliquerCOG(int anneeCOG, Dataset<Row> sirenCommunes, Dataset<Row> comptes, Dataset<Row> groupements);

   /**
    * {@inheritDoc}
    */
   @Override
   protected RowToObjetMetierInterface<Compte> objetMetierEncoder() {
      return this.encoder;
   }
}
