package fr.ecoemploi.adapters.outbound.spark.dataset.balance;

import java.io.Serial;

import org.apache.spark.sql.Column;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.TriDataset;
import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Tri de dataset : partitionnement par département, puis tri par commune, siret, numéro de compte
 * @author Marc Le Bihan
 */
public class BalanceComptesTriDepartementCommuneSiretCompte extends TriDataset {
   @Serial
   private static final long serialVersionUID = 7325160923458089249L;

   /**
    * Constuire un tri.
    */
   public BalanceComptesTriDepartementCommuneSiretCompte() {
      super("PARTITION-departement-TRI-commune-siret-compte", true, CODE_DEPARTEMENT.champ(), false,
         CODE_DEPARTEMENT.champ(), CODE_COMMUNE.champ(), SIRET_ETABLISSEMENT.champ(), NUMERO_COMPTE.champ());
   }

   /**
    * Renvoyer une condition equalTo sur le code département et le code commune.
    * @param codeDepartement Code département. Si null, isNull() servira au test.
    * @param codeCommune Code commune. Si null, isNull() servira au test.
    * @param siret SIRET d'un établissement.
    * @param numeroCompte Numéro de compte de comptabilité.
    * @return Colonne contenant la condition.
    */
   public Column equalTo(String codeDepartement, String codeCommune, String siret, String numeroCompte) {
      return equalTo(codeDepartement, codeCommune, siret)
         .and(numeroCompte != null ? NUMERO_COMPTE.col().equalTo(numeroCompte) : NUMERO_COMPTE.col().isNull());
   }

   /**
    * Renvoyer une condition equalTo sur le code département et le code commune.
    * @param codeDepartement Code département. Si null, isNull() servira au test.
    * @param codeCommune Code commune. Si null, isNull() servira au test.
    * @param siret SIRET d'un établissement.
    * @return Colonne contenant la condition.
    */
   public Column equalTo(String codeDepartement, String codeCommune, String siret) {
      return equalTo(codeDepartement, codeCommune)
         .and(siret != null ? SIRET_ETABLISSEMENT.col().equalTo(siret) : SIRET_ETABLISSEMENT.col().isNull());
   }

   /**
    * Renvoyer une condition equalTo sur le code département et le code commune.
    * @param codeDepartement Code département. Si null, isNull() servira au test.
    * @param codeCommune Code commune. Si null, isNull() servira au test.
    * @return Colonne contenant la condition.
    */
   public Column equalTo(String codeDepartement, String codeCommune) {
      return equalTo(codeDepartement)
         .and(codeCommune != null ? CODE_COMMUNE.col().equalTo(codeCommune) : CODE_COMMUNE.col().isNull());
   }

   /**
    * Renvoyer une condition equalTo sur le code département.
    * @param codeDepartement Code département. Si null, isNull() servira au test.
    * @return Colonne contenant la condition.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
