package fr.ecoemploi.adapters.outbound.spark.dataset.individuel.communes;

import java.io.Serial;
import java.text.*;
import java.util.*;
import java.util.function.Supplier;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.CODE_COMMUNE;
import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;
import org.apache.spark.util.*;

import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.comptabilite.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels.*;

import fr.ecoemploi.adapters.outbound.port.compte.BalanceCompteIndividuelCommuneRepository;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.RowToObjetMetierInterface;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.CogDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.individuel.AbstractComptesIndividuelsDataset;

/**
 * Dataset des comptes individuels des communes.
 * @author Marc Le Bihan
 */
@Service
public class ComptesIndividuelsCommunesDataset extends AbstractComptesIndividuelsDataset<ComptesAvecMoyenneStrate, ComptesAvecStrateEtRatioStructureEtStrate, ComptesAvecStrateEtReductionVotee, ComptesAvecStrateEtTauxVoteEtStrate, ComptesIndividuelsCommune> implements BalanceCompteIndividuelCommuneRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -5413388851056896163L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ComptesIndividuelsCommunesDataset.class);

   /** Encodeur d'objets comptes individuels de communes. */
   private final ComptesIndividuelsCommunesEncoder encoder;

   /** Dataset du Code Officiel Géographique. */
   @Autowired
   private CogDataset cogDataset;

   /**
    * Construire un dataset de comptes individuels de communes.
    * @param comptesIndividuelsRowCsvLoader Chargeur de fichier csv.
    * @param encoder Encodeur d'objets comptes individuels de commune.
    */
   @Autowired
   public ComptesIndividuelsCommunesDataset(ComptesIndividuelsCommunesRowCsvLoader comptesIndividuelsRowCsvLoader, ComptesIndividuelsCommunesEncoder encoder) {
      super(comptesIndividuelsRowCsvLoader, "an", "Commune");
      this.encoder = encoder;
   }

   /**
    * Obtenir les comptes individuels d'une commune.
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param anneeExercice Année.
    * @param codeCommune Code commune.
    * @return Comptes individuel de la commune (analyse financière) ou null si elle n'a pas été trouvée.
    */
   public ComptesIndividuelsCommune obtenirComptesIndividuels(int anneeCOG, int anneeExercice, CodeCommune codeCommune) {
      Objects.requireNonNull(codeCommune, "Le numéro de commune ne peut pas valoir null.");

      OptionsCreationLecture optionsCreationLecture = optionsCreationLecture();

      Dataset<Row> comptesIndividuels = rowComptesIndividuels(optionsCreationLecture, null, anneeExercice, anneeCOG);

      List<ComptesIndividuelsCommune> comptesIndividuelCommune =
         collectComptesIndividuels(comptesIndividuels, CODE_COMMUNE.col(comptesIndividuels).equalTo(codeCommune.getId()));

      if (comptesIndividuelCommune.size() > 1) {
         throw new IllegalStateException("Plus d'un compte a été trouvé pour la commune " + codeCommune);
      }

      return comptesIndividuelCommune.isEmpty() ? null : comptesIndividuelCommune.get(0);
   }

   /**
    * Obtenir les comptes individuels d'une commune.
    * @param comptesNonFiltres Le dataset sur lequel il faut appliquer une condition.
    * @param condition Condition à appliquer, si elle est différente de null.
    * @return Liste des comptes individuels.
    * @deprecated À remplacer par des méthodes faisant le bon tri et filtrage.
    */
   @Deprecated
   private List<ComptesIndividuelsCommune> collectComptesIndividuels(Dataset<Row> comptesNonFiltres, Column condition) {
      Dataset<Row> filtre = condition != null ? comptesNonFiltres.where(condition) : comptesNonFiltres;
      return objetMetierEncoder().toDatasetObjetsMetiers(filtre).collectAsList();
   }

   /**
    * Recenser les taxes locales des communes d'une intercommunalité.
    * @param codeEPCI Code EPCI.
    * @param anneeComptableDebut Année comptable de début.
    * @param anneeComptableFin Année comptable de fin.
    * @param anneeCOG Année du COG : l'année comptable de fin est prise par défaut, s'il vaut zéro. 
    * @param optionsCreationLecture Options de constitution de dataset.
    * @param historique Historique d'exécution.
    * @return Taxes locales par communes et par années.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> taxesLocalesCommunesIntercommunalite(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, String codeEPCI, int anneeComptableDebut, int anneeComptableFin, int anneeCOG) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.aucuneDeRegleValidation(this.session);
      }

      Dataset<Row> ds = rowComptesIndividuels(options, historique, 0, anneeComptableFin);
      Dataset<Row> communes = this.cogDataset.rowCommunes(optionsCreationLecture, historique, anneeCOG != 0 ? anneeCOG : anneeComptableFin, false).where(col("codeEPCI").equalTo(codeEPCI));
      
      Dataset<Row> resultat = null;

      for(int annee = anneeComptableDebut; annee <= anneeComptableFin; annee ++) {
         Dataset<Row> dsComptesIndividuelsCommune = ds
            .where(ds.col("anneeExercice").equalTo(annee))
            .selectExpr("nomCommune", "anneeExercice",
               "capaciteDesendettement",
               
               // Taxe d'habitation
               "fiscLocalBaseNetteTaxeHabitationTotal as TH_base_nette",
               "fiscLocalBaseNetteTaxeHabitationParHabitant as TH_base_nette_hab",
               "fiscLocalBaseNetteTaxeHabitationMoyenneStrate as TH_base_nette_strate",
                  
               "impotsLocauxTaxeHabitationTauxVote as TH_taux",
               "impotsLocauxTaxeHabitationTauxMoyenStrate as TH_taux_strate",
               
               "impotsLocauxTaxeHabitationTotal as TH",
               "impotsLocauxTaxeHabitationParHabitant as TH_hab",
               "impotsLocauxTaxeHabitationMoyenneStrate as TH_strate",

               // Foncier Bâti
               "fiscLocalBaseNetteTaxeFonciereProprietesBatiesTotal as FB_base_nette",
               "fiscLocalBaseNetteTaxeFonciereProprietesBatiesParHabitant as FB_base_nette_hab",
               "fiscLocalBaseNetteTaxeFonciereProprietesBatiesMoyenneStrate as FB_base_nette_strate",

               "impotsLocauxTaxeFonciereProprietesBatiesTauxVote as FB_taux",
               "impotsLocauxTaxeFonciereProprietesBatiesTauxMoyenStrate as FB_taux_strate",

               "impotsLocauxTaxeFonciereProprietesBatiesTotal as FB",
               "impotsLocauxTaxeFonciereProprietesBatiesParHabitant as FB_hab",
               "impotsLocauxTaxeFonciereProprietesBatiesMoyenneStrate as FB_strate",
               
               // Foncier non bâti.
               "fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesTotal as FNB_base_nette",
               "fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBatiesTotal as FNB_base_nette_add",
               "fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesParHabitant as FNB_base_nette_hab",
               "fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBatiesParHabitant as FNB_base_nette_add_hab",
               "fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesMoyenneStrate as FNB_base_nette_strate",
               "fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBatiesMoyenneStrate as FNB_base_nette_add_strate",

               "impotsLocauxTaxeFonciereProprietesNonBatiesTauxVote as FNB_taux",
               "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesTauxVote as FNB_add_taux",
               "impotsLocauxTaxeFonciereProprietesNonBatiesTauxMoyenStrate as FNB_taux_strate",
               "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesTauxMoyenStrate as FNB_add_taux_strate",

               "impotsLocauxTaxeFonciereProprietesNonBatiesTotal as FNB",
               "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesTotal as FNB_add",
               "impotsLocauxTaxeFonciereProprietesNonBatiesParHabitant as FNB_hab",
               "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesParHabitant as FNB_add_hab",
               "impotsLocauxTaxeFonciereProprietesNonBatiesMoyenneStrate as FNB_strate",
               "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesMoyenneStrate as FNB_add_strate",

               // Cotisation Foncière des Entreprises
               "fiscLocalBaseNetteCotisationFonciereEntreprisesTotal as CFE_base_nette",
               "fiscLocalBaseNetteCotisationFonciereEntreprisesParHabitant as CFE_base_nette_hab",
               "fiscLocalBaseNetteCotisationFonciereEntreprisesMoyenneStrate as CFE_base_nette_strate",
               
               "impotsLocauxCotisationFonciereEntreprisesTotal as CFE",
               "impotsLocauxCotisationFonciereEntreprisesParHabitant as CFE_hab",
               "impotsLocauxCotisationFonciereEntreprisesMoyenneStrate as CFE_strate",
               "impotsLocauxCotisationFonciereEntreprisesTauxVote as CFE_taux",
               "impotsLocauxCotisationFonciereEntreprisesTauxMoyenStrate as CFE_taux_strate",
               
               // CVAE
               "produitsImpotsRepartitionCotisationValeurAjouteeEntreprisesTotal as CFE_rep",
               "produitsImpotsRepartitionCotisationValeurAjouteeEntreprisesParHabitant as CFE_rep_hab",
               "produitsImpotsRepartitionCotisationValeurAjouteeEntreprisesMoyenneStrate as CFE_rep_strate",
               
               // IFER
               "produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseauTotal as IFER_rep",
               "produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseauParHabitant as IFER_rep_hab",
               "produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseauMoyenneStrate as IFER_strate",
               
               // TASCOM
               "produitsImpotsRepartitionTaxeSurfacesCommercialesTotal as TASCOM_rep",
               "produitsImpotsRepartitionTaxeSurfacesCommercialesParHabitant as TASCOM_rep_hab",
               "produitsImpotsRepartitionTaxeSurfacesCommercialesMoyenneStrate as TASCOM_rep_strate",
            
               "codeCommune");
         
         // Cumuler les résultats annuels avec ceux déjà extraits.
         if (resultat == null) {
            resultat = dsComptesIndividuelsCommune;
         }
         else {
            resultat = resultat.union(dsComptesIndividuelsCommune);
         }
      }
      
      if (resultat != null) {
         resultat = resultat.join(communes, ds.col("codeCommune").equalTo(communes.col("codeCommune")), "leftsemi")
            .orderBy("nomCommune", "anneeExercice");
      }
      
      return resultat;
   }
   
   /**
    * Obtenir un Dataset Row des comptes individuels des communes.
    * @param anneeComptable Année comptable : si 0, toutes les années comptables sont rapatritées.
    * @param anneeCOG année du COG pour rectifier les communes.
    * @param optionsCreationLecture Options de constitution de dataset.
    * @param historique Historique d'exécution.
    * @return Dataset des comptes individuels des communes.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> rowComptesIndividuels(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeComptable, int anneeCOG) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.aucuneDeRegleValidation(this.session);
      }

      Supplier<Dataset<Row>> worker = () -> {
         Dataset<Row> comptesIndividuels = super.rowComptesIndividuels(anneeComptable);
         comptesIndividuels = applicationCog(comptesIndividuels, anneeCOG);

         return comptesIndividuels;
      };

      Column postFiltre = options.hasRestrictionAuxCommunes() ? CODE_COMMUNE.col().isin(options.restrictionAuxCommunes().toArray()) : null;
      boolean anneeEligibleCache = options.anneeEligibleMiseEnCache(anneeComptable);

      return constitutionStandard(optionsCreationLecture, historique, worker, anneeEligibleCache, new ConditionPostFiltrageConstante(postFiltre),
         new CacheParqueteur<>(optionsCreationLecture, this.session, exportCSV(), "comptes-individuels-communes", "FILTRE-exercice_{0,number,#0}-cog_{1,number,#0}", new ComptesIndividuelsTriDepartementCommune(),
            anneeComptable, anneeCOG));
   }

   /**
    * Application du COG.
    * @param comptesIndividuels Dataset à compléter.
    * @param anneeCOG Année du COG à charger.
    * @return dataset complété.
    */
   private Dataset<Row> applicationCog(Dataset<Row> comptesIndividuels, int anneeCOG) {
      Dataset<Row> communes = this.cogDataset.rowCommunes(null, null, anneeCOG, false);

      // Correction des départements d'outremer, et association du code commune si demandé.

      // Une erreur du fichier (à priori) fait que les départements d'outremer sont mal codés.
      // 101 = Guadeloupe au lieu de 971
      // 102 = Guyane au lieu de 973
      // 103 = Martinique au lieu de 972
      // 104 = La Réunion au lieu de 974
      // 106 = Mayotte au lieu de 976
      String nomChampDepartement = "dep";

      Column colDepartementSur3 =
         when(col(nomChampDepartement).isin("101", "102", "103", "104", "106"), lit(97)) // DOM
            .when(col(nomChampDepartement).isin("2A", "2B"), lit("020"))                    // Corse
            .otherwise(col(nomChampDepartement));

      Column colOutremer = colDepartementSur3.isin("97", "98").cast(BooleanType);

      Column colNumeroCommune = lpad(col("icom"), 3, "0");
      Column colDepartement = when(colOutremer, colDepartementSur3).otherwise(colDepartementSur3.substr(2, 2));

      Column colCodeCmmuneIndividuel = when(colOutremer, concat(colDepartementSur3, colNumeroCommune))
         .otherwise(concat(colDepartement, colNumeroCommune));
      comptesIndividuels = comptesIndividuels.withColumn("codeCommuneIndividuel", colCodeCmmuneIndividuel);

      CollectionAccumulator<String> codesCommunesInvalides = this.session.sparkContext().collectionAccumulator("codesCommunesInvalides");
      CollectionAccumulator<String> nomsCommunesInvalides = this.session.sparkContext().collectionAccumulator("nomsCommunesInvalides");

      comptesIndividuels = comptesIndividuels.join(communes, comptesIndividuels.col("codeCommuneIndividuel").equalTo(communes.col("codeCommune")), "left_outer")
         .drop(communes.col("populationCompteApart"))
         .filter((FilterFunction<Row>) individuel -> {
            String codeCommune = individuel.getAs("codeCommune");

            if (codeCommune == null) {
               codesCommunesInvalides.add(individuel.getAs("codeCommuneIndividuel"));
               nomsCommunesInvalides.add(individuel.getAs("inom"));

               LOGGER.warn("Un compte individuel de commune '{}' [dep='{}', icom='{}', reg='{}'] ({}), exercice {} est écarté car cette commune est inconnue du COG {}.",
                  individuel.getAs("codeCommuneIndividuel"), individuel.getAs("dep"), individuel.getAs("icom"), individuel.getAs("reg"),
                  individuel.getAs("inom"), individuel.getAs("anneeExercice"), anneeCOG);

               return false;
            }

            return true;
         })
         .drop(comptesIndividuels.col("inom"))
         .drop(comptesIndividuels.col("dep"))  // Remplacé par le code département du fichier de correspondance Siren <-> Communes.
         .drop(comptesIndividuels.col("icom")) // Remplacé par le code commune du fichier de correspondance Siren <-> Communes.
         .drop(comptesIndividuels.col("reg"))  // Remplacé par le code région du fichier de correspondance Siren <-> Communes.
         .drop(comptesIndividuels.col("codeCommuneIndividuel")); // Qui a servi temporairement pour la rectification du code département + commune.

      // Bilan des désynchronisations communes des comptes et COG.
      if (codesCommunesInvalides.value().isEmpty() == false) {
         LinkedHashSet<String> codesInvalides = new LinkedHashSet<>(codesCommunesInvalides.value());
         LinkedHashSet<String> nomsInvalides = new LinkedHashSet<>(nomsCommunesInvalides.value());

         LOGGER.warn("Bilan du contrôle : {} communes n'ont pas trouvé de correspondance dans le COG {} :", codesInvalides.size(), anneeCOG);

         Iterator<String> itCodes = codesInvalides.iterator();
         Iterator<String> itNoms = nomsInvalides.iterator();

         while (itCodes.hasNext()) {
            LOGGER.warn("{} - {}", itCodes.next(), itNoms.next());
         }
      } else {
         LOGGER.debug("Bilan du contrôle : toutes les communes ont trouvé leur correspondance dans le COG {}.", anneeCOG);
      }

      return comptesIndividuels;
   }

   /**
    * Alimenter la carte de désendettement des communes de France :<br>
    * Elle sera écrite dans une table nommmée desendettement_communes_{exercice comptable}, qui doit être présente en base de données.
    * @param exerciceComptable Exercice comptable.
    * @param anneeCOG Année de référence du Code Officiel Géographique.
    * @param optionsCreationLecture Options de constitution de dataset.
    * @param historiqueExecution Historique d'exécution.
    */
   public void alimenterCarteDesendettement(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution, int exerciceComptable, int anneeCOG) {
      Dataset<Row> write = rowComptesIndividuels(optionsCreationLecture, historiqueExecution, exerciceComptable, anneeCOG)
          .select(col("codeCommune").alias("codecommune"), col("nomCommune").alias("nomcommune"), col("capaciteDesendettementCorrigeeArrondie").alias("capaciteDesendettement"));
      
      write.write()
         .format("jdbc")
         .option("url", this.urlJDBC)
         .option("dbtable", MessageFormat.format("desendettement_communes_{0,number,#0}", exerciceComptable))
         .option("user", this.usernameJDBC)
         .option("password", this.passwordJDBC)
         .option("truncate", "true")
         .mode(SaveMode.Overwrite)
         .save();      
   }

   @Override
   protected RowToObjetMetierInterface<ComptesIndividuelsCommune> objetMetierEncoder() {
      return this.encoder;
   }
}
