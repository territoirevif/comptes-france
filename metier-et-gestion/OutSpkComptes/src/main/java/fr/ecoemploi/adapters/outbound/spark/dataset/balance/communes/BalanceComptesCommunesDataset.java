package fr.ecoemploi.adapters.outbound.spark.dataset.balance.communes;

import java.io.Serial;
import java.util.*;

import org.slf4j.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.balance.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.Compte;

import fr.ecoemploi.adapters.outbound.port.compte.BalanceCompteCommuneRepository;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.*;

/**
 * Dataset de la balance des comptes des communes.
 * @author Marc Le Bihan
 */
@Service
public class BalanceComptesCommunesDataset extends AbstractBalanceComptesDataset implements BalanceCompteCommuneRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -1147752864318311368L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(BalanceComptesCommunesDataset.class);
   
   /** Dataset du Code Officiel Géographique. */
   private final CogDataset cogDataset;

   /** Dataset des départements. */
   private final DepartementDataset departementsDataset;

   /** Chargeur de CSV. */
   private final BalanceComptesCommuneRowCsvLoader rowCsvLoader;

   /** Nom du fichier de cache. */
   private static final String CACHE ="comptes-balance-communes";

   /**
    * Construire un dataset de lecture des balances des comptes de communes.
    * @param cogDataset Dataset du Code Officiel Géographique.
    * @param departementsDataset Dataset des départements.
    * @param rowCsvLoader Chargeur de CSV.
    * @param rowValidator Validateur de Row.
    * @param encoder Encodeur d'objet métier depuis un Row
    */
   @Autowired
   public BalanceComptesCommunesDataset(CogDataset cogDataset, DepartementDataset departementsDataset,
      BalanceComptesCommuneRowCsvLoader rowCsvLoader, BalanceCompteCommunesRowValidator rowValidator, CompteEncoder encoder) {
      super(rowCsvLoader, rowValidator, encoder, false);
      this.cogDataset = cogDataset;
      this.departementsDataset = departementsDataset;
      this.rowCsvLoader = rowCsvLoader;
   }

   /**
    * Obtenir la balance des comptes des d'une commune (Budget Principal et annexes).
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param anneeExercice Année.
    * @param codeCommune Code commune.
    * @return Liste des comptes par SIRET.
    */
   public Map<SIRET, List<Compte>> obtenirBalanceComptesCommune(int anneeCOG, int anneeExercice, CodeCommune codeCommune) {
      Objects.requireNonNull(codeCommune, "Le numéro de commune ne peut pas valoir null.");

      LOGGER.info("Demande de la balance des comptes de la commune de code {} du COG d''année {} pour l''exercice comptable {}.",
         codeCommune, anneeCOG, anneeExercice);

      Dataset<Row> rows = obtenirComptesCommune(optionsCreationLecture(), null,
        codeCommune, anneeExercice, anneeCOG);

      List<Compte> comptes = rows.as(Encoders.bean(Compte.class)).collectAsList();

      HashMap<SIRET, List<Compte>> comptesParEtablissement = new HashMap<>();

      for(Compte compte : comptes) {
         List<Compte> comptesEtablissement = comptesParEtablissement.computeIfAbsent(compte.asSiret(), k -> new ArrayList<>());
         comptesEtablissement.add(compte);
      }

      return comptesParEtablissement;
   }

   /**
    * Renvoyer tous les comptes d'une commune : chaque budget, chaque établissement repéré par un siret.
    * @param optionsCreationLecture Options d'écriture et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution
    * @param codeCommune Code commune dont on désire tous les plan de comptes de tous les établissements/budgets qu'elle a.
    * @param anneeComptable Année comptable.
    * @param anneeCOG Année COG.
    * @return Liste des plans de comptes.
    */
   public Dataset<Row> obtenirComptesCommune(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution,
       CodeCommune codeCommune, int anneeComptable, int anneeCOG) {
       Objects.requireNonNull(codeCommune, "Le code commune ne peut pas valoir null.");
       return obtenirComptesCommune(optionsCreationLecture, historiqueExecution, codeCommune.toString(), anneeComptable, anneeCOG, codeCommune.numeroDepartement());
   }

   /**
    * Renvoyer tous les comptes d'une commune : chaque budget, chaque établissement repéré par un siret.
    * @param optionsCreationLecture Options d'écriture et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution
    * @param codeCommune Code commune dont on désire tous les plan de comptes de tous les établissements/budgets qu'elle a.
    * @param anneeComptable Année comptable.
    * @param anneeCOG Année COG.
    * @return Liste des plans de comptes.
    */
   public Dataset<Row> obtenirComptesCommune(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution,
      String codeCommune, int anneeComptable, int anneeCOG) {
      Objects.requireNonNull(codeCommune, "Le code commune ne peut pas valoir null.");

      String codeDepartement = new CodeCommune(codeCommune).numeroDepartement();
      return obtenirComptesCommune(optionsCreationLecture, historiqueExecution, codeCommune, anneeComptable, anneeCOG, codeDepartement);
   }
   
   /**
    * Renvoyer tous les comptes d'une commune : chaque budget, chaque établissement repéré par un siret.
    * @param optionsCreationLecture Options d'écriture et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution
    * @param codeCommune Code commune dont on désire tous les plan de comptes de tous les établissements/budgets qu'elle a.
    * @param siret Siret recherché.
    * @param anneeComptable Année comptable.
    * @param anneeCOG Année COG.
    * @return Liste des plans de comptes.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> obtenirComptesCommune(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution,
      String codeCommune, String siret, int anneeComptable, int anneeCOG) {
      Objects.requireNonNull(siret, "Le code siret des comptes recherchés d'une commune ne peut pas valoir null.");

      return obtenirComptesCommune(optionsCreationLecture, historiqueExecution, codeCommune, anneeComptable, anneeCOG)
        .where(col("siret").equalTo(siret));
   }

   /**
    * Renvoyer tous les comptes d'une commune : chaque budget, chaque établissement repéré par un siret.
    * @param optionsCreationLecture Options d'écriture et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution
    * @param codeCommune Code commune dont on désire tous les plan de comptes de tous les établissements/budgets qu'elle a.
    * @param anneeComptable Année comptable.
    * @param anneeCOG Année COG.
    * @param codeDepartement Code département.
    * @return Liste des plans de comptes.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   private Dataset<Row> obtenirComptesCommune(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution,
      String codeCommune, int anneeComptable, int anneeCOG, String codeDepartement) {
      Objects.requireNonNull(codeCommune, "le code commune ne peut pas valoir null.");
      Objects.requireNonNull(codeDepartement, "le code département ne peut pas valoir null.");

      return rowComptesCommunes(optionsCreationLecture, historiqueExecution, anneeComptable, anneeCOG)
        .where(col("codeDepartement").equalTo(codeDepartement)
        .and(col("codeCommune").equalTo(codeCommune)));
   }

   /**
    * Créer une matrice code commune X compte (leur solde est soldeCréditeur - soldeDébiteur).
    * @param optionsCreationLecture Options d'écriture et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution
    * @param anneeComptable Année comptable.
    * @param anneeCOG Année de référence du Code Officiel Géographique.
    * @param montantParHabitants true s'il faut renvoyer les soldes créditeurs par habitant.
    * @param condition Condition optionnelle.
    * @return matrice commune x compte
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> matriceCommuneComptes(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution,
      int anneeComptable, int anneeCOG, boolean montantParHabitants, Column condition) {

      // Calculer un solde = crédit - débit
      Dataset<Row> matrice = rowComptesCommunes(optionsCreationLecture, historiqueExecution, anneeComptable, anneeCOG)
         .withColumn("solde", col("soldeCrediteur").minus(col("soldeDebiteur")));

      if (condition != null) {
         matrice = matrice.filter(condition);
      }

      // Faire un ratio par habitant, si demandé.
      if (montantParHabitants) {
         matrice = matrice.selectExpr("codeCommune", "numeroCompte", "solde", "populationMunicipale")
            .withColumn("soldeParHabitant", col("solde").divide(col("populationMunicipale")))
            .groupBy("codeCommune").pivot("numeroCompte").sum("soldeParHabitant");
      }
      else {
         matrice = matrice.selectExpr("codeCommune", "numeroCompte", "solde")
            .groupBy("codeCommune").pivot("numeroCompte").sum("solde");
      }

      matrice = matrice.na().fill(0.0); // Remplacer l'absence d'un solde (null) par un montant à 0.0
      return matrice;
   }

   /**
    * Obtenir un Dataset Row des balances des comptes des communes.
    * @param optionsCreationLecture Options d'écriture et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution
    * @param anneeComptable Année comptable.
    * @param anneeCOG année du COG pour rectifier les communes.
    * @return Dataset des comptes des communes trié oar codeCommune, siret, numéro de compte.
    */
   public Dataset<Row> rowComptesCommunes(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution,
      int anneeComptable, int anneeCOG) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      emetteur().jobDescription(this.session, "Balance des comptes des communes pour l''exercice {0,number,#0}, anéee COG : {1,number,#0}", anneeComptable, anneeCOG);

      return rowComptes(options, historiqueExecution, new BalanceComptesTriDepartementCommuneSiretCompte(), CACHE,
         anneeComptable, anneeCOG);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   protected Dataset<Row> renommerEtCreerChamps(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, Dataset<Row> csv, int annee) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.addReglesValidation(this.session, validator());
      }

      Dataset<Row> comptes = this.rowCsvLoader.rename(csv, annee);
      comptes = this.rowCsvLoader.cast(comptes, annee);

      // Jonction d'autres référentiels.
      comptes = referentiel().joinReferentiel(comptes, referentielTypeEtablissement(), "typeEtablissement");
      comptes = referentiel().joinReferentielDeuxClefs(comptes, referentielSousTypeEtablissement(), "typeEtablissement", "typeEtablissement", "sousTypeEtablissement", "sousTypeEtablissement");
      comptes = referentiel().joinReferentiel(comptes, referentielActivites(), "codeActivite");
      comptes = referentiel().joinReferentiel(comptes, referentielTypeBudget(), "typeBudget");

      // Corriger le code département et établir le code commune.
      comptes = this.departementsDataset.withDepartement(options, historique, "codeDepartement", comptes, "codeDepartementSur3", "INSEE", false, 0);
      comptes = comptes.drop("codeDepartementSur3");

      comptes = this.cogDataset.withCodeCommune(historique, "codeCommuneBalance", comptes, "codeDepartement", "INSEE");
      comptes = comptes.drop("INSEE");
      return comptes;
   }

  /**
   * {@inheritDoc}
   */
   @Override
   protected Dataset<Row> appliquerCOG(int anneeCOG, Dataset<Row> sirenCommunes, Dataset<Row> comptes, @SuppressWarnings("unused") Dataset<Row> groupements) {
      // Restreindre aux communes du COG.
      Column joinCondition = comptes.col("codeDepartement").equalTo(sirenCommunes.col("codeDepartement")).and(comptes.col("siren").equalTo(sirenCommunes.col("sirenCommune")));

      comptes = comptes.join(sirenCommunes.select("codeCommune", "codeDepartement", "sirenCommune", "nomCommune", "populationMunicipale"),
         joinCondition, emetteur().isAvertirIncidents() ?"left_outer" :"inner")
        .drop(comptes.col("codeCommuneBalance"))
        .drop(sirenCommunes.col("codeDepartement"))
        .drop(sirenCommunes.col("sirenCommune"));

      if (emetteur().isAvertirIncidents()) {
         Dataset<Row> communesInconnues = comptes.select("libelleBudget").where(col("codeCommune").isNull());
         Dataset<Row> communesDeleguees = communesInconnues.where(col("typeEtablissement").equalTo(lit("102"))).distinct();
         Dataset<Row> communesInexistantes = communesInconnues.where(col("typeEtablissement").notEqual(lit("102"))).distinct();

         comptes = comptes.where(col("codeCommune").isNotNull());

         LOGGER.info("{} communes déléguées ont été écartées de l'exercice {} parce qu'elles ne sont pas dans le COG :", communesDeleguees.count(), anneeCOG);
         communesDeleguees.show(36000, false);

         LOGGER.info("{} communes de l'exercice {} sont inconnues du COG :", communesInexistantes.count(), anneeCOG);
         communesInexistantes.show(36000, false);
      }
  
      return comptes;
   }
}
