package fr.ecoemploi.adapters.outbound.spark.dataset.balance;

import java.io.Serial;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ecoemploi.adapters.outbound.port.compte.BalanceCompteRepository;
import fr.ecoemploi.adapters.outbound.spark.dataset.balance.communes.BalanceComptesCommunesDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.balance.intercommunalites.BalanceComptesIntercommunalitesDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.individuel.communes.ComptesIndividuelsCommunesDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.individuel.intercommunalites.ComptesIndividiuelsIntercommunalitesDataset;
import fr.ecoemploi.service.*;

/**
 * Chargeur des différents datasets
 */
@Service
public class ChargeurComptes implements BalanceCompteRepository {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -7608537362439955557L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ChargeurComptes.class);

   /** Balance des comptes des communes. */
   @Autowired
   private BalanceComptesCommunesDataset balancesComptesCommunes;

   /** Balance des comptes des intercommunalités. */
   @Autowired
   private BalanceComptesIntercommunalitesDataset balancesComptesIntercommunalites;

   /** Comptes individuels communes. */
   @Autowired
   private ComptesIndividuelsCommunesDataset comptesIndividuelsCommunes;

   /** Comptes individuels intercommunaux. */
   @Autowired
   private ComptesIndividiuelsIntercommunalitesDataset comptesIndividuelsIntercommunalites;

   /** Année du cog de départ. */
   private static final int COG_START = 2017;

   /** Année du cog de fin. */
   private static final int COG_END = 2019;

   /**
    * Charger les comptes des communes et des intercommunalités.
    * @param anneeCOG Année de référence du Code Officiel Géographique à charger.<br>
    * 0 si toutes les années connues doivent être chargées (à partir de 2016).
    */
   public void chargerReferentiels(int anneeCOG) {
      int anneeDebut = anneeCOG != 0 ? anneeCOG : COG_START;
      int anneeFin = anneeCOG != 0 ? anneeCOG : COG_END;

      // Seuls les datasets stockant leurs données sont appelés.
      for(int annee = anneeDebut; annee <= anneeFin; annee ++) {
         try {
            // Les communes qui ont existé et sont apparues dans l'execice n, sont connues dans le COG n+1.
            this.balancesComptesCommunes.rowComptesCommunes(null, new HistoriqueExecution(), annee, annee+1);
         }
         catch(RepertoireInexistantException | FichierGeographiqueAbsentException e) {
            LOGGER.info(e.getMessage());
         }

         try {
            this.balancesComptesIntercommunalites.rowComptesIntercommunalites(null, new HistoriqueExecution(), annee, annee+1);
         }
         catch(RepertoireInexistantException | FichierGeographiqueAbsentException e) {
            LOGGER.info(e.getMessage());
         }

         try {
            this.comptesIndividuelsCommunes.rowComptesIndividuels(null, new HistoriqueExecution(), annee, annee+1);
         }
         catch(RepertoireInexistantException | FichierGeographiqueAbsentException e) {
            LOGGER.info(e.getMessage());
         }

         try {
            this.comptesIndividuelsIntercommunalites.rowComptesIntercommunaux(null, new HistoriqueExecution(), annee, annee+1);
         }
         catch(RepertoireInexistantException | FichierGeographiqueAbsentException e) {
            LOGGER.info(e.getMessage());
         }
      }
   }
}
