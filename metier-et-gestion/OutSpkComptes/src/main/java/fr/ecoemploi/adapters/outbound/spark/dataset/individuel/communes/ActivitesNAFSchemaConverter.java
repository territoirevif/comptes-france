package fr.ecoemploi.adapters.outbound.spark.dataset.individuel.communes;

import static org.apache.spark.sql.types.DataTypes.*;

import java.util.*;

import org.apache.spark.sql.types.*;

/**
 * Créateur de schémas de dataset pour activités de communes par code NAF.
 * @author Marc LE BIHAN
 */
public class ActivitesNAFSchemaConverter {
   /**
    * Ajouter à un schéma de Dataset existant un ensmemble de champs pour recevoir des activités.
    * @param schema Schéma cible.
    * @param codesNAF Codes NAF à adjoindre.
    * @return Schéma modifié.
    */
   public static StructType addToSchema(StructType schema, List<String> codesNAF) {
      StructType s = schema.add("codeCommuneAC", StringType, false);
      s = s.add("nomAC", StringType, false);
      s = s.add("populationAC", IntegerType, false);
      s = s.add("nombreEntreprises", IntegerType, false);
      s = s.add("nombreSalaries", IntegerType, false);
      
      for(String codeNAF : codesNAF) {
         s = s.add("codeNAF_" + codeNAF, StringType, false);
         s = s.add("nombreEntreprises_" + codeNAF, IntegerType, false);
         s = s.add("pourcentageEntreprises_" + codeNAF, DoubleType, false);
         s = s.add("nombreSalaries_" + codeNAF, IntegerType, false);
         s = s.add("pourcentageSalaries_" + codeNAF, DoubleType, false);
      }
      
      return s;
   }
}
