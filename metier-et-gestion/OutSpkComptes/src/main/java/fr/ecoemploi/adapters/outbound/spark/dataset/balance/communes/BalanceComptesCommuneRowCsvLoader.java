package fr.ecoemploi.adapters.outbound.spark.dataset.balance.communes;

import java.io.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.balance.AbstractBalanceComptesRowCsvLoader;

/**
 * Chargeur de fichier CSV pour les fichiers Balance de communes
 * @author Marc Le Bihan
 */
@Component
public class BalanceComptesCommuneRowCsvLoader extends AbstractBalanceComptesRowCsvLoader {
   @Serial
   private static final long serialVersionUID = 2467737079326670241L;

   /**
    * Construire un chargeur de CSV depuis un fichier de comptes de communes.
    * @param nomFichier Nom du fichier
    */
   @Autowired
   public BalanceComptesCommuneRowCsvLoader(@Value("${balance-comptes.fichier.csv.nom}") String nomFichier) {
      super(nomFichier);
   }
}
