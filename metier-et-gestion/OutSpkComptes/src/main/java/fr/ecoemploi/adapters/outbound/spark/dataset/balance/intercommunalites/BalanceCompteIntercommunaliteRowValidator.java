package fr.ecoemploi.adapters.outbound.spark.dataset.balance.intercommunalites;

import java.io.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.sql.Row;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.balance.AbstractCompteRowValidator;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.HistoriqueExecution;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.NUMERO_COMPTE;

/**
 * Validateur de Row de Balance de comptes d'intercommunalité
 * @author Marc Le Bihan
 */
@Component
public class BalanceCompteIntercommunaliteRowValidator extends AbstractCompteRowValidator implements ApplicationContextAware {
   @Serial
   private static final long serialVersionUID = -6883913981656525543L;

   /** Un numéro de compte n'est pas accompagné d'un SIRET */
   public static final String VLD_COMPTE_SANS_SIRET = "COMPTE_SANS_SIRET";

   /**
    * Valider un compte de commune
    * @param historique Historique d'exécution
    * @param compte Compte
    * @return true s'il est valide
    */
   @Override
   public boolean validerCompte(HistoriqueExecution historique, Row compte) {
      String numeroCompte = compte.getAs(NUMERO_COMPTE.champ());
      String siret = compte.getAs("siret");

      boolean valide = super.validerCompte(historique, compte);

      invalideSi(VLD_COMPTE_SANS_SIRET, historique, compte,
         c -> StringUtils.isBlank(siret),
         () -> new Serializable[] {numeroCompte});

      return valide;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
      super.setApplicationContext(applicationContext);
      declareRegle(VLD_COMPTE_SANS_SIRET, this.messageSource, "Numero de compte sans SIRET", String.class);
   }
}
