package fr.ecoemploi.adapters.outbound.spark.dataset.balance;

import java.io.*;
import java.util.*;
import java.util.function.BooleanSupplier;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.*;
import org.springframework.stereotype.Component;

import org.apache.spark.sql.Row;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.HistoriqueExecution;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.validation.AbstractRowValidator;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.NUMERO_COMPTE;

/**
 * Validateur de Row de Balance de comptes de communes
 * @author Marc Le Bihan
 */
@Component
public abstract class AbstractCompteRowValidator extends AbstractRowValidator implements ApplicationContextAware {
   @Serial
   private static final long serialVersionUID = 7143856958375448129L;

   /** Un enregistrement de compte n'est pas décodable */
   public static final String VLD_COMPTE_NON_DECODABLE = "COMPTE_NON_DECODABLE";

   /** Un numéro de compte a une nomenclature invalide */
   public static final String VLD_COMPTE_NOMENCLATURE_INVALIDE = "COMPTE_NOMENCLATURE_INVALIDE";

   /**
    * Valider un compte de commune
    * @param historique Historique d'exécution
    * @param compte Compte
    * @return true s'il est valide
    */
   public boolean validerCompte(HistoriqueExecution historique, Row compte) {
      // Vérifier que le numéro de compte est bien alimenté.
      String numeroCompte = compte.getAs(NUMERO_COMPTE.champ());
      String nomenclatureComptable = compte.getAs("nomenclatureComptable");

      List<BooleanSupplier> validations = new ArrayList<>();

      // Enregistrement de compte non décodable
      validations.add(() -> invalideSi(VLD_COMPTE_NON_DECODABLE, historique, compte,
         c -> numeroCompte == null,
         () -> new Serializable[]{compte.mkString()}));

      // Les comptes qui ne sont pas de nomenclature comptable correcte sont exclus
      // TODO Cette liste de nomclatures comptables admises est en dur
      validations.add(() -> valideSi(VLD_COMPTE_NOMENCLATURE_INVALIDE, historique, compte,
         c -> "M14".equals(nomenclatureComptable) || "M14A".equals(nomenclatureComptable)
               || "M4".equals(nomenclatureComptable) || "M21".equals(nomenclatureComptable)
               || "M22".equals(nomenclatureComptable) || "M41".equals(nomenclatureComptable)
               || "M42".equals(nomenclatureComptable)
               || "M43".equals(nomenclatureComptable) || "M43A".equals(nomenclatureComptable)
               || "M49".equals(nomenclatureComptable) || "M49A".equals(nomenclatureComptable)
               || "M52".equals(nomenclatureComptable)
               || "M57".equals(nomenclatureComptable)|| "M57A".equals(nomenclatureComptable)
               || "M61".equals(nomenclatureComptable) || "M71".equals(nomenclatureComptable)
               || "M832".equals(nomenclatureComptable),
         () -> new Serializable[] {numeroCompte, nomenclatureComptable}));

      return validations.stream().allMatch(BooleanSupplier::getAsBoolean);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
      declareRegle(VLD_COMPTE_NON_DECODABLE, this.messageSource, "Enregistrement de compte non décodable", String.class);
      declareRegle(VLD_COMPTE_NOMENCLATURE_INVALIDE, this.messageSource, "Numéro de compte avec nomenclature invalide", String.class, String.class);
   }
}
