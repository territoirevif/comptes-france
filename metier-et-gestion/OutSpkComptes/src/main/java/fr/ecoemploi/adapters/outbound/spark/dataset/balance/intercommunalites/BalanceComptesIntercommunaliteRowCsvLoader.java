package fr.ecoemploi.adapters.outbound.spark.dataset.balance.intercommunalites;

import java.io.*;

import org.apache.spark.sql.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.balance.AbstractBalanceComptesRowCsvLoader;

/**
 * Chargeur de fichier CSV pour les fichiers Balance des intercommunalités
 * @author Marc Le Bihan
 */
@Component
public class BalanceComptesIntercommunaliteRowCsvLoader extends AbstractBalanceComptesRowCsvLoader {
   @Serial
   private static final long serialVersionUID = 7313330555091934660L;

   /**
    * Construire un chargeur de CSV depuis un fichier de comptes d'intercommunalités.
    * @param nomFichier Nom du fichier
    */
   @Autowired
   public BalanceComptesIntercommunaliteRowCsvLoader(@Value("${balance-comptes-intercommunalites.fichier.csv.nom}") String nomFichier) {
      super(nomFichier);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Dataset<Row> loadOpenData(int anneeComptable) {
      return super.loadOpenData(0);
   }
}
