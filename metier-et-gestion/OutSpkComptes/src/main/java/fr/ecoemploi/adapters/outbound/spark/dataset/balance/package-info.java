/**
 * Dataset de balances des comptes communaux ou intercommunaux.
 * @author Marc LE BIHAN
 * 
 * <ol>
 * <li>Balance_REG_Exercice       Régions</li>
 * <li>Balance_DeptCtu_Exercice   Départements + collectivités territoriales uniques</li> 
 * <li>Balance_Commune_Exercice   Communes + PARIS</li>
 * <li>Balance_GfpEptML_Exercice  Groupements à Fiscalité Propre + établissements publics territoriaux + métropole de Lyon</li>
 * <li>Balance_SYND_Exercice      Syndicats + pôles métropolitains + pôles d'équilibre territoriaux</li>
 * <li>Balance_EPL_Exercice       Établissements publics locaux</li>
 * </ol>
 */
package fr.ecoemploi.adapters.outbound.spark.dataset.balance;