package fr.ecoemploi.adapters.outbound.spark.dataset.individuel;

import java.io.*;

import org.apache.spark.sql.*;

import fr.ecoemploi.domain.model.territoire.comptabilite.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels.*;

/**
 * Encodeur d'objet de compte individuel (pour l'analyse financière).
 * @param <MR> Comptes individuels avec montant total ou base, par habitant + Moyenne ou Ratio Structure, selon le cas.
 * @param <RS> Ratio Structure, avec moyennes strates selon le cas.
 * @param <RV> Réductions votées, avec moyennes strates selon le cas.
 * @param <TV> Taux votés, avec moyennes strates selon le cas.
 * @author Marc Le Bihan
 */
public abstract class AbstractComptesIndividuelsEncoder<MR extends ComptesTotalOuBaseEtParHabitant, RS extends ComptesAvecRatioStructure, RV extends ComptesAvecReductionVotee, TV extends ComptesAvecTauxVote> implements Serializable {
   @Serial
   private static final long serialVersionUID = -4468024893959650013L;

   /**
    * Alimenter la classe de base Comptes Individuels de la collectivité locale.
    * @param row Enregistrement source.
    * @param population Population de la collectivité locale.
    * @param siren Siren de la collectivité locale.
    * @param code Code de la collectivité locale.
    * @param nom Nom de la collectivité locale.
    * @param moyennesStrates True si les comptes individuels proposent des moyennes strates ou taux votés strates ou réduction votées strates,<br>
    * false s'ils n'ont que des ratios de structure.
    * @return Comptes Individuels de la collectivité locale.
    */
   protected ComptesIndividuels<MR, RS, RV, TV> alimenterCompteIndividuels(Row row, Integer population, String siren, String code, String nom, boolean moyennesStrates) {
      MR produitsDeFonctionnement = mr("TOTAL DES PRODUITS DE FONCTIONNEMENT = A\t\t\ten % des produits CAF", "produitsDeFonctionnement", moyennesStrates, row);
      MR produitsDeFonctionnementCAF = mr("\tPRODUITS DE FONCTIONNEMENT CAF", "produitsDeFonctionnementCAF", moyennesStrates, row);
      RS produitsDeFonctionnementCAFImpotsLocaux = rs("\t\tdont : Impôts Locaux\t\t", "produitsDeFonctionnementCAFImpotsLocaux", moyennesStrates, row);
      RS produitsDeFonctionnementCAFAutresImpotsEtTaxes = rs("\t\tAutres impôts et taxes\t\t", "produitsDeFonctionnementCAFAutresImpotsEtTaxes", moyennesStrates, row);
      RS produitsDeFonctionnementCAFDGF = rs("\t\tDotation globale de fonctionnement", "produitsDeFonctionnementCAFDGF", moyennesStrates, row);

      MR chargesDeFonctionnement = mr("TOTAL DES CHARGES DE FONCTIONNEMENT = B\t\t\t\ten % des charges CAF", "chargesDeFonctionnement", moyennesStrates, row);
      MR chargesDeFonctionnementCAF = mr("\tCharges de fonctionnement CAF", "chargesDeFonctionnementCAF", moyennesStrates, row);
      RS chargesDeFonctionnementCAFChargesDePersonnel = rs("\t\tdont : Charges de personnel\t", "chargesDeFonctionnementCAFChargesDePersonnel", moyennesStrates, row);
      RS chargesDeFonctionnementCAFAchatsEtChargesExternes = rs("\t\tachats et charges externes\t", "chargesDeFonctionnementCAFAchatsEtChargesExternes", moyennesStrates, row);
      RS chargesDeFonctionnementCAFChargesFinancieres = rs("\t\tCharges financières\t\t", "chargesDeFonctionnementCAFChargesFinancieres", moyennesStrates, row);
      RS chargesDeFonctionnementCAFSubventionsVersees = rs("\t\tSubventions versées\t\t", "chargesDeFonctionnementCAFSubventionsVersees", moyennesStrates, row);

      MR resultatComptable = mr("Résultat comptable = A - B = R", "resultatComptable", moyennesStrates, row);

      MR ressourcesInvestissement = mr("TOTAL DES RESSOURCES D'INVESTISSEMENT = C\t\t\ten % des ressources", "ressourcesInvestissement", moyennesStrates, row);
      RS ressourcesInvestissementEmpruntsBancairesDettesAssimilees = rs("\tdont : Emprunts bancaires et dettes assimilées", "ressourcesInvestissementEmpruntsBancairesDettesAssimilees", moyennesStrates, row);
      RS ressourcesInvestissementSubventionsRecues = rs("\tSubventions reçues\t\t\t", "ressourcesInvestissementSubventionsRecues", moyennesStrates, row);
      RS ressourcesInvestissementFCTVA = rs("\tFonds de compensation de la TVA (FCTVA)\t", "ressourcesInvestissementFCTVA", moyennesStrates, row);

      MR emploisInvestissement = mr("TOTAL DES EMPLOIS D'INVESTISSEMENT = D\t\t\t\ten % des emplois", "emploisInvestissement", moyennesStrates, row);
      RS emploisInvestissementDepensesEquipement = rs("\tdont : Dépenses d'équipement\t\t", "emploisInvestissementDepensesEquipement", moyennesStrates, row);
      RS emploisInvestissementRemboursementEmpruntsDettesAssimilees = rs("\tRemboursement d'emprunts et dettes assimilées", "emploisInvestissementRemboursementEmpruntsDettesAssimilees", moyennesStrates, row);

      RS autoFinancementCAF = rs("Capacité d'autofinancement = CAF\t\t", "autoFinancementCAF", moyennesStrates, row);
      RS autoFinancementCAFNette = rs("CAF nette du remboursement en capital des emprunts", "autoFinancementCAFNette", moyennesStrates, row);

      RS endettementEncoursTotalDetteAu31DecembreN = rs("Encours total de la dette au 31 décembre N\t", "endettementEncoursTotalDetteAu31DecembreN", moyennesStrates, row);
      RS endettementAnnuiteDette = rs("Annuité de la dette\t\t\t\t", "endettementAnnuiteDette", moyennesStrates, row);
      RS endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques = rs("Encours des dettes bancaires net de l'aide du fonds de soutien pour la sortie des emprunts toxiques", "endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques", moyennesStrates, row);
      RS endettementEncoursDettesBancairesEtAssimilees = rs("Encours des dettes bancaires et assimilées\t", "endettementEncoursDettesBancairesEtAssimilees", moyennesStrates, row);

      RV fiscLocalBaseNetteTaxeHabitation = rv("Taxe d'habitation (y compris THLV)\t\t", "fiscLocalBaseNetteTaxeHabitation", moyennesStrates, row);
      RV fiscLocalBaseNetteTaxeFonciereProprietesBaties = rv("Taxe foncière sur les propriétés bâties\t\t", "fiscLocalBaseNetteTaxeFonciereProprietesBaties", moyennesStrates, row);
      RV fiscLocalBaseNetteTaxeFonciereProprietesNonBaties = rv("Taxe foncière sur les propriétés non bâties\t", "fiscLocalBaseNetteTaxeFonciereProprietesNonBaties", moyennesStrates, row);
      MR fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties = mr("Taxe additionnelle à la taxe foncière sur les propriétés non bâties", "fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties", moyennesStrates, row);
      RV fiscLocalBaseNetteCotisationFonciereEntreprises = rv("Cotisation foncière des entreprises\t\t", "fiscLocalBaseNetteCotisationFonciereEntreprises", moyennesStrates, row);

      TV impotsLocauxTaxeHabitation = tv("Taxe d'habitation (y compris THLV)\t\t", "impotsLocauxTaxeHabitation", moyennesStrates, row);
      TV impotsLocauxTaxeFonciereProprietesBaties = tv("Taxe foncière sur les propriétés bâties\t\t", "impotsLocauxTaxeFonciereProprietesBaties", moyennesStrates, row);
      TV impotsLocauxTaxeFonciereProprietesNonBaties = tv("Taxe foncière sur les propriétés non bâties\t", "impotsLocauxTaxeFonciereProprietesNonBaties", moyennesStrates, row);
      TV impotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties = tv("Taxe additionnelle à la taxe foncière sur les propriétés non bâties\t", "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties", moyennesStrates, row);
      TV impotsLocauxCotisationFonciereEntreprises = tv("Cotisation foncière des entreprises\t\t", "impotsLocauxCotisationFonciereEntreprises", moyennesStrates, row);

      MR produitsImpotsRepartitionCotisationValeurAjouteeEntreprises = mr("Cotisation sur la valeur ajoutée des entreprises", "produitsImpotsRepartitionCotisationValeurAjouteeEntreprises", moyennesStrates, row);
      MR produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau = mr("Imposition forfaitaire sur les entreprises de réseau", "produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau", moyennesStrates, row);
      MR produitsImpotsRepartitionTaxeSurfacesCommerciales = mr("Taxe sur les surfaces commerciales", "produitsImpotsRepartitionTaxeSurfacesCommerciales", moyennesStrates, row);

      RatiosNiveauxEtStructure ratios = new RatiosNiveauxEtStructure();
      ratios.setCapaciteDesendettement((Double)row.getAs("capaciteDesendettement"));

      return new ComptesIndividuels<>((Integer)row.getAs("anneeExercice"), population,
         siren, code, nom,
         produitsDeFonctionnement, produitsDeFonctionnementCAF,
         produitsDeFonctionnementCAFImpotsLocaux, produitsDeFonctionnementCAFAutresImpotsEtTaxes, produitsDeFonctionnementCAFDGF,
         chargesDeFonctionnement, chargesDeFonctionnementCAF,
         chargesDeFonctionnementCAFChargesDePersonnel, chargesDeFonctionnementCAFAchatsEtChargesExternes, chargesDeFonctionnementCAFChargesFinancieres,
         chargesDeFonctionnementCAFSubventionsVersees,
         resultatComptable,
         ressourcesInvestissement, ressourcesInvestissementEmpruntsBancairesDettesAssimilees,
         ressourcesInvestissementSubventionsRecues, ressourcesInvestissementFCTVA,
         emploisInvestissement, emploisInvestissementDepensesEquipement, emploisInvestissementRemboursementEmpruntsDettesAssimilees,
         autoFinancementCAF, autoFinancementCAFNette,
         endettementEncoursTotalDetteAu31DecembreN, endettementAnnuiteDette,
         endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques, endettementEncoursDettesBancairesEtAssimilees,
         fiscLocalBaseNetteTaxeHabitation, fiscLocalBaseNetteTaxeFonciereProprietesBaties,
         fiscLocalBaseNetteTaxeFonciereProprietesNonBaties, fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties, fiscLocalBaseNetteCotisationFonciereEntreprises,
         impotsLocauxTaxeHabitation, impotsLocauxTaxeFonciereProprietesBaties,
         impotsLocauxTaxeFonciereProprietesNonBaties, impotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties, impotsLocauxCotisationFonciereEntreprises,
         produitsImpotsRepartitionCotisationValeurAjouteeEntreprises, produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau,
         produitsImpotsRepartitionTaxeSurfacesCommerciales,
         ratios);
   }

   /**
    * Extraire un MR.
    * @param libelleComptesIndividuels Libellé du compte individuel.
    * @param prefixe Préfixe des champs à lire dans l'enregistrement.
    * @param avecMoyenne true si la/les moyenne(s) strates doivent être extraites également,<br>
    * false s'il n'y en a pas.
    * @param row Enregistrement.
    * @return MR.
    */
   @SuppressWarnings("unchecked")
   protected MR mr(String libelleComptesIndividuels, String prefixe, boolean avecMoyenne, Row row) {
      return avecMoyenne
         ? (MR)new ComptesAvecMoyenneStrate(libelleComptesIndividuels, (Double)row.getAs(prefixe + "Total"), (Double)row.getAs(prefixe + "ParHabitant"), (Double)row.getAs(prefixe + "MoyenneStrate"))
         : (MR)new ComptesAvecRatioStructure(libelleComptesIndividuels, (Double)row.getAs(prefixe + "Total"), (Double)row.getAs(prefixe + "ParHabitant"), (Double)row.getAs(prefixe + "RatioStructure"));
   }

   /**
    * Extraire un RS.
    * @param libelleComptesIndividuels Libellé du compte individuel.
    * @param prefixe Préfixe des champs à lire dans l'enregistrement.
    * @param avecMoyenne true si la moyenne.
    * @param row Enregistrement.
    * @return RS.
    */
   @SuppressWarnings("unchecked")
   protected RS rs(String libelleComptesIndividuels, String prefixe, boolean avecMoyenne, Row row) {
      return avecMoyenne
         ? (RS)new ComptesAvecStrateEtRatioStructureEtStrate(libelleComptesIndividuels, (Double)row.getAs(prefixe + "Total"), (Double)row.getAs(prefixe + "ParHabitant"), (Double)row.getAs(prefixe + "MoyenneStrate"),
         (Double)row.getAs(prefixe + "RatioStructure"), (Double)row.getAs(prefixe + "RatioStrate"))
         : (RS)new ComptesAvecRatioStructure(libelleComptesIndividuels, (Double)row.getAs(prefixe + "Total"), (Double)row.getAs(prefixe + "ParHabitant"), (Double)row.getAs(prefixe + "RatioStructure"));
   }

   /**
    * Extraire un RV.
    * @param libelleComptesIndividuels Libellé du compte individuel.
    * @param prefixe Préfixe des champs à lire dans l'enregistrement.
    * @param avecMoyenne true si la moyenne.
    * @param row Enregistrement.
    * @return RV.
    */
   @SuppressWarnings("unchecked")
   protected RV rv(String libelleComptesIndividuels, String prefixe, boolean avecMoyenne, Row row) {
      return avecMoyenne
         ? (RV)new ComptesAvecStrateEtReductionVotee(libelleComptesIndividuels, (Double)row.getAs(prefixe + "Total"), (Double)row.getAs(prefixe + "ParHabitant"), (Double)row.getAs(prefixe + "MoyenneStrate"),
         (Double)row.getAs(prefixe + "ReductionVoteeTotal"), (Double)row.getAs(prefixe + "ReductionVoteeParHabitant"), (Double)row.getAs(prefixe + "ReductionVoteeMoyenneStrate"))
         : (RV)new ComptesAvecReductionVotee(libelleComptesIndividuels, (Double)row.getAs(prefixe + "Total"), (Double)row.getAs(prefixe + "ParHabitant"), (Double)row.getAs(prefixe + "ReductionVoteeTotal"));
   }

   /**
    * Extraire un TV.
    * @param libelleComptesIndividuels Libellé du compte individuel.
    * @param prefixe Préfixe des champs à lire dans l'enregistrement.
    * @param avecMoyenne true si la moyenne.
    * @param row Enregistrement.
    * @return TV.
    */
   @SuppressWarnings("unchecked")
   protected TV tv(String libelleComptesIndividuels, String prefixe, boolean avecMoyenne, Row row) {
      return avecMoyenne
         ? (TV)new ComptesAvecStrateEtTauxVoteEtStrate(libelleComptesIndividuels, (Double)row.getAs(prefixe + "Total"), (Double)row.getAs(prefixe + "ParHabitant"), (Double)row.getAs(prefixe + "MoyenneStrate"),
         (Double)row.getAs(prefixe + "TauxVote"), (Double)row.getAs(prefixe + "TauxMoyenStrate"))
         : (TV)new ComptesAvecTauxVote(libelleComptesIndividuels, (Double)row.getAs(prefixe + "Total"), (Double)row.getAs(prefixe + "ParHabitant"), (Double)row.getAs(prefixe + "TauxVote"));
   }
}
