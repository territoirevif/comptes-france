package fr.ecoemploi.adapters.outbound.spark.dataset.individuel;

import java.io.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

/**
 * Classe de base des chargeurs de CSV pour les comptes individuels
 * @author Marc Le Bihan
 */
public abstract class AbstractComptesIndividuelsRowCsvLoader extends AbstractSparkCsvLoader {
   @Serial
   private static final long serialVersionUID = -8388269722952078052L;

   /** Nom du fichier CSV. */
   private final String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${comptes.dir}")
   protected String repertoireFichier;

   /** Session Spark. */
   @Autowired
   protected SparkSession session;

   /**
    * Construire un chargeur de comptes depuis un fichier CSV.
    * @param nomFichier Nom du fichier
    */
   protected AbstractComptesIndividuelsRowCsvLoader(String nomFichier) {
      this.nomFichier = nomFichier;
   }

   /**
    * Charger un Dataset Row des balances des comptes individuels.
    * @param anneeComptable Année comptable.
    * @return Dataset des comptes des communes.
    */
   public Dataset<Row> loadOpenData(int anneeComptable) {
      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, anneeComptable, this.nomFichier);

      return rename(this.load(schema(anneeComptable), source), anneeComptable);
   }

   /**
    * Provoquer le chargement effectif de la source de données.
    * @param schema Schéma.
    */
   protected Dataset<Row> load(StructType schema, File source) {
      return this.session.read().schema(schema).format("csv")
         .option("header","true")
         .option("quote", "\"")
         .option("escape", "\"")
         .option("dec", ".")
         .option("sep", ";")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath());
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Dataset<Row> cast(Dataset<Row> dataset, int annee) {
      // Calcul des capacités de désendettement.
      double nombreAnneesDesendettementCAFNegative = 25.0;
      double nombreAnneeeDesendettementLimite = 20.0;

      // Calcul de la capacité de désendettement brute, en années.
      Column colCapaciteDesendettement =
         when(col("autoFinancementCAFTotal").equalTo(lit(0.0)), lit(Double.NaN))
            .otherwise(col("endettementEncoursTotalDetteAu31DecembreNTotal").divide(col("autoFinancementCAFTotal"))).cast(DoubleType);

      // Cette capacité de désendettement peut être négative parce que la CAF dela commune l'est.
      // Egalement elle peut être très grande si la CAF est très faible alors que le montant de la dette est très grand.
      // Dans ce cas, il faut rectifier les chiffres.
      Column colCapaciteDesendettementCorrigee =
         when(col("autoFinancementCAFTotal").$less$eq(lit(0.0)), lit(nombreAnneesDesendettementCAFNegative))
            .when(colCapaciteDesendettement.$less(0.0), lit(0))
            .when(colCapaciteDesendettement.$greater(nombreAnneeeDesendettementLimite), lit(nombreAnneeeDesendettementLimite))
            .otherwise(colCapaciteDesendettement).cast(DoubleType);

      Column capaciteDesendettementCorrigeeArrondie = round(colCapaciteDesendettementCorrigee, 0).cast(IntegerType);

      return dataset.withColumn("capaciteDesendettement", colCapaciteDesendettement)
         .withColumn("capaciteDesendettementCorrigee", colCapaciteDesendettementCorrigee)
         .withColumn("capaciteDesendettementCorrigeeArrondie", capaciteDesendettementCorrigeeArrondie);
   }

   /**
    * Renvoyer le schéma initial du dataset
    * @return Schéma
    */
   public abstract StructType schema(int annee);
}
