package fr.ecoemploi.adapters.outbound.spark.dataset.balance.intercommunalites;

import java.io.Serial;
import java.util.*;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

import org.apache.spark.sql.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import fr.ecoemploi.adapters.outbound.port.compte.BalanceCompteIndividuelIntercommunaliteRepository;

import fr.ecoemploi.adapters.outbound.spark.dataset.balance.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.individuel.communes.ComptesIndividuelsCommunesDataset;

import fr.ecoemploi.domain.utils.objets.TechniqueException;
import fr.ecoemploi.domain.model.territoire.SIREN;
import fr.ecoemploi.domain.model.territoire.comptabilite.ComptesIndividuelsCommune;

/**
 * Dataset de balance des comptes des intercommunalités.
 * @author Marc Le Bihan
 */
@Service
public class BalanceComptesIntercommunalitesDataset extends AbstractBalanceComptesDataset implements BalanceCompteIndividuelIntercommunaliteRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -1961175630401564304L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(BalanceComptesIntercommunalitesDataset.class);

   /** Dataset du Code Officiel Géographique. */
   private final CogDataset cogDataset;

   /** Dataset des départements. */
   private final DepartementDataset departementsDataset;

   /** Dataset des comptes individuels. */
   private final ComptesIndividuelsCommunesDataset datasetComptesIndividuelsCommunes;

   /** Chargeur de CSV. */
   private final BalanceComptesIntercommunaliteRowCsvLoader rowCsvLoader;

   /** Nom du fichier de cache. */
   private static final String CACHE = "comptes-balance-intercommunalites";

   /**
    * Construire un dataset de lecture des balances des comptes de communes.
    * @param cogDataset Dataset du Code Officiel Géographique.
    * @param departementsDataset Dataset des départements.
    * @param datasetComptesIndividuelsCommunes Dataset des comptes individuels
    * @param rowCsvLoader Chargeur de CSV.
    * @param rowValidator Validateur de Row.
    * @param encoder Encodeur d'objet métier depuis un Row
    */
   @Autowired
   public BalanceComptesIntercommunalitesDataset(CogDataset cogDataset, DepartementDataset departementsDataset, ComptesIndividuelsCommunesDataset datasetComptesIndividuelsCommunes,
      BalanceComptesIntercommunaliteRowCsvLoader rowCsvLoader, BalanceCompteIntercommunaliteRowValidator rowValidator, CompteEncoder encoder) {
      super(rowCsvLoader, rowValidator, encoder, true);
      this.cogDataset = cogDataset;
      this.departementsDataset = departementsDataset;
      this.datasetComptesIndividuelsCommunes = datasetComptesIndividuelsCommunes;
      this.rowCsvLoader = rowCsvLoader;
   }

   /**
    * Obtenir les comptes individuels d'une intercommunalité.
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param anneeExercice Année.
    * @param codeEPCI Code de l'intercommunalité.
    * @return Liste des comptes.
    */
   public List<ComptesIndividuelsCommune> obtenirComptesIndividuels(int anneeCOG, int anneeExercice, SIREN codeEPCI) {
      Objects.requireNonNull(codeEPCI, "Le code de l'intercommunalité ne peut pas valoir null.");

      Dataset<Row> comptesIndividuels = rowComptesIntercommunalites(null, null, anneeExercice, anneeCOG);
      return collectComptesIndividuels(comptesIndividuels, comptesIndividuels.col("codeEPCI").equalTo(codeEPCI.getId()));
   }

   /**
    * Obtenir un Dataset Row des balances des comptes des intercommunalités.
    * @param optionsCreationLecture Options d'écriture et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution
    * @param anneeComptable Année comptable.
    * @param anneeCOG année du COG pour rectifier les intercommunalités.
    * @return Dataset des communes.
    * @throws TechniqueException si un incident survient.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> rowComptesIntercommunalites(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution, int anneeComptable, int anneeCOG) {
      return rowComptes(optionsCreationLecture, historiqueExecution, new BalanceComptesTriDepartementCommuneSiretCompte(), CACHE,
         anneeComptable, anneeCOG);
   }

   /**
    * Obtenir les comptes individuels d'une commune.
    * @param comptesNonFiltres Le dataset sur lequel il faut appliquer une condition.
    * @param condition Condition à appliquer, si elle est différente de null.
    * @return Liste des comptes individuels.
    * @deprecated À remplacer par des méthodes faisant le bon tri et filtrage.
    */
   @Deprecated(forRemoval = true)
   private List<ComptesIndividuelsCommune> collectComptesIndividuels(Dataset<Row> comptesNonFiltres, Column condition) {
      Dataset<Row> filtrage = condition != null ? comptesNonFiltres.where(condition) : comptesNonFiltres;
      Dataset<ComptesIndividuelsCommune> comptes = this.datasetComptesIndividuelsCommunes.toDatasetObjetsMetiers(filtrage);
      return comptes.collectAsList();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   protected Dataset<Row> renommerEtCreerChamps(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, Dataset<Row> csv, int annee) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.addReglesValidation(this.session, validator());
      }

      Dataset<Row> comptes = this.rowCsvLoader.rename(csv, annee);
      comptes = this.rowCsvLoader.cast(comptes, annee);

      // Jonction d'autres référentiels.
      comptes = referentiel().joinReferentiel(comptes, referentielTypeEtablissement(), "typeEtablissement");
      comptes = referentiel().joinReferentielDeuxClefs(comptes, referentielSousTypeEtablissement(), "typeEtablissement", "typeEtablissement", "sousTypeEtablissement", "sousTypeEtablissement");
      comptes = referentiel().joinReferentiel(comptes, referentielActivites(), "codeActivite");
      comptes = referentiel().joinReferentiel(comptes, referentielTypeBudget(), "typeBudget");

      comptes = this.departementsDataset.withDepartement(options, historique, "codeDepartement", comptes, "codeDepartementSur3", "INSEE", false, 0);
      comptes = this.cogDataset.withCodeCommune(historique, "codeCommuneBalance", comptes, "codeDepartement", "INSEE");

      // Nouveaux champs
      comptes = comptes.withColumn("nombreChiffresNumeroCompte", length(col("numeroCompte")))
         .withColumn("budgetPrincipal", col("typeBudget").eqNullSafe("1").cast(BooleanType))
         .drop("codeDepartementSur3")
         .drop("codeINSEE")
         .drop("INSEE");

      return comptes;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   protected Dataset<Row> appliquerCOG(int anneeCOG, Dataset<Row> sirenCommunes, Dataset<Row> comptes, Dataset<Row> groupements) {
      Dataset<Row> communes = sirenCommunes.select("codeCommune", "sirenCommune", "nomCommune", "populationMunicipale");

      // Faire du dataset des comptes deux sous-ensembles :
      
      // 1) Ceux que l'on peut lier directement aux communes par leur code commune mentionné dans la balance (idéal).
      Column joinCodeCommune = comptes.col("codeCommuneBalance").equalTo(communes.col("codeCommune"));
      Dataset<Row> comptesLiesParCodeCommune = comptes.join(communes, joinCodeCommune, "left_semi");
      Dataset<Row> comptesExclusParCodeCommune = comptes.join(communes, joinCodeCommune, "left_anti");

      // 2) Ceux que l'on peut lier par le siren mentionné dans la balance avec le siren du groupement dans la déclaration des groupements, et par là, revenir aussi à sa commune siège.
      // TODO Revérifier cela, mainentant que l'on joint par périmètres, et plus par groupement
      Column joinSirenGroupements = comptes.col("siren").equalTo(groupements.col("sirenGroupement"));

      Dataset<Row> comptesLiesParSirenGroupement = comptesExclusParCodeCommune.join(groupements.select("sirenGroupement", "sirenCommuneSiege", "codeCommuneSiege"), joinSirenGroupements);

      comptesLiesParSirenGroupement = comptesLiesParSirenGroupement
         .drop(groupements.col("sirenGroupement")) // Seul le code commune siège valide nous intéresse.
         .drop(groupements.col("sirenCommuneSiege"));
      
      Dataset<Row> comptesExclusParSirenGroupement = comptesExclusParCodeCommune.join(groupements.select("sirenGroupement", "sirenCommuneSiege", "codeCommuneSiege"), joinSirenGroupements, "left_anti");

      // Reconstituer le dataset des comptes réellement traitables en réunissant ces sous-ensembles avec leurs communes liées.
      comptesLiesParCodeCommune = comptesLiesParCodeCommune.join(communes, comptesLiesParCodeCommune.col("codeCommuneBalance").equalTo(communes.col("codeCommune")));
      
      comptesLiesParSirenGroupement = comptesLiesParSirenGroupement.join(communes, comptesLiesParSirenGroupement.col("codeCommuneSiege").equalTo(communes.col("codeCommune")))
         .drop("codeCommuneSiege"); // Nous avons corrigé le commune commune à partir du code commune siège de l'intercommunalité, qui peut maintenant disparaître.
      
      // Vérifier que tous les comptes ont pu trouver leur commune.
      long nombreComptesExclus = comptesExclusParSirenGroupement.count();
      
      if (nombreComptesExclus > 0) {
         Dataset<Row> communesSansRattagement = comptesExclusParSirenGroupement.groupBy("codeCommuneBalance", "libelleBudget").count();
         long nombreCommunesSansRattachement = communesSansRattagement.count();
         
         LOGGER.warn("{} communes portant sur {} comptes n'ont pas trouvé de rattachement dans le COG {} :", nombreCommunesSansRattachement, nombreComptesExclus, anneeCOG);
         communesSansRattagement.show((int)nombreCommunesSansRattachement, false);
      }
      
      return comptesLiesParCodeCommune.union(comptesLiesParSirenGroupement)
         .drop("codeCommuneBalance")
         .drop("sirenCommune");
   }
}
