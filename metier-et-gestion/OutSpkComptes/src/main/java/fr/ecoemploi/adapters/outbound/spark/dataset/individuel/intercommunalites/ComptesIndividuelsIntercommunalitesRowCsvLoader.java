package fr.ecoemploi.adapters.outbound.spark.dataset.individuel.intercommunalites;

import java.io.Serial;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.individuel.AbstractComptesIndividuelsRowCsvLoader;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

/**
 * Chargeur de comptes individuels d'intercommunalités depuis un fichier CSV
 * @author Marc Le Bihan
 */
@Component
public class ComptesIndividuelsIntercommunalitesRowCsvLoader extends AbstractComptesIndividuelsRowCsvLoader {
   @Serial
   private static final long serialVersionUID = 6929257054902625228L;

   /**
    * Construire un chargeur de comptes depuis un fichier CSV.
    * @param nomFichier Nom du fichier
    */
   @Autowired
   public ComptesIndividuelsIntercommunalitesRowCsvLoader(@Value("${individuel-comptes-intercommunalites.csv.nom}") String nomFichier) {
      super(nomFichier); //"caftot", "entot"
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Dataset<Row> loadOpenData(int anneeComptable) {
      return super.loadOpenData(0);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Dataset<Row> rename(Dataset<Row> dataset, int annee) {
      return dataset
         .withColumnRenamed("exer", "anneeExercice")
         .withColumnRenamed("mpoid", "populationIntercommunalite")
         .withColumnRenamed("lbudg", "nomIntercommunalite")
         .withColumnRenamed("nomsst2", "typeGroupementAppartenance")

         /*
         .add("nbtotba", IntegerType, false) // Consolidation avec {{nbtotba}} budgets annexes (BA)
         .add("siren", StringType, true)    // SIREN / Code EPCI. Null pour les EPCI qui n'existent plus.
         .add("lbudg", StringType, false)    // Nom de la commune (libellé du budget)
         .add("nbbaspic", IntegerType, false)// Nombre de budgets annexes industriels et commerciaux (SPIC)
         */
         .withColumnRenamed("pftot", "produitsDeFonctionnementTotal")
         .withColumnRenamed("pftothab", "produitsDeFonctionnementParHabitant")
         .withColumnRenamed("rpftot", "produitsDeFonctionnementRatioStructure")

         // Produits de fonctionnement CAF
         .withColumnRenamed("pfr", "produitsDeFonctionnementCAFTotal")            // Produits de fonctionnement CAF, en milliers d'euros
         .withColumnRenamed("mpfr", "produitsDeFonctionnementCAFParHabitant")     // Produits de fonctionnement CAF, par habitant
         .withColumnRenamed("tpfr", "produitsDeFonctionnementCAFRatioStructure")   // Produits de fonctionnement CAF, ratio de structure

         .withColumnRenamed("iltot", "produitsDeFonctionnementCAFImpotsLocauxTotal")
         .withColumnRenamed("iltothab", "produitsDeFonctionnementCAFImpotsLocauxParHabitant")
         .withColumn("produitsDeFonctionnementCAFImpotsLocauxRatioStructure", lit(null).cast(DoubleType))  // N'a pas connu dans le ratio de structure dans une intercommunalité.

         .withColumnRenamed("aittot", "produitsDeFonctionnementCAFAutresImpotsEtTaxesTotal")
         .withColumnRenamed("aittothab", "produitsDeFonctionnementCAFAutresImpotsEtTaxesParHabitant")
         .withColumn("produitsDeFonctionnementCAFAutresImpotsEtTaxesRatioStructure", lit(null).cast(DoubleType))  // N'a pas connu dans le ratio de structure dans une intercommunalité.

         .withColumnRenamed("dgftot", "produitsDeFonctionnementCAFDGFTotal")
         .withColumnRenamed("dgftothab", "produitsDeFonctionnementCAFDGFParHabitant")
         .withColumn("produitsDeFonctionnementCAFDGFRatioStructure", lit(null).cast(DoubleType))  // N'a pas connu dans le ratio de structure dans une intercommunalité.

         .withColumnRenamed("cftot", "chargesDeFonctionnementTotal")             // TOTAL DES CHARGES DE FONCTIONNEMENT = B, en milliers d'euros
         .withColumnRenamed("cftothab", "chargesDeFonctionnementParHabitant")    // TOTAL DES CHARGES DE FONCTIONNEMENT = B, par habitant
         .withColumnRenamed("rcftot", "chargesDeFonctionnementRatioStructure")   // TOTAL DES CHARGES DE FONCTIONNEMENT = B, ratio de structure

         // Charges de fonctionnement CAF
         .withColumnRenamed("cfr", "chargesDeFonctionnementCAFTotal")             // Charges de fonctionnement CAF : total
         .withColumnRenamed("mcfr", "chargesDeFonctionnementCAFParHabitant")      // Charges de fonctionnement CAF : par habitant
         .withColumnRenamed("tcfr", "chargesDeFonctionnementCAFRatioStructure")    // Charges de fonctionnement CAF, ratio de structure

         .withColumnRenamed("perstot", "chargesDeFonctionnementCAFChargesDePersonnelTotal")
         .withColumnRenamed("perstothab", "chargesDeFonctionnementCAFChargesDePersonnelParHabitant")
         .withColumnRenamed("rperstot", "chargesDeFonctionnementCAFChargesDePersonnelRatioStructure")

         .withColumnRenamed("acetot", "chargesDeFonctionnementCAFAchatsEtChargesExternesTotal")
         .withColumnRenamed("acetothab", "chargesDeFonctionnementCAFAchatsEtChargesExternesParHabitant")
         .withColumnRenamed("racetot", "chargesDeFonctionnementCAFAchatsEtChargesExternesRatioStructure")

         .withColumnRenamed("cfitot", "chargesDeFonctionnementCAFChargesFinancieresTotal")
         .withColumnRenamed("cfitothab", "chargesDeFonctionnementCAFChargesFinancieresParHabitant")
         .withColumnRenamed("rcfitot", "chargesDeFonctionnementCAFChargesFinancieresRatioStructure")

         .withColumnRenamed("suvftot", "chargesDeFonctionnementCAFSubventionsVerseesTotal")
         .withColumnRenamed("suvftothab", "chargesDeFonctionnementCAFSubventionsVerseesParHabitant")
         .withColumnRenamed("rsuvftot", "chargesDeFonctionnementCAFSubventionsVerseesRatioStructure")

         .withColumnRenamed("rtot", "resultatComptableTotal")
         .withColumnRenamed("rtothab", "resultatComptableParHabitant")
         .withColumnRenamed("rrtot", "resultatComptableRatioStructure")

         .withColumnRenamed("ritot", "ressourcesInvestissementTotal")
         .withColumnRenamed("ritothab", "ressourcesInvestissementParHabitant")
         .withColumnRenamed("rritot", "ressourcesInvestissementRatioStructure")

         .withColumnRenamed("rdettot", "ressourcesInvestissementEmpruntsBancairesDettesAssimileesTotal")
         .withColumnRenamed("rdettothab", "ressourcesInvestissementEmpruntsBancairesDettesAssimileesParHabitant")
         .withColumnRenamed("rrdettot", "ressourcesInvestissementEmpruntsBancairesDettesAssimileesRatioStructure")

         .withColumnRenamed("subvitot", "ressourcesInvestissementSubventionsRecuesTotal")
         .withColumnRenamed("subvitothab", "ressourcesInvestissementSubventionsRecuesParHabitant")
         .withColumnRenamed("rsubvitot", "ressourcesInvestissementSubventionsRecuesRatioStructure")

         .withColumnRenamed("fattot", "ressourcesInvestissementFCTVATotal")
         .withColumnRenamed("fattothab", "ressourcesInvestissementFCTVAParHabitant")
         .withColumnRenamed("rfattot", "ressourcesInvestissementFCTVARatioStructure")

         .withColumnRenamed("eitot", "emploisInvestissementTotal")           // total
         .withColumnRenamed("eitothab", "emploisInvestissementParHabitant")    // par habitant
         .withColumnRenamed("reitot", "emploisInvestissementRatioStructure")  // moyenne strate

         // Dépenses d'équipement
         .withColumnRenamed("detot", "emploisInvestissementDepensesEquipementTotal")            // EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement : total
         .withColumnRenamed("detothab", "emploisInvestissementDepensesEquipementParHabitant")     // EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement : par habitant
         .withColumnRenamed("rdetot", "emploisInvestissementDepensesEquipementRatioStructure")  // EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement : ratio de structure

         // Remboursement d'emprunts et dettes assimilées
         .withColumnRenamed("edettot", "emploisInvestissementRemboursementEmpruntsDettesAssimileesTotal")            // EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées : total
         .withColumnRenamed("edettothab", "emploisInvestissementRemboursementEmpruntsDettesAssimileesParHabitant")     // EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées : par habitant
         .withColumnRenamed("redettot", "emploisInvestissementRemboursementEmpruntsDettesAssimileesRatioStructure")  // EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées : ratio de structure

         // Capacité d'autofinancement = CAF
         .withColumnRenamed("caftot", "autoFinancementCAFTotal")           // AUTOFINANCEMENT : Capacité d'autofinancement = CAF : total
         .withColumnRenamed("caftothab", "autoFinancementCAFParHabitant")    // AUTOFINANCEMENT : Capacité d'autofinancement = CAF : par habitant
         .withColumnRenamed("rcaftot", "autoFinancementCAFRatioStructure") // AUTOFINANCEMENT : Capacité d'autofinancement = CAF : ratio de structure

         // CAF nette du remboursement en capital des emprunts
         .withColumnRenamed("cafntot", "autoFinancementCAFNetteTotal")           // AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts : total
         .withColumnRenamed("cafntothab", "autoFinancementCAFNetteParHabitant")    // AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts : par habitant
         .withColumnRenamed("rcafntot", "autoFinancementCAFNetteRatioStructure") // AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts : ratio de structure

         // ENDETTEMENT
         // Encours total de la dette au 31 décembre N
         .withColumnRenamed("entot", "endettementEncoursTotalDetteAu31DecembreNTotal")             // ENDETTEMENT : Encours total de la dette au 31 décembre N : total
         .withColumnRenamed("entothab", "endettementEncoursTotalDetteAu31DecembreNParHabitant")      // ENDETTEMENT : Encours total de la dette au 31 décembre N : par habitant
         .withColumnRenamed("rentot", "endettementEncoursTotalDetteAu31DecembreNRatioStructure")   // ENDETTEMENT : Encours total de la dette au 31 décembre N : ratio de structure

         // Encours des dettes bancaires et assimilées
         .withColumnRenamed("encdb", "endettementEncoursDettesBancairesEtAssimileesTotal")          // ENDETTEMENT : Encours des dettes bancaires et assimilées : total
         .withColumnRenamed("mencdb", "endettementEncoursDettesBancairesEtAssimileesParHabitant")   // ENDETTEMENT : Encours des dettes bancaires et assimilées : par habitant
         .withColumnRenamed("tencdb", "endettementEncoursDettesBancairesEtAssimileesRatioStructure")// ENDETTEMENT : Encours des dettes bancaires et assimilées : ratio de structure

         .withColumnRenamed("encdbr", "endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiquesTotal")            // ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques : total
         .withColumnRenamed("mencdbr", "endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiquesParHabitant")     // ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques : par habitant
         .withColumnRenamed("tencdbr", "endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiquesRatioStructure")  // ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques : ratio de structure

         // Annuité de la dette
         .withColumnRenamed("antot", "endettementAnnuiteDetteTotal")           // ENDETTEMENT : Annuité de la dette : total
         .withColumnRenamed("antothab", "endettementAnnuiteDetteParHabitant")    // ENDETTEMENT : Annuité de la dette : par habitant
         .withColumnRenamed("rantot", "endettementAnnuiteDetteRatioStructure") // ENDETTEMENT : Annuité de la dette : ratio de structure

         // ELEMENTS DE FISCALITE DIRECTE LOCALE
         // Bases nettes imposées au profit de la commune
         // Taxe d'habitation (y compris THLV)
         .withColumnRenamed("bth", "fiscLocalBaseNetteTaxeHabitationTotal")                            // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : total
         .withColumnRenamed("fbth", "fiscLocalBaseNetteTaxeHabitationParHabitant")                     // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : par habitant
         .withColumnRenamed("bthexod", "fiscLocalBaseNetteTaxeHabitationReductionVoteeTotal")          // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : Réductions de base accordées sur délibérations : total

         // Taxe foncière sur les propriétés bâties
         .withColumnRenamed("bfb", "fiscLocalBaseNetteTaxeFonciereProprietesBatiesTotal")                             // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : total
         .withColumnRenamed("fbfb", "fiscLocalBaseNetteTaxeFonciereProprietesBatiesParHabitant")                      // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : par habitant
         .withColumnRenamed("bfbexod", "fiscLocalBaseNetteTaxeFonciereProprietesBatiesReductionVoteeTotal")           // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : Réductions de base accordées sur délibérations : total

         // Taxe foncière sur les propriétés non bâties
         .withColumnRenamed("bfnb", "fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesTotal")                            // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : total
         .withColumnRenamed("fbfnb", "fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesParHabitant")                     // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : par habitant
         .withColumnRenamed("bfnbexod", "fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesReductionVoteeTotal")          // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : Réductions de base accordées sur délibérations : total

         // Taxe additionnelle à la taxe foncière sur les propriétés non bâties
         .withColumnRenamed("btafnb", "fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBatiesTotal")          // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : total
         .withColumnRenamed("fbtafnb", "fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBatiesParHabitant")   // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : par habitant
         .withColumn("fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBatiesRatioStructure", lit(null).cast(DoubleType))       // N'est pas connu dans le ratio de structure.

         // TODO Simplification : la CFE : Professionnelle + additionnelle + éolienne, rassemblées dans une seule CFE.
         .withColumn("fiscLocalBaseNetteCotisationFonciereEntreprisesTotal", col("bcfe1").plus(col("bcfe2")).plus(col("bcfe3")))
         .withColumn("fiscLocalBaseNetteCotisationFonciereEntreprisesParHabitant", col("fbcfe1").plus(col("fbcfe2")).plus(col("fbcfe3")))
         .withColumn("fiscLocalBaseNetteCotisationFonciereEntreprisesReductionVoteeTotal", col("bcfeexod"))

         .withColumnRenamed("pth", "impotsLocauxTaxeHabitationTotal")
         .withColumnRenamed("fpth", "impotsLocauxTaxeHabitationParHabitant")
         .withColumnRenamed("tth", "impotsLocauxTaxeHabitationTauxVote")

         .withColumnRenamed("pfb", "impotsLocauxTaxeFonciereProprietesBatiesTotal")
         .withColumnRenamed("fpfb", "impotsLocauxTaxeFonciereProprietesBatiesParHabitant")
         .withColumnRenamed("tfb", "impotsLocauxTaxeFonciereProprietesBatiesTauxVote")

         .withColumnRenamed("pfnb", "impotsLocauxTaxeFonciereProprietesNonBatiesTotal")
         .withColumnRenamed("fpfnb", "impotsLocauxTaxeFonciereProprietesNonBatiesParHabitant")
         .withColumnRenamed("tfnb", "impotsLocauxTaxeFonciereProprietesNonBatiesTauxVote")

         .withColumnRenamed("ttp", "impotsLocauxCotisationFonciereEntreprisesTauxVote")
         .withColumnRenamed("tmtp", "impotsLocauxCotisationFonciereEntreprisesTauxMoyenStrate")

         // Produits des impôts locaux (suite)
         .withColumnRenamed("ptafnb", "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesTotal")               // Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : total
         .withColumnRenamed("fptafnb", "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesParHabitant")        // Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : par habitant
         .withColumnRenamed("tafnb", "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesTauxVote")             // Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : taux voté

         // TODO Simplification : la CFE : Professionnelle + additionnelle + éolienne, rassemblées dans une seule CFE.
         .withColumn("impotsLocauxCotisationFonciereEntreprisesTotal", col("pcfe1").plus(col("pcfe2")).plus(col("pcfe3")))
         .withColumn("impotsLocauxCotisationFonciereEntreprisesParHabitant", col("fpcfe1").plus(col("fpcfe2")).plus(col("fpcfe3")))
         .withColumn("impotsLocauxCotisationFonciereEntreprisesTauxVote", col("tcfe1").plus(col("tcfe2")).plus(col("tcfe3")))

         .withColumnRenamed("cvaeg", "produitsImpotsRepartitionCotisationValeurAjouteeEntreprisesTotal")          // Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises : total
         .withColumnRenamed("fcvaeg", "produitsImpotsRepartitionCotisationValeurAjouteeEntreprisesParHabitant")   // Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises : par habitant
         .withColumn("produitsImpotsRepartitionCotisationValeurAjouteeEntreprisesRatioStructure", lit(null).cast(DoubleType))      // N'est pas connu dans le ratio de structure.

         .withColumnRenamed("iferg", "produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseauTotal")       // Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau : total
         .withColumnRenamed("fiferg", "produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseauParHabitant")// Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau : par habitant
         .withColumn("produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseauRatioStructure", lit(null).cast(DoubleType))   // N'est pas connu dans le ratio de structure.

         .withColumnRenamed("tascomg", "produitsImpotsRepartitionTaxeSurfacesCommercialesTotal")            // Produits des impôts de répartition : Taxe sur les surfaces commerciales : total
         .withColumnRenamed("ftascomg", "produitsImpotsRepartitionTaxeSurfacesCommercialesParHabitant")     // Produits des impôts de répartition : Taxe sur les surfaces commerciales : par habitant
         .withColumn("produitsImpotsRepartitionTaxeSurfacesCommercialesRatioStructure", lit(null).cast(DoubleType))          // N'est pas connu dans le ratio de structure.

         .withColumnRenamed("nomsst1", "libelleStrateCommune");  // Strate commune
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public StructType schema(int annee) {
      /* Intercommunalités
      SIREN       :   siren            Libellé du budget : lbudg
      Population  :   mpoid
      Consolidation avec {{nbtotba}} budgets annexes (BA) dont {{nbbaspic}} budgets annexes industriels et commerciaux (SPIC)
      Exercice    : exer -  N° de département :  ndept

                                        ANALYSE DES EQUILIBRES FINANCIERS FONDAMENTAUX
      En milliers d'Euros  En euros par habitant                                                      Ratios de structure   % des budgets annexes dans le total

                                             OPERATIONS DE FONCTIONNEMENT
      pftot             pftothab             TOTAL DES PRODUITS DE FONCTIONNEMENT = A                       rpftot
      pfr               mpfr                 Produits de fonctionnement CAF                                 tpfr
      iltot             iltothab             dont : Impôts Locaux
      revtot            revtothab               Reversement de fiscalité
      aittot            aittothab               Autres impôts et taxes
      dgftot            dgftothab               Dotation globale de fonctionnement
      dfctva            mdfctva              FCTVA                                                          tdfctva
      pserdom           mpserdom             Produits des services et du domaine                            tpserdom

      cftot             cftothab             TOTAL DES CHARGES DE FONCTIONNEMENT = B                        rcftot
      cfr               mcfr                 Charges de fonctionnement CAF                                  tcfr
      perstot           perstothab           dont : Charges de personnel                                    rperstot
      acetot            acetothab               Achats et charges externes                                  racetot
      cfitot            cfitothab               Charges financières                                         rcfitot
      suvftot           suvftothab              Subventions versées                                         rsuvftot
      rtot              rtothab              Résultat comptable = A - B = R                                 rrtot

                                             OPERATIONS D'INVESTISSEMENT
      ritot             ritothab             TOTAL DES RESSOURCES D'INVESTISSEMENT = C                      rritot
      rdettot           rdettothab           dont : Emprunts bancaires et dettes assimilées                 rrdettot
      subvitot          subvitothab          Subventions reçues                                             rsubvitot
      fattot            fattothab            FCTVA                                                          rfattot

      eitot             eitothab             TOTAL DES EMPLOIS D'INVESTISSEMENT = D                         reitot
      detot             detothab             dont : Dépenses d'équipement                                   rdetot
      edettot           edettothab           Remboursement d'emprunts et dettes assimilées                  redettot

                                             AUTOFINANCEMENT   en % des BA SPIC par rapport au total
      caftot            caftothab            Capacité d'autofinancement = CAF                               rcaftot
      cafntot           cafntothab           CAF nette du remboursement en capital des emprunts             rcafntot

                                             ENDETTEMENT en % des BA SPIC par rapport au total
      entot             entothab             Encours total de la dette au 31/12/N                           rentot
      encdb             mencdb               Encours des dettes bancaires et assimilées                     tencdb
      encdbr            mencdbr              Encours des dettes bancaires net de l'aide du fonds de soutien pour la sortie des emprunts toxiques   tencdbr
      antot             antothab             Annuité de la dette                                            rantot

                                          ELEMENTS DE FISCALITE LOCALE
                          Les bases imposées et les réductions (exonérations, abattements) accordées sur délibérations
      Bases nettes imposées au profit du GFP                                           Taxe  Réductions de bases accordées sur délibérations
      En milliers d'Euros  En euros par habitant      En milliers d'Euros
      bth               fbth                 Taxe d'habitation                                              bthexod
      bfb               fbfb                 Taxe foncière sur les propriétés bâties                        bfbexod
      bfnb              fbfnb                Taxe foncière sur les propriétés non bâties                    bfnbexod
      btafnb            fbtafnb              Taxe additionnelle à la taxe sur les propriétés non bâties
      bcfe1             fbcfe1               Cotisation foncière des entreprises (fiscalité additionnelle)  bcfeexod
      bcfe2             fbcfe2               Cotisation foncière des entreprises (fiscalité prof. unique ou de zone)
      bcfe3             fbcfe3               Cotisation foncière des entreprises (fiscalité des éoliennes)

                           Les taux et les produits de la fiscalité directe locale
      Produits des impôts locaux    Taxe  Taux (%)
      pth               fpth                 Taxe d'habitation (dont THLV et GEMAPI)                        tth
      pfb               fpfb                 Taxe foncière sur les propriétés bâties (dont GEMAPI)          tfb
      pfnb              fpfnb                Taxe foncière sur les propriétés non bâties (dont GEMAPI)      tfnb
      ptafnb            fptafnb              Taxe additionnelle à la taxe sur les propriétés non bâties     tafnb
      pcfe1             fpcfe1               Cotisation foncière des entreprises (fiscalité additionnelle-hors GEMAPI)        tcfe1
      pcfe2             fpcfe2               Cotis. foncière des entreprises (fiscalité prof. unique ou de zone- hors GEMAPI) tcfe2
      pcfe3             fpcfe3               Cotisation foncière des entreprises (fiscalité des éoliennes- hors GEMAPI)       tcfe3

                           Les produits des impôts de répartition
      cvaeg             fcvaeg               Cotisation Valeur Ajoutée des Entreprises (tous régimes fiscaux confondus)
      iferg             fiferg               Imposition forfaitaire sur les entreprises de réseau
      tascomg           ftascomg             Taxe sur les surfaces commerciales
      */

      return new StructType()
         // mpoid lbudg                  exer nbbaspic   pftot      pftothab rpftot   iltot     iltothab revtot      revtothab   aittot   aittothab   dgftot
         // 92880 CA AEROPORT DU BOURGET 2013 0          34904.0    376.0    0.0      21795.0   235.0    -17346.0    -187.0      9754.0   105.0       15200.0

         // dgftothab   cftot    cftothab rcftot   perstot  perstothab  rperstot acetot   acetothab  racetot  cfitot   cfitothab   rcfitot
         // 164.0       34315.0  369.0    0.0      11256.0  121.0       0.0      16151.0  174.0       0.0     483.0    5.0         0.0

         // suvftot  suvftothab  rsuvftot rtot  rtothab  rrtot ritot    ritothab rritot   rdettot  rdettothab  rrdettot subvitot subvitothab
         // 180.0    2.0         0.0      590.0 6.0      0.0   8431.0   91.0     0.0      3000.0   32.0        0.0      532.0    6.0

         // rsubvitot   fattot   fattothab   rfattot  eitot    eitothab reitot   detot    detothab rdetot   edettot  edettothab  redettot caftot
         // 0.0         711.0    8.0         0.0      8569.0   92.0     0.0      7399.0   80.0     0.0      994.0    11.0        0.0      2421.0

         // caftothab   rcaftot  cafntot  cafntothab  rcafntot entot    entothab rentot   antot    antothab rantot   bth         fbth       bthexod  bfb fbfb
         // 26.0        0.0      1427.0   15.0        0.0      21130.0  227.0    0.0      1475.0   16.0     0.0      113816.0    1225.0     20332.0  0.0 0.0

         // bfbexod  bfnb     fbfnb    bfnbexod btafnb   fbtafnb  bcfe1    fbcfe1   bcfeexod bcfe2    fbcfe2   bcfe3    fbcfe3  pth       fpth    tth   pfb   fpfb   tfb
         // 0.0      376.0    4.0      0.0      375.0    4.0      0.0      0.0      0.0      23342.0  251.0    0.0      0.0     10073.0   108.0   8.85  0.0   0.0    0.0

         // pfnb  fpfnb  tfnb  ptafnb     fptafnb  tafnb pcfe1 fpcfe1   tcfe1 pcfe2    fpcfe2   tcfe2 pcfe3 fpcfe3   tcfe3    cvaeg    fcvaeg   iferg fiferg
         // 6.0    0.0   1.57    67.0     1.0      17.73 0.0   0.0      0.0   8595.0   93.0     36.82 0.0   0.0      0.0      2650.0   29.0     242.0 3.0

         // tascomg  ftascomg nbtotba  siren encdbr   mencdbr  tencdbr  pfr   mpfr  tpfr  cfr   mcfr  tcfr  encdb mencdb   tencdb
         // 570.0    6.0      0


      // ndept;mpoid;lbudg;exer;nbbaspic;pftot;pftothab;rpftot;iltot;iltothab;revtot;revtothab;aittot;aittothab;dgftot;dgftothab;cftot;cftothab;rcftot;perstot;perstothab;rperstot;acetot;acetothab;racetot;cfitot;cfitothab;rcfitot;suvftot;suvftothab;rsuvftot;rtot;rtothab;rrtot;ritot;ritothab;rritot;rdettot;rdettothab;rrdettot;subvitot;subvitothab;rsubvitot;fattot;fattothab;rfattot;eitot;eitothab;reitot;detot;detothab;rdetot;edettot;edettothab;redettot;caftot;caftothab;rcaftot;cafntot;cafntothab;rcafntot;entot;entothab;rentot;antot;antothab;rantot;bth;fbth;bthexod;bfb;fbfb;bfbexod;bfnb;fbfnb;bfnbexod;btafnb;fbtafnb;bcfe1;fbcfe1;bcfeexod;bcfe2;fbcfe2;bcfe3;fbcfe3;pth;fpth;tth;pfb;fpfb;tfb;pfnb;fpfnb;tfnb;ptafnb;fptafnb;tafnb;pcfe1;fpcfe1;tcfe1;pcfe2;fpcfe2;tcfe2;pcfe3;fpcfe3;tcfe3;cvaeg;fcvaeg;iferg;fiferg;tascomg;ftascomg;nbtotba;siren;encdbr;mencdbr;tencdbr;pfr;mpfr;tpfr;cfr;mcfr;tcfr;encdb;mencdb;tencdb

         // Entête
         .add("ndept", StringType, true)     // Code département
         .add("mpoid", IntegerType, true)    // Population légale en vigueur au 1er janvier de l'exercice
         .add("lbudg", StringType, false)    // Nom de la commune (libellé du budget)
         .add("exer", IntegerType, false)    // Exercice

         .add("nbbaspic", IntegerType, false)// Nombre de budgets annexes industriels et commerciaux (SPIC)

         .add("pftot", DoubleType, true)     // TOTAL DES PRODUITS DE FONCTIONNEMENT = A, en milliers d'euros
         .add("pftothab", DoubleType, true)  // TOTAL DES PRODUITS DE FONCTIONNEMENT = A, par habitant
         .add("rpftot", DoubleType, true)    // TOTAL DES PRODUITS DE FONCTIONNEMENT = A, ratio de structure

         .add("iltot", DoubleType, true)     // dont : Impôts Locaux, par habitant
         .add("iltothab", DoubleType, true)  // dont : Impôts Locaux, ratio de structure

         .add("revtot", DoubleType, true)    // Reversement de fiscalité, en milliers d'euros
         .add("revtothab", DoubleType, true) // Reversement de fiscalité, par habitant

         .add("aittot", DoubleType, true)    // Autres impôts et taxes, en milliers d'euros
         .add("aittothab", DoubleType, true) // Autres impôts et taxes, par habitant

         .add("dgftot", DoubleType, true)    // Dotation globale de fonctionnement, en milliers d'euros
         .add("dgftothab", DoubleType, true) // Dotation globale de fonctionnement, par habitant

         .add("cftot", DoubleType, true)     // TOTAL DES CHARGES DE FONCTIONNEMENT = B, en milliers d'euros
         .add("cftothab", DoubleType, true)  // TOTAL DES CHARGES DE FONCTIONNEMENT = B, par habitant
         .add("rcftot", DoubleType, true)    // TOTAL DES CHARGES DE FONCTIONNEMENT = B, ratio de structure

         .add("perstot", DoubleType, true)   // dont : Charges de personnel, en milliers d'euros
         .add("perstothab", DoubleType, true)// dont : Charges de personnel, par habitant
         .add("rperstot", DoubleType, true)  // dont : Charges de personnel, ratio de structure

         .add("acetot", DoubleType, true)    // Achats et charges externes, en milliers d'euros
         .add("acetothab", DoubleType, true) // Achats et charges externes, par habitant
         .add("racetot", DoubleType, true)   // Achats et charges externes, ratio de structure

         .add("cfitot", DoubleType, true)    // Charges financières, en milliers d'euros
         .add("cfitothab", DoubleType, true) // Charges financières, par habitant
         .add("rcfitot", DoubleType, true)   // Charges financières, ratio de structure

         .add("suvftot", DoubleType, true)   // Subventions versées, en milliers d'euros
         .add("suvftothab", DoubleType, true)// Subventions versées, par habitant
         .add("rsuvftot", DoubleType, true)  // Subventions versées, ratio de structure

         .add("rtot", DoubleType, true)      // Résultat comptable = A - B = R, en milliers d'euros
         .add("rtothab", DoubleType, true)   // Résultat comptable = A - B = R, par habitant
         .add("rrtot", DoubleType, true)     // Résultat comptable = A - B = R, ratio de structure

         .add("ritot", DoubleType, true)     // TOTAL DES RESSOURCES D'INVESTISSEMENT = C, en milliers d'euros
         .add("ritothab", DoubleType, true)  // TOTAL DES RESSOURCES D'INVESTISSEMENT = C, par habitant
         .add("rritot", DoubleType, true)    // TOTAL DES RESSOURCES D'INVESTISSEMENT = C, ratio de structure

         .add("rdettot", DoubleType, true)   // dont : Emprunts bancaires et dettes assimilées, en milliers d'euros
         .add("rdettothab", DoubleType, true)// dont : Emprunts bancaires et dettes assimilées, par habitant
         .add("rrdettot", DoubleType, true)  // dont : Emprunts bancaires et dettes assimilées, ratio de structure

         .add("subvitot", DoubleType, true)  // Subventions reçues, en milliers d'euros
         .add("subvitothab", DoubleType, true)   // Subventions reçues, par habitant
         .add("rsubvitot", DoubleType, true) // Subventions reçues, ratio de structure

         .add("fattot", DoubleType, true)    // FCTVA RESSOURCES D'INVESTISSEMENT, en milliers d'euros
         .add("fattothab", DoubleType, true) // FCTVA RESSOURCES D'INVESTISSEMENT, par habitant
         .add("rfattot", DoubleType, true)   // FCTVA RESSOURCES D'INVESTISSEMENT, ratio de structure

         .add("eitot", DoubleType, true)     // TOTAL DES EMPLOIS D'INVESTISSEMENT, en milliers d'euros
         .add("eitothab", DoubleType, true)  // TOTAL DES EMPLOIS D'INVESTISSEMENT, par habitant
         .add("reitot", DoubleType, true)    // TOTAL DES EMPLOIS D'INVESTISSEMENT, ratio de structure

         .add("detot", DoubleType, true)     // dont : Dépenses d'équipement, en milliers d'euros
         .add("detothab", DoubleType, true)  // dont : Dépenses d'équipement, par habitant
         .add("rdetot", DoubleType, true)    // dont : Dépenses d'équipement, ratio de structure

         .add("edettot", DoubleType, true)   // Remboursement d'emprunts et dettes assimilées, en milliers d'euros
         .add("edettothab", DoubleType, true)// Remboursement d'emprunts et dettes assimilées, par habitant
         .add("redettot", DoubleType, true)  // Remboursement d'emprunts et dettes assimilées, ratio de structure

         .add("caftot", DoubleType, true)    // Capacité d'autofinancement = CAF, en milliers d'euros
         .add("caftothab", DoubleType, true) // Capacité d'autofinancement = CAF, par habitant
         .add("rcaftot", DoubleType, true)   // Capacité d'autofinancement = CAF, ratio de structure

         .add("cafntot", DoubleType, true)    // CAF nette du remboursement en capital des emprunt, en milliers d'euros
         .add("cafntothab", DoubleType, true) // CAF nette du remboursement en capital des emprunt, par habitant
         .add("rcafntot", DoubleType, true)   // CAF nette du remboursement en capital des emprunt, ratio de structure

         .add("entot", DoubleType, true)      // Encours total de la dette au 31/12/N, en milliers d'euros
         .add("entothab", DoubleType, true)   // Encours total de la dette au 31/12/N, par habitant
         .add("rentot", DoubleType, true)     // Encours total de la dette au 31/12/N, ratio de structure

         .add("antot", DoubleType, true)      // Annuité de la dette, en milliers d'euros
         .add("antothab", DoubleType, true)   // Annuité de la dette, par habitant
         .add("rantot", DoubleType, true)     // Annuité de la dette, ratio de structure

         .add("bth", DoubleType, true)        // Taxe d'habitation, en milliers d'euros
         .add("fbth", DoubleType, true)       // Taxe d'habitation, par habitant
         .add("bthexod", DoubleType, true)    // Taxe d'habitation, réductions de bases accordées sur délibérations

         .add("bfb", DoubleType, true)        // Taxe foncière sur les propriétés bâties, en milliers d'euros
         .add("fbfb", DoubleType, true)       // Taxe foncière sur les propriétés bâties, par habitant
         .add("bfbexod", DoubleType, true)    // Taxe foncière sur les propriétés bâties, réductions de bases accordées sur délibérations

         .add("bfnb", DoubleType, true)       // Taxe foncière sur les propriétés non bâties, en milliers d'euros
         .add("fbfnb", DoubleType, true)      // Taxe foncière sur les propriétés non bâties, par habitant
         .add("bfnbexod", DoubleType, true)   // Taxe foncière sur les propriétés non bâties, réductions de bases accordées sur délibérations

         .add("btafnb", DoubleType, true)      // Taxe additionnelle à la taxe sur les propriétés non bâties, en milliers d'euros
         .add("fbtafnb", DoubleType, true)   // Taxe additionnelle à la taxe sur les propriétés non bâties, par habitant

         .add("bcfe1", DoubleType, true)      // Cotisation foncière des entreprises (fiscalité additionnelle), en milliers d'euros
         .add("fbcfe1", DoubleType, true)   // Cotisation foncière des entreprises (fiscalité additionnelle), par habitant
         .add("bcfeexod", DoubleType, true)     // Cotisation foncière des entreprises (fiscalité additionnelle), réductions de bases accordées sur délibérations

         .add("bcfe2", DoubleType, true)      // Cotisation foncière des entreprises (fiscalité prof. unique ou de zone), en milliers d'euros
         .add("fbcfe2", DoubleType, true)   // Cotisation foncière des entreprises (fiscalité prof. unique ou de zone), par habitant

         .add("bcfe3", DoubleType, true)      // Cotisation foncière des entreprises (fiscalité des éoliennes), en milliers d'euros
         .add("fbcfe3", DoubleType, true)   // Cotisation foncière des entreprises (fiscalité des éoliennes), par habitant

         .add("pth", DoubleType, true)      // Produits de la Taxe d'habitation (dont THLV et GEMAPI), en milliers d'euros
         .add("fpth", DoubleType, true)   // Produits de la Taxe d'habitation (dont THLV et GEMAPI), par habitant
         .add("tth", DoubleType, true)     // Produits de la Taxe d'habitation (dont THLV et GEMAPI), réductions de bases accordées sur délibérations

         .add("pfb", DoubleType, true)      // Produits de la Taxe foncière sur les propriétés bâties (dont GEMAPI), en milliers d'euros
         .add("fpfb", DoubleType, true)   // Produits de la Taxe foncière sur les propriétés bâties (dont GEMAPI), par habitant
         .add("tfb", DoubleType, true)     // Produits de la Taxe foncière sur les propriétés bâties (dont GEMAPI), réductions de bases accordées sur délibérations

         .add("pfnb", DoubleType, true)      // Produits de la Taxe foncière sur les propriétés non bâties (dont GEMAPI), en milliers d'euros
         .add("fpfnb", DoubleType, true)   // Produits de la Taxe foncière sur les propriétés non bâties (dont GEMAPI), par habitant
         .add("tfnb", DoubleType, true)     // Produits de la Taxe foncière sur les propriétés non bâties (dont GEMAPI), réductions de bases accordées sur délibérations

         .add("ptafnb", DoubleType, true)      // Produits de la Taxe additionnelle à la taxe sur les propriétés non bâties, en milliers d'euros
         .add("fptafnb", DoubleType, true)   // Produits de la Taxe additionnelle à la taxe sur les propriétés non bâties, par habitant
         .add("tafnb", DoubleType, true)     // Produits de la Taxe additionnelle à la taxe sur les propriétés non bâties, réductions de bases accordées sur délibérations

         .add("pcfe1", DoubleType, true)      // Produits de la Cotisation foncière des entreprises (fiscalité additionnelle-hors GEMAPI), en milliers d'euros
         .add("fpcfe1", DoubleType, true)   // Produits de la Cotisation foncière des entreprises (fiscalité additionnelle-hors GEMAPI), par habitant
         .add("tcfe1", DoubleType, true)     // Produits de la Cotisation foncière des entreprises (fiscalité additionnelle-hors GEMAPI), réductions de bases accordées sur délibérations

         .add("pcfe2", DoubleType, true)      // Produits de la Cotis. foncière des entreprises (fiscalité prof. unique ou de zone- hors GEMAPI), en milliers d'euros
         .add("fpcfe2", DoubleType, true)   // Produits de la Cotis. foncière des entreprises (fiscalité prof. unique ou de zone- hors GEMAPI), par habitant
         .add("tcfe2", DoubleType, true)     // Produits de la Cotis. foncière des entreprises (fiscalité prof. unique ou de zone- hors GEMAPI), réductions de bases accordées sur délibérations

         .add("pcfe3", DoubleType, true)      // Produits de la Cotisation foncière des entreprises (fiscalité des éoliennes- hors GEMAPI), en milliers d'euros
         .add("fpcfe3", DoubleType, true)   // Produits de la Cotisation foncière des entreprises (fiscalité des éoliennes- hors GEMAPI), par habitant
         .add("tcfe3", DoubleType, true)     // Produits de la Cotisation foncière des entreprises (fiscalité des éoliennes- hors GEMAPI), réductions de bases accordées sur délibérations

         .add("cvaeg", DoubleType, true)      // Produits de la Cotisation Valeur Ajoutée des Entreprises (tous régimes fiscaux confondus), en milliers d'euros
         .add("fcvaeg", DoubleType, true)   // Produits de la Cotisation Valeur Ajoutée des Entreprises (tous régimes fiscaux confondus), par habitant

         .add("iferg", DoubleType, true)      // Produits de l'Imposition forfaitaire sur les entreprises de réseau, en milliers d'euros
         .add("fiferg", DoubleType, true)   // Produits de l'Imposition forfaitaire sur les entreprises de réseau, par habitant

         .add("tascomg", DoubleType, true)      // Produits de la Taxe sur les surfaces commerciales, en milliers d'euros
         .add("ftascomg", DoubleType, true)   // Produits de la Taxe sur les surfaces commerciales, par habitant

         .add("nbtotba", IntegerType, false) // Consolidation avec {{nbtotba}} budgets annexes (BA)
         .add("siren", StringType, true)    // SIREN / Code EPCI. Null pour les EPCI qui n'existent plus.

         .add("encdbr", DoubleType, true)      // Encours des dettes bancaires net de l'aide du fonds de soutien pour la sortie des emprunts toxiques, en milliers d'euros
         .add("mencdbr", DoubleType, true)   // Encours des dettes bancaires net de l'aide du fonds de soutien pour la sortie des emprunts toxiques, par habitant
         .add("tencdbr", DoubleType, true)     // Encours des dettes bancaires net de l'aide du fonds de soutien pour la sortie des emprunts toxiques, ratio de structure

         .add("pfr", DoubleType, true)      // Produits de fonctionnement CAF, en milliers d'euros
         .add("mpfr", DoubleType, true)   // Produits de fonctionnement CAF, par habitant
         .add("tpfr", DoubleType, true)     // Produits de fonctionnement CAF, ratio de structure

         .add("cfr", DoubleType, true)      // Charges de fonctionnement CAF, en milliers d'euros
         .add("mcfr", DoubleType, true)   // Charges de fonctionnement CAF, par habitant
         .add("tcfr", DoubleType, true)     // Charges de fonctionnement CAF, ratio de structure

         .add("encdb", DoubleType, true)      // Encours des dettes bancaires et assimilées, en milliers d'euros
         .add("mencdb", DoubleType, true)   // Encours des dettes bancaires et assimilées, par habitant
         .add("tencdb", DoubleType, true);     // Encours des dettes bancaires et assimilées, ratio de structure
   }
}
