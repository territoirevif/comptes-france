package fr.ecoemploi.adapters.outbound.spark.dataset.individuel.communes;

import java.io.*;
import java.util.Objects;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.RowToObjetMetierInterface;
import fr.ecoemploi.adapters.outbound.spark.dataset.individuel.AbstractComptesIndividuelsEncoder;
import fr.ecoemploi.domain.model.territoire.comptabilite.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels.*;

/**
 * Encodeur d'objet de compte individuel (pour l'analyse financière).
 * @author Marc Le Bihan
 */
@Component
public class ComptesIndividuelsCommunesEncoder extends AbstractComptesIndividuelsEncoder<ComptesAvecMoyenneStrate, ComptesAvecStrateEtRatioStructureEtStrate, ComptesAvecStrateEtReductionVotee, ComptesAvecStrateEtTauxVoteEtStrate>
   implements RowToObjetMetierInterface<ComptesIndividuelsCommune>, Serializable {
   @Serial
   private static final long serialVersionUID = -4468024893959650013L;

   @Override
   public Dataset<ComptesIndividuelsCommune> toDatasetObjetsMetiers(Dataset<Row> rows) {
      Objects.requireNonNull(rows, "Le dataset de comptes individuels de communes ne peut pas valoir null.");

      return rows.map((MapFunction<Row, ComptesIndividuelsCommune>) row -> {
         ComptesAvecStrateEtRatioStructureEtStrate chargesDeFonctionnementCAFContingents = rs("\t\tContingents\t\t\t", "chargesDeFonctionnementCAFContingents", true, row);

         ComptesAvecStrateEtRatioStructureEtStrate ressourcesInvestissementRetourDeBiensAffectesConcedes = rs("\tRetour de biens affectés, concédés, ...\t", "ressourcesInvestissementRetourDeBiensAffectesConcedes", true, row);

         ComptesAvecStrateEtRatioStructureEtStrate emploisInvestissementChargesARepartir = rs("\tCharges à répartir\t\t\t", "emploisInvestissementChargesARepartir", true, row);
         ComptesAvecStrateEtRatioStructureEtStrate emploisInvestissementImmobilisationsAffecteesConcedees = rs("\tImmobilisations affectées, concédées, ...", "emploisInvestissementImmobilisationsAffecteesConcedees", true, row);

         ComptesAvecMoyenneStrate operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissement = mr("Besoin ou capacité de financement résiduel de la section d'investissement = D - C", "operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissement", true, row);
         ComptesAvecMoyenneStrate operationsInvestissementSoldeDesOperationsPourCompteDeTiers = mr("+ Solde des opérations pour le compte de tiers", "operationsInvestissementSoldeDesOperationsPourCompteDeTiers", true, row);
         ComptesAvecMoyenneStrate operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissement = mr("Besoin ou capacité de financement de la section d'investissement = E", "operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissement", true, row);
         ComptesAvecMoyenneStrate operationsInvestissementResultatEnsemble = mr("Résultat d'ensemble = R - E", "operationsInvestissementResultatEnsemble", true, row);

         ComptesAvecStrateEtRatioStructureEtStrate autoFinancementExcedentBrutDeFonctionnement = rs("Excédent brut de fonctionnement\t\t\t", "autoFinancementExcedentBrutDeFonctionnement", true, row);

         ComptesAvecMoyenneStrate endettementFondsDeRoulement = mr("FONDS DE ROULEMENT", "endettementFondsDeRoulement", true, row);

         ComptesIndividuels<ComptesAvecMoyenneStrate, ComptesAvecStrateEtRatioStructureEtStrate, ComptesAvecStrateEtReductionVotee, ComptesAvecStrateEtTauxVoteEtStrate> comptesIndividuels = super.alimenterCompteIndividuels(row,
            row.getAs("populationCommune"), row.getAs("sirenCommune"), row.getAs("codeCommune"), row.getAs("nomCommune"), true);

         return new ComptesIndividuelsCommune(comptesIndividuels,
            row.getAs("codeRegion"), row.getAs("codeDepartement"),
            row.getAs("typeGroupementAppartenance"),
            row.getAs("libelleStrateCommune"),
            ressourcesInvestissementRetourDeBiensAffectesConcedes,
            chargesDeFonctionnementCAFContingents,
            emploisInvestissementChargesARepartir, emploisInvestissementImmobilisationsAffecteesConcedees,
            autoFinancementExcedentBrutDeFonctionnement,
            endettementFondsDeRoulement,
            operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissement,
            operationsInvestissementSoldeDesOperationsPourCompteDeTiers,
            operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissement,
            operationsInvestissementResultatEnsemble);
      }, Encoders.bean(ComptesIndividuelsCommune.class));
   }

   @Override
   public ComptesIndividuelsCommune toObjetMetier(Row row) {
      throw new UnsupportedOperationException("toObjetMetier(row -> new ComptesIndividuels n'est pas encore proposé pour new ComptesIndividuels()");
   }
}
