package fr.ecoemploi.adapters.outbound.spark.dataset.individuel.intercommunalites;

import java.io.*;
import java.util.Objects;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.RowToObjetMetierInterface;
import fr.ecoemploi.adapters.outbound.spark.dataset.individuel.AbstractComptesIndividuelsEncoder;
import fr.ecoemploi.domain.model.territoire.comptabilite.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels.*;

/**
 * Encodeur d'objet de compte individuel (pour l'analyse financière).
 * @author Marc Le Bihan
 */
@Component
public class ComptesIndividuelsIntercommunalitesEncoder extends AbstractComptesIndividuelsEncoder<ComptesAvecRatioStructure, ComptesAvecRatioStructure, ComptesAvecReductionVotee, ComptesAvecTauxVote>
   implements RowToObjetMetierInterface<ComptesIndividuelsIntercommunalite>, Serializable {
   @Serial
   private static final long serialVersionUID = -6633876461803384607L;

   @Override
   public Dataset<ComptesIndividuelsIntercommunalite> toDatasetObjetsMetiers(Dataset<Row> rows) {
      Objects.requireNonNull(rows, "Le dataset de comptes individuels ne peut pas valoir null.");

      return rows.map((MapFunction<Row, ComptesIndividuelsIntercommunalite>) row -> {
         ComptesIndividuels<ComptesAvecRatioStructure, ComptesAvecRatioStructure, ComptesAvecReductionVotee, ComptesAvecTauxVote> comptesIndividuels = super.alimenterCompteIndividuels(row,
            (Integer)row.getAs("populationIntercommunalite"), (String)row.getAs("siren"), (String)row.getAs("siren"), (String)row.getAs("nomIntercommunalite"), false);

         return new ComptesIndividuelsIntercommunalite(comptesIndividuels);
      }, Encoders.bean(ComptesIndividuelsIntercommunalite.class));

   }

   @Override
   public ComptesIndividuelsIntercommunalite toObjetMetier(Row row) {
      throw new UnsupportedOperationException("toObjetMetier(row -> new ComptesIndividuels n'est pas encore proposé pour new ComptesIndividuels()");
   }
}
