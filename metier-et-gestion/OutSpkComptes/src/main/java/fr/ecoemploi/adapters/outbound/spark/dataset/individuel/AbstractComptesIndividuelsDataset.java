package fr.ecoemploi.adapters.outbound.spark.dataset.individuel;

import java.io.*;
import java.util.*;

import org.apache.spark.sql.*;

import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.domain.model.territoire.comptabilite.ComptesIndividuels;
import fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;

/**
 * Dataset de base des extractions de comptes individuels de collectivités locales et intercommunalités. 
 * @param <CI> Comptes individuels de collectivité locale ou intercommunalité.
 * @param <MR> Comptes individuels avec montant total ou base, par habitant + Moyenne ou Ratio Structure, selon le cas. 
 * @param <RS> Ratio Structure, avec moyennes strates selon le cas.
 * @param <RV> Réductions votées, avec moyennes strates selon le cas.
 * @param <TV> Taux votés, avec moyennes strates selon le cas.
 * @author Marc Le Bihan
 */
public abstract class AbstractComptesIndividuelsDataset<MR extends ComptesTotalOuBaseEtParHabitant, RS extends ComptesAvecRatioStructure, RV extends ComptesAvecReductionVotee, TV extends ComptesAvecTauxVote,
   CI extends ComptesIndividuels<MR, RS, RV, TV>> extends AbstractSparkObjetMetierDataset<CI> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 1244173470384121109L;

   /** Type de collectivité représentée. */
   private String typeCollectivite;

   /** Nom du champ année comptable dans ce fichier. */
   private final String nomChampAnneeComptable;

   /** Chargeur de Rows depuis un fichier CSV */
   private final AbstractComptesIndividuelsRowCsvLoader rowCsvLoader;

   /**
    * Construire un dataset de comptes individuels.
    * @param typeCollectivite Type de collectivité représenté par ce dataset. Commune, EPCI, Departement, Region...<br>
    * Il peut servir de suffixe pour certains champs du dataset produit.
    */
   protected AbstractComptesIndividuelsDataset(AbstractComptesIndividuelsRowCsvLoader rowCsvLoader, String nomChampAnneeComptable,
      String typeCollectivite) {
      this.rowCsvLoader = rowCsvLoader;
      this.nomChampAnneeComptable = nomChampAnneeComptable;
      this.typeCollectivite = typeCollectivite; 
   }
   
   /**
    * Obtenir un Dataset Row des comptes individuels.
    * @param anneeComptable Année comptable : si 0, toutes les années comptables sont rapatritées.
    * @return Dataset des comptes individuels.
    */
   protected Dataset<Row> rowComptesIndividuels(int anneeComptable) {
      Dataset<Row> comptesIndividuels = this.rowCsvLoader.loadOpenData(anneeComptable);

      if (anneeComptable != 0) {
         comptesIndividuels = comptesIndividuels.where(col(this.nomChampAnneeComptable).equalTo(anneeComptable));
      }

      return this.rowCsvLoader.cast(comptesIndividuels, anneeComptable);
   }

   /**
    * Renvoyer le type de collectivité représenté par ce Dataset.
    * @return Type de collectivité représenté par ce Dataset
    */
   public String getTypeCollectivite() {
      return this.typeCollectivite;
   }

   /**
    * Fixer le type de collectivité représenté par ce Dataset
    * @param typeCollectivite Type de collectivité représenté par ce Dataset.
    */
   public void setTypeCollectivite(String typeCollectivite) {
      this.typeCollectivite = typeCollectivite;
   }

   /**
    * Convertir un Dataset Row en Listes de comptes individuels.
    * @param dsRow Dataset Row à convertir.
    * @return Comptes individuels de communes.
    */
   public List<CI> toComptesIndividuels(Dataset<Row> dsRow) {
      Objects.requireNonNull(dsRow, "Le dataset de comptes individuels ne peut pas valoir null.");
      return objetMetierEncoder().toDatasetObjetsMetiers(dsRow).collectAsList();
   }
}
