package fr.ecoemploi.adapters.outbound.spark.dataset.individuel.communes;

import java.io.Serial;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.individuel.AbstractComptesIndividuelsRowCsvLoader;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

/**
 * Chargeur de comptes individuels de communes depuis un fichier CSV
 * @author Marc Le Bihan
 */
@Component
public class ComptesIndividuelsCommunesRowCsvLoader extends AbstractComptesIndividuelsRowCsvLoader {
   @Serial
   private static final long serialVersionUID = 6929257054902625228L;

   /**
    * Construire un chargeur de comptes depuis un fichier CSV.
    * @param nomFichier Nom du fichier
    */
   @Autowired
   public ComptesIndividuelsCommunesRowCsvLoader(@Value("${individuel-comptes-communes.csv.nom}") String nomFichier) {
      super(nomFichier); // "caf", "dette"
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Dataset<Row> loadOpenData(int anneeComptable) {
      return super.loadOpenData(0);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Dataset<Row> rename(Dataset<Row> dataset, int annee) {
      return dataset
         .withColumnRenamed("an", "anneeExercice")
         .withColumnRenamed("pop1", "populationCommune")
         .withColumnRenamed("nomsst2", "typeGroupementAppartenance")

         .withColumnRenamed("prod", "produitsDeFonctionnementTotal")
         .withColumnRenamed("fprod", "produitsDeFonctionnementParHabitant")
         .withColumnRenamed("mprod", "produitsDeFonctionnementMoyenneStrate")

         // Produits de fonctionnement CAF
         .withColumnRenamed("pfcaf", "produitsDeFonctionnementCAFTotal")            // Produits de fonctionnement CAF : total
         .withColumnRenamed("fpfcaf", "produitsDeFonctionnementCAFParHabitant")     // Produits de fonctionnement CAF : par habitant
         .withColumnRenamed("mpfcaf", "produitsDeFonctionnementCAFMoyenneStrate")   // Produits de fonctionnement CAF : moyenne de la strate

         .withColumnRenamed("impo1", "produitsDeFonctionnementCAFImpotsLocauxTotal")
         .withColumnRenamed("fimpo1", "produitsDeFonctionnementCAFImpotsLocauxParHabitant")
         .withColumnRenamed("mimpo1", "produitsDeFonctionnementCAFImpotsLocauxMoyenneStrate")
         .withColumnRenamed("rimpo1", "produitsDeFonctionnementCAFImpotsLocauxRatioStructure")
         .withColumnRenamed("rmimpo1", "produitsDeFonctionnementCAFImpotsLocauxRatioStrate")

         .withColumnRenamed("impo2", "produitsDeFonctionnementCAFAutresImpotsEtTaxesTotal")
         .withColumnRenamed("fimpo2", "produitsDeFonctionnementCAFAutresImpotsEtTaxesParHabitant")
         .withColumnRenamed("mimpo2", "produitsDeFonctionnementCAFAutresImpotsEtTaxesMoyenneStrate")
         .withColumn("produitsDeFonctionnementCAFAutresImpotsEtTaxesRatioStructure", lit(null).cast(DoubleType))       // TODO Annoncé dans la maquette, mais pas présent dans le fichier.
         .withColumnRenamed("rmimpo2", "produitsDeFonctionnementCAFAutresImpotsEtTaxesRatioStrate")

         .withColumnRenamed("dgf", "produitsDeFonctionnementCAFDGFTotal")
         .withColumnRenamed("fdgf", "produitsDeFonctionnementCAFDGFParHabitant")
         .withColumnRenamed("mdgf", "produitsDeFonctionnementCAFDGFMoyenneStrate")
         .withColumnRenamed("rdgf", "produitsDeFonctionnementCAFDGFRatioStructure")
         .withColumnRenamed("rmdgf", "produitsDeFonctionnementCAFDGFRatioStrate")

         .withColumnRenamed("charge", "chargesDeFonctionnementTotal")
         .withColumnRenamed("fcharge", "chargesDeFonctionnementParHabitant")
         .withColumnRenamed("mcharge", "chargesDeFonctionnementMoyenneStrate")

         // Charges de fonctionnement CAF
         .withColumnRenamed("cfcaf", "chargesDeFonctionnementCAFTotal")             // Charges de fonctionnement CAF : total
         .withColumnRenamed("fcfcaf", "chargesDeFonctionnementCAFParHabitant")      // Charges de fonctionnement CAF : par habitant
         .withColumnRenamed("mcfcaf", "chargesDeFonctionnementCAFMoyenneStrate")    // Charges de fonctionnement CAF : moyenne de la strate

         .withColumnRenamed("perso", "chargesDeFonctionnementCAFChargesDePersonnelTotal")
         .withColumnRenamed("fperso", "chargesDeFonctionnementCAFChargesDePersonnelParHabitant")
         .withColumnRenamed("mperso", "chargesDeFonctionnementCAFChargesDePersonnelMoyenneStrate")
         .withColumnRenamed("rperso", "chargesDeFonctionnementCAFChargesDePersonnelRatioStructure")
         .withColumnRenamed("rmperso", "chargesDeFonctionnementCAFChargesDePersonnelRatioStrate")

         .withColumnRenamed("achat", "chargesDeFonctionnementCAFAchatsEtChargesExternesTotal")
         .withColumnRenamed("fachat", "chargesDeFonctionnementCAFAchatsEtChargesExternesParHabitant")
         .withColumnRenamed("machat", "chargesDeFonctionnementCAFAchatsEtChargesExternesMoyenneStrate")
         .withColumnRenamed("rachat", "chargesDeFonctionnementCAFAchatsEtChargesExternesRatioStructure")
         .withColumnRenamed("rmachat", "chargesDeFonctionnementCAFAchatsEtChargesExternesRatioStrate")

         .withColumnRenamed("fin", "chargesDeFonctionnementCAFChargesFinancieresTotal")
         .withColumnRenamed("ffin", "chargesDeFonctionnementCAFChargesFinancieresParHabitant")
         .withColumnRenamed("mfin", "chargesDeFonctionnementCAFChargesFinancieresMoyenneStrate")
         .withColumnRenamed("rfin", "chargesDeFonctionnementCAFChargesFinancieresRatioStructure")
         .withColumnRenamed("rmfin", "chargesDeFonctionnementCAFChargesFinancieresRatioStrate")

         .withColumnRenamed("cont", "chargesDeFonctionnementCAFContingentsTotal")
         .withColumnRenamed("fcont", "chargesDeFonctionnementCAFContingentsParHabitant")
         .withColumnRenamed("mcont", "chargesDeFonctionnementCAFContingentsMoyenneStrate")
         .withColumnRenamed("rcont", "chargesDeFonctionnementCAFContingentsRatioStructure")
         .withColumnRenamed("rmcont", "chargesDeFonctionnementCAFContingentsRatioStrate")

         .withColumnRenamed("subv", "chargesDeFonctionnementCAFSubventionsVerseesTotal")
         .withColumnRenamed("fsubv", "chargesDeFonctionnementCAFSubventionsVerseesParHabitant")
         .withColumnRenamed("msubv", "chargesDeFonctionnementCAFSubventionsVerseesMoyenneStrate")
         .withColumnRenamed("rsubv", "chargesDeFonctionnementCAFSubventionsVerseesRatioStructure")
         .withColumnRenamed("rmsubv", "chargesDeFonctionnementCAFSubventionsVerseesRatioStrate")

         .withColumnRenamed("res1", "resultatComptableTotal")
         .withColumnRenamed("fres1", "resultatComptableParHabitant")
         .withColumnRenamed("mres1", "resultatComptableMoyenneStrate")

         .withColumnRenamed("pth", "impotsLocauxTaxeHabitationTotal")
         .withColumnRenamed("fpth", "impotsLocauxTaxeHabitationParHabitant")
         .withColumnRenamed("mpth", "impotsLocauxTaxeHabitationMoyenneStrate")
         .withColumnRenamed("tth", "impotsLocauxTaxeHabitationTauxVote")
         .withColumnRenamed("tmth", "impotsLocauxTaxeHabitationTauxMoyenStrate")

         .withColumnRenamed("pfb", "impotsLocauxTaxeFonciereProprietesBatiesTotal")
         .withColumnRenamed("fpfb", "impotsLocauxTaxeFonciereProprietesBatiesParHabitant")
         .withColumnRenamed("mpfb", "impotsLocauxTaxeFonciereProprietesBatiesMoyenneStrate")
         .withColumnRenamed("tfb", "impotsLocauxTaxeFonciereProprietesBatiesTauxVote")
         .withColumnRenamed("tmfb", "impotsLocauxTaxeFonciereProprietesBatiesTauxMoyenStrate")

         .withColumnRenamed("pfnb", "impotsLocauxTaxeFonciereProprietesNonBatiesTotal")
         .withColumnRenamed("fpfnb", "impotsLocauxTaxeFonciereProprietesNonBatiesParHabitant")
         .withColumnRenamed("mpfnb", "impotsLocauxTaxeFonciereProprietesNonBatiesMoyenneStrate")
         .withColumnRenamed("tfnb", "impotsLocauxTaxeFonciereProprietesNonBatiesTauxVote")
         .withColumnRenamed("tmfnb", "impotsLocauxTaxeFonciereProprietesNonBatiesTauxMoyenStrate")

         .withColumnRenamed("ttp", "impotsLocauxCotisationFonciereEntreprisesTauxVote")
         .withColumnRenamed("tmtp", "impotsLocauxCotisationFonciereEntreprisesTauxMoyenStrate")

         .withColumnRenamed("recinv", "ressourcesInvestissementTotal")
         .withColumnRenamed("frecinv", "ressourcesInvestissementParHabitant")
         .withColumnRenamed("mrecinv", "ressourcesInvestissementMoyenneStrate")

         .withColumnRenamed("emp", "ressourcesInvestissementEmpruntsBancairesDettesAssimileesTotal")
         .withColumnRenamed("femp", "ressourcesInvestissementEmpruntsBancairesDettesAssimileesParHabitant")
         .withColumnRenamed("memp", "ressourcesInvestissementEmpruntsBancairesDettesAssimileesMoyenneStrate")
         .withColumnRenamed("remp", "ressourcesInvestissementEmpruntsBancairesDettesAssimileesRatioStructure")
         .withColumnRenamed("rmemp", "ressourcesInvestissementEmpruntsBancairesDettesAssimileesRatioStrate")

         .withColumnRenamed("subr", "ressourcesInvestissementSubventionsRecuesTotal")
         .withColumnRenamed("fsubr", "ressourcesInvestissementSubventionsRecuesParHabitant")
         .withColumnRenamed("msubr", "ressourcesInvestissementSubventionsRecuesMoyenneStrate")
         .withColumnRenamed("rsubr", "ressourcesInvestissementSubventionsRecuesRatioStructure")
         .withColumnRenamed("rmsubr", "ressourcesInvestissementSubventionsRecuesRatioStrate")

         .withColumnRenamed("fctva", "ressourcesInvestissementFCTVATotal")
         .withColumnRenamed("ffctva", "ressourcesInvestissementFCTVAParHabitant")
         .withColumnRenamed("mfctva", "ressourcesInvestissementFCTVAMoyenneStrate")
         .withColumnRenamed("rfctva", "ressourcesInvestissementFCTVARatioStructure")
         .withColumnRenamed("rmfctva", "ressourcesInvestissementFCTVARatioStrate")

         .withColumnRenamed("raff", "ressourcesInvestissementRetourDeBiensAffectesConcedesTotal")
         .withColumnRenamed("fraff", "ressourcesInvestissementRetourDeBiensAffectesConcedesParHabitant")
         .withColumnRenamed("mraff", "ressourcesInvestissementRetourDeBiensAffectesConcedesMoyenneStrate")
         .withColumnRenamed("rraff", "ressourcesInvestissementRetourDeBiensAffectesConcedesRatioStructure")
         .withColumnRenamed("rmraff", "ressourcesInvestissementRetourDeBiensAffectesConcedesRatioStrate")

         .withColumnRenamed("depinv", "emploisInvestissementTotal")           // total
         .withColumnRenamed("fdepinv", "emploisInvestissementParHabitant")    // par habitant
         .withColumnRenamed("mdepinv", "emploisInvestissementMoyenneStrate")  // moyenne strate

         // Dépenses d'équipement
         .withColumnRenamed("equip", "emploisInvestissementDepensesEquipementTotal")            // EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement : total
         .withColumnRenamed("fequip", "emploisInvestissementDepensesEquipementParHabitant")     // EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement : par habitant
         .withColumnRenamed("mequip", "emploisInvestissementDepensesEquipementMoyenneStrate")   // EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement : moyenne strate
         .withColumnRenamed("requip", "emploisInvestissementDepensesEquipementRatioStructure")  // EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement : ratio de structure
         .withColumnRenamed("rmequip", "emploisInvestissementDepensesEquipementRatioStrate")    // EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement : ratio de la strate

         // Remboursement d'emprunts et dettes assimilées
         .withColumnRenamed("remb", "emploisInvestissementRemboursementEmpruntsDettesAssimileesTotal")            // EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées : total
         .withColumnRenamed("fremb", "emploisInvestissementRemboursementEmpruntsDettesAssimileesParHabitant")     // EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées : par habitant
         .withColumnRenamed("mremb", "emploisInvestissementRemboursementEmpruntsDettesAssimileesMoyenneStrate")   // EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées : moyenne strate
         .withColumnRenamed("rremb", "emploisInvestissementRemboursementEmpruntsDettesAssimileesRatioStructure")  // EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées : ratio de structure
         .withColumnRenamed("rmremb", "emploisInvestissementRemboursementEmpruntsDettesAssimileesRatioStrate")    // EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées : ratio de la strate

         // Charges à répartir
         .withColumnRenamed("repart", "emploisInvestissementChargesARepartirTotal")             // EMPLOIS D'INVESTISSEMENT : Charges à répartir : total
         .withColumnRenamed("frepart", "emploisInvestissementChargesARepartirParHabitant")      // EMPLOIS D'INVESTISSEMENT : Charges à répartir : par habitant
         .withColumnRenamed("mrepart", "emploisInvestissementChargesARepartirMoyenneStrate")    // EMPLOIS D'INVESTISSEMENT : Charges à répartir : moyenne strate
         .withColumnRenamed("rrepart", "emploisInvestissementChargesARepartirRatioStructure")   // EMPLOIS D'INVESTISSEMENT : Charges à répartir : ratio de structure
         .withColumnRenamed("rmrepart", "emploisInvestissementChargesARepartirRatioStrate")     // EMPLOIS D'INVESTISSEMENT : Charges à répartir : ratio de la strate

         // Immobilisations affectées, concédées, ...
         .withColumnRenamed("daff", "emploisInvestissementImmobilisationsAffecteesConcedeesTotal")             // EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ... : total
         .withColumnRenamed("fdaff", "emploisInvestissementImmobilisationsAffecteesConcedeesParHabitant")      // EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ... : par habitant
         .withColumnRenamed("mdaff", "emploisInvestissementImmobilisationsAffecteesConcedeesMoyenneStrate")    // EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ... : moyenne strate
         .withColumnRenamed("rdaff", "emploisInvestissementImmobilisationsAffecteesConcedeesRatioStructure")   // EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ... : ratio de structure
         .withColumnRenamed("rmdaff", "emploisInvestissementImmobilisationsAffecteesConcedeesRatioStrate")     // EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ... : ratio de la strate

         // Besoin ou capacité de financement résiduel de la section d'investissement = D - C
         .withColumnRenamed("bf1", "operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissementTotal")            // Besoin ou capacité de financement résiduel de la section d'investissement = D - C : total
         .withColumnRenamed("fbf1", "operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissementParHabitant")     // Besoin ou capacité de financement résiduel de la section d'investissement = D - C : par habitant
         .withColumnRenamed("mbf1", "operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissementMoyenneStrate")   // Besoin ou capacité de financement résiduel de la section d'investissement = D - C : moyenne strate

         // + Solde des opérations pour le compte de tiers
         .withColumnRenamed("solde", "operationsInvestissementSoldeDesOperationsPourCompteDeTiersTotal")          // Solde des opérations pour le compte de tiers : total
         .withColumnRenamed("fsolde", "operationsInvestissementSoldeDesOperationsPourCompteDeTiersParHabitant")   // Solde des opérations pour le compte de tiers : par habitant
         .withColumnRenamed("msolde", "operationsInvestissementSoldeDesOperationsPourCompteDeTiersMoyenneStrate") // Solde des opérations pour le compte de tiers : moyenne strate

         // Besoin ou capacité de financement de la section d'investissement = E
         .withColumnRenamed("bf2", "operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissementTotal")           // Besoin ou capacité de financement de la section d'investissement = E : total
         .withColumnRenamed("fbf2", "operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissementParHabitant")    // Besoin ou capacité de financement de la section d'investissement = E : par habitant
         .withColumnRenamed("mbf2", "operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissementMoyenneStrate")  // Besoin ou capacité de financement de la section d'investissement = E : moyenne strate

         // Résultat d'ensemble = R - E
         .withColumnRenamed("res2", "operationsInvestissementResultatEnsembleTotal")            // Résultat d'ensemble = R - E : total
         .withColumnRenamed("fres2", "operationsInvestissementResultatEnsembleParHabitant")     // Résultat d'ensemble = R - E : par habitant
         .withColumnRenamed("mres2", "operationsInvestissementResultatEnsembleMoyenneStrate")   // Résultat d'ensemble = R - E : moyenne strate

         // Excédent brut de fonctionnement
         .withColumnRenamed("ebf", "autoFinancementExcedentBrutDeFonctionnementTotal")             // AUTOFINANCEMENT : Excédent brut de fonctionnement : total
         .withColumnRenamed("febf", "autoFinancementExcedentBrutDeFonctionnementParHabitant")      // AUTOFINANCEMENT : Excédent brut de fonctionnement : par habitant
         .withColumnRenamed("mebf", "autoFinancementExcedentBrutDeFonctionnementMoyenneStrate")    // AUTOFINANCEMENT : Excédent brut de fonctionnement : moyenne strate
         .withColumnRenamed("rebf", "autoFinancementExcedentBrutDeFonctionnementRatioStructure")   // AUTOFINANCEMENT : Excédent brut de fonctionnement : ratio de structure
         .withColumnRenamed("rmebf", "autoFinancementExcedentBrutDeFonctionnementRatioStrate")     // AUTOFINANCEMENT : Excédent brut de fonctionnement : ratio de la strate

         // Capacité d'autofinancement = CAF
         .withColumnRenamed("caf", "autoFinancementCAFTotal")           // AUTOFINANCEMENT : Capacité d'autofinancement = CAF : total
         .withColumnRenamed("fcaf", "autoFinancementCAFParHabitant")    // AUTOFINANCEMENT : Capacité d'autofinancement = CAF : par habitant
         .withColumnRenamed("mcaf", "autoFinancementCAFMoyenneStrate")  // AUTOFINANCEMENT : Capacité d'autofinancement = CAF : moyenne strate
         .withColumnRenamed("rcaf", "autoFinancementCAFRatioStructure") // AUTOFINANCEMENT : Capacité d'autofinancement = CAF : ratio de structure
         .withColumnRenamed("rmcaf", "autoFinancementCAFRatioStrate")   // AUTOFINANCEMENT : Capacité d'autofinancement = CAF : ratio de la strate

         // CAF nette du remboursement en capital des emprunts
         .withColumnRenamed("cafn", "autoFinancementCAFNetteTotal")           // AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts : total
         .withColumnRenamed("fcafn", "autoFinancementCAFNetteParHabitant")    // AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts : par habitant
         .withColumnRenamed("mcafn", "autoFinancementCAFNetteMoyenneStrate")  // AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts : moyenne strate
         .withColumnRenamed("rcafn", "autoFinancementCAFNetteRatioStructure") // AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts : ratio de structure
         .withColumnRenamed("rmcafn", "autoFinancementCAFNetteRatioStrate")   // AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts : ratio de la strate

         // ENDETTEMENT
         // Encours total de la dette au 31 décembre N
         .withColumnRenamed("dette", "endettementEncoursTotalDetteAu31DecembreNTotal")             // ENDETTEMENT : Encours total de la dette au 31 décembre N : total
         .withColumnRenamed("fdette", "endettementEncoursTotalDetteAu31DecembreNParHabitant")      // ENDETTEMENT : Encours total de la dette au 31 décembre N : par habitant
         .withColumnRenamed("mdette", "endettementEncoursTotalDetteAu31DecembreNMoyenneStrate")    // ENDETTEMENT : Encours total de la dette au 31 décembre N : moyenne strate
         .withColumnRenamed("rdette", "endettementEncoursTotalDetteAu31DecembreNRatioStructure")   // ENDETTEMENT : Encours total de la dette au 31 décembre N : ratio de structure
         .withColumnRenamed("rmdette", "endettementEncoursTotalDetteAu31DecembreNRatioStrate")     // ENDETTEMENT : Encours total de la dette au 31 décembre N : ratio de la strate

         // Encours des dettes bancaires et assimilées
         .withColumnRenamed("det2cal", "endettementEncoursDettesBancairesEtAssimileesTotal")          // ENDETTEMENT : Encours des dettes bancaires et assimilées : total
         .withColumnRenamed("fdet2cal", "endettementEncoursDettesBancairesEtAssimileesParHabitant")   // ENDETTEMENT : Encours des dettes bancaires et assimilées : par habitant
         .withColumnRenamed("mdet2cal", "endettementEncoursDettesBancairesEtAssimileesMoyenneStrate") // ENDETTEMENT : Encours des dettes bancaires et assimilées : moyenne de la strate
         .withColumnRenamed("rdet2cal", "endettementEncoursDettesBancairesEtAssimileesRatioStructure")// ENDETTEMENT : Encours des dettes bancaires et assimilées : ratio de structure
         .withColumnRenamed("rmdet2cal", "endettementEncoursDettesBancairesEtAssimileesRatioStrate")  // ENDETTEMENT : Encours des dettes bancaires et assimilées : ratio de la strate

         .withColumnRenamed("encdbr", "endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiquesTotal")            // ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques : total
         .withColumnRenamed("fencdbr", "endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiquesParHabitant")     // ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques : par habitant
         .withColumnRenamed("mencdbr", "endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiquesMoyenneStrate")   // ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques : moyenne de la strate
         .withColumnRenamed("rencdbr", "endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiquesRatioStructure")  // ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques : ratio de structure
         .withColumnRenamed("rmencdbr", "endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiquesRatioStrate")    // ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques : ratio de la strate

         // Annuité de la dette
         .withColumnRenamed("annu", "endettementAnnuiteDetteTotal")           // ENDETTEMENT : Annuité de la dette : total
         .withColumnRenamed("fannu", "endettementAnnuiteDetteParHabitant")    // ENDETTEMENT : Annuité de la dette : par habitant
         .withColumnRenamed("mannu", "endettementAnnuiteDetteMoyenneStrate")  // ENDETTEMENT : Annuité de la dette : moyenne strate
         .withColumnRenamed("rannu", "endettementAnnuiteDetteRatioStructure") // ENDETTEMENT : Annuité de la dette : ratio de structure
         .withColumnRenamed("rmannu", "endettementAnnuiteDetteRatioStrate")   // ENDETTEMENT : Annuité de la dette : ratio de la strate

         // FONDS DE ROULEMENT
         .withColumnRenamed("fdr", "endettementFondsDeRoulementTotal")           // ENDETTEMENT : FONDS DE ROULEMENT : total
         .withColumnRenamed("ffdr", "endettementFondsDeRoulementParHabitant")    // ENDETTEMENT : FONDS DE ROULEMENT : par habitant
         .withColumnRenamed("mfdr", "endettementFondsDeRoulementMoyenneStrate")  // ENDETTEMENT : FONDS DE ROULEMENT : moyenne strate

         // ELEMENTS DE FISCALITE DIRECTE LOCALE
         // Bases nettes imposées au profit de la commune
         // Taxe d'habitation (y compris THLV)
         .withColumnRenamed("bth", "fiscLocalBaseNetteTaxeHabitationTotal")                            // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : total
         .withColumnRenamed("fbth", "fiscLocalBaseNetteTaxeHabitationParHabitant")                     // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : par habitant
         .withColumnRenamed("mbth", "fiscLocalBaseNetteTaxeHabitationMoyenneStrate")                   // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : moyenne strate
         .withColumnRenamed("bthexod", "fiscLocalBaseNetteTaxeHabitationReductionVoteeTotal")          // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : Réductions de base accordées sur délibérations : total
         .withColumnRenamed("fbthexod", "fiscLocalBaseNetteTaxeHabitationReductionVoteeParHabitant")   // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : Réductions de base accordées sur délibérations : par habitant
         .withColumnRenamed("mbthexod", "fiscLocalBaseNetteTaxeHabitationReductionVoteeMoyenneStrate") // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : Réductions de base accordées sur délibérations : moyenne strate

         // Taxe foncière sur les propriétés bâties
         .withColumnRenamed("bfb", "fiscLocalBaseNetteTaxeFonciereProprietesBatiesTotal")                             // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : total
         .withColumnRenamed("fbfb", "fiscLocalBaseNetteTaxeFonciereProprietesBatiesParHabitant")                      // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : par habitant
         .withColumnRenamed("mbfb", "fiscLocalBaseNetteTaxeFonciereProprietesBatiesMoyenneStrate")                    // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : moyenne strate
         .withColumnRenamed("bfbexod", "fiscLocalBaseNetteTaxeFonciereProprietesBatiesReductionVoteeTotal")           // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : Réductions de base accordées sur délibérations : total
         .withColumnRenamed("fbfbexod", "fiscLocalBaseNetteTaxeFonciereProprietesBatiesReductionVoteeParHabitant")    // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : Réductions de base accordées sur délibérations : par habitant
         .withColumnRenamed("mbfbexod", "fiscLocalBaseNetteTaxeFonciereProprietesBatiesReductionVoteeMoyenneStrate")  // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : Réductions de base accordées sur délibérations : moyenne strate

         // Taxe foncière sur les propriétés non bâties
         .withColumnRenamed("bfnb", "fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesTotal")                            // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : total
         .withColumnRenamed("fbfnb", "fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesParHabitant")                     // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : par habitant
         .withColumnRenamed("mbfnb", "fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesMoyenneStrate")                   // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : moyenne strate
         .withColumnRenamed("bfnbexod", "fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesReductionVoteeTotal")          // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : Réductions de base accordées sur délibérations : total
         .withColumnRenamed("fbfnbexod", "fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesReductionVoteeParHabitant")   // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : Réductions de base accordées sur délibérations : par habitant
         .withColumnRenamed("mbfnbexod", "fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesReductionVoteeMoyenneStrate") // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : Réductions de base accordées sur délibérations : moyenne strate

         // Taxe additionnelle à la taxe foncière sur les propriétés non bâties
         .withColumnRenamed("btafnb", "fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBatiesTotal")          // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : total
         .withColumnRenamed("fbtafnb", "fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBatiesParHabitant")   // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : par habitant
         .withColumnRenamed("mbtafnb", "fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBatiesMoyenneStrate") // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : moyenne strate

         // Cotisation foncière des entreprises
         .withColumnRenamed("btp", "fiscLocalBaseNetteCotisationFonciereEntreprisesTotal")                            // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises : total
         .withColumnRenamed("fbtp", "fiscLocalBaseNetteCotisationFonciereEntreprisesParHabitant")                     // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises :  par habitant
         .withColumnRenamed("mbtp", "fiscLocalBaseNetteCotisationFonciereEntreprisesMoyenneStrate")                   // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises : moyenne strate
         .withColumnRenamed("btpexod", "fiscLocalBaseNetteCotisationFonciereEntreprisesReductionVoteeTotal")          // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises : Réductions de base accordées sur délibérations : total
         .withColumnRenamed("fbtpexod", "fiscLocalBaseNetteCotisationFonciereEntreprisesReductionVoteeParHabitant")   // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises : Réductions de base accordées sur délibérations : par habitant
         .withColumnRenamed("mbtpexod", "fiscLocalBaseNetteCotisationFonciereEntreprisesReductionVoteeMoyenneStrate") // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises : Réductions de base accordées sur délibérations : moyenne strate

         // Produits des impôts locaux (suite)
         .withColumnRenamed("ptafnb", "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesTotal")               // Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : total
         .withColumnRenamed("fptafnb", "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesParHabitant")        // Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : par habitant
         .withColumnRenamed("mptafnb", "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesMoyenneStrate")      // Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : moyenne de la strate
         .withColumnRenamed("tafnb", "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesTauxVote")             // Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : taux voté
         .withColumnRenamed("mtmtafnb", "impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesTauxMoyenStrate")   // Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : taux moyen de la strate

         .withColumnRenamed("pcfe", "impotsLocauxCotisationFonciereEntreprisesTotal")           // Impôts locaux : Cotisation foncière des entreprises : total
         .withColumnRenamed("fpcfe", "impotsLocauxCotisationFonciereEntreprisesParHabitant")    // Impôts locaux : Cotisation foncière des entreprises : par habitant
         .withColumnRenamed("mpcfe", "impotsLocauxCotisationFonciereEntreprisesMoyenneStrate")  // Impôts locaux : Cotisation foncière des entreprises : moyenne de la strate

         .withColumnRenamed("cvaec", "produitsImpotsRepartitionCotisationValeurAjouteeEntreprisesTotal")            // Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises : total
         .withColumnRenamed("fcvaec", "produitsImpotsRepartitionCotisationValeurAjouteeEntreprisesParHabitant")     // Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises : par habitant
         .withColumnRenamed("mcvaec", "produitsImpotsRepartitionCotisationValeurAjouteeEntreprisesMoyenneStrate")   // Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises : moyenne de la strate

         .withColumnRenamed("iferc", "produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseauTotal")            // Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau : total
         .withColumnRenamed("fiferc", "produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseauParHabitant")     // Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau : par habitant
         .withColumnRenamed("miferc", "produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseauMoyenneStrate")   // Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau : moyenne de la strate

         .withColumnRenamed("tascomc", "produitsImpotsRepartitionTaxeSurfacesCommercialesTotal")            // Produits des impôts de répartition : Taxe sur les surfaces commerciales : total
         .withColumnRenamed("ftascomc", "produitsImpotsRepartitionTaxeSurfacesCommercialesParHabitant")     // Produits des impôts de répartition : Taxe sur les surfaces commerciales : par habitant
         .withColumnRenamed("mtascomc", "produitsImpotsRepartitionTaxeSurfacesCommercialesMoyenneStrate")   // Produits des impôts de répartition : Taxe sur les surfaces commerciales : moyenne de la strate

         .withColumnRenamed("nomsst1", "libelleStrateCommune");  // Strate commune
   }

   /* Entre 2020 et 2022, ont été ajoutés les champs :
      avance
      favance
      fpotfis
      fptp
      ftamen
      mavance
      mpotfis
      mptp
      mtamen
      pop2
      potfis
      ptp
      ravance
      rimpo2
      rmavance
      rmtamen
      rtamen
      tamen

      Et supprimés les champs :
         bfb
         bfbexod
         bfnb
         bfnbexod
         btafnb
         bth
         bthexod
         btp
         btpexod
         cfcaf
         Column 220
         cvaec
         det2cal
         encdbr
         fbfb
         fbfbexod
         fbfnb
         fbfnbexod
         fbtafnb
         fbth
         fbthexod
         fbtp
         fbtpexod
         fcfcaf
         fcvaec
         fdet2cal
         fencdbr
         fiferc
         fpcfe
         fpfcaf
         fptafnb
         ftascomc
         iferc
         mbfb
         mbfbexod
         mbfnb
         mbfnbexod
         mbtafnb
         mbth
         mbthexod
         mbtp
         mbtpexod
         mcfcaf
         mcvaec
         mdet2cal
         mencdbr
         miferc
         mpcfe
         mpfcaf
         mptafnb
         mtascomc
         mtmtafnb
         pcfe
         pfcaf
         ptafnb
         rdet2cal
         repart
         rmdet2cal
         rmencdbr
         tafnb
         tascomc
    */


   /**
    * {@inheritDoc}
    */
   @Override
   public StructType schema(int annee) {
      // anneeExercice  produitsDeFonctionnementTotal produitsDeFonctionnementParHabitant produitsDeFonctionnementMoyenneStrate  produitsDeFonctionnementCAFImpotsLocauxTotal produitsDeFonctionnementCAFImpotsLocauxParHabitant produitsDeFonctionnementCAFImpotsLocauxMoyenneStrate  produitsDeFonctionnementCAFImpotsLocauxRatioStructure produitsDeFonctionnementCAFImpotsLocauxRatioStrate produitsDeFonctionnementCAFAutresImpotsEtTaxesTotal   produitsDeFonctionnementCAFAutresImpotsEtTaxesParHabitant   produitsDeFonctionnementCAFAutresImpotsEtTaxesMoyenneStrate produitsDeFonctionnementCAFAutresImpotsEtTaxesRatioStrate   produitsDeFonctionnementCAFDGFTotal produitsDeFonctionnementCAFDGFParHabitant produitsDeFonctionnementCAFDGFMoyenneStrate  produitsDeFonctionnementCAFDGFRatioStructure produitsDeFonctionnementCAFDGFRatioStrate chargesDeFonctionnementTotal  chargesDeFonctionnementParHabitant  chargesDeFonctionnementMoyenneStrate   chargesDeFonctionnementCAFChargesDePersonnelTotal  chargesDeFonctionnementCAFChargesDePersonnelParHabitant  chargesDeFonctionnementCAFChargesDePersonnelMoyenneStrate   chargesDeFonctionnementCAFChargesDePersonnelRatioStructure  chargesDeFonctionnementCAFChargesDePersonnelRatioStrate  chargesDeFonctionnementCAFAchatsEtChargesExternesTotal   chargesDeFonctionnementCAFAchatsEtChargesExternesParHabitant   chargesDeFonctionnementCAFAchatsEtChargesExternesMoyenneStrate chargesDeFonctionnementCAFAchatsEtChargesExternesRatioStructure   chargesDeFonctionnementCAFAchatsEtChargesExternesRatioStrate   chargesDeFonctionnementCAFChargesFinancieresTotal  chargesDeFonctionnementCAFChargesFinancieresParHabitant  chargesDeFonctionnementCAFChargesFinancieresMoyenneStrate   chargesDeFonctionnementCAFChargesFinancieresRatioStructure  chargesDeFonctionnementCAFChargesFinancieresRatioStrate  chargesDeFonctionnementCAFContingentsTotal   chargesDeFonctionnementCAFContingentsParHabitant   chargesDeFonctionnementCAFContingentsMoyenneStrate chargesDeFonctionnementCAFContingentsRatioStructure   chargesDeFonctionnementCAFContingentsRatioStrate   chargesDeFonctionnementCAFSubventionsVerseesTotal  chargesDeFonctionnementCAFSubventionsVerseesParHabitant  chargesDeFonctionnementCAFSubventionsVerseesMoyenneStrate   chargesDeFonctionnementCAFSubventionsVerseesRatioStructure  chargesDeFonctionnementCAFSubventionsVerseesRatioStrate  resultatComptableTotal  resultatComptableParHabitant  resultatComptableMoyenneStrate   impotsLocauxTaxeHabitationTotal  impotsLocauxTaxeHabitationParHabitant  impotsLocauxTaxeHabitationMoyenneStrate   impotsLocauxTaxeHabitationTauxVote  impotsLocauxTaxeHabitationTauxMoyenStrate impotsLocauxTaxeFonciereProprietesBatiesTotal   impotsLocauxTaxeFonciereProprietesBatiesParHabitant   impotsLocauxTaxeFonciereProprietesBatiesMoyenneStrate impotsLocauxTaxeFonciereProprietesBatiesTauxVote   impotsLocauxTaxeFonciereProprietesBatiesTauxMoyenStrate  impotsLocauxTaxeFonciereProprietesNonBatiesTotal   impotsLocauxTaxeFonciereProprietesNonBatiesParHabitant   impotsLocauxTaxeFonciereProprietesNonBatiesMoyenneStrate impotsLocauxTaxeFonciereProprietesNonBatiesTauxVote   impotsLocauxTaxeFonciereProprietesNonBatiesTauxMoyenStrate  impotsLocauxCotisationFonciereEntreprisesTauxVote  impotsLocauxCotisationFonciereEntreprisesTauxMoyenStrate ressourcesInvestissementTotal ressourcesInvestissementParHabitant ressourcesInvestissementMoyenneStrate  ressourcesInvestissementEmpruntsBancairesDettesAssimileesTotal ressourcesInvestissementEmpruntsBancairesDettesAssimileesParHabitant ressourcesInvestissementEmpruntsBancairesDettesAssimileesMoyenneStrate  ressourcesInvestissementEmpruntsBancairesDettesAssimileesRatioStructure ressourcesInvestissementEmpruntsBancairesDettesAssimileesRatioStrate ressourcesInvestissementSubventionsRecuesTotal  ressourcesInvestissementSubventionsRecuesParHabitant  ressourcesInvestissementSubventionsRecuesMoyenneStrate   ressourcesInvestissementSubventionsRecuesRatioStructure  ressourcesInvestissementSubventionsRecuesRatioStrate  ressourcesInvestissementFCTVATotal  ressourcesInvestissementFCTVAParHabitant  ressourcesInvestissementFCTVAMoyenneStrate   ressourcesInvestissementFCTVARatioStructure  ressourcesInvestissementFCTVARatioStrate  ressourcesInvestissementRetourDeBiensAffectesConcedesTotal  ressourcesInvestissementRetourDeBiensAffectesConcedesParHabitant  ressourcesInvestissementRetourDeBiensAffectesConcedesMoyenneStrate   ressourcesInvestissementRetourDeBiensAffectesConcedesRatioStructure  ressourcesInvestissementRetourDeBiensAffectesConcedesRatioStrate  emploisInvestissementTotal emploisInvestissementParHabitant emploisInvestissementMoyenneStrate  emploisInvestissementDepensesEquipementTotal emploisInvestissementDepensesEquipementParHabitant emploisInvestissementDepensesEquipementMoyenneStrate  emploisInvestissementDepensesEquipementRatioStructure emploisInvestissementDepensesEquipementRatioStrate emploisInvestissementRemboursementEmpruntsDettesAssimileesTotal   emploisInvestissementRemboursementEmpruntsDettesAssimileesParHabitant   emploisInvestissementRemboursementEmpruntsDettesAssimileesMoyenneStrate emploisInvestissementRemboursementEmpruntsDettesAssimileesRatioStructure   emploisInvestissementRemboursementEmpruntsDettesAssimileesRatioStrate   emploisInvestissementChargesARepartirTotal   emploisInvestissementChargesARepartirParHabitant   emploisInvestissementChargesARepartirMoyenneStrate emploisInvestissementChargesARepartirRatioStructure   emploisInvestissementChargesARepartirRatioStrate   emploisInvestissementImmobilisationsAffecteesConcedeesTotal emploisInvestissementImmobilisationsAffecteesConcedeesParHabitant emploisInvestissementImmobilisationsAffecteesConcedeesMoyenneStrate  emploisInvestissementImmobilisationsAffecteesConcedeesRatioStructure emploisInvestissementImmobilisationsAffecteesConcedeesRatioStrate operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissementTotal  operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissementParHabitant  operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissementMoyenneStrate   operationsInvestissementSoldeDesOperationsPourCompteDeTiersTotal  operationsInvestissementSoldeDesOperationsPourCompteDeTiersParHabitant  operationsInvestissementSoldeDesOperationsPourCompteDeTiersMoyenneStrate   operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissementTotal operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissementParHabitant operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissementMoyenneStrate  operationsInvestissementResultatEnsembleTotal   operationsInvestissementResultatEnsembleParHabitant   operationsInvestissementResultatEnsembleMoyenneStrate autoFinancementExcedentBrutDeFonctionnementTotal   autoFinancementExcedentBrutDeFonctionnementParHabitant   autoFinancementExcedentBrutDeFonctionnementMoyenneStrate autoFinancementExcedentBrutDeFonctionnementRatioStructure   autoFinancementExcedentBrutDeFonctionnementRatioStrate   autoFinancementCAFTotal autoFinancementCAFParHabitant autoFinancementCAFMoyenneStrate  autoFinancementCAFRatioStructure autoFinancementCAFRatioStrate autoFinancementCAFNetteTotal  autoFinancementCAFNetteParHabitant  autoFinancementCAFNetteMoyenneStrate   autoFinancementCAFNetteRatioStructure  autoFinancementCAFNetteRatioStrate  endettementEncoursTotalDetteAu31DecembreNTotal  endettementEncoursTotalDetteAu31DecembreNParHabitant  endettementEncoursTotalDetteAu31DecembreNMoyenneStrate   endettementEncoursTotalDetteAu31DecembreNRatioStructure  endettementEncoursTotalDetteAu31DecembreNRatioStrate  endettementAnnuiteDetteTotal  endettementAnnuiteDetteParHabitant  endettementAnnuiteDetteMoyenneStrate   endettementAnnuiteDetteRatioStructure  endettementAnnuiteDetteRatioStrate  endettementFondsDeRoulementTotal endettementFondsDeRoulementParHabitant endettementFondsDeRoulementMoyenneStrate  fiscLocalBaseNetteTaxeHabitationTotal  fiscLocalBaseNetteTaxeHabitationParHabitant  fiscLocalBaseNetteTaxeHabitationMoyenneStrate   fiscLocalBaseNetteTaxeHabitationReductionVoteeTotal   fiscLocalBaseNetteTaxeHabitationReductionVoteeParHabitant   fiscLocalBaseNetteTaxeHabitationReductionVoteeMoyenneStrate fiscLocalBaseNetteTaxeFonciereProprietesBatiesTotal   fiscLocalBaseNetteTaxeFonciereProprietesBatiesParHabitant   fiscLocalBaseNetteTaxeFonciereProprietesBatiesMoyenneStrate fiscLocalBaseNetteTaxeFonciereProprietesBatiesReductionVoteeTotal fiscLocalBaseNetteTaxeFonciereProprietesBatiesReductionVoteeParHabitant fiscLocalBaseNetteTaxeFonciereProprietesBatiesReductionVoteeMoyenneStrate  fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesTotal   fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesParHabitant   fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesMoyenneStrate fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesReductionVoteeTotal fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesReductionVoteeParHabitant fiscLocalBaseNetteTaxeFonciereProprietesNonBatiesReductionVoteeMoyenneStrate  fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBatiesTotal  fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBatiesParHabitant  fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBatiesMoyenneStrate   fiscLocalBaseNetteCotisationFonciereEntreprisesTotal  fiscLocalBaseNetteCotisationFonciereEntreprisesParHabitant  fiscLocalBaseNetteCotisationFonciereEntreprisesMoyenneStrate   fiscLocalBaseNetteCotisationFonciereEntreprisesReductionVoteeTotal   fiscLocalBaseNetteCotisationFonciereEntreprisesReductionVoteeParHabitant   fiscLocalBaseNetteCotisationFonciereEntreprisesReductionVoteeMoyenneStrate impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesTotal  impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesParHabitant  impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesMoyenneStrate   impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesTauxVote  impotsLocauxTaxeAdditionnelleFonciereProprietesNonBatiesTauxMoyenStrate impotsLocauxCotisationFonciereEntreprisesTotal  impotsLocauxCotisationFonciereEntreprisesParHabitant  impotsLocauxCotisationFonciereEntreprisesMoyenneStrate   produitsImpotsRepartitionCotisationValeurAjouteeEntreprisesTotal produitsImpotsRepartitionCotisationValeurAjouteeEntreprisesParHabitant produitsImpotsRepartitionCotisationValeurAjouteeEntreprisesMoyenneStrate  produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseauTotal produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseauParHabitant produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseauMoyenneStrate  produitsImpotsRepartitionTaxeSurfacesCommercialesTotal   produitsImpotsRepartitionTaxeSurfacesCommercialesParHabitant   produitsImpotsRepartitionTaxeSurfacesCommercialesMoyenneStrate libelleStrateCommune  endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiquesTotal  endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiquesParHabitant  endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiquesRatioStructure  endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiquesMoyenneStrate   endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiquesRatioStrate  produitsDeFonctionnementCAFTotal produitsDeFonctionnementCAFParHabitant produitsDeFonctionnementCAFMoyenneStrate  chargesDeFonctionnementCAFTotal  chargesDeFonctionnementCAFParHabitant  chargesDeFonctionnementCAFMoyenneStrate   endettementEncoursDettesBancairesEtAssimileesTotal endettementEncoursDettesBancairesEtAssimileesParHabitant endettementEncoursDettesBancairesEtAssimileesRatioStructure endettementEncoursDettesBancairesEtAssimileesMoyenneStrate  endettementEncoursDettesBancairesEtAssimileesRatioStrate codeRegion  codeDepartement   siren codeCommune nomCommune
      // 2017           307,99                        2264,66                             885,78                                 42,94                                        315,71   239,84   13,94 27,08 50,66 372,51   87,46 9,87  93 683,8 197,3 30,19 22,27 236,53   1739,2   693,46   105,52   775,85   200,02   44,61 28,84 80,31 590,5 229,87   33,95 33,15 12,2  89,73 16,57 5,16  2,39  10,39 76,42 83,62 4,39  12,06 2,29  16,84 23,59 0,97  3,4   71,46 525,46   192,32   20,64 151,74   106,99   7,46  10,4  22,66 166,59   98,65 8,24  12,22 10,1  74,27 59,39 56,24 33,44 0  0  73,81 542,69   513,98   0  0  97,33 0  18,94 15 110,29   129,11   20,32 25,12 52,89 388,88   49,98 71,66 9,72  0  0  0  0  0  243,1 1787,47  506,71   190,75   1402,55  406,82   78,47 80,29 51,59 379,37   77,11 21,22 15,22 0  0  0,12  0  0,02  0  0  0,01  0  0  169,29   1244,79  -7,26 0  0  -0,31 169,29   1244,79  -7,58 -97,83   -719,33  199,9 87,71 644,92   213,33   28,48 24,08 75,48 555,01   207,23   24,51 23,4  23,89 175,65   130,12   7,76  14,69 421,8 3101,44  577,88   136,95   65,24 63,8  469,1 93,02 20,71 10,5  133,48   981,45   983,56   276,6 2033,79  1028,45  0  0  19,02 274,99   2021,95  807,02   0  0  0,79  17,95 132   177,57   0  0  0,09  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  communes de moins... 415,95   3058,47  135,05   557,69   62,96 307,99   2264,66  865,3 232,51   1709,65  658,07   415,95   3058,47  135,05   557,69   62,96 7  84 7  200085140   7262  Saint-Laurent-les...
      // 2017           307,99                        2264,66                             885,78                                 42,94                                        315,71   239,84   13,94 27,08 50,66 372,51   87,46 9,87  93 683,8 197,3 30,19 22,27 236,53   1739,2   693,46   105,52   775,85   200,02   44,61 28,84 80,31 590,5 229,87   33,95 33,15 12,2  89,73 16,57 5,16  2,39  10,39 76,42 83,62 4,39  12,06 2,29  16,84 23,59 0,97  3,4   71,46 525,46   192,32   20,64 151,74   106,99   7,46  10,4  22,66 166,59   98,65 8,24  12,22 10,1  74,27 59,39 56,24 33,44 0  0  73,81 542,69   513,98   0  0  97,33 0  18,94 15 110,29   129,11   20,32 25,12 52,89 388,88   49,98 71,66 9,72  0  0  0  0  0  243,1 1787,47  506,71   190,75   1402,55  406,82   78,47 80,29 51,59 379,37   77,11 21,22 15,22 0  0  0,12  0  0,02  0  0  0,01  0  0  169,29   1244,79  -7,26 0  0  -0,31 169,29   1244,79  -7,58 -97,83   -719,33  199,9 87,71 644,92   213,33   28,48 24,08 75,48 555,01   207,23   24,51 23,4  23,89 175,65   130,12   7,76  14,69 421,8 3101,44  577,88   136,95   65,24 63,8  469,1 93,02 20,71 10,5  133,48   981,45   983,56   276,6 2033,79  1028,45  0  0  19,02 274,99   2021,95  807,02   0  0  0,79  17,95 132   177,57   0  0  0,09  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  communes de moins de 250 hab  415,95   3058,47  135,05   557,69   62,96 307,99   2264,66  865,3 232,51   1709,65  658,07   415,95   3058,47  135,05   557,69   62,96
      // an             prod                          fprod                               mprod                                  impo1                                        fimpo1   mimpo1   rimpo1   rmimpo1  impo2 fimpo2   mimpo2   rmimpo2  dgf   fdgf  mdgf  rdgf  rmdgf charge   fcharge  mcharge  perso fperso   mperso   rperso   rmperso  achat fachat   machat   rachat   rmachat  fin   ffin  mfin  rfin  rmfin cont  fcont mcont rcont rmcont   subv  fsubv msubv rsubv rmsubv   res1  fres1 mres1 pth   fpth  mpth  tth   tmth  pfb   fpfb  mpfb  tfb   tmfb  pfnb  fpfnb mpfnb tfnb  tmfnb ttp   tmtp  recinv   frecinv  mrecinv  emp   femp  memp  remp  rmemp subr  fsubr msubr rsubr rmsubr   fctva ffctva   mfctva   rfctva   rmfctva  raff  fraff mraff rraff rmraff   depinv   fdepinv  mdepinv  equip fequip   mequip   requip   rmequip  remb  fremb mremb rremb rmremb   repart   frepart  mrepart  rrepart  rmrepart daff  fdaff mdaff rdaff rmdaff   bf1   fbf1  mbf1  solde fsolde   msolde   bf2   fbf2  mbf2  res2  fres2 mres2 ebf   febf  mebf  rebf  rmebf caf   fcaf  mcaf  rcaf  rmcaf cafn  fcafn mcafn rcafn rmcafn   dette fdette   mdette   rdette   rmdette  annu  fannu mannu rannu rmannu   fdr   ffdr  mfdr  bth   fbth  mbth  bthexod  fbthexod mbthexod bfb   fbfb  mbfb  bfbexod  fbfbexod mbfbexod bfnb  fbfnb mbfnb bfnbexod fbfnbexod   mbfnbexod   btafnb   fbtafnb  mbtafnb  btp   fbtp  mbtp  btpexod  fbtpexod mbtpexod ptafnb   fptafnb  mptafnb  tafnb mtmtafnb pcfe  fpcfe mpcfe cvaec fcvaec   mcvaec   iferc fiferc   miferc   tascomc  ftascomc mtascomc nomsst1  encdbr   fencdbr  rencdbr  mencdbr  rmencdbr pfcaf fpfcaf   mpfcaf   cfcaf fcfcaf   mcfcaf   det2cal  fdet2cal rdet2cal mdet2cal rmdet2cal

/*
   Libellé du budget :                                         INOM  Libellé du département : NOMDEP
   Population légale en vigueur au 1er janvier de l'exercice : POP1  Budget principal seul
   Strate: Strate commune: NOMSST1                                 Type groupement d'appartenance: NOMSST2
   Exercice: AN                 N° de département: DEP             N° insee de la commune: ICOM             N° de région: REG


                                       ANALYSE DES EQUILIBRES FINANCIERS FONDAMENTAUX

En milliers d'Euros  Euros par habitant         Moyenne de la strate OPERATIONS DE FONCTIONNEMENT  Ratios de structure  Moyenne de la strate
prod     fprod       mprod       TOTAL DES PRODUITS DE FONCTIONNEMENT = A                                en % des produits
pfcaf    fpfcaf      mpfcaf         Produits de fonctionnement CAF
impo1    fimpo1      mimpo1            dont : Impôts Locaux                                        rimpo1               rmimpo1
impo2    fimpo2      mimpo2            Autres impôts et taxes                                      RIMPO2               rmimpo2
dgf      fdgf        mdgf              Dotation globale de fonctionnement                          rdgf                 rmdgf

// TODO : Ceci manque par le fait de la DGFIP : attendre une correction de leur part.
dfctva   fdfctva     mdfctva           FCTVA                                                       rdfctva              rmdfctva
dpserdom fpserdom    mpserdom          Produits des services et du domaine                         rpserdom             rmpserdom

=== 2022
prod	fprod			mprod	TOTAL DES PRODUITS DE FONCTIONNEMENT = A			en % des produits
pfcaf	fpfcaf			mpfcaf	Produits de fonctionnement CAF
impo1	fimpo1			mimpo1	dont : Impôts Locaux			rimpo1					rmimpo1
fiscrev	ffiscrev			mfiscrev	          Fiscalité reversée par les groupements à fiscalité propre			-					-
impo2	fimpo2			mimpo2	          Autres impôts et taxes			rimpo2					rmimpo2
dgf	fdgf			mdgf	          Dotation globale de fonctionnement			rdgf					rmdgf

autdot	fautdot	fautdot	fautdot	mautdot	          Autres dotations et participations			rautdot	rautdot	rautdot	rautdot	rautdot	rmautdot	rmautdot	rmautdot	rmautdot	rmautdot
dfctva	fdfctva			mdfctva	       dont :   FCTVA			rdfctva					rmdfctva
dpserdom	fpserdom			mpserdom	          Produits des services et du domaine			rpserdom					rmpserdom
---


charge   fcharge     mcharge     TOTAL DES CHARGES DE FONCTIONNEMENT = B                                 en % des charges
cfcaf    fcfcaf      mcfcaf         Charges de fonctionnement CAF
perso    fperso      mperso            dont : Charges de personnel                                 rperso               rmperso
achat    fachat      machat            Achats et charges externes                                  rachat               rmachat
fin      ffin        mfin              Charges financières                                         rfin                 rmfin
cont     fcont       mcont             Contingents                                                 rcont                rmcont
subv     fsubv       msubv             Subventions versées                                         rsubv                rmsubv
res1     fres1       mres1       Résultat comptable = A - B = R

=== 2022
charge	fcharge			mcharge	TOTAL DES CHARGES DE FONCTIONNEMENT = B			en % des charges
cfcaf	fcfcaf			mcfcaf	Charges de fonctionnement CAF
perso	fperso			mperso	dont : Charges de personnel			rperso					rmperso
achat	fachat			machat	          Achats et charges externes			rachat					rmachat
fin	ffin			mfin	          Charges financières			rfin					rmfin
cont	fcont			mcont	          Contingents			rcont					rmcont
subv	fsubv			msubv	          Subventions versées			rsubv					rmsubv

res1	fres1			mres1	Résultat comptable = A - B = R
---

                                       OPERATIONS D'INVESTISSEMENT

recinv   frecinv     mrecinv     TOTAL DES RESSOURCES D'INVESTISSEMENT = C                             en % des ressources
emp      femp        memp           dont : Emprunts bancaires et dettes assimilées                 remp                 rmemp
subr     fsubr       msubr          Subventions reçues                                             rsubr                rmsubr
fctva    ffctva      mfctva         Fonds de compensation de la taxe sur la valeur ajoutée (FCTVA) rfctva               rmfctva
raff     fraff       mraff          Retour de biens affectés, concédés, ...                        rraff                rmraff

=== 2022
recinv	frecinv			mrecinv	TOTAL DES RESSOURCES D'INVESTISSEMENT = C			en % des ressources
emp	femp			memp	dont : Emprunts bancaires et dettes assimilées			remp					rmemp
subr	fsubr			msubr	          Subventions reçues			rsubr					rmsubr
tamen	ftamen			mtamen	          Taxe d’aménagement			rtamen					rmtamen
fctva	ffctva			mfctva	          FCTVA			rfctva					rmfctva
raff	fraff			mraff	          Retour de biens affectés, concédés, ...			rraff					rmraff
---

depinv   fdepinv     mdepinv     TOTAL DES EMPLOIS D'INVESTISSEMENT = D                               en % des emplois
equip    fequip      mequip         dont : Dépenses d'équipement                                   requip               rmequip
remb     fremb       mremb          Remboursement d'emprunts et dettes assimilées                  rremb                rmremb
repart   frepart     mrepart        Charges à répartir                                             rrepart              rmrepart
daff     fdaff       mdaff          Immobilisations affectées, concédées, ...                      rdaff                rmdaff
bf1      fbf1        mbf1        Besoin ou capacité de financement résiduel de la section d'investissement = D - C
solde    fsolde      msolde      + Solde des opérations pour le compte de tiers
bf2      fbf2        mbf2        Besoin ou capacité de financement de la section d'investissement = E
res2     fres2       mres2       Résultat d'ensemble = R - E

=== 2022
depinv	fdepinv			mdepinv	TOTAL DES EMPLOIS D'INVESTISSEMENT = D			en % des emplois
equip	fequip			mequip	dont : Dépenses d'équipement			requip					rmequip
remb	fremb			mremb	          Remboursement d'emprunts et dettes assimilées			rremb					rmremb
repart	frepart			mrepart	          Charges à répartir			rrepart					rmrepart
daff	fdaff			mdaff	          Immobilisations affectées, concédées, ...			rdaff					rmdaff
bf1	fbf1			mbf1	Besoin ou capacité de financement résiduel de la section d'investissement = D - C
solde	fsolde			msolde	+ Solde des opérations pour le compte de tiers
bf2	fbf2			mbf2	Besoin ou capacité de financement de la section d'investissement = E
res2	fres2			mres2	Résultat d'ensemble = R - E
---


                                       AUTOFINANCEMENT                                              en % des prod. de fonct.
ebf      febf        mebf        Excédent brut de fonctionnement                                   rebf                 rmebf
caf      fcaf        mcaf        Capacité d'autofinancement = CAF                                  rcaf                 rmcaf
cafn     fcafn       mcafn       CAF nette du remboursement en capital des emprunts                rcafn                rmcafn

=== 2022
ebf	febf			mebf	Excédent brut de fonctionnement			rebf					rmebf
caf	fcaf			mcaf	Capacité d'autofinancement = CAF			rcaf					rmcaf
cafn	fcafn			mcafn	CAF nette du remboursement en capital des emprunts			rcafn					rmcafn
---

                                       ENDETTEMENT                                                    en % des prod. de fonct.
dette    fdette      mdette      Encours total de la dette au 31 décembre N                        rdette               rmdette
det2cal  fdet2cal    mdet2cal    Encours des dettes bancaires et assimilées                        rdet2cal             rmdet2cal
encdbr   fencdbr     mencdbr     Encours des dettes bancaires net de l'aide du fonds de soutien pour la sortie des emprunts toxiques         rencdbr              rmencdbr
annu     fannu       mannu       Annuité de la dette                                               rannu                rmannu
fdr      ffdr        mfdr        FONDS DE ROULEMENT

=== 2022
dette	fdette			mdette	Encours total de la dette au 31 décembre N			rdette					rmdette
det2cal	fdet2cal			mdet2cal	Encours des dettes bancaires et assimilées			rdet2cal					rmdet2cal
encdbr	fencdbr			mencdbr	Encours des dettes bancaires net de l'aide du fonds de soutien pour la sortie des emprunts toxiques			rencdbr					rmencdbr
annu	fannu			mannu	Annuité de la dette			rannu					rmannu
fdr	ffdr			mfdr	FONDS DE ROULEMENT
---


                                       ELEMENTS DE FISCALITE DIRECTE LOCALE
Les bases imposées et les réductions (exonérations, abattements) accordées sur délibérations
Bases nettes imposées au profit de la commune               Taxe     Réductions de base accordées sur délibérations
En milliers d'Euros  Euros par habitant         Moyenne de la strate                                     En milliers d'Euros     Euros par habitant   Moyenne de la strate
bth                  fbth                       mbth        Taxe d'habitation (y compris THLV)           bthexod                 fbthexod             mbthexod
bfb                  fbfb                       mbfb        Taxe foncière sur les propriétés bâties      bfbexod                 fbfbexod             mbfbexod
bfnb                 fbfnb                      mbfnb       Taxe foncière sur les propriétés non bâties  bfnbexod                fbfnbexod            mbfnbexod
btafnb               fbtafnb                    mbtafnb     Taxe additionnelle à la taxe foncière sur les propriétés non bâties     -     -           -
btp                  fbtp                       mbtp        Cotisation foncière des entreprises          btpexod                 fbtpexod             mbtpexod

=== 2022
Bases nettes imposées au profit de la commune					Taxe		Réductions de base accordées sur délibérations
En milliers d'Euros	Euros par habitant			Moyenne de la strate			En milliers d'Euros		Euros par habitant					Moyenne de la strate
bth	fbth			mbth	Taxe d'habitation (résidences secondaires et logements vacants)		bthexod		fbthexod					mbthexod
bfb	fbfb			mbfb	Taxe foncière sur les propriétés bâties		bfbexod		fbfbexod					mbfbexod
bfnb	fbfnb			mbfnb	Taxe foncière sur les propriétés non bâties		bfnbexod		fbfnbexod					mbfnbexod
btafnb	fbtafnb			mbtafnb	Taxe additionnelle à la taxe foncière sur les propriétés non bâties		-		-					-
btp	fbtp			mbtp	Cotisation foncière des entreprises		btpexod		fbtpexod					mbtpexod
Les taux et les produits de la fiscalité directe locale
---

Les taux et les produits de la fiscalité directe locale
Produits des impôts locaux                Taxe                                                        Taux voté (%)  Taux moyen de la strate (%)
pth      fpth     mpth        Taxe d'habitation (y compris THLV)                                      tth            tmth
pfb      fpfb     mpfb        Taxe foncière sur les propriétés bâties                                 tfb            tmfb
pfnb     fpfnb    mpfnb       Taxe foncière sur les propriétés non bâties                             tfnb           tmfnb
ptafnb   fptafnb  mptafnb     Taxe additionnelle à la taxe foncière sur les propriétés non bâties     tafnb          mtmtafnb
pcfe     fpcfe    mpcfe       Cotisation foncière des entreprises                                     ttp            tmtp

=== 2022
pth	fpth			mpth	Taxe d'habitation (résidences secondaires et logements vacants)		tth			tmth
pfb	fpfb			mpfb	Taxe foncière sur les propriétés bâties (avant application du coefficient correcteur)		tfb			tmfb
pecoco	fpecoco			mpecoco	Effet du coefficient correcteur : Communes sous-compensées (+)/Communes surcompensées (-)		-			-
pfbcoco	fpfbcoco			mpfbcoco	Taxe foncière sur les propriétés bâties (après application du coefficient correcteur)		-			-
allovleifb	fallovleifb			mallovleifb	Allocation compensatrice de foncier bâti - réduction 50% valeur locative des établissements industriels (méthode comptable)		-			-
pfnb	fpfnb			mpfnb	Taxe foncière sur les propriétés non bâties		tfnb			tmfnb
ptafnb	fptafnb			mptafnb	Taxe additionnelle à la taxe foncière sur les propriétés non bâties		tafnb			mtmtafnb
pcfe	fpcfe			mpcfe	Cotisation foncière des entreprises		ttp			tmtp
allovleicfe	fallovleicfe			mallovleicfe	Allocation compensatrice de cotisation foncière des entreprises - réduction de 50% valeur locative des établissements industriels (méthode comptable)		-			-
---

Les produits des impôts de répartition
Produits des impôts de répartition              Taxe
cvaec    fcvaec   mcvaec      Cotisation sur la valeur ajoutée des entreprises      -           -
iferc    fiferc   miferc      Imposition forfaitaire sur les entreprises de réseau     -           -
tascomc  ftascomc mtascomc    Taxe sur les surfaces commerciales     -           -

=== 2022
Produits des impôts de répartition					Taxe
cvaec	fcvaec			mcvaec	Cotisation sur la valeur ajoutée des entreprises		-				-
iferc	fiferc			miferc	Imposition forfaitaire sur les entreprises de réseau		-				-
tascomc	ftascomc			mtascomc	Taxe sur les surfaces commerciales		-				-
Fraction de TVA					Taxe
tvac	ftvac			mtvac	Fraction de TVA (seulement pour la Ville de Paris)		-				-
---
*/

      return new StructType()
         // Entête
         .add("an", IntegerType, false)      // Exercice : an
         .add("dep", StringType, false)      // N° de département : dep
         .add("icom", StringType, false)     // N° insee de la commune : icom
         .add("inom", StringType, false)     // Nom de la commune (libellé du budget)
         .add("reg", StringType, false)      // N° de région : reg

         .add("pop1", IntegerType, true)    // Population légale en vigueur au 1er janvier de l'exercice : pop1
         .add("nomsst2", StringType, true)  // Type groupement d'appartenance : nomsst2

         // ANALYSE DES EQUILIBRES FINANCIERS FONDAMENTAUX
         // TOTAL DES PRODUITS DE FONCTIONNEMENT = A
         .add("prod", DoubleType, true)      // total
         .add("fprod", DoubleType, true)     // par habitant
         .add("mprod", DoubleType, true)     // moyenne strate

         // CAF
         // Impôts locaux
         .add("impo1", DoubleType, true)     // CAF : Impôts Locaux : total
         .add("fimpo1", DoubleType, true)    // CAF : Impôts Locaux : par habitant
         .add("mimpo1", DoubleType, true)    // CAF : Impôts Locaux : moyenne de la strate
         .add("rimpo1", DoubleType, true)    // CAF : Impôts Locaux : ratio de structure
         .add("rmimpo1", DoubleType, true)   // CAF : Impôts Locaux : ratio de la strate

         // Autres impôts et taxes
         .add("impo2", DoubleType, true)     // CAF : Autres impôts et taxes : total
         .add("fimpo2", DoubleType, true)    // CAF : Autres impôts et taxes : par habitant
         .add("mimpo2", DoubleType, true)    // CAF : Autres impôts et taxes : moyenne de la strate
         .add("rmimpo2", DoubleType, true)   // CAF : Autres impôts et taxes : ratio de la strate

         // Dotation globale de fonctionnement
         .add("dgf", DoubleType, true)       // CAF : Dotation globale de fonctionnement : total
         .add("fdgf", DoubleType, true)      // CAF : Dotation globale de fonctionnement : par habitant
         .add("mdgf", DoubleType, true)      // CAF : Dotation globale de fonctionnement : moyenne de la strate
         .add("rdgf", DoubleType, true)      // CAF : Dotation globale de fonctionnement : ratio de structure
         .add("rmdgf", DoubleType, true)     // CAF : Dotation globale de fonctionnement : ratio de la strate

         // TOTAL DES CHARGES DE FONCTIONNEMENT = B
         .add("charge", DoubleType, true)    // total
         .add("fcharge", DoubleType, true)   // par habitant
         .add("mcharge", DoubleType, true)   // moyenne de la strate

         // CAF
         // Charges de personnel
         .add("perso", DoubleType, true)     // CAF : Charges de personnel : total
         .add("fperso", DoubleType, true)    // CAF : Charges de personnel : par habitant
         .add("mperso", DoubleType, true)    // CAF : Charges de personnel : moyenne de la strate
         .add("rperso", DoubleType, true)    // CAF : Charges de personnel : ratio de structure
         .add("rmperso", DoubleType, true)   // CAF : Charges de personnel : ratio de la strate

         // Achats et charges externes
         .add("achat", DoubleType, true)     // CAF : Achats et charges externes : total
         .add("fachat", DoubleType, true)    // CAF : Achats et charges externes : par habitant
         .add("machat", DoubleType, true)    // CAF : Achats et charges externes : moyenne de la strate
         .add("rachat", DoubleType, true)    // CAF : Achats et charges externes : ratio de structure
         .add("rmachat", DoubleType, true)   // CAF : Achats et charges externes : ratio de la strate

         // Charges financières
         .add("fin", DoubleType, true)       // CAF : Charges financières : total
         .add("ffin", DoubleType, true)      // CAF : Charges financières : par habitant
         .add("mfin", DoubleType, true)      // CAF : Charges financières : moyenne de la strate
         .add("rfin", DoubleType, true)      // CAF : Charges financières : ratio de structure
         .add("rmfin", DoubleType, true)     // CAF : Charges financières : ratio de la strate

         // Contingents
         .add("cont", DoubleType, true)      // CAF : Contingents : total
         .add("fcont", DoubleType, true)     // CAF : Contingents : par habitant
         .add("mcont", DoubleType, true)     // CAF : Contingents : moyenne de la strate
         .add("rcont", DoubleType, true)     // CAF : Contingents : ratio de structure
         .add("rmcont", DoubleType, true)    // CAF : Contingents : ratio de la strate

         // Subventions versées
         .add("subv", DoubleType, true)      // CAF : Subventions versées : total
         .add("fsubv", DoubleType, true)     // CAF : Subventions versées : par habitant
         .add("msubv", DoubleType, true)     // CAF : Subventions versées : moyenne de la strate
         .add("rsubv", DoubleType, true)     // CAF : Subventions versées : ratio de structure
         .add("rmsubv", DoubleType, true)    // CAF : Subventions versées : ratio de la strate

         // Résultat comptable = A - B = R
         .add("res1", DoubleType, true)      // Résultat comptable = A - B = R : total
         .add("fres1", DoubleType, true)     // Résultat comptable = A - B = R : par habitant
         .add("mres1", DoubleType, true)     // Résultat comptable = A - B = R : moyenne par strate

         // ELEMENTS DE FISCALITE DIRECTE LOCALE
         // Les taux et les produits de la fiscalité directe locale
         // Produits des impôts locaux
         .add("pth", DoubleType, true)       // Impôts locaux : Taxe d'habitation : total
         .add("fpth", DoubleType, true)      // Impôts locaux : Taxe d'habitation : par habitant
         .add("mpth", DoubleType, true)      // Impôts locaux : Taxe d'habitation : moyenne par strate
         .add("tth", DoubleType, true)       // Impôts locaux : Taxe d'habitation : taux voté
         .add("tmth", DoubleType, true)      // Impôts locaux : Taxe d'habitation : taux moyen de la strate

         .add("pfb", DoubleType, true)       // Impôts locaux : Taxe foncière sur les propriétés bâties : total
         .add("fpfb", DoubleType, true)      // Impôts locaux : Taxe foncière sur les propriétés bâties : par habitant
         .add("mpfb", DoubleType, true)      // Impôts locaux : Taxe foncière sur les propriétés bâties : moyenne par strate
         .add("tfb", DoubleType, true)       // Impôts locaux : Taxe foncière sur les propriétés bâties : taux voté
         .add("tmfb", DoubleType, true)      // Impôts locaux : Taxe foncière sur les propriétés bâties : taux moyen de la strate

         .add("pfnb", DoubleType, true)      // Impôts locaux : Taxe foncière sur les propriétés non bâties : total
         .add("fpfnb", DoubleType, true)     // Impôts locaux : Taxe foncière sur les propriétés non bâties : par habitant
         .add("mpfnb", DoubleType, true)     // Impôts locaux : Taxe foncière sur les propriétés non bâties : moyenne par strate
         .add("tfnb", DoubleType, true)      // Impôts locaux : Taxe foncière sur les propriétés non bâties : taux voté
         .add("tmfnb", DoubleType, true)     // Impôts locaux : Taxe foncière sur les propriétés non bâties : taux moyen de la strate

         .add("ttp", DoubleType, true)       // Impôts locaux : Cotisation foncière des entreprises : taux voté
         .add("tmtp", DoubleType, true)      // Impôts locaux : Cotisation foncière des entreprises : taux moyen de la strate

         // OPERATIONS D'INVESTISSEMENT
         // TOTAL DES RESSOURCES D'INVESTISSEMENT = C
         .add("recinv", DoubleType, true)    // total
         .add("frecinv", DoubleType, true)   // par habitant
         .add("mrecinv", DoubleType, true)   // moyenne strate

         .add("emp", DoubleType, true)    // RESSOURCES D'INVESTISSEMENT : Emprunts bancaires et dettes assimilées : total
         .add("femp", DoubleType, true)   // RESSOURCES D'INVESTISSEMENT : Emprunts bancaires et dettes assimilées : par habitant
         .add("memp", DoubleType, true)   // RESSOURCES D'INVESTISSEMENT : Emprunts bancaires et dettes assimilées : moyenne strate
         .add("remp", DoubleType, true)   // RESSOURCES D'INVESTISSEMENT : Emprunts bancaires et dettes assimilées : ratio de structure
         .add("rmemp", DoubleType, true)  // RESSOURCES D'INVESTISSEMENT : Emprunts bancaires et dettes assimilées : ratio de la strate

         .add("subr", DoubleType, true)   // RESSOURCES D'INVESTISSEMENT : Subventions reçues : total
         .add("fsubr", DoubleType, true)  // RESSOURCES D'INVESTISSEMENT : Subventions reçues : par habitant
         .add("msubr", DoubleType, true)  // RESSOURCES D'INVESTISSEMENT : Subventions reçues : moyenne strate
         .add("rsubr", DoubleType, true)  // RESSOURCES D'INVESTISSEMENT : Subventions reçues : ratio de structure
         .add("rmsubr", DoubleType, true) // RESSOURCES D'INVESTISSEMENT : Subventions reçues : ratio de la strate

         .add("fctva", DoubleType, true)  // RESSOURCES D'INVESTISSEMENT : Fonds de compensation de la taxe sur la valeur ajoutée (FCTVA) : total
         .add("ffctva", DoubleType, true) // RESSOURCES D'INVESTISSEMENT : Fonds de compensation de la taxe sur la valeur ajoutée (FCTVA) : par habitant
         .add("mfctva", DoubleType, true) // RESSOURCES D'INVESTISSEMENT : Fonds de compensation de la taxe sur la valeur ajoutée (FCTVA) : moyenne strate
         .add("rfctva", DoubleType, true) // RESSOURCES D'INVESTISSEMENT : Fonds de compensation de la taxe sur la valeur ajoutée (FCTVA) : ratio de structure
         .add("rmfctva", DoubleType, true)   // RESSOURCES D'INVESTISSEMENT : Fonds de compensation de la taxe sur la valeur ajoutée (FCTVA) : ratio de la strate

         .add("raff", DoubleType, true)   // RESSOURCES D'INVESTISSEMENT : Retour de biens affectés, concédés, ... : total
         .add("fraff", DoubleType, true)  // RESSOURCES D'INVESTISSEMENT : Retour de biens affectés, concédés, ... : par habitant
         .add("mraff", DoubleType, true)  // RESSOURCES D'INVESTISSEMENT : Retour de biens affectés, concédés, ... : moyenne strate
         .add("rraff", DoubleType, true)  // RESSOURCES D'INVESTISSEMENT : Retour de biens affectés, concédés, ... : ratio de structure
         .add("rmraff", DoubleType, true) // RESSOURCES D'INVESTISSEMENT : Retour de biens affectés, concédés, ... : ratio de la strate

         // TOTAL DES EMPLOIS D'INVESTISSEMENT = D
         .add("depinv", DoubleType, true)    // total
         .add("fdepinv", DoubleType, true)   // par habitant
         .add("mdepinv", DoubleType, true)   // moyenne strate

         // Dépenses d'équipement
         .add("equip", DoubleType, true)  // EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement : total
         .add("fequip", DoubleType, true) // EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement : par habitant
         .add("mequip", DoubleType, true) // EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement : moyenne strate
         .add("requip", DoubleType, true) // EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement : ratio de structure
         .add("rmequip", DoubleType, true)   // EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement : ratio de la strate

         // Remboursement d'emprunts et dettes assimilées
         .add("remb", DoubleType, true)   // EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées : total
         .add("fremb", DoubleType, true)  // EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées : par habitant
         .add("mremb", DoubleType, true)  // EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées : moyenne strate
         .add("rremb", DoubleType, true)  // EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées : ratio de structure
         .add("rmremb", DoubleType, true) // EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées : ratio de la strate

         // Charges à répartir
         .add("repart", DoubleType, true) // EMPLOIS D'INVESTISSEMENT : Charges à répartir : total
         .add("frepart", DoubleType, true)   // EMPLOIS D'INVESTISSEMENT : Charges à répartir : par habitant
         .add("mrepart", DoubleType, true)   // EMPLOIS D'INVESTISSEMENT : Charges à répartir : moyenne strate
         .add("rrepart", DoubleType, true)   // EMPLOIS D'INVESTISSEMENT : Charges à répartir : ratio de structure
         .add("rmrepart", DoubleType, true)  // EMPLOIS D'INVESTISSEMENT : Charges à répartir : ratio de la strate

         // Immobilisations affectées, concédées, ...
         .add("daff", DoubleType, true)   // EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ... : total
         .add("fdaff", DoubleType, true)  // EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ... : par habitant
         .add("mdaff", DoubleType, true)  // EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ... : moyenne strate
         .add("rdaff", DoubleType, true)  // EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ... : ratio de structure
         .add("rmdaff", DoubleType, true) // EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ... : ratio de la strate

         // Besoin ou capacité de financement résiduel de la section d'investissement = D - C
         .add("bf1", DoubleType, true)    // Besoin ou capacité de financement résiduel de la section d'investissement = D - C : total
         .add("fbf1", DoubleType, true)   // Besoin ou capacité de financement résiduel de la section d'investissement = D - C : par habitant
         .add("mbf1", DoubleType, true)   // Besoin ou capacité de financement résiduel de la section d'investissement = D - C : moyenne strate

         // + Solde des opérations pour le compte de tiers
         .add("solde", DoubleType, true)  // + Solde des opérations pour le compte de tiers : total
         .add("fsolde", DoubleType, true) // + Solde des opérations pour le compte de tiers : par habitant
         .add("msolde", DoubleType, true) // + Solde des opérations pour le compte de tiers : moyenne strate

         // Besoin ou capacité de financement de la section d'investissement = E
         .add("bf2", DoubleType, true)    // Besoin ou capacité de financement de la section d'investissement = E : total
         .add("fbf2", DoubleType, true)   // Besoin ou capacité de financement de la section d'investissement = E : par habitant
         .add("mbf2", DoubleType, true)   // Besoin ou capacité de financement de la section d'investissement = E : moyenne strate

         // Résultat d'ensemble = R - E
         .add("res2", DoubleType, true)   // Résultat d'ensemble = R - E : total
         .add("fres2", DoubleType, true)  // Résultat d'ensemble = R - E : par habitant
         .add("mres2", DoubleType, true)  // Résultat d'ensemble = R - E : moyenne strate

         // AUTOFINANCEMENT
         // Excédent brut de fonctionnement
         .add("ebf", DoubleType, true)    // AUTOFINANCEMENT : Excédent brut de fonctionnement : total
         .add("febf", DoubleType, true)   // AUTOFINANCEMENT : Excédent brut de fonctionnement : par habitant
         .add("mebf", DoubleType, true)   // AUTOFINANCEMENT : Excédent brut de fonctionnement : moyenne strate
         .add("rebf", DoubleType, true)   // AUTOFINANCEMENT : Excédent brut de fonctionnement : ratio de structure
         .add("rmebf", DoubleType, true)  // AUTOFINANCEMENT : Excédent brut de fonctionnement : ratio de la strate

         // Capacité d'autofinancement = CAF
         .add("caf", DoubleType, true)    // AUTOFINANCEMENT : Capacité d'autofinancement = CAF : total
         .add("fcaf", DoubleType, true)   // AUTOFINANCEMENT : Capacité d'autofinancement = CAF : par habitant
         .add("mcaf", DoubleType, true)   // AUTOFINANCEMENT : Capacité d'autofinancement = CAF : moyenne strate
         .add("rcaf", DoubleType, true)   // AUTOFINANCEMENT : Capacité d'autofinancement = CAF : ratio de structure
         .add("rmcaf", DoubleType, true)  // AUTOFINANCEMENT : Capacité d'autofinancement = CAF : ratio de la strate

         // CAF nette du remboursement en capital des emprunts
         .add("cafn", DoubleType, true)   // AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts : total
         .add("fcafn", DoubleType, true)  // AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts : par habitant
         .add("mcafn", DoubleType, true)  // AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts : moyenne strate
         .add("rcafn", DoubleType, true)  // AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts : ratio de structure
         .add("rmcafn", DoubleType, true) // AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts : ratio de la strate

         // ENDETTEMENT
         // Encours total de la dette au 31 décembre N
         .add("dette", DoubleType, true)     // ENDETTEMENT : Encours total de la dette au 31 décembre N : total
         .add("fdette", DoubleType, true)    // ENDETTEMENT : Encours total de la dette au 31 décembre N : par habitant
         .add("mdette", DoubleType, true)    // ENDETTEMENT : Encours total de la dette au 31 décembre N : moyenne strate
         .add("rdette", DoubleType, true)    // ENDETTEMENT : Encours total de la dette au 31 décembre N : ratio de structure
         .add("rmdette", DoubleType, true)   // ENDETTEMENT : Encours total de la dette au 31 décembre N : ratio de la strate

         // Annuité de la dette
         .add("annu", DoubleType, true)   // ENDETTEMENT : Annuité de la dette : total
         .add("fannu", DoubleType, true)  // ENDETTEMENT : Annuité de la dette : par habitant
         .add("mannu", DoubleType, true)  // ENDETTEMENT : Annuité de la dette : moyenne strate
         .add("rannu", DoubleType, true)  // ENDETTEMENT : Annuité de la dette : ratio de structure
         .add("rmannu", DoubleType, true) // ENDETTEMENT : Annuité de la dette : ratio de la strate

         // FONDS DE ROULEMENT
         .add("fdr", DoubleType, true)    // ENDETTEMENT : FONDS DE ROULEMENT : total
         .add("ffdr", DoubleType, true)   // ENDETTEMENT : FONDS DE ROULEMENT : par habitant
         .add("mfdr", DoubleType, true)   // ENDETTEMENT : FONDS DE ROULEMENT : moyenne strate

         // ELEMENTS DE FISCALITE DIRECTE LOCALE
         // Bases nettes imposées au profit de la commune
         // Taxe d'habitation (y compris THLV)
         .add("bth", DoubleType, true)       // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : total
         .add("fbth", DoubleType, true)      // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : par habitant
         .add("mbth", DoubleType, true)      // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : moyenne strate
         .add("bthexod", DoubleType, true)   // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : Réductions de base accordées sur délibérations : total
         .add("fbthexod", DoubleType, true)  // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : Réductions de base accordées sur délibérations : par habitant
         .add("mbthexod", DoubleType, true)  // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) : Réductions de base accordées sur délibérations : moyenne strate

         // Taxe foncière sur les propriétés bâties
         .add("bfb", DoubleType, true)       // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : total
         .add("fbfb", DoubleType, true)      // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : par habitant
         .add("mbfb", DoubleType, true)      // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : moyenne strate
         .add("bfbexod", DoubleType, true)   // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : Réductions de base accordées sur délibérations : total
         .add("fbfbexod", DoubleType, true)  // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : Réductions de base accordées sur délibérations : par habitant
         .add("mbfbexod", DoubleType, true)  // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties : Réductions de base accordées sur délibérations : moyenne strate

         // Taxe foncière sur les propriétés non bâties
         .add("bfnb", DoubleType, true)      // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : total
         .add("fbfnb", DoubleType, true)     // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : par habitant
         .add("mbfnb", DoubleType, true)     // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : moyenne strate
         .add("bfnbexod", DoubleType, true)  // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : Réductions de base accordées sur délibérations : total
         .add("fbfnbexod", DoubleType, true) // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : Réductions de base accordées sur délibérations : par habitant
         .add("mbfnbexod", DoubleType, true) // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties : Réductions de base accordées sur délibérations : moyenne strate

         // Taxe additionnelle à la taxe foncière sur les propriétés non bâties
         .add("btafnb", DoubleType, true)    // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : total
         .add("fbtafnb", DoubleType, true)   // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : par habitant
         .add("mbtafnb", DoubleType, true)   // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : moyenne strate

         // Cotisation foncière des entreprises
         .add("btp", DoubleType, true)       // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises : total
         .add("fbtp", DoubleType, true)      // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises : par habitant
         .add("mbtp", DoubleType, true)      // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises : moyenne strate
         .add("btpexod", DoubleType, true)   // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises : Réductions de base accordées sur délibérations : total
         .add("fbtpexod", DoubleType, true)  // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises : Réductions de base accordées sur délibérations : par habitant
         .add("mbtpexod", DoubleType, true)  // ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises : Réductions de base accordées sur délibérations : moyenne strate

         // Les taux et les produits de la fiscalité directe locale (Suite)
         // Produits des impôts locaux (Suite)
         .add("ptafnb", DoubleType, true)    // Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : total
         .add("fptafnb", DoubleType, true)   // Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : par habitant
         .add("mptafnb", DoubleType, true)   // Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : moyenne de la strate
         .add("tafnb", DoubleType, true)     // Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : taux voté
         .add("mtmtafnb", DoubleType, true)  // Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties : taux moyen de la strate

         // Produits des impôts de répartition
         .add("pcfe", DoubleType, true)      // Impôts locaux : Cotisation foncière des entreprises : total
         .add("fpcfe", DoubleType, true)     // Impôts locaux : Cotisation foncière des entreprises : par habitant
         .add("mpcfe", DoubleType, true)     // Impôts locaux : Cotisation foncière des entreprises : moyenne de la strate

         .add("cvaec", DoubleType, true)     // Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises : total
         .add("fcvaec", DoubleType, true)    // Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises : par habitant
         .add("mcvaec", DoubleType, true)    // Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises : moyenne de la strate

         .add("iferc", DoubleType, true)     // Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau : total
         .add("fiferc", DoubleType, true)    // Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau : par habitant
         .add("miferc", DoubleType, true)    // Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau : moyenne de la strate

         .add("tascomc", DoubleType, true)   // Produits des impôts de répartition : Taxe sur les surfaces commerciales : total
         .add("ftascomc", DoubleType, true)  // Produits des impôts de répartition : Taxe sur les surfaces commerciales : par habitant
         .add("mtascomc", DoubleType, true)  // Produits des impôts de répartition : Taxe sur les surfaces commerciales : moyenne de la strate

         .add("nomsst1", StringType, true)   // Strate commune

         // Encours des dettes bancaires net de l'aide du fonds de soutien pour la sortie des emprunts toxiques
         .add("encdbr", DoubleType, true)    // ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques : total
         .add("fencdbr", DoubleType, true)   // ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques : par habitant
         .add("rencdbr", DoubleType, true)   // ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques : ratio de structure
         .add("mencdbr", DoubleType, true)   // ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques : moyenne de la strate
         .add("rmencdbr", DoubleType, true)  // ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques : ratio de la strate

         // Produits de fonctionnement CAF
         .add("pfcaf", DoubleType, true)     // Produits de fonctionnement CAF : total
         .add("fpfcaf", DoubleType, true)    // Produits de fonctionnement CAF : par habitant
         .add("mpfcaf", DoubleType, true)    // Produits de fonctionnement CAF : moyenne de la strate

         // Charges de fonctionnement CAF
         .add("cfcaf", DoubleType, true)     // Charges de fonctionnement CAF : total
         .add("fcfcaf", DoubleType, true)    // Charges de fonctionnement CAF : par habitant
         .add("mcfcaf", DoubleType, true)    // Charges de fonctionnement CAF : moyenne de la strate

         // Encours des dettes bancaires et assimilées
         .add("det2cal", DoubleType, true)   // ENDETTEMENT : Encours des dettes bancaires et assimilées : total
         .add("fdet2cal", DoubleType, true)  // ENDETTEMENT : Encours des dettes bancaires et assimilées : par habitant
         .add("rdet2cal", DoubleType, true)  // ENDETTEMENT : Encours des dettes bancaires et assimilées : ratio de structure
         .add("mdet2cal", DoubleType, true)  // ENDETTEMENT : Encours des dettes bancaires et assimilées : moyenne de la strate
         .add("rmdet2cal", DoubleType, true) // ENDETTEMENT : Encours des dettes bancaires et assimilées : ratio de la strate

         .add("Column 220", StringType, true); // Probablement un bug du fichier csv
   }
}
