package fr.ecoemploi.adapters.outbound.spark.dataset.balance.communes;

import java.io.*;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

import org.apache.spark.sql.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.CogDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.individuel.communes.ComptesIndividuelsCommunesDataset;
import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.ComptesIndividuelsCommune;

/**
 * Test sur les comptes individuels des communes.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkComptesTestApplication.class)
class ComptesIndividuelsCommunesIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 8075316753535004587L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ComptesIndividuelsCommunesIT.class);
   
   /** Dataset de la balance des comptes des communes. */
   @Autowired
   private ComptesIndividuelsCommunesDataset individuelComptesCommunesDataset;
   
   /** Dataset du Code Officiel Géographique. */
   @Autowired
   private CogDataset cogDataset;

   /** Année comptable pour les tests. */
   private static final int EXERCICE = 2019;

   /** Année COG pour les tests. */
   private static final int COG = 2019;

   /**
    * Statistiques sur l'endettement des communes.
    */
   @Test
   @DisplayName("Statistiques sur la capacité de désendettement des communes")
   void capaciteDesendettementCommunes() {
      OptionsCreationLecture options = this.individuelComptesCommunesDataset.optionsCreationLecture();

      assertDoesNotThrow(() -> this.individuelComptesCommunesDataset.alimenterCarteDesendettement(options, null, EXERCICE, COG), "L'alimentation de la carte de désenttetement n'a pas réussi");
   }

   /**
    * Lecture du fichier des comptes individuels de communes.
    */
   @DisplayName("Row des comptes individuels des communes.")
   @ParameterizedTest(name = "Comptes individuels communes {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProviderIndividuelsCommunes.class)
   void datasetComptesIndividuelsCommune(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.individuelComptesCommunesDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      HistoriqueExecution historique = new HistoriqueExecution();

      Dataset<Row> ds = this.individuelComptesCommunesDataset.rowComptesIndividuels(options, historique, EXERCICE, COG);
      assertNotNull(ds, "Aucun dataset n'a été extrait des comptes individuels de commune");

      long count = ds.count();
      LOGGER.info("{} entrées de comptes individuels ont été lues.", count);
      assertNotEquals(0L, count, "Plusieurs comptes individuels de communes auraient du être lus.");
      ds.show();
      
      Dataset<ComptesIndividuelsCommune> comptes = this.individuelComptesCommunesDataset.toDatasetObjetsMetiers(ds);
      ComptesIndividuelsCommune compte = comptes.first();
      assertNotNull(compte.getNomCommune(), "Le nom de la commune d'un compte ne peut pas valoir null");
      assertNotNull(compte.getPopulationCommune(), "La population de la commune ne peut pas valoir null");
      assertNotEquals(0, compte.getPopulationCommune(), "La population de la commune ne peut pas valoir 0");

      dump(comptes, 100, LOGGER);
   }
   
   /**
    * Lire les comptes individuels de Douarnenez.
    */
   @Test
   @DisplayName("Comptes individuels de Douarnenez (29046).")
   void datasetComptesIndividuelsDouarnenez() {
      OptionsCreationLecture options = this.individuelComptesCommunesDataset.optionsCreationLecture();

      Dataset<Row> ds = this.individuelComptesCommunesDataset.rowComptesIndividuels(options, null, EXERCICE, COG);
      assertNotNull(ds, "Aucun dataset n'a été extrait des comptes individuels de commune");

      ds = ds.where(ds.col("codeCommune").equalTo("29046"));
      
      ds.show(500, false);
      
      List<ComptesIndividuelsCommune> comptes = this.individuelComptesCommunesDataset.toComptesIndividuels(ds);      
      assertEquals(1, comptes.size(), "Une et une seule ligne de comptes individuels auraient du être lue.");
      LOGGER.info(comptes.get(0).toString());
   }

   /**
    * Lire les comptes individuels de Gif-sur-Yvette.
    */
   @Test
   @DisplayName("Comptes individuels de Gif-sur-Yvette (91272).")
   void datasetComptesIndividuelsGifSurYvette() {
      OptionsCreationLecture options = this.individuelComptesCommunesDataset.optionsCreationLecture();

      Dataset<Row> ds = this.individuelComptesCommunesDataset.rowComptesIndividuels(options, null, EXERCICE, COG);
      assertNotNull(ds, "Aucun dataset n'a été extrait des comptes individuels de commune");

      ds = ds.where(ds.col("codeCommune").equalTo("91272"));
      ds.show(500, false);
      
      List<ComptesIndividuelsCommune> comptes = this.individuelComptesCommunesDataset.toComptesIndividuels(ds);      
      assertEquals(1, comptes.size(), "Une et une seule ligne de comptes individuels auraient du être lue.");
      LOGGER.info(comptes.get(0).toString());
   }

   /**
    * Présenter les taxes locales d'une intercommunalité sur plusieurs années.
    */
   @Test
   @DisplayName("Taxes locales des communes d'une intercommunalité")
   void taxesLocalesCommunesIntercommunalite() {
      OptionsCreationLecture options = this.individuelComptesCommunesDataset.optionsCreationLecture();

      Dataset<Row> taxesAnnuelles = this.individuelComptesCommunesDataset.taxesLocalesCommunesIntercommunalite(options, null, "200039808", 2013, 2019, 2020);
      assertNotEquals(0, taxesAnnuelles.count(), "Au moins une taxe annuelle aurait dû être lue.");

      taxesAnnuelles.show(5000, false);
   }
   
   /**
    * Lire les comptes individuels de Douarnenez.
    */
   @Test
   @DisplayName("Comptes individuels des communes d'une EPCI.")
   void datasetComptesIndividuelsCommunesEPCI() {
      OptionsCreationLecture options = this.individuelComptesCommunesDataset.optionsCreationLecture();
      Dataset<Row> ds = this.individuelComptesCommunesDataset.rowComptesIndividuels(options, null, EXERCICE, COG);
      
      CodeOfficielGeographique cog = this.cogDataset.obtenirCodeOfficielGeographique(COG, true);
      
      try {
         Set<String> codesCommunesMembres = cog.getIntercommunalites().getIntercommunalite(new SIRENCommune("242900645")).getCodesCommunesMembres();
         
         for(String codeCommune : codesCommunesMembres) {
            try {
               Commune commune = cog.getCommune(codeCommune);
               LOGGER.info("\nComptes individuels de la commune de {} en {} : \n", commune.getNomCommune(), EXERCICE);
               
               Dataset<Row> dsComptesIndividuelsCommune = ds.where(ds.col("codeCommune").equalTo(codeCommune));
               
               List<ComptesIndividuelsCommune> comptes = this.individuelComptesCommunesDataset.toComptesIndividuels(dsComptesIndividuelsCommune);      
               assertEquals(1, comptes.size(), "Une et une seule ligne de comptes individuels auraient du être lue.");
               LOGGER.info(comptes.get(0).toString());
            } 
            catch(CommuneInexistanteException e) {
               LOGGER.warn(e.getMessage());
            }
         }
      } 
      catch(IntercommunaliteAbsenteDansCommuneSiegeException e) {
         LOGGER.warn(e.getMessage());
      }
   }
}
