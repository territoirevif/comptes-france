package fr.ecoemploi.adapters.outbound.spark.dataset.balance.communes;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.util.*;

import fr.ecoemploi.adapters.outbound.port.compte.*;
import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.model.territoire.comptabilite.Compte;

import org.junit.jupiter.api.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

/**
 * Test sur le service de la balance des comptes des communes.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkComptesTestApplication.class)
class BalanceComptesCommunesServiceIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 1816404273903780150L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(BalanceComptesCommunesServiceIT.class);
   
   /** Dataset de la balance des comptes des communes. */
   @Autowired
   private BalanceCompteCommuneRepository balanceCompteService;

   /** Année comptable pour les tests. */
   private static final int EXERCICE = 2022;

   /** Année COG pour les tests. */
   private static final int COG = 2022;

   /**
    * Lecture de la balance des comptes de la commune de Douarnenez.
    */
   @Test
   @DisplayName("Comptes de la commune de Douarnenez (29046)")
   void comptesCommuneDouarnenez() {
      Map<SIRET, List<Compte>> comptes = this.balanceCompteService.obtenirBalanceComptesCommune(COG, EXERCICE, new CodeCommune("29046"));
      assertNotEquals(0, comptes.size(), "Plusieurs comptes auraient du être lus pour Douarnenez.");

      Set<SIRET> sirets = comptes.keySet();
      
      for(SIRET siret : sirets) {
         LOGGER.info("Pour le siret : {}", siret);
         LOGGER.info(comptes.get(siret).toString());
      }
   }
}
