package fr.ecoemploi.adapters.outbound.spark.dataset.balance.communes;

import java.io.*;
import static org.junit.jupiter.api.Assertions.*;

import org.apache.spark.sql.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.plan.comptes.PlanDeComptesDataset;
import fr.ecoemploi.domain.utils.objets.TechniqueException;

/**
 * Test des plans de comptes.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkComptesTestApplication.class)
class PlansDeComptesIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 6597821490971472006L;

   /** Dataset des plans de comptes. */
   @Autowired
   private PlanDeComptesDataset planDeComptesDataset;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Activer ou désactiver les caches des datasets.
    */
   @BeforeEach
   public void desactiverCaches() {
      // this.planDeComptesDataset.loader().setCache(activerCache());
   }

   /**
    * Lecture du fichier du plan de compte.
    * @throws TechniqueException si un incident survient.
    */
   @Test
   @DisplayName("Lecture du plan de comptes.")
   void datasetPlanDeComptes() throws TechniqueException {
      Dataset<Row> plansDeComptes = this.planDeComptesDataset.plansDeComptes();

      if (useExplainPlan()) {
         plansDeComptes.explain(explainPlan());
      }
      
      assertNotEquals(0, plansDeComptes.count(), "Plusieurs comptes de plan de comptes auraient du être lus.");
      plansDeComptes.show(100, false);
   }
   
   /**
    * Lecture des nomenclatures comptables.
    */
   @Test
   @DisplayName("Lecture des nomenclatures comptables.")
   void datasetNomenclaturesComptables() {
      Dataset<Row> nomemclatures = this.planDeComptesDataset.rowNomenclatures(this.session);
      nomemclatures.explain("formatted");

      assertNotEquals(0, nomemclatures.count(), "Plusieurs nomenclatures comptables auraient dûes être lues.");
      nomemclatures.show(100, false);
   }
}
