package fr.ecoemploi.adapters.outbound.spark.dataset.balance.communes;

import static org.apache.spark.sql.functions.*;
import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.util.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

import org.apache.spark.sql.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

/**
 * Test sur le dataset de la balance des comptes des communes.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkComptesTestApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class BalanceComptesCommunesDatasetIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   //@Serial
   @Serial
   private static final long serialVersionUID = 5878582886504165631L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(BalanceComptesCommunesDatasetIT.class);
   
   /** Dataset de la balance des comptes des communes. */
   @Autowired
   private BalanceComptesCommunesDataset balanceComptesCommunesDataset;

   /** Année comptable pour les tests. */
   private static final int EXERCICE = 2022;

   /** Année COG pour les tests. */
   private static final int COG = 2022;
   
   /**
    * Désactiver les caches des datasets à tester.
    */
   @BeforeEach
   public void desactiverCaches() {
      // this.balanceComptesCommunesDataset.loader().setCache(activerCache());
      this.balanceComptesCommunesDataset.emetteur().setAvertirIncidents(false);
   }
   
   /**
    * Lecture du fichier de la balance des communes.
    */
   @DisplayName("Lecture complète du fichier des comptes des communes")
   @ParameterizedTest(name = "Balance communes {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   @Order(10)
   void comptesCommune(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.balanceComptesCommunesDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> comptes = this.balanceComptesCommunesDataset.rowComptesCommunes(options, historiqueExecution, EXERCICE, COG);

      if (useExplainPlan()) {
         comptes.explain(explainPlan());
      }
      
      assertNotEquals(0L, comptes.count(), "Plusieurs comptes de communes auraient du être lus.");
      comptes.show(5000, false);
   }

   /**
    * Dresser la liste des nomemclatures comptables observées.
    */
   @Test
   @DisplayName("Dresser la liste des nomenclatures comptables observées.")
   @Order(30)
   void nomenclaturesComptables() {
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> nomenclatures = this.balanceComptesCommunesDataset.rowComptesCommunes(null, historiqueExecution, EXERCICE, COG).select("nomenclatureComptable")
         .distinct().orderBy("nomenclatureComptable");

      assertNotEquals(0, nomenclatures.count(), "Plusieurs nomenclatures auraient du être lues depuis la balance des comptes de communes.");
      nomenclatures.show(100);
   }
   
   /**
    * Lecture de la balance des comptes de la commune de Douarnenez.
    */
   @Test
   @DisplayName("Comptes de la commune de Douarnenez (29046)")
   @Order(20)
   void comptesCommuneDouarnenez() {
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> comptesDouarnenez = this.balanceComptesCommunesDataset.obtenirComptesCommune(null, historiqueExecution, "29046", EXERCICE, COG);
      assertNotEquals(0, comptesDouarnenez.count(), "Plusieurs comptes de la commune de Douarnenez auraient du être lus.");
      comptesDouarnenez.explain("formatted");

      List<String> sirets = comptesDouarnenez.select("siret").distinct().as(Encoders.STRING()).collectAsList();
      
      for(String siret : sirets) {
         LOGGER.info("Pour le siret : {}", siret);
         Dataset<Row> comptesSiret = comptesDouarnenez.where(comptesDouarnenez.col("siret").equalTo(siret)).orderBy(comptesDouarnenez.col("numeroCompte"));

         comptesSiret.show(2000, false);
      }
   }
   
   /**
    * Création d'une matrice code commune X comptes.
    */
   @Test
   @Disabled("Ce test est devenu trop long, et la fonctionnalité n'est pas sûre de demeurer")
   @DisplayName("Création d'une matrice code commune X comptes")
   void matriceCommunesComptes() {
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Column condition = col("populationMunicipale").$less(lit(3500)).and(col("typeBudget").equalTo(lit("1"))); // Comptes issus des budgets principaux des communes de moins de 3 500h.

      Dataset<Row> matrice = this.balanceComptesCommunesDataset.matriceCommuneComptes(null, historiqueExecution, EXERCICE, COG, true, condition);
      assertNotEquals(0, matrice.count(), "La matrice des comptes des communes de moins de 3 500h devrait être alimentée.");

      matrice.explain("formatted");
      matrice.show(1000);
   }
}
