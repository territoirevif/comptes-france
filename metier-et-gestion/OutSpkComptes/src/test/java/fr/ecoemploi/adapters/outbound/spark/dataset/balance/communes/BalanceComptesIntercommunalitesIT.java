package fr.ecoemploi.adapters.outbound.spark.dataset.balance.communes;

import java.io.*;
import java.text.*;
import java.util.*;

import org.apache.commons.lang3.time.*;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.slf4j.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import org.apache.spark.sql.*;

import fr.ecoemploi.domain.model.territoire.comptabilite.Compte;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.balance.intercommunalites.BalanceComptesIntercommunalitesDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

/**
 * Test sur la balance des comptes des communes.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkComptesTestApplication.class)
class BalanceComptesIntercommunalitesIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -9050052369064398294L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(BalanceComptesIntercommunalitesIT.class);
   
   /** Dataset de la balance des comptes des intercommunalités. */
   @Autowired
   private BalanceComptesIntercommunalitesDataset balanceComptesIntercommunalitesDataset;

   /** Année comptable pour les tests. */
   private static final int EXERCICE = 2022;

   /** Année COG pour les tests. */
   private static final int COG = 2022;

   /**
    * Lecture du fichier de la balance des intercommunalités.
    */
   @DisplayName("Row des comptes des intercommunalités")
   @ParameterizedTest(name = "Balance intercommunalités {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   void comptesIntercommunalites(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.balanceComptesIntercommunalitesDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> comptes = this.balanceComptesIntercommunalitesDataset.rowComptesIntercommunalites(options, historiqueExecution, EXERCICE, COG /*, Verification.SHOW_DATASET */);
      long nombreComptes = comptes.count();
      
      assertNotEquals(0, nombreComptes, "Plusieurs comptes d'intercommunalités auraient du être lus.");
      comptes.show(5000, false);

      LOGGER.info("{} lignes de comptabilité ont été retenues.", nombreComptes);
   }

   /**
    * Lecture de la balance de la communauté de communes des Loges.
    */
   @Test
   @DisplayName("Comptes de l'intercommunalité des Loges (244500427)")
   void comptesIntercommunaliteDesLoges() {
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> comptesLoges = this.balanceComptesIntercommunalitesDataset.rowComptesIntercommunalites(null, historiqueExecution, EXERCICE, COG);
      comptesLoges = comptesLoges.where(comptesLoges.col("siren").equalTo("244500427"));
      
      List<Row> rowsSirets = comptesLoges.select("siret").distinct().collectAsList();
      
      for(Row rowSiret : rowsSirets) {
         String siret = rowSiret.getString(0);
         LOGGER.info("Pour le siret : {}", siret);
         Dataset<Row> comptesSiret = comptesLoges.where(comptesLoges.col("siret").equalTo(siret)).orderBy(comptesLoges.col("numeroCompte"));

         List<Compte> comptes = this.balanceComptesIntercommunalitesDataset.toObjetsMetiersList(comptesSiret);
         assertNotEquals(0, comptesSiret.count(), MessageFormat.format("Plusieurs comptes d''intercommunalité auraient du être lus pour le siret {0}.", siret));
         LOGGER.info(comptes.toString());
         
         comptesSiret.show(2000, false);
      }
   }
   
   /**
    * Performances : Lire les comptes de toutes les intercommunalités du Finistère.
    */
   @Test @Disabled("Il pose un problème d'outof memory déguisé")
   @DisplayName("Lire les comptes de toutes les intercommunalités du Finistère")
   void performancesListeIntercosFinistere() {
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      String[] epci = {
         "242900314", "200068120", "242900835", "242900694", "242900769",
         "242900801", "242900074", "242900553", "242900702", "242900751",
         "200067072", "242900660", "242900793", "200067247", "200066868",
         "242900645", "242900710", "242900744", "242900629", "242900561",
         "200067197"      
      };
      
      Dataset<Row> comptes = this.balanceComptesIntercommunalitesDataset.rowComptesIntercommunalites(null, historiqueExecution, EXERCICE, COG);
      assertNotEquals(0, comptes.count(), "Il devrait y avoir plusieurs comptes d'intercommunalités lus.");

      comptes = comptes.persist();
      LOGGER.info("Il y a {} comptes intercommunaux au total.", comptes.count());
      
      StopWatch chrono = new StopWatch();
      chrono.start();
      
      Dataset<Row> union = null;
      
      for(String codeEPCI : epci) {
         union = union != null ? union.union(comptes.where(comptes.col("siren").equalTo(codeEPCI))) : comptes.where(comptes.col("siren").equalTo(codeEPCI));
      }

      LOGGER.info("Il y a {} comptes dans les intercommunalités", union != null ? union.count() : 0);

      chrono.stop();
      LOGGER.info("Durée des {} recherches : {}s", epci.length, chrono.getTime()/1000.0);
   }

   /**
    * Dresser la liste des nomemclatures comptables observées.
    */
   @Test
   @DisplayName("Dresser la liste des nomenclatures comptables observées.")
   void nomenclaturesComptables() {
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> nomenclatures = this.balanceComptesIntercommunalitesDataset.rowComptesIntercommunalites(null, historiqueExecution, EXERCICE, COG).select("nomenclatureComptable")
         .distinct().orderBy("nomenclatureComptable");

      assertNotEquals(0, nomenclatures.count(), "Plusieurs nomenclatures auraient du être lues depuis la balance des comptes d'intercommunalités.");
      nomenclatures.show(100);
   }

   /**
    * Démarrer le chronométrage du test
    */
   @BeforeEach
   public void demarrerChrono() {
      this.chrono.start();
   }
}
