package fr.ecoemploi.adapters.outbound.spark.dataset.balance.communes;

import java.io.*;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

import org.apache.spark.sql.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.individuel.intercommunalites.ComptesIndividiuelsIntercommunalitesDataset;
import fr.ecoemploi.domain.model.territoire.comptabilite.ComptesIndividuelsIntercommunalite;

/**
 * Test sur les comptes individuels des intercommunalités.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkComptesTestApplication.class)
class ComptesIndividuelsIntercommunalitesIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 8326681232752795728L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ComptesIndividuelsIntercommunalitesIT.class);
   
   /** Dataset de la balance des comptes des communes. */
   @Autowired
   private ComptesIndividiuelsIntercommunalitesDataset individuelComptesIntercommunauxDataset;

   /** Année comptable pour les tests. */
   private static final int EXERCICE = 2022;

   /** Année COG pour les tests. */
   private static final int COG = 2022;
   
   /**
    * Désactiver les caches des datasets à tester.
    */
   @BeforeEach
   public void desactiverCaches() {
      // this.individuelComptesIntercommunauxDataset.loader().setCache(false);
   }

   /**
    * Lecture du fichier des comptes individuels des intercommunalités.
    */
   @DisplayName("Row fichier des comptes individuels des intercommunalités.")
   @ParameterizedTest(name = "Comptes individuels intercommunalités {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   void datasetComptesIndividuelsIntercommunalites(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.individuelComptesIntercommunauxDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> ds = this.individuelComptesIntercommunauxDataset.rowComptesIntercommunaux(options, historiqueExecution, EXERCICE, COG);
      long count = ds.count();
      LOGGER.info("{} entrées de comptes individuels ont été lues.", count);
      assertNotEquals(0L, count, "Plusieurs comptes individuels d'intercommunalités auraient du être lus.");
      ds.show(false);
      
      Dataset<ComptesIndividuelsIntercommunalite> dsComptes = this.individuelComptesIntercommunauxDataset.toDatasetObjetsMetiers(ds);
      dump(dsComptes, 100, LOGGER);
   }
   
   /**
    * Lire les comptes individuels de la Communauté de communes de Douarnenez.
    */
   @Test
   @DisplayName("Comptes individuels des intercommunalités de la Communauté de communes de Douarnenez.")
   void datasetComptesIndividuelsIntercommunaliteCCDouarnenez() {
      OptionsCreationLecture options = this.individuelComptesIntercommunauxDataset.optionsCreationLecture();

      Dataset<Row> ds = this.individuelComptesIntercommunauxDataset.rowComptesIntercommunaux(options, new HistoriqueExecution(), EXERCICE, COG)
         .where("siren = '242900645'");

      assertNotNull(ds, "Les comptes individuels de la CC de Douarnenez auraient dû être trouvés");
      ds.show(false);

      List<ComptesIndividuelsIntercommunalite> comptes = this.individuelComptesIntercommunauxDataset.toComptesIndividuels(ds);
      assertEquals(1, comptes.size(), "Une et une seule ligne de comptes individuels auraient du être lue.");
      LOGGER.info(comptes.get(0).toString());
   }
}
