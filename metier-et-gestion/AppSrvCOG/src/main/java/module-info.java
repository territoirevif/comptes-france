module fr.ecoemploi.application.service.cog {
   requires fr.ecoemploi.application.service.core;
   requires fr.ecoemploi.application.port.cog;
   requires fr.ecoemploi.outbound.port.cog;
   requires fr.ecoemploi.domain.model;

   requires spring.beans;
   requires spring.context;

   requires org.slf4j;

   exports fr.ecoemploi.application.service.cog;
}
