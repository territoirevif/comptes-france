package fr.ecoemploi.application.service.cog;

import java.io.Serial;
import java.util.*;

import fr.ecoemploi.adapters.outbound.port.cog.*;
import fr.ecoemploi.application.port.cog.CogPort;
import fr.ecoemploi.domain.model.territoire.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

/**
 * Service d'accès au code officiel géographique. 
 * @author Marc Le Bihan
 */
@Service
public class CogService implements CogPort {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -565865581203087279L;

   /** Repository du code officiel géographique. */
   private final CogRepository cogRepository;

   /** Repository des évènements communaux. */
   private final EvenementsCommunauxRepository evenementsCommunauxRepository;

   /**
    * Construire un service d'accès au code officiel géographique et aux évènements communaux
    * @param cogRepository Repository du COG
    * @param evenementsCommunauxRepository Repository des évènements communaux.
    */
   @Autowired
   public CogService(CogRepository cogRepository, EvenementsCommunauxRepository evenementsCommunauxRepository) {
      this.cogRepository = cogRepository;
      this.evenementsCommunauxRepository = evenementsCommunauxRepository;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void chargerReferentiels(int anneeCOG) {
      this.cogRepository.chargerReferentiels(anneeCOG);
   }

   /**
    * Obtenir le code officiel géographique.
    * @param anneeCOG année du COG recherché.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *                                          false si seules les communes de type COM sont lues.
    * @return Code Officiel Géographique.
    */
   @Override
   public CodeOfficielGeographique obtenirCodeOfficielGeographique(int anneeCOG, boolean inclureCommunesDelegueesAssociees) {
      return this.cogRepository.obtenirCodeOfficielGeographique(anneeCOG, inclureCommunesDelegueesAssociees);
   }
   
   /**
    * Obtenir la liste des communes.
    * @param anneeCOG Année de référence du Code Officiel Géographique.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *                                          false si seules les communes de type COM sont lues.
    * @return Liste des communes.
    */
   @Override
   public Map<String, Commune> obtenirCommunes(int anneeCOG, boolean inclureCommunesDelegueesAssociees) {
      return obtenirCodeOfficielGeographique(anneeCOG, inclureCommunesDelegueesAssociees).getCommunesAsMap();
   }

   /**
    * Obtenir la liste des communes, triées par une locale.
    * @param anneeCOG Année de référence du Code Officiel Géographique.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *                                          false si seules les communes de type COM sont lues.
    * @param locale Locale à utiliser pour le tri.
    * @return Liste des communes.
    */
   @Override
   public List<Commune> obtenirCommunesTriParLocale(int anneeCOG, boolean inclureCommunesDelegueesAssociees, String locale) {
      // Obtenir les communes, puis les trier selon l'ordre de la locale désirée.
      Communes communesTriees = obtenirCodeOfficielGeographique(anneeCOG, inclureCommunesDelegueesAssociees).getCommunes().triParNomDeCommune(Locale.forLanguageTag(locale));
      return new ArrayList<>(communesTriees.values());
   }

   /**
    * Déterminer ce qu'est devenue une commune au 1er Janvier d'une année.
    * @param codeCommune Code de la commune initiale.
    * @param annee Année considérée.
    * @return Commune résultante ou null, s'il n'y a aucun changement.
    */
   @Override
   public CommuneHistorique estDevenue(String codeCommune, int annee) {
      return this.evenementsCommunauxRepository.estDevenue(codeCommune, annee);
   }
}
