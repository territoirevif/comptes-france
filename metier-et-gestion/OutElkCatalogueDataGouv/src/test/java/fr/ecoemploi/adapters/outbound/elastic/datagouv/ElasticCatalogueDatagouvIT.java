package fr.ecoemploi.adapters.outbound.elastic.datagouv;

import java.util.List;

import org.junit.jupiter.api.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import fr.ecoemploi.domain.model.catalogue.JeuDeDonneesElastic;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests sur l'insertion et lecture dans elasticsearch des jeux de données du catalogue data.gouv.fr
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkElasticCatalogueDatagouvTestApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ElasticCatalogueDatagouvIT {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ElasticCatalogueDatagouvIT.class);

   /** Composant Elastic pour le catalogue data.gouv.fr. */
   @Autowired
   private CatalogDatagouvElastic catalogDatagouvElastic;

   /**
    * Insertion de données Elastic
    * Sous Kibana, sont contenu peut être observé par un
    * <br><code>
    * GET catalogue-index-test/_search
    * {
    *     "query": {
    *         "match_all": {}
    *     }
    * }
    * </code>
    */
   @Order(10)
   @Test @DisplayName("Insertion de jeux de données de catalogue")
   void insertionCatalogue() {
      assertDoesNotThrow(() ->
         this.catalogDatagouvElastic.chargerJeuxDeDonneesDatagouv(),
         "La réinsertion de jeux de données du catalogue dans Elastic a échoué");
   }

   @Order(100)
   @Test @DisplayName("Recherche dans les jeux de données Elastic")
   void recherche() {
      List<JeuDeDonneesElastic> j1 = this.catalogDatagouvElastic.search("quimper", null, JeuDeDonneesElastic.class);
         assertFalse(j1.isEmpty(), "Au moins un résultat de recherche global aurait dû être obtenu");
         dump(j1);

      long nombreTitresAyantQuimper = j1.stream().filter(jeu -> jeu.getTitre().toLowerCase().contains("quimper")).count();

      List<JeuDeDonneesElastic> j2 = this.catalogDatagouvElastic.search("quimper", "titre", JeuDeDonneesElastic.class);
         assertFalse(j2.isEmpty(), "Au moins un résultat de recherche sur le champ titre aurait dû être obtenu");
         dump(j2);

      assertTrue(j1.size() >= j2.size(), "Davantage de résultats par restriction que sans");
      assertEquals(j2.size(), nombreTitresAyantQuimper, "Le nombre de documents obtenus par le champ titre n'est pas celui attendu");
   }

   /**
    * Logger les résultats de recherche.
    * @param jeuxDeDonnees Liste des résultats
    */
   private void dump(List<JeuDeDonneesElastic> jeuxDeDonnees) {
      LOGGER.info("{} résultats de recherche :", jeuxDeDonnees.size());
      jeuxDeDonnees.forEach(jeu -> LOGGER.info(jeu.getTitre()));
   }
}
