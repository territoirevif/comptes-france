package fr.ecoemploi.adapters.outbound.elastic.datagouv;

import java.io.Serial;
import java.util.*;
import java.util.stream.Stream;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import fr.ecoemploi.adapters.outbound.elastic.core.*;
import fr.ecoemploi.adapters.outbound.port.catalogue.CatalogueJeuxDonneesRepository;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.datagouv.CatalogueDatagouvJeuxDeDonneesDataset;
import fr.ecoemploi.domain.model.catalogue.*;
import fr.ecoemploi.domain.utils.objets.ObjetMetier;

/**
 * Accès à Elastic pour les données de type Jeux de Données (catalogue data.gouv.fr)
 * @author Marc Le Bihan
 */
@Service
@Qualifier("elk")
public class CatalogDatagouvElastic extends AbstractElastic implements CatalogueJeuxDonneesRepository {
   @Serial
   private static final long serialVersionUID = -4411756041138416346L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CatalogDatagouvElastic.class);

   /** Lecteurs de datasets de jeux de données de data.gouv.fr */
   @Autowired
   private CatalogueDatagouvJeuxDeDonneesDataset jeuxDeDonneesDataset;

   /** Taille des blocks de l'écriture bulk */
   private final int streamSplitBlockSize;

   /** Taille des blocks de l'écriture bulk */
   private final int bulk;

   /**
    * Pour la désérialisation.
    */
   public CatalogDatagouvElastic() {
      super();
      this.bulk = 0;
      this.streamSplitBlockSize = 0;
   }

   /**
    * Construire un composant Elastic pour les jeux de données data.gouv.fr
    * @param indexName Index Elastic associé.
    * @param aliasName Alias de l'index, optionnel.
    * @param streamSplitBlockSize Taille du bloc
    * @param bulk Taille du bulk
    */
   @Autowired
   public CatalogDatagouvElastic(@Value("${catalogue.elastic.index.name}") String indexName, @Value("${catalogue.elastic.alias.name}") String aliasName,
      @Value("${catalogue.elastic.stream.split.block.size}") int streamSplitBlockSize,
      @Value("${catalogue.elastic.bulk.block}") int bulk) {
      super(indexName, aliasName);
      this.bulk = bulk;
      this.streamSplitBlockSize = streamSplitBlockSize;
   }

   /**
    * Créer un index Elastic
    */
   public void createIndex() {
      JeuDeDonneesElastic jeuElastic = new JeuDeDonneesElastic();
      ObjetMetier.alimenter(jeuElastic).tags(Set.of("d1", "d2"));

      super.createIndex(jeuElastic, JeuDeDonneesElastic::getCatalogueId, Set.of("titre", "description", "organisation"));
   }

   /**
    * Insérer dans l'index Elastic des jeux de données.
    * @param jeuxDeDonnees Jeux de données.
    */
   public void insert(Stream<JeuDeDonneesElastic> jeuxDeDonnees) {
      Objects.requireNonNull(jeuxDeDonnees, "Le jeu de données à insérer dans Elastic ne peut pas valoir null.");

      super.insert(jeuxDeDonnees, JeuDeDonneesElastic.class, JeuDeDonneesElastic::getCatalogueId, this.bulk);
   }

   /**
    * Insérer les jeux de données du dataset Spark dans Elastic
    */
   @Override
   public void chargerJeuxDeDonneesDatagouv() {
      deleteIndex();
      createIndex();

      LOGGER.info("Insertion du catalogue data.gouv.fr Spark dans l'index {} Elastic", getIndexName());

      OptionsCreationLecture options = this.jeuxDeDonneesDataset.optionsCreationLecture();

      Dataset<JeuDeDonneesElastic> jeuxDeDonneesElastic = this.jeuxDeDonneesDataset.catalogueDataset(options, new HistoriqueExecution())
         .map((MapFunction<JeuDeDonnees, JeuDeDonneesElastic>) JeuDeDonneesElastic::new, Encoders.bean(JeuDeDonneesElastic.class));

      insert(this.jeuxDeDonneesDataset.toStream(jeuxDeDonneesElastic, this.streamSplitBlockSize));
   }

   /**
    * Rechercher dans Elastic
    * @param searchText Texte à rechercher
    * @param field Champ où rechercher, spécifiquement. Peut valoir null, et alors la recherche est globale.
    * @return Liste de résultats de recherche
    * @throws RechercheElasticEchoueeException si un incident de recherche survient
    */
   public List<JeuDeDonneesElastic> recherche(String searchText, String field) {
      return search(searchText, field, JeuDeDonneesElastic.class);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Map<String, Long> enumererValeursDistinctes(String champ) {
      throw new UnsupportedOperationException("enumererValeursDistinctes n'est pas admise sur un Catalogue de jeux de données Elastic");
   }
}
