/**
 * Extractions elasticsearch du catalogue des données open data de data.gouv.fr
 * @author Marc LE BIHAN
 */
package fr.ecoemploi.adapters.outbound.elastic.datagouv;
