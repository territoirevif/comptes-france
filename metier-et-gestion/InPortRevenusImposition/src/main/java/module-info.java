/**
 * Inbound Port des revenus et imposition
 */
module fr.ecoemploi.inbound.port.revenus {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.adapters.inbound.port.revenus;
}
