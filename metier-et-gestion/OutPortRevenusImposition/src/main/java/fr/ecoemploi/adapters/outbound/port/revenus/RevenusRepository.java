package fr.ecoemploi.adapters.outbound.port.revenus;

import java.io.Serializable;

/**
 * Interface d'accès aux données des revenus et imposition des ménages.
 * @author Marc Le Bihan
 */
public interface RevenusRepository extends Serializable {
   /**
    * Charger les revenus et imposition des ménages.
    * @param anneeImposition Année d'imposition (exemple : 2018 = déclaration d'impôt 2019).
    * @param anneeCOG Année du code officiel géographique.
    */
   void chargerRevenusEtImpositionMenages(int anneeImposition, int anneeCOG);
}
