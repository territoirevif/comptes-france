/**
 * Outbound Port des revenus et imposition
 */
module fr.ecoemploi.outbound.port.revenus {
   requires fr.ecoemploi.domain.model;
   requires spring.context;

   exports fr.ecoemploi.adapters.outbound.port.revenus;
}
