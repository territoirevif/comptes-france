/**
 * Inbound Port de la base équipement
 */
module fr.ecoemploi.inbound.port.equipement {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.adapters.inbound.port.equipement;
}
