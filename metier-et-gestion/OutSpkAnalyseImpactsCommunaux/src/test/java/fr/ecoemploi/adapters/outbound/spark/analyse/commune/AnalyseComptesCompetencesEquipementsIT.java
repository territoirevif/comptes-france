package fr.ecoemploi.adapters.outbound.spark.analyse.commune;

import java.text.MessageFormat;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;
import org.apache.spark.ml.classification.*;
import org.apache.spark.ml.evaluation.*;
import org.apache.spark.ml.feature.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.analyse.intercommunalite.SparkImpactsTestApplication;
import fr.ecoemploi.adapters.outbound.spark.dataset.balance.communes.BalanceComptesCommunesDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.equipement.EquipementDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.plan.comptes.PlanDeComptesDataset;

/**
 * Analyse des comptes, compétences et équipements.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkImpactsTestApplication.class)
class AnalyseComptesCompetencesEquipementsIT extends AbstractRestIT {
    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AnalyseComptesCompetencesEquipementsIT.class);

    /** Dataset des comptes de communes. */
    @Autowired
    private BalanceComptesCommunesDataset comptesCommunesDataset;

    /** Plan de comptes. */
    @Autowired
    private PlanDeComptesDataset planDeComptesDataset;

    /** Dataset de la base équipement. */
    @Autowired
    private EquipementDataset equipementDataset;

    /** Proportion de jeu d'apprentissage. */
    public final double PROPORTION_APPRENTISSAGE = 0.7;

    /** Graine pour générateur aléatoire. */
    public final long SEED = 1;

    /** Année de référence du code officiel géographique. */
    public final int ANNEE_COG = 2021;

    /** Année de référence de la base équipement. */
    public final int ANNEE_BASE_EQUIPEMENT = 2020;

    /** Nombre de composantes majeures affichées. */
    public final int NOMBRE_FEATURES_IMPORTANTES = 20;

    /** True, s'il faut centrer réduire. */
    public final boolean CENTRER_REDUIRE = true;

    /**
     * Enumération des tests, classifications, etc.
     */
    @Test // Expérimental
    @DisplayName("Analyse")
    void analyse() {
        assertDoesNotThrow(() -> {
            // Préparation de la matrice lignes (individus) = communes, colonnes = numéros comptes et codes équipements.
            Dataset<Row> comptesEquipements = preparationDonnees(null, true);

            // Extraction d'un jeu d'apprentissage et d'un jeu de test
            VectorAssembler assembleur = getVectorAssemblerForFeaturesComptes(comptesEquipements);
            comptesEquipements = assembleur.transform(comptesEquipements);

            // Centrer réduire les données
            if (this.CENTRER_REDUIRE) {
                StandardScaler scaler = new StandardScaler().setInputCol("features").setOutputCol("scaledFeatures")
                   .setWithStd(true)   // Réduire
                   .setWithMean(true); // Centrer

                comptesEquipements = scaler.fit(comptesEquipements).transform(comptesEquipements);

                comptesEquipements = comptesEquipements.withColumnRenamed("features", "reference");
                comptesEquipements = comptesEquipements.withColumnRenamed("scaledFeatures", "features");
            }

            Dataset<Row>[] jeux = comptesEquipements.randomSplit(new double[]{this.PROPORTION_APPRENTISSAGE, 1.0 - this.PROPORTION_APPRENTISSAGE}, this.SEED);

            String[] codesEquipements = {
               "F303", // Cinéma
               "F307", // Bibliothèque
            };

            for (String codeEquipement : codesEquipements) {
                extraireComptesMajeurs(codeEquipement, jeux, assembleur.getInputCols());
            }
        }, "La préparation de la matrice et son analyse ont échoué");
    }

    /**
     * Extrairee les comptes majeurs impliqués dans la comptabilité d'un équipement.
     * @param nomColonneEquipement Nom de colonne de l'équipement.
     * @param jeux Jeu d'apprentissage et jeu de test.
     * @param colonnesDesFeatures Nom des colonnes des features.
     */
    private void extraireComptesMajeurs(String nomColonneEquipement, Dataset<Row>[] jeux, String[] colonnesDesFeatures) {
        Dataset<Row> apprentissage = jeux[0].select("features", nomColonneEquipement);
        Dataset<Row> donneesDeTest = jeux[1].select("features", nomColonneEquipement);

        // Apprentissage et prédiction d'équipement d'après les comptes
        RandomForestClassifier algorithme = new RandomForestClassifier();
        algorithme.setLabelCol(nomColonneEquipement);
        RandomForestClassificationModel modele = algorithme.fit(apprentissage);
        Dataset<Row> predictions = modele.transform(donneesDeTest);

        // Evaluation de la qualité de la prédiction
        MulticlassClassificationEvaluator evaluateur = new MulticlassClassificationEvaluator();
        evaluateur.setLabelCol(nomColonneEquipement);
        evaluateur.setPredictionCol("prediction");
        evaluateur.setMetricName("accuracy");

        double precision = evaluateur.evaluate(predictions);

        // Calcul de l'importance de chaque composante dans le modèle.
        double[] importanceComposantes = modele.featureImportances().toArray(); // Indexés par numéro de champ
        Map<String, Double> composantes = new LinkedHashMap<>();
        Map<String, Double> composantesTriees = new LinkedHashMap<>();

        Map<String, String> planDeComptes = planDeComptes();

        for(int index=0; index < importanceComposantes.length; index ++) {
            // On complète le numéro de compte par son nom.
            String numeroEtNomCompte = planDeComptes.get(colonnesDesFeatures[index]) != null ? planDeComptes.get(colonnesDesFeatures[index]) : colonnesDesFeatures[index];
            composantes.put(numeroEtNomCompte, importanceComposantes[index]);
        }

        composantes.entrySet().stream()
           .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
           .forEachOrdered(x -> composantesTriees.put(x.getKey(), x.getValue()));

        LOGGER.info("Importance des comptes pour l'équipement {} (erreur du modèle : {}) : ", nomColonneEquipement, 1.0 - precision);
        Iterator<Map.Entry<String, Double>> it = composantesTriees.entrySet().iterator();

        for(int index = 0; it.hasNext() && index < this.NOMBRE_FEATURES_IMPORTANTES; index ++) {
            Map.Entry<String, Double> composante = it.next();
            LOGGER.info("{} : {}", composante.getKey(), composante.getValue());
        }
    }

    /**
     * Renvoyer les comptes comme vecteur de features.
     * @param comptesEquipements Matrice comptes équipements.
     * @return Vecteur Assembler.
     */
    private VectorAssembler getVectorAssemblerForFeaturesComptes(Dataset<Row> comptesEquipements) {
        // Les colonnes de comptes ont des noms commençant par un chiffre.
        List<String> colonnesNumeriques = new ArrayList<>();

        for(String nomColonne : comptesEquipements.columns()) {
            if (Character.isDigit(nomColonne.charAt(0))) {
                colonnesNumeriques.add(nomColonne);
            }
        }

        String[] colonnesComptes = colonnesNumeriques.toArray(new String[]{});

        VectorAssembler assembler = new VectorAssembler();
        assembler.setInputCols(colonnesComptes);
        assembler.setOutputCol("features");

        return assembler;
    }

    /**
     * Préparer la matrice des données à étudier.
     * @param conditionComptes Condition de sélection des comptes.
     * @param soldeParHabitant true si le solde par habitant doit être calculé, false s'il est repris tel quel.
     * @return Matrice commune compte.
     */
    private Dataset<Row> preparationDonnees(Column conditionComptes, boolean soldeParHabitant) {
        Dataset<Row> comptes = matriceCommunesComptes(conditionComptes, soldeParHabitant);
        Dataset<Row> equipements = matriceCommuneEquipements();

        return comptes.join(equipements, comptes.col("codeCommune").equalTo(equipements.col("codeCommune")), "inner")
           .drop(equipements.col("codeCommune"))
           .persist();
    }

    /**
     * Obtenir la matrice communes - comptes.
     * @param condition Condition de sélection.
     * @param soldeParHabitant true si le solde par habitant doit être calculé, false s'il est repris tel quel.
     * @return Matrice commune compte.
     */
    private Dataset<Row> matriceCommunesComptes(Column condition, boolean soldeParHabitant) {
        // Obtenir la matrice des soldes de comptes, montant par habitant.
        // COG : 2021, Exercice
        return this.comptesCommunesDataset.matriceCommuneComptes(null, null, this.ANNEE_BASE_EQUIPEMENT, this.ANNEE_COG, soldeParHabitant, condition)
           .orderBy("codeCommune");
    }

    /**
     * Renvoyer la matrice communes - équipements.
     * @return Matrice commune équipements.
     */
    private Dataset<Row> matriceCommuneEquipements() {
        return this.equipementDataset.matriceCommuneEquipements(null, null, this.ANNEE_BASE_EQUIPEMENT)
           .orderBy("codeCommune");
    }

    /**
     * Associer numéro de compte et libellé.
     * @return Le numéro de compte complété de son libellé.
     */
    private Map<String, String> planDeComptes() {
        List<Row> planComptable =  this.planDeComptesDataset.plansDeComptes()
           .filter(col("nomenclatureComptablePlan").equalTo(lit("M14")))
           .collectAsList();

        Map<String, String> libelles = new LinkedHashMap<>();

        for(Row compte : planComptable) {
            String libelle = MessageFormat.format("{0} - {1}", (String)compte.getAs("numeroComptePlan"), compte.getAs("libelleComptePlan"));
            libelles.put(compte.getAs("numeroComptePlan"), libelle);
        }

        return libelles;
    }
}
