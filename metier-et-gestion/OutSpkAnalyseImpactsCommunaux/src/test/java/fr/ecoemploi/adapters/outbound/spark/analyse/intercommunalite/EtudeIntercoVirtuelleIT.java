package fr.ecoemploi.adapters.outbound.spark.analyse.intercommunalite;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.domain.model.territoire.association.ThemeObjetSocial;
import static fr.ecoemploi.domain.model.territoire.association.ThemeObjetSocial.*;

/**
 * Etude de l'intercommunalité virtuelle de Lisieux Normandie en 2016.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkImpactsTestApplication.class)
class EtudeIntercoVirtuelleIT extends AbstractRestIT {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EtudeIntercoVirtuelleIT.class);
   
   /** Intercommunalité virtuelle de Lisieux-Normandie en 2016 */
   @Autowired
   private IntercoLisieuxVirtuelleDataset lisieuxVirtuelDataset;

   /** CA Lisieux-Normandie. */
   private static final String EPCI_LISIEUX_NORMANDIE = "200069532";

   /** CC Douarnenez-communauté. */
   private static final String EPCI_DOUARNENEZ_COMMUNAUTE = "242900645";
   
   /** Intercommunalité utilisée par défaut pour les tests. */
   private static final String EPCI = EPCI_DOUARNENEZ_COMMUNAUTE;

   /**
    * Options globales des tests.
    */
   @BeforeEach
   public void optionsGlobales() {
      // this.lisieuxVirtuelDataset.loader().setCache(activerCache());
   }

   /** 
    * Vérifier que les cinq intercommunalités sont présentes. 
    */
   @Test
   @DisplayName("Liste des communes de l'intercommunalité en 2019")
   void listeDesCommunesInterco() {
      Dataset<Row> communes = this.lisieuxVirtuelDataset.communes(2019, EPCI);
      assertNotEquals(0L, communes.count(), "Au moins une commune aurait dû être lue, dans l'intercommunalité");

      communes.show(300, false);
   }
   
   /**
    * Vérifier la structure du dataset des formations et diplômes.
    */
   @Test
   @DisplayName("Formation et diplômes de la population en 2016")
   void formation2016() {
      assertDoesNotThrow(() -> {
         this.lisieuxVirtuelDataset.creerCoucheFormationsDiplomes2016(EPCI);
      }, "La création de la couche 'formation et diplômes' a échoué");
   }

   /**
    * Créer la table des déplacements domicile-travail en 2017 dans le Calvados.
    */
   @Test
   @DisplayName("Déplacements domicile-travail en 2017 dans le Calvados, par rapport à Lisieux-Normandie")
   void deplacementsDomicileTravail2017Calvados() {
      assertDoesNotThrow(() -> {
         this.lisieuxVirtuelDataset.creerCoucheMobiliteDomicileTravail("14", EPCI_LISIEUX_NORMANDIE);
      }, "La création de la couche 'déplacements domicile-travail, Calvados' a échoué");
   }
   
   /**
    * Créer la table des déplacements domicile-travail en 2017 dans le Finistère.
    */
   @Test
   @DisplayName("Déplacements domicile-travail en 2017 dans le Finistère, par rapport à Douarnenez-Communauté")
   void deplacementsDomicileTravail2017Finistere() {
      assertDoesNotThrow(() -> {
         this.lisieuxVirtuelDataset.creerCoucheMobiliteDomicileTravail("29", EPCI_DOUARNENEZ_COMMUNAUTE);
      }, "La création de la couche 'déplacements domicile-travail, Finistère' a échoué");
   }

   /**
    * Créer la table des revenus des foyers en 2017.
    */
   @Test
   @DisplayName("Revenus des foyers en 2017")
   @Disabled("Le fichier 2017 des impôts doit être reconstitué avant. Il existait avant, seulement pour un département.")
   void revenus2017() {
      assertDoesNotThrow(() -> {
         this.lisieuxVirtuelDataset.creerCoucheRevenusFoyers();
      }, "La création de la couche 'revenus des foyers' a échoué");
   }
   
   /**
    * Provoquer la création de la couche chômage 2016.
    */
   @Test
   @DisplayName("Créer la couche des données du chômage 2016")
   void chomage2016() {
      assertDoesNotThrow(() -> {
         this.lisieuxVirtuelDataset.creerCoucheChomage2016(EPCI);
      }, "La création de la couche 'formation et diplômes' a échoué");
   }
   
   /**
    * Liste des associations de Lisieux-Normandie.
    */
   @Test
   @DisplayName("associations 2020, intercommunalité Lisieux-Normandie")
   void associations2020() {
      assertDoesNotThrow(() -> {
         associations(RETOUR_A_EMPLOI);
         associations(PROFESSIONNELS_EMPLOI);
         associations(FORMATIONS);
         associations(SOCIAL_EMPLOI);
         associations(SOUTIEN_PUBLICS_PARTICULIERS);
      }, "L'extraction des associations par thématique a échoué");
   }

   /**
    * Liste des associations répondant à une liste de codes sociaux.
    * @param themeObjetSocial Thème des objets sociaux.
    */
   private void associations(ThemeObjetSocial themeObjetSocial) {
      Dataset<Row> associations = this.lisieuxVirtuelDataset.associations(null, null, 2020, 2020, EPCI, themeObjetSocial.getCodesObjetsSociaux());
      affichage(themeObjetSocial.getLibelle(), associations);
   }
   
   /**
    * Afficher les associations.
    * @param libelle Libellé.
    * @param associations Associations.
    */
   @SuppressWarnings("all")
   private void affichage(String libelle, Dataset<Row> associations) {
      associations = associations.orderBy(associations.col("nomCommune"), associations.col("code_objet_social1"));
      
      LOGGER.info("{} associations ont pour thématique : {}.", associations.count(), libelle);
      associations.show(5000, false);
      
      associations.select("nomCommune", "libelle_objet_social1", "libelle_objet_social2", "titre", "objet")
         .foreach((ForeachFunction<Row>)a -> {
             LOGGER.info(a.mkString(","));  
         });
   }
}
