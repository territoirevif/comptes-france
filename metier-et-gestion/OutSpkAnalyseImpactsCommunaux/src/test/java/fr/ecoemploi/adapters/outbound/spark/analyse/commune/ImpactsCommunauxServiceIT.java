package fr.ecoemploi.adapters.outbound.spark.analyse.commune;

import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.analyse.intercommunalite.SparkImpactsTestApplication;

/**
 * Tests sur les impacts communaux.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkImpactsTestApplication.class)
class ImpactsCommunauxServiceIT extends AbstractRestIT {
   /** Service d'analyse d'impacts communaux. */
   @Autowired
   private ImpactsCommunauxDataset impactsCommunauxService;

   /** Année comptable pour les tests. */
   private final static int EXERCICE = 2018;

   /** Année COG pour les tests. */
   private final static int COG = 2019;
   
   /** Année de la base équipement pour les tests. */
   private final static int ANNEE_EQUIPEMENTS = 2018;

   /**
    * Options globales des tests.
    */
   @BeforeEach
   public void optionsGlobales() {
      // this.impactsCommunauxService.loader().setCache(activerCache());
   }

   /**
    * Déterminer les corrélations entre équipements et comptes communaux rattachés aux budgets principaux des M14A.
    */
   @Test
   @DisplayName("Analyse d'impacts d'équipements sur comptes rattachés aux budgets princiapux des M14A")
   void analyseImpactEquipementsSurComptesBudgetsPrinicpauxM14A() {
      assertDoesNotThrow(() -> {
         Column condition = col("nomenclatureComptable").equalTo(lit("M14A")).and(col("typeBudget").equalTo(lit("1"))); // Plans comptables M14A et Budgets principaux.
         this.impactsCommunauxService.analyseImpactEquipementsSurComptes(COG, EXERCICE, ANNEE_EQUIPEMENTS, 0.6, condition);
      }, "L'analyse d'impact sur les comptes M14A a échoué");
   }

   /**
    * Déterminer les corrélations entre équipements et comptes communaux sur les budgets M14.
    */
   @Test
   @DisplayName("Analyse d'impacts d'équipements sur comptes rattachés aux M14")
   void analyseImpactEquipementsSurComptesM14() {
      assertDoesNotThrow(() -> {
         Column condition = col("nomenclatureComptable").equalTo(lit("M14")); // Plans comptables M14.
         this.impactsCommunauxService.analyseImpactEquipementsSurComptes(COG, EXERCICE, ANNEE_EQUIPEMENTS, 0.3, condition);
      }, "L'analyse d'impact sur les comptes M14 a échoué");
   }
}
