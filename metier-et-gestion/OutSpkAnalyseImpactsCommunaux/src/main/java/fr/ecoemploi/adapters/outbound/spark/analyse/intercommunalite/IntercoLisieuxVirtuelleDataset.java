package fr.ecoemploi.adapters.outbound.spark.analyse.intercommunalite;

import static org.apache.spark.sql.functions.*;

import java.io.Serial;
import java.text.*;

import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.association.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.CogDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.emploi.activite.ActivitePopulationParAgeDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.formation.diplome.DiplomesFormationDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.mobilite.FluxActifs15AnsAvecEmploiDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.revenus.*;

/**
 * Former l'intercommunalité virtuelle Lisieux Normandie avec ses communes de 2016
 * Lintercom Lisieux - Pays d'Auge – Normandie SIREN 200033124
 * Communauté de communes du Pays de Livarot SIREN 241400738
 * Communauté de communes du Pays de l'Orbiquet SIREN 241400910
 * Communauté de communes de la Vallée d'Auge SIREN 241400779
 * Communauté de communes des Trois Rivières SIREN 241400795  
 * @author Marc Le Bihan
 */
@Service
public class IntercoLisieuxVirtuelleDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5975485732214696938L;

   /** COG Dataset. */
   @Autowired
   private CogDataset cogDataset;
   
   /** Activité de la population par âge. */
   @Autowired
   private ActivitePopulationParAgeDataset activitePopulationParAgeDataset;
   
   /** Formation et diplômes de la population. */
   @Autowired
   private DiplomesFormationDataset formationDataset;
   
   /** Associations présentes sur le territoire. */
   @Autowired
   private AssociationWaldecDataset associationsDataset;

   /** Revenus et imposition des ménages. */
   @Autowired
   private RevenusImpositionDataset revenusImpositionDataset;

   /** Mobilité domicile-travail. */
   @Autowired
   private FluxActifs15AnsAvecEmploiDataset mobiliteDomicileTravailDataset;

   /**
    * Enumération des associations présentes sur le territoire.
    * @param codesSociaux Filtrage optionnel sur des codes sociaux.
    * @param codeEPCI Code de l'intercommunalité où rechercher les associations.
    * @param anneeRNA Année de recherche des associations
    * @param anneeCog Année du Code officiel géographique
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution.
    * @return Associations actives en 2020.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> associations(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution, int anneeRNA, int anneeCog, String codeEPCI, String[] codesSociaux) {
      Dataset<Row> associations = this.associationsDataset
         .rowAssociationsThematiques(optionsCreationLecture, historiqueExecution, codesSociaux, anneeRNA, anneeCog, true, AssociationWaldecTri.EPCI);

      associations.where(associations.col("codeEPCI").equalTo(codeEPCI));
      
      associations = associations
         .select("nomCommune", "code_objet_social1", "libelle_objet_social1", "code_objet_social2", "libelle_objet_social2", 
            "titre", "objet", "titre_court", "adresse_siege_complement", "adresse_siege_numero_voie", "adresse_siege_type_voie", 
            "adresse_siege_libelle_voie", "adresse_siege_distribution", "adresse_siege_code_postal", "position_activite", "autorisation_publication_web", 
            "numero_waldec", "groupement", "codeCommune"); // Restriction aux champs utiles.
      
      return associations;
   }
   
   /**
    * Créer la couche des données du chômage 2016.
    * @param codeEPCI code de l'intercommunalité à examiner.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public void creerCoucheChomage2016(String codeEPCI) {
      Dataset<Row> write = this.activitePopulationParAgeDataset.populationActivite(this.session, communes(2019, codeEPCI), true, true, 2016)
         .where("typeActivite = 12") // Chômage
         .select(col("codeCommune"), col("nomCommune"), col("sirencommune"), col("populationtotale"), col("nombre"),
            col("effectifs"), col("pctsurpopactive"), col("pctsurpoptotale"));
      
      write.write()
         .format("jdbc")
         .option("url", this.urlJDBC)
         .option("dbtable", "chomage_2016")
         .option("user", this.usernameJDBC)
         .option("password", this.passwordJDBC)
         .option("truncate", "true")
         .mode(SaveMode.Overwrite)
         .save();      
   }

   /**
    * Créer la couche des revenus des foyers en 2017.
    */
   public void creerCoucheRevenusFoyers() {
      Dataset<Row> write = this.revenusImpositionDataset.rowRevenusEtImposition(null, null, 2017, "14", 2017, RevenusImpositionTri.CODE_COMMUNE)
         .select("codeCommune", "nomCommune", "montant_salaire_par_foyer", "traitement_salaires_nombre_foyers")
         .withColumnRenamed("codeCommune", "codecommunerevenu")
         .withColumnRenamed("nomCommune", "nom_commune");

      write.write()
         .format("jdbc")
         .option("url", this.urlJDBC)
         .option("dbtable", "revenus_foyer_2017")
         .option("user", this.usernameJDBC)
         .option("password", this.passwordJDBC)
         .option("truncate", "true")
         .mode(SaveMode.Overwrite)
         .save();
   }

   /**
    * Créer la couche de la mobilité domicile travail des plus de 15 ans ayant un emploi.
    * @param codeEPCI code de l'intercommunalité à examiner.
    * @param codeDepartement Département d'examen.
    */
   public void creerCoucheMobiliteDomicileTravail(String codeDepartement, String codeEPCI) {
      Dataset<Row> communes = communes(2018, codeEPCI);
      Dataset<Row> mobilite = this.mobiliteDomicileTravailDataset.mobiliteDomicileTravail(this.session, communes, codeDepartement);      

      mobilite.write()
         .format("jdbc")
         .option("url", this.urlJDBC)
         .option("dbtable", MessageFormat.format("deplacements_domicile_travail_2017_dept_{0}", codeDepartement))
         .option("user", this.usernameJDBC)
         .option("password", this.passwordJDBC)
         .option("truncate", "true")
         .mode(SaveMode.Overwrite)
         .save();
   }

   /**
    * Créer la couche de données des formations et diplômes en 2016.
    * @param codeEPCI code de l'intercommunalité à examiner.
    */
   public void creerCoucheFormationsDiplomes2016(String codeEPCI) {
      Dataset<Row> write = populationFormation(codeEPCI)
         .select(col("population2a5ans"), col("population6a10ans"), col("population11a14ans"), col("population15a17ans"), col("population18a24ans"), 
            col("population25a29ans"), col("population30ansEtPlus"), col("populationScolarisee2a5ans"), col("populationScolarisee6a10ans"), col("populationScolarisee11a14ans"), 
            col("populationScolarisee15a17ans"), col("populationScolarisee18a24ans"), col("populationScolarisee25a29ans"), col("populationScolarisee30ansEtPlus"), col("populationNonScolarisesTotal"), 
            col("populationNonScolarisesSansDiplomeOuBEPC"), col("populationNonScolarisesCAPouBEP"), col("populationNonScolarisesBAC"), col("populationNonScolarisesSuperieur"), col("hommesNonScolarisesTotal"), 
            col("hommesNonScolarisesSansDiplomeOuBEPC"), col("hommesNonScolarisesCAPouBEP"), col("hommesNonScolarisesBAC"), col("hommesNonScolarisesSuperieur"), col("femmesNonScolariseesTotal"), 
            col("femmesNonScolariseesSansDiplomeOuBEPC"), col("femmesNonScolariseesCAPouBEP"), col("femmesNonScolariseesBAC"), col("femmesNonScolariseesSuperieur"), col("pctScolarisee2a5ans"), 
            col("pctScolarisee6a10ans"), col("pctScolarisee11a14ans"), col("pctScolarisee15a17ans"), col("pctScolarisee18a24ans"), col("pctScolarisee25a29ans"), col("pctScolarisee30ansEtPlus"), 
            col("pctNonScolarisesSansDiplomeOuBEPC"), col("pctNonScolarisesCAPouBEP"), col("pctNonScolarisesBAC"), col("pctNonScolarisesSuperieur"), col("pctHommesNonScolarisesSansDiplomeOuBEPC"), 
            col("pctHommesNonScolarisesCAPouBEP"), col("pctHommesNonScolarisesBAC"), col("pctHommesNonScolarisesSuperieur"), col("pctFemmesNonScolariseesSansDiplomeOuBEPC"), col("pctFemmesNonScolariseesCAPouBEP"), 
            col("pctFemmesNonScolariseesBAC"), col("pctFemmesNonScolariseesSuperieur"), col("codeCommune"), col("nomCommune"), col("sirenCommune"), 
            col("populationTotale"));
      
      write.write()
      .format("jdbc")
      .option("url", this.urlJDBC)
      .option("dbtable", "formation_2016")
      .option("user", this.usernameJDBC)
      .option("password", this.passwordJDBC)
      .option("truncate", "true")
      .mode(SaveMode.Overwrite)
      .save();      
   }
   
   /**
    * Renvoyer un dataset sur les formations et diplômes de la population.
    * @param codeEPCI code de l'intercommunalité à examiner.
    * @return Dataset sur les formations et diplômes.
    */
   public Dataset<Row> populationFormation(String codeEPCI) {
      Dataset<Row> formations = this.formationDataset.rowDiplomesEtFormations(this.session, 2016);
      
      Dataset<Row> communes = communes(2018, codeEPCI);
      
      return formations.join(communes, communes.col("codeCommune").equalTo(formations.col("codeCommune")), "inner")
         .drop(formations.col("codeCommune"))
         .drop(formations.col("codeDepartement"))
         .drop(formations.col("codeRegion"))
         .drop(formations.col("nomCommune"));
   }
   
   /**
    * Renvoyer un Dataset<Row> des communes des intercommunalités réunies.
    * @param anneeCOG Année du COG à prendre en référence.
    * @param codeEPCI Code de l'intercommunaité.
    * @return Communes de ces intercommunalités.
    */
   public Dataset<Row> communes(int anneeCOG, String codeEPCI) {
      if (anneeCOG >= 2017) {
         return this.cogDataset.rowCommunes(null, null, anneeCOG, false).where(col("codeEPCI").equalTo(codeEPCI));
      }
      
      // Nous forcerons sur celles de 2016, avec l'intercommunalité virtuelle.
      return communesAnciennesEPCI();
   }
   
   /**
    * Renvoyer un Dataset<Row> des communes des intercommunalités réunies.
    * @return Communes de ces intercommunalités.
    */
   public Dataset<Row> communesAnciennesEPCI() {
      Dataset<Row> communes = this.cogDataset.rowCommunes(null, null, 2016, true);

      return communes.filter((FilterFunction<Row>)candidat -> {
         String codeEPCI = candidat.getAs("codeEPCI");
         return "200033124".equals(codeEPCI) || "241400738".equals(codeEPCI) || "241400910".equals(codeEPCI) || "241400779".equals(codeEPCI) || "241400795".equals(codeEPCI);
      });
   }
}
