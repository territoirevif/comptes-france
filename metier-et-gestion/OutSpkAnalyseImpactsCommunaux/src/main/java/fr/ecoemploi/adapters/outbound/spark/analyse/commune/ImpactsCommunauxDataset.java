package fr.ecoemploi.adapters.outbound.spark.analyse.commune;

import java.io.Serial;

import org.apache.spark.ml.feature.*;
import org.apache.spark.ml.linalg.*;
import org.apache.spark.ml.stat.*;
import org.apache.spark.sql.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.balance.communes.BalanceComptesCommunesDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.CogDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.equipement.EquipementDataset;

import scala.collection.*;

/**
 * Recherche d'impacts communaux.
 * @author Marc Le Bihan
 */
@Service
public class ImpactsCommunauxDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3905470174324891806L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ImpactsCommunauxDataset.class);

   /** Dataset du code officiel géographique. */
   @Autowired
   private CogDataset cogDataset;
   
   /** Dataset de la base équipement de l'INSEE. */
   @Autowired
   private EquipementDataset equipementDataset;
   
   /** Dataset de la balance des comptes des communes de la DGFIP. */
   @Autowired
   private BalanceComptesCommunesDataset balanceComptesDataset;
   
   /**
    * Analyse de l'impact des équipements sur les comptes.
    * @param anneeCOG Année du COG.
    * @param anneeComptable Année Comptable.
    * @param anneeEquipement Année des équipements.
    * @param seuil Seul en valeur absolue des corrélations à rapporter.
    * @param condition Condition optionnelle.
    */
   @SuppressWarnings("java:S1192") // Avertissement pour duplication de noms de colonnes
   public void analyseImpactEquipementsSurComptes(int anneeCOG, int anneeComptable, int anneeEquipement, double seuil, Column condition) {
      Dataset<Row> matrice = matriceCommunesEtEquipementsComptes(anneeComptable, anneeCOG, anneeEquipement, true, condition);
      Dataset<Row> matriceCorrelation = matriceCorrelation(matrice);
      
      LOGGER.info("Corrélation supérieures ou égales à {} :", seuil);
      analyse(matriceCorrelation, seuil, matrice.drop("codeCommune").drop("populationTotale").columns());
   }
   
   /**
    * Analyse de la matrice de correlation.
    * @param matriceCorrelation Matrice de corrélation.
    * @param seuil Seuil de correlation.
    * @param nomsColonnes Nom des colonnes.
    */
   private void analyse(Dataset<Row> matriceCorrelation, double seuil, String[] nomsColonnes) {
      DenseMatrix matrix = matriceCorrelation.head().getAs(0);
      int ligne = 0;
      
      Iterator<Vector> rowIter = matrix.rowIter();
      
      while(rowIter.hasNext()) {
         Vector row = rowIter.next();
         
         double[] valeurs = row.toArray();
         
         for(int colonne=0; colonne < valeurs.length; colonne ++) {
            double correlation = valeurs[colonne];
            
            // Afficher la corrélation comme intéressante si :
            // - Ce n'est pas la variable corrélée avec elle-même,
            // - Si la corrélation a été résolue (pas de NaN),
            // - Si la corrélation, en valeur absolue, atteint le seuil.
            if (ligne != colonne && !Double.isNaN(correlation) && Math.abs(correlation) >= seuil) {
               LOGGER.info("({}, {}) = {}", nomsColonnes[ligne], nomsColonnes[colonne], correlation);
            }
         }
         
         ligne ++;
      }
   }
   
   /**
    * Créer une matrice de correlation à partir d'un Dataset.
    * @param matrice Matrice source.
    * @return Matrice de corrélation.
    */
   private Dataset<Row> matriceCorrelation(Dataset<Row> matrice) {
      // Créer un vecteur à partir de toutes les valeurs numériques.
      VectorAssembler assembler = new VectorAssembler()
         .setInputCols(matrice.drop("codeCommune").drop("populationTotale").columns())
         .setOutputCol("features");
      
      // Le centrer réduire.
      Dataset<Row> centrageReduction = assembler.transform(matrice);

      StandardScaler scaler = new StandardScaler().setInputCol("features").setOutputCol("scaledFeatures") //NOSONAR
         .setWithStd(true)   // Réduire
         .setWithMean(true); // Centrer
         
      centrageReduction = scaler.fit(centrageReduction).transform(centrageReduction).select("scaledFeatures"); //NOSONAR
      
      // Et renvoyer sa matrice de corrélation.
      return Correlation.corr(centrageReduction, "scaledFeatures");
   }
   
   /**
    * Obtenir une matrice communes X équipements et comptes.
    * @param anneeCOG Année du COG.
    * @param anneeComptable Année Comptable.
    * @param anneeEquipement Année des équipements.
    * @param montantParHabitant true s'il faut renvoyer les montants par habitant
    * <br>false si les montants des soldes doivent être renvoyés tels qu'ils sont.
    * @param condition Condition optionnelle.
    * @return matrice.
    */
   public Dataset<Row> matriceCommunesEtEquipementsComptes(int anneeComptable, int anneeCOG, int anneeEquipement, boolean montantParHabitant, Column condition) {
      Dataset<Row> matriceCommunesEtComptes = this.balanceComptesDataset.matriceCommuneComptes(null, null, anneeComptable, anneeCOG, montantParHabitant, condition);
      Dataset<Row> matriceCommunesEtEquipements = this.equipementDataset.matriceCommuneEquipements(null, null, anneeEquipement);
      Dataset<Row> cog = this.cogDataset.rowCommunes(null, null, anneeCOG, false);
      
      Dataset<Row> matrice = matriceCommunesEtComptes.join(matriceCommunesEtEquipements, 
         matriceCommunesEtComptes.col("codeCommune").equalTo(matriceCommunesEtEquipements.col("codeCommune")), "inner");
      matrice = matrice.drop(matriceCommunesEtEquipements.col("codeCommune"));

      return matrice.join(cog.selectExpr("codeCommune", "populationTotale"),
         matrice.col("codeCommune").equalTo(cog.col("codeCommune")), "inner");
   }
}
