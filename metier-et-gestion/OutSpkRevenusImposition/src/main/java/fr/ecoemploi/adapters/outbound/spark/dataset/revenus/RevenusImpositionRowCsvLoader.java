package fr.ecoemploi.adapters.outbound.spark.dataset.revenus;

import java.io.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;

import static org.apache.spark.sql.types.DataTypes.*;

/**
 * Chargeur de revenus issus des fichiers d'imposition, depuis un fichier CSV open data.
 * @author Marc Le Bihan
 */
@Component
public class RevenusImpositionRowCsvLoader implements Serializable {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 5242842698060113739L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(RevenusImpositionRowCsvLoader.class);

   /** Nom du fichier CSV. */
   @Value("${revenus-imposition.fichier.csv.nom}")
   private String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${revenus-imposition.dir}")
   private String repertoireFichier;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Charger un dataset.
    * @param annee Année pour laquelle rechercher les revenus.
    * @return Ensemble des données attributaires accessibles sur les revenus.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> loadOpenData(int annee) {
      LOGGER.info("Constitution du dataset des revenus de {}...", annee);

      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, annee, this.nomFichier);

      return this.load(schema(), source);
   }

   /**
    * Renvoyer le schéma.
    * @return Schéma.
    */
   @SuppressWarnings("java:S1192") // Avertissement pour duplication de noms de colonnes
   private StructType schema() {
      /* "code_departement","INSEE","nom_commune","tranche_fiscale","nombre_foyers_fiscaux","revenu_fiscal_reference","impot_net_total","nombre_foyers_fiscaux_imposes","revenu_fiscal_reference_foyers_fiscaux_imposes","traitement_salaires_nombre_foyers","traitements_salaires_montant","retraites_pensions_nombre_foyers","retraites_pensions_montant"
       * "14","001","Ablon","Total",661,19141.807,1158.55,320,13515.324,445,13252.084,242,5441.569
       * "14","002","Acqueville","Total",102,2417.753,61.513,38,1400.405,74,1996.018,32,739.846
       * "14","003","Agy","Total",142,4495.379,163.045,85,3262.901,95,3199.345,56,1553.269 */

      return new StructType()
         .add("code_departement", StringType, false)
         .add("INSEE", StringType, false)
         .add("nom_commune", StringType, false)
         .add("tranche_fiscale", StringType, false)
         .add("nombre_foyers_fiscaux", IntegerType, true)
         .add("revenu_fiscal_reference", DoubleType, true)
         .add("impot_net_total", DoubleType, true)
         .add("nombre_foyers_fiscaux_imposes", IntegerType, true)
         .add("revenu_fiscal_reference_foyers_fiscaux_imposes", DoubleType, true)
         .add("traitement_salaires_nombre_foyers", IntegerType, true)
         .add("traitements_salaires_montant", DoubleType, true)
         .add("retraites_pensions_nombre_foyers", IntegerType, true)
         .add("retraites_pensions_montant", DoubleType, true);
   }

   /**
    * Provoquer le chargement effectif de la source de données.
    * @param schema Schéma.
    */
   private Dataset<Row> load(StructType schema, File source) {
      return this.session.read().schema(schema).format("csv")
         .option("header","true")
         .option("quote", "\"")
         .option("escape", "\"")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath());
   }
}
