package fr.ecoemploi.adapters.outbound.spark.dataset.revenus;

import java.io.*;
import java.util.List;
import java.util.function.Supplier;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.*;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

import fr.ecoemploi.adapters.outbound.port.revenus.RevenusRepository;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.CogDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.domain.model.imposition.RevenuImpositionCommune;

/**
 * Revenus et imposition des ménages.
 * @author Marc Le Bihan
 */
@Service
public class RevenusImpositionDataset extends AbstractSparkObjetMetierDataset<RevenuImpositionCommune> implements RevenusRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -6471453800291293257L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(RevenusImpositionDataset.class);

   /** Année revenus début. */
   private final int annneRevenusDebut;

   /** Année revenus fin. */
   private final int annneRevenusFin;

   /** Chargeur de données brutes open data. */
   private final RevenusImpositionRowCsvLoader loader;

   /** Validateur d'enregistrement de revenus d'imposition */
   private final RevenusImpositionRowValidator validator;

   /** Encodeur d'objet métier. */
   private final RevenuImpositionEncoder encoder = new RevenuImpositionEncoder();

   /** Dataset du Code Officiel Géographique. */
   @Autowired
   private CogDataset cogDataset;

   /**
    * Construire un générateur de dataset de revenus d'imposition
    * @param annneRevenusDebut Année revenus début.
    * @param annneRevenusFin Année revenus fin.
    * @param cogDataset Dataset du Code Officiel Géographique.
    * @param loader Chargeur de données brutes open data.
    * @param validator Validateur d'enregistrement de revenus d'imposition
    */
   @Autowired
   public RevenusImpositionDataset(@Value("${annee.revenus.debut}") int annneRevenusDebut, @Value("${annee.revenus.fin}") int annneRevenusFin,
       CogDataset cogDataset, RevenusImpositionRowCsvLoader loader, RevenusImpositionRowValidator validator) {
      this.annneRevenusDebut = annneRevenusDebut;
      this.annneRevenusFin = annneRevenusFin;
      this.cogDataset = cogDataset;
      this.loader = loader;
      this.validator = validator;
   }

   /**
    * Charger les revenus et imposition des ménages.
    * @param anneeImposition Année d'imposition (exemple : 2018 = déclaration d'impôt 2019).
    * @param anneeCOG Année du code officiel géographique.
    */
   @Override
   public void chargerRevenusEtImpositionMenages(int anneeImposition, int anneeCOG) {
      if (anneeImposition != 0) {
         chargement(anneeImposition, anneeCOG);
      }
      else {
         for (int annee = this.annneRevenusDebut; annee <= this.annneRevenusFin; annee ++) {
            int anneeCommunes = anneeCOG;

            if (Math.abs(anneeCOG) < 10) {
               anneeCommunes = annee + anneeCOG;
            }

            chargement(annee, anneeCommunes);
         }
      }
   }

   /**
    * Charger les revenus et imposition des ménages.
    * @param anneeImposition Année d'imposition (exemple : 2018 = déclaration d'impôt 2019).
    * @param anneeCOG Année du code officiel géographique.
    */
   private void chargement(int anneeImposition, int anneeCOG) {
      for(RevenusImpositionTri tri : RevenusImpositionTri.values()) {
         rowRevenusEtImposition(null, null, anneeImposition, anneeCOG, tri);
      }
   }

   /**
    * Obtenir un Dataset Row des revenus et impositions des ménages.
    * @param anneeImposition Année d'imposition (exemple : 2018 = déclaration d'impôt 2019).
    * @param anneeCOG Année du code officiel géographique.
    * @param departement Département.
    * @param tri Tri
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution.
    * @return Dataset des revenus et impositions des ménages.
    */
   public Dataset<Row> rowRevenusEtImposition(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution, int anneeImposition, String departement, int anneeCOG, RevenusImpositionTri tri) {
      return rowRevenusEtImposition(optionsCreationLecture, historiqueExecution, anneeImposition, anneeCOG, tri)
         .where(col("codeDepartement").equalTo(departement));
   }

   /**
    * Obtenir un Dataset Row des revenus et impositions des ménages.
    * @param anneeImposition Année d'imposition (exemple : 2018 = déclaration d'impôt 2019).
    * @param anneeCOG Année du code officiel géographique.
    * @param tri Tri
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution.
    * @return Dataset des revenus et impositions des ménages.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> rowRevenusEtImposition(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution, int anneeImposition, int anneeCOG, RevenusImpositionTri tri) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      Supplier<Dataset<Row>> worker = () -> {
         LOGGER.info("Début de l'acquisition des revenus et impositions des ménages pour l'année d'imposition {}, COG {}...", anneeImposition, anneeCOG);

         Dataset<Row> revenusImposition = this.loader.loadOpenData(anneeImposition);

         // Les pays étrangers sont écartés : leur code département commence par un "B".
         revenusImposition = revenusImposition.filter((FilterFunction<Row>)revenu -> {
            String codeDepartement = revenu.getAs("code_departement");
            return codeDepartement != null && codeDepartement.startsWith("B") == false;
         });

         // Les codes INSEE d'outremer ont un code commune qui recopie, en première position, la dernière lettre de leur département.
         // exemple, pour Mayotte : "976" "616" au lieu de "97616"
         Column correctionCodeINSEEOutremer =
            when(col("code_departement").isin("971", "972", "973", "974", "975", "976"),
               col("INSEE").substr(2,3))
               .otherwise(col("INSEE"));

         // Les codes départements de métropole se terminent par 0 superflu.
         Column correctionCodeDepartementMetropole =
            when(col("code_departement").isin("971", "972", "973", "974", "975", "976"),
               col("code_departement"))
               .otherwise(col("code_departement").substr(1,2));

         revenusImposition = revenusImposition
            .withColumn("INSEE", correctionCodeINSEEOutremer)
            .withColumn("code_departement", correctionCodeDepartementMetropole)
            .withColumn("codeCommuneRevenu", concat(col("code_departement"), col("INSEE")))
            .drop("code_departement", "INSEE");

         // Liaison avec le code officiel géographique.
         Dataset<Row> communes = this.cogDataset.rowCommunes(optionsCreationLecture, historiqueExecution, anneeCOG, false);
         Column joinCommunes = revenusImposition.col("codeCommuneRevenu").equalTo(communes.col("codeCommune"));

         revenusImposition = revenusImposition.join(communes, joinCommunes, "left_outer");

         revenusImposition = revenusImposition
            .filter((FilterFunction<Row>)revenu -> this.validator.validerRevenusImposition(historiqueExecution, revenu, anneeImposition, anneeCOG) != false)
            .drop("codeCommuneRevenu", "nom_commune")

            .withColumn("montant_fiscal_par_foyer", col("revenu_fiscal_reference").multiply(lit(1000)).divide(col("nombre_foyers_fiscaux")).cast(IntegerType))
            .withColumn("montant_foyer_impose", col("revenu_fiscal_reference_foyers_fiscaux_imposes").multiply(lit(1000)).divide(col("nombre_foyers_fiscaux_imposes")).cast(IntegerType))
            .withColumn("montant_salaire_par_foyer", col("traitements_salaires_montant").multiply(lit(1000)).divide(col("traitement_salaires_nombre_foyers")).cast(IntegerType))
            .withColumn("montant_retraite_par_foyer", col("retraites_pensions_montant").multiply(lit(1000)).divide(col("retraites_pensions_nombre_foyers")).cast(IntegerType));


         // Calculer la fréquence d'imposition des ménages.
         revenusImposition = revenusImposition.withColumn("frequence_imposition_menages", col("nombre_foyers_fiscaux_imposes").cast(DoubleType).divide(col("nombre_foyers_fiscaux")));
         return revenusImposition;
      };

      return constitutionStandard(options, historiqueExecution, worker,
         new CacheParqueteur<>(options, this.session, exportCSV(),
            "revenus-impositions", "FILTRE-annee_{0,number,#0}-cog_{1,number,#0}-bruts", tri, anneeImposition, anneeCOG));
   }

   /**
    * Obtenir un Dataset Row des revenus et impositions des ménages de la tranche fiscale totale.<br>
    * Cette fonction ne renvoie pas les lignes de détail.
    * @param anneeImposition Année d'imposition (exemple : 2018 = déclaration d'impôt 2019).
    * @param anneeCOG Année du code officiel géographique.
    * @param tri Tri
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution.
    * @return Dataset des revenus et impositions des ménages.
    */
   public Dataset<Row> rowRevenusEtImpositionTrancheFiscaleTotale(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution, int anneeImposition, int anneeCOG, RevenusImpositionTri tri) {
      Dataset<Row> revenus = rowRevenusEtImposition(optionsCreationLecture, historiqueExecution, anneeImposition, anneeCOG, tri);
      return revenus.filter(col("tranche_fiscale").equalTo("Total"));
   }

   /**
    * Obtenir les revenus et impositions des ménages de la tranche fiscale totale.<br>
    * Cette fonction ne renvoie pas les lignes de détail.
    * @param anneeImposition Année d'imposition (exemple : 2018 = déclaration d'impôt 2019).
    * @param anneeCOG Année du code officiel géographique.
    * @param tri Tri
    * @return Revenus et impositions des ménages.
    */
   public List<RevenuImpositionCommune> revenusEtImpositionTrancheFiscaleTotale(int anneeImposition, int anneeCOG, RevenusImpositionTri tri) {
      return toObjetsMetiersList(rowRevenusEtImpositionTrancheFiscaleTotale(null, null, anneeImposition, anneeCOG, tri));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected RowToObjetMetierInterface<RevenuImpositionCommune> objetMetierEncoder() {
      return this.encoder;
   }
}
