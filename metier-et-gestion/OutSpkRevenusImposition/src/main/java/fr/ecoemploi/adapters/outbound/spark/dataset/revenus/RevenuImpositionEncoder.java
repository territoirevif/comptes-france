package fr.ecoemploi.adapters.outbound.spark.dataset.revenus;

import java.io.*;
import java.util.Objects;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.RowToObjetMetierInterface;
import fr.ecoemploi.domain.model.imposition.RevenuImpositionCommune;

/**
 * Encodeur de revenus et imposition dans une commune
 */
public class RevenuImpositionEncoder implements RowToObjetMetierInterface<RevenuImpositionCommune>, Serializable {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -5603744234740909781L;

   /**
    * {@inheritDoc}
    */
   @Override
   public Dataset<RevenuImpositionCommune> toDatasetObjetsMetiers(Dataset<Row> rows) {
      Objects.requireNonNull(rows, "Le dataset des revenus et imposition ne peut pas valoir null.");
      return rows.map((MapFunction<Row, RevenuImpositionCommune>)this::toObjetMetier, Encoders.bean(RevenuImpositionCommune.class));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public RevenuImpositionCommune toObjetMetier(Row r) {
      return new RevenuImpositionCommune(r.getAs("codeCommune"), r.getAs("nomCommune"), r.getAs("codeDepartement"), r.getAs("nomDepartement"), r.getAs("codeRegion"),
         r.getAs("populationMunicipale"),
         r.getAs("codeEPCI"), r.getAs("nomEPCI"),
         r.getAs("nombre_foyers_fiscaux"),
         r.getAs("nombre_foyers_fiscaux_imposes"),
         r.getAs("frequence_imposition_menages"),
         r.getAs("montant_fiscal_par_foyer"),
         r.getAs("montant_foyer_impose"),
         r.getAs("traitement_salaires_nombre_foyers"),
         r.getAs("montant_salaire_par_foyer"),
         r.getAs("retraites_pensions_nombre_foyers"),
         r.getAs("montant_retraite_par_foyer")
      );
   }
}
