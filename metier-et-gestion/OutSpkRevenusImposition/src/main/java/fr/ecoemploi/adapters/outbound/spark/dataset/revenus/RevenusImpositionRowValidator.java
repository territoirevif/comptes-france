package fr.ecoemploi.adapters.outbound.spark.dataset.revenus;

import java.io.*;
import java.util.*;
import java.util.function.BooleanSupplier;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.*;
import org.springframework.stereotype.Component;
import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.HistoriqueExecution;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.validation.AbstractRowValidator;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Validateur d'enregistrement de dataset de revenus d'imposition
 * @author Marc Le Bihan
 */
@Component
public class RevenusImpositionRowValidator extends AbstractRowValidator implements ApplicationContextAware {
   @Serial
   private static final long serialVersionUID = -777642984825006538L;

   /** Il n'y a ni foyers ni salaires décrits pour cette commune */
   private static final String VLD_REVENUS_AUCUN_FOYER_OU_SALAIRE_COMMUNE = "REVENUS_AUCUN_FOYER_OU_SALAIRE_COMMUNE";

   /** Aucun foyer n'a un salaire dans une commune */
   private static final String VLD_REVENUS_AUCUN_FOYER_SALAIRE_COMMUNE = "REVENUS_AUCUN_FOYER_SALAIRE_COMMUNE";

   /** Une commune est inconnue du COG */
   private static final String VLD_REVENUS_COMMUNE_INCONNUE = "REVENUS_COMMUNE_INCONNUE";

   /**
    * Valider un enregistrement de revenus d'imposition.
    * @param revenu Revenu d'imposition
    * @param anneeImposition Année d'imposition
    * @param anneeCOG Année du COG
    * @param historique Historique d'exécution
    * @return true s'il est valide
    */
   public boolean validerRevenusImposition(HistoriqueExecution historique, Row revenu, int anneeImposition, int anneeCOG) {
      List<BooleanSupplier> validations = new ArrayList<>();

      String codeCommune = CODE_COMMUNE.getAs(revenu);

      // La commune doit être connue du COG de cette année-là.
      validations.add(() -> invalideSi(VLD_REVENUS_COMMUNE_INCONNUE, historique, revenu,
         p -> codeCommune == null,
         () -> new Serializable[] {revenu.getAs("codeCommuneRevenu"), revenu.getAs("nom_commune"), anneeImposition, anneeCOG}));

      // Le nombre de foyers fiscaux et leur montant doivent être alimentés.
      Integer nombreFoyersSalaire = revenu.getAs("traitement_salaires_nombre_foyers");
      Double montantFoyersSalaire = revenu.getAs("traitements_salaires_montant");

      //  Anomalie : il n'y a ni foyers ni salaires décrits pour cette commune
      validations.add(() -> invalideSi(VLD_REVENUS_AUCUN_FOYER_OU_SALAIRE_COMMUNE, historique, revenu,
         p -> nombreFoyersSalaire == null || montantFoyersSalaire == null,
         () -> new Serializable[] {NOM_COMMUNE.getAs(revenu), CODE_COMMUNE.getAs(revenu), revenu.getAs("populationMunicipale"), revenu.getAs("tranche_fiscale")}));

      // Si le nombre de foyers ayant un salaire est égal à zéro, nous devrons aussi l'exclure
      validations.add(() -> invalideSi(VLD_REVENUS_AUCUN_FOYER_SALAIRE_COMMUNE, historique, revenu,
         p -> nombreFoyersSalaire == 0,
         () -> new Serializable[] {NOM_COMMUNE.getAs(revenu), CODE_COMMUNE.getAs(revenu), revenu.getAs("populationMunicipale"), revenu.getAs("tranche_fiscale")}));

      return validations.stream().allMatch(BooleanSupplier::getAsBoolean);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
      declareRegle(VLD_REVENUS_AUCUN_FOYER_OU_SALAIRE_COMMUNE, this.messageSource,
         "Il n'y a ni foyers ni salaires décrits pour une commune", String.class, String.class, Integer.class, String.class);

      declareRegle(VLD_REVENUS_AUCUN_FOYER_SALAIRE_COMMUNE, this.messageSource,
         "Aucun foyer n'a de salaire dans une commune", String.class, String.class, Integer.class, String.class);

      declareRegle(VLD_REVENUS_COMMUNE_INCONNUE, this.messageSource,
         "Une commune est inconnue du COG", String.class, String.class, Integer.class, Integer.class);
   }
}
