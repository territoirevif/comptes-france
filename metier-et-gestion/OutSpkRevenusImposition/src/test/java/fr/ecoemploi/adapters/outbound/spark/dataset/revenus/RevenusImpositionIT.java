package fr.ecoemploi.adapters.outbound.spark.dataset.revenus;

import java.io.*;
import java.util.List;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.domain.model.imposition.RevenuImpositionCommune;

import org.apache.commons.lang3.builder.*;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import org.apache.spark.sql.*;

/**
 * Tests sur les revenus et impôts des ménages.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkRevenusTestApplication.class)
class RevenusImpositionIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -8516900345348543067L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(RevenusImpositionIT.class);

   /** Dataset des revenus et impositions. */
   @Autowired
   private RevenusImpositionDataset revenusImpositionDataset;

   @Value("${test.annee.revenus.debut:2024}")
   private int anneeRevenusDebut;

   @Value("${test.annee.revenus.fin:2024}")
   private int anneeRevenusFin;

   /**
    * Lecture des revenus et impositions des ménages.
    */
   @Test
   @DisplayName("Revenus et imposition des ménages, toutes années connues")
   void revenusEtImposition() {
      for(int annee = this.anneeRevenusDebut; annee <= this.anneeRevenusFin; annee ++) {
         Dataset<Row> revenus = this.revenusImpositionDataset.rowRevenusEtImposition(null, null, annee, annee, RevenusImpositionTri.CODE_COMMUNE);

         long count = revenus.count();
         LOGGER.info("Année : {}, sur {} enregistrements acceptés :", annee, count);

         revenus.show(100, false);
         assertNotEquals(0L, count, "Une information de revenu, au moins, aurait du être lue");
      }
   }

   /**
    * Lecture rows des revenus et impositions des ménages.
    */
   @Test
   @DisplayName("Row revenus et imposition des ménages 2022 (tranche fiscale totale seulement)")
   void rowRevenusEtImpositionTrancheFiscaleTotale() {
      Dataset<Row> revenus = this.revenusImpositionDataset.rowRevenusEtImpositionTrancheFiscaleTotale(null, null, 2022, 2022, RevenusImpositionTri.CODE_COMMUNE);

      long count = revenus.count();
      LOGGER.info("Sur {} communes retenues :", count);

      revenus.show(100, false);
      assertNotEquals(0L, count, "Une information de revenu de tranche fiscale totale, au moins, aurait du être lue, depuis les rows");

      revenus.select("nomCommune", "nomDepartement", "populationMunicipale", "nombre_foyers_fiscaux", "montant_fiscal_par_foyer",
          "nombre_foyers_fiscaux_imposes", "frequence_imposition_menages",
          "montant_foyer_impose", "montant_salaire_par_foyer", "montant_retraite_par_foyer", "traitement_salaires_nombre_foyers", "retraites_pensions_nombre_foyers")
         .show(1000, false);
   }

   /**
    * Lecture des revenus et imposition des ménages
    */
   @Test
   @DisplayName("Revenus et imposition des ménages 2022 (tranche fiscale totale seulement)")
   void revenusEtImpositionTrancheFiscaleTotale() {
      List<RevenuImpositionCommune> revenus = this.revenusImpositionDataset.revenusEtImpositionTrancheFiscaleTotale(2022, 2022, RevenusImpositionTri.CODE_COMMUNE);
      revenus.forEach(revenu ->
         LOGGER.info(ReflectionToStringBuilder.toString(revenu, ToStringStyle.DEFAULT_STYLE)));

      assertNotEquals(0L, revenus.size(), "Une information de revenu de tranche fiscale totale, au moins, aurait du être lue");
   }

   /**
    * Lecture des revenus et impositions des ménages du Finistère.
    */
   @Test
   @DisplayName("Revenus et imposition des ménages 2022 dans le Finistère en export CSV")
   void revenusEtImpositionExportCsv29() {
      Dataset<Row> revenusImposition = this.revenusImpositionDataset.rowRevenusEtImposition(null, null, 2022, "29", 2022, RevenusImpositionTri.CODE_COMMUNE)
         .orderBy("nomCommune");
      
      this.revenusImpositionDataset.exportCSV().exporterCSV(revenusImposition, "imposition", true);
      assertNotEquals(0L, revenusImposition.count(), "Une information de revenu au moins aurait du être lue");
   }

   /**
    * Lecture des revenus et impositions des ménages (Sada 2022).
    */
   @Test
   @DisplayName("Revenus et imposition des ménages de Sada, Mayotte")
   void revenusEtImpositionSada() {
      Dataset<Row> revenusImposition = this.revenusImpositionDataset.rowRevenusEtImposition(null, null, 2022, 2022, RevenusImpositionTri.CODE_COMMUNE);
      revenusImposition = revenusImposition.where(revenusImposition.col("codeCommune").equalTo("97616"));

      revenusImposition.show(false);
      assertNotEquals(0L, revenusImposition.count(), "Une information de revenu au moins aurait du être lue de Sada (97616)");
   }
}
