package fr.ecoemploi.adapters.outbound.spark.dataset.revenus;

import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;


/**
 * Application de test pour Spark
 * @author Marc LE BIHAN
 */
@SpringBootApplication
@ComponentScan(basePackages={"fr", "com"})
public class SparkRevenusTestApplication {
}
