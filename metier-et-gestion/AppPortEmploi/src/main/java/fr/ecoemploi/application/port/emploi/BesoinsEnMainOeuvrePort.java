package fr.ecoemploi.application.port.emploi;

import java.io.Serializable;

/**
 * Cas d'utilisations axés sur les thèmes de l'emploi.
 * @author Marc Le Bihan
 */
public interface BesoinsEnMainOeuvrePort extends Serializable {
   /**
    * Charger le ou les Référentiels des Besoins en Main d'Oeuvre (BMO).
    * @param anneeBMO Année des besoins en main d'oeuvre<br>
    *     0: pour toutes les années disponibles
    */
   void chargerBesoinsEnMainOeuvre(int anneeBMO);
}
