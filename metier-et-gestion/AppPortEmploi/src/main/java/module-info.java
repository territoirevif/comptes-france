/**
 * Application Port de l'emploi et des formations
 */
module fr.ecoemploi.application.port.emploi {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.application.port.emploi;
}
