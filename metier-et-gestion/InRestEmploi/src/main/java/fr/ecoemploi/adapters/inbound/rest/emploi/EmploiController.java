package fr.ecoemploi.adapters.inbound.rest.emploi;

import java.io.Serial;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import fr.ecoemploi.adapters.inbound.port.emploi.BesoinsEnMainOeuvreAPI;
import fr.ecoemploi.adapters.inbound.rest.core.AbstractRestController;
import fr.ecoemploi.application.port.emploi.BesoinsEnMainOeuvrePort;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.*;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * Contrôleur REST pour la gestion de l'emploi.
 * @author Marc LE BIHAN
 */
@RestController
@RequestMapping(value = "/emploi", name="emploi")
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:4200", "http://localhost:9091"})
@Tag(name = "Emploi", description = "Examens et services autour de l'emploi")
public class EmploiController extends AbstractRestController implements BesoinsEnMainOeuvreAPI {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 4912060220075104249L;

   /** Dataset des équipements. */
   @Autowired
   private BesoinsEnMainOeuvrePort besoinsEnMainOeuvrePort;

   /**
    * Charger la base emploi (Besoins en Main d'Oeuvre) et la mettre en cache.
    * @param anneeBMO Année de la base emploi à charger.<br>
    * 0 si toutes les années connues doivent l'être (à partir de 2020).
    */
   @Operation(description = "Charger la  dans les fichiers de cache")
   @GetMapping(value = "/chargerBesoinsEnMainOeuvre")
   @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Bases équipements chargées en cache."),
      @ApiResponse(responseCode = "403", description = "Si la génération de données par l'application est interdite."),
      @ApiResponse(responseCode = "500", description = "Un incident durant le chargement.")
   })
   @Override
   public void chargerBesoinsEnMainOeuvre(
      @Parameter(name = "anneeBMO", description = "Année des besoins en main d'oeuvre : 0 pour toutes les années connues.", examples = {
         @ExampleObject(value = "2024", name="précise", summary = "une année précise", description = "une année précise"),
         @ExampleObject(value = "0", name="relative", summary = "Toutes les années disponibles", description = "Toutes les années des besoins en main d'oeuvre disponibles")
   }) int anneeBMO) {
      refusSiGenerationInterdite();
      this.besoinsEnMainOeuvrePort.chargerBesoinsEnMainOeuvre(anneeBMO);
   }
}
