/**
 * Inbound Rest de l'emploi et de la formation
 */
module fr.ecoemploi.inbound.rest.emploi {
   // requires jakarta.ws.rs;
   requires java.ws.rs;
   requires spring.beans;
   requires spring.web;
   requires spring.context;
   requires io.swagger.v3.oas.annotations;

   requires fr.ecoemploi.domain.model;
   requires fr.ecoemploi.inbound.rest.core;

   requires fr.ecoemploi.application.port.emploi;
   requires fr.ecoemploi.inbound.port.emploi;

   exports fr.ecoemploi.adapters.inbound.rest.emploi;
}
