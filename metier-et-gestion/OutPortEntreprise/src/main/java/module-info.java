/**
 * Outbound Port des entreprises et des établissements
 */
module fr.ecoemploi.outbound.port.entreprise {
   requires fr.ecoemploi.domain.model;
   requires spring.context;

   exports fr.ecoemploi.adapters.outbound.port.entreprise;
}
