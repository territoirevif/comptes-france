package fr.ecoemploi.adapters.outbound.port.equipement;

import java.io.Serializable;

/**
 * Interface d'accès aux données d'équipement.
 * @author Marc Le Bihan
 */
public interface EquipementRepository extends Serializable {
   /**
    * Charger la base équipements.
    * @param anneeEquipement Année de la base équipement à charger.<br>
    * 0 si toutes les années connues doivent l'être (à partir de 2018 pour la base équipement).
    */
   void chargerEquipements(int anneeEquipement);
}
