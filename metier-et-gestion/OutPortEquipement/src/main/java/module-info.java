/**
 * Outbound Port de la base équipement
 */
module fr.ecoemploi.outbound.port.equipement {
   requires fr.ecoemploi.domain.model;
   requires spring.context;

   exports fr.ecoemploi.adapters.outbound.port.equipement;
}
