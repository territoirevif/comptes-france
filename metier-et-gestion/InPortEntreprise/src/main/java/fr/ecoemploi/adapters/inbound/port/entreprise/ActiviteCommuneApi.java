package fr.ecoemploi.adapters.inbound.port.entreprise;

import java.io.Serializable;
import java.util.List;

import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.entreprise.ActiviteCommune;

/**
 * Actions possibles sur les activités de communes
 * @author Marc Le Bihan
 */
public interface ActiviteCommuneApi extends Serializable {
   /**
    * Obtenir les activités d'une commune.
    * @param anneeCOG Année du COG.
    * @param anneeSIRENE Année SIRENE des entreprises à consulter.
    * @param codeCommune Code commune.
    * @return Activités de la commune.
    */
   List<ActiviteCommune> obtenirActivitesCommune(int anneeCOG, int anneeSIRENE, CodeCommune codeCommune);

   /**
    * Exporter les comptes de résultats et activités par niveau NAF, pour une série d'intercommunalités, dans un fichier CSV.
    * @param anneeCOG Année du COG.
    * @param anneeSIRENE Année des données SIRENE à prendre en considération, pour l'extraction des données entreprise/établissement.
    * @param codeEPCI Code de l'intercommunalité dont on veut les activités.
    * @param codeCommune Code de la commune dont on veut les activités.
    */
   void exporterActivitesCommuneCSV(int anneeCOG, int anneeSIRENE,String codeEPCI, String codeCommune);
}
