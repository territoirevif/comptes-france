/**
 * Inbound Port des entreprises et établissements
 */
module fr.ecoemploi.inbound.port.entreprise {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.adapters.inbound.port.entreprise;
}
