package fr.ecoemploi.application.service.revenus;

import java.io.Serial;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

import fr.ecoemploi.adapters.outbound.port.revenus.RevenusRepository;
import fr.ecoemploi.application.port.revenus.RevenusImpositionPort;

/**
 * Service des revenus et imposition des ménages.
 * @author Marc LE BIHAN
 */
@Service
public class RevenusImpositionService implements RevenusImpositionPort {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -1701730393833027893L;

   /** Repository des entreprises présentes sur le territoire. */
   @Autowired
   private RevenusRepository revenusRepository;

   /**
    * Charger le référentiel des revenus et imposition des ménages.
    * @param anneeImposition Année d'imposition (exemple : 2018 = déclaration d'impôt 2019).
    * @param anneeCOG Année du code officiel géographique.
    */
   @Override
   public void chargerRevenusEtImpositionMenages(int anneeImposition, int anneeCOG) {
      this.revenusRepository.chargerRevenusEtImpositionMenages(anneeImposition, anneeCOG);
   }
}
