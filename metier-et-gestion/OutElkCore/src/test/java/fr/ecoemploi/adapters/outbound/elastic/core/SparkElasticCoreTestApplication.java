package fr.ecoemploi.adapters.outbound.elastic.core;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * Application de test pour Spark
 * @author Marc LE BIHAN
 */
@SpringBootApplication
@ComponentScan(basePackages={"fr.ecoemploi"})
public class SparkElasticCoreTestApplication {
}
