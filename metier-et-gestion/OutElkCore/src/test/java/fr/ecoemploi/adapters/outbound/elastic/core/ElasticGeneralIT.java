package fr.ecoemploi.adapters.outbound.elastic.core;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.indices.*;

/**
 * Tests généraux d'elasticsearch
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkElasticCoreTestApplication.class)
class ElasticGeneralIT {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ElasticGeneralIT.class);

   /** Nom de l'index de test. */
   private static final String INDEX_NAME = "test-index";

   /** Nom de l'index de test. */
   private static final String ALIAS_NAME = "alias-test";

   /** Client Elastic. */
   @Autowired
   private ElasticsearchClient elasticsearchClient;

   /**
    * Test de création d'index.
    */
   @Test @DisplayName("Création d'index Elastic")
   void createTestIndex() {
      deleteIndex();

      assertDoesNotThrow(() -> {
         CreateIndexResponse indexRequest = this.elasticsearchClient.indices().create(indexBuilder ->
            indexBuilder.index(INDEX_NAME)
               .aliases(ALIAS_NAME, aliasBuilder -> aliasBuilder.isWriteIndex(true))
         );

         // Check whether the operation that is specified in the IndexRequest request is confirmed by the Elasticsearch cluster.
         assertTrue(indexRequest.acknowledged(), "La création d'index elastic a échoué");
      }, "La création d'index elastic a rencontré un incident");

      deleteIndex();
   }

   /**
    * Supprimer l'index de test.
    */
   private void deleteIndex() {
      try {
         this.elasticsearchClient.indices().delete(indexBuilder -> indexBuilder.index(INDEX_NAME));
      }
      catch(Exception ex)
      {
         LOGGER.trace(ex.getMessage());
      }
   }
}
