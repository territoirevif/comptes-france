package fr.ecoemploi.adapters.outbound.elastic.core;

import org.apache.http.HttpHost;
import org.apache.http.auth.*;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.slf4j.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

import org.elasticsearch.client.*;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;

/**
 * Configuration d'Elasticsearch
 * @author Marc LE BIHAN
 */
@Configuration
public class ElasticConfiguration implements AutoCloseable {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ElasticConfiguration.class);

   /** Utilisateur de connexion. */
   @Value("${elasticsearch.username}")
   private String userName;

   /** Mot de passe de connexion. */
   @Value("${elasticsearch.password}")
   private String password;

   /** URL du serveur elasticsearch. */
   @Value("${elasticsearch.url}")
   private String url;

   /** Port du serveur elasticsearch. */
   @Value("${elasticsearch.port}")
   private int port;

   /** Scheme du serveur elastic. */
   @Value("${elasticsearch.scheme}")
   private String scheme;

   /** Transport. */
   private ElasticsearchTransport transport;

   /** Client REST. */
   private RestClient restClient;

   /**
    * Obtenir un client Elasticsearch.
    * @return Client Elasticsearch
    */
   @Bean
   public ElasticsearchClient getElasticsearchClient() {
      // Use basic authentication for the Elasticsearch cluster.
      CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
      credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(this.userName, this.password));

      this.restClient = RestClient.builder(new HttpHost(this.url, this.port, this.scheme))
         .setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider)).build();

      this.transport = new RestClientTransport(this.restClient, new JacksonJsonpMapper());
      LOGGER.info("Liaison avec elasticsearch sur {}://{}:{}, user: {}", this.scheme, this.url, this.port, this.userName);

      LOGGER.info("Configuration d'Elasticsearch (Bean prêt).");
      return new ElasticsearchClient(this.transport);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void close() throws Exception {
      LOGGER.info("Fermeture des connexions vers Elasticsearch");

      if (this.transport != null) {
         this.transport.close();
      }

      if (this.restClient != null) {
         this.restClient.close();
      }
   }
}
