package fr.ecoemploi.adapters.outbound.elastic.core;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.TechniqueException;

/**
 * Exception levée lorsqu'un index elastic n'a pas pu être créé.
 * @author Marc Le Bihan
 */
public class IndexElasticNonCreeException extends TechniqueException {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 620061796088199442L;

   /**
    * Construire une exception
    * @param message Message de l'exception.
    */
   public IndexElasticNonCreeException(String message) {
      super(message);
   }

   /**
    * Construire une exception
    * @param message Message de l'exception.
    * @param cause Cause racine de l'incident.
    */
   public IndexElasticNonCreeException(String message, Throwable cause) {
      super(message, cause);
   }
}
