package fr.ecoemploi.adapters.outbound.elastic.core;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.TechniqueException;

/**
 * Exception levée lorsqu'une recherche dans elastic a échoué.
 * @author Marc Le Bihan
 */
public class RechercheElasticEchoueeException extends TechniqueException {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 6048891601420317977L;

   /**
    * Construire une exception
    * @param message Message de l'exception.
    */
   public RechercheElasticEchoueeException(String message) {
      super(message);
   }

   /**
    * Construire une exception
    * @param message Message de l'exception.
    * @param cause Cause racine de l'incident.
    */
   public RechercheElasticEchoueeException(String message, Throwable cause) {
      super(message, cause);
   }
}
