package fr.ecoemploi.adapters.outbound.elastic.core;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.TechniqueException;

/**
 * Exception levée lorsqu'une insertion dans un index elastic a échoué.
 * @author Marc Le Bihan
 */
public class InsertionElasticEchoueeException extends TechniqueException {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 6048891601420317976L;

   /**
    * Construire une exception
    * @param message Message de l'exception.
    */
   public InsertionElasticEchoueeException(String message) {
      super(message);
   }

   /**
    * Construire une exception
    * @param message Message de l'exception.
    * @param cause Cause racine de l'incident.
    */
   public InsertionElasticEchoueeException(String message, Throwable cause) {
      super(message, cause);
   }
}
