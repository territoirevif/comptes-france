package fr.ecoemploi.adapters.outbound.elastic.core;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.*;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.ElasticsearchException;
import co.elastic.clients.elasticsearch._types.mapping.*;
import co.elastic.clients.elasticsearch._types.query_dsl.*;
import co.elastic.clients.elasticsearch.core.*;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import co.elastic.clients.elasticsearch.core.search.*;
import co.elastic.clients.elasticsearch.indices.*;

/**
 * Classe de base des composants Elastic
 * @author Marc Le Bihan
 */
public abstract class AbstractElastic {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AbstractElastic.class);

   /** Client Elastic. */
   @Autowired
   protected ElasticsearchClient elasticsearchClient;

   /** Nom de l'index associé. */
   private final String indexName;

   /** Nom de l'alias de l'index. */
   private final String aliasName;

   /** Nombre limite de documents à extraire lors d'une recherche par Elastic. Ne peut dépasser 10 000 */
   @Value("${elasticsearch.limite_recherche}")
   private int limiteRecherche;

   /**
    * Pour la désérialisation
    */
   protected AbstractElastic() {
      this.indexName = "";
      this.aliasName = "";
   }

   /**
    * Construire un composant Elastic.
    * @param indexName Nom de l'index.
    * @param aliasName Nom de l'alias, optionnel.
    */
   protected AbstractElastic(String indexName, String aliasName) {
      this.indexName = indexName;
      this.aliasName = aliasName;
   }

   /**
    * Créer l'index.
    * @param mappings Mappings. Peut valoir null, et alors il ne sont pas précisés.
    * @throws IndexElasticNonCreeException si l'index n'a pas pu être créé, suite à un problème technique.
    */
   public void createIndex(TypeMapping mappings) throws IndexElasticNonCreeException {
      try {
         // Créer l'index désiré, avec un alias si demandé.
         CreateIndexResponse createIndexResponse;
         CreateIndexRequest.Builder indexBuilder = new CreateIndexRequest.Builder().index(this.indexName);

         if (this.aliasName != null) {
            indexBuilder.aliases(this.aliasName, aliasBuilder -> aliasBuilder.isWriteIndex(true));
         }

         if (mappings != null) {
            indexBuilder.mappings(mappings);
         }

         // Demander la création de l'index
         CreateIndexRequest createIndexRequest = indexBuilder.build();
         createIndexResponse = this.elasticsearchClient.indices().create(createIndexRequest);

         if (createIndexResponse.acknowledged() == false) {
            LOGGER.warn("La création de l'index Elastic {} (alias {}) n'a pas été acceptée.", this.indexName, this.aliasName);
         }

         LOGGER.info("Un index Elastic {} (alias {}) a été créé.", this.indexName, this.aliasName);
      }
      catch(ElasticsearchException e) {
         String format = "La création de l''index Elastic {0} (alias {1}) sur l''endpoint d''id {2} a échoué sur un statut {3} : {4}. Cause de l''erreur : {5}, Réponse de l''erreur: {6}.";
         String message = MessageFormat.format(format, this.indexName, this.aliasName, e.endpointId(), e.status(), e.getMessage(), e.error(), e.response());

         IndexElasticNonCreeException ex = new IndexElasticNonCreeException(message, e);
         LOGGER.error(ex.getMessage(), ex);
         throw ex;
      }
      catch(IOException e) {
         String format = "La création de l''index Elastic {0} (alias {1}) a échoué : {2}.";
         String message = MessageFormat.format(format, this.indexName, this.aliasName, e.getMessage());

         IndexElasticNonCreeException ex = new IndexElasticNonCreeException(message, e);
         LOGGER.error(ex.getMessage(), ex);
         throw ex;
      }
   }

   /**
    * Obtenir l'identifiant d'insertion d'un objet Elastic
    * @param <T> Classe de l'objet à insérer.
    */
   public interface GetElasticObjectIdMethod<T> {
      /**
       * Retourner l'identifiant de l'objet
       * @param objet Objet
       * @return Identifiant
       */
      String getId(T objet);
   }

   /**
    * Créer un index sur la base d'un objet représentant
    * @param objetRepresentant Objet dont les champs vont être convertis en mappings
    * @param <T> Classe de l'objet représentant.
    * @param getId Méthode pour obtenir leur id.
    * @param champsEnFrancais Champs à mettre en français ensuite par un analyzer.
    * @throws IndexElasticNonCreeException si l'index n'a pas pu être créé, suite à un problème technique.
    */
   public <T> void createIndex(T objetRepresentant, GetElasticObjectIdMethod<T> getId, Set<String> champsEnFrancais) throws IndexElasticNonCreeException {
      Objects.requireNonNull(objetRepresentant, "L'objet représentant de l'index à créer Elastic ne peut pas valoir null.");
      Objects.requireNonNull(getId, "La méthode d'extraction d'id d'objet représentant de l'index à créer dans Elastic ne peut pas valoir null.");
      Objects.requireNonNull(champsEnFrancais, "La liste des champs à déclarer en français dans l'index Elastic peut être vide mais ne doit pas valoir null.");

      // Créer l'index initial, sans mappings associé
      createIndex(null);

      // Il faut qu'une entrée ait été placée pour que le mappings soit constitué.
      List<T> representants = List.of(objetRepresentant);
      insert(representants, objetRepresentant.getClass(), getId);

      // Relire puis modifier l'index, en ajoutant "analyzer": "french" à certains champs texte.
      TypeMapping mappings = getIndex().mappings();

      if (mappings == null) {
         deleteIndex();

         String format = "Pas de mappings obtenus de l''index {0} lors de sa création.";
         String message = MessageFormat.format(format, getIndexName());

         IndexElasticNonCreeException ex = new IndexElasticNonCreeException(message);
         LOGGER.error(message, ex);
         throw ex;
      }

      Map<String, Property> properties = mappings.properties();
      TypeMapping.Builder typeMappingBuilder = new TypeMapping.Builder();

      for(Map.Entry<String, Property> champ : properties.entrySet()) {
         String nomChamp = champ.getKey();
         Property property = champ.getValue();

         typeMappingBuilder.properties(nomChamp,
            champsEnFrancais.contains(nomChamp) ? addAnalyzer(property) : property);
      }

      TypeMapping nouveauxMappings = typeMappingBuilder.build();

      // Recréer l'index modifié.
      deleteIndex();
      createIndex(nouveauxMappings);
   }

   /**
    * Supprimer l'index
    */
   public void deleteIndex() {
      try {
         DeleteIndexResponse deleteIndexResponse = this.elasticsearchClient.indices().delete(indexBuilder -> indexBuilder.index(this.indexName));

         if (LOGGER.isDebugEnabled()) {
            if (deleteIndexResponse.acknowledged() == false) {
               LOGGER.debug("La suppression de l'index Elastic {} (alias {}) n'a pas été acceptée. Peut-être l'index n'existe t-il pas.", this.indexName, this.aliasName);
            }
            else {
               LOGGER.info("Un index Elastic {} (alias {}) a été supprimé.", this.indexName, this.aliasName);
            }
         }
      }
      catch(ElasticsearchException ex) {
         LOGGER.warn("La suppression de l'index Elastic {} (alias {}) sur l'endpoint d'id {} a échoué sur un statut {} : {}. Cause de l'erreur : {}, Réponse de l'erreur: {}.",
            this.indexName, this.aliasName, ex.endpointId(), ex.status(), ex.getMessage(), ex.error(), ex.response());
      }
      catch(IOException ex) {
         LOGGER.warn("La suppression de l'index Elastic {} (alias {}) a échoué : {}.", this.indexName, this.aliasName, ex.getMessage());
      }
   }

   /**
    * Obtenir la description de l'index.
    * @return Description de l'index.
    */
   public IndexState getIndex() {
      try {
         GetIndexRequest.Builder getIndexBuilder = new GetIndexRequest.Builder().index(this.indexName);
         GetIndexResponse getIndexResponse = this.elasticsearchClient.indices().get(getIndexBuilder.build());

         // La requête renvoie plusieurs indexs. Un seul nous intéresse.
         Map<String, IndexState> indexs = getIndexResponse.result();

         IndexState indexState = indexs.get(this.indexName);

         if (indexState == null) {
            String format = "L''index Elastic {0} (alias {1}) n''a pas été trouvé.";
            String message = MessageFormat.format(format, this.indexName, this.aliasName);
            LOGGER.warn(message);

            throw new IndexElasticInexistantException(message);
         }

         return indexState;
      }
      catch(ElasticsearchException e) {
         String format = "La lecture de l''index Elastic {0} (alias {1}) sur l''endpoint d''id {2} a échoué sur un statut {3} : {4}. Cause de l''erreur : {5}, Réponse de l''erreur: {6}.";
         String message = MessageFormat.format(format, this.indexName, this.aliasName, e.endpointId(), e.status(), e.getMessage(), e.error(), e.response());
         LOGGER.warn(message);

         throw new IndexElasticInexistantException(message, e);
      }
      catch(IOException e) {
         String format = "La lecture de l''index Elastic {0} (alias {1}) a échoué : {2}.";
         String message = MessageFormat.format(format, this.indexName, this.aliasName, e.getMessage());
         LOGGER.warn(message);

         throw new IndexElasticInexistantException(message, e);
      }
   }

   /**
    * Renvoyer le nom de l'index Elastic utilisé pour ce composant.
    * @return Nom de l'index Elastic
    */
   public String getIndexName() {
      return this.indexName;
   }

   /**
    * Rechercher dans Elastic
    * @param searchText Texte à rechercher
    * @param field Champ (optionnel)
    * @param classeObjetRetour Classe de l'objet de retour.
    * @return Liste de résultats de recherche
    * @param <T> Classe de l'objet de retour
    * @throws RechercheElasticEchoueeException si un incident de recherche survient
    */
   public <T> List<T> search(String searchText, String field, Class<T> classeObjetRetour) throws RechercheElasticEchoueeException {
      return searchHits(searchText, field, classeObjetRetour).stream().map(Hit::source).toList();
   }

   /**
    * Rechercher dans Elastic
    * @param searchText Texte à rechercher (au format Simple Query d'Elastic)
    * @param field Champ (optionnel)
    * @param classeObjetRetour Classe de l'objet de retour.
    * @return List de Hits correspondant à chaque résultat de recherche
    * @param <T> Classe de l'objet de retour
    * @throws RechercheElasticEchoueeException si un incident de recherche survient
    */
   public <T> List<Hit<T>> searchHits(String searchText, String field, Class<T> classeObjetRetour) throws RechercheElasticEchoueeException {
      Objects.requireNonNull(searchText, "Le texte à rechercher dans Elastic ne peut pas valoir null");
      Objects.requireNonNull(classeObjetRetour, "La classe de l'objet de retour de résultat Elastic ne peut pas valoir null");

      Query query = simpleQuery(searchText, field);
      SearchResponse<T> response;

      try {
         response = this.elasticsearchClient.search(s -> s.index(getIndexName()).query(query).size(this.limiteRecherche), classeObjetRetour);
      }
      catch(IOException e) {
         String format = "La recherche dans l''index Elastic {0} avec le query {1} a échoué : {2}";
         String message = MessageFormat.format(format, this.indexName, query.toString(), e.getMessage());

         RechercheElasticEchoueeException ex = new RechercheElasticEchoueeException(message, e);
         LOGGER.error(message, ex);
         throw ex;
      }

      // Avertir si la requête a davantage de résultats à fournir qu'elle ne nous ne propose.
      TotalHits total = response.hits().total();

      if (total != null && total.relation() != TotalHitsRelation.Eq) {
         LOGGER.warn("La requête elastic retourne {} réponses, mais elle en aurait davantage.", total.value());
      }

      return response.hits().hits();
   }

   /**
    * Préparer le paramètre query.
    * @param searchText Texte de recherche
    * @param field Champ optionnel de recherche.
    * @return Query
    */
   private Query simpleQuery(String searchText, String field) {
      SimpleQueryStringQuery.Builder simpleQueryStringQuery = new SimpleQueryStringQuery.Builder()
         .query(searchText);

      if (StringUtils.isBlank(field) == false) {
         simpleQueryStringQuery.fields(field);
      }

      Query.Builder queryBuilder = new Query.Builder();
      queryBuilder.simpleQueryString(simpleQueryStringQuery.build());
      return queryBuilder.build();
   }

   /**
    * Insérer dans l'index Elastic un groupe d'objets, en bulk.
    * @param objets Stream d'objets.
    * @param <T> Classe des objets insérés.
    * @param classeObjet Classe des objets.
    * @param getId Méthode pour obtenir leur id.
    * @param tailleBlocs taille des blocs insérés en bulk
    */
   public <T> void insert(Stream<T> objets, Class<?> classeObjet, GetElasticObjectIdMethod<T> getId, int tailleBlocs) {
      Objects.requireNonNull(objets, "L'objet à insérer dans Elastic ne peut pas valoir null.");
      Objects.requireNonNull(classeObjet, "La classe de l'objet à insérer dans Elastic ne peut pas valoir null.");
      Objects.requireNonNull(getId, "La méthode d'extraction d'id d'objet à insérer dans Elastic ne peut pas valoir null.");

      final AtomicInteger count = new AtomicInteger();
      final List<T> objetsBlocBulk = new ArrayList<>();

      objets.forEach(objet -> {
         if (count.intValue() >= tailleBlocs) {
            insert(objetsBlocBulk, classeObjet, getId);

            count.set(0);
            objetsBlocBulk.clear();
         }

         objetsBlocBulk.add(objet);
         count.incrementAndGet();
      });

      if (count.intValue() > 0) {
         insert(objetsBlocBulk, classeObjet, getId);
      }
   }

   /**
    * Insérer dans l'index Elastic un groupe d'objets, en bulk.
    * @param objets Liste d'objets.
    * @param <T> Classe des objets à insérer.
    * @param classeObjet Classe des objets.
    * @param getId Méthode pour obtenir leur id.
    */
   public <T> void insert(List<T> objets, Class<?> classeObjet, GetElasticObjectIdMethod<T> getId) {
      Objects.requireNonNull(objets, "L'objet à insérer dans Elastic ne peut pas valoir null.");
      Objects.requireNonNull(classeObjet, "La classe de l'objet à insérer dans Elastic ne peut pas valoir null.");
      Objects.requireNonNull(getId, "La méthode d'extraction d'id d'objet à insérer dans Elastic ne peut pas valoir null.");

      if (objets.isEmpty()) {
         return;
      }

      BulkRequest.Builder bulk = new BulkRequest.Builder();

      for(T objet : objets) {
         bulk.operations(op -> op
            .index(idx -> idx.index(getIndexName())
               .id(getId.getId(objet))
               .document(objet)
            )
         );
      }

      try {
         BulkResponse result = this.elasticsearchClient.bulk(bulk.build());

         if (result.errors()) {
            LOGGER.error("Bulk had errors");

            for (BulkResponseItem item: result.items()) {
               if (item.error() != null) {
                  LOGGER.error(item.error().reason());
               }
            }
         }
      }
      catch(ElasticsearchException e) {
         String format = "L''insertion bulk d''objets de classe {0} dans l''index {1} sur l''endpoint d''id {2} a échoué sur un statut {3} : {4}. Cause de l''erreur : {5}, Réponse de l''erreur: {6}.";
         String message = MessageFormat.format(format, classeObjet.getSimpleName(), getIndexName(), e.endpointId(), e.status(), e.getMessage(), e.error(), e.response());

         fr.ecoemploi.adapters.outbound.elastic.core.IndexElasticNonCreeException ex = new fr.ecoemploi.adapters.outbound.elastic.core.IndexElasticNonCreeException(message, e);
         LOGGER.error(ex.getMessage(), ex);
         throw ex;

      }
      catch(IOException e) {
         String format = "L''insertion bulk d''objets de classe {0} dans l''index {1} a échoué : {2}.";
         String message = MessageFormat.format(format, classeObjet.getSimpleName(), getIndexName(), e.getMessage());

         fr.ecoemploi.adapters.outbound.elastic.core.IndexElasticNonCreeException ex = new IndexElasticNonCreeException(message, e);
         LOGGER.error(ex.getMessage(), ex);
         throw ex;
      }
   }

   /**
    * Ajouter un "analyzer": "french" au mapping d'un objet texte.
    * @return Propriété modifiée, qui reçoit le mapping.
    */
   private Property addAnalyzer(Property candidate) {
      Property.Builder propertyBuilder = new Property.Builder();

      TextProperty.Builder textPropertyBuilder = new TextProperty.Builder().analyzer("french");
      textPropertyBuilder.fields(candidate.text().fields());

      TextProperty textProperty = textPropertyBuilder.build();
      propertyBuilder.text(textProperty);

      return propertyBuilder.build();
   }
}
