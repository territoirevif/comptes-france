package fr.ecoemploi.adapters.outbound.elastic.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.*;
import org.springframework.context.annotation.*;

/**
 * Configuration de l'API Jackson
 * @author Marc Le Bihan
 */
@Configuration
public class JacksonConfiguration {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(JacksonConfiguration.class);

    /**
     * Création d'un object mapper supportant le time module.
     * @return Object Mapper.
     */
    @Bean
    public ObjectMapper jacksonObjectMapper() {
       LOGGER.info("Configuration Jackson : time module (Bean prêt).");
       return new ObjectMapper().registerModule(new JavaTimeModule());
    }
}
