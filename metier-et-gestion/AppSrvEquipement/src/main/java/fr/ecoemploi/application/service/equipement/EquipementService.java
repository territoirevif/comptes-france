package fr.ecoemploi.application.service.equipement;

import java.io.Serial;

import fr.ecoemploi.adapters.outbound.port.equipement.EquipementRepository;
import fr.ecoemploi.application.port.equipement.EquipementPort;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

/**
 * Service de gestion des équipements.
 * @author Marc Le Bihan
 */
@Service
@Transactional
public class EquipementService implements EquipementPort {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -1556569498255157482L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EquipementService.class);

   /** Année équipement début. */
   @Value("${annee.equipement.debut}")
   private int annneEquipementDebut;

   /** Année équipement début. */
   @Value("${annee.equipement.fin}")
   private int annneEquipementFin;

   /** Gestionnaire d'équipements. */
   @Autowired
   private EquipementRepository equipementRepository;

   @Override
   public void chargerEquipements(int anneeEquipement) {
      int anneeDebut = anneeEquipement != 0 ? anneeEquipement : this.annneEquipementDebut;
      int anneeFin = anneeEquipement != 0 ? anneeEquipement : this.annneEquipementFin;

      // Seuls les datasets stockant leurs données sont appelés.
      for (int annee = anneeDebut; annee <= anneeFin; annee++) {
         this.equipementRepository.chargerEquipements(annee);
      }
   }
}
