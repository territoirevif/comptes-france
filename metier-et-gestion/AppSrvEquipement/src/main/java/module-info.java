module fr.ecoemploi.application.service.equipement {
   requires fr.ecoemploi.application.service.core;
   requires fr.ecoemploi.application.port.equipement;
   requires fr.ecoemploi.outbound.port.equipement;
   requires fr.ecoemploi.domain.model;

   requires spring.beans;
   requires spring.context;
   requires spring.tx;

   requires org.slf4j;

   exports fr.ecoemploi.application.service.equipement;
}
