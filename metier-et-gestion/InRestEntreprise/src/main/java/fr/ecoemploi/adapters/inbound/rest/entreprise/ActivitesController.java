package fr.ecoemploi.adapters.inbound.rest.entreprise;

import java.io.Serial;
import java.util.List;
// import jakarta.ws.rs.*;
import javax.ws.rs.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.info.*;
import io.swagger.v3.oas.annotations.media.*;
import io.swagger.v3.oas.annotations.responses.*;

import fr.ecoemploi.adapters.inbound.port.entreprise.ActiviteCommuneApi;
import fr.ecoemploi.adapters.inbound.rest.core.AbstractRestController;
import fr.ecoemploi.application.port.entreprise.ActiviteCommunePort;
import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.entreprise.ActiviteCommune;

/**
 * Contrôleur REST pour la gestion des activités.
 * @author Marc LE BIHAN
 */
@RestController
@RequestMapping(value = "/activites", name="activites")
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:4200", "http://localhost:9091"})
@OpenAPIDefinition(info = @Info(title = "Activités des communes"))
public class ActivitesController extends AbstractRestController implements ActiviteCommuneApi {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 4804522911468211960L;

   /** Service d'extraction des activités communales. */
   @Autowired
   private ActiviteCommunePort activitesService;

   /**
    * Obtenir les activités d'une commune.
    * @param anneeCOG Année du COG.
    * @param anneeSIRENE Année SIRENE des entreprises à consulter.
    * @param codeCommune Code commune.
    * @return Activités de la commune.
    */
   @GetMapping(value="/obtenirActivitesCommune")
   @Produces("text/plain")

   @Operation(description = "Obtenir les activités d'une commune")
      @ApiResponses(value = {
         @ApiResponse(responseCode="200", description = "Activité d'une commune.",
            content = {@Content(schema = @Schema(implementation = ActiviteCommune.class))}
         )
      }
   )
   @Override
   public List<ActiviteCommune> obtenirActivitesCommune(
      @Parameter(name = "anneeCOG", description = "Année du Code Officiel Géographique.", example = "2024")
      @RequestParam(name="anneeCOG")
      int anneeCOG,

      @Parameter(name = "anneeSIRENE", description = "Année de référence pour l'extraction des entreprises et établissements.", example = "2024")
      @RequestParam(name="anneeSIRENE")
      int anneeSIRENE,

      @Parameter(name = "codeCommune", description = "Code de la commune dont on veut les activités.", example = "29046")
      @RequestParam(name="codeCommune")
      CodeCommune codeCommune) {
      return this.activitesService.obtenirActivitesCommune(anneeCOG, anneeSIRENE, codeCommune);
   }

   /**
    * Exporter les comptes de résultats et activités par niveau NAF, pour une série d'intercommunalités, dans un fichier CSV.
    * @param anneeCOG Année du COG.
    * @param anneeSIRENE Année des données SIRENE à prendre en considération, pour l'extraction des données entreprise/établissement.
    * @param codeEPCI Code de l'intercommunalité dont on veut les activités.
    * @param codeCommune Code de la commune dont on veut les activités.
    */
   @GetMapping(value="/exporterActivitesCommuneCSV")
   @Produces("text/plain")
   
   @Operation(description = "Exporte les activités (Code APE) d'une commune")
   @ApiResponses(value = {
        @ApiResponse(responseCode="200", description = "CSV des activités de la commune.",  
              content = {@Content(schema = @Schema(implementation = String.class))})
      }
   )
   @Override
   public void exporterActivitesCommuneCSV(
      @Parameter(name = "anneeCOG", description = "Année du Code Officiel Géographique.", example = "2019")
      @RequestParam(name="anneeCOG") int anneeCOG,
      
      @Parameter(name = "anneeSIRENE", description = "Année de référence pour l'extraction des entreprises et établissements.", example = "2018")
      @RequestParam(name="anneeSIRENE") int anneeSIRENE, 

      @Parameter(name = "codeEPCI", description = "Code de l'intercommunalité dont on veut les activités.", example = "242900645")         
      @RequestParam(name="codeEPCI", required = false) String codeEPCI,

      @Parameter(name = "codeCommune", description = "Code de la commune dont on veut les activités.", example = "29046")         
      @RequestParam(name="codeCommune", required = false) String codeCommune) {
      this.activitesService.exporterActivitesCommuneCSV(anneeCOG, anneeSIRENE, codeEPCI, codeCommune);
   }
}
