package fr.ecoemploi.adapters.inbound.rest.entreprise;

import java.io.Serial;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import fr.ecoemploi.adapters.inbound.port.entreprise.EntrepriseApi;
import fr.ecoemploi.adapters.inbound.rest.core.AbstractRestController;
import fr.ecoemploi.application.port.entreprise.EntreprisePort;
import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.entreprise.*;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.media.*;
import io.swagger.v3.oas.annotations.responses.*;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * Contrôleur REST pour la gestion des entreprises.
 * @author Marc LE BIHAN
 */
@RestController
@RequestMapping(value = "/entreprises", name="entreprises")
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:4200", "http://localhost:9091"})
@Tag(name = "Entreprise", description = "Gestion des entreprises et établissements")
public class EntrepriseController extends AbstractRestController implements EntrepriseApi {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 1631295700813886720L;

   /** Service de recherche des entreprises présentes sur le territoire. */
   @Autowired
   private EntreprisePort entreprisePort;

   /**
    * Charger le référentiel des entreprises.
    * @param anneeCOG Année du Code Officiel Géographique, pour les communes de référence
    *     Si toutes les années sirene sont chargées, placer dans ce champ un nombre relatif (0, -1...).
    * @param anneeSirene Année de référence SIREN à charger.<br>
    * 0 si toutes les années connues doivent être chargées (à partir de 2018).
    */
   @Operation(description = "Charger les référentiels d'entreprises et d'établissements (SIRENE, INSEE) dans les fichiers de cache")
   @GetMapping(value = "/chargerReferentiel")
   @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Référentiel chargé."),
      @ApiResponse(responseCode = "403", description = "Si la génération de données par l'application est interdite."),
      @ApiResponse(responseCode = "500", description = "Un incident durant le chargement.")
   })
   @Override
   public void chargerReferentiel(
      @Parameter(name = "anneeCOG", description = "Année du code officiel géographique de référence, pour les communes.<br>" +
         "Si vous chargez toutes les années SIRENE disponibles, placez ici un nombre relatif : 0, -1... : <br>" +
         "l'année du code officiel géographique sera alors calculée d'après l'année SIRENE.",
         examples = {
            @ExampleObject(value = "2024", name="précise", summary = "une année du COG précise",
               description = "Une année du code officiel géographique précise.<br>" +
                  "Au début d'une année, les fichiers SIRENE peuvent faire référence aux communes connues l'année précdénte, pendant un moment."),

            @ExampleObject(value = "-1", name="relative", summary = "une année du COG relative à celle du SIRENE choisi",
               description = "Ici pour une année SIRENE 2023, par exemple,<br>" +
                  "2023 - 1 = 2022 sera l'année du code officiel géographique associé.")
        }
      )
      @RequestParam(name="anneeCOG") int anneeCOG,

      @Parameter(name = "anneeSirene", description = "Année SIRENE des entreprises : 0 pour toutes les années connues.", examples = {
         @ExampleObject(value = "2024", name = "précise", summary = "une année de base SIRENE particulière"),
         @ExampleObject(value = "0", name = "relative", summary = "Toutes les années de la SIRENE disponibles")
      })
      @RequestParam(name="anneeSirene") int anneeSirene) {
      refusSiGenerationInterdite();
      this.entreprisePort.chargerReferentiel(anneeCOG, anneeSirene);
   }

   /**
    * Obtenir les entreprises et établissements d'une commune.
    * @param anneeCOG Année du Code Officiel Géographique de la commune.
    * @param anneeSirene Année du SIREN des entreprises.
    * @param codeCommune Code de la commune désirée.
    * @return Entreprises et établissements de cette commune.
    */
   @Operation(description = "Obtenir les entreprises et établissements d'une commune")
   @GetMapping(value = "/obtenirEntreprises")
   @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Entreprises obtenues",
         content = {@Content(schema = @Schema(implementation = Entreprises.class))}
      ),
      @ApiResponse(responseCode = "500", description = "Un incident durant la demande.")
   })
   public Entreprises obtenirEntreprisesEtEtablissements(
      @Parameter(name = "anneeCOG", description = "Année du code officiel géographique de référence de la commune.",
         examples = {
            @ExampleObject(value = "2024", name="une année de COG", summary = "une année du COG",
               description = "Une année du code officiel géographique<br>"),
         }
      )
      @RequestParam(name="anneeCOG")
      int anneeCOG,

      @Parameter(name = "anneeSirene", description = "Année SIRENE des entreprises", examples = {
         @ExampleObject(value = "2024", name = "une année SIRENE", summary = "une année de base SIRENE particulière")
      })
      @RequestParam(name="anneeSirene")
      int anneeSirene,

      @Parameter(name = "codeCommune", description = "Code commune dont ont désire les entreprises et établissements", examples = {
         @ExampleObject(value = "38522", name = "un code commune", summary = "La ville de Valjouffrey dans l'Isère")
      })
      @RequestParam(name="codeCommune")
      CodeCommune codeCommune) {
      return this.entreprisePort.obtenirEntreprisesEtEtablissements(anneeCOG, anneeSirene, codeCommune);
   }
}
