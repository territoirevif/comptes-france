/**
 * Inbound Rest des entreprises et établissements
 */
module fr.ecoemploi.inbound.rest.entreprise {
   // requires jakarta.ws.rs;
   requires java.ws.rs;
   requires spring.beans;
   requires spring.web;
   requires spring.context;
   requires io.swagger.v3.oas.annotations;

   requires fr.ecoemploi.domain.model;
   requires fr.ecoemploi.inbound.rest.core;

   requires fr.ecoemploi.application.port.entreprise;
   requires fr.ecoemploi.inbound.port.entreprise;

   exports fr.ecoemploi.adapters.inbound.rest.entreprise;
}
