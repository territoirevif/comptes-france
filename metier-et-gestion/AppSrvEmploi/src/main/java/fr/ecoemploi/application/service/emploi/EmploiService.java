package fr.ecoemploi.application.service.emploi;

import java.io.Serial;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ecoemploi.adapters.outbound.port.emploi.BesoinsEnMainOeuvreRepository;
import fr.ecoemploi.application.port.emploi.BesoinsEnMainOeuvrePort;

/**
 * Acquisition des balances de comptes.
 * @author Marc LE BIHAN
 */
@Service
public class EmploiService implements BesoinsEnMainOeuvrePort {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = 9204833260662834156L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EmploiService.class);

   /** Repository des besoins en main d'oeuvre. */
   @Autowired
   private BesoinsEnMainOeuvreRepository besoinsEnMainOeuvreRepository;

   /**
    * {@inheritDoc}
    */
   @Override
   public void chargerBesoinsEnMainOeuvre(int anneeBMO) {
      this.besoinsEnMainOeuvreRepository.chargerBesoinsEnMainOeuvre(anneeBMO);
   }
}
