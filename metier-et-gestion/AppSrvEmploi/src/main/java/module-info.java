module fr.ecoemploi.application.service.emploi {
   requires fr.ecoemploi.application.service.core;
   requires fr.ecoemploi.application.port.emploi;
   requires fr.ecoemploi.outbound.port.emploi;
   requires fr.ecoemploi.domain.model;

   requires spring.beans;
   requires spring.context;

   requires org.slf4j;

   exports fr.ecoemploi.application.service.emploi;
}
