module fr.ecoemploi.application.service.catalogue {
   requires fr.ecoemploi.application.service.core;
   requires fr.ecoemploi.application.port.catalogue;
   requires fr.ecoemploi.outbound.port.catalogue;
   requires fr.ecoemploi.domain.model;

   requires spring.beans;
   requires spring.context;
   requires spring.tx;

   requires org.slf4j;

   exports fr.ecoemploi.application.service.catalogue;
}
