package fr.ecoemploi.application.service.catalogue;

import java.io.Serial;
import java.util.*;

import fr.ecoemploi.adapters.outbound.port.catalogue.CatalogueRessourcesRepository;
import fr.ecoemploi.adapters.outbound.port.catalogue.CatalogueJeuxDonneesRepository;
import fr.ecoemploi.application.port.catalogue.CataloguePort;
import fr.ecoemploi.domain.model.catalogue.JeuDeDonneesElastic;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

/**
 * Service de gestion des catalogues data.gouv.fr
 * @author Marc Le Bihan
 */
@Service
@Transactional
public class CatalogueService implements CataloguePort {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -1556569498255157482L;

   /** Repository des jeux de données data.gouv.fr ELK */
   @Autowired
   @Qualifier("elk")
   private CatalogueJeuxDonneesRepository jeuxDonneesRepositoryElk;

   /** Repository des jeux de données data.gouv.fr Spark */
   @Autowired
   @Qualifier("spark")
   private CatalogueJeuxDonneesRepository jeuxDonneesRepositorySpark;

   /** Repository des ressources data.gouv.fr */
   @Autowired
   @Qualifier("spark")
   private CatalogueRessourcesRepository ressourcesRepository;

   /**
    * Charger le catalogue datagouv.fr
    */
   @Override
   public void chargerCatalogueDataGouvFr() {
      this.jeuxDonneesRepositorySpark.chargerJeuxDeDonneesDatagouv();
      this.jeuxDonneesRepositoryElk.chargerJeuxDeDonneesDatagouv();
      this.ressourcesRepository.chargerRessourcesDatagouv();
   }

   /**
    * Rechercher dans Elastic
    * @param searchText Texte à rechercher
    * @param field Champ où rechercher, spécifiquement. Peut valoir null, et alors la recherche est globale.
    * @return Liste de résultats de recherche
    */
   @Override
   public List<JeuDeDonneesElastic> recherche(String searchText, String field) {
      return this.jeuxDonneesRepositoryElk.recherche(searchText, field);
   }

   /**
    * Enumérer les valeurs distinctes d'un champ du catalogue datagouv.<br>
    * "frequence_catalogue", "licence_catalogue", "granularite_spatiale_catalogue", "zones_spatiales_catalogue", "tags_catalogue", "score_qualite_catalogue"<br>
    * ont des valeurs énumérées.
    * @param champ nom du champ dont ont veut l'énumération des valeurs distinctes
    * @return Map de valeurs que peut prendre ce champ et le nombre d'occurrences qu'on lui a trouvées.<br>
    *    - les valeurs sont converties en chaînes de caractères
    *    - les valeurs sont classés par nombre d'occurences décroissantes.
    */
   @Override
   public Map<String, Long> enumererValeursDistinctes(String champ) {
      return this.jeuxDonneesRepositorySpark.enumererValeursDistinctes(champ);
   }
}
