/**
 * Outbound Port du Code Officiel Géographique (COG)
 */
module fr.ecoemploi.outbound.port.cog {
   requires fr.ecoemploi.domain.model;
   requires spring.context;

   exports fr.ecoemploi.adapters.outbound.port.cog;
}
