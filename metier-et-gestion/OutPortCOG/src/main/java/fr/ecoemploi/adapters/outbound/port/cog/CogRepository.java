package fr.ecoemploi.adapters.outbound.port.cog;

import java.io.Serializable;

import fr.ecoemploi.domain.model.territoire.CodeOfficielGeographique;

/**
 * Repository du Code Officiel Géographique
 */
public interface CogRepository extends Serializable {
   /**
    * Charger le référentiel territorial dans les fichiers de cache.
    * @param anneeCOG Année de référence du Code Officiel Géographique à charger.<br>
    * 0 si toutes les années connues doivent être chargées (à partir de 2016).
    */
   void chargerReferentiels(int anneeCOG);

   /**
    * Obtenir le code officiel géographique.
    * @param anneeCOG année du COG recherché.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *                                          false si seules les communes de type COM sont lues.
    * @return Code Officiel Géographique.
    */
   CodeOfficielGeographique obtenirCodeOfficielGeographique(int anneeCOG, boolean inclureCommunesDelegueesAssociees);
}
