package fr.ecoemploi.adapters.outbound.port.cog;

import java.io.Serializable;

import fr.ecoemploi.domain.model.territoire.*;

/**
 * Repository des évènements communaux
 */
public interface EvenementsCommunauxRepository extends Serializable {
   /**
    * Déterminer ce qu'est devenue une commune au 1er Janvier d'une année.
    * @param codeCommune Code de la commune initiale.
    * @param annee Année considérée.
    * @return Commune résultante ou null, s'il n'y a aucun changement.
    */
   public CommuneHistorique estDevenue(String codeCommune, int annee);
}
