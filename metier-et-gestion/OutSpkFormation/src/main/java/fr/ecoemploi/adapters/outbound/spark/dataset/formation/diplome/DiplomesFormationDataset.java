package fr.ecoemploi.adapters.outbound.spark.dataset.formation.diplome;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

import java.io.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.domain.utils.objets.TechniqueException;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

/**
 * Diplômes et formations à partir des données du recensement 2017, données accessibles : 2016.
 * @author Marc Le Bihan
 */
@Service
public class DiplomesFormationDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -6471453800291293256L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(DiplomesFormationDataset.class);

   /** Nom du fichier CSV. */
   private final String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   private final String repertoireFichier;

   /**
    * Construire un dataset de lecture des diplômes et formations.
    * @param fichier Nom du fichier de recensement.
    * @param repertoire Répertoire du fichier.
    */
   @Autowired
   public DiplomesFormationDataset(@Value("${diplome-formation.fichier.csv.nom}") String fichier, 
      @Value("${formation.dir}") String repertoire) {
      this.repertoireFichier = repertoire;
      this.nomFichier = fichier;
   }
   
   /**
    * Obtenir un Dataset Row des diplômes et formations.
    * @param session Session Spark.
    * @param anneeRecensement Année du recensement.
    * @return Dataset des diplômes et formations.
    * @throws TechniqueException si un incident survient.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> rowDiplomesEtFormations(SparkSession session, int anneeRecensement) throws TechniqueException {
      LOGGER.info("Acquisition des diplômes et formations du recensement {}...", anneeRecensement);

      File repertoireParent = assertExistenceRepertoire(this.repertoireFichier);
      File source = assertExistenceFichierAnnuel(repertoireParent, anneeRecensement, this.nomFichier);
      
      Dataset<Row> formations = session.read().schema(schema(true)).format("csv")
         .option("header","true")
         .option("delimiter", ";")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath())
         .selectExpr("*");

      formations = rename(formations);
      
      RelationalGroupedDataset group =  formations.groupBy(formations.col("codeRegion"), formations.col("codeDepartement"), formations.col("codeCommune"), formations.col("nomCommune"));
      
      formations = group.sum("population2a5ans", "population6a10ans", "population11a14ans", "population15a17ans", "population18a24ans", "population25a29ans", "population30ansEtPlus", 
         "populationScolarisee2a5ans", "populationScolarisee6a10ans", "populationScolarisee11a14ans", "populationScolarisee15a17ans", "populationScolarisee18a24ans", "populationScolarisee25a29ans", "populationScolarisee30ansEtPlus", 
         "populationNonScolarisesTotal", "populationNonScolarisesSansDiplomeOuBEPC", "populationNonScolarisesCAPouBEP", "populationNonScolarisesBAC", "populationNonScolarisesSuperieur",
         "hommesNonScolarisesTotal", "hommesNonScolarisesSansDiplomeOuBEPC", "hommesNonScolarisesCAPouBEP", "hommesNonScolarisesBAC", "hommesNonScolarisesSuperieur",
         "femmesNonScolariseesTotal", "femmesNonScolariseesSansDiplomeOuBEPC", "femmesNonScolariseesCAPouBEP", "femmesNonScolariseesBAC", "femmesNonScolariseesSuperieur");
      
      formations = formations.withColumnRenamed("sum(population2a5ans)", "population2a5ans")
         .withColumnRenamed("sum(population6a10ans)", "population6a10ans")
         .withColumnRenamed("sum(population11a14ans)", "population11a14ans")
         .withColumnRenamed("sum(population15a17ans)", "population15a17ans")
         .withColumnRenamed("sum(population18a24ans)", "population18a24ans")
         .withColumnRenamed("sum(population25a29ans)", "population25a29ans")
         .withColumnRenamed("sum(population30ansEtPlus)", "population30ansEtPlus")
         .withColumnRenamed("sum(populationScolarisee2a5ans)", "populationScolarisee2a5ans")
         .withColumnRenamed("sum(populationScolarisee6a10ans)", "populationScolarisee6a10ans")
         .withColumnRenamed("sum(populationScolarisee11a14ans)", "populationScolarisee11a14ans")
         .withColumnRenamed("sum(populationScolarisee15a17ans)", "populationScolarisee15a17ans")
         .withColumnRenamed("sum(populationScolarisee18a24ans)", "populationScolarisee18a24ans")
         .withColumnRenamed("sum(populationScolarisee25a29ans)", "populationScolarisee25a29ans")
         .withColumnRenamed("sum(populationScolarisee30ansEtPlus)", "populationScolarisee30ansEtPlus")
         .withColumnRenamed("sum(populationNonScolarisesTotal)", "populationNonScolarisesTotal")
         .withColumnRenamed("sum(populationNonScolarisesSansDiplomeOuBEPC)", "populationNonScolarisesSansDiplomeOuBEPC")
         .withColumnRenamed("sum(populationNonScolarisesCAPouBEP)", "populationNonScolarisesCAPouBEP")
         .withColumnRenamed("sum(populationNonScolarisesBAC)", "populationNonScolarisesBAC")
         .withColumnRenamed("sum(populationNonScolarisesSuperieur)", "populationNonScolarisesSuperieur")
         .withColumnRenamed("sum(hommesNonScolarisesTotal)", "hommesNonScolarisesTotal")
         .withColumnRenamed("sum(hommesNonScolarisesSansDiplomeOuBEPC)", "hommesNonScolarisesSansDiplomeOuBEPC")
         .withColumnRenamed("sum(hommesNonScolarisesCAPouBEP)", "hommesNonScolarisesCAPouBEP")
         .withColumnRenamed("sum(hommesNonScolarisesBAC)", "hommesNonScolarisesBAC")
         .withColumnRenamed("sum(hommesNonScolarisesSuperieur)", "hommesNonScolarisesSuperieur")
         .withColumnRenamed("sum(femmesNonScolariseesTotal)", "femmesNonScolariseesTotal")
         .withColumnRenamed("sum(femmesNonScolariseesSansDiplomeOuBEPC)", "femmesNonScolariseesSansDiplomeOuBEPC")
         .withColumnRenamed("sum(femmesNonScolariseesCAPouBEP)", "femmesNonScolariseesCAPouBEP")
         .withColumnRenamed("sum(femmesNonScolariseesBAC)", "femmesNonScolariseesBAC")
         .withColumnRenamed("sum(femmesNonScolariseesSuperieur)", "femmesNonScolariseesSuperieur");

      formations = formations.withColumn("pctScolarisee2a5ans", formations.col("populationScolarisee2a5ans").multiply(lit(100)).divide(formations.col("population2a5ans")))
         .withColumn("pctScolarisee6a10ans", formations.col("populationScolarisee6a10ans").multiply(lit(100)).divide(formations.col("population6a10ans")))
         .withColumn("pctScolarisee11a14ans", formations.col("populationScolarisee11a14ans").multiply(lit(100)).divide(formations.col("population11a14ans")))
         .withColumn("pctScolarisee15a17ans", formations.col("populationScolarisee15a17ans").multiply(lit(100)).divide(formations.col("population15a17ans")))
         .withColumn("pctScolarisee18a24ans", formations.col("populationScolarisee18a24ans").multiply(lit(100)).divide(formations.col("population18a24ans")))
         .withColumn("pctScolarisee25a29ans", formations.col("populationScolarisee25a29ans").multiply(lit(100)).divide(formations.col("population25a29ans")))
         .withColumn("pctScolarisee30ansEtPlus", formations.col("populationScolarisee30ansEtPlus").multiply(lit(100)).divide(formations.col("population30ansEtPlus")))
         
         .withColumn("pctNonScolarisesSansDiplomeOuBEPC", formations.col("populationNonScolarisesSansDiplomeOuBEPC").multiply(lit(100)).divide(formations.col("populationNonScolarisesTotal")))
         .withColumn("pctNonScolarisesCAPouBEP", formations.col("populationNonScolarisesCAPouBEP").multiply(lit(100)).divide(formations.col("populationNonScolarisesTotal")))
         .withColumn("pctNonScolarisesBAC", formations.col("populationNonScolarisesBAC").multiply(lit(100)).divide(formations.col("populationNonScolarisesTotal")))
         .withColumn("pctNonScolarisesSuperieur", formations.col("populationNonScolarisesSuperieur").multiply(lit(100)).divide(formations.col("populationNonScolarisesTotal")))
         
         .withColumn("pctHommesNonScolarisesSansDiplomeOuBEPC", formations.col("hommesNonScolarisesSansDiplomeOuBEPC").multiply(lit(100)).divide(formations.col("populationNonScolarisesTotal")))
         .withColumn("pctHommesNonScolarisesCAPouBEP", formations.col("hommesNonScolarisesCAPouBEP").multiply(lit(100)).divide(formations.col("populationNonScolarisesTotal")))
         .withColumn("pctHommesNonScolarisesBAC", formations.col("hommesNonScolarisesBAC").multiply(lit(100)).divide(formations.col("populationNonScolarisesTotal")))
         .withColumn("pctHommesNonScolarisesSuperieur", formations.col("hommesNonScolarisesSuperieur").multiply(lit(100)).divide(formations.col("populationNonScolarisesTotal")))
         
         .withColumn("pctFemmesNonScolariseesSansDiplomeOuBEPC", formations.col("femmesNonScolariseesSansDiplomeOuBEPC").multiply(lit(100)).divide(formations.col("populationNonScolarisesTotal")))
         .withColumn("pctFemmesNonScolariseesCAPouBEP", formations.col("femmesNonScolariseesCAPouBEP").multiply(lit(100)).divide(formations.col("populationNonScolarisesTotal")))
         .withColumn("pctFemmesNonScolariseesBAC", formations.col("femmesNonScolariseesBAC").multiply(lit(100)).divide(formations.col("populationNonScolarisesTotal")))
         .withColumn("pctFemmesNonScolariseesSuperieur", formations.col("femmesNonScolariseesSuperieur").multiply(lit(100)).divide(formations.col("populationNonScolarisesTotal")));
      
      return formations;
   }
   
   /**
    * Renvoyer le schema du dataset.
    * @param renamed true si les champs doivent être renvoyés renommés, false pour ceux originaux du fichier CSV. 
    * @return schema.
    */
   public StructType schema(boolean renamed) {
      /*
         Données infra-communales    -     Diplômes - Formation      
         Mise en ligne le 22/10/2019       Géographie au 01/01/2018     
               
         IRIS                    Code du département suivi du numéro de commune ou du numéro d'arrondissement municipal suivi du numéro d'IRIS
         REG                     Code de la région
         DEP                     Code du département
         UU2010                  Code du département ou "00" pour les unités urbaines qui s'étendent sur plusieurs départements voire au-delà de la frontière suivi d'un code sur une position indiquant la taille de la population puis d'un numéro d'ordre à l'intérieur de la taille
         COM                     Code du département suivi du numéro de commune ou du numéro d'arrondissement municipal pour Paris Lyon et Marseille
         LIBCOM                  Libellé de la commune ou de l'arrondissement municipal pour Paris Lyon et Marseille
         TRIRIS                  Code du département suivi d'un numéro d'ordre à l'intérieur du département sur trois positions puis d'un indicateur de TRIRIS
         GRD_QUART               Code du département suivi du numéro de commune ou du numéro d'arrondissement municipal pour Paris Lyon et Marseille suivi du numéro de grand quartier
         LIBIRIS                 Libellé de l'IRIS à l'intérieur de la commune ou de l'arrondissement municipal pour Paris Lyon et Marseille
         TYP_IRIS                Type d'IRIS : habitat (H), activité (A), divers (D), Autre (Z)
         MODIF_IRIS              Type de modification de l'IRIS
         LAB_IRIS                Label de qualité de l'IRIS
         P16_POP0205             Nombre de personnes de 2 à 5 ans
         P16_POP0610             Nombre de personnes de 6 à 10 ans
         P16_POP1114             Nombre de personnes de 11 à 14 ans
         P16_POP1517             Nombre de personnes de 15 à 17 ans
         P16_POP1824             Nombre de personnes de 18 à 24 ans
         P16_POP2529             Nombre de personnes de 25 à 29 ans
         P16_POP30P              Nombre de personnes de 30 ans ou plus
         P16_SCOL0205            Nombre de personnes scolarisées de 2 à 5 ans
         P16_SCOL0610            Nombre de personnes scolarisées de 6 à 10 ans
         P16_SCOL1114            Nombre de personnes scolarisées de 11 à 14 ans
         P16_SCOL1517            Nombre de personnes scolarisées de 15 à 17 ans
         P16_SCOL1824            Nombre de personnes scolarisées de 18 à 24 ans
         P16_SCOL2529            Nombre de personnes scolarisées de 25 à 29 ans
         P16_SCOL30P             Nombre de personnes scolarisées de 30 ans ou plus
         P16_NSCOL15P            Nombre de personnes non scolarisées de 15 ans ou plus
         P16_NSCOL15P_DIPLMIN    Nombre de personnes non scolarisées de 15 ans ou plus titulaires d'aucun diplôme ou au plus un BEPC, brevet des collèges ou DNB
         P16_NSCOL15P_CAPBEP     Nombre de personnes non scolarisées de 15 ans ou plus titulaires d'un CAP ou d'un BEP
         P16_NSCOL15P_BAC        Nombre de personnes non scolarisées de 15 ans ou plus titulaires d'un baccalauréat (général, technologique, professionnel)
         P16_NSCOL15P_SUP        Nombre de personnes non scolarisées de 15 ans ou plus titulaires d'un diplôme de l'enseignement supérieur
         P16_HNSCOL15P           Nombre d'hommes non scolarisés de 15 ans ou plus
         P16_HNSCOL15P_DIPLMIN   Nombre d'hommes non scolarisés de 15 ans ou plus titulaires d'aucun diplôme ou au plus un BEPC, brevet des collèges ou DNB
         P16_HNSCOL15P_CAPBEP    Nombre d'hommes non scolarisés de 15 ans ou plus titulaires d'un CAP ou d'un BEP
         P16_HNSCOL15P_BAC       Nombre d'hommes non scolarisés de 15 ans ou plus titulaires d'un baccalauréat (général, technologique, professionnel)
         P16_HNSCOL15P_SUP       Nombre d'hommes non scolarisés de 15 ans ou plus titulaires d'un diplôme de l'enseignement supérieur
         P16_FNSCOL15P           Nombre de femmes non scolarisées de 15 ans ou plus
         P16_FNSCOL15P_DIPLMIN   Nombre de femmes non scolarisées de 15 ans ou plus titulaires d'aucun diplôme ou au plus un BEPC, brevet des collèges ou DNB
         P16_FNSCOL15P_CAPBEP    Nombre de femmes non scolarisées de 15 ans ou plus titulaires d'un CAP ou d'un BEP
         P16_FNSCOL15P_BAC       Nombre de femmes non scolarisées de 15 ans ou plus titulaires d'un baccalauréat (général, technologique, professionnel)
         P16_FNSCOL15P_SUP       Nombre de femmes non scolarisées de 15 ans ou plus titulaires d'un diplôme de l'enseignement supérieur
       */
      
      /*
       * IRIS;REG;DEP;UU2010;COM;LIBCOM;TRIRIS;GRD_QUART;LIBIRIS;TYP_IRIS;MODIF_IRIS;LAB_IRIS;P16_POP0205;P16_POP0610;P16_POP1114;P16_POP1517;P16_POP1824;P16_POP2529;P16_POP30P;P16_SCOL0205;P16_SCOL0610;P16_SCOL1114;P16_SCOL1517;P16_SCOL1824;P16_SCOL2529;P16_SCOL30P;P16_NSCOL15P;P16_NSCOL15P_DIPLMIN;P16_NSCOL15P_CAPBEP;P16_NSCOL15P_BAC;P16_NSCOL15P_SUP;P16_HNSCOL15P;P16_HNSCOL15P_DIPLMIN;P16_HNSCOL15P_CAPBEP;P16_HNSCOL15P_BAC;P16_HNSCOL15P_SUP;P16_FNSCOL15P;P16_FNSCOL15P_DIPLMIN;P16_FNSCOL15P_CAPBEP;P16_FNSCOL15P_BAC;P16_FNSCOL15P_SUP
       * 010010000;84;01;01000;01001;L'Abergement-Clémenciat;ZZZZZZ;0100100;L'Abergement-Clémenciat (commune non irisée);Z;0;5;25;57;57;33;36;33;504;17;55;57;32;20;2;2;550;131;184;94;141;278;63;98;46;71;272;68;86;48;70
       * 010020000;84;01;01000;01002;L'Abergement-de-Varey;ZZZZZZ;0100200;L'Abergement-de-Varey (commune non irisée);Z;0;5;16;14;17;7;8;13;160;11;14;17;7;3;1;2;175;29;47;38;61;93;15;29;23;26;82;14;18;15;35
       */
      StructType schema = new StructType();
      schema = schema.add("IRIS", StringType, false)
         .add("REG", StringType, false)
         .add("DEP", StringType, false)
         .add("UU2010", StringType, false)
         .add("COM", StringType, false)
         .add("LIBCOM", StringType, false)
         .add("TRIRIS", StringType, false)
         .add("GRD_QUART", StringType, false)
         .add("LIBIRIS", StringType, false)
         .add("TYP_IRIS", StringType, false)
         .add("MODIF_IRIS", StringType, false)
         .add("LAB_IRIS", StringType, false)
         .add("P16_POP0205", IntegerType, false)
         .add("P16_POP0610", IntegerType, false)
         .add("P16_POP1114", IntegerType, false)
         .add("P16_POP1517", IntegerType, false)
         .add("P16_POP1824", IntegerType, false)
         .add("P16_POP2529", IntegerType, false)
         .add("P16_POP30P", IntegerType, false)
         .add("P16_SCOL0205", IntegerType, false)
         .add("P16_SCOL0610", IntegerType, false)
         .add("P16_SCOL1114", IntegerType, false)
         .add("P16_SCOL1517", IntegerType, false)
         .add("P16_SCOL1824", IntegerType, false)
         .add("P16_SCOL2529", IntegerType, false)
         .add("P16_SCOL30P", IntegerType, false)
         .add("P16_NSCOL15P", IntegerType, false)
         .add("P16_NSCOL15P_DIPLMIN", IntegerType, false)
         .add("P16_NSCOL15P_CAPBEP", IntegerType, false)
         .add("P16_NSCOL15P_BAC", IntegerType, false)
         .add("P16_NSCOL15P_SUP", IntegerType, false)
         .add("P16_HNSCOL15P", IntegerType, false)
         .add("P16_HNSCOL15P_DIPLMIN", IntegerType, false)
         .add("P16_HNSCOL15P_CAPBEP", IntegerType, false)
         .add("P16_HNSCOL15P_BAC", IntegerType, false)
         .add("P16_HNSCOL15P_SUP", IntegerType, false)
         .add("P16_FNSCOL15P", IntegerType, false)
         .add("P16_FNSCOL15P_DIPLMIN", IntegerType, false)
         .add("P16_FNSCOL15P_CAPBEP", IntegerType, false)
         .add("P16_FNSCOL15P_BAC", IntegerType, false)
         .add("P16_FNSCOL15P_SUP", IntegerType, false);
      
      return schema;
   }

   /**
    * Renommer les champs du dataset.
    * @param dataset Dataset aux champs à renomer.
    * @return Dataset aux champs renommés.
    */
   private Dataset<Row> rename(Dataset<Row> dataset) {
      return dataset
         .withColumnRenamed("P16_POP0205", "population2a5ans")
         .withColumnRenamed("P16_POP0610","population6a10ans")
         .withColumnRenamed("P16_POP1114","population11a14ans")
         .withColumnRenamed("P16_POP1517","population15a17ans")
         .withColumnRenamed("P16_POP1824","population18a24ans")

         .withColumnRenamed("P16_POP2529","population25a29ans")
         .withColumnRenamed("P16_POP30P","population30ansEtPlus")
         .withColumnRenamed("P16_SCOL0205","populationScolarisee2a5ans")
         .withColumnRenamed("P16_SCOL0610","populationScolarisee6a10ans")
         .withColumnRenamed("P16_SCOL1114","populationScolarisee11a14ans")

         .withColumnRenamed("P16_SCOL1517","populationScolarisee15a17ans")
         .withColumnRenamed("P16_SCOL1824","populationScolarisee18a24ans")
         .withColumnRenamed("P16_SCOL2529","populationScolarisee25a29ans")
         .withColumnRenamed("P16_SCOL30P","populationScolarisee30ansEtPlus")
         .withColumnRenamed("P16_NSCOL15P","populationNonScolarisesTotal")

         .withColumnRenamed("P16_NSCOL15P_DIPLMIN","populationNonScolarisesSansDiplomeOuBEPC")
         .withColumnRenamed("P16_NSCOL15P_CAPBEP","populationNonScolarisesCAPouBEP")
         .withColumnRenamed("P16_NSCOL15P_BAC","populationNonScolarisesBAC")
         .withColumnRenamed("P16_NSCOL15P_SUP","populationNonScolarisesSuperieur")
         .withColumnRenamed("P16_HNSCOL15P","hommesNonScolarisesTotal")

         .withColumnRenamed("P16_HNSCOL15P_DIPLMIN","hommesNonScolarisesSansDiplomeOuBEPC")
         .withColumnRenamed("P16_HNSCOL15P_CAPBEP","hommesNonScolarisesCAPouBEP")
         .withColumnRenamed("P16_HNSCOL15P_BAC","hommesNonScolarisesBAC")
         .withColumnRenamed("P16_HNSCOL15P_SUP","hommesNonScolarisesSuperieur")
         .withColumnRenamed("P16_FNSCOL15P","femmesNonScolariseesTotal")

         .withColumnRenamed("P16_FNSCOL15P_DIPLMIN","femmesNonScolariseesSansDiplomeOuBEPC")
         .withColumnRenamed("P16_FNSCOL15P_CAPBEP","femmesNonScolariseesCAPouBEP")
         .withColumnRenamed("P16_FNSCOL15P_BAC","femmesNonScolariseesBAC")
         .withColumnRenamed("P16_FNSCOL15P_SUP","femmesNonScolariseesSuperieur")
         .withColumnRenamed("REG", "codeRegion")

         .withColumnRenamed("DEP", "codeDepartement")
         .withColumnRenamed("COM", "codeCommune")
         .withColumnRenamed("LIBCOM", "nomCommune");
   }
}
