package fr.ecoemploi.adapters.outbound.spark.dataset.formation.diplome;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.domain.utils.objets.TechniqueException;

import org.apache.spark.sql.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

/**
 * Tests sur les diplômes et formations.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkFormationTestApplication.class)
class DiplomesFormationsIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -8516900345348543066L;

   /** Dataset des diplômes et formations. */
   @Autowired
   private DiplomesFormationDataset diplomesFormationsDataset;
   
   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Activer ou désactiver les caches des datasets.
    */
   @BeforeEach
   public void desactiverCaches() {
      // this.diplomesFormationsDataset.loader().setCache(activerCache());
   }

   /**
    * Lecture des diplômes et formations.
    * @throws TechniqueException si un incident survient.
    */
   @Test
   @DisplayName("Diplômes et formations")
   void diplomesEtFormations() throws TechniqueException {
      Dataset<Row> ds = this.diplomesFormationsDataset.rowDiplomesEtFormations(this.session, 2016);
      
      ds.show(500, false);
      assertNotEquals(0L, ds.count(), "Une information de diplôme/formation au moins aurait du être lue");
   }
}
