#!/bin/bash
#
# ********************************************************************************
# * Démarrrage, en mode développement, du contrôleur web, Java, de l'IHM Angular *
# ********************************************************************************

java --add-exports java.base/sun.nio.ch=ALL-UNNAMED \
   --add-exports java.base/sun.nio.ch=ALL-UNNAMED \
   --add-opens java.base/java.util=ALL-UNNAMED \
   --add-opens java.base/java.io=ALL-UNNAMED \
   --add-opens java.base/java.nio=ALL-UNNAMED \
   --add-opens java.base/java.lang=ALL-UNNAMED \
   --add-opens java.base/java.lang.invoke=ALL-UNNAMED \
   --add-opens java.base/sun.security.util=ALL-UNNAMED \
   --add-opens java.base/sun.security.action=ALL-UNNAMED \
   -jar target/etude.war
