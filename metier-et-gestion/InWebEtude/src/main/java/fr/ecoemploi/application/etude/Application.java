package fr.ecoemploi.application.etude;

import java.util.*;

import org.slf4j.*;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.*;
import org.springframework.context.annotation.*;
import org.springframework.core.env.*;
import org.springframework.web.client.*;

/**
 * Application de démarrage Spring-boot.
 * @author Marc LE BIHAN
 */
@SpringBootApplication
@ComponentScan(basePackages={"fr.ecoemploi"})
public class Application implements ApplicationContextAware {
   /** LOGGER. */
   private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);
   
   /**
    * Démarrage de l'application.
    * @param args Arguments.
    */
   public static void main(String[] args) {
      SpringApplication.run(Application.class, args);
   }

   /**
    * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
    */
   @Override
   public void setApplicationContext(ApplicationContext ctx) {
      Environment env = ctx.getEnvironment();
      LOGGER.info("\nL'application démarre avec ces profils : {}", Arrays.asList(env.getActiveProfiles()));

      if (env.getActiveProfiles().length == 0) {
         LOGGER.warn("Vous n'avez pas précisé de profil actif. L'application va fonctionner avec des valeurs de développement très spécifiques.");
      }
   }

   /**
    * REST Template pour le démarrage de SwaggerAPI (OpenAPI).
    * @return REST Template.
    */
   @Bean
   public RestTemplate getRestTemplate() {
       return new RestTemplate();
   }   
}
