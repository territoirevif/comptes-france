import { Component } from '@angular/core';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'etude';

  constructor() {
    const message = `Démarrage environnement ${environment.nom_environnement} spark: ${environment.spark_url} backend: ${environment.backend_url}`;
    console.log(message);
  }

  protected readonly environment = environment;
}
