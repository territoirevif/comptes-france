import {Component, OnInit, Input, OnDestroy} from '@angular/core';

import Map from 'ol/Map';
import View from 'ol/View';
import { Tile } from 'ol/layer';
import TileLayer from 'ol/layer/Tile';
import WMTS from 'ol/source/WMTS';
import WMTSTileGrid from 'ol/tilegrid/WMTS';

import {fromLonLat, get as getProjection, Projection} from 'ol/proj';
import {getWidth, getTopLeft, Extent} from 'ol/extent';
import {Subscription} from 'rxjs';
import {CommuneService} from '../../core/services/commune.service';
import {Commune} from '../../../../openapi';

/**
 * Une carte présentée par Open Layers
 */
@Component({
  selector: 'app-carte',
  templateUrl: './carte.component.html',
  styleUrls: ['./carte.component.scss']
})
export class CarteComponent implements OnInit, OnDestroy {
  /** Identifiant de la carte (placé dans sa balise) */
  @Input() id!: string;

  /** Nom de cette carte */
  @Input() nom!: string;

  /** Longitude. */
  @Input() longitude!: number;

  /** Latitude. */
  @Input() latitude!: number;

  /** Niveau de zoom initial. */
  @Input() zoom!: number;

  /** Niveau de zoom quand un focus est demandé sur une commune en particulier. */
  @Input() zoom_focus!: number;

  /** Projection */
  @Input() projection!: string;

  /** URL de la couche à afficher initialement. */
  @Input() url_couche!: string;

  /** Layer de la couche à afficher initialement. */
  @Input() layer_couche!: string;

  /** Filtre de région. */
  @Input() filtre_region: string;

  /** Code des régions d'outremer */
  private readonly codes_regions_outremer: string[] = ['01', '02', '03', '04', '06'];

  /** Couche géographique à afficher */
  private couche_geographique: TileLayer<WMTS>;

  /** Map Openlayer */
  private coucheOpenLayer: Map;

  /** Souscription aux évènements relatifs aux communes. */
  communesSubscription: Subscription;

  /**
   * Construire le composant
   * @param communeService Service de gestion des communes.
   */
  constructor(private readonly communeService: CommuneService) {
    this.communesSubscription = Subscription.EMPTY;
  }

  /**
   * Initialiser le composant carte
   */
  ngOnInit(): void {
    // Déclarer la couche, si elle n'est pas encore connue
    if (this.couche_geographique == null) {
      console.log(`Chargement initial de la carte '${this.nom}' (${this.id}) avec la couche '${this.layer_couche}' depuis l'URL ${this.url_couche}`)
      this.couche_geographique = this.declare_couche(this.url_couche, this.layer_couche, this.projection, 'image/png');
    }

    // Placer la couche demandée, aux coordonnées et zoom initiaux demandées
    this.coucheOpenLayer = new Map({
      layers: [ this.couche_geographique ],
      target: this.id,
      view: new View({
        center: fromLonLat([this.longitude, this.latitude]),
        zoom: this.zoom,
        projection: this.projection
      })
    });

    // Réagir à un changement de commune
    this.communesSubscription = this.communeService.selectionCommune.subscribe({
      next: (commune: Commune) => this.onChangeCommune(commune)
    })
  }

  /** Réaction à la destruction de l'application */
  ngOnDestroy(): void {
    this.communesSubscription.unsubscribe();
  }

  /**
   * Réagir à un changement de commune.
   * @param commune Commune qui vient d'être choisie.
   */
  private onChangeCommune(commune: Commune): void {
    // Si une carte désire une région spécifique, la limiter à celle-ci
    if (this.filtre_region != null && this.filtre_region != '') {
      if (commune.codeRegion != this.filtre_region) {
        return;
      }
    }

    this.coucheOpenLayer.getView().setCenter(fromLonLat([commune.longitude, commune.latitude]));
    this.coucheOpenLayer.getView().setZoom(this.zoom_focus);
    console.log(`%s affiche %s (lat: %f, long: %f)`, this.nom, commune.nomCommune, commune.latitude, commune.longitude);
  }

  /**
   * Déclare une couche tuilée WMTS
   * @param url URL
   * @param layer Nom de la couche
   * @param projection Projection à utiliser pour calculer la grille de tuiles (exemple : EPSG:3857)
   * @param format Format d'image à utiliser.
   * @return Couche tuilée WMTS
   */
  private declare_couche(url: string, layer: string, projection: string, format: string): TileLayer<WMTS> {
    return new Tile({
      source : new WMTS({
        url,
        layer,
        matrixSet: 'PM',
        format,
        style: 'normal',
        tileGrid: this.grilleTuiles(projection)
      })
    });
  }

  /**
   * Renvoie une grille de tuiles
   * @param projection Projection (exemple : EPSG:3857)
   * @return Les résolutions à suivre dans un WMTS Tile Grid
   */
  private grilleTuiles(projection: string): WMTSTileGrid {
    const proj: Projection = getProjection(projection);

    if (proj == null) {
      console.error(`La projection '${projection}' n'a pas pu être construite par OpenLayers pour la carte '${this.nom}'`)
      return
    }

    const maxResolution: number = getWidth(proj.getExtent()) / 256;
    const projectionExtent: Extent = proj.getExtent();

    const resolutions: number[] = [];
    const matrixIds: string[] = [];

    for (let i: number = 0; i < 20; i++) {
      matrixIds[i] = i.toString();
      resolutions[i] = maxResolution / Math.pow(2, i);
    }

    return new WMTSTileGrid( {
      origin: getTopLeft(projectionExtent), // coin supérieur gauche
      resolutions,
      matrixIds
    });
  }
}
