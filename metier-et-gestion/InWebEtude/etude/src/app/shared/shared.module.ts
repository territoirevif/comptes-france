import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import {MenuComponent} from "./menu/menu.component";
import {SchemaComponent} from "./schema/schema.component";
import {PresentationComponent} from "./presentation/presentation.component";
import {CarteComponent} from "./carte/carte.component";


@NgModule({
  declarations: [
    MenuComponent,
    CarteComponent,
    PresentationComponent,
    SchemaComponent,
  ],
  imports: [
    CommonModule,
    SharedRoutingModule
  ],
  exports: [
    MenuComponent,
    CarteComponent,
    PresentationComponent,
    SchemaComponent
  ]
})
export class SharedModule { }
