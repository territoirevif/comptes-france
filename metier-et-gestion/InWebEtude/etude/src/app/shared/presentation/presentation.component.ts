import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {Commune} from "../../../../openapi";
import {CommuneService} from "../../core/services/commune.service";
import {round} from "ol/math";

/**
 * Le composant parent des présentations ou descriptions de communes ou d'analyses
 */
@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.scss']
})
export class PresentationComponent implements OnInit, OnDestroy {
  /** Souscription aux évènements relatifs aux communes. */
  communesSubscription: Subscription;

  /** Commune sur laquelle le composant se focalise, s'il y en a une */
  commune: Commune

  /**
   * Construire le composant
   * @param communeService Service de gestion des communes.
   */
  constructor(private readonly communeService: CommuneService) {
    this.communesSubscription = Subscription.EMPTY;
  }

  /**
   * Initialiser le composant de présentation
   */
  ngOnInit(): void {
    // Réagir à un changement de commune
    this.communesSubscription = this.communeService.selectionCommune.subscribe({
      next: (commune: Commune) => this.onChangeCommune(commune)
    })
  }

  /** Réaction à la destruction de l'application */
  ngOnDestroy(): void {
    this.communesSubscription.unsubscribe();
  }

  /**
   * Déterminer si ce composant se focalise sur une commune en particulier
   * @return true si c'est le cas.
   */
  public isCommuneSelected() : boolean {
    return this.commune != null;
  }

  /**
   * Renvoyer la densité de population
   * @return densité de population
   */
  public getDensitePopulation() : number {
    if (this.commune == null || this.commune.surface == 0) {
      return 0
    }

    let surfaceKm2 = this.commune.surface / 100.00; // La surface est exprimée en hectares
    return this.commune.population / surfaceKm2
  }

  /**
   * Réagir à un changement de commune.
   * @param commune Commune qui vient d'être choisie.
   */
  private onChangeCommune(commune: Commune): void {
    console.log(`composant présentation: réagit à onChangeCommune(%s = %s)`, commune.codeCommune, commune.nomCommune);
    this.commune = commune;
  }

  protected readonly round = round;
}
