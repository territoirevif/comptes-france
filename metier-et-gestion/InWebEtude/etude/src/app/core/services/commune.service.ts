import {EventEmitter, Injectable, Output} from '@angular/core';
import {Commune} from '../../../../openapi';

/** Service de gestion des communes */
@Injectable({
  providedIn: 'root'
})
export class CommuneService {
  /** Emetteur d'évènements de changement de commune */
  @Output()
  public selectionCommune: EventEmitter<Commune> = new EventEmitter<Commune>();

  /**
   * Réaction à un changement de commune
   * @param commune Commune qui remplace la précédente sélectionnée.
   */
  public changementCommune(commune: Commune) : void {
    console.log('une commune a été sélectionnée: %s (%s, code région : %s)', commune.nomCommune, commune.nomDepartement, commune.codeRegion);
    this.selectionCommune.emit(commune);
  }
}
