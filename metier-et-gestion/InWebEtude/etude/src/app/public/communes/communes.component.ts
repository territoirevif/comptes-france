import {Component, OnInit, Input} from '@angular/core';

import { CodeOfficielGographiqueService, Commune } from '../../../../openapi';

import { environment } from '../../../environments/environment';
import {CommuneService} from '../../core/services/commune.service';

/**
 * Combobox en autocomplete des communes du COG.
 */
@Component({
  selector: 'app-communes',
  templateUrl: './communes.component.html',
  styleUrls: ['./communes.component.scss']
})
export class CommunesComponent implements OnInit {
  /** Année du Code Officiel Géographique. */
  @Input()
  anneeCOG: number;

  /** Locale du tri des communes */
  @Input()
  locale: string = 'fr_FR';

  /** Liste des communes ordonnées par locale */
  communes: Commune[];

  /** Codes communes avec leur commune associée. */
  communesMap: Map<string, Commune> = new Map<string, Commune>();

  /** Code commune sélectionné. */
  codeCommune: string = null;

  /** Commune sélectionnée (objet). */
  commune: Commune = null;

  /** Libellé final de la commune sélectionnée. */
  libelleCommuneSelectionnee: string;

  /**
   * Construire une instance du composant.
   * @param cogService Service REST d'accès au Code Officiel Géographique.
   * @param communeService Gestion des communes.
   */
  constructor(private readonly cogService: CodeOfficielGographiqueService, private communeService: CommuneService) {
    cogService.configuration.basePath = environment.spark_url;
  }

  /**
   * Réaction à l'initialisation du composant :
   * Lire les communes triées par locale, et sous forme de Map indexée par codeCommune.
   */
  ngOnInit(): void {
    // Vérification du paramètre d'appel (année)
    if (this.anneeCOG < 2019 || this.anneeCOG >= 2100) {
      const message = 'L\'année du code officiel géographique ' + this.anneeCOG + ' n\'est pas valide : choissez-là à partir de 2019.';
      console.log(message);
      return;
    }

    this.cogService.obtenirCommunesTriParLocale(this.anneeCOG, false, this.locale).subscribe(data => {
      this.communes = data;

      this.communes.forEach(commune => {
        this.communesMap.set(commune.codeCommune, commune);
      });
    });
  }

  /**
   * Réagir au changement de code commune.
   * @param codeCommune Code commune sélectionné.
   */
  public selectCommune(codeCommune: string) : void {
    this.codeCommune = codeCommune;
    this.commune = this.communesMap.get(codeCommune);

    // Si une commune est sélectionnée, modifier son libellé de présentation dans le sélecteur et avertir le service de gestion des communes
    if (this.commune != null) {
      this.libelleCommuneSelectionnee = `${this.commune.nomCommune} (${this.commune.nomDepartement}) ${this.commune.population} hab.`;
      this.communeService.changementCommune(this.commune);
    }
  }
}
