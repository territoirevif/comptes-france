import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import {FormsModule} from "@angular/forms";

import {CarteFranceComponent} from "./carte-france/carte-france.component";
import {CommunesComponent} from "./communes/communes.component";
import {SharedModule} from "../shared/shared.module";


@NgModule({
  declarations: [
    CarteFranceComponent,
    CommunesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,

    PublicRoutingModule,
    SharedModule
  ],
  exports: [
    CarteFranceComponent,
    CommunesComponent
  ]
})
export class PublicModule { }
