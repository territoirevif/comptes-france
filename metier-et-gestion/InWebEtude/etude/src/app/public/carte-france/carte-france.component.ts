import { Component } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-carte-france',
  templateUrl: './carte-france.component.html',
  styleUrls: ['./carte-france.component.scss']
})
export class CarteFranceComponent {
  protected readonly environment= environment;
  protected regions: string[] = ['metropole', 'guadeloupe', 'martinique', 'guyane', 'la_reunion', 'mayotte']
}
