export const environment = {
  production: true,

  nom_environnement: 'production',
  spark_url: 'http://localhost:9090',
  backend_url: 'http://localhost:9091',

  // Année du Cog pour l'affichage du maillage territorial, par défaut
  anneeCog: 2024,

  // Couche plan IGN V2
  plan_ign_v2_layer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2',

  // Couche Orthophotos
  orthophotos_layer: 'ORTHOIMAGERY.ORTHOPHOTOS',

  // Projections, zooms, latitudes et longitudes initiales pour les régions françaises
  // Métropole
  metropole_id: 'carte_metropole',         // Identifiant de la carte (placé dans la balise)
  metropole_nom: 'Métropole',              // Nom de cette partie de carte
  metropole_couche_layer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2',           // Plan IGN V2
  metropole_couche_url: 'https://data.geopf.fr/wmts', // Plan IGN V2, tuilé
  metropole_projection: 'EPSG:3857',       // Web Mercator projection
  metropole_latitude_initale: 46.53333333, // Centroïde de la France, d'après IGN en 1993 (Commune de Vesdun dans le Cher)
  metropole_longitude_initale: 2.41666667,
  metropole_zoom: 5.9,                     // Zoom initial d'affichage de la carte
  metropole_zoom_focus: 12,                // Zoom si un focus est demandé sur une commune
  metropole_filtre_region: '',             // Si non vide, filtre les régions auquel la carte réagit

  // Guadeloupe
  guadeloupe_id: 'carte_guadeloupe',
  guadeloupe_nom: 'Guadeloupe',
  guadeloupe_couche_layer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2',
  guadeloupe_couche_url: 'https://data.geopf.fr/wmts',
  guadeloupe_projection: 'EPSG:3857',
  guadeloupe_latitude_initale: 16.265,
  guadeloupe_longitude_initale: -61.551,
  guadeloupe_zoom: 8.24,
  guadeloupe_zoom_focus: 12,
  guadeloupe_filtre_region: '01',

  // Martinique
  martinique_id: 'carte_martinique',
  martinique_nom: 'Martinique',
  martinique_couche_layer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2',
  martinique_couche_url: 'https://data.geopf.fr/wmts',
  martinique_projection: 'EPSG:3857',
  martinique_latitude_initale: 14.641528,
  martinique_longitude_initale: -61.024174,
  martinique_zoom: 8.1,
  martinique_zoom_focus: 12,
  martinique_filtre_region: '02',

  // Guyane
  guyane_id: 'carte_guyane',
  guyane_nom: 'Guyane',
  guyane_couche_layer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2',
  guyane_couche_url: 'https://data.geopf.fr/wmts',
  guyane_projection: 'EPSG:3857',
  guyane_latitude_initale: 3.933889,
  guyane_longitude_initale: -53.125782,
  guyane_zoom: 5.5,
  guyane_zoom_focus: 12,
  guyane_filtre_region: '03',

  // La Réunion
  la_reunion_id: 'carte_la_reunion',
  la_reunion_nom: 'La Réunion',
  la_reunion_couche_layer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2',
  la_reunion_couche_url: 'https://data.geopf.fr/wmts',
  la_reunion_projection: 'EPSG:3857',
  la_reunion_latitude_initale: -21.115141,
  la_reunion_longitude_initale: 55.536384,
  la_reunion_zoom: 8.1,
  la_reunion_zoom_focus: 12,
  la_reunion_filtre_region: '04',

  // Mayotte
  mayotte_id: 'carte_mayotte',
  mayotte_nom: 'Mayotte',
  mayotte_couche_layer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2',
  mayotte_couche_url: 'https://data.geopf.fr/wmts',
  mayotte_projection: 'EPSG:3857',
  mayotte_latitude_initale: -12.827500,
  mayotte_longitude_initale: 45.166244,
  mayotte_zoom: 8.9,
  mayotte_zoom_focus: 12,
  mayotte_filtre_region: '06'
};
