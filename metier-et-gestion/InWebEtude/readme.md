# Application cliente web

### Web (Angular) 

En ébauche. En attente de la réouverture de l'espace Pro d'IGN pour l'exploiter.

- Installation d'Open Layers par `npm install ol`.
- Installation du générateur de code typescript pour OpenAPI par : `npm add @openapitools/openapi-generator-cli`.

- Lancement 

1. Aller sur `comptes-france/dev/metier-et-gestion/dev/ApplicationEtude/etude`
2. Lancer `npm start`
3. Ouvrir un navigateur sur : [http://localhost:4200/](http://localhost:4200/)

- En cas d'apparition d'un code d'erreur -126 lors d'un `ng build`, supprimer le répertoire `node_modules` et `node`.

- Tenir à jour npm par la commande `npm update npm -g`.
  
(La génération de la partie cliente (sources initiaux) a été faite par un `ng new webapp` lancé depuis `src/main/`).  

```bash
# Exécute un code Javascript, pour test. 
npm install typescript - g
tsc xy.ts
node xy.ts
```

À placer dans angular.json :

```json
"styles": [
   "node_modules/ol/ol.css",
   "src/styles.css"
],

```

Pour générer les stubs REST :

```bash
# Appelle la commande placée dans la liste de scripts de package.json
npm run generate:api
```

