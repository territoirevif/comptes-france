/**
 * Inbound Port du Catalogue data.gouv.fr
 */
module fr.ecoemploi.inbound.port.catalogue {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.adapters.inbound.port.catalogue;
}
