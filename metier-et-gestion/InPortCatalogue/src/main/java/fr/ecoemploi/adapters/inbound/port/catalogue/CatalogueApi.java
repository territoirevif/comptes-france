package fr.ecoemploi.adapters.inbound.port.catalogue;

import java.io.Serializable;
import java.util.Map;

/**
 * Actions possibles sur les catalogues data.gouv.fr
 * @author Marc Le Bihan
 */
public interface CatalogueApi extends Serializable {
   /**
    * Charger le catalogue datagouv.fr
    */
   void chargerCatalogueDataGouvFr();

   /**
    * Enumérer les valeurs distinctes d'un champ du catalogue datagouv.<br>
    * "frequence_catalogue", "licence_catalogue", "granularite_spatiale_catalogue", "zones_spatiales_catalogue", "tags_catalogue"<br>
    * ont des valeurs énumérées.
    * @param champ nom du champ dont ont veut l'énumération des valeurs distinctes
    * @return Map de valeurs que peut prendre ce champ et le nombre d'occurrences qu'on lui a trouvées.<br>
    *    - les valeurs sont converties en chaînes de caractères
    *    - les valeurs sont classés par nombre d'occurences décroissantes.
    */
   Map<String, Long> enumererValeursDistinctes(String champ);
}
