package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise.activite;


import java.io.Serial;
import java.text.*;
import java.util.*;

import org.apache.commons.lang3.*;

import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;
import org.apache.spark.sql.api.java.*;
import org.apache.spark.sql.types.*;
import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import fr.ecoemploi.adapters.outbound.port.entreprise.ActiviteCommuneRepository;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.OptionsCreationLecture;
import fr.ecoemploi.adapters.outbound.spark.dataset.entreprise.*;
import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.entreprise.*;
import fr.ecoemploi.domain.model.territoire.entreprise.ape.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;

/**
 * Dataset d'activité de commune.
 * @author Marc Le Bihan
 */
@Component
public class ActiviteCommuneDataset extends AbstractSparkDataset implements ActiviteCommuneRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3924165897266574507L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ActiviteCommuneDataset.class);

   /** Dataset d'entreprises. */
   @Autowired
   private transient EntrepriseDataset entrepriseDataset; // FIXME Il est anormal qu'EntrepriseDataset n'arrive pas à se sérialiser quand il est dans un autre dataset

   /** Nomenclature NAF. */
   @Autowired
   private NAFDataset nafDataset;

   /**
    * Obtenir les activités d'une commune.
    * @param optionsCreationLecture Options de création et de lecture d'un dataset.
    * @param anneeCOG    Année du COG.
    * @param anneeSIRENE Année SIRENE des entreprises à consulter.
    * @param codeCommune Code commune.
    * @return Activités de la commune.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public List<ActiviteCommune> obtenirActivitesCommune(OptionsCreationLecture optionsCreationLecture, int anneeCOG, int anneeSIRENE, CodeCommune codeCommune) {
      Objects.requireNonNull(codeCommune, "Le code Commune ne peut pas valoir null");

      Dataset<Row> entreprises = this.entrepriseDataset.rowEntreprisesEtEtablissements(optionsCreationLecture, null, new EntrepriseTriDepartementCommuneSirenSiret(),
            anneeCOG, anneeSIRENE, true, true, true)
         .where(col("codeDepartement").equalTo(codeCommune.numeroDepartement())
            .and(col("codeCommune").equalTo(codeCommune.departementEtCommune())));

      return activitesParCommune(entreprises).collectAsList();
   }

   /**
    * Obtenir les activités d'une commune.
    * @param anneeCOG    Année du COG.
    * @param anneeSIRENE Année SIRENE des entreprises à consulter.
    * @param codeCommune Code commune.
    * @return Activités de la commune.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public List<ActiviteCommune> obtenirActivitesCommune(int anneeCOG, int anneeSIRENE, CodeCommune codeCommune) {
      return obtenirActivitesCommune(optionsCreationLecture(), anneeCOG, anneeSIRENE, codeCommune);
   }

   /**
    * Recenser les activités par commune.
    * @param rowEntreprisesEtablissements Dataset d'entreprises et établissements joints.
    * @return Dataset d'activités de communes.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<ActiviteCommune> activitesParCommune(Dataset<Row> rowEntreprisesEtablissements) {
      Objects.requireNonNull(rowEntreprisesEtablissements, "Le Dataset en entrée ne peut pas valoir null.");

      // Ajouter les champs Section, NomEntreprise, NomEtablissement s'ils ne sont pas encore présents.
      ajoutChampSiAbsent(rowEntreprisesEtablissements, new StructField("section", StringType, false, null));
      ajoutChampSiAbsent(rowEntreprisesEtablissements, new StructField("nomEntreprise", StringType, false, null));
      ajoutChampSiAbsent(rowEntreprisesEtablissements, new StructField("nomEtablissement", StringType, false, null));

      return rowEntreprisesEtablissements
         .map((MapFunction<Row, ActiviteCommune>) row -> {
            try {
               return toActiviteCommune(row);
            } catch (RuntimeException e) {
               String format = "Un row durant le mapDetailActivites(..) a provoqué un incident : {0}. Contenu du row : \n{1}";
               String message = MessageFormat.format(format, e.getMessage(), row.mkString());
               RuntimeException ex = new RuntimeException(message, e);

               LOGGER.error(ex.getMessage(), ex);
               throw ex;
            }
         }, Encoders.bean(ActiviteCommune.class));
   }

   /**
    * Exporter les activités des communes en CSV.
    * @param optionsCreationLecture Options de création et de lecture d'un dataset.
    * @param anneeCOG    Année du COG.
    * @param anneeSIRENE Année SIRENE de la base entreprise.
    * @param codeEPCI    Code EPCI de filrage, si non null.
    * @param codeCommune Code Commune de filrage, si non null.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public void exporterActivitesCommuneCSV(OptionsCreationLecture optionsCreationLecture, int anneeCOG, int anneeSIRENE, String codeEPCI, String codeCommune) {
      Dataset<Row> entreprises = this.entrepriseDataset.rowEntreprisesEtEtablissements(optionsCreationLecture, null, new EntrepriseTriDepartementCommuneSirenSiret(),
         anneeCOG, anneeSIRENE, true, true, true);

      // Des filtres peuvent avoir été demandés sur l'EPCI ou la commune.
      entreprises = StringUtils.isNotBlank(codeEPCI) ? entreprises.filter(functions.col("codeEPCI").equalTo(codeEPCI)) : entreprises;
      entreprises = StringUtils.isNotBlank(codeCommune) ? entreprises.filter(functions.col("codeCommune").equalTo(codeCommune)) : entreprises;

      Dataset<ActiviteCommune> activites = activitesParCommune(entreprises);

      Dataset<Row> regroupementNiveau5 = regroupementActivites(entreprises, anneeSIRENE, 5, true);
      Dataset<Row> regroupementNiveau4 = regroupementActivites(entreprises, anneeSIRENE, 4, true);
      Dataset<Row> regroupementNiveau3 = regroupementActivites(entreprises, anneeSIRENE, 3, true);
      Dataset<Row> regroupementNiveau2 = regroupementActivites(entreprises, anneeSIRENE, 2, true);
      Dataset<Row> regroupementNiveau1 = regroupementActivites(entreprises, anneeSIRENE, 1, true);

      exportCSV().exporterCSV(activites.toDF(), "etablissements_detail_simplifie_avec_effectifs_", true);
      exportCSV().exporterCSV(regroupementNiveau5, "etablissements_regroupes_naf5_", true);
      exportCSV().exporterCSV(regroupementNiveau4, "etablissements_regroupes_naf4_", true);
      exportCSV().exporterCSV(regroupementNiveau3, "etablissements_regroupes_naf3_", true);
      exportCSV().exporterCSV(regroupementNiveau2, "etablissements_regroupes_naf2_", true);
      exportCSV().exporterCSV(regroupementNiveau1, "etablissements_regroupes_naf1_", true);
      exportCSV().exporterCSV(entreprises.drop("etablissements"), "etablissements_donnees_brutes_", true);
   }

   /**
    * Exporter les activités des communes en CSV.
    * @param anneeCOG    Année du COG.
    * @param anneeSIRENE Année SIRENE de la base entreprise.
    * @param codeEPCI    Code EPCI de filrage, si non null.
    * @param codeCommune Code Commune de filrage, si non null.
    */
   public void exporterActivitesCommuneCSV(int anneeCOG, int anneeSIRENE, String codeEPCI, String codeCommune) {
      exporterActivitesCommuneCSV(optionsCreationLecture(), anneeCOG, anneeSIRENE, codeEPCI, codeCommune);
   }

   /**
    * Recenser les activités par commune.
    * @param entreprisesEtablissements Dataset de jointure entreprises et établissements.
    * @param anneeSirene Année de la base Sirene à considérer.
    * @param niveau NAF pour le regroupement.
    * @param orderBySalarieDesc true s'il faut trier par salariés décroissants.
    * @return Dataset d'activités de communes.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> regroupementActivites(Dataset<Row> entreprisesEtablissements, int anneeSirene, int niveau, boolean orderBySalarieDesc) {
      Dataset<ActiviteCommune> activite = activitesParCommune(entreprisesEtablissements);

      // Déterminer le champ de regroupement.
      Column colCodeDepartement = activite.col("codeDepartement");
      Column colEPCI = activite.col("codeEPCI");
      Column colCodeCommune = activite.col("codeCommune");
      Column colNom = activite.col("nomCommune");
      Column colAPE = activite.col("ape");
      Column colSection = activite.col("section");
      Column colSiren = activite.col("siren");
      Column colNombreSalaries = activite.col("nombreSalaries");
      Column colNombreActifs = activite.col("nombreActifs");

      Column colonneRegroupement;
      Dataset<Row> rowActivitesParCommune = activite.toDF();

      if (niveau == 5) {
         colonneRegroupement = colAPE.as("regroupement");
      }
      else {
         // Si ce n'est pas le code APE complet sur lequel l'on regroupe, le libellé APE n'est plus le même. Il faut le rechercher.
         colonneRegroupement = colAPE.substr(1, NiveauxNAF.getLongueurChaineDiscriminante(niveau)).as("regroupement");

         rowActivitesParCommune = rowActivitesParCommune.drop("libelleAPE");

         Dataset<Row> nomenclatureNAF = this.nafDataset.rowNomenclatureNAF(anneeSirene);
         rowActivitesParCommune = rowActivitesParCommune.join(nomenclatureNAF, colAPE.substr(1, NiveauxNAF.getLongueurChaineDiscriminante(niveau)).equalTo(nomenclatureNAF.col("codeNAF")) , "left_outer");
         rowActivitesParCommune = rowActivitesParCommune.drop("codeNAF");
         rowActivitesParCommune = rowActivitesParCommune.drop("niveauNAF");
         rowActivitesParCommune = rowActivitesParCommune.withColumnRenamed("libelleNAF", "libelleAPE");
      }

      RelationalGroupedDataset group;
      Column colLibelleAPE = rowActivitesParCommune.col("libelleAPE");

      if (niveau != 1) {
         group = rowActivitesParCommune.select(colCodeDepartement, colEPCI, colCodeCommune, colNom,
               colonneRegroupement, colSection, colLibelleAPE, colSiren, colNombreSalaries, colNombreActifs)
            .groupBy(colCodeDepartement, colEPCI, colCodeCommune, colNom, col("regroupement"), colSection, colLibelleAPE);
      }
      else {
         group = rowActivitesParCommune.select(colCodeDepartement, colEPCI, colCodeCommune, colNom,
               colSection, colLibelleAPE, colSiren, colNombreSalaries, colNombreActifs)
            .groupBy(colCodeDepartement, colEPCI, colCodeCommune, colNom, colSection, colLibelleAPE);
      }


      Dataset<Row> activitesCommune = group.agg(countDistinct(colSiren).as("nombreEntreprises"), sum(colNombreSalaries).as("nombreSalaries"), sum(colNombreActifs).as("nombreActifs"));

      colCodeCommune = activitesCommune.col("codeCommune");
      colNombreSalaries = activitesCommune.col("nombreSalaries");
      //colNombreActifs = activitesCommune.col("nombreActifs");

      if (orderBySalarieDesc) {
         activitesCommune = activitesCommune.orderBy(colCodeCommune, colNombreSalaries.desc());
      }

      return activitesCommune;
   }

   /**
    * Calculer la moyenne des salariées par activité, pour les établissements ayant des tranches d'effectifs alimentés.
    * @param dsEtablissements Dataset d'établissements.
    * @return Dataset de (regroupement activite, cumul des salariés, nombre d'établissements).
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> salariesActivite(Dataset<Row> dsEtablissements) {
      // UDF : Evaluer le nombre de salariés présents dans l'établissement. trancheEffectifSalarie -> nombreSalariés
      this.session.udf().register("nombreSalariesUDF", (UDF4<String, String, Boolean, Boolean, Double>)(trancheEffectifSalarie, categorieJuridique, caractereEmployeur, compterCommeActifs)
         -> evaluerNombreSalaries(categorieJuridique, trancheEffectifSalarie, caractereEmployeur, compterCommeActifs), DataTypes.DoubleType);

      // Les statistiques ne se font que sur les tranches alimentées.
      Dataset<Row> ds = dsEtablissements.filter("trancheEffectifSalarie is not NULL or categorieJuridique = \"1000\"");
      ds = ds.withColumn("nombreSalaries", callUDF("nombreSalariesUDF", col("trancheEffectifSalarie"), col("categorieJuridique"), col("caractereEmployeur"), lit(false)));
      ds = ds.withColumn("nombreActifs", callUDF("nombreSalariesUDF", col("trancheEffectifSalarie"), col("categorieJuridique"), col("caractereEmployeur"), lit(true)));

      Column colAPE = ds.col("activitePrincipale");
      Column colNombreSalaries = ds.col("nombreSalaries");
      RelationalGroupedDataset group = ds.select(colAPE, colNombreSalaries).groupBy(colAPE);

      ds = group.agg(sum(colNombreSalaries).as("nombreSalaries"), count("*").as("nombreEtablissements"));
      return ds;
   }

   /**
    * Déterminer si une estimation INSEE est fournie.
    * @param categorieJuridique Catégorie juridique de l'entreprise.
    * @param trancheEffectifSalarie Tranche d'effectif salarié.
    * @param caractereEmployeur Caractère employeur de l'établissement.
    * @return true si elle a un caractère employeur et une tranche d'effectif, ou bien qu'elle n'en a pas mais qu'elle est une entreprise individuelle.
    */
   public boolean estimationFournie(String categorieJuridique, String trancheEffectifSalarie, Boolean caractereEmployeur) {
      // Si l'établissement n'a pas de caractère employeur, elle est considéré comme fournie.
      if (caractereEmployeur != null && caractereEmployeur == false) {
         return true;
      }

      // Si le caractère employeur n'est pas transmis ou qu'il l'est mais la tranche non alimentée
      if (trancheEffectifSalarie == null) {
         // On considère qu'elle ne l'est pas, sauf s'il s'agit d'une entreprise individuelle (car alors on peut fixer le nombre de salariés (le patron) à 1).
         return "1000".equals(categorieJuridique);
      }

      // Dans tous les autres cas de figure, elle est dite fournie car la tranche d'effectif est non nulle.
      return true;
   }

   /**
    * Constituer un objet Activité de commune à partir d'un Row.
    * @param row Row.
    * @return Activité de commune
    * @throws NiveauNAFInexistantException si le code NAF n'a pas été trouvé.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   private ActiviteCommune toActiviteCommune(Row row) throws NiveauNAFInexistantException {
      // Déterminer la section associée au code APE.
      String siren = row.getAs("siren");
      String siret = row.getAs("siret");
      String codeAPE = row.getAs("activitePrincipale");
      String libelleAPE = row.getAs("libelleNAF");
      String section = CodeAPE.toSection(codeAPE);
      String nomEntreprise = construireNomEntreprise(row);
      String enseigne1 = row.getAs("enseigne1");

      String codeCommune = row.getAs("codeCommune");
      String nomCommune = row.getAs("nomCommune");
      String codeDepartement = row.getAs("codeDepartement");
      Integer strateCommune = row.getAs("strateCommune");
      String nomEtablissement = row.getAs("denominationEtablissement");
      String categorieJuridique = row.getAs("categorieJuridique");
      String libelleCategorieJuridique = row.getAs("libelleCategorieJuridique");

      String categorieEntreprise = row.getAs("categorieEntreprise");

      nomEtablissement = StringUtils.isBlank(nomEtablissement) ? enseigne1 : nomEtablissement;
      String epci = row.getAs("codeEPCI");

      // Evaluer le nombre de salariés présents dans l'établissement.
      Boolean coreCaractereEmployeur = row.getAs("caractereEmployeur");
      String trancheEffectifSalarie = row.getAs("trancheEffectifSalarie");
      boolean estimationFournieParINSEE = estimationFournie(categorieJuridique, trancheEffectifSalarie, coreCaractereEmployeur);
      boolean caractereEmployeur = coreCaractereEmployeur != null ? coreCaractereEmployeur : false;

      if (coreCaractereEmployeur == null) {
         LOGGER.warn("On ne sait pas si l'établissement {} - {} (entreprise {}) de la commune {} - {} de code APE {} et catégorie juridique {} - {} est employeur ou non. Estimation des salariées possible : {}.",
            siret, nomEtablissement == null ? "<sans nom>" : nomEtablissement, nomEntreprise, codeCommune, nomCommune,
            codeAPE, categorieJuridique, libelleCategorieJuridique, estimationFournieParINSEE);
      }
      else {
         if (estimationFournieParINSEE == false) {
            LOGGER.warn("L'établissement {} - {} (entreprise {}) de la commune {} - {} de code APE {} et catégorie juridique {} - {} est employeur mais n'a pas de tranche d'effecif associé.",
               siret, nomEtablissement == null ? "<sans nom>" : nomEtablissement, nomEntreprise, codeCommune, nomCommune,
               codeAPE, categorieJuridique, libelleCategorieJuridique);
         }
      }

      double nombreSalaries = evaluerNombreSalaries(categorieJuridique, trancheEffectifSalarie, caractereEmployeur, false);
      double nombreActifs = evaluerNombreSalaries(categorieJuridique, trancheEffectifSalarie, caractereEmployeur, true);

      String categorieEtablissement = categorieEtablissement(caractereEmployeur, categorieJuridique, nombreSalaries);

      return new ActiviteCommune(codeCommune, codeDepartement, epci, nomCommune, strateCommune,
         codeAPE, section, 1, nombreSalaries, nombreActifs,
         siren, nomEntreprise, siret, nomEtablissement, categorieJuridique,
         libelleAPE, estimationFournieParINSEE, libelleCategorieJuridique, categorieEntreprise, categorieEtablissement);
   }

   /**
    * Evaluer le nombre de salariés ou d'actifs.
    * @param categorieJuridique Catégorie juridique de l'entreprise.
    * @param trancheEffectifSalarie Tranche d'effectif salarié.
    * @param caractereEmployeur Caractère employeur de l'établissement.
    * @param compterCommeActifs true s'il faut compter un établissement non employeur ou à zéro salarié comme ayant un actif (son dirigeant)<br>
    * false s'il faut rapporter strictement le nombre de salariés, et alors il n'y en a aucun.
    * @return Nombre de salariés.
    */
   private double evaluerNombreSalaries(String categorieJuridique, String trancheEffectifSalarie, Boolean caractereEmployeur, boolean compterCommeActifs) {
      // Si l'établissement n'est pas employeur, on compte son détenteur comme seul salarié.
      if (caractereEmployeur == null || caractereEmployeur == false) {
         return compterCommeActifs ? 1.0 : 0.0;
      }

      // Si une estimation est fournie par l'INSEE, on prend sa valeur centrale.
      if (trancheEffectifSalarie != null) {
         TrancheEffectif tranche = TrancheEffectif.valueFromCode(trancheEffectifSalarie);

         if (tranche != null) {
            return tranche.getMoyenne(compterCommeActifs);
         }
      }
      
      // Si la nature juridique est celle d'une entreprise individuelle, le nombre d'actif est fixé à 1 avec certitude.
      if (compterCommeActifs) {
         if ("1000".equals(categorieJuridique)) {
            return 1.0;
         }
      }
      
      // Sinon, par défaut, l'on peut le fixe à 1.0 si l'on compte en actifs, 0 commme salariés.
      return compterCommeActifs ? 1.0 : 0.0;
   }
   
   /**
    * Construire le nom de l'entreprise.
    * @param row Etablissement de l'entreprise observé.
    * @return Nom de l'entreprise.
    */
   private String construireNomEntreprise(Row row) {
      String nomEntreprise = row.getAs("denominationEntreprise");
      
      // Si l'entreprise ne porte pas de nom, l'établir à partir du patronyme de la personne physique.
      if (StringUtils.isBlank(nomEntreprise)) {
         String prenom = row.getAs("prenomUsuel");
         String nomMarital = row.getAs("nomUsage");
         String nomNaissance = row.getAs("nomNaissance");
         
         StringBuilder nomConstruit = new StringBuilder();
         
         if (StringUtils.isNotBlank(prenom)) {
            nomConstruit.append(prenom).append(" ");
         }

         if (StringUtils.isNotBlank(nomMarital)) {
            nomConstruit.append(nomMarital);
         }
         else {
            if (StringUtils.isNotBlank(nomNaissance)) {
               nomConstruit.append(nomNaissance);
            }
         }
         
         nomEntreprise = nomConstruit.toString();
      }
      
      return nomEntreprise;
   }

   /**
    * Retourne la catégorie d'établissement.
    * @param caractereEmployeur Caractère employeur (null : inconnu).
    * @param categorieJuridique Catégorie juridique de l'établissement.
    * @param nombreSalaries Nombre de salariés estimés.
    * @return EI : Entreprise individuelle<br>
    * NE : Etablissement non employeur<br>
    * TPE : Très petite entreprise (moins de 10 salariés)<br>
    * PE : Petite entreprise (moins de 50 salariés)<br>
    * ME : Moyenne entreprise (moins de 250 salariés)<br>
    * ETI : Entreprise de taille intermédiaire (moins de 5000 salariés)<br>
    * GE : Grande entrprise
    */
   private String categorieEtablissement(Boolean caractereEmployeur, String categorieJuridique, double nombreSalaries) {
      if ("1000".equals(categorieJuridique)) {
         return "EI";
      }
      
      if (caractereEmployeur == null || caractereEmployeur == false) {
         return "NE";
      }
      
      if (nombreSalaries < 10) {
         return "TPE";
      }
      
      if (nombreSalaries < 50) {
         return "PE";
      }
      
      if (nombreSalaries < 250) {
         return "ME";
      }
      
      if (nombreSalaries < 5000) {
         return "ETI";
      }

      return "GE";
   }
   
   /**
    * Ajouter un champ dans le dataset s'il ne s'y trouve pas encore.
    * @param ds Dataset.
    * @param champ Champ.
    */
   private void ajoutChampSiAbsent(Dataset<Row> ds, StructField champ) {
      // Rajouter le champ Section s'il n'est pas encore présent. 
      boolean champSectionPresent = Arrays.stream(ds.columns()).anyMatch(c -> c.equals(champ.name()));
      
      if (champSectionPresent == false) {
         ds.schema().add(champ);
      }
   }
}
