package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.*;
import static org.apache.spark.sql.types.DataTypes.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.domain.utils.objets.TechniqueException;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

/**
 * Dataset des catégories juridiques.
 * @author Marc Le Bihan
 */
@Service
public class CategorieJuridiqueDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 6427899968002647184L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CategorieJuridiqueDataset.class);

   /** Nom du fichier CSV. */
   private final String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   private final String repertoireFichier;

   /**
    * Construire un dataset de lecture des catégories juridiques.
    * @param fichier Nom du fichier des catégories juridiques.
    * @param repertoire Répertoire du fichier.
    */
   @Autowired
   public CategorieJuridiqueDataset(@Value("${categories_juridique.csv.nom}") String fichier, 
      @Value("${sirene.dir}") String repertoire) {
      this.repertoireFichier = repertoire;
      this.nomFichier = fichier;
   }
   
   /**
    * Obtenir un Dataset Row des catégories juridiques.
    * @param session Session Spark.
    * @param annee Année du référentiel des catégories juridiques.
    * @return Dataset des catégories juridiques.
    * @throws TechniqueException si un incident survient.
    */
   public Dataset<Row> rowCategoriesJuridiques(SparkSession session, int annee) throws TechniqueException {
      LOGGER.info("Acquisition des catégories juridiques de {}...", annee);

      File repertoireParent = assertExistenceRepertoire(this.repertoireFichier);
      File source = assertExistenceFichierAnnuel(repertoireParent, annee, this.nomFichier);
      
      return session.read().schema(schema()).format("csv").option("encoding", "UTF-8")
         .option("header","true")
         .option("delimiter", ",")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath()).orderBy("codeCategorieJuridique")
         .selectExpr("*")
         .cache();
   }
   
   /**
    * Renvoyer le schema.
    * @return schema.
    */
   public StructType schema() {
      /*
       * codeCategorieJuridique,libelleCategorieJuridique
       * 1000,Entrepreneur individuel
       * 2110,Indivision entre personnes physiques 
       * 2120,Indivision avec personne morale 
       * 2210,Société créée de fait entre personnes physiques 
       */
      StructType schema = new StructType();
      schema = schema.add("codeCategorieJuridique", StringType, false) // Code de la catéogorie juridique
         .add("libelleCategorieJuridique", StringType, true);          // Libellé de lma catégorie juridique

      return schema;
   }
}
