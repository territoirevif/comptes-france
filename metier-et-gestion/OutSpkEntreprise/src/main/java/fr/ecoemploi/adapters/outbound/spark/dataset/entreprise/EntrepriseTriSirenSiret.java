package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;
import static org.apache.spark.sql.functions.col;

/**
 * Tri d'établissement par siren, siret.
 * @author Marc Le Bihan
 */
public class EntrepriseTriSirenSiret extends EntrepriseTri {
   @Serial
   private static final long serialVersionUID = 4964366170295345968L;

   /** Colonne de partition par code siren */
   private static final String COL_PARTITION_SIREN = "partitionSiren";

   /**
    * Création d'un tri d'établissement.
    */
   public EntrepriseTriSirenSiret() {
      super("PARTITION-substr(siren,0,2)-TRI-siren-siret", true, COL_PARTITION_SIREN, false, COL_PARTITION_SIREN, SIREN_ENTREPRISE.champ(), SIRET_ETABLISSEMENT.champ());
   }

   /**
    * Renvoyer une condition sur Siren, Siret.
    * @param siren Code Siren à joindre. Si null, isNull() servira au test.
    * @param siret Code Siret à joindre. Si null, isNull() servira au test.
    * @return Condition.
    */
   public Column equalTo(String siren, String siret) {
      Column condition = equalTo(siren);
      return condition.and(siret != null ? SIRET_ETABLISSEMENT.col().equalTo(siret) : SIRET_ETABLISSEMENT.col().isNull());
   }

   /**
    * Renvoyer une condition sur Siren.
    * @param siren Code Siren à joindre. Si null, isNull() servira au test.
    * @return Condition.
    */
   public Column equalTo(String siren) {
      String partitionSiren = siren != null ? siren.substring(0, 2) : null;

      Column condition = partitionSiren != null ? col(COL_PARTITION_SIREN).equalTo(partitionSiren) : col(COL_PARTITION_SIREN).isNull();
      return condition.and(siren != null ? SIREN_ENTREPRISE.col().equalTo(siren) : SIREN_ENTREPRISE.col().isNull());
   }
}
