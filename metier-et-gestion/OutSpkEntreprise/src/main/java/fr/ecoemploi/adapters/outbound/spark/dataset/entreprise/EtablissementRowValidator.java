package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.*;
import java.util.*;
import java.util.function.BooleanSupplier;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.*;
import org.springframework.stereotype.Component;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.types.DataTypes.*;

import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.model.territoire.entreprise.ape.CodeAPE;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.validation.AbstractRowValidator;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Validateur d'enregistrement d'établissement.
 * @author Marc Le Bihan
 */
@Component
public class EtablissementRowValidator extends AbstractRowValidator implements ApplicationContextAware {
   /** Serial UID. */
   @Serial
   private static final long serialVersionUID = -1177387960065156426L;

   /** Etablissement sans code SIRET */
   public static final String VLD_SANS_SIRET = "ETAB_SANS_SIRET";

   /** Code SIRET invalide. */
   public static final String VLD_SIRET_INV = "ETAB_SIRET_INVALIDE";

   /** Code SIREN d'entreprise de l'établissement absent. */
   public static final String VLD_SANS_SIREN_ENT = "ETAB_SANS_SIREN_ENT";

   /** Code SIREN d'entreprise de l'établissement invalide. */
   public static final String VLD_SIREN_ENT_INV = "ETAB_SIREN_ENT_INVALIDE";

   /** Pas de nomenclature APE */
   public static final String VLD_SANS_NOMEN_APE = "ETAB_SANS_NOMEN_APE";

   /** Nomenclature APE trop ancienne */
   public static final String VLD_NOMEN_APE_ANCIEN = "ETAB_NOMEN_APE_ANCIEN";

   /** Etablissement sans code APE */
   public static final String VLD_SANS_APE = "ETAB_SANS_APE";

   /** Code APE invalide */
   public static final String VLD_APE_INV = "ETAB_APE_INVALIDE";

   /** Type de voie principale invalide */
   public static final String VLD_TYPE_VOIE_PRINC_INV = "ETAB_TYPE_VOIE_PRINC_INVALIDE";

   /** Type de voie secondaire invalide */
   public static final String VLD_TYPE_VOIE_SEC_INV = "ETAB_TYPE_VOIE_SEC_INVALIDE";

   /** Date de création invalide */
   public static final String VLD_DATE_CREATION_INV = "ETAB_DATE_CREATION_INVALIDE";

   /** Date de traitement invalide */
   public static final String VLD_DATE_TRAITEMENT_INV = "ETAB_DATE_TRAITEMENT_INVALIDE";

   /** Date d'historisation invalide */
   public static final String VLD_DATE_HISTO_INV = "ETAB_DATE_HISTORISATION_INVALIDE";

   /**
    * Validation d'un établissement.
    * @param etablissement Etablissement, sous forme Row.
    * @param actifsSeulement true s'il faut filtrer les seuls établissements actifs.
    * @param nomenclaturesNAF2Valides true s'il faut restreindre aux établissements dont les codes NAF sont des NAF2 et valides.
    * @param indexs Index des champs.
    * @param historique Historique d'exécution. Peut valoir null.
    * @return true si l'établissement est valide.
    */
   public boolean validerEtablissement(HistoriqueExecution historique, Row etablissement, boolean actifsSeulement, boolean nomenclaturesNAF2Valides, Map<String, Integer> indexs) {
      // Restreindre aux seuls établissements actifs, si souhaité.
      if (actifsSeulement && (Boolean.FALSE.equals(etablissement.getAs("active")))) {
         return false;
      }

      String siret = getString(indexs, etablissement, SIRET_ETABLISSEMENT.champ());
      String siren = getString(indexs, etablissement, SIREN_ENTREPRISE.champ());
      Boolean diffusable = getBoolean(indexs, etablissement, "diffusable");
      String nomenclatureAPE = getString(indexs, etablissement, "nomenclatureActivitePrincipale");
      String code = getString(indexs, etablissement, "activitePrincipale");
      String typeVoie = getString(indexs, etablissement, TYPE_DE_VOIE.champ());
      String typeVoieSecondaire = getString(indexs, etablissement, TYPE_DE_VOIE_SECONDAIRE.champ());
      String dateCreation = getString(indexs, etablissement, DATE_CREATION_ETABLISSEMENT.champ());
      String dateDernierTraitement = getString(indexs, etablissement, "dateDernierTraitement");
      String dateDebutHistorisation = getString(indexs, etablissement, "dateDebutHistorisation");
      String nomPaysEtranger = getString(indexs, etablissement, "nomPaysEtranger");

      List<BooleanSupplier> validations = new ArrayList<>();

      // Vérifier que le SIRET de l'établissement est correct.
      validations.add(() -> valideSi(VLD_SANS_SIRET, historique, etablissement, e -> siret != null, () -> new Serializable[]{nomPaysEtranger}));
      validations.add(() -> valideSi(VLD_SIRET_INV, historique, etablissement, e -> new SIRET(siret).valide(), () -> new Serializable[]{siret}));

      // Vérifier que le SIREN de l'entreprise mentionnée par l'établissement est correct.
      validations.add(() -> valideSi(VLD_SANS_SIREN_ENT, historique, etablissement, e -> siren != null, () -> new Serializable[]{nomPaysEtranger, siret}));
      validations.add(() -> valideSi(VLD_SIREN_ENT_INV, historique, etablissement, e -> new SIREN(siren).valide(), () -> new Serializable[]{siren}));

      // Si le type de voie des adresses sont alimentés, vérifier qu'ils sont valides.
      validations.add(() -> invalideSi(VLD_TYPE_VOIE_PRINC_INV, historique, etablissement, e -> typeDeVoieInvalide(typeVoie, diffusable), () -> new Serializable[]{siret, typeVoie}));
      validations.add(() -> invalideSi(VLD_TYPE_VOIE_SEC_INV, historique, etablissement, e -> typeDeVoieInvalide(typeVoieSecondaire, diffusable), () -> new Serializable[]{siret, typeVoieSecondaire}));

      // Contrôler le format des dates
      validations.add(() -> invalideSi(VLD_DATE_CREATION_INV, historique, etablissement, e -> dateInvalide(dateCreation) != null, () -> new Serializable[]{siret, dateCreation}));
      validations.add(() -> invalideSi(VLD_DATE_TRAITEMENT_INV, historique, etablissement, e -> dateTimeInvalide(dateDernierTraitement) != null && dateInvalide(dateDernierTraitement) != null, () -> new Serializable[]{siret, dateDernierTraitement, dateInvalide(dateDernierTraitement)}));
      validations.add(() -> invalideSi(VLD_DATE_HISTO_INV, historique, etablissement, e -> dateInvalide(dateDebutHistorisation) != null, () -> new Serializable[]{siret, dateDebutHistorisation, dateInvalide(dateDebutHistorisation)}));

      if (nomenclaturesNAF2Valides) {
         // La nomenclature du code APE doit être NAFRev2.
         validations.add(() -> valideSi(VLD_SANS_NOMEN_APE, historique, etablissement, e -> nomenclatureAPE != null, () -> new Serializable[]{siret}));
         validations.add(() -> valideSi(VLD_NOMEN_APE_ANCIEN, historique, etablissement, e -> "NAFRev2".equals(nomenclatureAPE), () -> new Serializable[]{siret, nomenclatureAPE}));

         // Le code APE doit être valide.
         validations.add(() -> valideSi(VLD_SANS_APE, historique, etablissement, e -> code != null, () -> new Serializable[]{siret}));
         validations.add(() -> valideSi(VLD_APE_INV, historique, etablissement, e -> new CodeAPE(code).valide(false), () -> new Serializable[]{siret, code}));
      }

      return validations.stream().allMatch(BooleanSupplier::getAsBoolean);
   }

   /**
    * Une fois le contenu du dataset validé, certains de ses champs peuvent être convertis en leurs types finaux.
    * @param dataset Dataset à convertir.
    * @return Dataset converti.
    */
   protected Dataset<Row> cast(Dataset<Row> dataset) {
      return dataset.withColumn(DATE_CREATION_ETABLISSEMENT.champ(), DATE_CREATION_ETABLISSEMENT.col().cast(DateType))
         .withColumn("anneeValiditeEffectifSalarie", col("anneeEffectifsEtablissement").cast(IntegerType))
         .withColumn("nombrePeriodes", col("nombrePeriodesEtablissement").cast(IntegerType))
         .drop("anneeEffectifsEtablissement", "nombrePeriodesEtablissement");
   }

   /**
    * Déterminer si un type de voie est invalide.
    * @param typeVoie Type de voie.
    * @param diffusable Statut de diffusion de l'établissement : peut être 'O' ou 'P'.<br>
    *     s'il vaut "P", ses champs d'adresse et de propriétaire ne seront pas renseignés.
    * @return true s'il est invalide.
    */
   private boolean typeDeVoieInvalide(String typeVoie, Boolean diffusable) {
      // Si l'établissement n'est pas diffusable, sa voie est valide.
      if (diffusable == null || Boolean.FALSE.equals(diffusable)) {
         return false;
      }

      try {
         TypeDeVoie.valueFromAbreviation(typeVoie);
         return false;
      }
      catch(@SuppressWarnings("unused") NoSuchElementException ex) {
         return true;
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
      declareRegle(VLD_SANS_SIRET, this.messageSource, "Etablissement sans code SIRET (possiblement un établissement étranger)", String.class);
      declareRegle(VLD_SIRET_INV, this.messageSource, "Etablissement au SIRET invalide", String.class);
      declareRegle(VLD_SANS_SIREN_ENT, this.messageSource, "Etablissement sans SIREN d'entreprise (possiblement un établissement étranger)", String.class, String.class);
      declareRegle(VLD_SIREN_ENT_INV, this.messageSource, "Etablissement au siren d'entreprise invalide", String.class);
      declareRegle(VLD_SANS_NOMEN_APE, this.messageSource, "Etablissement sans nomenclature APE", String.class);

      declareRegle(VLD_NOMEN_APE_ANCIEN, this.messageSource, "Etablissement dont la nomenclature APE n'est plus soutenue", String.class, String.class);
      declareRegle(VLD_SANS_APE, this.messageSource, "Etablissement sans code APE", String.class);
      declareRegle(VLD_APE_INV, this.messageSource, "Etablissement au code APE invalide", String.class, String.class);
      declareRegle(VLD_TYPE_VOIE_PRINC_INV, this.messageSource, "Etablissement dont le type de voie de l'adresse principale est invalide", String.class, String.class);
      declareRegle(VLD_TYPE_VOIE_SEC_INV, this.messageSource, "Etablissement dont le type de voie de l'adresse secondaire est invalide", String.class, String.class);

      declareRegle(VLD_DATE_CREATION_INV, this.messageSource, "Etablissement dont la date de création est invalide", String.class, String.class);
      declareRegle(VLD_DATE_TRAITEMENT_INV, this.messageSource, "Etablissement dont la date de dernier traitement est invalide", String.class, String.class, String.class);
      declareRegle(VLD_DATE_HISTO_INV, this.messageSource, "Etablissement dont la date d'historisation est invalide", String.class, String.class, String.class);
   }
}
