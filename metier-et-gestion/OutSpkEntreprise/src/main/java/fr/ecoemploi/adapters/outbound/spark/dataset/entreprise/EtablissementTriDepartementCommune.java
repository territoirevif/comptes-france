package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Partitionnement d'établissement par code département<br>
 * Tri par code département, code Commune.
 * @author Marc Le Bihan
 */
public class EtablissementTriDepartementCommune extends EtablissementTri {
   @Serial
   private static final long serialVersionUID = -395634009943479694L;

   /**
    * Création d'un tri d'établissement.
    */
   public EtablissementTriDepartementCommune() {
      super("PARTITION-departement-TRI-commune-siren-siret", true,
         CODE_DEPARTEMENT.champ(), false, CODE_DEPARTEMENT.champ(), CODE_COMMUNE.champ(), SIRET_ETABLISSEMENT.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec isNull
    * @param codeCommune Code commune, si null sera testé avec isNull
    * @param siren SIREN de l'entreprise.
    * @param siret SIRET de l'établissement.
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommune, String siren, String siret) {
      Column conditionSirenSiret = (siren != null ? SIREN_ENTREPRISE.col().equalTo(siren) : SIREN_ENTREPRISE.col().isNull())
         .and(siret != null ? SIRET_ETABLISSEMENT.col().equalTo(siret) : SIRET_ETABLISSEMENT.col().isNull());

      return equalTo(codeDepartement, codeCommune)
         .and(conditionSirenSiret);
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec isNull
    * @param codeCommune Code commune, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommune) {
      return equalTo(codeDepartement)
         .and(codeCommune != null ? CODE_COMMUNE.col().equalTo(codeCommune) : CODE_COMMUNE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
