package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Partitionnement d'établissement par code département<br>
 * Tri par code département, siren, siret.
 * @author Marc Le Bihan
 */
public class
EtablissementTriDepartementSirenSiret extends EtablissementTri {
   @Serial
   private static final long serialVersionUID = -395634009943479694L;

   /**
    * Création d'un tri d'établissement.
    */
   public EtablissementTriDepartementSirenSiret() {
      super("PARTITION-departement-TRI-siren-siret", true, CODE_DEPARTEMENT.champ(), false, CODE_DEPARTEMENT.champ(), SIREN_ENTREPRISE.champ(), SIRET_ETABLISSEMENT.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec isNull
    * @param siren Code siren, si null sera testé avec isNull
    * @param siret Code siret, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String siren, String siret) {
      return equalTo(codeDepartement, siren)
         .and(siret != null ? SIRET_ETABLISSEMENT.col().equalTo(siret) : SIRET_ETABLISSEMENT.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec isNull
    * @param siren Code siren, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String siren) {
      return equalTo(codeDepartement)
         .and(siren != null ? SIREN_ENTREPRISE.col().equalTo(siren) : SIREN_ENTREPRISE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
