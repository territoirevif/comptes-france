package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.*;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.function.BooleanSupplier;

import org.apache.spark.sql.Row;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.HistoriqueExecution;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.validation.AbstractRowValidator;
import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

import fr.ecoemploi.domain.utils.autocontrole.ObjetMetierSpark;
import fr.ecoemploi.domain.model.territoire.SIREN;

/**
 * Validateur de dataset Row d'entreprise.
 * @author Marc Le Bihan
 */
@Component
public class EntrepriseRowValidator extends AbstractRowValidator implements ApplicationContextAware {
   @Serial
   private static final long serialVersionUID = 6450628700036390478L;

   /** Entreprise sans code SIREN */
   public static final String VLD_SIREN_INVALIDE = "ENT_SIREN_INVALIDE";

   /** Entreprise dont la date de création est invalide */
   public static final String VLD_DATE_CREATION_INVALIDE = "ENT_DATE_CREATION_INVALIDE";

   /** Entreprise dont la date de dernier traitement est invalide */
   public static final String VLD_DATE_DERNIER_TRAITEMENT_ENTREPRISE_INVALIDE = "ENT_DATE_DERNIER_TRAITEMENT_INVALIDE";

   /** Entreprise dont la date d'historisation est invalide */
   public static final String VLD_DATE_HISTORISATION_INVALIDE = "ENT_DATE_HISTORISATION_INVALIDE";

   /**
    * Valider une entreprise.
    * @param historique Historique d'exécution.
    * @param entreprise Entreprise (Row) à valider.
    * @param activesSeulement s'il ne faut retenir que les entreprises actives.
    * @return true si l'entreprise est valide.
    */
   public boolean validerEntrerprise(HistoriqueExecution historique, Row entreprise, boolean activesSeulement) {
      // Restreindre aux seuls établissements actifs, si souhaité.
      if (activesSeulement && (Boolean.FALSE.equals(entreprise.getAs("active")))) {
         return false;
      }

      List<BooleanSupplier> validations = new ArrayList<>();

      // Valider le code siren de l'entreprise
      String siren = entreprise.getAs(SIREN_ENTREPRISE.champ());
      validations.add(() -> valideSi(VLD_SIREN_INVALIDE, historique, entreprise, e -> new SIREN(siren).valide(), () -> new Serializable[] {siren}));

      // Valider la date de création de l'entreprise
      String dateCreation = entreprise.getAs(DATE_CREATION_ENTREPRISE.champ());
      validations.add(() -> valideSi(VLD_DATE_CREATION_INVALIDE, historique, entreprise, e -> {
         try {
            ObjetMetierSpark.fromINSEEtoLocalDate(dateCreation);
            return true;
         }
         catch(@SuppressWarnings("unused") DateTimeParseException ex) {
            return false;
         }
      },
      () -> new Serializable[] {siren, dateCreation}));

      // Valider la date de dernier traitement de l'entreprise
      String dateDernierTraitement = entreprise.getAs(DATE_DERNIER_TRAITEMENT_ENTREPRISE.champ());

      validations.add(() -> valideSi(VLD_DATE_DERNIER_TRAITEMENT_ENTREPRISE_INVALIDE, historique, entreprise, e ->
      {
         try {
            ObjetMetierSpark.fromINSEEtoLocalDateTime(dateDernierTraitement);
            return true;
         }
         catch(DateTimeParseException ex) {
            try {
               ObjetMetierSpark.fromINSEEtoLocalDate(dateDernierTraitement);
               return true;
            }
            catch(@SuppressWarnings("unused") DateTimeParseException ex2) {
               return false;
            }
         }
      },
      () -> new Serializable[] {siren, dateDernierTraitement}));

      // Valider la date de début historisation de l'entreprise
      String dateDebutHistorisation = entreprise.getAs(DATE_DEBUT_HISTORISATION_ENTREPRISE.champ());

      validations.add(() -> valideSi(VLD_DATE_HISTORISATION_INVALIDE, historique, entreprise, e -> {
         try {
            ObjetMetierSpark.fromINSEEtoLocalDate(dateDebutHistorisation);
            return true;
         }
         catch(DateTimeParseException ex) {
            return false;
         }
      }, () -> new Serializable[]{siren}));

      return validations.stream().allMatch(BooleanSupplier::getAsBoolean);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
      declareRegle(VLD_SIREN_INVALIDE, this.messageSource, "Un siren d'entreprise est invalide", String.class);
      declareRegle(VLD_DATE_CREATION_INVALIDE, this.messageSource, "Une date de création d'entreprise est invalide", String.class);
      declareRegle(VLD_DATE_DERNIER_TRAITEMENT_ENTREPRISE_INVALIDE, this.messageSource, "Une date de dernier traitement d'entreprise est invalide", String.class);
      declareRegle(VLD_DATE_HISTORISATION_INVALIDE, this.messageSource, "Une date d'historisation d'entreprise est invalide", String.class);
   }
}
