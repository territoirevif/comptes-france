package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.Serial;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.TriDataset;

/**
 * Tris pour les établissements
 */
public class EtablissementTri extends TriDataset {
   /** Serial UID */
   @Serial
   private static final long serialVersionUID = -8598052038867127958L;

   /**
    * Création d'un tri d'établissement.
    * @param suffixeFichier Suffixe de fichier parquet.
    * @param sortWithinPartition true s'il faut que les order by soient fait au sein des partitions.
    * @param nomColonnePartitionnement Nom de la colonne de partitionnement.
    * @param partitionByRange true s'il faut faire des partitions par range.
    * @param nomColonnesTri Nom des colonnes de tri.
    */
   public EtablissementTri(String suffixeFichier, boolean sortWithinPartition, String nomColonnePartitionnement, boolean partitionByRange, String... nomColonnesTri) {
      super(suffixeFichier, sortWithinPartition, nomColonnePartitionnement, partitionByRange, nomColonnesTri);
   }
}
