package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise.activite;

import java.io.*;
import static org.apache.spark.sql.types.DataTypes.*;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.domain.utils.objets.TechniqueException;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

/**
 * Dataset des codes NAF, par niveau.
 * @author Marc Le Bihan
 */
@Service
public class NAFDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -2455628917007045217L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(NAFDataset.class);

   /** Nom du fichier CSV. */
   private final String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   private final String repertoireFichier;

   /** Nomenclature NAF de niveau 1 (section). */
   @Value("${naf_niveau1.fichier.csv.nom}")
   private String nomFichierNAFNiveau1;
   
   /** Nomenclature NAF de niveau 2. */
   @Value("${naf_niveau2.fichier.csv.nom}")
   private String nomFichierNAFNiveau2;
   
   /** Nomenclature NAF de niveau 3. */
   @Value("${naf_niveau3.fichier.csv.nom}")
   private String nomFichierNAFNiveau3;
   
   /** Nomenclature NAF de niveau 4. */
   @Value("${naf_niveau4.fichier.csv.nom}")
   private String nomFichierNAFNiveau4;
   
   /** Nomenclature NAF de niveau 5. */
   @Value("${naf_niveau5.fichier.csv.nom}")
   private String nomFichierNAFNiveau5;

   /**
    * Construire un dataset de lecture des codes NAF.
    * @param fichier Nom du fichier NAF : n'est pas utilisé.
    * @param repertoire Répertoire du fichier.
    */
   public NAFDataset(@Value("") String fichier, 
      @Value("${sirene.dir}") String repertoire) {
      this.repertoireFichier = repertoire;
      this.nomFichier = fichier;
   }
   
   /**
    * Obtenir un Dataset Row des codes NAF.
    * @param annee Année du registre.
    * @return Dataset des codes NAF.
    * @throws TechniqueException si un incident survient.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> rowNomenclatureNAF(int annee) {
      LOGGER.info("Acquisition de la nomenclature NAF de {}...", annee);
      
      return rowNomenclatureNiveauNAF(annee, 1, this.nomFichierNAFNiveau1)
         .union(rowNomenclatureNiveauNAF(annee, 2, this.nomFichierNAFNiveau2))
         .union(rowNomenclatureNiveauNAF(annee, 3, this.nomFichierNAFNiveau3))
         .union(rowNomenclatureNiveauNAF(annee, 4, this.nomFichierNAFNiveau4))
         .union(rowNomenclatureNiveauNAF(annee, 5, this.nomFichierNAFNiveau5))
         .orderBy("codeNAF")
         .cache();
   }
   
   /**
    * Obtenir un Dataset Row des codes NAF.
    * @param annee Année de la nomenclature NAF.
    * @param niveau Niveau NAF que l'on alimente.
    * @param nomFichier Nom du fichier NAF contenant ce niveau.
    * @return Dataset des codes NAF.
    * @throws TechniqueException si un incident survient.
    */
   private Dataset<Row> rowNomenclatureNiveauNAF(int annee, int niveau, String nomFichier) {
      File repertoireParent = assertExistenceRepertoire(this.repertoireFichier);
      File source = assertExistenceFichierAnnuel(repertoireParent, annee, nomFichier);
      
      Dataset<Row> niveauNAF = this.session.read().schema(schemaCSV()).format("csv").option("encoding", "UTF-8")
         .option("header","true")
         .option("delimiter", ",")
         .option("quote", "\"")
         .option("escape", "\"")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath()).orderBy("codeNAF")
         .selectExpr("*");
      
      niveauNAF = niveauNAF.withColumn("niveauNAF", lit(niveau));
      return niveauNAF;
   }   
   
   /**
    * Renvoyer le schema du fichier CSV source.
    * @return schema.
    */
   public StructType schemaCSV() {
      /*
       * "codeNAF","libelleNAF"
       * "01.11Z","Culture de céréales (à l'exception du riz), de légumineuses et de graines oléagineuses"
       * "01.12Z","Culture du riz"
       * "01.13Z","Culture de légumes, de melons, de racines et de tubercules"       
       */
      StructType schema = new StructType();
      schema = schema.add("codeNAF", StringType, false) // Code NAF
         .add("libelleNAF", StringType, true);          // Libellé NAF

      return schema;
   }
}
