/**
 * Dataset des établissements et des entreprises.
 * @author Marc Le Bihan
 */
package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;