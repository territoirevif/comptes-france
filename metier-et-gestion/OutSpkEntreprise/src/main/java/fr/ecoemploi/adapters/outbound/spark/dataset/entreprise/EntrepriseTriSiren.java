package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;
import static org.apache.spark.sql.functions.col;

/**
 * Tri d'établissement par siren.
 * @author Marc Le Bihan
 */
public class EntrepriseTriSiren extends EntrepriseTri {
   @Serial
   private static final long serialVersionUID = -6616592373473155709L;

   /** Colonne de partition par code siren */
   private static final String COL_PARTITION_SIREN = "partitionSiren";

   /**
    * Création d'un tri d'établissement.
    */
   public EntrepriseTriSiren() {
      super("PARTITION-substr(siren,0,2)-TRI-siren", true, COL_PARTITION_SIREN, false, COL_PARTITION_SIREN, SIREN_ENTREPRISE.champ());
   }

   /**
    * Renvoyer une condition sur Siren.
    * @param siren Code Siren à joindre. Si null, isNull() servira au test.
    * @return Condition.
    */
   public Column equalTo(String siren) {
      String partitionSiren = siren != null ? siren.substring(0, 2) : null;

      Column condition = partitionSiren != null ? col(COL_PARTITION_SIREN).equalTo(partitionSiren) : col(COL_PARTITION_SIREN).isNull();
      return condition.and(siren != null ? SIREN_ENTREPRISE.col().equalTo(siren) : SIREN_ENTREPRISE.col().isNull());
   }
}
