package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.*;
import java.util.Objects;

import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.RowToObjetMetierInterface;
import fr.ecoemploi.domain.model.territoire.entreprise.Etablissement;

/**
 * Encodeur d'établissement.
 * @author Marc Le Bihan
 */
public class EtablissementEncoder implements RowToObjetMetierInterface<Etablissement>, Serializable {
   @Serial
   private static final long serialVersionUID = 7995765470793747026L;

   @Override
   public Dataset<Etablissement> toDatasetObjetsMetiers(Dataset<Row> rows) {
      Objects.requireNonNull(rows, "Le dataset d'établissements ne peut pas valoir null.");
      return rows.as(Encoders.bean(Etablissement.class));
   }

   @Override
   public Etablissement toObjetMetier(Row row) {
      throw new UnsupportedOperationException("Pas d'encodage unique d'établissement");
   }
}
