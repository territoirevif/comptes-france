package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.Serial;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;
import static org.apache.spark.sql.functions.*;
import org.apache.spark.sql.Column;

/**
 * Tri d'établissement par siren, siret.
 * @author Marc Le Bihan
 */
public class EtablissementTriSirenSiret extends EtablissementTri {
   @Serial
   private static final long serialVersionUID = -395634009943479694L;

   /** Colonne de partition par code siren */
   private static final String COL_PARTITION_SIREN = "partitionSiren";

   /**
    * Création d'un tri d'établissement.
    */
   public EtablissementTriSirenSiret() {
      super("PARTITION-substr(siren,0,2)-TRI-siren-siret", true, COL_PARTITION_SIREN, false, COL_PARTITION_SIREN, SIREN_ENTREPRISE.champ(), SIRET_ETABLISSEMENT.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param siren Code siren, si null sera testé avec isNull
    * @param siret Code siret, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String siren, String siret) {
      Column condition = conditionSurSiren(siren);
      return condition.and(siret != null ? SIRET_ETABLISSEMENT.col().equalTo(siret) : SIRET_ETABLISSEMENT.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param siren Code siren, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String siren) {
      return conditionSurSiren(siren);
   }

   /**
    * Renvoyer une condition sur Siren.
    * @param siren Code Siren à joindre. Si null, isNull() servira au test.
    * @return Condition.
    */
   private Column conditionSurSiren(String siren) {
      String partitionSiren = siren != null ? siren.substring(0, 2) : null;

      Column condition = partitionSiren != null ? col(COL_PARTITION_SIREN).equalTo(partitionSiren) : col(COL_PARTITION_SIREN).isNull();
      return condition.and(siren != null ? SIREN_ENTREPRISE.col().equalTo(siren) : SIREN_ENTREPRISE.col().isNull());
   }
}
