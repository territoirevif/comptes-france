package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.Serial;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;


/**
 * Tri disponibles pour les datasets d'entreprises.
 * @author Marc Le Bihan
 */
public class EntrepriseTri extends TriDataset {
   @Serial
   private static final long serialVersionUID = 8102162388992531328L;

   /**
    * Constuire un tri possible.
    * @param suffixeFichier            Suffixe du fichier trié.
    * @param sortWithinPartition       true s'il faut faire un tri au sein des partitions,<br>
    *                                  false si c'est un tri global.
    * @param nomColonnePartitionnement Nom de la colonne de partitionnement, s'il y en a une.
    * @param nomColonnesTri            Nom des colonnes de tri.
    */
   EntrepriseTri(String suffixeFichier, boolean sortWithinPartition, String nomColonnePartitionnement, boolean partitionByRange, String... nomColonnesTri) {
      super(suffixeFichier, sortWithinPartition, nomColonnePartitionnement, partitionByRange, nomColonnesTri);
   }
}
