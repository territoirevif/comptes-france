package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Partitionnement des entreprises par code département
 * Tri par code département, code EPCI, siren, siret
 * @author Marc Le Bihan
 */
public class EntrepriseTriDepartementEPCISirenSiret extends EntrepriseTri {
   @Serial
   private static final long serialVersionUID = 7116402599013545369L;

   /**
    * Construire un tri.
    */
   public EntrepriseTriDepartementEPCISirenSiret() {
      super("PARTITION-departement-TRI-epci-siren-siret", true, CODE_DEPARTEMENT.champ(), false, CODE_DEPARTEMENT.champ(), CODE_EPCI.champ(), SIREN_ENTREPRISE.champ(), SIRET_ETABLISSEMENT.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec isNull
    * @param codeEPCI Code EPCI, si null sera testé avec isNull
    * @param codeCommune Code commune, si null sera testé avec isNull
    * @param siren SIREN de l'entreprise.
    * @param siret SIRET de l'établissement.
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeEPCI, String codeCommune, String siren, String siret) {
      Column conditionSirenSiret = (siren != null ? SIREN_ENTREPRISE.col().equalTo(siren) : SIREN_ENTREPRISE.col().isNull())
         .and(siret != null ? SIRET_ETABLISSEMENT.col().equalTo(siret) : SIRET_ETABLISSEMENT.col().isNull());

      return equalTo(codeDepartement, codeEPCI, codeCommune)
         .and(conditionSirenSiret);
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec isNull
    * @param codeEPCI Code EPCI, si null sera testé avec isNull
    * @param codeCommune Code commune, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeEPCI, String codeCommune) {
      return equalTo(codeDepartement, codeEPCI)
         .and(codeCommune != null ? CODE_COMMUNE.col().equalTo(codeCommune) : CODE_COMMUNE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec isNull
    * @param codeEPCI Code EPCI, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeEPCI) {
      return equalTo(codeDepartement)
         .and(codeEPCI != null ? CODE_EPCI.col().equalTo(codeEPCI) : CODE_EPCI.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
