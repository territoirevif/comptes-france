package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.*;
import java.util.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;
import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.domain.model.territoire.entreprise.Etablissement;

/**
 * Chargeur de données CSV d'entreprises SIRENE
 * @author Marc Le Bihan
 */
@Component
public class EntrepriseRowCsvLoader extends AbstractSparkCsvLoader {
   @Serial
   private static final long serialVersionUID = 8256245006048763485L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EntrepriseRowCsvLoader.class);

   /** Nom du fichier CSV. */
   @Value("${stock_unitelegale.csv.nom}")
   private String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${sirene.dir}")
   private String repertoireFichier;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   // Ces champs vont être remplacés, renommés ou castés en un meilleur type
   private static final String RPL_STATUT_DIFFUSION_UNITE_LEGALE = "statutDiffusionUniteLegale";
   private static final String RPL_UNITE_PURGEE_UNITE_LEGALE = "unitePurgeeUniteLegale";

   private static final String RPL_ANNEE_EFFECTIFS_UNITE_LEGALE = "anneeEffectifsUniteLegale";
   private static final String RPL_ANNEE_CATEGORIE_ENTREPRISE = "anneeCategorieEntreprise";
   private static final String RPL_ETAT_ADMINISTRATIF_UNITE_LEGALE = "etatAdministratifUniteLegale";
   private static final String RPL_ECONOMIE_SOCIALE_SOLIDAIRE_UNITE_LEGALE = "economieSocialeSolidaireUniteLegale";
   private static final String RPL_CARACTERE_EMPLOYEUR_UNITE_LEGALE = "caractereEmployeurUniteLegale";

   /**
    * Charger un dataset.
    * @param anneeSIRENE Année pour laquelle rechercher les entreprises.
    * @return Ensemble des données attributaires accessibles sur les entreprises.
    */
   public Dataset<Row> loadOpenData(int anneeSIRENE) {
      LOGGER.info("Constitution du dataset des entreprises SIRENE de {}...", anneeSIRENE);

      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, anneeSIRENE, this.nomFichier);

      return rename(this.load(schema(anneeSIRENE), source), anneeSIRENE);
   }

   /**
    * Renvoyer le schéma du CSV source.
    * @param anneeSIRENE Année SIRENE.
    * @return Schema.
    */
   @Override
   protected StructType schema(int anneeSIRENE) {
      StructType schema = new StructType();

      schema = schema
         .add("siren", StringType, true)
         .add(RPL_STATUT_DIFFUSION_UNITE_LEGALE, StringType, true)
         .add(RPL_UNITE_PURGEE_UNITE_LEGALE, StringType, true)
         .add("dateCreationUniteLegale", StringType, true)
         .add("sigleUniteLegale", StringType, true)

         .add("sexeUniteLegale", StringType, true)
         .add("prenom1UniteLegale", StringType, true)
         .add("prenom2UniteLegale", StringType, true)
         .add("prenom3UniteLegale", StringType, true)
         .add("prenom4UniteLegale", StringType, true)

         .add("prenomUsuelUniteLegale", StringType, true)
         .add("pseudonymeUniteLegale", StringType, true)
         .add("identifiantAssociationUniteLegale", StringType, true)
         .add("trancheEffectifsUniteLegale", StringType, true)
         .add(RPL_ANNEE_EFFECTIFS_UNITE_LEGALE, StringType, true)

         .add("dateDernierTraitementUniteLegale", StringType, true)
         .add("nombrePeriodesUniteLegale", StringType, true)
         .add("categorieEntreprise", StringType, true)
         .add(RPL_ANNEE_CATEGORIE_ENTREPRISE, StringType, true)
         .add("dateDebut", StringType, true)

         .add(RPL_ETAT_ADMINISTRATIF_UNITE_LEGALE, StringType, true)
         .add("nomUniteLegale", StringType, true)
         .add("nomUsageUniteLegale", StringType, true)
         .add("denominationUniteLegale", StringType, true)
         .add("denominationUsuelle1UniteLegale", StringType, true)

         .add("denominationUsuelle2UniteLegale", StringType, true)
         .add("denominationUsuelle3UniteLegale", StringType, true)
         .add("categorieJuridiqueUniteLegale", StringType, true)
         .add("activitePrincipaleUniteLegale", StringType, true)
         .add("nomenclatureActivitePrincipaleUniteLegale", StringType, true)

         .add("nicSiegeUniteLegale", StringType, true)
         .add(RPL_ECONOMIE_SOCIALE_SOLIDAIRE_UNITE_LEGALE, StringType, true);

      if (anneeSIRENE >= 2023) {
         schema = schema
            .add("societeMissionUniteLegale", BooleanType, true);
      }

      schema = schema
         .add(RPL_CARACTERE_EMPLOYEUR_UNITE_LEGALE, BooleanType, true);

      return schema;
   }

   /**
    * Provoquer le chargement effectif de la source de données.
    * @param schema Schéma.
    */
   protected Dataset<Row> load(StructType schema, File source) {
      return this.session.read().schema(schema).format("csv")
         .option("header","true")
         .option("quote", "\"")
         .option("escape", "\"")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath());
   }

   /**
    * Renommer les champs du dataset.
    * @param dataset Dataset.
    */
   @Override
   protected Dataset<Row> rename(Dataset<Row> dataset, int annee) {
      Map<String, String> r1 = Map.of(
         "dateCreationUniteLegale", "dateCreationEntreprise",
         "sigleUniteLegale", "sigle",
         "sexeUniteLegale", "sexe",
         "prenom1UniteLegale", "prenom1",
         "prenom2UniteLegale", "prenom2",

         "prenom3UniteLegale", "prenom3",
         "prenom4UniteLegale", "prenom4",
         "prenomUsuelUniteLegale", "prenomUsuel",
         "pseudonymeUniteLegale", "pseudonyme",
         "identifiantAssociationUniteLegale", "rna"
      );

      Map<String, String> r2 = Map.of(
         "trancheEffectifsUniteLegale", "trancheEffectifSalarie",
         "dateDernierTraitementUniteLegale", "dateDernierTraitement",
         "categorieEntrepriseUniteLegale", "categorieEntreprise",
         "dateDebut", "dateDebutHistorisation",
         "nomUniteLegale", "nomNaissance",

         "distributionSpecialeUniteLegale", "distributionSpeciale",
         "denominationUniteLegale", "denominationEntreprise",
         "denominationUsuelle1UniteLegale", "denominationUsuelle1",
         "denominationUsuelle2UniteLegale", "denominationUsuelle2",
         "denominationUsuelle3UniteLegale", "denominationUsuelle3"
      );

      Map<String, String> r3 = Map.of(
         "categorieJuridiqueUniteLegale", "categorieJuridique",
         "activitePrincipaleUniteLegale", "activitePrincipale",
         "nomenclatureActivitePrincipaleUniteLegale", "nomenclatureActivitePrincipale",
         "nicSiegeUniteLegale", "nicSiege",
         "societeMissionUniteLegale", "societeMission",
         "nomUsageUniteLegale", "nomUsage",
         "siren", SIREN_ENTREPRISE.champ()
      );

      Dataset<Row> rename = super.renameFields(dataset, r1);
      rename = super.renameFields(rename, r2);
      rename = super.renameFields(rename, r3);

      return cast(rename, annee).selectExpr("*");
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Dataset<Row> cast(Dataset<Row> dataset, int annee) {
      StructType etablissementType = Encoders.bean(Etablissement.class).schema();

      return dataset
         .withColumn("anneeValiditeEffectifSalarie", col(RPL_ANNEE_EFFECTIFS_UNITE_LEGALE).cast(IntegerType))
         .withColumn("nombrePeriodes", col("nombrePeriodesUniteLegale").cast(IntegerType))
         .withColumn("anneeCategorie", col(RPL_ANNEE_CATEGORIE_ENTREPRISE).cast(IntegerType))
         .withColumn("active", when(col(RPL_ETAT_ADMINISTRATIF_UNITE_LEGALE).equalTo("A"), true).otherwise(false))
         .withColumn("economieSocialeSolidaire", when(col(RPL_ECONOMIE_SOCIALE_SOLIDAIRE_UNITE_LEGALE).equalTo("O"), true).otherwise(false))

         .withColumn("caractereEmployeur", when(col(RPL_CARACTERE_EMPLOYEUR_UNITE_LEGALE).equalTo("O"),true).otherwise(false))
         .withColumn("purgee", when(col(RPL_UNITE_PURGEE_UNITE_LEGALE).equalTo("true"), true).otherwise(false))
         .withColumn("diffusable", when(col(RPL_STATUT_DIFFUSION_UNITE_LEGALE).equalTo("O"), true).otherwise(false))
         .withColumn("etablissements", lit(null).cast(DataTypes.createMapType(StringType, etablissementType)))

         // Colonnes supprimées, issues des transformations.
         .drop(RPL_UNITE_PURGEE_UNITE_LEGALE, RPL_ANNEE_EFFECTIFS_UNITE_LEGALE, RPL_ANNEE_CATEGORIE_ENTREPRISE, RPL_ETAT_ADMINISTRATIF_UNITE_LEGALE,
            RPL_ECONOMIE_SOCIALE_SOLIDAIRE_UNITE_LEGALE, RPL_CARACTERE_EMPLOYEUR_UNITE_LEGALE, RPL_STATUT_DIFFUSION_UNITE_LEGALE);
   }
}
