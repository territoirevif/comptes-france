package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.*;
import java.util.*;
import java.util.function.Supplier;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;
import org.apache.spark.api.java.function.*;

import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.model.territoire.entreprise.*;

import fr.ecoemploi.adapters.outbound.port.entreprise.EntrepriseRepository;
import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

/**
 * Dataset d'entreprises.
 * @author Marc Le Bihan
 */
@Component
public class EntrepriseDataset extends AbstractSparkObjetMetierDataset<Entreprise> implements EntrepriseRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -3832045307656694020L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EntrepriseDataset.class);

   /** Année Sirene début. */
   @Value("${annee.sirene.debut}")
   private int annneSireneDebut;

   /** Année Sirene début. */
   @Value("${annee.sirene.fin}")
   private int annneSireneFin;

   /** Chargeur de données brutes open data */
   private final EntrepriseRowCsvLoader rowLoader;

   /** Validateur de Row d'entreprise */
   private final EntrepriseRowValidator validator;

   /** Dataset d'établissement. */
   private final EtablissementDataset datasetEtablissement;
   
   /** Dataset des Catégories juridiques. */
   private final CategorieJuridiqueDataset categorieJuridiqueDataset;

   /** Colonne de partition par code siren */
   private static final String COL_PARTITION_SIREN = "partitionSiren";

   /**
    * Construire un Dataset d'entreprises.
    * @param rowLoader Chargeur de données brutes.
    * @param validator Validateur de Row d'entreprise.
    * @param datasetEtablissement Dataset d'établissement.
    * @param categorieJuridiqueDataset Dataset des catégories juridiques.
    */
   @Autowired
   public EntrepriseDataset(EntrepriseRowCsvLoader rowLoader, EntrepriseRowValidator validator, EtablissementDataset datasetEtablissement, CategorieJuridiqueDataset categorieJuridiqueDataset) {
      this.rowLoader = rowLoader;
      this.validator = validator;
      this.datasetEtablissement = datasetEtablissement;
      this.categorieJuridiqueDataset = categorieJuridiqueDataset;
   }

   /**
    * Obtenir les entreprises et établissements d'une commune.
    * @param optionsCreationLecture Options de création et de lecture de dataset.
    * @param historiqueExecution Historique d'exécution
    * @param anneeCOG Année du Code Officiel Géographique de la commune.
    * @param anneeSirene Année du SIREN des entreprises.
    * @param codeCommune Code de la commune désirée.
    * @param triJointureEntreprises Tri intermédiaire réclamé pour la partie dataset entreprises, pour permettre la jointure avec le dataset établissements
    * @param triJointureEtablissements Tri intermédiaire réclamé pour la partie dataset établissements, pour permettre la jointure avec le dataset entreprises
    * @param triFinal Partionnement et tri choisi.
    * @return Entreprises et établissements de cette commune.
    */
   public Entreprises obtenirEntreprisesEtEtablissements(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution,
      EntrepriseTri triJointureEntreprises, EtablissementTri triJointureEtablissements, EntrepriseTri triFinal,
      int anneeCOG, int anneeSirene, CodeCommune codeCommune) {
      Objects.requireNonNull(codeCommune, "Le code commune ne peut pas valoir null");

      return entreprises(optionsCreationLecture, historiqueExecution, triJointureEntreprises, triJointureEtablissements, triFinal,
         null, col("codeCommune").equalTo(codeCommune.departementEtCommune()),
         anneeCOG, anneeSirene, true, true, true);
   }

   /**
    * Obtenir les entreprises et établissements d'une commune.
    * @param anneeCOG Année du Code Officiel Géographique de la commune.
    * @param anneeSirene Année du SIREN des entreprises.
    * @param codeCommune Code de la commune désirée.
    * @return Entreprises et établissements de cette commune.
    */
   @Override
   public Entreprises obtenirEntreprisesEtEtablissements(int anneeCOG, int anneeSirene, CodeCommune codeCommune) {
      return obtenirEntreprisesEtEtablissements(optionsCreationLecture(), null,
         new EntrepriseTriSiren(), new EtablissementTriSirenSiret(), new EntrepriseTriDepartementCommuneSirenSiret(),
         anneeCOG, anneeSirene, codeCommune);
   }

   /**
    * Obtenir un Dataset d'entreprises actives ou non.
    * @param optionsCreationLecture Options de création et de lecture de dataset.
    * @param historiqueExecution Historique d'exécution
    * @param annee Année pour laquelle rechercher les entreprises.
    * @param actifsSeulement true s'il faut se limiter aux seuls entreprises actives.
    * @param condition Condition optionnelle à appliquer.
    * @param tri Partionnement et tri choisi.
    * @return Ensemble des données attributaires accessibles sur les entreprises.
    */
   public Dataset<Entreprise> entreprisesSeules(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution, EntrepriseTri tri, Column condition, int annee, boolean actifsSeulement) {
      Dataset<Row> entreprises = rowEntreprises(optionsCreationLecture, historiqueExecution, annee, actifsSeulement, tri);

      if (condition != null) {
         entreprises = entreprises.where(condition);
      }

      return toDatasetEntreprise(entreprises);
   }

   /**
    * Convertir un Dataset Row en Dataset d'entreprises.
    * @param row Dataset Row.
    * @return Dataset d'entreprises.
    */
   public Dataset<Entreprise> toDatasetEntreprise(Dataset<Row> row) {
      Objects.requireNonNull(row, "Le dataset d'entreprises ne peut pas valoir null.");

      return row.as(Encoders.bean(Entreprise.class));
   }

   /**
    * Obtenir les entreprises liées à leur établissements.
    * @param entreprises Dataset d'entreprises.
    * @param etablissements Dataset d'établissements.
    * @return Entreprises alimentées avec leurs établissements.
    */
   public Entreprises entreprises(Dataset<Entreprise> entreprises, Dataset<Etablissement> etablissements) {
      return super.declinaison(new Entreprises(),
         entreprises, SIREN_ENTREPRISE.col(entreprises), Entreprise::getSiren,
         etablissements, SIREN_ENTREPRISE.col(etablissements), Etablissement::getSiret,
         Entreprise::getEtablissements);
   }

   /**
    * Obtenir les entreprises liées à leur établissements.
    * @param optionsCreationLecture Options de création et de lecture de dataset.
    * @param historiqueExecution Historique d'exécution
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param anneeSIRENE Année pour laquelle rechercher les établissements.
    * @param actifsSeulement true s'il faut se limiter aux seuls entreprises et établissements actifs.
    * @param communesValides true s'il faut restreindre les établissements sélectionnés à ceux dans des communes valides.
    * @param nomenclaturesNAF2Valides true s'il faut restreindre aux établissements dont les codes NAF sont des NAF2 et valides.
    * @param conditionSurEntreprises Conditions optionnelles sur les entreprises.
    * @param conditionSurEtablissements Condtions optionnelles sur les établissements.
    * @param triJointureEntreprises Tri intermédiaire réclamé pour la partie dataset entreprises, pour permettre la jointure avec le dataset établissements
    * @param triJointureEtablissements Tri intermédiaire réclamé pour la partie dataset établissements, pour permettre la jointure avec le dataset entreprises
    * @param triFinal Partionnement et tri choisi.
    * @return Entreprises alimentées avec leurs établissements.
    */
   public Entreprises entreprises(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution,
          EntrepriseTri triJointureEntreprises, EtablissementTri triJointureEtablissements, EntrepriseTri triFinal,
          Column conditionSurEntreprises, Column conditionSurEtablissements,
          int anneeCOG, int anneeSIRENE, boolean actifsSeulement, boolean communesValides, boolean nomenclaturesNAF2Valides) {
      return entreprises(entreprisesSeules(optionsCreationLecture, historiqueExecution, triJointureEntreprises, conditionSurEntreprises, anneeSIRENE, actifsSeulement),
         this.datasetEtablissement.etablissements(optionsCreationLecture, historiqueExecution, triJointureEtablissements, conditionSurEtablissements,
            anneeCOG, anneeSIRENE, actifsSeulement, communesValides, nomenclaturesNAF2Valides));
   }

   /**
    * Charger un référentiel d'entreprise, pour une ou toutes les années disponibles.
    * @param anneeCOG Année du Code Officiel Géographique, pour les communes de référence.
    * @param anneeSirene Année à charger ou 0 pour charger toutes celles disponibles.
    */
   @Override
   public void chargerReferentiel(int anneeCOG, int anneeSirene) {
      chargerReferentiel(optionsCreationLecture(), null, anneeCOG, anneeSirene);
   }

   /**
    * Charger un référentiel d'entreprise, pour une ou toutes les années disponibles.
    * @param optionsCreationLecture Options de création et de lecture de dataset.
    * @param historiqueExecution Historique d'exécution
    * @param anneeCOG Année du Code Officiel Géographique, pour les communes de référence.
    * @param anneeSirene Année à charger ou 0 pour charger toutes celles disponibles.
    */
   public void chargerReferentiel(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution, int anneeCOG, int anneeSirene) {
      if (anneeSirene != 0) {
         chargement(optionsCreationLecture, historiqueExecution, anneeCOG, anneeSirene);
      }
      else {
         for (int annee = this.annneSireneDebut; annee <= this.annneSireneFin; annee ++) {
            int anneeCommunes = anneeCOG;

            if (Math.abs(anneeCOG) < 10) {
               anneeCommunes = annee + anneeCOG;
            }

            chargement(optionsCreationLecture, historiqueExecution, anneeCommunes, annee);
         }
      }
   }

   /**
    * Provoquer le chargement effectif d'une année.
    * @param optionsCreationLecture Options de création et de lecture de dataset.
    * @param historiqueExecution Historique d'exécution
    * @param anneeCOG Année COG.
    * @param anneeSirene Année SIRENE.
    */
   private void chargement(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution, int anneeCOG, int anneeSirene) {
      LOGGER.info(this.messageSource.getMessage("service.chargementReferentiel.demande", new Object[] {anneeSirene, anneeCOG}, Locale.getDefault()));

      OptionsCreationLecture options = optionsCreationLecture();
      this.rowEntreprises(options, historiqueExecution, new EntrepriseTriSiren(), anneeSirene, false);
      this.rowEntreprises(options, historiqueExecution, new EntrepriseTriSiren(), anneeSirene, true);

      this.rowEntreprisesEtEtablissements(optionsCreationLecture, historiqueExecution, new EntrepriseTriDepartementEPCISirenSiret(), anneeCOG, anneeSirene, true, true, true);
      this.rowEntreprisesEtEtablissements(optionsCreationLecture, historiqueExecution, new EntrepriseTriSiren(), anneeCOG, anneeSirene, true, true, true);
      this.rowEntreprisesEtEtablissements(optionsCreationLecture, historiqueExecution, new EntrepriseTriDepartementCommuneSirenSiret(), anneeCOG, anneeSirene, true, true, true);
   }

   /**
    * Obtenir un Dataset Row d'entreprises actives ou non.
    * @param optionsCreationLecture Options de création et de lecture de dataset.
    * @param historiqueExecution Historique d'exécution
    * @param anneeSIRENE Année pour laquelle rechercher les entreprises.
    * @param actifsSeulement true s'il faut se limiter aux seuls entreprises actives.
    * @param tri Partionnement et tri choisi.
    * @return Ensemble des données attributaires accessibles sur les entreprises, ordonnées par SIREN.
    */
   public Dataset<Row> rowEntreprises(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution, int anneeSIRENE, boolean actifsSeulement, EntrepriseTri tri) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      return rowEntreprises(options, historiqueExecution, tri, anneeSIRENE, actifsSeulement);
   }

   /**
    * Obtenir un Dataset Row d'entreprises actives ou non. Sont écartées les entreprises qui sont : <br>
    *   - purgées<br>
    *   - ou non diffusables
    * @param anneeSIRENE Année pour laquelle rechercher les entreprises.
    * @return Row d'entreprises, triées par SIREN (et partitionnées par range sur SIREN)<br>
    * (les établissements ne sont pas alimentés).
    */
   public Dataset<Row> rowEntreprisesNonFiltrees(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeSIRENE) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();
      return rowEntreprisesNonFiltrees(options, historique, new EntrepriseTriSiren(), anneeSIRENE);
   }

   /**
    * Obtenir un Dataset Row d'entreprises actives ou non. Sont écartées les entreprises qui sont : <br>
    *   - purgées<br>
    *   - ou non diffusables
    * @param anneeSIRENE Année pour laquelle rechercher les entreprises.
    * @param tri Partionnement et tri choisi.
    * @return Row d'entreprises, triées par SIREN (et partitionnées par range sur SIREN)<br>
    * (les établissements ne sont pas alimentés).
    */
   public Dataset<Row> rowEntreprisesNonFiltrees(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, EntrepriseTri tri, int anneeSIRENE) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      Supplier<Dataset<Row>> worker = () -> {
         LOGGER.info("Début de l'acquisition des entreprises SIRENE non filtrées, actives ou non, de l'année {}...", anneeSIRENE);

         Dataset<Row> entreprises = this.rowLoader.loadOpenData(anneeSIRENE);
         entreprises = entreprises.filter(col("purgee").equalTo(false).and(col("diffusable").equalTo(true)));

         return entreprises.withColumn(COL_PARTITION_SIREN, col(SIREN_ENTREPRISE.champ()).substr(1,2));
      };

      boolean anneeEligibleCache = options.anneeEligibleMiseEnCache(anneeSIRENE);

      return constitutionStandard(options, historique, worker,
         anneeEligibleCache, (ConditionPostFiltrage) null,
         new CacheParqueteur<>(options, this.session, exportCSV(),
            "entreprises", "FILTRE-annee_{0,number,#0}-brutes", tri, anneeSIRENE));
   }

   /**
    * Obtenir un Dataset Row d'entreprises liées à leur établissements.
    * @param optionsCreationLecture Options de création et de lecture de dataset.
    * @param historiqueExecution Historique d'exécution
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param anneeSIRENE Année pour laquelle rechercher les établissements.
    * @param etablissementsActifsSeulement true s'il faut se limiter aux seuls entreprises et établissements actifs.
    * @param communesValidesSeulement true s'il faut restreindre les établissements sélectionnés à ceux dans des communes valides.
    * @param nomenclaturesNAF2Valides true s'il faut restreindre aux établissements dont les codes NAF sont des NAF2 et valides.
    * @param tri Tri à sélectionner.
    * @return Jointure des données attributaires entre établissements et entreprises.
    */
   public Dataset<Row> rowEntreprisesEtEtablissements(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution, EntrepriseTri tri,
      int anneeCOG, int anneeSIRENE, boolean etablissementsActifsSeulement, boolean communesValidesSeulement, boolean nomenclaturesNAF2Valides) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();
      final OptionsCreationLecture opt = options.genererCSV(false); // Car le dataset contient une Map<String, Etablissements> que le CSV ne peut présenter.

      Supplier<Dataset<Row>> worker = () -> {
         super.setStageDescription(this.messageSource, "row.entreprisesEtEtablissements.libelle.long", "row.entreprisesEtEtablissements.libelle.court",
            anneeSIRENE, anneeCOG, etablissementsActifsSeulement, communesValidesSeulement, tri);

         LOGGER.info(this.messageSource.getMessage("row.entreprisesEtEtablissements.libelle.long",
            new Object[]{anneeSIRENE, anneeCOG, etablissementsActifsSeulement, communesValidesSeulement, tri}, Locale.getDefault()));

         // Un tri spécifique est requis pour permettre la jointure entre entreprises et établissements : par SIREN [puis SIRET, pour les établissements]
         EntrepriseTriSiren triEntrepriseJointure = new EntrepriseTriSiren();
         EtablissementTriSirenSiret triEtablissementJointure = new EtablissementTriSirenSiret();

         Dataset<Row> entreprises = rowEntreprises(optionsCreationLecture, historiqueExecution, anneeSIRENE, etablissementsActifsSeulement, triEntrepriseJointure);
         Dataset<Row> etablissements = this.datasetEtablissement.rowEtablissements(opt, historiqueExecution, triEtablissementJointure,
            anneeCOG, anneeSIRENE, etablissementsActifsSeulement, communesValidesSeulement, nomenclaturesNAF2Valides);

         entreprises =  entreprises.join(etablissements, triEntrepriseJointure.joinCondition(entreprises, etablissements, true), "inner")
            .drop(entreprises.col("active"))
            .drop(entreprises.col("activitePrincipale"))
            .drop(entreprises.col("anneeValiditeEffectifSalarie"))
            .drop(entreprises.col("caractereEmployeur"))
            .drop(entreprises.col("dateDebutHistorisation"))
            .drop(entreprises.col("dateDernierTraitement"))
            .drop(entreprises.col("diffusable"))
            .drop(entreprises.col("nombrePeriodes"))
            .drop(entreprises.col("nomenclatureActivitePrincipale"))
            .drop(entreprises.col(COL_PARTITION_SIREN))
            .drop(entreprises.col("siren"))
            .drop(entreprises.col("trancheEffectifSalarie"));

         return entreprises.withColumn(COL_PARTITION_SIREN, col(SIREN_ENTREPRISE.champ()).substr(1,2));
      };

      Column postFiltre = options.hasRestrictionAuxCommunes() ?
         (CODE_COMMUNE.col().isin(options.restrictionAuxCommunes().toArray())).or(CODE_COMMUNE_SECONDAIRE.col().isin(options.restrictionAuxCommunes().toArray())) : null;

      boolean anneeEligibleCache = options.anneeEligibleMiseEnCache(anneeSIRENE);

      // C'est ici que le tri et partitionnement final, demandé par l'appelant, va être appliqué
      return constitutionStandard(options, historiqueExecution, worker,
         anneeEligibleCache, new ConditionPostFiltrageConstante(postFiltre),
         new CacheParqueteur<>(options, this.session, exportCSV(),
            "entreprises_et_etablissements", "FILTRE-annee_{0,number,#0}-cog_{1,number,#0}-etablissements_actifs_{2}-communes_valides_{3}-nafs_valides_{4}",
            tri, anneeSIRENE, anneeCOG, etablissementsActifsSeulement, communesValidesSeulement, nomenclaturesNAF2Valides));
   }

   /**
    * Obtenir un Dataset Row d'entreprises actives ou non.
    * @param options Options de création et de lecture de dataset.
    * @param historiqueExecution Historique d'exécution
    * @param anneeSIRENE Année pour laquelle rechercher les entreprises.
    * @param actifsSeulement true s'il faut se limiter aux seuls entreprises actives.
    * @return Ensemble des données attributaires accessibles sur les entreprises, ordonnées par SIREN.
    */
   private Dataset<Row> rowEntreprises(OptionsCreationLecture options, HistoriqueExecution historiqueExecution, EntrepriseTri tri, int anneeSIRENE, boolean actifsSeulement) {
      Supplier<Dataset<Row>> worker = () -> {
         LOGGER.info("Consitution du dataset des entreprises, année (SIRENE) : {}, actifs seulement: {}", anneeSIRENE, actifsSeulement);
         Dataset<Row> entreprises = rowEntreprisesNonFiltrees(options, historiqueExecution, anneeSIRENE);

         // Restreindre aux seuls entreprises actifs, si souhaité.
         if (actifsSeulement) {
            entreprises = entreprises.where(col("active"));
         }

         entreprises = entreprises
            .filter((FilterFunction<Row>)entreprise -> this.validator.validerEntrerprise(historiqueExecution, entreprise, actifsSeulement));

         // Associer les libellés des catégories juridiques.
         Dataset<Row> categoriesJuridiques = this.categorieJuridiqueDataset.rowCategoriesJuridiques(this.session, anneeSIRENE);

         entreprises = entreprises.join(categoriesJuridiques, entreprises.col("categorieJuridique").equalTo(categoriesJuridiques.col("codeCategorieJuridique")) , "left_outer")
            .withColumn(COL_PARTITION_SIREN, col(SIREN_ENTREPRISE.champ()).substr(1,2));

         return entreprises.drop("codeCategorieJuridique");
      };

      boolean anneeEligibleCache = options.anneeEligibleMiseEnCache(anneeSIRENE);

      return constitutionStandard(options, historiqueExecution, worker,
         anneeEligibleCache, (ConditionPostFiltrage)null,
         new CacheParqueteur<>(options, this.session, exportCSV(),
            "entreprises", "FILTRE-annee_{0,number,#0}-entreprises_actives_{1}", tri, anneeSIRENE, actifsSeulement));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected RowToObjetMetierInterface<Entreprise> objetMetierEncoder() {
      throw new UnsupportedOperationException("objetMetierEncoder n'est pas encore proposée sur Entreprise");
   }
}
