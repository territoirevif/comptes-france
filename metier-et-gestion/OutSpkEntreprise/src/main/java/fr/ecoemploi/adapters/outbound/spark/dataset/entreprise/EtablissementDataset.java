package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.function.Supplier;

import org.apache.commons.lang3.*;
import org.slf4j.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.model.territoire.entreprise.Etablissement;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.CogDataset;

import fr.ecoemploi.adapters.outbound.spark.dataset.entreprise.activite.NAFDataset;

/**
 * Dataset d'établissement.
 * @author Marc Le Bihan
 */
@Component
public class EtablissementDataset extends AbstractSparkObjetMetierDataset<Etablissement> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -487783006528050130L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EtablissementDataset.class);

   /** Chargeur de données brutes open data */
   private final EtablissementRowCsvLoader rowLoader;

   /** Validateur d'établissement. */
   private final EtablissementRowValidator validator;

   /** Encodeur d'établissement. */
   private final EtablissementEncoder encoder = new EtablissementEncoder();

   /** Service du Code Officiel Géographique. */
   private final CogDataset cogDataset;
   
   /** Nomenclature NAF. */
   private final NAFDataset nafDataset;

   /**
    * Construire un dataset d'établissements.
    * @param rowLoader Chargeur de row.
    * @param validator Validateur de row.
    * @param cogDataset Dataset du code officiel géographique.
    * @param nafDataset Dataset de la nomenclature NAF.
    */
   @Autowired
   public EtablissementDataset(EtablissementRowCsvLoader rowLoader, EtablissementRowValidator validator, CogDataset cogDataset, NAFDataset nafDataset) {
      this.rowLoader = rowLoader;
      this.validator = validator;
      this.cogDataset = cogDataset;
      this.nafDataset = nafDataset;
   }

   /**
    * Obtenir un Dataset d'établissements actifs ou non.
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param anneeSIRENE Année pour laquelle rechercher les établissements.
    * @param actifsSeulement true s'il faut se limiter aux seuls établissements actifs.
    * @param communesValides true s'il faut restreindre les établissements sélectionnés à ceux dans des communes valides.
    * @param nomenclaturesNAF2Valides true s'il faut restreindre aux établissements dont les codes NAF sont des NAF2 et valides.
    * @param condition Condition optionnelle à appliquer.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution.
    * @param tri Tri à appliquer.
    * @return Ensemble des données attributaires accessibles sur les établissements.
    */
   public Dataset<Etablissement> etablissements(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution, EtablissementTri tri, Column condition,
      int anneeCOG, int anneeSIRENE, boolean actifsSeulement, boolean communesValides, boolean nomenclaturesNAF2Valides) {
      Dataset<Row> rowsEtablissements = rowEtablissements(optionsCreationLecture, historiqueExecution,  tri, anneeCOG, anneeSIRENE, actifsSeulement, communesValides, nomenclaturesNAF2Valides);

      if (condition != null) {
         rowsEtablissements = rowsEtablissements.where(condition);
      }

      return this.encoder.toDatasetObjetsMetiers(rowsEtablissements);
   }

   /**
    * Obtenir un Dataset Row d'établissements non filtrés.
    * @param anneeSIRENE Année pour laquelle rechercher les établissements.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historique Historique d'exécution.
    * @return Ensemble des données attributaires accessibles sur les établissements<br>
    *         utiliser {@link EtablissementTriSirenSiret} pour rechercher ou joindre.
    */
   public Dataset<Row> etablissementsNonFiltres(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeSIRENE) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      Supplier<Dataset<Row>> worker = () -> {
         LOGGER.info("Début de l'acquisition des établissements non filtrés pour l'année {}...", anneeSIRENE);
         return this.rowLoader.loadOpenData(anneeSIRENE, 0.0 /* options.sampleFraction() */, 0L/* options.sampleSeed() */);
      };

      return constitutionStandard(options, historique, () -> worker.get()
         .withColumn("partitionSiren", col(SIREN_ENTREPRISE.champ()).substr(1,2)),
         new CacheParqueteur<>(options, this.session, exportCSV(),
            "etablissements", "FILTRE-annee_{0,number,#0}-bruts",
            new EtablissementTriSirenSiret(), anneeSIRENE));
   }

   /**
    * Obtenir un Dataset Row d'établissements actifs ou non.
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param anneeSIRENE Année pour laquelle rechercher les établissements.
    * @param actifsSeulement true s'il faut se limiter aux seuls établissements actifs.
    * @param communesValides true s'il faut restreindre les établissements sélectionnés à ceux dans des communes valides.
    * @param nomenclaturesNAF2Valides true s'il faut restreindre aux établissements dont les codes NAF sont des NAF2 et valides.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution.
    * @param tri Tri à appliquer.
    * @return Ensemble des données attributaires accessibles sur les établissements<br>
    *         utiliser {@link EtablissementTriDepartementSirenSiret} pour rechercher ou joindre.
    */
   public Dataset<Row> rowEtablissements(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution, EtablissementTri tri,
      int anneeCOG, int anneeSIRENE, boolean actifsSeulement, boolean communesValides, boolean nomenclaturesNAF2Valides) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historiqueExecution != null) {
         historiqueExecution.addReglesValidation(this.session, this.validator);
      }

      Supplier<Dataset<Row>> worker = () -> {
         super.setStageDescription(this.messageSource, "row.etablissements.libelle.long", "row.etablissements.libelle.court", anneeSIRENE, anneeCOG, actifsSeulement, communesValides, nomenclaturesNAF2Valides);
         LOGGER.info(this.messageSource.getMessage("row.etablissements.libelle.long", new Object[] {anneeSIRENE, anneeCOG, actifsSeulement, communesValides, nomenclaturesNAF2Valides}, Locale.getDefault()));

         Map<String, Integer> indexs = new HashMap<>();
         Dataset<Row> etablissements = etablissementsNonFiltres(optionsCreationLecture, historiqueExecution, anneeSIRENE);

         // Corriger les types de voies qui ont un motif d'invalidité assez simple et réparable
         etablissements = etablissements.withColumn(TYPE_DE_VOIE.champ(),
            when(TYPE_DE_VOIE.col().equalTo("R").or(TYPE_DE_VOIE.col().equalTo("RUE ")), TypeDeVoie.RUE.getAbreviation())
               .when(TYPE_DE_VOIE.col().equalTo("VGE"), TypeDeVoie.VILLAGE.getAbreviation())
               .when(TYPE_DE_VOIE.col().equalTo("LDT"), TypeDeVoie.LIEU_DIT.getAbreviation())
               .otherwise(TYPE_DE_VOIE.col()));

         etablissements = etablissements.withColumn(TYPE_DE_VOIE_SECONDAIRE.champ(),
            when(TYPE_DE_VOIE_SECONDAIRE.col().equalTo("R").or(TYPE_DE_VOIE_SECONDAIRE.col().equalTo("RUE ")), TypeDeVoie.RUE.getAbreviation())
               .when(TYPE_DE_VOIE_SECONDAIRE.col().equalTo("VGE"), TypeDeVoie.VILLAGE.getAbreviation())
               .when(TYPE_DE_VOIE_SECONDAIRE.col().equalTo("LDT"), TypeDeVoie.LIEU_DIT.getAbreviation())
               .otherwise(TYPE_DE_VOIE_SECONDAIRE.col()));

         // Détecter les établissements qui ont des communes étrangères comme communes primaires, et nationales comme communes secondaires.
         // Il faut alors permuter leurs champs.
         etablissements = permutationCommunesEtrangeresPrimairesAvecCommunesSecondairesNationales(etablissements);

         etablissements = etablissements.filter(
            // TODO n'est-il pas plus rapide de faire un where(... établissements actifs ...) avant ?
            (FilterFunction<Row>)etablissement -> this.validator.validerEtablissement(historiqueExecution, etablissement,
               actifsSeulement, nomenclaturesNAF2Valides, indexs));

         // Si le filtrage par communes valides a été demandé, l'appliquer.
         if (communesValides) {
            etablissements = rowRestreindreAuxCommunesValides(etablissements, historiqueExecution, indexs, anneeCOG, anneeSIRENE);
         }
         else {
            etablissements = etablissements.withColumn("codeDepartement", substring(CODE_COMMUNE.col(), 1, 2));
         }

         // Associer les libellés des codes APE/NAF.
         Dataset<Row> nomenclatureNAF = this.nafDataset.rowNomenclatureNAF(anneeSIRENE);
         etablissements = etablissements.join(nomenclatureNAF, etablissements.col("activitePrincipale").equalTo(nomenclatureNAF.col("codeNAF")) , "left_outer")
            .drop("codeNAF", "niveauNAF");

         // Le dataset est maintenant considéré comme valide, et ses champs peuvent être castés dans leurs types définitifs.
         return this.validator.cast(etablissements);
      };

      return constitutionStandard(options, historiqueExecution, () -> worker.get()
         .withColumn("partitionSiren", SIREN_ENTREPRISE.col().substr(1,2)),
         new CacheParqueteur<>(options, this.session, exportCSV(),
            "etablissements", "FILTRE-annee_{0,number,#0}-cog_{1,number,#0}-actifs_{2}-communes_verifiees_{3}-nafs_verifies_{4}", tri,
            anneeSIRENE, anneeCOG, actifsSeulement, communesValides, nomenclaturesNAF2Valides));
   }

   /** Suffixe du champ intermédiaire utilisé durant la permutation de champs */
   private static final String SUFFIXE_CHAMP_PERMUTATION = "_permutation";

   /**
    * Certains établissements ont des communes étrangères comme commune primaire, et nationales pour communes secondaires.<br>
    * La plupart des contrôles et des classements se font sur la commune primaire : une permutation est nécessaire.
    * @param etablissements Dataset Row d'établissements dont les communes posent ce problème.
    * @return Dataset Row d'établissements aux communes primaires et secondaires permutées.
    */
   private Dataset<Row> permutationCommunesEtrangeresPrimairesAvecCommunesSecondairesNationales(Dataset<Row> etablissements) {
      // Détecter les établissements qui ont des communes étrangères comme communes primaires, et nationales pour communes secondaires.
      // Il faut alors permuter leurs champs.
      Column communeSecondaireNationalePresente = etablissements.col("codeCommuneSecondaire").isNotNull().and(etablissements.col("codeCommuneSecondaire").notEqual(lit("")));
      Column adressePrincipaleEtrangere = etablissements.col("codePaysEtranger").isNotNull().and(etablissements.col("codePaysEtranger").notEqual(lit("")));
      Column adressePrincipaleSansCodeCommune = etablissements.col("codeCommune").isNull().or(etablissements.col("codeCommune").equalTo(lit("")));

      Column communeEtrangerePrimaireEtNationaleSecondaire =
         (adressePrincipaleEtrangere.or(adressePrincipaleSansCodeCommune)).and(communeSecondaireNationalePresente);

      Dataset<Row> etablissementsCommunesPrimairesEtrangeresSecondairesNationales = etablissements.where(communeEtrangerePrimaireEtNationaleSecondaire);
      etablissements = etablissements.where(not(communeEtrangerePrimaireEtNationaleSecondaire));
      etablissementsCommunesPrimairesEtrangeresSecondairesNationales = withColumnAdresse(etablissementsCommunesPrimairesEtrangeresSecondairesNationales, SUFFIXE_CHAMP_PERMUTATION, "");
      etablissementsCommunesPrimairesEtrangeresSecondairesNationales = withColumnAdresse(etablissementsCommunesPrimairesEtrangeresSecondairesNationales, "", "Secondaire");
      etablissementsCommunesPrimairesEtrangeresSecondairesNationales = withColumnAdresse(etablissementsCommunesPrimairesEtrangeresSecondairesNationales, "Secondaire", SUFFIXE_CHAMP_PERMUTATION);
      etablissementsCommunesPrimairesEtrangeresSecondairesNationales = dropColumnsPermutations(etablissementsCommunesPrimairesEtrangeresSecondairesNationales);

      return etablissements.union(etablissementsCommunesPrimairesEtrangeresSecondairesNationales);
   }

   /**
    * Assignation, avec création si nécessaire, de champs d'adresse d'établissements.
    * @param etablissements Dataset Row d'établissements
    * @param suffixeDestination Suffixe du champ de destination, pour <code>withColumn</code>
    * @param suffixeSource Suffixe du champ source, pour <code>withColumn</code>
    * @return Dataset Row d'établissements dont les champs ont été permutés
    */
   private Dataset<Row> withColumnAdresse(Dataset<Row> etablissements, String suffixeDestination, String suffixeSource) {
      etablissements = etablissements.withColumn("cedex" + suffixeDestination, etablissements.col("cedex" + suffixeSource));
      etablissements = etablissements.withColumn("libelleCedex" + suffixeDestination, etablissements.col("libelleCedex" + suffixeSource));

      etablissements = etablissements.withColumn("codeCommune" + suffixeDestination, etablissements.col("codeCommune" + suffixeSource));
      etablissements = etablissements.withColumn("nomCommune" + suffixeDestination, etablissements.col("nomCommune" + suffixeSource));

      etablissements = etablissements.withColumn("codePaysEtranger" + suffixeDestination, etablissements.col("codePaysEtranger" + suffixeSource));
      etablissements = etablissements.withColumn("nomPaysEtranger" + suffixeDestination, etablissements.col("nomPaysEtranger" + suffixeSource));

      etablissements = etablissements.withColumn("codePostal" + suffixeDestination, etablissements.col("codePostal" + suffixeSource));
      etablissements = etablissements.withColumn("complementAdresse" + suffixeDestination, etablissements.col("complementAdresse" + suffixeSource));
      etablissements = etablissements.withColumn("distributionSpeciale" + suffixeDestination, etablissements.col("distributionSpeciale" + suffixeSource));
      etablissements = etablissements.withColumn("indiceRepetition" + suffixeDestination, etablissements.col("indiceRepetition" + suffixeSource));

      etablissements = etablissements.withColumn("libelleVoie" + suffixeDestination, etablissements.col("libelleVoie" + suffixeSource));
      etablissements = etablissements.withColumn("typeDeVoie" + suffixeDestination, etablissements.col("typeDeVoie" + suffixeSource));
      etablissements = etablissements.withColumn("numeroVoie" + suffixeDestination, etablissements.col("numeroVoie" + suffixeSource));
      return etablissements;
   }

   /**
    * Suppression des champs de travail qui ont servi à la permutation des adresses primaires et secondaires.
    * @param etablissements Dataset d'établissements
    * @return Dataset Row d'établissements dont les champs de travail de permutation ont disparu.
    */
   private Dataset<Row> dropColumnsPermutations(Dataset<Row> etablissements) {
      return etablissements.drop("cedex" + SUFFIXE_CHAMP_PERMUTATION, "libelleCedex" + SUFFIXE_CHAMP_PERMUTATION, "codeCommune" + SUFFIXE_CHAMP_PERMUTATION, "nomCommune" + SUFFIXE_CHAMP_PERMUTATION, "codePaysEtranger" + SUFFIXE_CHAMP_PERMUTATION,
         "nomPaysEtranger" + SUFFIXE_CHAMP_PERMUTATION, "codePostal" + SUFFIXE_CHAMP_PERMUTATION, "complementAdresse" + SUFFIXE_CHAMP_PERMUTATION, "distributionSpeciale" + SUFFIXE_CHAMP_PERMUTATION, "indiceRepetition" + SUFFIXE_CHAMP_PERMUTATION,
         "libelleVoie" + SUFFIXE_CHAMP_PERMUTATION, "typeDeVoie" + SUFFIXE_CHAMP_PERMUTATION, "numeroVoie" + SUFFIXE_CHAMP_PERMUTATION);
   }

   /**
    * Restreindre le dataset<Row> des établissements à ceux aux communes valides.
    * @param etablissements Etablissements.
    * @param historiqueExecution Historique d'exécution.
    * @param anneeCOG Année du code officiel des communes.
    * @param indexs Index des champs.
    * @return Etablissements filtrés par communes valides<br>
    *         utiliser {@link EtablissementTriDepartementSirenSiret} pour rechercher ou joindre.
    */
   private Dataset<Row> rowRestreindreAuxCommunesValides(Dataset<Row> etablissements, HistoriqueExecution historiqueExecution, Map<String, Integer> indexs,
      int anneeCOG, int anneeSIRENE) {
      Dataset<Row> communes = this.cogDataset.rowCommunes(null, historiqueExecution, anneeCOG, false); // TODO mettre true, mais avec un avertissement dans le validateur que l'on est associé à une commune déléguée ou associée

      return etablissements
         .join(communes, CODE_COMMUNE.col(etablissements).equalTo(CODE_COMMUNE.col(communes)), "left_outer")
         .drop(CODE_COMMUNE.col(communes))
         .drop(NOM_COMMUNE.col(etablissements))
         .filter((FilterFunction<Row>)etablissement -> isCommuneValide(etablissement, indexs, anneeCOG, anneeSIRENE));
   }

   /**
    * Déterminer si la commune portée par un établissement est valide.
    * @param etablissement Row de l'établissement à considérer.
    * @param anneeCOG Année du code officiel géographique.
    * @param anneeSIRENE Année de la base de donnée SIRENE.
    * @param indexs Index des champs.
    * @return true si la commune est valide.
    */
   private boolean isCommuneValide(Row etablissement, Map<String, Integer> indexs, int anneeCOG, int anneeSIRENE) {
      String nomCommune = getString(indexs, etablissement, NOM_COMMUNE.champ());
      String codeCommune = getString(indexs, etablissement, CODE_COMMUNE.champ());

      if (StringUtils.isNotBlank(nomCommune)) {
         return true;
      }

      // Déterminer si nous sommes face à un établissement implanté à l'étranger.
      String nomCommuneEtrangere = etablissement.getAs("nomCommuneEtrangere");
      String paysEtranger = etablissement.getAs("nomPaysEtranger");
      String nomCommuneEtrangereSecondaire = etablissement.getAs("nomCommuneEtrangereSecondaire");
      String paysEtrangerSecondaire = etablissement.getAs("nomPaysEtrangerSecondaire");
      String codeCommuneSecondaire = etablissement.getAs("codeCommuneSecondaire");
      String nomCommuneSecondaire = etablissement.getAs("nomCommuneSecondaire");

      if (detecterCommuneSecondaireNonExploitee(etablissement, nomCommuneEtrangere, paysEtranger, SIRET_ETABLISSEMENT.getAs(etablissement), anneeSIRENE, codeCommuneSecondaire, nomCommuneSecondaire) == false &&
          detecterCommuneSecondaireNonExploitee(etablissement, nomCommuneEtrangereSecondaire, paysEtrangerSecondaire, SIRET_ETABLISSEMENT.getAs(etablissement), anneeSIRENE, codeCommuneSecondaire, nomCommuneSecondaire) == false &&
          new CodeCommune(codeCommune).estCollectiviteOutremer() == false) {
         // Les établissements des Antilles ou des collectivités d'outre-mer ne peuvent pas être retenus parce que leurs communes ne sont pas dans le maillage communal INSEE.
         // Mais si cette commune que l'on a pas trouvée n'en est pas une, c'est une anomalie que l'on rapporte.
         String format = "Commune de code {1} inconnue du COG {2,number,#0} pour un des établissements (SIRET) ou entreprise (SIREN) ''{3}'' déclaré dans le fichier INSEE/SIRENE de {0,number,#0}: l''établissement sera écarté.";
         String message = MessageFormat.format(format, anneeSIRENE, codeCommune != null ? codeCommune : "<Non défini>", anneeCOG, SIRET_ETABLISSEMENT.getAs(etablissement));
         LOGGER.warn(message);
      }

      return false;
   }

   /**
    * Déterminer s'il reste dans l'établissement une commune étrangère secondaire non exploitée commune et pays étranger.
    * @param etablissement Row de l'établissement en cause.
    * @param nomCommuneEtrangere Nom de la commune étrangère.
    * @param paysEtranger Pays étranger.
    * @param siret Siret de l'établissement.
    * @param anneeSIRENE Année siren.
    * @param codeCommuneSecondaire Code commune secondaire.
    * @param nomCommuneSecondaire Nom commune secondaire
    * @return true si ce problème a été repéré.
    */
   private boolean detecterCommuneSecondaireNonExploitee(Row etablissement, String nomCommuneEtrangere, String paysEtranger, String siret, int anneeSIRENE, String codeCommuneSecondaire, String nomCommuneSecondaire) {
      if (StringUtils.isBlank(nomCommuneEtrangere) && StringUtils.isBlank(paysEtranger)) {
         return false;
      }

      if (LOGGER.isTraceEnabled()) {
         String format = "Commune {1} étrangère ({2}) pour un des établissements (SIRET) ou entreprise (SIREN) ''{3}'' déclaré dans le fichier INSEE/SIRENE de {0,number,#0} : l''établissement sera écarté car une restriction à celles du COG a été demandée.";
         String message = MessageFormat.format(format, anneeSIRENE, nomCommuneEtrangere != null ? nomCommuneEtrangere : "non précisée", paysEtranger, siret);

         LOGGER.trace(message);
      }

      if (codeCommuneSecondaire != null) {
         LOGGER.error("Il y a une commune secondaire {} - {} sur un établissement ayant une adresse étrangère, non exploitée : {}",
            codeCommuneSecondaire, nomCommuneSecondaire, etablissement.mkString(", "));
      }

      return true;
   }

   /**
    * Renvoyer une valeur de champ d'un row sous forme de chaîne de caractères.
    * @param indexs Liste des indexs connus.
    * @param row Row pour acquisition des connaissance.
    * @param nomChamp Nom du champ.
    * @return Valeur du champ.
    */
   private String getString(Map<String, Integer> indexs, Row row, String nomChamp) {
      return row.getString(rowIndex(indexs, row, nomChamp));
   }

   /**
    * Renvoyer l'index d'un champ dans un row.
    * @param indexs Liste des indexs connus.
    * @param row Row pour acquisition des connaissance.
    * @param nomChamp Nom du champ.
    * @return Index du champ dans le row.
    */
   private int rowIndex(Map<String, Integer> indexs, Row row, String nomChamp) {
      return indexs.computeIfAbsent(nomChamp, row::fieldIndex);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected RowToObjetMetierInterface<Etablissement> objetMetierEncoder() {
      return this.encoder;
   }
}
