package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import java.io.*;
import java.util.Map;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;
import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Chargeur de données Open Data d'établissement, au format CSV
 * @author Marc Le Bihan
 */
@Component
public class EtablissementRowCsvLoader extends AbstractSparkCsvLoader {
   /** Serial ID.  */
   @Serial
   private static final long serialVersionUID = 1881078800377729015L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EtablissementRowCsvLoader.class);

   /** Nom du fichier CSV. */
   @Value("${stock_etablissement.csv.nom}")
   private String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${sirene.dir}")
   private String repertoireFichier;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Charger un dataset.
    * @param anneeSIRENE Année pour laquelle rechercher les établissements.
    * @param sampleFraction Fraction de sample. Pas de sample réalisé si égal à 0.0
    * @param sampleSeed seed utilisé pour le sample. Pas de seed si égal à 0 ou null.
    * @return Ensemble des données attributaires accessibles sur les établissements.
    */
   public Dataset<Row> loadOpenData(int anneeSIRENE, double sampleFraction, Long sampleSeed) {
      LOGGER.info("Constitution du dataset des établissements SIRENE de {}...", anneeSIRENE);

      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, anneeSIRENE, this.nomFichier);

      return rename(load(schema(), source, sampleFraction, sampleSeed), anneeSIRENE);
   }

   // Les champs qui suivent vont être remplacés par de meilleurs champs (renommés, castés)
   private static final String RPL_STATUT_DIFFUSION_ETABLISSEMENT = "statutDiffusionEtablissement";
   private static final String RPL_ETABLISSEMENT_SIEGE = "etablissementSiege";
   private static final String RPL_ETAT_ADMINISTRATIF_ETABLISSEMENT = "etatAdministratifEtablissement";
   private static final String RPL_CARACTERE_EMPLOYEUR_ETABLISSEMENT = "caractereEmployeurEtablissement";

   /**
    * Renvoyer le schéma du CSV source<br>
    * (Schéma 2024)
    * @return Schema.
    */
   protected StructType schema() {
      return new StructType()
         .add("siren", StringType, true)
         .add("nic", StringType, true)
         .add("siret", StringType, true)
         .add(RPL_STATUT_DIFFUSION_ETABLISSEMENT, StringType, true)
         .add("dateCreationEtablissement", StringType, true)

         .add("trancheEffectifsEtablissement", StringType, true)
         .add("anneeEffectifsEtablissement", StringType, true)
         .add("activitePrincipaleRegistreMetiersEtablissement", StringType, true)
         .add("dateDernierTraitementEtablissement", StringType, true)
         .add(RPL_ETABLISSEMENT_SIEGE, StringType, false)

         .add("nombrePeriodesEtablissement", StringType, true)
         .add("complementAdresseEtablissement", StringType, true)
         .add("numeroVoieEtablissement", StringType, true)
         .add("indiceRepetitionEtablissement", StringType, true)
         .add("typeVoieEtablissement", StringType, true)

         .add("libelleVoieEtablissement", StringType, true)
         .add("codePostalEtablissement", StringType, true)
         .add("libelleCommuneEtablissement", StringType, true)
         .add("libelleCommuneEtrangerEtablissement", StringType, true)
         .add("distributionSpecialeEtablissement", StringType, true)

         .add("codeCommuneEtablissement", StringType, true)
         .add("codeCedexEtablissement", StringType, true)
         .add("libelleCedexEtablissement", StringType, true)
         .add("codePaysEtrangerEtablissement", StringType, true)
         .add("libellePaysEtrangerEtablissement", StringType, true)

         .add("complementAdresse2Etablissement", StringType, true)
         .add("numeroVoie2Etablissement", StringType, true)
         .add("indiceRepetition2Etablissement", StringType, true)
         .add("typeVoie2Etablissement", StringType, true)
         .add("libelleVoie2Etablissement", StringType, true)

         .add("codePostal2Etablissement", StringType, true)
         .add("libelleCommune2Etablissement", StringType, true)
         .add("libelleCommuneEtranger2Etablissement", StringType, true)
         .add("distributionSpeciale2Etablissement", StringType, true)
         .add("codeCommune2Etablissement", StringType, true)

         .add("codeCedex2Etablissement", StringType, true)
         .add("libelleCedex2Etablissement", StringType, true)
         .add("codePaysEtranger2Etablissement", StringType, true)
         .add("libellePaysEtranger2Etablissement", StringType, true)
         .add("dateDebut", StringType, true)

         .add(RPL_ETAT_ADMINISTRATIF_ETABLISSEMENT, StringType, true)
         .add("enseigne1Etablissement", StringType, true)
         .add("enseigne2Etablissement", StringType, true)
         .add("enseigne3Etablissement", StringType, true)
         .add("denominationUsuelleEtablissement", StringType, true)

         .add("activitePrincipaleEtablissement", StringType, true)
         .add("nomenclatureActivitePrincipaleEtablissement", StringType, true)
         .add(RPL_CARACTERE_EMPLOYEUR_ETABLISSEMENT, StringType, true);
   }

   /**
    * Provoquer le chargement effectif de la source de données.
    * @param schema Schéma.
    */
   protected Dataset<Row> load(StructType schema, File source) {
      return load(schema, source, 0.0, null);
   }

   /**
    * Provoquer le chargement effectif de la source de données.
    * @param sampleFraction Fraction de sample. Pas de sample réalisé si égal à 0.0
    * @param sampleSeed seed utilisé pour le sample. Pas de seed si égal à 0 ou null.
    * @param schema Schéma.
    */
   protected Dataset<Row> load(StructType schema, File source, double sampleFraction, Long sampleSeed) {
      return applySample(this.session.read().schema(schema).format("csv")
         .option("header","true")
         .option("quote", "\"")
         .option("escape", "\"")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath()).selectExpr("*"), sampleFraction, sampleSeed);
   }

   /**
    * Renommer les champs du dataset.
    * @param dataset Dataset.
    */
   @Override
   protected Dataset<Row> rename(Dataset<Row> dataset, int annee) {
      Map<String, String> r1 = Map.of(
         "activitePrincipaleEtablissement", "activitePrincipale",
         "activitePrincipaleRegistreMetiersEtablissement", "activiteArtisanRegistreDesMetiers",
         "codeCedexEtablissement", "cedex",
         "codeCedex2Etablissement", "cedexSecondaire",
         "codeCommuneEtablissement", "codeCommune",

         "codeCommune2Etablissement", "codeCommuneSecondaire",
         "codePaysEtrangerEtablissement", "codePaysEtranger",
         "codePaysEtranger2Etablissement", "codePaysEtrangerSecondaire",
         "codePostalEtablissement", "codePostal",
         "codePostal2Etablissement", "codePostalSecondaire"
      );

      Map<String, String> r2 = Map.of(
         "complementAdresseEtablissement", "complementAdresse",
         "complementAdresse2Etablissement", "complementAdresseSecondaire",
         "dateDebut", "dateDebutHistorisation",
         "dateDernierTraitementEtablissement", "dateDernierTraitement",
         "denominationUsuelleEtablissement", "denominationEtablissement",

         "distributionSpecialeEtablissement", "distributionSpeciale",
         "distributionSpeciale2Etablissement", "distributionSpecialeSecondaire",
         "enseigne1Etablissement", "enseigne1",
         "enseigne2Etablissement", "enseigne2",
         "enseigne3Etablissement", "enseigne3"
      );

      Map<String, String> r3 = Map.of(
         "indiceRepetitionEtablissement", "indiceRepetition",
         "indiceRepetition2Etablissement", "indiceRepetitionSecondaire",
         "libelleCedexEtablissement", "libelleCedex",
         "libelleCedex2Etablissement", "libelleCedexSecondaire",
         "libelleCommuneEtablissement", "nomCommune",

         "libelleCommune2Etablissement", "nomCommuneSecondaire",
         "libelleCommuneEtrangerEtablissement", "nomCommuneEtrangere",
         "libelleCommuneEtranger2Etablissement", "nomCommuneEtrangereSecondaire",
         "libellePaysEtrangerEtablissement", "nomPaysEtranger",
         "libellePaysEtranger2Etablissement", "nomPaysEtrangerSecondaire"
      );

      Map<String, String> r4 = Map.of(
         "libelleVoieEtablissement", "libelleVoie",
         "libelleVoie2Etablissement", "libelleVoieSecondaire",
         "nomenclatureActivitePrincipaleEtablissement", "nomenclatureActivitePrincipale",
         "numeroVoieEtablissement", "numeroVoie",
         "numeroVoie2Etablissement", "numeroVoieSecondaire",

         "trancheEffectifsEtablissement", "trancheEffectifSalarie",
         "typeVoieEtablissement", TYPE_DE_VOIE.champ(),
         "typeVoie2Etablissement", TYPE_DE_VOIE_SECONDAIRE.champ()
      );

      Dataset<Row> rename = super.renameFields(dataset, r1);
      rename = super.renameFields(rename, r2);
      rename = super.renameFields(rename, r3);
      rename = super.renameFields(rename, r4);

      return cast(rename, annee).selectExpr("*");
   }

   @Override
   protected Dataset<Row> cast(Dataset<Row> dataset, int annee) {
      return dataset
         .withColumn("active", when(col(RPL_ETAT_ADMINISTRATIF_ETABLISSEMENT).equalTo("A"), true).otherwise(false))
         .withColumn("diffusable", when(col(RPL_STATUT_DIFFUSION_ETABLISSEMENT).equalTo("O"), true).otherwise(false))
         .withColumn("caractereEmployeur", when(col(RPL_CARACTERE_EMPLOYEUR_ETABLISSEMENT).equalTo("O"), true).otherwise(false))
         .withColumn("siege", when(col(RPL_ETABLISSEMENT_SIEGE).equalTo("O"), true).otherwise(false))

         .drop(RPL_ETAT_ADMINISTRATIF_ETABLISSEMENT, RPL_STATUT_DIFFUSION_ETABLISSEMENT, RPL_CARACTERE_EMPLOYEUR_ETABLISSEMENT, RPL_ETABLISSEMENT_SIEGE);
   }
}
