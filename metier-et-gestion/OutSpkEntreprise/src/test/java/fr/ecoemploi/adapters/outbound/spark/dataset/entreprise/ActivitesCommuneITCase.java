package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import static org.apache.spark.sql.functions.*;
import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.text.MessageFormat;

import org.apache.spark.sql.*;
import org.junit.jupiter.api.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.entreprise.activite.*;
import fr.ecoemploi.domain.model.territoire.entreprise.ActiviteCommune;

/**
 * Test sur les activités des communes.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkEntrepriseTestApplication.class)
//@TestPropertySource("classpath:application-test_EntreprisesIT.properties")
class ActivitesCommuneITCase extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 2983051568876900369L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ActivitesCommuneITCase.class);

   /** Dataset d'entreprises. */
   @Autowired
   private EntrepriseDataset entrepriseDataset;
   
   /** Dataset d'activités de communes. */
   @Autowired
   private ActiviteCommuneDataset activiteCommuneDataset;

   /** Dataset de la nomenclature NAF. */
   @Autowired
   private NAFDataset nafDataset;
   
   /** Année du cog pour les tests. */
   private final static int COG = 2024;
   
   /** Année du SIRENE pour les tests. */
   private final static int ANNEE_SIRENE = 2024;

   /*
   En 2019 : L'entreprise 005720784 a 7 établissements actifs en France.
  005720784,00015,00572078400015,²O,1900-01-01,NN,,,2018-07-01T20:12:10,true,4,,24,,RUE,JULES GUESDE,80210,FEUQUIERES EN VIMEU,,,80308,,,,,,,,,,,,,,,,,,,2008-01-01,A,,,,,25.99A,NAFRev2,O
  005720784,00031,00572078400031,O,1993-04-01,31,2016,,2018-09-29T12:00:17,false,4,,,,,ZONE INDUSTRIELLE,80210,FEUQUIERES EN VIMEU,,,80308,,,,,,,,,,,,,,,,,,,2008-01-01,A,,,,,25.72Z,NAFRev2,O
  005720784,00049,00572078400049,O,1997-06-25,NN,,,2017-05-04T01:02:32,false,4,,,,RUE,DU CHEVALIER DE LA BARRE,80210,FEUQUIERES EN VIMEU,,,80308,,,,,,,,,,,,,,,,,,,2008-01-01,A,,,,,46.74A,NAFRev2,N
  005720784,00106,00572078400106,O,2008-12-28,12,2016,,2018-09-29T12:00:17,false,1,,52,,RUE,HENRI BARBUSSE,80210,FEUQUIERES EN VIMEU,,,80308,,,,,,,,,,,,,,,,,,,2008-12-28,A,,,,,25.99A,NAFRev2,O
  005720784,00056,00572078400056,O,2000-09-01,NN,,,2011-04-23T04:00:24,false,4,,79,B,AV,DU GENERAL LECLERC,93500,PANTIN,,,93055,,,,,,,,,,,,,,,,,,,2009-03-06,F,,,,,46.74A,NAFRev2,O
  005720784,00064,00572078400064,O,2000-10-23,NN,,,,false,4,,97,,RUE,DU CHATEAU D EAU,80100,ABBEVILLE,,,80001,,,,,,,,,,,,,,,,,,,2002-12-25,F,,,,,28.6F,NAF1993,N
  005720784,00072,00572078400072,O,2005-11-01,NN,,,2013-05-18T02:02:21,false,4,ZI LES ESTROUBLANS,15,,AV,DE LONDRES,13127,VITROLLES,,,13117,,,,,,,,,,,,,,,,,,,2011-03-31,F,,,,,46.19B,NAFRev2,O
  005720784,00080,00572078400080,O,2008-02-01,01,2016,,2018-09-29T12:00:17,false,2,,2,,RUE,DE LA CAVEE DE VAUCHELLES,80100,ABBEVILLE,,,80001,,,,,,,,,,,,,,,,,,,2008-10-01,A,,,,,16.21Z,NAFRev2,O
  005720784,00098,00572078400098,O,2008-11-28,,,,2009-01-28T20:02:22,false,1,,7,B,RUE,DES SARCELLES,80100,ABBEVILLE,,,80001,,,,,,,,,,,,,,,,,,,2008-11-28,F,,,,,25.99A,NAFRev2,O
  005720784,00114,00572078400114,O,2008-11-28,NN,,,2015-05-14T03:02:04,false,2,,15,,RUE,SIBUET,75012,PARIS 12,,,75112,,,,,,,,,,,,,,,,,,,2014-03-31,F,,,,,25.72Z,NAFRev2,O
  005720784,00122,00572078400122,O,2009-02-16,NN,,,2014-06-13T23:02:09,false,2,,25,,,GRANDE RUE,80130,BETHENCOURT SUR MER,,,80096,,,,,,,,,,,,,,,,,,,2012-06-30,F,,,,,25.61Z,NAFRev2,O
  005720784,00130,00572078400130,O,2009-03-01,NN,,,2013-05-18T02:02:21,false,2,ZAC DES PETITS PONTS,73,,RUE,HENRI FARMAN,93290,TREMBLAY EN FRANCE,,,93073,,,,,,,,,,,,,,,,,,,2011-01-01,F,,,,,25.99A,NAFRev2,O
  005720784,00148,00572078400148,O,2011-01-01,01,2016,,2018-09-29T12:00:17,false,1,ZA CHARLES DE GAULLE,1,,IMP,NICEPHORE NIEPCE,93290,TREMBLAY EN FRANCE,,,93073,,,,,,,,,,,,,,,,,,,2011-01-01,A,,,,,25.99A,NAFRev2,O
  005720784,00155,00572078400155,O,2012-10-01,12,2016,,2018-09-29T12:00:17,false,2,ZONE INDUSTRIELLE,7,,RUE,DES SARCELLES,80100,ABBEVILLE,,BP 10507,80001,,,,,,,,,,,,,,,,,,,2013-02-03,A,,,,,25.99A,NAFRev2,O
    */
   
   /**
    * Désactiver les caches des datasets à tester.
    */
   @BeforeEach
   public void desactiverCaches() {
      // this.entrepriseDataset.loader().setCache(activerCache());
      // this.activiteCommuneDataset.loader().setCache(activerCache());
      // this.nafDataset.loader().setCache(activerCache());
   }

   /**
    * Activités des communes par code APE.
    */
   @Test
   @DisplayName("Dataset d'activités de communes, détail par entreprise et établissement.")
   void activitesCommunesDetail() {
      OptionsCreationLecture options = this.entrepriseDataset.optionsCreationLecture();
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> entreprisesEtablissements = this.entrepriseDataset.rowEntreprisesEtEtablissements(options, historiqueExecution, new EntrepriseTriDepartementCommuneSirenSiret(),
         COG, ANNEE_SIRENE, true, true, true);

      Dataset<ActiviteCommune> activites = this.activiteCommuneDataset.activitesParCommune(entreprisesEtablissements);
      assertNotEquals(0, activites.count(), "Plusieurs activités dans les communes auraient du être déduites.");
      
      dump(activites, 100, LOGGER);
   }

   /**
    * Activités des communes par code APE.
    */
   @Test
   @DisplayName("Activités des communes, regroupées par code APE.")
   void activitesCommunesRegroupeesAPE() {
      OptionsCreationLecture options = this.entrepriseDataset.optionsCreationLecture();
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> entreprises = this.entrepriseDataset.rowEntreprisesEtEtablissements(options, historiqueExecution, new EntrepriseTriDepartementCommuneSirenSiret(),
         COG, ANNEE_SIRENE, true, true, true);

      int nombre = 10000;

      for(int niveauNAF = 5; niveauNAF >= 1; niveauNAF --) {
         assertActivitesCommunesRegroupeesAPE(entreprises, niveauNAF, nombre);
      }
   }

   /**
    * Activités des communes par code APE de niveau 3.
    */
   @Test
   @DisplayName("Activités des communes regroupées par code APE de niveau 3.")
   void activitesCommunesRegroupeesAPE3() {
      OptionsCreationLecture options = this.entrepriseDataset.optionsCreationLecture();
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> entreprises = this.entrepriseDataset.rowEntreprisesEtEtablissements(options, historiqueExecution, new EntrepriseTriDepartementCommuneSirenSiret(),
         COG, ANNEE_SIRENE, true, true, true);

      int nombre = 10000;
      assertActivitesCommunesRegroupeesAPE(entreprises, 3, nombre);
   }


   /**
    * Activité d'une commune par code APE.
    */
   @Test
   @DisplayName("Activités d'une commune, regroupées par code APE.")
   void activiteCommuneRegroupeesAPE() {
      OptionsCreationLecture options = this.entrepriseDataset.optionsCreationLecture();
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> entreprises = this.entrepriseDataset.rowEntreprisesEtEtablissements(options, historiqueExecution, new EntrepriseTriDepartementCommuneSirenSiret(),
         COG, ANNEE_SIRENE, true, true, true);

      String codeCommune = "14654";
      int nombre = 10000;

      entreprises = entreprises.filter(col("codeCommune").equalTo(codeCommune));

      for(int niveauNAF = 5; niveauNAF >= 1; niveauNAF --) {
         assertActivitesCommunesRegroupeesAPE(entreprises, niveauNAF, nombre);
      }
   }

   /**
    * Vérifier que le résultat retourné par le test des activités par code APE est alimenté.
    * @param entreprises Dataset d'entreprises à regrouper par code NAF.
    * @param niveauNAF Niveau NAF de regroupement.
    * @param nombre Nombre d'activités à afficher.
    */
   private void assertActivitesCommunesRegroupeesAPE(Dataset<Row> entreprises, int niveauNAF, int nombre) {
      Dataset<Row> regroupementActivitesNAF = this.activiteCommuneDataset.regroupementActivites(entreprises, ANNEE_SIRENE, niveauNAF, true);

      assertNotEquals(0, regroupementActivitesNAF.count(), MessageFormat.format("Au moins une activité de niveau NAF {0} aurait dû être trouvée", niveauNAF));

      regroupementActivitesNAF.show(nombre, false);
      regroupementActivitesNAF.printSchema();
      regroupementActivitesNAF.explain();
   }

   /**
    * Répartition PME ETI GE.
    */
   @Test
   @DisplayName("Répartition PME ETI GE.")
   void summaryPME_ETI_GE() {
      OptionsCreationLecture options = this.entrepriseDataset.optionsCreationLecture();
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> entreprises = this.entrepriseDataset.rowEntreprisesEtEtablissements(options, historiqueExecution, new EntrepriseTriDepartementEPCISirenSiret(),
            COG, ANNEE_SIRENE, true, true, true)
            .where("codeEPCI = '242900645'");
      
      Dataset<Row> activites = this.activiteCommuneDataset.activitesParCommune(entreprises).toDF();
      assertNotEquals(0, activites.count(), "Au moins une activité aurait dû être extraite pour le code EPCI");

      activites = activites.selectExpr("nomCommune", "categorieEtablissement", "nombreSalaries").groupBy("nomCommune", "categorieEtablissement").agg(count("*"), sum("nombreSalaries"))
         .orderBy("nomCommune", "categorieEtablissement");

      this.activiteCommuneDataset.exportCSV().exporterCSV(activites, "repartition_categories_etablissements_", true);
   }

   /**
    * Répartition PME ETI GE par strate.
    */
   @Test
   @DisplayName("Répartition PME ETI GE par strate.")
   void summaryPME_ETI_GE_Strate() {
      OptionsCreationLecture options = this.entrepriseDataset.optionsCreationLecture();
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> entreprises = this.entrepriseDataset.rowEntreprisesEtEtablissements(options, historiqueExecution, new EntrepriseTriDepartementCommuneSirenSiret(),
            COG, ANNEE_SIRENE, true, true, true)
            .where("strateCommune = 7");
      
      Dataset<Row> activites = this.activiteCommuneDataset.activitesParCommune(entreprises).toDF();
      assertNotEquals(0, activites.count(), "Au moins une activité aurait dû être extraite pour la strate communale");

      activites = activites.selectExpr("strateCommune", "categorieEtablissement", "nombreSalaries").groupBy("strateCommune", "categorieEtablissement").agg(count("*"))
         .orderBy("strateCommune", "categorieEtablissement");

      this.activiteCommuneDataset.exportCSV().exporterCSV(activites, "repartition_categories_etablissements_", true);
   }

   /**
    * Salariés par code APE.
    */
   @Test
   @DisplayName("Dataset de salariés par code APE.")
   void salariesActivite() {
      OptionsCreationLecture options = this.entrepriseDataset.optionsCreationLecture();
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> entreprisesEtablissements = this.entrepriseDataset.rowEntreprisesEtEtablissements(options, historiqueExecution, new EntrepriseTriDepartementCommuneSirenSiret(),
         COG, ANNEE_SIRENE, true, true, true);

      assertNotEquals(0, entreprisesEtablissements.count(), "Au moins un établissement d'une entreprise aurait dû être extraite");
      this.activiteCommuneDataset.salariesActivite(entreprisesEtablissements).show(300);
   }
   
   /**
    * Salariés par code APE.
    */
   @Test
   @DisplayName("Dataset de salariés par code APE.")
   void salariesActiviteCommune() {
      OptionsCreationLecture options = this.entrepriseDataset.optionsCreationLecture();
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> entreprises = this.entrepriseDataset.rowEntreprisesEtEtablissements(options, historiqueExecution, new EntrepriseTriDepartementCommuneSirenSiret(),
            COG, ANNEE_SIRENE, true, true, true)
         .where("codeCommune = 14654");
     this.activiteCommuneDataset.salariesActivite(entreprises).show(5000);
     
     Dataset<ActiviteCommune> activites = this.activiteCommuneDataset.activitesParCommune(entreprises);
     assertNotEquals(0, activites.count(), "Au moins une activté aurait dû être déterminée");
     dump(activites, 5000, LOGGER);
   }
   
   /**
    * Nomenclature NAF.
    */
   @Test
   @DisplayName("Dataset de la nomenclature NAF.")
   void rowNomenclatureNAF() {
      Dataset<Row> resultat = this.nafDataset.rowNomenclatureNAF(ANNEE_SIRENE);
      assertNotEquals(0, resultat.count(), "Au moins un code NAF aurait dû être lu");
      resultat.show(2000, false);
   }
}
