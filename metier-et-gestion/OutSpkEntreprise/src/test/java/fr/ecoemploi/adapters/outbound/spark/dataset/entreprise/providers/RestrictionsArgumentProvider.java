package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise.providers;

import java.util.*;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.*;

import static org.junit.jupiter.params.provider.Arguments.arguments;

/**
 * Restrictions passées aux options de création et lecture de dataset
 * @author Marc Le Bihan
 */
public class RestrictionsArgumentProvider implements ArgumentsProvider {
   /**
    * Retourne des arguments pour les tests d'entreprises et établissements
    * @param extensionContext the current extension context; never {@code null}
    * @return type de restriction, communes, année
    */
   @Override
   public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
      Integer anneeLaPlusRecenteDisponible = 2024; // ArgumentsProvider de JUnit ne soutient pas l'injection Spring

      return Stream.of(
         arguments("France entière", new HashSet<String>(), null),
         arguments("Douarnenez", Set.of("29046"), anneeLaPlusRecenteDisponible),
         arguments("Douarnenez Communauté et Quimper Bretagne Occidentale", intercommunalite(), anneeLaPlusRecenteDisponible)
      );
   }

   /**
    * Renvoyer les communes d'une intercommunalité à tester en restriction
    * @return Communes de l'intercommunalité
    */
   private Set<String> intercommunalite() {
      String[] codesCommunes = {
         // Quimper Bretagne Occidentale (CA 2024)
         "29232", "29020", "29048", "29051", "29066",
         "29106", "29107", "29110", "29134", "29169",
         "29170", "29173", "29216", "29229",

         // Douanenez Communauté (CC 2024)
         "29046", "29087", "29090", "29224", "29226"
      };

      return new LinkedHashSet<>(Arrays.asList(codesCommunes));
   }
}
