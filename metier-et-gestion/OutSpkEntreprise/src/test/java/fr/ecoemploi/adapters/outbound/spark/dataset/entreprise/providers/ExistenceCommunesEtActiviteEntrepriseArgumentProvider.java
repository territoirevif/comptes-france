package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise.providers;

import java.util.*;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.*;

import static org.junit.jupiter.params.provider.Arguments.arguments;

/**
 * Restrictions et arguments sur l'existence des communes et l'activité des entreprises passées aux options de création et lecture de dataset
 * @author Marc Le Bihan
 */
public class ExistenceCommunesEtActiviteEntrepriseArgumentProvider implements ArgumentsProvider {
   /**
    * Retourne des arguments pour les tests d'entreprises et établissements
    * @param extensionContext the current extension context; never {@code null}
    * @return type de restriction, communes, année, communes valides ou non, entreprises actives ou non
    */
   @Override
   public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
      Integer anneeLaPlusRecenteDisponible = 2024; // ArgumentsProvider de JUnit ne soutient pas l'injection Spring

      return Stream.of(
         // type de restriction, codes communes, année, communes existantes, entreprises actives
         arguments("France entière, communes existantes, entreprises actives", new HashSet<String>(), null, true, true),
         arguments("France entière, communes existantes ou disparues, entreprises actives", new HashSet<String>(), null, false, true),
         arguments("France entière, communes existantes, entreprises actives ou inactives", new HashSet<String>(), null, true, false),
         arguments("France entière, communes existantes ou disparues, entreprises actives ou inactives", new HashSet<String>(), null, false, false),
         arguments("Douarnenez, communes existantes, entreprises actives", Set.of("29046"), anneeLaPlusRecenteDisponible, true, true),
         arguments("Douarnenez, communes existantes, entreprises actives ou inactives", Set.of("29046"), anneeLaPlusRecenteDisponible, true, false),

         arguments("Douarnenez Communauté et CA Quimper Bretagne Occidentale, communes existantes, entreprises actives",
            intercommunalite(), anneeLaPlusRecenteDisponible, true, true),

         arguments("Douarnenez Communauté et Quimper Bretagne Occidentale, communes existantes, entreprises actives ou inactives",
            intercommunalite(), anneeLaPlusRecenteDisponible, true, false)
      );
   }

   /**
    * Renvoyer les communes d'une intercommunalité à tester en restriction
    * @return Communes de l'intercommunalité
    */
   private Set<String> intercommunalite() {
      String[] codesCommunes = {
         // Quimper Bretagne Occidentale (CA 2024)
         "29232", "29020", "29048", "29051", "29066",
            "29106", "29107", "29110", "29134", "29169",
            "29170", "29173", "29216", "29229",

         // Douanenez Communauté (CC 2024)
         "29046", "29087", "29090", "29224", "29226"
      };

      return new LinkedHashSet<>(Arrays.asList(codesCommunes));
   }
}
