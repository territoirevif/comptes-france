package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import org.junit.jupiter.api.DisplayName;
import org.junit.platform.suite.api.*;

/**
 * Suite de tests pour les entreprises
 */
@Suite
@SelectClasses({
   EtablissementDatasetITCase.class, EntrepriseDatasetITCase.class,
   ActivitesCommuneITCase.class
})
@DisplayName("Entreprises, établissements et activités communales")
public class EntrepriseSuiteIT {
}
