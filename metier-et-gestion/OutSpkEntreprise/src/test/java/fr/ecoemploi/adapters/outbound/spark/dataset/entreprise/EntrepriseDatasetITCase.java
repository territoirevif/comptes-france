package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import static org.junit.jupiter.api.Assertions.*;

import java.io.Serial;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.slf4j.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.entreprise.providers.*;
import fr.ecoemploi.domain.model.territoire.entreprise.*;
import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

/**
 * Test sur les entreprises.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkEntrepriseTestApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
//@TestPropertySource("classpath:application-test_EntreprisesIT.properties")
class EntrepriseDatasetITCase extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5878582886504165631L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EntrepriseDatasetITCase.class);
   
   /** Session Spark. */
   @Autowired
   private SparkSession session;
   
   /** Dataset d'entreprises. */
   @Autowired
   private EntrepriseDataset entrepriseDataset;
   
   /** Dataset d'établissements. */
   @Autowired
   private EtablissementDataset etablissementDataset;
   
   /** Dataset des catégories juridiques. */
   @Autowired
   private CategorieJuridiqueDataset categorieJuridiqueDataset;

   /** True, s'il faut compter les partitions. */
   private final boolean compterPartitions = false;

   /** Année du cog pour les tests. */
   private static final int ANNEE_COG = 2024;
   
   /** Année du SIRENE pour les tests. */
   private static final int ANNEE_SIRENE = 2024;

   /** Siren de la mairie de Douarnenez */
   @SuppressWarnings("java:S3008") // Underscores dans le nom de variable statique
   protected static String MAIRIE_DOUARNENEZ_SIREN = "212900468";

   /** Siret de la mairie de Douarnenez */
   @SuppressWarnings("java:S3008") // Underscores dans le nom de variable statique
   protected static String MAIRIE_DOUARNENEZ_SIRET = "21290046800019";

   /** Siren de l'école maternelle de Douarnenez, rattaché à la mairie */
   @SuppressWarnings("java:S3008") // Underscores dans le nom de variable statique
   protected static String ECOLE_MATERNELLE_DOUARNENEZ_SIRET = "21290046800084";

   /**
    * Entreprises brutes, non filtrées.
    * @param typeRestriction Type de la restriction appliquée
    * @param communes Restriction aux communes, si non vide ou null
    * @param annee Restriction à l'année, si non null
    */
   @Order(10)
   @ParameterizedTest(name = "Entreprises non filtrées {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   @DisplayName("Dataset<Row> d'entreprises non filtrées (inclut les inactives).")
   void rowEntreprisesNonFiltrees(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.entrepriseDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      LOGGER.info("Lecture des entreprises non filtrées pour l'année {}, restriction : {} → {}, {}",
         ANNEE_SIRENE, typeRestriction, communes, annee);

      Dataset<Row> entreprises = this.entrepriseDataset.rowEntreprisesNonFiltrees(options, new HistoriqueExecution(), ANNEE_SIRENE);
      chrono("Chrono création store" , LOGGER);

      assertNotEquals(0, entreprises.count(), "Plusieurs entreprises non filtrées auraient du être lues.");
      chrono("Chrono vérification", LOGGER);

      testShow(entreprises, showTake(), explainPlan(), this.compterPartitions, false, LOGGER);
      chrono("Chrono show/explain", LOGGER);

      assertRelecture(entreprises, RELECTURE_PAR_SIREN, communes);
   }

   /**
    * Entreprises (actives ou inactives).
    * @param typeRestriction Type de la restriction appliquée
    * @param communes Restriction aux communes, si non vide ou null
    * @param annee Restriction à l'année, si non null
    * @param entreprisesActives true si l'on veut uniquement des entreprises actives<br>
    *        false si l'on veut aussi celles inactives
    */
   @Order(20)
   @DisplayName("Dataset<Row> d'entreprises")
   @ParameterizedTest(name = "Entreprises {0} : {1}, année : {2}, actives seulement : {3})")
   @ArgumentsSource(ActiviteEntrepriseArgumentProvider.class)
   void rowEntreprises(String typeRestriction, Set<String> communes, Integer annee, boolean entreprisesActives) {
      OptionsCreationLecture options = this.entrepriseDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      LOGGER.info("Lecture des entreprises {} pour l'année {}, restriction : {} → {}, {}",
         entreprisesActives ? "actives" : "actives ou inactives",
         ANNEE_SIRENE, typeRestriction, communes, annee);

      Dataset<Row> entreprises = this.entrepriseDataset.rowEntreprises(options, new HistoriqueExecution(), ANNEE_SIRENE, entreprisesActives, new EntrepriseTriSiren());
      chrono("Chrono création store" , LOGGER);

      assertNotEquals(0, entreprises.count(), "Plusieurs entreprises auraient du être lues.");
      chrono("Chrono vérification", LOGGER);

      testShow(entreprises, showTake(), explainPlan(), this.compterPartitions, false, LOGGER);
      chrono("Chrono show/explain", LOGGER);

      assertRelecture(entreprises, RELECTURE_PAR_SIREN, communes);
   }

   /**
    * Entreprises, sous forme d'objets.
    * @param typeRestriction Type de la restriction appliquée
    * @param communes Restriction aux communes, si non vide ou null
    * @param annee Restriction à l'année, si non null
    * @param entreprisesActives true si l'on veut uniquement des entreprises actives<br>
    *        false si l'on veut aussi celles inactives
    */
   @Order(30)
   @DisplayName("Dataset<Entreprise>")
   @ParameterizedTest(name = "Entreprises {0} : {1}, année : {2}, actives seulement : {3})")
   @ArgumentsSource(ActiviteEntrepriseArgumentProvider.class)
   void entreprises(String typeRestriction, Set<String> communes, Integer annee, boolean entreprisesActives) {
      OptionsCreationLecture options = this.entrepriseDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      LOGGER.info("Lecture des entreprises {} sous forme d'objets pour l'année {}, restriction : {} → {}, {}",
         entreprisesActives ? "actives" : "actives ou inactives",
         ANNEE_SIRENE, typeRestriction, communes, annee);

      Dataset<Entreprise> datasetEntreprises = this.entrepriseDataset.entreprisesSeules(options, new HistoriqueExecution(), new EntrepriseTriSiren(), null,
         ANNEE_SIRENE, entreprisesActives).limit(10000);
      chrono("Chrono création store" , LOGGER);

      assertNotEquals(0, datasetEntreprises.count(), "Plusieurs entreprises filtrées auraient du être lues.");
      chrono("Chrono vérification", LOGGER);

      dump(datasetEntreprises, 20, LOGGER);

      List<Entreprise> entreprises = datasetEntreprises.collectAsList();

      for(Entreprise entreprise : entreprises) {
         assertNotNull(entreprise.getSiren(), "Le numéro SIREN de l'entreprise ne devrait pas valoir null");
      }
   }

   /**
    * Composer un Dataset d'entreprises avec leurs établissements, tous tris.
    * @param communes Restriction aux communes, si non vide ou null
    * @param annee Restriction à l'année, si non null
    * @param communesExistantes true si l'on veut uniquement des communes existantes (pouvant inclure les déléguées ou associées)<br>
    *        false si l'on veut aussi celles disparues
    * @param entreprisesActives true si l'on veut uniquement des entreprises actives<br>
    *        false si l'on veut aussi celles inactives
    */
   @Order(40)
   @DisplayName("Dataset<Row> d'entreprises avec établissements")
   @ParameterizedTest(name = "Entreprises avec établissements {0} : {1}, année : {2}, actives seulement : {4}, de communes existantes (possiblement déléguée ou associées) seulement : {3})")
   @ArgumentsSource(ExistenceCommunesEtActiviteEntrepriseArgumentProvider.class)
   void rowEntreprisesJoinEtablissements(String typeRestriction, Set<String> communes, Integer annee, boolean communesExistantes, boolean entreprisesActives) {
      OptionsCreationLecture options = this.entrepriseDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      LOGGER.info("Lecture des entreprises jointes à leurs établissements {}, sur communes {}, pour l'année {}, restriction : {} → {}, {}",
         entreprisesActives ? "actives" : "actives ou inactives",
         communesExistantes ? "communes existantes (possiblement déléguées et associées)" : "communes existantes (possiblement déléguées et associées) ou disparues",
         ANNEE_SIRENE, typeRestriction, communes, annee);

      EntrepriseTri[] tris = {
         new EntrepriseTriSirenSiret(),
         new EntrepriseTriDepartementSirenSiret(),
         new EntrepriseTriDepartementEPCISirenSiret(),

         new EntrepriseTriDepartementCommuneSirenSiret()
      };

      for(EntrepriseTri tri : tris) {
         // Si la restriction aux communes valides n'est pas demandée, il ne pourra pas y avoir de tri par EPCI
         if (tri instanceof EntrepriseTriDepartementEPCISirenSiret && communesExistantes == false) {
            continue;
         }

         rowEntreprisesJoinEtablissementsCommunesEtEntreprises(options, new HistoriqueExecution(), tri, communesExistantes, entreprisesActives, true, communes);

         if (tousTris() == false) {
            return;
         }
      }
   }

   /**
    * Composer un Dataset d'entreprises avec leurs établissements répartis
    * @param communesExistantes true si l'on veut uniquement des communes existantes (pouvant inclure les déléguées ou associées)<br>
    *        false si l'on veut aussi celles disparues
    * @param entreprisesActives true si l'on veut uniquement des entreprises actives<br>
    *        false si l'on veut aussi celles inactives
    * @param nomenclaturesNAF2Valides true s'il faut restreindre aux établissements dont les codes NAF sont des NAF2 et valides
    * @param options Options de création et de lecture de dataset
    * @param historique Historique d'exécution
    * @param tri Tri
    */
   private void rowEntreprisesJoinEtablissementsCommunesEtEntreprises(OptionsCreationLecture options, HistoriqueExecution historique,
      EntrepriseTri tri, boolean communesExistantes, boolean entreprisesActives, boolean nomenclaturesNAF2Valides, Set<String> restrictionCommunes) {

      Dataset<Row> entreprisesEtEtablissements = this.entrepriseDataset.rowEntreprisesEtEtablissements(options, historique, tri,
         ANNEE_COG, ANNEE_SIRENE, entreprisesActives, communesExistantes, nomenclaturesNAF2Valides);
      chrono("Chrono création store" , LOGGER);

      assertNotEquals(0, entreprisesEtEtablissements.count(),
         MessageFormat.format("Plusieurs entreprises {0} avec leurs établissements {1} (triées par {2}) auraient du être lues.",
            communesExistantes ? "valides" : "invalides", entreprisesActives ? "actives" : "inactives", tri));
      chrono("Chrono vérification", LOGGER);

      testShow(entreprisesEtEtablissements, showTake(), explainPlan(), this.compterPartitions, false, LOGGER);
      chrono("Chrono show/explain", LOGGER);

      assertRelecture(entreprisesEtEtablissements, RELECTURE_PAR_SIREN_SIRET, restrictionCommunes);
   }

   /**
    * Joindre entreprises et établissements.
    * @param communes Restriction aux communes, si non vide ou null
    * @param annee Restriction à l'année, si non null
    * @param communesExistantes true si l'on veut uniquement des communes existantes (pouvant inclure les déléguées ou associées)<br>
    *        false si l'on veut aussi celles disparues
    * @param entreprisesActives true si l'on veut uniquement des entreprises actives<br>
    *        false si l'on veut aussi celles inactives
    */
   @Order(50)
   @DisplayName("Dataset<Entreprise> avec établissements")
   @ParameterizedTest(name = "Entreprises avec établissements {0} : {1}, année : {2}, actives seulement : {4}, de communes existantes (possiblement déléguée ou associées) seulement : {3})")
   @ArgumentsSource(ExistenceCommunesEtActiviteEntrepriseArgumentProvider.class)
   void entreprisesJoinWithEtablissements(String typeRestriction, Set<String> communes, Integer annee, boolean communesExistantes, boolean entreprisesActives) {
      OptionsCreationLecture options = this.entrepriseDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      LOGGER.info("Lecture des entreprises jointes à leurs établissements, en objets {}, sur communes {}, pour l'année {}, restriction : {} → {}, {}",
         entreprisesActives ? "actives" : "actives ou inactives",
         communesExistantes ? "communes existantes (possiblement déléguées et associées)" : "communes existantes (possiblement déléguées et associées) ou disparues",
         ANNEE_SIRENE, typeRestriction, communes, annee);

      // Sur une sélection assez vaste d'entreprises et établissements
      Column restriction = col(SIREN_ENTREPRISE.champ()).substr(0, 2).isin("79", "32", "21");
      assertEntreprisesJoinWithEtablissements(options, new HistoriqueExecution(), restriction, restriction, -1, false, false, communes);

      // Sur le jeu d'essai
      Entrees entrees = elementsPresenceVerifiee(communes);
      assertEntreprisesJoinWithEtablissements(options, new HistoriqueExecution(), null, null, entrees.nombreEntreprisesDistinctes(), true, true, communes);

      // Sur la Mairie de Douarnenez et ses établissements : mairie et école maternelle
      Column selectionEntreprises = col(SIREN_ENTREPRISE.champ()).equalTo(MAIRIE_DOUARNENEZ_SIREN);

      Column selectionEtablissements = col(SIRET_ETABLISSEMENT.champ()).equalTo(MAIRIE_DOUARNENEZ_SIRET)
         .or(col(SIRET_ETABLISSEMENT.champ()).equalTo(ECOLE_MATERNELLE_DOUARNENEZ_SIRET));

      assertEntreprisesJoinWithEtablissements(options, new HistoriqueExecution(), selectionEntreprises, selectionEtablissements, 1, false, true, communes);
   }

   /**
    * Obtenir un dataset de catégories juridiques.
    */
   @Test @Order(5)
   @DisplayName("Dataset<Row> de catégories juridiques.")
   void rowCategoriesJuridiques() {
      Dataset<Row> categoriesJuridiques = this.categorieJuridiqueDataset.rowCategoriesJuridiques(this.session, ANNEE_SIRENE);
      categoriesJuridiques.show(1000, false);

      assertNotEquals(0, categoriesJuridiques.count(), "Au moins une catégorie juridique aurait dû être trouvée");
   }

   /**
    * Vérifier la bonne jonction entreprises et établissements dans l'objet entreprises créé.
    * @param selectionEntreprises Condition de sélection sur les entreprises, peut valoir null.
    * @param selectionEtablissements Condition de sélection sur les établissements, peut valoir null.
    * @param nombreEntreprisesAttendues Le nombre d'entreprises qui est attendu en réponse à cette sélection.
    * @param relireJeuInitial true s'il faut relire le jeu initial
    * @param restrictionCommunes Restriction communes appliquées.
    * @param testerNombreEntreprises true s'il faut tester le nombre d'entreprises
    */
   void assertEntreprisesJoinWithEtablissements(OptionsCreationLecture options, HistoriqueExecution historique,
      Column selectionEntreprises, Column selectionEtablissements, int nombreEntreprisesAttendues, boolean relireJeuInitial, boolean testerNombreEntreprises,
      Set<String> restrictionCommunes) {

      // Extraction des deux datasets
      Dataset<Entreprise> dsEntreprise = this.entrepriseDataset.entreprisesSeules(options, historique, new EntrepriseTriSiren(),
         selectionEntreprises,
         ANNEE_SIRENE, true);

      Dataset<Etablissement> dsEtablissements = this.etablissementDataset.etablissements(options, historique, new EtablissementTriSirenSiret(),
         selectionEtablissements,
         ANNEE_COG, ANNEE_SIRENE, true, true, true);

      chrono("Chrono création store" , LOGGER);

      if (relireJeuInitial) {
         dsEtablissements = assertRelecture(dsEtablissements, RELECTURE_PAR_SIREN_SIRET, restrictionCommunes);
      }

      // Jonction des deux datasets
      Entreprises entreprises = this.entrepriseDataset.entreprises(dsEntreprise, dsEtablissements);

      if (testerNombreEntreprises) {
         assertEquals(nombreEntreprisesAttendues, entreprises.size(), "Le nombre d'entreprises (avec leurs établissements) relu n'est pas celui attendu.");
      }

      if (entreprises.size() < 300) {
         for(Entreprise entreprise : entreprises) {
            LOGGER.info(entreprise.toString());
            entreprise.getEtablissements().values().forEach(etablissement -> LOGGER.info("\t{}", etablissement));
         }
      }

      assertMairieDeDouarnenezEtSesEtablissements(entreprises);
      chrono("Chrono vérification", LOGGER);
   }

   private static final int RELECTURE_PAR_SIREN = 0;
   private static final int RELECTURE_PAR_SIREN_SIRET = 1;
   private static final int RELECTURE_PAR_DEPARTEMENT_SIREN_SIRET = 2;
   private static final int RELECTURE_PAR_DEPARTEMENT_COMMUNE = 3;

   /**
    * Relecture avec vérification d'un dataset d'entreprises.
    * @param entreprises Dataset d'entreprises.
    * @param choixRelecture Choix de relecture.
    * @param restrictionCommunes Restriction communes appliquées.
    * @return Dataset contenant ce qui a été effectivement relu.
    */
   private <T> Dataset<T> assertRelecture(Dataset<T> entreprises, int choixRelecture, Set<String> restrictionCommunes) {
      Entrees entrees = elementsPresenceVerifiee(restrictionCommunes);

      Column whereClause;
      Iterator<Entree> itEntree = entrees.iterator();

      switch(choixRelecture) {
         case RELECTURE_PAR_SIREN: {
            EntrepriseTriSiren tri = new EntrepriseTriSiren();
            String siret = itEntree.next().siret;
            String siren = siret.substring(0, 9);
            whereClause = tri.equalTo(siren);

            while(itEntree.hasNext()) {
               siret = itEntree.next().siret;
               siren = siret.substring(0, 9);
               whereClause = whereClause.or(tri.equalTo(siren));
            }

            break;
         }

         case RELECTURE_PAR_SIREN_SIRET: {
            EntrepriseTriSirenSiret tri = new EntrepriseTriSirenSiret();
            String siret = itEntree.next().siret;
            String siren = siret.substring(0, 9);

            whereClause = tri.equalTo(siren, siret);

            while(itEntree.hasNext()) {
               siret = itEntree.next().siret;
               siren = siret.substring(0, 9);
               whereClause = whereClause.or(tri.equalTo(siren, siret));
            }

            break;
         }

         case RELECTURE_PAR_DEPARTEMENT_SIREN_SIRET: {
            EntrepriseTriDepartementSirenSiret tri = new EntrepriseTriDepartementSirenSiret();
            Entree entree = itEntree.next();
            String departement = entree.departement;
            String siret = entree.siret;
            String siren = siret.substring(0, 9);
            whereClause = tri.equalTo(departement, siren, siret);

            while(itEntree.hasNext()) {
               entree = itEntree.next();
               departement = entree.departement;
               siret = entree.siret;
               siren = siret.substring(0, 9);
               whereClause = whereClause.or(tri.equalTo(departement, siren, siret));
            }

            break;
         }

         case RELECTURE_PAR_DEPARTEMENT_COMMUNE: {
            EntrepriseTriDepartementCommuneSirenSiret tri = new EntrepriseTriDepartementCommuneSirenSiret();
            Entree entree = itEntree.next();
            String departement = entree.departement;
            String codeCommune = entree.codeCommune;
            String siret = entree.siret;
            String siren = siret.substring(0, 9);
            whereClause = tri.equalTo(departement, codeCommune, siren, siret);

            while(itEntree.hasNext()) {
               entree = itEntree.next();
               departement = entree.departement;
               codeCommune = entree.codeCommune;
               siret = entree.siret;
               siren = siret.substring(0, 9);
               whereClause = whereClause.or(tri.equalTo(departement, codeCommune, siren, siret));
            }

            break;
         }

         default:
            throw new UnsupportedOperationException(MessageFormat.format("Il n''existe pas de relecture avec le choix {0}", choixRelecture));
      }

      LOGGER.info("Lecture des entreprises avec la requête : '{}'", whereClause);

      Dataset<T> lecture = entreprises.where(whereClause);
      List<Row> rowsLus = lecture.toDF().collectAsList();
      chrono("Chrono lecture", LOGGER);

      testShow(lecture, showTake(), explainPlan(), this.compterPartitions, false, LOGGER);
      chrono("Chrono show/explain", LOGGER);

      Set<String> resultats = new HashSet<>();
      rowsLus.forEach(row -> resultats.add(row.getAs("siren")));

      entrees.forEach(entree -> assertTrue(resultats.contains(entree.siret.substring(0,9)),
         MessageFormat.format("Un des siren demandé, {0}, n''est pas parmi les résultats", entree.siret.substring(0,9))));

      return lecture;
   }

   /**
    * Vérifier que dans les entreprises, la Mairie de Douarnenez a bien parmi ses établissements :<br>
    *    <li>La mairie elle-même</li>
    *    <li>L'école maternelle</li>
    * @param entreprisesAvecEtablissements Les entreprises avec établissements
    */
   private void assertMairieDeDouarnenezEtSesEtablissements(Entreprises entreprisesAvecEtablissements) {
      // Lire l'entreprise de siren 212900468 : c'est la Mairie
      Entreprise mairie = entreprisesAvecEtablissements.get(MAIRIE_DOUARNENEZ_SIREN);
      assertNotNull(mairie, "La mairie de Douarnenez n'a pas été trouvée dans les objets Entreprise créés pour la ville");

      // Elle doit avoir deux établissements : 21290046800019 (elle-même) et 21290046800084 (l'école maternelle)
      Etablissement elleMeme = mairie.getEtablissements().get(MAIRIE_DOUARNENEZ_SIRET);
      assertNotNull(elleMeme, "L'établissement 'mairie' n'est pas parmi ceux de l'entreprise Mairie de Douarnenez");

      Etablissement ecole = mairie.getEtablissements().get(ECOLE_MATERNELLE_DOUARNENEZ_SIRET);
      assertNotNull(ecole, "L'établissement 'école maternelle' n'est pas parmi ceux de l'entreprise Mairie de Douarnenez");
   }

   /**
    * Entrée de jeu d'essai
    */
   static class Entree {
      Entree(String departement, String codeCommune, String siret) {
         this.siret = siret;
         this.codeCommune = codeCommune;
         this.departement = departement;
      }

      final String siret;
      final String departement;
      final String codeCommune;
   }

   /**
    * Entrées de jeu d'essai
    */
   static class Entrees extends LinkedHashSet<Entree> {
      @Serial
      private static final long serialVersionUID = -7141729008688639756L;

      Entrees(Entree... entrees) {
         this.addAll(Arrays.asList(entrees));
      }

      /**
       * Renvoyer le nombre d'entreprises distinctes présentes dans la section vérifiée escomptée
       * @return Nombre de siren distincts
       */
      public int nombreEntreprisesDistinctes() {
         Stream<String> restriction = this.stream().map(e -> e.siret.substring(0,9));
         return restriction.collect(Collectors.toSet()).size();
      }
   }

   /**
    * Renvoyer la liste des éléments dont ont veut s'assurer de la présence dans le dataset de résultat
    * @param restrictionCommunes Restriction aux communes
    * @return Eléments dont va vérifer la présence
    */
   private Entrees elementsPresenceVerifiee(Set<String> restrictionCommunes) {
      Entrees entrees = new Entrees(new Entree("71", "71076", "79384240200018"),
         new Entree("29", "29160", "32067374200013"),
         new Entree("75", "75109", "32025248901729"),
         new Entree("29", "29046", MAIRIE_DOUARNENEZ_SIRET),
         new Entree("29", "29046", ECOLE_MATERNELLE_DOUARNENEZ_SIRET));

      if (restrictionCommunes != null && restrictionCommunes.isEmpty() == false) {
         entrees.removeIf(e -> restrictionCommunes.contains(e.codeCommune) == false);
      }

      return entrees;
   }

   /**
    * Avant chaque test
    */
   @BeforeEach
   void beforeEach() {
      super.chrono.start();
   }
}
