package fr.ecoemploi.adapters.outbound.spark.dataset.entreprise;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Stream;

import org.junit.jupiter.api.*;
import org.slf4j.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import org.apache.spark.sql.*;

import fr.ecoemploi.domain.model.territoire.entreprise.Etablissement;

import fr.ecoemploi.adapters.outbound.spark.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.stream.Paginateur;

/**
 * Test sur les établissements.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkEntrepriseTestApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
//@TestPropertySource("classpath:application-test_EntreprisesIT.properties")
class EtablissementDatasetITCase extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -8388514658120285553L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EtablissementDatasetITCase.class);

   /** Dataset d'établissements. */
   @Autowired
   private EtablissementDataset etablissementDataset;

   /** Année du cog pour les tests. */
   private final static int COG = 2024;

   /** Année du SIRENE pour les tests. */
   private final static int ANNEE_SIRENE = 2024;

   /** Année sirene de départ pour les tests. */
   @Value("${test.annee.sirene.debut:2024}")
   private int sireneAnneeDebut;

   /** Année sirene de fin pour les tests. */
   @Value("${test.annee.sirene.fin:2024}")
   private int sireneAnneeFin;

   /** True, s'il faut compter les partitions. */
   private final boolean compterPartitions = false;

   /**
    * Test d'un overflow sur un grand jeu de données.
    */
   @Test @Disabled("Eviter de jouer sans raison : il dure près de deux heures")
   @DisplayName("Pas d'Overflow sur grand jeu de données")
   void sans_overflow() {
      assertDoesNotThrow(() -> {
         HistoriqueExecution historiqueExecution = new HistoriqueExecution();

         Stream<Etablissement> etablissements = Paginateur.paginerEnStream(this.etablissementDataset.etablissements(null, historiqueExecution, new EtablissementTriSirenSiret(), null,
            COG, ANNEE_SIRENE, true, false, false),
            p -> this.etablissementDataset.toDatasetObjetsMetiers(p), 10000);

         // Le filter traverse volontairement tous les datasets (il est très mauvais en optimisation, de fait).
         // Son but est que chacun ait vu ses données extraites par le stream.
         etablissements = etablissements.filter(etablissement -> etablissement.getActivitePrincipale().equals("70.22Z"));

         Spliterator<Etablissement> itEtablissements = etablissements.spliterator();
         while(itEtablissements.tryAdvance(etablissement -> LOGGER.info(etablissement.toString())));
      });
   }

   /**
    * Objets etablissements indifférents.
    */
   @Test @Order(50)
   @DisplayName("Etablissements indifférents.")
   void etablissements() {
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Etablissement> etablissements = this.etablissementDataset.etablissements(null, historiqueExecution, new EtablissementTriDepartementSirenSiret(), null,
         COG, ANNEE_SIRENE, true, false, false);

      assertNotEquals(0, etablissements.count(), "Plusieurs établissements auraient du être lus.");

      testShow(etablissements, showTake(), explainPlan(), false, true, LOGGER);
      assertRelecture(etablissements, RELECTURE_PAR_DEPARTEMENT_SIREN_SIRET);
      historiqueExecution.dumpNotifications(new HistoriqueExecution.DumpDestinationLogger(LOGGER), true);
   }

   /**
    * Objets établissements actifs aux communes valides.
    */
   @Test @Order(60)
   @DisplayName("Etablissements actifs.")
   void etablissementsActifs() {
      OptionsCreationLecture options = this.etablissementDataset.optionsCreationLecture();
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Etablissement> etablissements = this.etablissementDataset.etablissements(options, historiqueExecution, new EtablissementTriDepartementSirenSiret(), null,
         COG, ANNEE_SIRENE, true, true, false);

      assertNotEquals(0, etablissements.count(), "Plusieurs établissements auraient du être lus.");

      testShow(etablissements, showTake(), explainPlan(), false, true, LOGGER);
      assertRelecture(etablissements, RELECTURE_PAR_DEPARTEMENT_SIREN_SIRET);
      historiqueExecution.dumpNotifications(new HistoriqueExecution.DumpDestinationLogger(LOGGER), true);
   }

   /**
    * Etablissements (Row) non filtrés.
    */
   @Test @Order(10)
   @DisplayName("Rows établissements non filtrés.")
   void rowEtablissementsNonFiltres() {
      for(int anneeSirene = this.sireneAnneeDebut; anneeSirene <= this.sireneAnneeFin; anneeSirene ++) {
         if (toutesAnnees() == false && anneeSirene != this.sireneAnneeFin) {
            continue;
         }

         LOGGER.info("***************************");
         LOGGER.info("* TEST DATASET ANNEE {} *", anneeSirene);
         LOGGER.info("***************************");

         // Ecriture d'établissements
         OptionsCreationLecture options = this.etablissementDataset.optionsCreationLecture();
         options.useCache(false);

         Dataset<Row> etablissements = this.etablissementDataset.etablissementsNonFiltres(options, new HistoriqueExecution(), anneeSirene);
         chrono("Chrono création store", LOGGER);

         assertFalse(etablissements.isEmpty(), "Des établissements auraient du être lus.");
         chrono("Chrono vérification", LOGGER);

         testShow(etablissements, showTake(), explainPlan(), this.compterPartitions, false, LOGGER);
         chrono("Chrono show/explain", LOGGER);

         assertRelecture(etablissements, RELECTURE_PAR_SIREN_SIRET);
      }
   }

   /**
    * Etablissements (Row) actifs, aux communes valides ou non.
    */
   @Test @Order(20)
   @DisplayName("Rows établissements indifférents, communes indifférentes.")
   void rowEtablissementsIndifferentsCommunesIndifferentes() {
      assertRelecture(rowEtablissements(false, false, false), RELECTURE_PAR_DEPARTEMENT_SIREN_SIRET);
   }

   /**
    * Etablissements (Row) actifs, aux communes valides ou non.
    */
   @Test @Order(30)
   @DisplayName("Rows établissements valides, communes indifférentes.")
   void rowEtablissementsActifsCommunesIndifferentes() {
      assertRelecture(rowEtablissements(true, false, false), RELECTURE_PAR_DEPARTEMENT_SIREN_SIRET);
   }

   /**
    * Etablissements (Row) actifs aux communes valides.
    */
   @Test @Order(40)
   @DisplayName("Rows établissements actifs, communes valides.")
   void rowEtablissementsActifsCommunesValides() {
      assertRelecture(rowEtablissements(true, true, false), RELECTURE_PAR_DEPARTEMENT_SIREN_SIRET);
   }

   /**
    * Etablissements (Row) actifs aux communes valides.
    */
   @Test @Order(50)
   @DisplayName("Rows établissements actifs, communes valides, naf 2 valides.")
   void rowEtablissementsActifsCommunesNaf2Valides() {
      assertRelecture(rowEtablissements(true, true, true), RELECTURE_PAR_DEPARTEMENT_SIREN_SIRET);
   }

   /**
    * Test d'établissements Row
    * @param etablissementsActifs true si seuls les établissements actifs doivent être retenus
    * @param communesValides true si seules les communes valides doivent être retenues
    * @param nomenclaturesNAF2Valides true s'il faut restreindre aux établissements dont les codes NAF sont des NAF2 et valides.
    * @return Dataset d'établissements constitué.
    */
   private Dataset<Row> rowEtablissements(boolean etablissementsActifs, boolean communesValides, boolean nomenclaturesNAF2Valides) {
      HistoriqueExecution historiqueExecution = new HistoriqueExecution();

      Dataset<Row> etablissements = this.etablissementDataset.rowEtablissements(null, historiqueExecution, new EtablissementTriDepartementCommune(), COG, ANNEE_SIRENE,
         etablissementsActifs, communesValides, nomenclaturesNAF2Valides);

      chrono("Chrono création store" , LOGGER);

      assertFalse(etablissements.isEmpty(), "Plusieurs établissements auraient du être lus.");
      chrono("Chrono vérification", LOGGER);

      LOGGER.info("Résultat: établissements {} aux communes {} et codes NAF 2 {}.",
         etablissementsActifs ? "actifs" : "actifs ou inactifs",
         communesValides ? "valides" : "valides ou non",
         nomenclaturesNAF2Valides ? "valides" : "valides ou non");

      testShow(etablissements, showTake(), explainPlan(), this.compterPartitions, false, LOGGER);
      chrono("Chrono show/explain", LOGGER);
      historiqueExecution.dumpNotifications(new HistoriqueExecution.DumpDestinationLogger(LOGGER), true);

      return etablissements;
   }

   private static final int RELECTURE_PAR_SIREN_SIRET = 0;
   private static final int RELECTURE_PAR_DEPARTEMENT_SIREN_SIRET = 1;
   private static final int RELECTURE_PAR_DEPARTEMENT_COMMUNE = 2;

   /**
    * Relecture avec vérification d'un dataset d'établissements.
    * @param etablissements Dataset d'établissements.
    * @param choixRelecture Choix de relecture.
    */
   private <T> void assertRelecture(Dataset<T> etablissements, int choixRelecture) {
      // Relecture
      String[] sirets = {"79384240200018", "32067374200013", "32025248901729",
         "21290046800019", "21290046800084" // Ces deux établissements, actifs, appartiennent à la même entreprise, active, dans la même ville.
      };
      String[] departements = {"71", "29", "75", "29", "29"};
      String[] codesCommunes = {"71076", "29160", "75109", "29046", "29046"};

      Column whereClause;

      switch(choixRelecture) {
         case RELECTURE_PAR_SIREN_SIRET: {
            EtablissementTriSirenSiret tri = new EtablissementTriSirenSiret();

            whereClause = tri.equalTo(sirets[0].substring(0, 9), sirets[0]);

            for(int index=1; index < sirets.length; index ++) {
               whereClause = whereClause.or(tri.equalTo(sirets[index].substring(0, 9), sirets[index]));
            }

            break;
         }

         case RELECTURE_PAR_DEPARTEMENT_SIREN_SIRET: {
            EtablissementTriDepartementSirenSiret tri = new EtablissementTriDepartementSirenSiret();

            whereClause = tri.equalTo(departements[0], sirets[0].substring(0, 9), sirets[0]);

            for(int index=1; index < sirets.length; index ++) {
               whereClause = whereClause.or(tri.equalTo(departements[index], sirets[index].substring(0, 9), sirets[index]));
            }

            break;
         }

         case RELECTURE_PAR_DEPARTEMENT_COMMUNE: {
            EtablissementTriDepartementCommune tri = new EtablissementTriDepartementCommune();

            whereClause = tri.equalTo(departements[0], codesCommunes[0], sirets[0].substring(0, 9), sirets[0]);

            for(int index=1; index < sirets.length; index ++) {
               whereClause = whereClause.or(tri.equalTo(departements[index], codesCommunes[index], sirets[index].substring(0, 9), sirets[index]));
            }

            break;
         }

         default:
            throw new UnsupportedOperationException(MessageFormat.format("Il n''existe pas de relecture avec le choix {0}", choixRelecture));
      }

      LOGGER.info("Lecture des établissements avec la requête : '{}'", whereClause);
      Dataset<T> lecture = etablissements.where(whereClause);
      List<Row> rowsLus = lecture.toDF().collectAsList();
      chrono("Chrono lecture", LOGGER);

      testShow(lecture, showTake(), explainPlan(), this.compterPartitions, false, LOGGER);
      chrono("Chrono show/explain", LOGGER);

      assertEquals(sirets.length, rowsLus.size(), "En relecture sur des sirets de test, il n'y a pas autant d'établissements que demandés");

      Set<String> resultats = new HashSet<>();
      rowsLus.forEach(row -> resultats.add(row.getAs("siret")));
      Arrays.stream(sirets).forEach(siret -> assertTrue(resultats.contains(siret), MessageFormat.format("Un des siret demandé, {0}, n''est pas parmi les résultats", siret)));
   }

   /**
    * Avant chaque test
    */
   @BeforeEach
   void beforeEach() {
      super.chrono.start();
   }
}
