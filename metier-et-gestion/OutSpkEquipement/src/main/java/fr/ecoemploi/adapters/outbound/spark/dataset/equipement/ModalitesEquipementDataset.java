package fr.ecoemploi.adapters.outbound.spark.dataset.equipement;

import java.io.*;
import java.util.function.Supplier;
import org.slf4j.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.verifications.Verification;

/**
 * Dataset de lecture des modalités de la base équipements. 
 * @author Marc Le Bihan
 */
@Service
public class ModalitesEquipementDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -912894465071155131L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(ModalitesEquipementDataset.class);

   /** Chargeur de fichier CSV de modalités. */
   private final ModalitesRowCsvLoader rowCsvLoader;

   /**
    * Code des variables d'équipement définies dans les modalités.
    */
   public enum CodeVariableEquipement {
      /** Année */
      ANNEE("AN", "Annee"),
      
      /** Iris. */
      IRIS("DCIRIS", "Iris"),
      
      /** Département. */
      DEPARTEMENT("DEP", "Departement"),
      
      /** Code commune. */
      CODE_COMMUNE("DEPCOM", "Commune"),
      
      /** Coordonnée Lambert X. */
      COORDONNEE_LAMBERT_X("LAMBERT_X", "LambertX"),
      
      /** Coordonnée Lambert Y. */
      COORDONNEE_LAMBERT_Y("LAMBERT_Y", "LambertY"),
      
      /** Qualité de la géolocalisation. */
      QUALITE_LOCALISATION("QUALITE_XY", "Qualite"),
      
      /** Code région. */
      CODE_REGION("REG", "Region"),

      /** Type d'équipement : A104 Gendarmerie */
      TYPE_EQUIPEMENT("TYPEQU", "TypeEquipement");
      
      /** Code de la variable. */
      private final String codeVariable;
      
      /** Suffixe du champ si les noms des champs porteurs des modalités sont renommés :
       *  exemple : codeModalite et libelleModalite deviennent codeTypeEquipement et libelleEquipement. */
      private final String suffixeNomChamp;
      
      /**
       * Construire un type variable d'équipement.
       * @param codeVariable Code de la variable.
       * @param suffixeNomChamp Suffixe du nom du champ par lequel renommer le code ou le libellé de la modalité, quand c'est désiré<br>
       * Exemple : codeModalite et libelleModalite deviennent codeTypeEquipement et libelleEquipement.
       */
      CodeVariableEquipement(String codeVariable, String suffixeNomChamp) {
         this.codeVariable = codeVariable;
         this.suffixeNomChamp = suffixeNomChamp;
      }
      
      /**
       * Renvoyer le code de la variable.
       * @return Code de la variable.
       */
      public String getCodeVariable() {
         return this.codeVariable;
      }
      
      /**
       * Renvoyer le suffixe du champ par lequel renommer le code ou le libellé de la modalité, quand c'est désiré.
       * @return Suffixe du champ.
       */
      public String getSuffixeNomChamp() {
         return this.suffixeNomChamp;
      }
   }

   /**
    * Construire un dataset de modalités de fichier d'équipement.
    * @param rowCsvLoader Chargeur de modalités depuis un fichier CSV.
    */
   @Autowired
   public ModalitesEquipementDataset(ModalitesRowCsvLoader rowCsvLoader) {
      this.rowCsvLoader = rowCsvLoader;
   }

   /**
    * Obtenir un Dataset Row des types d'équipements.
    * @param anneeEquipements Année pour laquelle rechercher les équipements.
    * @param verifications Vérifications optionnelles.
    * @param optionsCreationLecture Options de lecture et de constitution du dataset.
    * @param historique Historique d'exécution de la constitution de ce dataset.
    * @return Ensemble des données attributaires accessibles sur les équipements.
    */
   public Dataset<Row> rowLibellesTypesEquipements(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeEquipements, Verification... verifications) {
      return rowModalites(optionsCreationLecture, historique, CodeVariableEquipement.TYPE_EQUIPEMENT, anneeEquipements, true, true, verifications);
   }

   /**
    * Obtenir un Dataset Row des types d'équipements.
    * @param codeVariable Code variable optionnel : les modalités seront restreintes à lui, et si un suffixe est présent, les champs renommés.
    * @param anneeEquipements Année pour laquelle rechercher les équipements.
    * @param renommerChamps true s'il faut renommer les champs avec leur suffixe.
    * @param restreindreAuxColonnesCodeLibelle true s'il faut restreindre les colonnes renvoyées à celles code et libellé de la modalité.
    * @param verifications Vérifications optionnelles.
    * @param optionsCreationLecture Options de lecture et de constitution du dataset.
    * @param historique Historique d'exécution de la constitution de ce dataset.
    * @return Ensemble des données attributaires accessibles sur les équipements.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> rowModalites(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique,
      CodeVariableEquipement codeVariable, int anneeEquipements, boolean renommerChamps, boolean restreindreAuxColonnesCodeLibelle, Verification... verifications) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      Supplier<Dataset<Row>> worker = () -> {
         LOGGER.info("Constitution du dataset des modalités de la base équipement de l'année {}...", anneeEquipements);

         Dataset<Row> modalites = this.rowCsvLoader.loadOpenData(anneeEquipements);
         verificateur().verifications("lecture csv", modalites, verifications);

         String champCode = "codeModalite";
         String champLibelle = "libelleModalite";

         // Dans le fichier des modalités de 2018, la modalité QUALITE_XY contient un caractère parasite, qu'il faut retirer.
         if (anneeEquipements == 2018) {
            LOGGER.warn("Les variables de modalité d'équipement 2018 portent une énuération QUALITE_XY avec un caractère parasite. Correction.");

            modalites = modalites.withColumnRenamed("codeTypeModalite", "codeTypeModaliteAcorriger");
            Column correctionQualitesInvalides = when(modalites.col("codeTypeModaliteAcorriger").like("QUALITE_XY%"), lit("QUALITE_XY")).otherwise(modalites.col("codeTypeModaliteAcorriger"));
            modalites = modalites.withColumn("codeTypeModalite", correctionQualitesInvalides)
               .drop("codeTypeModaliteAcorriger");
         }

         // Si un code variable équipement est défini, filtrer sur celui-ci.
         if (codeVariable != null) {
            modalites = modalites.filter(col("codeTypeModalite").equalTo(codeVariable.getCodeVariable()));

            if (renommerChamps && codeVariable.getSuffixeNomChamp() != null) {
               champCode = "code" + codeVariable.getSuffixeNomChamp();
               champLibelle = "libelle" + codeVariable.getSuffixeNomChamp();

               modalites = modalites.withColumnRenamed("codeModalite", champCode)
                  .withColumnRenamed("libelleModalite", champLibelle);
            }
         }

         modalites = modalites.coalesce(1); // Le nombre de modalités est limité.
         modalites = modalites.orderBy("codeTypeModalite", champCode);

         // S'il y a restriction à une seule modalité, la retrier par son code.
         if (restreindreAuxColonnesCodeLibelle) {
            modalites = modalites.select(col(champCode), col(champLibelle)).orderBy(col(champCode));
         }

         return modalites;
      };

      OptionsCreationLecture optionsSpecifiques = new OptionsCreationLecture(options).triDejaFait(true);

      return constitutionStandard(optionsSpecifiques, historique, worker,
         new CacheParqueteur<>(optionsSpecifiques, this.session, exportCSV(),
            "equipements_modalites", "FILTRE-annee_{0,number,#0}-champs_renommes_{1}-restriction_aux_libelles-{2}", new CatalogueTriModalite(),
            anneeEquipements, renommerChamps, restreindreAuxColonnesCodeLibelle));
   }
}
