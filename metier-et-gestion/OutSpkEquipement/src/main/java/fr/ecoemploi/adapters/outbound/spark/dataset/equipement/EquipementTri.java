package fr.ecoemploi.adapters.outbound.spark.dataset.equipement;

import java.io.Serial;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

/**
 * Tris et partitionnements disponibles pour le dataset des équipements
 * @author Marc Le Bihan
 */
public class EquipementTri extends TriDataset {
   @Serial
   private static final long serialVersionUID = 7904490319405230800L;

   /**
    * Constuire un tri possible.
    * @param suffixeFichier Suffixe du fichier trié.
    * @param sortWithinPartition true s'il faut faire un tri au sein des partitions,<br>
    *    false si c'est un tri global.
    * @param nomColonnePartitionnement Nom de la colonne de partitionnement, s'il y en a une.
    * @param partitionByRange true s'il faut faire un partitionnement par range.
    * @param nomColonnesTri Nom des colonnes de tri.
    */
   public EquipementTri(String suffixeFichier, boolean sortWithinPartition, String nomColonnePartitionnement, boolean partitionByRange, String... nomColonnesTri) {
      super(suffixeFichier, sortWithinPartition, nomColonnePartitionnement, partitionByRange, nomColonnesTri);
   }
}

