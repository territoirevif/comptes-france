package fr.ecoemploi.adapters.outbound.spark.dataset.equipement;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Partitionnement par code département<br>
 * Tri par code département, commune, type d'équipement.
 * @author Marc Le Bihan
 */
public class EquipementTriDepartementCommuneType extends EquipementTri {
   @Serial
   private static final long serialVersionUID = -4645257189942207272L;

   /**
    * Construire un tri d'équipements par département, commune, type d'équipement
    */
   public EquipementTriDepartementCommuneType() {
      super("PARTITIONNEMENT-departement-TRI-departement-commune-type_equipement", true,
         CODE_DEPARTEMENT.champ(), false,
         CODE_DEPARTEMENT.champ(), CODE_COMMUNE.champ(), TYPE_EQUIPEMENT.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @param codeCommune Code commune, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommune) {
      return equalTo(codeDepartement)
         .and(codeCommune != null ? CODE_COMMUNE.col().equalTo(codeCommune) : CODE_COMMUNE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @param codeCommune Code commune, si null sera testé avec <code>isNull()</code>
    * @param typeEquipement Type d'équipement, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommune, String typeEquipement) {
      return equalTo(codeDepartement, codeCommune)
         .and(typeEquipement != null ? TYPE_EQUIPEMENT.col().equalTo(typeEquipement) : TYPE_EQUIPEMENT.col().isNull());
   }
}
