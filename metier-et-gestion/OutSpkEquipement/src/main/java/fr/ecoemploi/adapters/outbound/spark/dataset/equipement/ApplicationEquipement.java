package fr.ecoemploi.adapters.outbound.spark.dataset.equipement;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;

/**
 * Application de démarrage Spring-boot.
 * @author Marc LE BIHAN
 */
@SpringBootApplication
@ComponentScan(basePackages={"fr.ecoemploi"})
public class ApplicationEquipement {
   /**
    * Démarrage de l'application.
    * @param args Arguments.
    */
   public static void main(String[] args) {
      SpringApplication.run(ApplicationEquipement.class, args);
   }
}
