package fr.ecoemploi.adapters.outbound.spark.dataset.equipement;

import java.io.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import static org.apache.spark.sql.types.DataTypes.StringType;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;

/**
 * Chargeur de Row de modalités depuis un fichier CSV
 * @author Marc Le Bihan
 */
@Component
public class ModalitesRowCsvLoader extends AbstractSparkCsvLoader {
   @Serial
   private static final long serialVersionUID = 7876596327804411319L;

   /** Nom du fichier CSV des types d'équipements. */
   private final String nomFichier;

   /** Chemin d'accès au répertoire des équipements. */
   private final String repertoireEquipements;

   /** Session Spark. */
   private final SparkSession session;

   /**
    * Construire un chargeur de Row de modalités.
    * @param nomFichier Nom du fichier CSV des types d'équipements
    * @param repertoireEquipements Chemin d'accès au répertoire des équipements
    * @param session Session Spark.
    */
   public ModalitesRowCsvLoader(SparkSession session,
      @Value("${types_equipements_ensemble.csv.nom}") String nomFichier, @Value("${equipement.dir}") String repertoireEquipements) {
      this.session = session;
      this.repertoireEquipements = repertoireEquipements;
      this.nomFichier = nomFichier;
   }

   /**
    * Charger le dataset du fichier des modalités associées au fichier des équipements
    * @param anneeEquipements Année du fichier
    * @return Dataset.
    */
   public Dataset<Row> loadOpenData(int anneeEquipements) {
      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireEquipements);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, anneeEquipements, this.nomFichier);

      Dataset<Row> csv = this.session.read().schema(schema(anneeEquipements)).format("csv")
         .option("header","true")
         .option("sep", ";")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath());

      return rename(csv, anneeEquipements);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Dataset<Row> rename(Dataset<Row> dataset, int annee) {
      Dataset<Row> renamed = dataset.withColumnRenamed("COD_VAR", "codeTypeModalite");

      if (annee <= 2021) {
         renamed = renamed.withColumnRenamed("LIB_VAR", "libelleTypeModalite");
      }

      renamed = renamed.withColumnRenamed("COD_MOD", "codeModalite")
         .withColumnRenamed("LIB_MOD", "libelleModalite");

      if (annee <= 2020) {
         renamed = renamed.withColumnRenamed("TYPE_VAR", "typeChamp")
            .withColumnRenamed("LONG_VAR", "longueurChamp");
      }
      else {
         renamed = renamed.withColumnRenamed("TYPE", "typeChamp");
      }

      return renamed;
   }

   /**
    * Renvoyer le schéma.
    * @param annee Année de référence
    * @return Schéma.
    */
   @Override
   protected StructType schema(int annee) {
      /*
         COD_VAR;LIB_VAR;COD_MOD;LIB_MOD;TYPE_VAR;LONG_VAR
         REG;Région d’implantation de l’équipement;01;Guadeloupe;CHAR;2
         REG;Région d’implantation de l’équipement;02;Martinique;CHAR;2
         TYPEQU;Type d’équipement;A101;Police;CHAR;4
         TYPEQU;Type d’équipement;A104;Gendarmerie;CHAR;4
         TYPEQU;Type d’équipement;A105;Cour d’appel (CA);CHAR;4
         TYPEQU;Type d’équipement;A106;Tribunal de grande instance (TGI);CHAR;4
      */
      StructType schema = new StructType()
         .add("COD_VAR", StringType, false);

      if (annee <= 2021) {
         schema = schema.add("LIB_VAR", StringType, false);
      }

      schema = schema.add("COD_MOD", StringType, false)
         .add("LIB_MOD", StringType, false);

      if (annee <= 2020) {
         schema = schema.add("TYPE_VAR", StringType, false)
            .add("LONG_VAR", StringType, false);
         return schema;
      }

      if (annee == 2021) {
         schema = schema.add("TYPE", StringType, false);
         return schema;
      }

      return schema;
   }
}
