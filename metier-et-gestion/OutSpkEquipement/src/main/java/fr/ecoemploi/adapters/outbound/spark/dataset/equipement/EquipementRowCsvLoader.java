package fr.ecoemploi.adapters.outbound.spark.dataset.equipement;

import java.io.*;
import java.text.MessageFormat;
import java.util.Map;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;

import static org.apache.spark.sql.types.DataTypes.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Chargeur de données d'équipements depuis la base équipement.
 * @author Marc Le Bihan
 */
@Component
public class EquipementRowCsvLoader extends AbstractSparkCsvLoader {
   /** Serial UID. */
   @Serial
   private static final long serialVersionUID = -7642655554487733586L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EquipementRowCsvLoader.class);

   /** Nom du fichier CSV des équipements (ensemble). */
   @Value("${equipement_ensemble.csv.nom}")
   protected String nomFichierStockEquipement;

   /** Chemin d'accès au répertoire des équipements. */
   @Value("${equipement.dir}")
   protected String repertoireEquipement;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Charger des données d'équipements.
    * @param anneeEquipement Année d'équipement.
    * @return Dataset.
    */
   public Dataset<Row> loadOpenData(int anneeEquipement) {
      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireEquipement);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, anneeEquipement, this.nomFichierStockEquipement);

      return rename(anneeEquipement, this.load(anneeEquipement, schema(anneeEquipement), source));
   }

   /**
    * Renvoyer le schéma.
    * @param anneeEquipement Année d'équipement.
    * @return Schéma
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   @Override
   protected StructType schema(int anneeEquipement) {
      switch(anneeEquipement) {
         case 2018, 2019 -> {
            /*
               REG;DEP;DEPCOM;DCIRIS;AN;TYPEQU;LAMBERT_X;LAMBERT_Y;QUALITE_XY
               84;01;01001;01001;2018;A401;;;type_équipement_non_géolocalisé_cette_année
               84;01;01001;01001;2018;A401;;;type_équipement_non_géolocalisé_cette_année
            */
            return new StructType()
               .add("REG", StringType, false)
               .add("DEP", StringType, false)
               .add("DEPCOM", StringType, false)
               .add("DCIRIS", StringType, false)
               .add("AN", StringType, false)
               .add("TYPEQU", StringType, false)
               .add("LAMBERT_X", StringType, true)
               .add("LAMBERT_Y", StringType, true)
               .add("QUALITE_XY", StringType, false);

         }

         case 2020 -> {
            /*
               AAV2020;AN;BV2012;DCIRIS;DEP;DEPCOM;EPCI;LABEL;LAMBERT_X;LAMBERT_Y;QUALITE_XY;REG;TYPEQU;UU2020
               524;2020;01093;01001;01;01001;200069193;X48167.97;6563141.11;Bonne4;A404;
               524;2020;01093;01001;01;01001;200069193;X47950;6566550;Mauvaise4;B202;
            */
            return new StructType()
               .add("AAV2020", StringType, false)  // Zonage en aire d'attraction des villes 2020 d'implantation de l'équipement
               .add("AN", StringType, false)       // Millésime de la base
               .add("BV2012", StringType, false)   // Bassin de vie 2012 d'implantation de l'équipement
               .add("DCIRIS", StringType, false)   // Code commune et code IRIS d'implantation
               .add("DEP", StringType, false)      // Département d'implantation de l'équipement

               .add("DEPCOM", StringType, false)   // Code département et commune d'implantation de l'équipement
               .add("EPCI", StringType, false)     // Etablissement public de coopération intercommunal d'implantation de l'équipement
               .add("LABEL", StringType, false)    // Caractérisation des MSAP / IFS
               .add("LAMBERT_X", StringType, true) // Coordonnée X de l'équipement
               .add("LAMBERT_Y", StringType, true) // Coordonnée Y de l'équipement

               .add("QUALITE_XY", StringType, false) // Qualité d'attribution des coordonnées XY pour un équipement
               .add("REG", StringType, false)      // Région d'implantation de l'équipement
               .add("TYPEQU", StringType, false)   // Type d'équipement
               .add("UU2020", StringType, false);  // Unité urbaine 2020 d'implantation de l'équipement
         }

         case 2021 -> {
            /*
               AAV2020;AN;BV2012;DEP;DEPCOM;DOM;EPCI;DCIRIS;LAMBERT_X;LAMBERT_Y;QP;QUALI_IRIS;QUALI_QP;QUALI_QVA;QUALI_ZFU;QUALI_ZUS;QUALITE_XY;QVA;REG;SDOM;TYPEQU;UU2020;ZFU;ZUS
               001;2021;75056;95;95607;D;200058485;956070113;643379.5;6880776;HZ;2;2;2;X;X;Bonne;HZ1;D4;D401;00851;CSZ;CSZ
               001;2021;75056;95;95625;D;200035970;956250000;624466.7;6889588;CSZ;X;X;X;X;X;Bonne;CSZ1;D4;D401;CSZ;CSZ;CSZ
            */
            return new StructType()
               .add("AAV2020", StringType, false)  // Zonage en aire d'attraction des villes 2020 d'implantation de l'équipement
               .add("AN", StringType, false)       // Millésime de la base
               .add("BV2012", StringType, false)   // Bassin de vie 2012 d'implantation de l'équipement
               .add("DEP", StringType, false)      // Département d'implantation de l'équipement
               .add("DEPCOM", StringType, false)   // Code département et commune d'implantation de l'équipement

               .add("DOM", StringType, false)
               .add("EPCI", StringType, false)     // Etablissement public de coopération intercommunal d'implantation de l'équipement
               .add("DCIRIS", StringType, false)   // Code commune et code IRIS d'implantation
               .add("LAMBERT_X", StringType, true) // Coordonnée X de l'équipement
               .add("LAMBERT_Y", StringType, true) // Coordonnée Y de l'équipement

               .add("QP", StringType, false)         // Quartier prioritaire de la politique de la ville d'appartenance de l'équipement
               .add("QUALI_IRIS", StringType, false) // Indicateur de qualité du géoréférencement dans l'iris
               .add("QUALI_QP", StringType, false)   // Indicateur de qualité du géoréférencement dans le quartier prioritaire de la politique de la ville
               .add("QUALI_QVA", StringType, false)  // Indicateur de qualité du géoréférencement dans le quartier de veille active
               .add("QUALI_ZFU", StringType, false)  // Indicateur de qualité du géoréférencement dans la zone franche urbaine

               .add("QUALI_ZUS", StringType, false)  // Indicateur de qualité du géoréférencement dans la zone urbaine sensible
               .add("QUALITE_XY", StringType, false) // Qualité d'attribution des coordonnées XY pour un équipement
               .add("QVA", StringType, false)        // Quartier de veille active d'appartenance de l'équipement
               .add("REG", StringType, false)        // Région d'implantation de l'équipement
               .add("SDOM", StringType, false)       // Sous-domaine d'appartenance de l'équipement

               .add("TYPEQU", StringType, false)     // Type d'équipement
               .add("UU2020", StringType, false)     // Unité urbaine 2020 d'implantation de l'équipement
               .add("ZFU", StringType, false)        // Zone franche urbaine d'appartenance de l'équipement
               .add("ZUS", StringType, false);       // Zone urbaine sensible d'appartenance de l'équipement
         }

         /*
            "AN";"NOMRS";"CNOMRS";"NUMVOIE";"INDREP";"TYPVOIE";"LIBVOIE";"CADR";"CODPOS";"DEPCOM";"DEP";"REG";"DOM";"SDOM";"TYPEQU";"SIRET";"STATUT_DIFFUSION";"CANTINE";"INTERNAT";"RPI";"EP";"CL_PGE";"SECT";"ACCES_AIRE_PRATIQUE";"ACCES_LIBRE";"ACCES_SANITAIRE";"ACCES_VESTIAIRE";"CAPACITE_D_ACCUEIL";"PRES_DOUCHE";"PRES_SANITAIRE";"SAISONNIER";"COUVERT";"ECLAIRE";"CATEGORIE";"MULTIPLEXE";"ACCUEIL";"ITINERANCE";"MODE_GESTION";"SSTYPHEB";"TYPE";"CAPACITE";"INDIC_CAPA";"NBEQUIDENT";"INDIC_NBEQUIDENT";"NBSALLES";"INDIC_NBSALLES";"NBLIEUX";"INDIC_NBLIEUX";"LAMBERT_X";"LAMBERT_Y";"LONGITUDE";"LATITUDE";"QUALITE_XY";"EPSG";"QUALITE_GEOLOC";"TR_DIST_PRECISION";"DCIRIS";"QUALI_IRIS";"IRISEE";"QP2015";"QUALI_QP2015";"QP";"QUALI_QP";"QVA";"QUALI_QVA";"ZUS";"QUALI_ZUS";"EPCI";"UU2020";"BV2022";"AAV2020";"DENS3";"DENS7"
            "2023";"SITE MARIE LES ABYMES";"";"";"";"RUE";"ACHILLE RENE BOISNEUF";"FACE AU STADE, PROVIDENCE";"97139";"97101";"971";"01";"D";"D3";"D302";"51119154600015";"O";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";;"0";;"0";;"0";;"0";659950799850;-61.50310839950546.2741832074831;"M";"5490";"12";">= 500";"971010103";"2";"1";"HZ";"1";"IND";"3";"CSZ";"_Z";"HZ";"1";"200018653";"9A701";"97101";"9A1";"2";"2"
            "2023";"ESAT LE CHAMPFLEURY (ABYMES)";"";"40";"";"LOT";"DUGAZON DE BOURGOGNE";"";"97139";"97101";"971";"01";"D";"D6";"D605";"33825976500083";"O";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z";"_Z"4;"1";;"0";;"0";;"0";659083.81798004.14;-61.51133836499056.2575591205121;"B";"5490";"11";"< 100";"971010119";"1";"1";"HZ";"1";"IND";"3";"CSZ";"_Z";"HZ";"1";"200018653";"9A701";"97101";"9A1";"2";"2"
          */
         case 2023 -> {
            return new StructType()
               .add("AN", StringType, false)
               .add("NOMRS", StringType, false)
               .add("CNOMRS", StringType, false)
               .add("NUMVOIE", StringType, false)
               .add("INDREP", StringType, false)
               .add("TYPVOIE", StringType, false)

               .add("LIBVOIE", StringType, false)
               .add("CADR", StringType, false)
               .add("LIBCOM", StringType, false)
               .add("CODPOS", StringType, false)
               .add("DEPCOM", StringType, false)

               .add("DEP", StringType, false)
               .add("REG", StringType, false)
               .add("DOM", StringType, false)
               .add("SDOM", StringType, false)
               .add("TYPEQU", StringType, false)

               .add("SIRET", StringType, false)
               .add("STATUT_DIFFUSION", StringType, false)
               .add("CANTINE", StringType, false)
               .add("INTERNAT", StringType, false)
               .add("RPI", StringType, false)

               .add("EP", StringType, false)
               .add("CL_PGE", StringType, false)
               .add("SECT", StringType, false)
               .add("ACCES_AIRE_PRATIQUE", StringType, false)
               .add("ACCES_LIBRE", StringType, false)

               .add("ACCES_SANITAIRE", StringType, false)
               .add("ACCES_VESTIAIRE", StringType, false)
               .add("CAPACITE_D_ACCUEIL", StringType, false)
               .add("PRES_DOUCHE", StringType, false)
               .add("PRES_SANITAIRE", StringType, false)

               .add("SAISONNIER", StringType, false)
               .add("COUVERT", StringType, false)
               .add("ECLAIRE", StringType, false)
               .add("CATEGORIE", StringType, false)
               .add("MULTIPLEXE", StringType, false)

               .add("ACCUEIL", StringType, false)
               .add("ITINERANCE", StringType, false)
               .add("MODE_GESTION", StringType, false)
               .add("SSTYPHEB", StringType, false)
               .add("TYPE", StringType, false)

               .add("CAPACITE", StringType, false)
               .add("INDIC_CAPA", StringType, false)
               .add("NBEQUIDENT", StringType, false)
               .add("INDIC_NBEQUIDENT", StringType, false)
               .add("NBSALLES", StringType, false)

               .add("INDIC_NBSALLES", StringType, false)
               .add("NBLIEUX", StringType, false)
               .add("INDIC_NBLIEUX", StringType, false)
               .add("LAMBERT_X", StringType, false)
               .add("LAMBERT_Y", StringType, false)

               .add("LONGITUDE", StringType, false)
               .add("LATITUDE", StringType, false)
               .add("QUALITE_XY", StringType, false)
               .add("EPSG", StringType, false)
               .add("QUALITE_GEOLOC", StringType, false)

               .add("TR_DIST_PRECISION", StringType, false)
               .add("DCIRIS", StringType, false)
               .add("QUALI_IRIS", StringType, false)
               .add("IRISEE", StringType, false)
               .add("QP2015", StringType, false)

               .add("QUALI_QP2015", StringType, false)
               .add("QP", StringType, false)
               .add("QUALI_QP", StringType, false)
               .add("QVA", StringType, false)
               .add("QUALI_QVA", StringType, false)

               .add("ZUS", StringType, false)
               .add("QUALI_ZUS", StringType, false)
               .add("EPCI", StringType, false)
               .add("UU2020", StringType, false)
               .add("BV2022", StringType, false)

               .add("AAV2020", StringType, false)
               .add("DENS3", StringType, false)
               .add("DENS7", StringType, false);
         }

         default -> {
            String format = "Il n''existe pas de schéma de base équipement pour l''année {0}.";
            String message = MessageFormat.format(format, anneeEquipement);
            IllegalArgumentException ex = new IllegalArgumentException(message);

            LOGGER.warn(message, ex);
            throw ex;
         }
      }
   }

   /**
    * Charger le dataset des équipements.
    * @param anneeEquipement Année d'équipement.
    * @param schema Schéma du dataset.
    * @param source Donnée source.
    * @return Dataset.
    */
   public Dataset<Row> load(int anneeEquipement, StructType schema, File source) {
      switch(anneeEquipement) {
         case 2018, 2019, 2020, 2021, 2023 -> {
            return this.session.read().schema(schema).format("csv")
               .option("header", "true")
               .option("quote", "\"")
               .option("escape", "\"")
               .option("sep", ";")
               .option("enforceSchema", false)
               .load(source.getAbsolutePath())
               .selectExpr("*");
         }

         default -> {
            String format = "Il n''existe pas de commande de chargement de la base équipement pour l''année {0}.";
            String message = MessageFormat.format(format, anneeEquipement);
            IllegalArgumentException ex = new IllegalArgumentException(message);

            LOGGER.warn(message, ex);
            throw ex;
         }
      }
   }

   /**
    * Charger la base équipement.
    * @param anneeEquipement Annéee équipements.
    * @param dataset Dataset dont les champs doivent être renommés.
    * @return Fichier CSV aux variables renommées.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   private Dataset<Row> rename(int anneeEquipement, Dataset<Row> dataset) {
      // 2021 et antérieurs
      Map<String, String> r1 = Map.of(
         "REG", CODE_REGION.champ(),
         "DEP", CODE_DEPARTEMENT.champ(),
         "DEPCOM", CODE_COMMUNE.champ(),
         "DCIRIS", "codeIRIS",
         "AN", "annee",

         "TYPEQU", TYPE_EQUIPEMENT.champ(),
         "LAMBERT_X", LAMBERT_X.champ(),
         "LAMBERT_Y", LAMBERT_Y.champ(),
         "QUALITE_XY", QUALITE_POSITIONNEMENT_XY.champ()
      );

      // 2021 et antérieurs
      Map<String, String> r2 = Map.of(
         "AAV2020", "zonageAireAttraction",
         "BV2012", CODE_BASSIN_VIE.champ(),
         "EPCI", CODE_EPCI.champ(),
         "LABEL", "caracterisationMSAP_IFS",
         "UU2020", CODE_UNITE_URBAINE.champ()
      );

      // 2023
      Map<String, String> r2023a = Map.of(
         "ACCES_AIRE_PRATIQUE", "accesAirePratique",
         "ACCES_LIBRE", "accesLibre",
         "ACCES_SANITAIRE", "accesSanitaire",
         "ACCES_VESTIAIRE", "accesVestiaire",
         "ACCUEIL", "accueil",

         "BV2022", CODE_BASSIN_VIE.champ(),
         "CADR", COMPLEMENT_ADRESSE.champ(),
         "CANTINE", "cantine",
         "CAPACITE", "capaciteEquipement",
         "CAPACITE_D_ACCUEIL", "categorieAccueilERP"
      );

      Map<String, String> r2023b = Map.of(
         "CATEGORIE", "categorieEquipementCulturel",
         "CL_PGE", "classePreparatoireAuxGrandesEcolesEnLycee",
         "CNOMRS", COMPLEMENT_NOM.champ(),
         "CODPOS", CODE_POSTAL.champ(),
         "COUVERT", "partieEquipementCouverte",

         "DENS3", "codeGrilleCommunaleDensite3Niveaux",
         "DENS7", "codeGrilleCommunaleDensite7Niveaux",
         "DOM", "domaineEquipement",
         "ECLAIRE", "equipementAvecPartieEclairee",
         "EP", "presenceDispositifEducationPrioritaire"
      );

      Map<String, String> r2023c = Map.of(
         "EPCI", CODE_EPCI.champ(),
         "EPSG", "codeEPSG",
         "INDIC_CAPA", "concernementValeursCapacite",
         "INDIC_NBEQUIDENT", "concernementValeursNombreInfrastructures",
         "INDIC_NBLIEUX", "concernementValeursNombreLieux",

         "INDIC_NBSALLES", "concernementValeursNombreSalles",
         "INDREP", INDICE_REPETITION.champ(),
         "INTERNAT", "presenceInternat",
         "IRISEE", "indicatriceIrisationCommune",
         "ITINERANCE", "presenceStructureItinerante"
      );

      Map<String, String> r2023d = Map.of(
         "LIBCOM", NOM_COMMUNE.champ(),
         "LIBVOIE", LIBELLE_VOIE.champ(),
         "MODE_GESTION", "modeDeGestionInfrastructure",
         "MULTIPLEXE", "presenceCinemaMultiplexe",
         "NBEQUIDENT", "nombreEquipementsIdentiques",

         "NBLIEUX", "nombreDeLieux",
         "NBSALLES", "nombreDeSallesParCinemaOuTheatre",
         "NOMRS", NOM_OU_RAISON_SOCIALE.champ(),
         "NUMVOIE", NUMERO_VOIE.champ(),
         "PRES_DOUCHE", "presenceDouches"
      );

      Map<String, String> r2023e = Map.of(
         "PRES_SANITAIRE", "presenceSanitaires",
         "QP", "qpvAappartenanceEquipement",
         "QP2015", "qpvAappartenanceEquipement2015",
         "QUALI_IRIS", "qualiteGeoreferencementIris",
         "QUALI_QP", "qualiteGeoreferencementQpv",

         "QUALI_QP2015", "qualiteGeoreferencementQpv2015",
         "QUALI_QVA", "qualiteGeoreferencementQuartierVeilleActive",
         "QUALI_ZUS", "qualiteGeoreferencementZoneUrbaineSensible",
         "QUALITE_GEOLOC", QUALITE_GEOLOCALISATION.champ(),
         "QVA", "quartierVeilleActiveAppartenanceEquipement"
      );

      Map<String, String> r2023f = Map.of(
         "RPI", "typeRegroupementPedagogiqueIntercommunal",
         "SAISONNIER", "presenceEquipementOuvertureExclusivementSaisonniere",
         "SDOM", "sousDomaineAppartenanceEquipement",
         "SECT", "appartenanceSecteurPublicOuPriveEnseignement",
         "SIRET", SIRET_ETABLISSEMENT.champ(),

         "SSTYPHEB", "sousTypeHebergement",
         "STATUT_DIFFUSION", DIFFUSABLE.champ(),
         "TR_DIST_PRECISION", "erreurMaximumPositionnementDansVoieOuLieuDit",
         "TYPE", "typeDeLieux",
         "TYPVOIE", TYPE_DE_VOIE.champ()
      );

      Map<String, String> r2023g = Map.of(
         "ZUS", "zoneUrbaineSensibleAppartenanceEquipement"
      );

      Dataset<Row> rename = dataset;

      switch (anneeEquipement) {
         case 2018, 2019 -> {
            rename = super.renameFields(rename, r1);
            return rename;
         }

         case 2020, 2021 -> {
            rename = super.renameFields(rename, r1);
            rename = super.renameFields(rename, r2);
            return rename;
         }

         case 2023 -> {
            rename = super.renameFields(rename, r1);
            rename = super.renameFields(rename, r2);
            rename = super.renameFields(rename, r2023a);
            rename = super.renameFields(rename, r2023b);
            rename = super.renameFields(rename, r2023c);
            rename = super.renameFields(rename, r2023d);
            rename = super.renameFields(rename, r2023e);
            rename = super.renameFields(rename, r2023f);
            rename = super.renameFields(rename, r2023g);
            return rename;
         }

         default -> {
            String format = "Il n''existe pas de base équipement pour l''année {0}.";
            String message = MessageFormat.format(format, anneeEquipement);
            IllegalArgumentException ex = new IllegalArgumentException(message);

            LOGGER.warn(message, ex);
            throw ex;
         }
      }
   }
}

