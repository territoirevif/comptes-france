package fr.ecoemploi.adapters.outbound.spark.dataset.equipement;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Partitionnement par type d'équipement<br>
 * Tri par type d'équipement, code département, commune.
 * @author Marc Le Bihan
 */
public class EquipementTriTypeDepartementCommune extends EquipementTri {
   @Serial
   private static final long serialVersionUID = -5015363337906391027L;

   /**
    * Construire un tri d'équipements par type d'équipement, département, commune
    */
   public EquipementTriTypeDepartementCommune() {
      super("PARTITIONNEMENT-type_equipement-TRI-departement-commune", true,
         TYPE_EQUIPEMENT.champ(), false,
         TYPE_EQUIPEMENT.champ(), CODE_DEPARTEMENT.champ(), CODE_COMMUNE.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param typeEquipement Type d'équipement
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String typeEquipement) {
      return typeEquipement != null ? TYPE_EQUIPEMENT.col().equalTo(typeEquipement) : TYPE_EQUIPEMENT.col().isNull();
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param typeEquipement Type d'équipement
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String typeEquipement, String codeDepartement) {
      return equalTo(typeEquipement)
         .and(codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param typeEquipement Type d'équipement
    * @param codeDepartement Code département, si null sera testé avec <code>isNull()</code>
    * @param codeCommune Code commune, si null sera testé avec <code>isNull()</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String typeEquipement, String codeDepartement, String codeCommune) {
      return equalTo(typeEquipement, codeDepartement)
         .and(codeCommune != null ? CODE_COMMUNE.col().equalTo(codeCommune) : CODE_COMMUNE.col().isNull());
   }
}
