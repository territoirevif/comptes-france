package fr.ecoemploi.adapters.outbound.spark.dataset.equipement;

import org.postgis.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.core.namedparam.*;
import org.springframework.stereotype.*;

/**
 * DAO des équipements des communes.
 * @author Marc LE BIHAN
 */
@Component
public class EquipementCommuneDAO {
   /** JDBC Template. */
   @Autowired
   private JdbcTemplate jdbcTemplate;
   
   /** JDBC Template. */
   @Autowired
   private NamedParameterJdbcTemplate namedParameterjdbcTemplate;
   
   /** SRId (EPSG) de la projection WGS84. */
   private static final Integer EPSG_WGS84 = 4326;
   
   /** SRId (EPSG) de la projection Lambert93. */
   private static final Integer EPSG_LAMBERT_93 = 2154;

   /** SRId (EPSG) de la projection UTM40S (La Réunion). */
   private static final Integer EPSG_UTM40S = 32740;
   
   /** SRId (EPSG) de la projection UTM20N (Martinique et Guadeloupe). */
   private static final Integer EPSG_UTM20N = 32620;
   
   /** SRId (EPSG) de la projection UTM22N (Guyane). */
   private static final Integer EPSG_UTM22N = 32622;
   
   /** SRId (EPSG) de la projection RGM04 (Mayotte). */
   private static final Integer EPSG_RGM04 = 4471;
   
   /** Code du département de la Guadeloupe. */
   private static final String GUADELOUPE = "971";
   
   /** Code du département de la Martinique. */
   private static final String MARTINIQUE = "972";
   
   /** Code du département de la Guyane. */
   private static final String GUYANE = "973";
   
   /** Code du département de La Réunion. */
   private static final String LA_REUNION = "974";
   
   /** Code du département de Mayotte. */
   private static final String MAYOTTE = "976";

   /*
    * FIXME Les projections déclarées ci-dessus sont-elles compatibles ?
    *
    * Projections d'Admin Express (Février 2024) :
    *    Guadeloupe : RGAF09UTM20
    *    Martinique : RGAF09UTM20
    *    Guyane : UTM22RGFG95
    *    La Réunion : RGR92UTM40S
    *    Mayotte : RGM04UTM38S
    */
   
   /**
    * Persister une activité de commune.
    * @param codeRegion Code région. 
    * @param codeDepartement Code département.
    * @param codeCommune Code commune.
    * @param nomCommune Nom de la commune.
    * @param typeEquipement Type de l'équipement.
    * @param x Longitude ou coordonnée X.
    * @param y Latitude ou coordonnée Y.
    * @param qualiteGeolocalisation Qualité de la géolocalisation.
    * @return true si l'insertion a effectivement eu lieu.
    */
   public boolean insert(String codeRegion, String codeDepartement, String codeCommune, String nomCommune, String typeEquipement, String x, String y, String qualiteGeolocalisation) {
      PGgeometry point = null;
      
      // Si la géolocalisation de l'emplacement est présente, la convertir en WGS84.
      if (qualiteGeolocalisation.equals("A") || qualiteGeolocalisation.equals("B") || qualiteGeolocalisation.equals("M")) {
         Integer srId = switch (codeDepartement) {
            case GUADELOUPE, MARTINIQUE -> EPSG_UTM20N;
            case GUYANE -> EPSG_UTM22N;
            case LA_REUNION -> EPSG_UTM40S;
            case MAYOTTE -> EPSG_RGM04;
            default -> EPSG_LAMBERT_93;
         };
         
         point = conversionPointWGS84(Double.parseDouble(x), Double.parseDouble(y), srId);
      }
      
      String sql = "INSERT INTO equipements_commune(codecommune, nomcommune, region, departement, type_equipement, qualite_geolocalisation, geom) VALUES(?, ?, ?, ?, ?, ?, ?)";
      int count = this.jdbcTemplate.update(sql, codeCommune, nomCommune, codeRegion, codeDepartement, typeEquipement, qualiteGeolocalisation, point);
      return count == 1;
   }
   
   /**
    * Convertir un point d'une projection source vers une projection WGS84 (EPSG:4326). 
    * @param lon Longitude ou coordonnée X du point.
    * @param lat Latitude ou coordonnée Y du point.
    * @param srIdSource SRId (EPSG) dans lequel sont données les coordonnées du point transmis en argument.
    * @return le point projeté en WGS84 (EPSG:4326).
    */
   private PGgeometry conversionPointWGS84(Double lon, Double lat, Integer srIdSource) {
      Integer srIdDestination = EPSG_WGS84;

      String sql = "SELECT ST_Transform(ST_SetSRID(ST_Point(:lon, :lat), :srIdSource), :srIdDestination)";
      
      MapSqlParameterSource parametres = new MapSqlParameterSource();
      parametres.addValue("lon", lon);
      parametres.addValue("lat", lat);
      parametres.addValue("srIdSource", srIdSource);
      parametres.addValue("srIdDestination", srIdDestination);
      
      return this.namedParameterjdbcTemplate.queryForObject(sql, parametres, PGgeometry.class);
   }
}
