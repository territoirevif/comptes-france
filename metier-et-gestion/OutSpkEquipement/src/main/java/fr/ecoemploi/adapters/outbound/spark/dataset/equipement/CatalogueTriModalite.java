package fr.ecoemploi.adapters.outbound.spark.dataset.equipement;

import java.io.Serial;

import org.apache.spark.sql.Column;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.TriDataset;

import static org.apache.spark.sql.functions.col;

/**
 * Tri de dataset : tri par modalité
 * @author Marc Le Bihan
 */
public class CatalogueTriModalite extends TriDataset {
   @Serial
   private static final long serialVersionUID = -6098433981713942089L;

   /**
    * Constuire un tri.
    */
   public CatalogueTriModalite() {
      super("TRI-type_modalite", false, null, false,
         "codeTypeModalite");
   }

   /**
    * Renvoyer une condition equalTo sur le code type de la modalité.
    * @param codeTypeModalite Code type de la modalité. Si null, isNull() servira au test.
    * @return Colonne contenant la condition.
    */
   public Column equalTo(String codeTypeModalite) {
      return codeTypeModalite != null ? col("codeTypeModalite").equalTo(codeTypeModalite) : col("codeTypeModalite").isNull();
   }
}
