package fr.ecoemploi.adapters.outbound.spark.dataset.equipement;

import java.io.*;
import java.util.List;
import java.util.function.Supplier;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.Transactional;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.adapters.outbound.port.equipement.EquipementRepository;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.stream.Paginateur;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.verifications.Verification;
import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Dataset de la base équipement.
 * @author Marc Le Bihan
 */
@Component
public class EquipementDataset extends AbstractSparkDataset implements EquipementRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 4257529152504357008L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EquipementDataset.class);

   /** DAO de la table Equipement Commune. */
   private final transient EquipementCommuneDAO equipementCommuneDAO;

   /** Dataset de description du territoire. */
   private final CogDataset cogDataset;

   /** Chargeur de données open data d'équipement. */
   private final EquipementRowCsvLoader equipementRowCsvLoader;

   /** Dataset des modalités de la base équipements. */
   private final ModalitesEquipementDataset modalitesEquipementsDataset;

   /**
    * Construire un dataset d'équipements
    * @param cogDataset Code Officiel Géographique
    * @param modalitesEquipementsDataset Dataset des modalités d'équipements
    * @param equipementCommuneDAO DAO de la base équipement
    * @param equipementRowCsvLoader Chargeur de CSV de la base équipement
    */
   @Autowired
   public EquipementDataset(CogDataset cogDataset, ModalitesEquipementDataset modalitesEquipementsDataset,
       EquipementCommuneDAO equipementCommuneDAO, EquipementRowCsvLoader equipementRowCsvLoader) {
      this.cogDataset = cogDataset;
      this.modalitesEquipementsDataset = modalitesEquipementsDataset;
      this.equipementCommuneDAO = equipementCommuneDAO;
      this.equipementRowCsvLoader = equipementRowCsvLoader;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void chargerEquipements(int anneeEquipement) {
      // Seuls les datasets stockant leurs données sont appelés.
      OptionsCreationLecture options = this.modalitesEquipementsDataset.optionsCreationLecture();
      this.modalitesEquipementsDataset.rowModalites(options, new HistoriqueExecution(), ModalitesEquipementDataset.CodeVariableEquipement.TYPE_EQUIPEMENT, anneeEquipement, false, false);

      rowEquipements(options, new HistoriqueExecution(), anneeEquipement, false, new EquipementTriDepartementCommuneType());
      rowEquipements(options, new HistoriqueExecution(), anneeEquipement, true, new EquipementTriDepartementCommuneType());

      rowEquipements(options, new HistoriqueExecution(), anneeEquipement, false, new EquipementTriDepartementTypeCommune());
      rowEquipements(options, new HistoriqueExecution(), anneeEquipement, true, new EquipementTriDepartementTypeCommune());

      rowEquipements(options, new HistoriqueExecution(), anneeEquipement, false, new EquipementTriTypeDepartementCommune());
      rowEquipements(options, new HistoriqueExecution(), anneeEquipement, true, new EquipementTriTypeDepartementCommune());
   }

   /**
    * Obtenir un Dataset Row d'équipements issu du fichier "ensemble des équipements".
    * @param optionsCreationLecture Options de lecture et de constitution du dataset.
    * @param historique Historique d'exécution.
    * @param anneeEquipement Année pour laquelle rechercher les équipements.
    * @param globaux true si le cumul des équipements par commune doit être fait (et la geolocalisation n'est plus fournie).
    * <br>false si chaque équipement doit être géolocalisé.
    * @param tri Tri préféré.
    * @param verifications Vérifications optionnelles.
    * @return Ensemble des données attributaires accessibles sur les équipements.
    */
   public Dataset<Row> rowEquipements(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeEquipement, boolean globaux, EquipementTri tri, Verification... verifications) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.aucuneDeRegleValidation(this.session);
      }

      Supplier<Dataset<Row>> worker = () -> {
         String formatLong = "Constitution du dataset des équipements de la base {0,number,#0} ({1}), tri par {2}";
         String formatShort = "équipements {0,number,#0} ({1})";
         LOGGER.info(setStageDescription(formatShort, formatLong, anneeEquipement, globaux ? "globaux" : "localisés", tri));

         Dataset<Row> modalites = this.modalitesEquipementsDataset.rowLibellesTypesEquipements(options, historique, anneeEquipement);

         Dataset<Row> equipements = this.equipementRowCsvLoader.loadOpenData(anneeEquipement)
            .join(modalites, TYPE_EQUIPEMENT.col().equalTo(modalites.col("codeTypeEquipement")), "left")
            .drop("codeTypeEquipement");

         // Si des résultats globaux sont demandés, un cumul a lieu.
         if (globaux) {
            RelationalGroupedDataset groupBy = equipements.groupBy(CODE_REGION.champ(), CODE_DEPARTEMENT.champ(), CODE_COMMUNE.champ(),
               "annee", TYPE_EQUIPEMENT.champ(), "libelleTypeEquipement");

            Dataset<Row> ds = groupBy.count().as("nombre");
            equipements = ds.orderBy(CODE_COMMUNE.champ(), TYPE_EQUIPEMENT.champ());
         } else {
            if (anneeEquipement <= 2021) {
               // 0=Non géolocalisé, 1=Type d''équipement non géolocalisé cette année, 2=Mauvaise, 3=Acceptable, 4=Bonne
               // Lambert 93 (RGF93) pour la France métropolitaine, UTM40S pour La Réunion, UTM20N pour la Martinique et la Guadeloupe, UTM22N pour la Guyane
               // sont encodés depuis 2023 par : A=Acceptable, B=Bonne, M=Mauvaise, _U=Indéterminée, _Z=Non-géolocalisé
               equipements = equipements.withColumn(QUALITE_POSITIONNEMENT_XY.champ(),
                  when(QUALITE_POSITIONNEMENT_XY.col(equipements).equalTo("type_équipement_non_géolocalisé_cette_année"), lit("_Z"))
                     .when(QUALITE_POSITIONNEMENT_XY.col(equipements).equalTo("Mauvaise"), lit("M"))
                     .when(QUALITE_POSITIONNEMENT_XY.col(equipements).equalTo("Acceptable"), lit("A"))
                     .when(QUALITE_POSITIONNEMENT_XY.col(equipements).equalTo("Bonne"), lit("B"))
                     .otherwise(lit("_U"))
               );
            }
         }

         return equipements;
      };

      Column postFiltre = options.hasRestrictionAuxCommunes() ? CODE_COMMUNE.col().isin(options.restrictionAuxCommunes().toArray()) : null;
      boolean anneeEligibleCache = options.anneeEligibleMiseEnCache(anneeEquipement);

      return constitutionStandard(options, historique, worker, anneeEligibleCache, new ConditionPostFiltrageConstante(postFiltre),
         new CacheParqueteur<>(options, this.session, exportCSV(),
            "equipements", "FILTRE-annee_{0,number,#0}-{1}",
            tri, anneeEquipement, globaux ? "globaux" : "localisés"));
   }
   
   /**
    * Créer une matrice code commune X équipement
    * @param anneeEquipement Année de la base équipement.
    * @param options Options de lecture et de constitution du dataset.
    * @param historique Historique d'exécution.
    * @return matrice commune x équipement
    */
   public Dataset<Row> matriceCommuneEquipements(OptionsCreationLecture options, HistoriqueExecution historique, int anneeEquipement) {
      Dataset<Row> matrice = rowEquipements(options, historique, anneeEquipement, true, new EquipementTriDepartementCommuneType())
         .selectExpr(CODE_COMMUNE.champ(), TYPE_EQUIPEMENT.champ(), "count")
         .groupBy(CODE_COMMUNE.champ()).pivot(TYPE_EQUIPEMENT.champ()).sum("count");
      
      return matrice.na().fill(0); // Remplacer l'absence d'un équipement (null) par un nombre à 0
   }

   /**
    * Insérer les équipements dans la base équipements.
    * @param anneeCOG Année du COG.
    * @param anneeEquipement Année équipement.
    * @param typeEquipement Restriction à un type de l'équipement, si non null.
    * @param options Options de lecture et de constitution du dataset.
    * @param historique Historique d'exécution.
    */
   @Transactional(rollbackFor = Exception.class)
   public void insererEquipements(OptionsCreationLecture options, int anneeCOG, int anneeEquipement, HistoriqueExecution historique, String typeEquipement) {
      Dataset<Row> communes = this.cogDataset.rowCommunes(options, historique, anneeCOG, false).select("codeCommune", "nomCommune");
      Dataset<Row> equipements = rowEquipements(options, historique, anneeEquipement, false, new EquipementTriDepartementCommuneType());

      Column condition = CODE_COMMUNE.col(equipements).equalTo(CODE_COMMUNE.col(communes));

      if (typeEquipement != null) {
         condition = condition.and(TYPE_EQUIPEMENT.col(equipements).equalTo(lit(typeEquipement)));
      }

      equipements = equipements.join(communes, condition, "inner")
         .drop(CODE_COMMUNE.col(communes));

      List<Dataset<Row>> divisionsDataset = Paginateur.paginer(equipements, 500000);
      long nombre = 0;

      for(int index=0; index < divisionsDataset.size(); index ++) {
         nombre += ecrireBlocDataset(divisionsDataset.get(index));
         LOGGER.info("Bloc {}/{} d'équipements insérés.", index + 1, divisionsDataset.size());
      }

      if (nombre == 0) {
         LOGGER.warn("Aucun enregistrement n'a été inséré dans la base équipement de l'année {} pour le type d'équipement {}.",
            anneeEquipement, typeEquipement != null ? typeEquipement : "<TOUS>");
      }
      else {
         LOGGER.info("{} enregistrements ont été insérés dans la base équipement de l'année {} pour le type d'équipement {}.",
            nombre, anneeEquipement, typeEquipement != null ? typeEquipement : "<TOUS>");
      }
   }

   /**
    * Ecrire un bloc d'enregistrements.
    * @param ds Dataset à insérer en base.
    * @return Nombre d'enregistrements insérés.
    */
   private long ecrireBlocDataset(Dataset<Row> ds) {
      List<Row> enregistrements = ds.collectAsList();
      long nombre = 0;

      for(Row enregistrement : enregistrements) {
         String codeRegion = CODE_REGION.getAs(enregistrement);
         String codeDepartement = CODE_DEPARTEMENT.getAs(enregistrement);
         String codeCommune = CODE_COMMUNE.getAs(enregistrement);
         String nomCommune = NOM_COMMUNE.getAs(enregistrement);
         String typeEquipement = TYPE_EQUIPEMENT.getAs(enregistrement);
         String x = LAMBERT_X.getAs(enregistrement);
         String y = LAMBERT_Y.getAs(enregistrement);
         String qualitePositionnementXY = QUALITE_POSITIONNEMENT_XY.getAs(enregistrement);

         if (this.equipementCommuneDAO.insert(codeRegion, codeDepartement, codeCommune, nomCommune, typeEquipement, x, y, qualitePositionnementXY)) {
            nombre++;
         }
      }

      return nombre;
   }
}
