package fr.ecoemploi.adapters.outbound.spark.dataset.equipement;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.text.MessageFormat;
import java.util.Set;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.domain.model.territoire.CodeRegion;

import org.apache.spark.sql.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;
import org.springframework.transaction.annotation.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Test sur la base équipement.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkEquipementTestApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Transactional
class EquipementDatasetIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 7132772021482720972L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EquipementDatasetIT.class);

   /** Dataset d'équipements. */
   @Autowired
   private EquipementDataset equipementDataset;
   
   /** Dataset des modalités d'équipements. */
   @Autowired
   private ModalitesEquipementDataset modalitesEquipementsDataset;

   /** Année du cog pour les tests. */
   private static final int COG = 2023;
   
   /** Année de la base équipement pour les tests. */
   static final int ANNEE_EQUIPEMENTS = 2023;

   /**
    * Lecture du fichier des équipements, équipement par équipement.
    */
   @DisplayName("Dataset d'équipements unitaires.")
   @ParameterizedTest(name = "Equipements unitaires {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   @Order(10)
   void equipementsUnitaires(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.equipementDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      LOGGER.info("Lecture des équipements unitaires pour l'année {}, restriction : {} → {}, {}", annee, typeRestriction, communes, annee);

      Dataset<Row> equipementsUnitaires = this.equipementDataset.rowEquipements(options, new HistoriqueExecution(), ANNEE_EQUIPEMENTS, false, new EquipementTriDepartementCommuneType());
      assertNotEquals(0, equipementsUnitaires.count(), "Plusieurs equipements auraient du être lus (unitaires).");

      this.equipementDataset.rowEquipements(options, new HistoriqueExecution(), ANNEE_EQUIPEMENTS, false, new EquipementTriDepartementTypeCommune());
      this.equipementDataset.rowEquipements(options, new HistoriqueExecution(), ANNEE_EQUIPEMENTS, false, new EquipementTriTypeDepartementCommune());

      dump(equipementsUnitaires, 200, null);
   }
   
   /**
    * Lecture du fichier des équipements, groupés par commune (nombre d'équipements par commune).
    */
   @DisplayName("Dataset d'équipements globaux par commune (nombre total).")
   @ParameterizedTest(name = "Equipements globaux {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   @Order(20)
   void equipementsGlobaux(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.equipementDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      LOGGER.info("Lecture des équipements globaux pour l'année {}, restriction : {} → {}, {}", annee, typeRestriction, communes, annee);

      Dataset<Row> ds = this.equipementDataset.rowEquipements(options, new HistoriqueExecution(), ANNEE_EQUIPEMENTS, true, new EquipementTriDepartementCommuneType());
      assertNotEquals(0, ds.count(), "Plusieurs equipements auraient du être lus (globaux).");

      this.equipementDataset.rowEquipements(options, new HistoriqueExecution(), ANNEE_EQUIPEMENTS, true, new EquipementTriDepartementTypeCommune());
      this.equipementDataset.rowEquipements(options, new HistoriqueExecution(), ANNEE_EQUIPEMENTS, true, new EquipementTriTypeDepartementCommune());

      dump(ds, 200, null);
   }

   /**
    * Renvoyer la liste des types d'équipements.
    */
   @Test
   @DisplayName("Dataset des types d'équipements.")
   @Order(40)
   void typesEquipements() {
      OptionsCreationLecture options = this.modalitesEquipementsDataset.optionsCreationLecture();

      Dataset<Row> ds = this.modalitesEquipementsDataset.rowLibellesTypesEquipements(options, new HistoriqueExecution(), ANNEE_EQUIPEMENTS);
      assertNotEquals(0L, ds.count(), "Au moins un type d'équipement aurait dû être lu");
      ds.show(1000, false);
   }

   /**
    * Renvoyer la liste des types d'équipements.
    */
   @Test
   @DisplayName("Dataset des modalités.")
   @Order(50)
   void modalites() {
      OptionsCreationLecture options = this.modalitesEquipementsDataset.optionsCreationLecture();

      Dataset<Row> ds = this.modalitesEquipementsDataset.rowModalites(options, new HistoriqueExecution(), ModalitesEquipementDataset.CodeVariableEquipement.TYPE_EQUIPEMENT, ANNEE_EQUIPEMENTS, false, false);
      assertNotEquals(0L, ds.count(), "Au moins une modalité d'équipement aurait dû être lue");
      ds.show(1000, false);

      ds.select("codeTypeModalite").distinct().show(1000, false);
   }

   /**
    * Création d'une matrice code commune X équipements.
    */
   @DisplayName("Création d'une matrice code commune X équipements")
   @ParameterizedTest(name = "Matrice commune X équipements {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   @Order(30)
   void matriceCommunesEquipements(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.modalitesEquipementsDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      LOGGER.info("Matrice commune - équipements pour l'année {}, restriction : {} → {}, {}", annee, typeRestriction, communes, annee);

      Dataset<Row> ds = this.equipementDataset.matriceCommuneEquipements(options, new HistoriqueExecution(), ANNEE_EQUIPEMENTS);
      assertNotEquals(0L, ds.count(), "La matrice des équipements par commune aurait dû être constituée");
      ds.show();
   }
   
   /**
    * Test d'insertion d'équipements (de camping).
    */
   @Test
   // @Commit // Si besoin, placer l'annotation @Commit sur ce test, pour éviter le rollback automatique en fin de test
   @DisplayName("Insertion des équipements 'Campings' en base.")
   @Order(60)
   void insertionEquipements() {
      final String CAMPING = "G103";
      assertDoesNotThrow(() -> this.equipementDataset.insererEquipements(null, COG, ANNEE_EQUIPEMENTS, new HistoriqueExecution(), CAMPING), "L'insertion des équipements dans la base équipement a échoué");
   }

   /**
    * Nombre de campings par région
    */
   @Test
   void campingsParRegion()  {
      OptionsCreationLecture options = this.equipementDataset.optionsCreationLecture();
      Dataset<Row> equipementsUnitaires = this.equipementDataset.rowEquipements(options, new HistoriqueExecution(), ANNEE_EQUIPEMENTS, false, new EquipementTriDepartementCommuneType());

      final String CAMPING = "G103";
      LOGGER.info("Nombre de campings par région :");

      for (CodeRegion codeRegion : CodeRegion.regionsMetropolitaines()) {
         Dataset<Row> campingsRegion = equipementsUnitaires.where((CODE_REGION.col().equalTo(functions.lit(codeRegion.getId())).and(TYPE_EQUIPEMENT.col().equalTo(functions.lit(CAMPING)))));
         long nombreCampings = campingsRegion.count();

         campingsRegion.show(5, false);
         LOGGER.info("Nombre de campings en {} : {}", codeRegion.getNom(), nombreCampings);
         assertNotEquals(0L, nombreCampings, MessageFormat.format("La région {0} devrait avoir au moins un camping", codeRegion.getNom()));
      }
   }

   /**
    * Démarrer le chronomètre.
    */
   @BeforeEach
   public void desactiverCaches() {
      this.chrono.start();
   }
}
