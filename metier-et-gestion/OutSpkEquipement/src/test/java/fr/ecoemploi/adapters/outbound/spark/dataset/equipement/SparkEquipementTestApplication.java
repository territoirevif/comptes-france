package fr.ecoemploi.adapters.outbound.spark.dataset.equipement;

import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;


/**
 * Application de test pour Spark
 * @author Marc LE BIHAN
 */
@SpringBootApplication
@ComponentScan(basePackages={"fr.ecoemploi"})
public class SparkEquipementTestApplication {
}
