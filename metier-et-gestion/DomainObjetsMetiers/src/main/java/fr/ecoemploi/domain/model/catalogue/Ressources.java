package fr.ecoemploi.domain.model.catalogue;

import java.io.Serial;
import java.util.*;

/**
 * Un ensemble de ressources associées à un jeu de données du catalogue datagouv.
 * @author Marc Le Bihan
 */
public class Ressources extends LinkedHashMap<RessourceJeuDeDonneesId, Ressource> implements Iterable<Ressource> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -7113678492964071031L;

   /**
    * Construire une liste de ressouces vides.
    */
   public Ressources() {
   }

   /**
    * Construire une liste de ressources alimentée.
    * @param source Source de ressources à copier.
    */
   public Ressources(Collection<Ressource> source) {
      Objects.requireNonNull(source, "La collection des ressources à copier ne peut pas valoir null.");
      source.forEach(this::add);
   }
   
   /**
    * Ajouter un élément à la liste.
    * @param ressource Ressource à ajouter.
    */
   public void add(Ressource ressource) {
      Objects.requireNonNull(ressource, "La ressource à ajouter ne peut pas valoir null.");
      put(ressource.getId(), ressource);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Iterator<Ressource> iterator() {
      return values().iterator();
   }
}
