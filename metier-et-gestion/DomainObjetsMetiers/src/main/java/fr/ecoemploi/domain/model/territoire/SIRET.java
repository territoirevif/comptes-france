package fr.ecoemploi.domain.model.territoire;

import org.apache.commons.lang3.*;

import java.io.Serial;
import java.text.MessageFormat;

import fr.ecoemploi.domain.utils.autocontrole.*;

/**
 * Un numéro de SIRET.
 * @author Marc LE BIHAN
 */
public class SIRET extends Id {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3267245029993758784L;

   /**
    * Construire un objet SIRET.
    */
   public SIRET() {
   }

   /**
    * Construire un numéro de SIRET alimenté.
    * @param siret SIRET.
    */
   public SIRET(String siret) {
      setId(siret);
   }

   /**
    * Construire un SIRET par copie.
    * @param siret SIRET à copier.
    */
   public SIRET(SIRET siret) {
      super(siret);
   }

   /**
    * Construire un numéro de SIREN alimenté.
    * @param siren SIREN.
    * @param nic Numéro Interne de Classement.
    */
   public SIRET(SIREN siren, NIC nic) {
      setId(siren.getId() + nic.getId());
   }
   
   /**
    * Renvoyer le SIREN associé à un SIRET.
    * @return SIREN.
    * @throws SIRETInvalideException si le SIRET actuel est invalide et ne permet pas l'extraction.
    */
   public SIREN siren() throws SIRETInvalideException {
      if (estSIREN() || valide()) {
         return new SIREN(getId().substring(0, 9));
      }
      
      Anomalies anomalies = new Anomalies();
      anomalies(anomalies);
      
      String message = MessageFormat.format("Impossible d''obtenir un numéro de SIREN à partir du numéro de SIRET ''{0}'' qui est invalide pour ces raisons : {1}.",
         getId(), anomalies);

      throw new SIRETInvalideException(getId(), message);
   }
   
   /**
    * Renvoyer le Numéro Interne de Classement (NIC) associé à un SIRET.
    * @return NIC.
    * @throws SIRETInvalideException si le SIRET actuel est invalide et ne permet pas l'extraction.
    */
   public NIC nic() throws SIRETInvalideException {
      if (estSIREN() || valide()) {
         return new NIC(getId().substring(9));
      }
      
      Anomalies anomalies = new Anomalies();
      anomalies(anomalies);
      
      String message = MessageFormat.format("Impossible d''obtenir un numéro de Numéro Interne de Classement (NIC) à partir du numéro de SIRET ''{0}'' qui est invalide pour ces raisons : {1}.",
         getId(), anomalies);
      throw new SIRETInvalideException(getId(), message);
   }
   
   /**
    * @see Id#anomalies(Anomalies)
    */
   @Override
   public void anomalies(Anomalies anomalies) {
      // Le numéro de SIRET doit être alimenté.
      if (StringUtils.isBlank(getId())) {
         anomalies.declare(SeveriteAnomalie.ERREUR, "Le numéro de SIRET n''est pas alimenté.");
         return; // Et il n'est pas possible d'aller plus loin dans les contrôles dans ce cas.
      }
      
      // Il doit faire 14 caractères.
      if (getId().length() != 14) {
         anomalies.declare(SeveriteAnomalie.ERREUR, "Ce numéro de SIRET ''{0}'' a {1} caractère(s) au lieu des 14 attendus.", getId(), getId().length());
      }
   }

   /**
    * Déterminer si ce SIRET est en fait limité à un SIREN: certains fichiers Open Data peuvent annoncer un champ SIRET, et parfois ne l'alimenter qu'avec un SIREN.
    * @return true si ce qui est dans ce champ est seulement un SIREN.<br>
    * false s'il est blanc ou nul ou que c'est bien un SIRET.
    */
   public boolean estSIREN() {
      String siret = getId();
      return StringUtils.isNotBlank(siret) && siret.length() == 9 && StringUtils.isNumeric(siret);
   }

   /**
    * Déterminer si le numéro de SIREN est valide.
    * @return true si c'est le cas.
    */
   public boolean valide() {
      String siret = getId();
      return StringUtils.isNotBlank(siret) && siret.length() == 14 && StringUtils.isNumeric(siret);
   }
}
