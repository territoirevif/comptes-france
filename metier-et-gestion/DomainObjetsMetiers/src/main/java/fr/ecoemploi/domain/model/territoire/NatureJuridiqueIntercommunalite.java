package fr.ecoemploi.domain.model.territoire;

import java.text.MessageFormat;

/**
 * Nature juridique d'une intercommunalité.
 * @author Marc LE BIHAN
 */
public enum NatureJuridiqueIntercommunalite {
   /** Communauté d'agglomération. */
   COMMUNAUTE_AGGLOMERATION("CA", "Communauté d''agglomération"),
   
   /** communauté de communes */
   COMMUNAUTE_COMMUNES("CC", "Communauté de communes"),
   
   /** Communauté urbaine. */
   COMMUNAUTE_URBAINE("CU", "Communauté urbaine"),
   
   /** Etablissement public territorial. */
   ETABLISSEMENT_PUBLIC_TERRITORIAL("EPT", "Etablissement public territorial"),
   
   /** Métropole. */
   METROPOLE("METRO", "Métropole"),
   
   /** Métropole lyonnaise. */
   METROPOLE_LYON("MET69", "Métropole de Lyon"),
   
   /** Pôle d'équilibre territorial et rural. */
   POLE_EQUILIBRE_TERRITORIAL_ET_RURAL("PETR", "Pôle d''équilibre territorial et rural"),
   
   /** Pôle métropolitain */
   POLE_METROPOLITAIN("POLEM", "Pôle métropolitain"),

   /** Syndicat d'agglomération urbaine. */
   SYNDICAT_AGGLOMERATION_URBAINE("SAN", "Syndicat d''agglomération urbaine"),
   
   /** Syndicat intercommunal à vocation multiple. */
   SYNDICAT_INTERCOMMUNAL_VOCATION_MULTIPLE("SIVOM", "Syndicat intercommunal à vocation multiple"),
   
   /** Syndicat intercommunal à vocation unique. */
   SYNDICAT_INTERCOMMUNAL_VOCATION_UNIQUE("SIVU", "Syndicat intercommunal à vocation unique"),
   
   /** syndicat mixte fermé */
   SYNDICAT_MIXTE_FERME("SMF", "Syndicat mixte fermé"),
   
   /** syndicat mixte ouvert */
   SYNDICAT_MIXTE_OUVERT("SMO", "Syndicat mixte ouvert");
   
   /** Code de cette nature juridique d'intercommunalité. */
   private String code;
   
   /** Libellé de cette nature d'intercommunalité. */
   private final String libelle;
   
   /**
    * Construire une entrée d'énumération en recherchant son libellé public.
    * @param code Code de repérage de cette nature d'intercommunalité.
    * @param libelle Libellé de la nature juridique.
    */
   NatureJuridiqueIntercommunalite(String code, String libelle) {
      this.setCode(code);
      this.libelle = libelle;
   }
   
   /**
    * Renvoyer le code de cette nature d'intercommunalité.
    * @return Code.
    */
   public String getCode() {
      return this.code;
   }

   /**
    * Fixer le code de cette nature d'intercommunalité.
    * @param code Code.
    */
   private void setCode(String code) {
      this.code = code;
   }

   /**
    * Renvoyer le libellé public de l'entrée.
    * @return Libellé.
    */
   public String getLibelle() {
      return this.libelle;
   }
   
   /**
    * Renvoyer une nature juridique d'intercommunalité depuis son code.
    * @param code Code de la nature juridique.
    * @return Nature juridique correspondante.
    * @throws NatureJuridiqueGroupementInconnueException si la nature juridique recherchée n'a pas été trouvée.
    */
   public static NatureJuridiqueIntercommunalite fromCode(String code) throws NatureJuridiqueGroupementInconnueException {
      for(NatureJuridiqueIntercommunalite nature : NatureJuridiqueIntercommunalite.values()) {
         if (nature.getCode().equals(code)) {
            return nature;
         }
      }
      
      // Si la nature recherchée n'a pas été trouvée, lever une exception.
      String message = MessageFormat.format("La nature juridique de groupement ''{0}'' est inconnue.", code);
      throw new NatureJuridiqueGroupementInconnueException(message);
   }
   
   /**
    * Déterminer si cette nature d'intercommunalité est à fiscalité propre.
    * @return true si elle l'est.
    */
   public boolean isFiscalitePropre() {
      switch(this) { // NOSONAR
         case COMMUNAUTE_AGGLOMERATION, COMMUNAUTE_COMMUNES, COMMUNAUTE_URBAINE, ETABLISSEMENT_PUBLIC_TERRITORIAL, METROPOLE, METROPOLE_LYON -> {
            return true;
         }

         case SYNDICAT_AGGLOMERATION_URBAINE, SYNDICAT_INTERCOMMUNAL_VOCATION_MULTIPLE, SYNDICAT_INTERCOMMUNAL_VOCATION_UNIQUE,
            SYNDICAT_MIXTE_FERME, SYNDICAT_MIXTE_OUVERT, POLE_EQUILIBRE_TERRITORIAL_ET_RURAL, POLE_METROPOLITAIN -> {
            return false;
         }
      }

      return false;
   }
   
   /**
    * @see java.lang.Enum#toString()
    */
   @Override
   public String toString() {
      return this.libelle;
   }
}
