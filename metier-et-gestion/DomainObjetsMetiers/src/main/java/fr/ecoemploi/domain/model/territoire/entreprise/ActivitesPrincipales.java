package fr.ecoemploi.domain.model.territoire.entreprise;

import fr.ecoemploi.domain.utils.objets.AbstractObjetsMetiersIndexes;
import fr.ecoemploi.domain.model.territoire.entreprise.ape.*;

import java.io.Serial;

/**
 * Ensemble d'activités principales d'entreprises ou d'établissements.
 * @author Marc LE BIHAN
 */
public class ActivitesPrincipales extends AbstractObjetsMetiersIndexes<CodeAPE, ActivitePrincipale> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5922834561029333891L;
}
