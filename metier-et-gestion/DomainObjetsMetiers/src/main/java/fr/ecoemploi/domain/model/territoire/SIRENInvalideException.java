package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.MetierException;

/**
 * Exception levée si un SIREN est invalide.
 * @author Marc LE BIHAN
 */
public class SIRENInvalideException extends MetierException {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 9130780691857489373L;
   
   /** Siren. */
   private final String siren;
   
   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param siren SIREN qui est invalide.
    */
   public SIRENInvalideException(String siren, String message) {
      super(message);
      this.siren = siren;
   }
   
   /**
    * Renvoyer le SIREN invalide.
    * @return SIREN.
    */
   public String getSIREN() {
      return this.siren;
   }
}
