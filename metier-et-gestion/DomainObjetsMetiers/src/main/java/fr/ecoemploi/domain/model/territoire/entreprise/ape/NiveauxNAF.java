package fr.ecoemploi.domain.model.territoire.entreprise.ape;

import java.io.Serial;
import java.text.*;
import java.util.*;

import org.slf4j.*;

/**
 * Niveaux NAF.
 * @author Marc LE BIHAN
 */
public class NiveauxNAF extends LinkedHashMap<String, NiveauNAF> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -8526226892984117525L;
   
   /** Sous-ensembles par niveaux. */
   private final HashMap<Integer, LinkedHashMap<String, NiveauNAF>> sousEnsemblesParNiveaux = new HashMap<>();
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(NiveauxNAF.class);

   /**
    * Construire les niveaux NAF.
    */
   public NiveauxNAF() {
      try {
         section("A", 1, "01", "02", "03");
         section("B", 1, "05", "06", "07", "08", "09");
         section("C", 1, "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33");
         section("D", 1, "35");
         section("E", 1, "36", "37", "38", "39");
         section("F", 1, "41", "42", "43");
         section("G", 1, "45", "46", "47");
         section("H", 1, "49", "50", "51", "52", "53");
         section("I", 1, "55", "56");
         section("J", 1, "58", "59", "60", "61", "62", "63");
         section("K", 1, "64", "65", "66");
         section("L", 1, "68");
         section("M", 1, "69", "70", "71", "72", "73", "74", "75");
         section("N", 1, "77", "78", "79", "80", "81", "82");
         section("O", 1, "84");
         section("P", 1, "85");
         section("Q", 1, "86", "87", "88");
         section("R", 1, "90", "91", "92", "93");
         section("S", 1, "94", "95", "96");
         section("T", 1, "97", "98");
         section("U", 1, "99");
         
         // FIXME Reconstitution de l'index rapide du niveau 2.
         LinkedHashMap<String, NiveauNAF> sousEnsembleNiveau2 = new LinkedHashMap<>();

         for(NiveauNAF naf : values()) {
            sousEnsembleNiveau2.putAll(naf.getSousNiveauxNAF());
         }
         
         this.sousEnsemblesParNiveaux.put(2, sousEnsembleNiveau2);
      }
      catch(NiveauNAFInexistantException e) {
         LOGGER.error(e.getMessage(), e);
         throw new RuntimeException(e.getMessage(), e);
      }
   }
   
   /**
    * Ajouter une section.
    * @param code Code de la section.
    * @param niveau Niveau NAF.
    * @param codesSousNiveauxNAF Sous-niveaux NAF associés.
    * @throws NiveauNAFInexistantException si le niveau NAF n'a pas été trouvé.
    */
   private void section(String code, int niveau, String... codesSousNiveauxNAF) throws NiveauNAFInexistantException {
      Objects.requireNonNull(code, "Le code de la section ne peut pas valoir null.");
   
      LinkedHashMap<String, NiveauNAF> sousNiveauxNAF = new LinkedHashMap<>();
      NiveauNAF niveauNAF = new NiveauNAF(code, getLibelle(code, niveau), null, niveau, sousNiveauxNAF);
      
      if (codesSousNiveauxNAF != null) {
         for(String sousCode : codesSousNiveauxNAF) {
            NiveauNAF sousNiveauNAF = new NiveauNAF(sousCode, getLibelle(sousCode, niveau+1), niveauNAF, niveau+1, null);
            sousNiveauxNAF.put(sousCode, sousNiveauNAF);
         }
      }
      
      // Liste globale.
      this.put(code, niveauNAF);

      // Sous-ensemble.
      LinkedHashMap<String, NiveauNAF> sousEnsemble = this.sousEnsemblesParNiveaux.computeIfAbsent(niveau, k -> new LinkedHashMap<>());
      sousEnsemble.put(code, niveauNAF);
   }
   
   /**
    * Renvoyer un libellé d'entrée NAF.
    * @param code Code dont on désire le libellé.
    * @param niveau Niveau du code NAF recherché.
    * @return Libellé du code NAF.
    * @throws NiveauNAFInexistantException si le niveau NAF n'a pas été trouvé.
    */
   public static String getLibelle(String code, int niveau) throws NiveauNAFInexistantException {
      Objects.requireNonNull(code, "Le code NAF dont on désire le libellé ne peut pas valoir null.");
      
      String sousCode = getPartieDiscriminante(code, niveau);

      ResourceBundle rsc = ResourceBundle.getBundle("fr.ecoemploi.domain.model.territoire.entreprise.ape.NiveauxNAF");

      try {
         String format = rsc.getString("NAF_" + sousCode);
         return MessageFormat.format(format, new Object[] {});
      }
      catch (MissingResourceException e) {
         String fmt2 = "Le code NAF ''{0}'' de niveau {1} est inconnu.";
         String message = MessageFormat.format(fmt2, sousCode, niveau);
         throw new NiveauNAFInexistantException(message);

      }
   }

   /**
    * Renvoyer la partie discriminante d'un code NAF : 01, 04.1, 04.10, 04.10Z
    * @param code Code dont on désire la chaîne discriminante.
    * @param niveau Niveau de la restriction du code NAF désiré.
    * @return Code NAF réduit à 1, 2, 4, 5 ou 6 caractères selon le cas.
    * <br>Attention ! Ne convertit pas un code APE en section !
    */
   public static String getPartieDiscriminante(String code, int niveau) {
      Objects.requireNonNull(code, "Le code NAF dont on désire le libellé ne peut pas valoir null.");
      int lengthSubstring = getLongueurChaineDiscriminante(niveau);
      
      return niveau > 1 ? code.substring(0, lengthSubstring) : code;
   }

   /**
    * Renvoyer la longueur de la partie discriminante d'un code NAF : 01, 04.1, 04.10, 04.10Z
    * @param niveau Niveau de la restriction du code NAF désiré.
    * @return Code NAF réduit à 1, 2, 4, 5 ou 6 caractères selon le cas.
    */
   public static int getLongueurChaineDiscriminante(int niveau) {
      return switch (niveau) {
         case 1 -> 1;
         case 2 -> 2;
         case 3 -> 4;
         case 4 -> 5;
         case 5 -> 6;

         default -> throw new IllegalArgumentException(MessageFormat.format("Niveau NAF de rang {0} invalide (B).", niveau));
      };
   }
   
   /**
    * Tous les codes NAF d'un niveau NAF sous forme de chaîne de caractères.
    * @param niveau Niveau NAF.
    * @return Codes.
    */
   public List<String> getTousCodesNAFString(int niveau) {
      List<String> sousEnsemble = new ArrayList<>();
      this.sousEnsemblesParNiveaux.get(niveau).values().forEach(c -> sousEnsemble.add(c.getCodeNAF()));
      return sousEnsemble;
   }

   /**
    * Tous les codes NAF d'un niveau NAF.
    * @param niveau Niveau NAF.
    * @return Codes.
    */
   public List<NiveauNAF> getTousCodesNAF(int niveau) {
      return new ArrayList<>(this.sousEnsemblesParNiveaux.get(niveau).values());
   }

   /**
    * Tous les niveaux NAF d'un niveau NAF.
    * @param niveau Niveau NAF.
    * @return Codes.
    */
   public Map<String, NiveauNAF> getTousNAF(int niveau) {
      return this.sousEnsemblesParNiveaux.get(niveau);
   }
}
