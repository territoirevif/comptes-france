package fr.ecoemploi.domain.model.territoire.entreprise;

import java.io.*;
import java.text.*;

import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.model.territoire.entreprise.ape.*;

/**
 * Activité d'une commune.
 * @author Marc LE BIHAN
 */
public class ActiviteCommune implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3283834279341212821L;
   
   /** Code de la commune. */
   private String codeCommune;

   /** Code du département. */
   private String codeDepartement;

   /** Code EPCI de l'intercommunalité. */
   private String codeEPCI;
   
   /** Nom de la commune */
   private String nomCommune;
   
   /** Strate de la commune. */
   private Integer strateCommune;
   
   /** Code APE de l'activité observée */
   private String ape;
   
   /** Libellé du code APE. */
   private String libelleAPE;
   
   /** Section du code NAF. */
   private String section;

   /** Nombre d'entreprises qui ont cette activité. */
   private int nombreEntreprises;
   
   /** Nombre de salariés qui ont cette activité. */
   private double nombreSalaries;
   
   /** Nombre d'actifs qui ont cette activité. */
   private double nombreActifs;
   
   /** L'estimation du nombre de salariés a été fournie par l'INSEE. */
   private boolean estimationFournie;
   
   /** Siren de l'entreprise où une mesure a eu lieu. */
   private String siren;
   
   /** Siret de l'établissement. */
   private String siret;
   
   /** Nom de l'entreprise. */
   private String nomEntreprise;
   
   /** Nom de l'établisseement. */
   private String nomEtablissement;
   
   /** Catégorie juridique de l'établissement. */
   private String categorieJuridique;
   
   /** Libellé de la catégorie juridique. */
   private String libelleCategorieJuridique;
   
   /** Catégorie d'entreprise. */
   private String categorieEntreprise;
   
   /** Catégorie d'entreprise. */
   private String categorieEtablissement;

   /**
    * Construire une activité de commune.
    */
   public ActiviteCommune() {
   }

   /**
    * Construire une activité de commune.
    * @param codeCommune Code de la commune.
    * @param codeDepartement Code du département.
    * @param codeEPCI Code EPCI de l'intercommunalité.
    * @param nomCommune Nom de la commune.
    * @param strateCommune Strate de la commune.
    * @param ape Code APE de l'activité observée.
    * @param section Section du code NAF.
    * @param nombreEntreprises Nombre d'entreprises qui ont cette activité.
    * @param nombreSalaries Nombre de salariés qui ont cette activité :  : un établissement non employeur ou à zéro salarié compte comme ayant zéro salarié, strictement.
    * @param nombreActifs Nombre d'actifs qui ont cette activité : un établissement non employeur ou à zéro salarié compte comme ayant un actif (son dirigeant).
    * @param siren Siren de l'entreprise où une mesure a eu lieu.
    * @param nomEntreprise Nom de l'entreprise. 
    * @param siret Siret de l'établissement où une mesure a eu lieu.
    * @param nomEtablissement Nom de l'établissement.
    * @param categorieJuridique Catégorie juridique de l'établissement.
    * @param libelleCategorieJuridique Libellé de la catégorie juridique.
    * @param libelleAPE Libellé du code APE. S'il n'est pas transmis (null), il est recherché dans le référentiel.
    * @param categorieEntreprise catégorie d'entreprise : PME, ETI, GE (INSEE).
    * @param estimationFournie true si l'INSEE a fourni l'estimation du nombre de salarié,
    * <br>false s'il a laissé la tranche d'effectif vide dans les informations SIRENE.
    * @param categorieEtablissement Catégorie d'établissement, basée sur ses effectifs propres et son caractère employeur
    * EI : Entreprise individuelle<br>
    * NE : Etablissement non employeur<br>
    * TPE : Très petite entreprise (moins de 10 salariés)<br>
    * PE : Petite entreprise (moins de 50 salariés)<br>
    * ME : Moyenne entreprise (moins de 250 salariés)<br>
    * ETI : Entreprise de taille intermédiaire (moins de 5000 salariés)<br>
    * GE : Grande entrprise.
    */
   public ActiviteCommune(String codeCommune, String codeDepartement, String codeEPCI, String nomCommune, Integer strateCommune,
         String ape, String section, int nombreEntreprises, double nombreSalaries, double nombreActifs,
         String siren, String nomEntreprise, String siret, String nomEtablissement, String categorieJuridique,
         String libelleAPE, boolean estimationFournie, String libelleCategorieJuridique, String categorieEntreprise, String categorieEtablissement) {
      this.codeCommune = codeCommune;
      this.codeDepartement = codeDepartement;
      this.codeEPCI = codeEPCI;
      this.nomCommune = nomCommune;
      this.strateCommune = strateCommune;
      this.ape = ape;
      
      // Si le libellé transmis est null, on le recherche dans le référentiel.
      if (libelleAPE == null) {
         try {
            if (this.ape != null && CodeAPE.toNAF(this.ape, 2) != null) {
               this.libelleAPE = CodeAPE.toNAF(this.ape, 2).getLibelle();
            }
         }
         catch(NiveauNAFInexistantException e) {
            this.libelleAPE = e.getMessage();
         }
      }
      else {
         this.libelleAPE = libelleAPE;
      }
      
      this.section = section;
      this.nombreEntreprises = nombreEntreprises;
      this.nombreSalaries = nombreSalaries;
      this.nombreActifs = nombreActifs;
      this.siren = siren;
      this.siret = siret;
      this.nomEntreprise = nomEntreprise; 
      this.nomEtablissement = nomEtablissement;
      this.categorieJuridique = categorieJuridique;
      this.libelleCategorieJuridique = libelleCategorieJuridique;
      this.estimationFournie = estimationFournie;
      this.categorieEntreprise = categorieEntreprise;
      this.categorieEtablissement = categorieEtablissement;
   }

   /**
    * Renvoyer le code APE de l'activité exercée.
    * @return Code APE.
    */
   public String getApe() {
      return this.ape;
   }

   /**
    * Fixer le code APE de l'activité exercée.
    * @param ape Code APE.
    */
   public void setApe(String ape) {
      this.ape = ape;
   }

   /**
    * Renvoyer la catégorie d'entreprise : PME, ETI, GE (INSEE).
    * @return Catégorie d'entreprise.
    */
   public String getCategorieEntreprise() {
      return this.categorieEntreprise;
   }

   /**
    * Fixer la catégorie d'entreprise : PME, ETI, GE (INSEE).
    * @param categorieEntreprise Catégorie d'entreprise.
    */
   public void setCategorieEntreprise(String categorieEntreprise) {
      this.categorieEntreprise = categorieEntreprise;
   }

   /**
    * Fixer la catégorie d'établissement : EI, NE, TPE, PE, ME, ETI, GE.
    * @param categorieEtablissement Catégorie d'établissement, basée sur ses effectifs propres et son caractère employeur
    * EI : Entreprise individuelle<br>
    * NE : Etablissement non employeur<br>
    * TPE : Très petite entreprise (moins de 10 salariés)<br>
    * PE : Petite entreprise (moins de 50 salariés)<br>
    * ME : Moyenne entreprise (moins de 250 salariés)<br>
    * ETI : Entreprise de taille intermédiaire (moins de 5000 salariés)<br>
    * GE : Grande entrprise.
    */
   public void setCategorieEtablissement(String categorieEtablissement) {
      this.categorieEtablissement = categorieEtablissement;
   }

   /**
    * Renvoyer la catégorie d'établissement : EI, NE, TPE, PE, ME, ETI, GE.
    * @return Catégorie d'établissementCatégorie d'établissement, basée sur ses effectifs propres et son caractère employeur
    * EI : Entreprise individuelle<br>
    * NE : Etablissement non employeur<br>
    * TPE : Très petite entreprise (moins de 10 salariés)<br>
    * PE : Petite entreprise (moins de 50 salariés)<br>
    * ME : Moyenne entreprise (moins de 250 salariés)<br>
    * ETI : Entreprise de taille intermédiaire (moins de 5000 salariés)<br>
    * GE : Grande entrprise.
    */
   public String getCategorieEtablissement() {
      return this.categorieEtablissement;
   }

   /**
    * Renvoyer la catégorie juridique de l'établissement.
    * @return Catégorie juridique de l'établissement.
    */
   public String getCategorieJuridique() {
      return this.categorieJuridique;
   }

   /**
    * Fixer la catégorie juridique de l'établissement.
    * @param categorieJuridique Catégorie juridique de l'établissement. 
    */
   public void setCategorieJuridique(String categorieJuridique) {
      this.categorieJuridique = categorieJuridique;
   }
   
   /**
    * Renvoyer le code de la commune.
    * @return code de la commune.
    */
   public CodeCommune asCodeCommune() {
      return new CodeCommune(this.codeCommune);
   }

   /**
    * Renvoyer le code de la commune.
    * @return code de la commune.
    */
   public String getCodeCommune() {
      return this.codeCommune;
   }

   /**
    * Fixer le code de la commune.
    * @param codeCommune Code de la commune.
    */
   public void setCodeCommune(String codeCommune) {
      this.codeCommune = codeCommune;
   }

   /**
    * Renvoyer le code département.
    * @return Code département.
    */
   public String getCodeDepartement() {
      return this.codeDepartement;
   }

   /**
    * Renvoyer le code département.
    * @return Code département
    */
   public CodeDepartement codeDepartement() {
      return new CodeDepartement(this.codeDepartement);
   }

   /**
    * Fixer le code département.
    * @param codeDepartement Code département.
    */
   public void setCodeDepartement(String codeDepartement) {
      this.codeDepartement = codeDepartement;
   }

   /**
    * Renvoyer le code EPCI de l'intercommunalité.
    * @return Code EPCI.
    */
   public SIREN asCodeEPCI() {
      return new SIREN(this.codeEPCI);
   }

   /**
    * Renvoyer le code EPCI de l'intercommunalité.
    * @return Code EPCI.
    */
   public String getCodeEPCI() {
      return this.codeEPCI;
   }

   /**
    * Fixer le code EPCI de l'intercommunalité.
    * @param codeEPCI Code EPCI.
    */
   public void setCodeEPCI(String codeEPCI) {
      this.codeEPCI = codeEPCI;
   }

   /**
    * Déterminer si l'estimation du nombre de salariés a été fournie par l'INSEE.
    * @return true si elle a été founie,
    * <br>false si elle a laissé le champ tranche d'effectif vide dans les informations SIRENE.
    */
   public boolean isEstimationFournie() {
      return this.estimationFournie;
   }

   /**
    * Indiquer si l'estimation du nombre de salariés a été fournie par l'INSEE.
    * @param estimationFournie true si elle a été founie,
    * <br>false si elle a laissé le champ tranche d'effectif vide dans les informations SIRENE.
    */
   public void setEstimationFournie(boolean estimationFournie) {
      this.estimationFournie = estimationFournie;
   }

   /**
    * Renvoyer le libellé du code APE.
    * @return Libellé du code APE.
    */
   public String getLibelleAPE() {
      return this.libelleAPE;
   }

   /**
    * Fixer le libellé du code APE.
    * @param libelleAPE Libellé du code APE.
    */
   public void setLibelleAPE(String libelleAPE) {
      this.libelleAPE = libelleAPE;
   }

   /**
    * Renvoyer le libellé de la catégorie juridique.
    * @return Libellé de la catégorie juridique.
    */
   public String getLibelleCategorieJuridique() {
      return this.libelleCategorieJuridique;
   }

   /**
    * Fixer le libellé de la catégorie juridique.
    * @param libelleCategorieJuridique Libellé de la catégorie juridique.
    */
   public void setLibelleCategorieJuridique(String libelleCategorieJuridique) {
      this.libelleCategorieJuridique = libelleCategorieJuridique;
   }

   /**
    * Renvoyer le nom de la commune.
    * @return Nom de la commune.
    */
   public String getNomCommune() {
      return this.nomCommune;
   }

   /**
    * Fixer le nom de la commune.
    * @param nom Nom de la commune.
    */
   public void setNomCommune(String nom) {
      this.nomCommune = nom;
   }

   /**
    * Renvoyer le nombre d'actifs.
    * @return nombre d'actifs.
    */
   public double getNombreActifs() {
      return this.nombreActifs;
   }

   /**
    * Fixer le nombre d'actifs.
    * @param nombreActifs Nombre d'actifs.
    */
   public void setNombreActifs(double nombreActifs) {
      this.nombreActifs = nombreActifs;
   }

   /**
    * Renvoyer le nombre d'entreprises ayant cette activité dans la commune.
    * @return Nombre d'entreprises.
    */
   public int getNombreEntreprises() {
      return this.nombreEntreprises;
   }

   /**
    * Fixer le nombre d'entreprises ayant cette activité dans la commune.
    * @param nombre Nombre d'entreprises. 
    */
   public void setNombreEntreprises(int nombre) {
      this.nombreEntreprises = nombre;
   }

   /**
    * Renvoyer le nombre de salariés qui exercent cette activité.
    * @return Nombre de salariés.
    */
   public double getNombreSalaries() {
      return this.nombreSalaries;
   }

   /**
    * Fixer le nombre de salariés qui exercent cette activité.
    * @param nombreSalaries Nombre de salariés.
    */
   public void setNombreSalaries(double nombreSalaries) {
      this.nombreSalaries = nombreSalaries;
   }

   /**
    * Renvoyer le nom de l'entreprise.
    * @return Nom de l'entreprise.
    */
   public String getNomEntreprise() {
      return this.nomEntreprise;
   }

   /**
    * Fixer le nom de l'entreprise.
    * @param nomEntreprise Nom de l'entreprise.
    */
   public void setNomEntreprise(String nomEntreprise) {
      this.nomEntreprise = nomEntreprise;
   }

   /**
    * Renvoyer le nom de l'établissement.
    * @return Nom de l'établissement.
    */
   public String getNomEtablissement() {
      return this.nomEtablissement;
   }

   /**
    * Fixer le nom de l'établissement.
    * @param nomEtablissement Nom de l'établissement.
    */
   public void setNomEtablissement(String nomEtablissement) {
      this.nomEtablissement = nomEtablissement;
   }
   
   /**
    * Renvoyer le code SIREN de l'entreprise où la mesure unitaire a eu lieu.
    * @return Code SIREN.
    */
   public String getSiren() {
      return this.siren;
   }

   /**
    * Fixer le code SIREN de l'entreprise où la mesure unitaire a eu lieu.
    * @param siren Code SIREN. 
    */
   public void setSiren(String siren) {
      this.siren = siren;
   }
   
   /**
    * Renvoyer la section du code NAF. 
    * @return section du code NAF.
    */
   public String getSection() {
      return this.section;
   }

   /**
    * Fixer la section du code NAF. 
    * @param section section du code NAF.
    */
   public void setSection(String section) {
      this.section = section;
   }
   
   /**
    * Renvoyer le code SIRET de l'établissement où la mesure unitaire a eu lieu.
    * @return Code SIRET.
    */
   public String getSiret() {
      return this.siret;
   }

   /**
    * Fixer le code SIRET de l'établissement où la mesure unitaire a eu lieu.
    * @param siret Code SIRET. 
    */
   public void setSiret(String siret) {
      this.siret = siret;
   }

   /**
    * Renvoyer la strate de la commune.
    * @return Strate de la commune.
    */
   public Integer getStrateCommune() {
      return this.strateCommune;
   }

   /**
    * Fixer la strate de la commune.
    * @param strateCommune Strate de la commune.
    */
   public void setStrateCommune(Integer strateCommune) {
      this.strateCommune = strateCommune;
   }

   /**
    * @see java.lang.Object#toString()
    */
   @Override
   public String toString() {
      String format = "'{'code département: {17}, code EPCI : {0}, code commune : {1}, nom : {2}, strate communale : {16}, code APE : {3} - {4} : section {5}, "
         + "nombre d''entreprises : {6}, nombre de salariés : {7} (données fournies par l''INSEE : {8}), SIREN : {9}, "
         + "nom entreprise : {10}, SIRET : {11}, nom établissement : {12}, catégorie juridique : {13} - {14}, catégorie d''entreprise (INSEE) : {15}'}'";

      return MessageFormat.format(format, this.codeEPCI, this.codeCommune, this.nomCommune, this.ape, this.libelleAPE,
         this.section, this.nombreEntreprises, this.nombreSalaries, this.estimationFournie, this.siren,
         this.nomEntreprise, this.siret, this.nomEtablissement, this.categorieJuridique, this.libelleCategorieJuridique,
         this.categorieEntreprise, this.strateCommune, this.codeDepartement);
   }
}
