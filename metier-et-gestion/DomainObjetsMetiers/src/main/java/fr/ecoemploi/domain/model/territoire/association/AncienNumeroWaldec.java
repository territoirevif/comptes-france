package fr.ecoemploi.domain.model.territoire.association;

import java.io.*;

/**
 * Ancien numéro waldec d'association.
 * @author Marc Le Bihan
 */
public class AncienNumeroWaldec implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -56692000852123809L;
   
   /** Numéro waldec. */
   private String numeroWaldec;
   
   /**
    * Construire un ancien numéro waldec d'association.
    */
   public AncienNumeroWaldec() {
   }
   
   /**
    * Construire un ancien numéro waldec d'association.
    * @param numero Ancien numéro waldec.
    */
   public AncienNumeroWaldec(String numero) {
      this.numeroWaldec = numero;
   }

   /**
    * Construire un ancien numéro waldec d'association par copie.
    * @param waldec Ancien numéro waldec.
    */
   public AncienNumeroWaldec(AncienNumeroWaldec waldec) {
      this.numeroWaldec = waldec.numeroWaldec;
   }

   /**
    * Renvoyer le numéro waldec.
    * @return Numéro waldec.
    */
   public String getNumeroWaldec() {
      return this.numeroWaldec;
   }
   
   /**
    * Fixer le numéro waldec.
    * @param numeroWaldec waldec.
    */
   public void setNumeroWaldec(String numeroWaldec) {
      this.numeroWaldec = numeroWaldec;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      return this.numeroWaldec;
   }
}
