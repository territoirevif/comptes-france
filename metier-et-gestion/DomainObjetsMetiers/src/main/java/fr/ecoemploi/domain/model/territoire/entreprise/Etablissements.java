package fr.ecoemploi.domain.model.territoire.entreprise;

import java.io.Serial;
import java.util.*;

/**
 * Un ensemble d'établissements.
 */
public class Etablissements extends LinkedHashMap<String, Etablissement> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -863899018743444613L;

   /**
    * Construire une liste d'établissements vide.
    */
   public Etablissements() {
   }
   
   /**
    * Construire une liste d'établissements à partir d'un contenu.
    * @param source Source d'établissements à copier. 
    */
   public Etablissements(Collection<Etablissement> source) {
      Objects.requireNonNull(source, "La collection des établissements à copier ne peut pas valoir null.");
      source.forEach(etablissement -> put(etablissement.getSiret(), etablissement));
   }

   /**
    * Ajouter un élément à la liste.
    * @param etablissement Etablissement à ajouter.
    */
   public void add(Etablissement etablissement) {
      Objects.requireNonNull(etablissement, "L'établissement à ajouter ne peut pas valoir null.");
      put(etablissement.getSiret(), etablissement);
   }
   
   /**
    * Renvoyer la liste des activités principales des établissements.
    * @return Liste des activités principales.
    */
   public Set<String> getActivites() {
      Set<String> activites = new HashSet<>();
      
      for(Etablissement etablissement : values()) {
         activites.add(etablissement.getActivitePrincipale());
      }
      
      return activites;
   }
}
