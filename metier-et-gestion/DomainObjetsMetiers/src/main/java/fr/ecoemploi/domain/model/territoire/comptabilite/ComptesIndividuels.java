package fr.ecoemploi.domain.model.territoire.comptabilite;

import java.io.*;
import java.time.*;

import fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels.*;

/**
 * Classe abstraite des comptes individuels.
 * @param <MR> Comptes individuels avec montant total ou base, par habitant + Moyenne ou Ratio Structure, selon le cas. 
 * @param <RS> Ratio Structure, avec moyennes strates selon le cas.
 * @param <RV> Réductions votées, avec moyennes strates selon le cas.
 * @param <TV> Taux votés, avec moyennes strates selon le cas.
 * @author Marc Le Bihan
 */
public class ComptesIndividuels<MR extends ComptesTotalOuBaseEtParHabitant, RS extends ComptesAvecRatioStructure, RV extends ComptesAvecReductionVotee, TV extends ComptesAvecTauxVote> implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3708127962460435660L;

   /** Exercice comptable. */
   private LocalDate exercice;

   /** Année d'exercice */
   private Integer anneeExercice;

   /** Population légale en vigueur au 1er janvier de l'exercice */
   private Integer population;

   /** Siren de la collectivité, si applicable */
   private String siren;
   
   /** Code de la collectivité, si applicable */
   private String code;
   
   /** Nom de la collectivité */
   private String nom;

   /** PRODUITS DE FONCTIONNEMENT = A */
   private MR produitsDeFonctionnement;

   /** Produits de fonctionnement CAF */
   private MR produitsDeFonctionnementCAF;
   
   /** PRODUITS DE FONCTIONNEMENT = A : CAF : Impôts Locaux */ 
   private RS produitsDeFonctionnementCAFImpotsLocaux;

   /** PRODUITS DE FONCTIONNEMENT = A : CAF : Autres impôts et taxes */
   private RS produitsDeFonctionnementCAFAutresImpotsEtTaxes;

   /** PRODUITS DE FONCTIONNEMENT = A : CAF : Dotation globale de fonctionnement */
   private RS produitsDeFonctionnementCAFDGF;
   
   /** CHARGES DE FONCTIONNEMENT = B */
   private MR chargesDeFonctionnement;

   /** Charges de fonctionnement CAF */
   private MR chargesDeFonctionnementCAF;

   /** CHARGES DE FONCTIONNEMENT = B : CAF : Charges de personnel */
   private RS chargesDeFonctionnementCAFChargesDePersonnel;

   /** CHARGES DE FONCTIONNEMENT = B : CAF : Achats et charges externes */
   private RS chargesDeFonctionnementCAFAchatsEtChargesExternes;

   /** CHARGES DE FONCTIONNEMENT = B : CAF : Charges financières */
   private RS chargesDeFonctionnementCAFChargesFinancieres;

   /** CHARGES DE FONCTIONNEMENT = B : CAF : Subventions versées */
   private RS chargesDeFonctionnementCAFSubventionsVersees;

   /** RÉSULTAT COMPTABLE = A - B = R */
   private MR resultatComptable;

   /** OPERATIONS D'INVESTISSEMENT : RESSOURCES D'INVESTISSEMENT */
   private MR ressourcesInvestissement;

   /** RESSOURCES D'INVESTISSEMENT : Emprunts bancaires et dettes assimilées */
   private RS ressourcesInvestissementEmpruntsBancairesDettesAssimilees;

   /** RESSOURCES D'INVESTISSEMENT : Subventions reçues */
   private RS ressourcesInvestissementSubventionsRecues;

   /** RESSOURCES D'INVESTISSEMENT : Fonds de compensation de la taxe sur la valeur ajoutée (FCTVA) */
   private RS ressourcesInvestissementFCTVA;
      
   /** EMPLOIS D'INVESTISSEMENT */
   private MR emploisInvestissement;

   /** EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement */
   private RS emploisInvestissementDepensesEquipement;

   /** EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées */
   private RS emploisInvestissementRemboursementEmpruntsDettesAssimilees;

   /** AUTOFINANCEMENT : Capacité d'autofinancement = CAF */
   private RS autoFinancementCAF;

   /** AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts */
   private RS autoFinancementCAFNette;

   /** ENDETTEMENT : Encours total de la dette au 31 décembre N */
   private RS endettementEncoursTotalDetteAu31DecembreN;

   /** ENDETTEMENT : Encours des dettes bancaires et assimilées */
   private RS endettementEncoursDettesBancairesEtAssimilees;

   /** ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques */
   private RS endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques;

   /** ENDETTEMENT : Annuité de la dette */
   private RS endettementAnnuiteDette;

   /** ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV) */
   private RV fiscLocalBaseNetteTaxeHabitation;

   /** ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties */
   private RV fiscLocalBaseNetteTaxeFonciereProprietesBaties;

   /** ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties */
   private RV fiscLocalBaseNetteTaxeFonciereProprietesNonBaties;

   /** ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties */
   private MR fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties;

   /** ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises */
   private RV fiscLocalBaseNetteCotisationFonciereEntreprises;

   /** ELEMENTS DE FISCALITE DIRECTE LOCALE : Impôts locaux : Taxe d'habitation */
   private TV impotsLocauxTaxeHabitation;

   /** ELEMENTS DE FISCALITE DIRECTE LOCALE : Impôts locaux : Taxe foncière sur les propriétés bâties */
   private TV impotsLocauxTaxeFonciereProprietesBaties;

   /** ELEMENTS DE FISCALITE DIRECTE LOCALE : Impôts locaux : Taxe foncière sur les propriétés non bâties */
   private TV impotsLocauxTaxeFonciereProprietesNonBaties;
   
   /** Impôts locaux : Cotisation foncière des entreprises */
   private TV impotsLocauxCotisationFonciereEntreprises;

   /** Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties */
   private TV impotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties;

   /** Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises */
   private MR produitsImpotsRepartitionCotisationValeurAjouteeEntreprises;

   /** Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau */
   private MR produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau;

   /** Produits des impôts de répartition : Taxe sur les surfaces commerciales */
   private MR produitsImpotsRepartitionTaxeSurfacesCommerciales;

   /** Les ratios de niveau et de structure (DGCL) */
   private RatiosNiveauxEtStructure ratiosNiveauxEtStructure;
   
   /**
    * Construire une série de comptes individuels. 
    */
   public ComptesIndividuels() {
   }

   /**
    * Constructeur par copie.
    * @param comptes Comptes individuels à reprendre.
    */
   public ComptesIndividuels(ComptesIndividuels<MR, RS, RV, TV> comptes) {
      this.exercice = comptes.exercice;
      this.anneeExercice = comptes.getAnneeExercice();
      this.population = comptes.getPopulation();
      this.siren = comptes.getSiren();
      this.code = comptes.getCode();
      this.nom = comptes.getNom();
      
      this.produitsDeFonctionnement = comptes.getProduitsDeFonctionnement();
      this.produitsDeFonctionnementCAF = comptes.getProduitsDeFonctionnementCAF();
      this.produitsDeFonctionnementCAFImpotsLocaux = comptes.getProduitsDeFonctionnementCAFImpotsLocaux();
      this.produitsDeFonctionnementCAFAutresImpotsEtTaxes = comptes.getProduitsDeFonctionnementCAFAutresImpotsEtTaxes();
      this.produitsDeFonctionnementCAFDGF = comptes.getProduitsDeFonctionnementCAFDGF();
      
      this.chargesDeFonctionnement = comptes.getChargesDeFonctionnement();
      this.chargesDeFonctionnementCAF = comptes.getChargesDeFonctionnementCAF();
      this.chargesDeFonctionnementCAFChargesDePersonnel = comptes.getChargesDeFonctionnementCAFChargesDePersonnel();
      this.chargesDeFonctionnementCAFAchatsEtChargesExternes = comptes.getChargesDeFonctionnementCAFAchatsEtChargesExternes();
      this.chargesDeFonctionnementCAFChargesFinancieres = comptes.getChargesDeFonctionnementCAFChargesFinancieres();
      this.chargesDeFonctionnementCAFSubventionsVersees = comptes.getChargesDeFonctionnementCAFSubventionsVersees();
      
      this.resultatComptable = comptes.getResultatComptable();
      
      this.ressourcesInvestissement = comptes.getRessourcesInvestissement();
      this.ressourcesInvestissementEmpruntsBancairesDettesAssimilees = comptes.getRessourcesInvestissementEmpruntsBancairesDettesAssimilees();
      this.ressourcesInvestissementSubventionsRecues = comptes.getRessourcesInvestissementSubventionsRecues();
      this.ressourcesInvestissementFCTVA = comptes.getRessourcesInvestissementFCTVA();
      
      this.emploisInvestissement = comptes.getEmploisInvestissement();
      this.emploisInvestissementDepensesEquipement = comptes.getEmploisInvestissementDepensesEquipement();
      this.emploisInvestissementRemboursementEmpruntsDettesAssimilees = comptes.getEmploisInvestissementRemboursementEmpruntsDettesAssimilees();
      
      this.endettementEncoursTotalDetteAu31DecembreN = comptes.getEndettementEncoursTotalDetteAu31DecembreN();
      this.endettementEncoursDettesBancairesEtAssimilees = comptes.getEndettementEncoursDettesBancairesEtAssimilees();
      this.endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques = comptes.getEndettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques();
      this.endettementAnnuiteDette = comptes.getEndettementAnnuiteDette();
      
      this.autoFinancementCAF = comptes.getAutoFinancementCAF();
      this.autoFinancementCAFNette = comptes.getAutoFinancementCAFNette();
      this.ratiosNiveauxEtStructure = comptes.getRatiosNiveauxEtStructure();
      
      this.fiscLocalBaseNetteTaxeHabitation = comptes.getFiscLocalBaseNetteTaxeHabitation();
      this.fiscLocalBaseNetteTaxeFonciereProprietesBaties = comptes.getFiscLocalBaseNetteTaxeFonciereProprietesBaties();
      this.fiscLocalBaseNetteTaxeFonciereProprietesNonBaties = comptes.getFiscLocalBaseNetteTaxeFonciereProprietesNonBaties();
      this.fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties = comptes.getFiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties();
      this.fiscLocalBaseNetteCotisationFonciereEntreprises = comptes.getFiscLocalBaseNetteCotisationFonciereEntreprises();
      
      this.impotsLocauxTaxeHabitation = comptes.getImpotsLocauxTaxeHabitation();
      this.impotsLocauxTaxeFonciereProprietesBaties = comptes.getImpotsLocauxTaxeFonciereProprietesBaties();
      this.impotsLocauxTaxeFonciereProprietesNonBaties = comptes.getImpotsLocauxTaxeFonciereProprietesNonBaties();
      this.impotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties = comptes.getImpotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties();
      this.impotsLocauxCotisationFonciereEntreprises = comptes.getImpotsLocauxCotisationFonciereEntreprises();

      this.produitsImpotsRepartitionCotisationValeurAjouteeEntreprises = comptes.getProduitsImpotsRepartitionCotisationValeurAjouteeEntreprises();
      this.produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau = comptes.getProduitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau();
      this.produitsImpotsRepartitionTaxeSurfacesCommerciales = comptes.getProduitsImpotsRepartitionTaxeSurfacesCommerciales();
   }

   /**
    * Construire une série de comptes individuels. 
    * @param anneeExercice Année d'exercice
    * @param population Population légale en vigueur au 1er janvier de l'exercice
    * @param siren Siren de la collectivité locale
    * @param code Code de la commune collectivité locale
    * @param nom Nom de la commune collectivité locale
    * @param produitsDeFonctionnement PRODUITS DE FONCTIONNEMENT = A
    * @param produitsDeFonctionnementCAF Produits de fonctionnement CAF
    * @param produitsDeFonctionnementCAFImpotsLocaux PRODUITS DE FONCTIONNEMENT = A : CAF : Impôts Locaux
    * @param produitsDeFonctionnementCAFAutresImpotsEtTaxes PRODUITS DE FONCTIONNEMENT = A : CAF : Autres impôts et taxes
    * @param produitsDeFonctionnementCAFDGF PRODUITS DE FONCTIONNEMENT = A : CAF : Dotation globale de fonctionnement
    * @param chargesDeFonctionnement CHARGES DE FONCTIONNEMENT = B
    * @param chargesDeFonctionnementCAF Charges de fonctionnement CAF
    * @param chargesDeFonctionnementCAFChargesDePersonnel CHARGES DE FONCTIONNEMENT = B : CAF : Charges de personnel
    * @param chargesDeFonctionnementCAFAchatsEtChargesExternes CHARGES DE FONCTIONNEMENT = B : CAF : Achats et charges externes
    * @param chargesDeFonctionnementCAFChargesFinancieres CHARGES DE FONCTIONNEMENT = B : CAF : Charges financières
    * @param chargesDeFonctionnementCAFSubventionsVersees CHARGES DE FONCTIONNEMENT = B : CAF : Subventions versées
    * @param resultatComptable RÉSULTAT COMPTABLE = A - B = R
    * @param ressourcesInvestissement OPERATIONS D'INVESTISSEMENT : RESSOURCES D'INVESTISSEMENT
    * @param ressourcesInvestissementEmpruntsBancairesDettesAssimilees RESSOURCES D'INVESTISSEMENT : Emprunts bancaires et dettes assimilées
    * @param ressourcesInvestissementSubventionsRecues RESSOURCES D'INVESTISSEMENT : Subventions reçues
    * @param ressourcesInvestissementFCTVA RESSOURCES D'INVESTISSEMENT : Fonds de compensation de la taxe sur la valeur ajoutée (FCTVA)
    * @param emploisInvestissement EMPLOIS D'INVESTISSEMENT
    * @param emploisInvestissementDepensesEquipement EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement
    * @param emploisInvestissementRemboursementEmpruntsDettesAssimilees EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées
    * @param autoFinancementCAF AUTOFINANCEMENT : Capacité d'autofinancement = CAF
    * @param autoFinancementCAFNette AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts
    * @param endettementEncoursTotalDetteAu31DecembreN ENDETTEMENT : Encours total de la dette au 31 décembre N
    * @param endettementAnnuiteDette ENDETTEMENT : Annuité de la dette
    * @param endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques
    * @param endettementEncoursDettesBancairesEtAssimilees ENDETTEMENT : Encours des dettes bancaires et assimilées
    * @param fiscLocalBaseNetteTaxeHabitation ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV)
    * @param fiscLocalBaseNetteTaxeFonciereProprietesBaties ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties
    * @param fiscLocalBaseNetteTaxeFonciereProprietesNonBaties ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties
    * @param fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties
    * @param fiscLocalBaseNetteCotisationFonciereEntreprises ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises
    * @param impotsLocauxTaxeHabitation ELEMENTS DE FISCALITE DIRECTE LOCALE : Impôts locaux : Taxe d'habitation
    * @param impotsLocauxTaxeFonciereProprietesBaties ELEMENTS DE FISCALITE DIRECTE LOCALE : Impôts locaux : Taxe foncière sur les propriétés bâties
    * @param impotsLocauxTaxeFonciereProprietesNonBaties ELEMENTS DE FISCALITE DIRECTE LOCALE : Impôts locaux : Taxe foncière sur les propriétés non bâties
    * @param impotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties
    * @param impotsLocauxCotisationFonciereEntreprises Impôts locaux : Cotisation foncière des entreprises
    * @param produitsImpotsRepartitionCotisationValeurAjouteeEntreprises Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises
    * @param produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau
    * @param produitsImpotsRepartitionTaxeSurfacesCommerciales Produits des impôts de répartition : Taxe sur les surfaces commerciales
    * @param ratiosNiveauxEtStructure Ratios de niveaux et de structure (DGCL).
    */
   public ComptesIndividuels(Integer anneeExercice, Integer population, String siren, String code, String nom,
      MR produitsDeFonctionnement, MR produitsDeFonctionnementCAF, 
      RS produitsDeFonctionnementCAFImpotsLocaux, RS produitsDeFonctionnementCAFAutresImpotsEtTaxes,
      RS produitsDeFonctionnementCAFDGF, MR chargesDeFonctionnement,
      MR chargesDeFonctionnementCAF,
      RS chargesDeFonctionnementCAFChargesDePersonnel, RS chargesDeFonctionnementCAFAchatsEtChargesExternes,
      RS chargesDeFonctionnementCAFChargesFinancieres, RS chargesDeFonctionnementCAFSubventionsVersees, 
      MR resultatComptable, 
      MR ressourcesInvestissement, RS ressourcesInvestissementEmpruntsBancairesDettesAssimilees,
      RS ressourcesInvestissementSubventionsRecues, RS ressourcesInvestissementFCTVA,
      MR emploisInvestissement,
      RS emploisInvestissementDepensesEquipement, RS emploisInvestissementRemboursementEmpruntsDettesAssimilees,
      RS autoFinancementCAF, RS autoFinancementCAFNette,
      RS endettementEncoursTotalDetteAu31DecembreN, RS endettementAnnuiteDette, 
      RS endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques, RS endettementEncoursDettesBancairesEtAssimilees,      
      RV fiscLocalBaseNetteTaxeHabitation, RV fiscLocalBaseNetteTaxeFonciereProprietesBaties,
      RV fiscLocalBaseNetteTaxeFonciereProprietesNonBaties, MR fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties, RV fiscLocalBaseNetteCotisationFonciereEntreprises,
      TV impotsLocauxTaxeHabitation, TV impotsLocauxTaxeFonciereProprietesBaties,
      TV impotsLocauxTaxeFonciereProprietesNonBaties, TV impotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties, TV impotsLocauxCotisationFonciereEntreprises,
      MR produitsImpotsRepartitionCotisationValeurAjouteeEntreprises, MR produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau,
      MR produitsImpotsRepartitionTaxeSurfacesCommerciales, 
      RatiosNiveauxEtStructure ratiosNiveauxEtStructure) {
      this.anneeExercice = anneeExercice;
      this.exercice = LocalDate.of(anneeExercice, 1, 1);
      this.population = population;
      this.siren = siren;
      this.code = code;
      this.nom = nom;
      
      this.produitsDeFonctionnement = produitsDeFonctionnement;
      this.produitsDeFonctionnementCAF = produitsDeFonctionnementCAF;
      this.produitsDeFonctionnementCAFImpotsLocaux = produitsDeFonctionnementCAFImpotsLocaux;
      this.produitsDeFonctionnementCAFAutresImpotsEtTaxes = produitsDeFonctionnementCAFAutresImpotsEtTaxes;
      this.produitsDeFonctionnementCAFDGF = produitsDeFonctionnementCAFDGF;

      this.chargesDeFonctionnement = chargesDeFonctionnement;
      this.chargesDeFonctionnementCAF = chargesDeFonctionnementCAF;
      this.chargesDeFonctionnementCAFChargesDePersonnel = chargesDeFonctionnementCAFChargesDePersonnel;
      this.chargesDeFonctionnementCAFAchatsEtChargesExternes = chargesDeFonctionnementCAFAchatsEtChargesExternes;
      this.chargesDeFonctionnementCAFChargesFinancieres = chargesDeFonctionnementCAFChargesFinancieres;
      this.chargesDeFonctionnementCAFSubventionsVersees = chargesDeFonctionnementCAFSubventionsVersees;
      this.resultatComptable = resultatComptable;
      
      this.ressourcesInvestissement = ressourcesInvestissement;
      this.ressourcesInvestissementEmpruntsBancairesDettesAssimilees = ressourcesInvestissementEmpruntsBancairesDettesAssimilees;
      this.ressourcesInvestissementSubventionsRecues = ressourcesInvestissementSubventionsRecues;
      this.ressourcesInvestissementFCTVA = ressourcesInvestissementFCTVA;
      
      this.emploisInvestissement = emploisInvestissement;
      this.emploisInvestissementDepensesEquipement = emploisInvestissementDepensesEquipement;
      this.emploisInvestissementRemboursementEmpruntsDettesAssimilees = emploisInvestissementRemboursementEmpruntsDettesAssimilees;

      this.autoFinancementCAF = autoFinancementCAF;
      this.autoFinancementCAFNette = autoFinancementCAFNette;

      this.endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques = endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques;
      this.endettementEncoursDettesBancairesEtAssimilees = endettementEncoursDettesBancairesEtAssimilees;
      this.endettementEncoursTotalDetteAu31DecembreN = endettementEncoursTotalDetteAu31DecembreN;
      this.endettementAnnuiteDette = endettementAnnuiteDette;
      
      this.fiscLocalBaseNetteTaxeHabitation = fiscLocalBaseNetteTaxeHabitation;
      this.fiscLocalBaseNetteTaxeFonciereProprietesBaties = fiscLocalBaseNetteTaxeFonciereProprietesBaties;
      this.fiscLocalBaseNetteTaxeFonciereProprietesNonBaties = fiscLocalBaseNetteTaxeFonciereProprietesNonBaties;
      this.fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties = fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties;
      this.fiscLocalBaseNetteCotisationFonciereEntreprises = fiscLocalBaseNetteCotisationFonciereEntreprises;
      
      this.impotsLocauxTaxeHabitation = impotsLocauxTaxeHabitation;
      this.impotsLocauxTaxeFonciereProprietesBaties = impotsLocauxTaxeFonciereProprietesBaties;
      this.impotsLocauxTaxeFonciereProprietesNonBaties = impotsLocauxTaxeFonciereProprietesNonBaties;
      this.impotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties = impotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties;
      this.impotsLocauxCotisationFonciereEntreprises = impotsLocauxCotisationFonciereEntreprises;
      
      this.produitsImpotsRepartitionCotisationValeurAjouteeEntreprises = produitsImpotsRepartitionCotisationValeurAjouteeEntreprises;
      this.produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau = produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau;
      this.produitsImpotsRepartitionTaxeSurfacesCommerciales = produitsImpotsRepartitionTaxeSurfacesCommerciales;

      this.ratiosNiveauxEtStructure = ratiosNiveauxEtStructure;
   }

   /**
    * Renvoyer l'exercice.
    * @return Exercice.
    */
   public LocalDate getExercice() {
      return this.exercice;
   }

   /**
    * Fixer l'exercice.
    * @param exercice Exercice.
    */
   public void setExercice(LocalDate exercice) {
      this.exercice = exercice;
   }

   /**
    * Renvoyer l'année d'exercice.
    * @return Année d'exercice
    */
   public Integer getAnneeExercice() {
      return this.anneeExercice;
   }

   /**
    * Fixer l'année d'exercice.
    * @param anneeExercice Année d'exercice. 
    */
   public void setAnneeExercice(Integer anneeExercice) {
      this.anneeExercice = anneeExercice;
   }

   /**
    * Renvoyer l'AUTOFINANCEMENT : Capacité d'autofinancement = CAF. 
    * @return AUTOFINANCEMENT : Capacité d'autofinancement = CAF.
    */
   public RS getAutoFinancementCAF() {
      return this.autoFinancementCAF;
   }

   /**
    * Fixer l'AUTOFINANCEMENT : Capacité d'autofinancement = CAF.
    * 
    * @param comptes AUTOFINANCEMENT : Capacité d'autofinancement = CAF.
    */
   public void setAutoFinancementCAF(RS comptes) {
      this.autoFinancementCAF = comptes;
   }

   /**
    * Renvoyer l'AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts. 
    * @return AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts.
    */
   public RS getAutoFinancementCAFNette() {
      return this.autoFinancementCAFNette;
   }

   /**
    * Fixer l'AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts.
    * @param comptes AUTOFINANCEMENT : CAF nette du remboursement en capital des emprunts.
    */
   public void setAutoFinancementCAFNette(RS comptes) {
      this.autoFinancementCAFNette = comptes;
   }

   /**
    * Renvoyer le code de la collectivité locale.
    * @return Code de la collectivité locale.
    */
   public String getCode() {
      return this.code;
   }

   /**
    * Fixer le code de la collectivité locale.
    * @param code Code de la collectivité locale.
    */
   public void setCode(String code) {
      this.code = code;
   }

   /**
    * Renvoyer le nom de la collectivité locale.
    * @return Nom de la collectivité locale.
    */
   public String getNom() {
      return this.nom;
   }

   /**
    * Fixer le nom de la collectivité locale.
    * @param nom Nom de la collectivité locale.
    */
   public void setNom(String nom) {
      this.nom = nom;
   }
   
   /**
    * Renvoyer la population légale en vigueur au 1er janvier de l'exercice.
    * @return Population légale en vigueur au 1er janvier de l'exercice.
    */
   public Integer getPopulation() {
      return this.population;
   }

   /**
    * Fixer la population légale en vigueur au 1er janvier de l'exercice.
    * @param population Population légale en vigueur au 1er janvier de l'exercice. 
    */
   public void setPopulation(Integer population) {
      this.population = population;
   }

   /**
    * Renvoyer les CHARGES DE FONCTIONNEMENT = B
    * @return Comptes associés.
    */
   public MR getChargesDeFonctionnement() {
      return this.chargesDeFonctionnement;
   }

   /**
    * Fixer les CHARGES DE FONCTIONNEMENT = B 
    * @param comptes Comptes associés.
    */
   public void setChargesDeFonctionnement(MR comptes) {
      this.chargesDeFonctionnement = comptes;
   }

   /**
    * Renvoyer les Charges de fonctionnement CAF. 
    * @return Charges de fonctionnement CAF.
    */
   public MR getChargesDeFonctionnementCAF() {
      return this.chargesDeFonctionnementCAF;
   }

   /**
    * Fixer les Charges de fonctionnement CAF. 
    * @param comptes Charges de fonctionnement CAF.
    */
   public void setChargesDeFonctionnementCAF(MR comptes) {
      this.chargesDeFonctionnementCAF = comptes;
   }

   /**
    * Renvoyer les CHARGES DE FONCTIONNEMENT = B : CAF : Charges de personnel
    * @return Comptes associés.
    */
   public RS getChargesDeFonctionnementCAFChargesDePersonnel() {
      return this.chargesDeFonctionnementCAFChargesDePersonnel;
   }

   /**
    * Fixer les CHARGES DE FONCTIONNEMENT = B : CAF : Charges de personnel
    * @param comptes Comptes associés.
    */
   public void setChargesDeFonctionnementCAFChargesDePersonnel(RS comptes) {
      this.chargesDeFonctionnementCAFChargesDePersonnel = comptes;
   }

   /**
    * Renvoyer les CHARGES DE FONCTIONNEMENT = B : CAF : Achats et charges externes
    * @return Comptes associés.
    */
   public RS getChargesDeFonctionnementCAFAchatsEtChargesExternes() {
      return this.chargesDeFonctionnementCAFAchatsEtChargesExternes;
   }

   /**
    * Fixer les CHARGES DE FONCTIONNEMENT = B : CAF : Achats et charges externes
    * @param comptes Comptes associés.
    */
   public void setChargesDeFonctionnementCAFAchatsEtChargesExternes(RS comptes) {
      this.chargesDeFonctionnementCAFAchatsEtChargesExternes = comptes;
   }

   /**
    * Renvoyer les CHARGES DE FONCTIONNEMENT = B : CAF : Charges financières
    * @return Comptes associés.
    */
   public RS getChargesDeFonctionnementCAFChargesFinancieres() {
      return this.chargesDeFonctionnementCAFChargesFinancieres;
   }

   /**
    * Fixer les CHARGES DE FONCTIONNEMENT = B : CAF : Charges financières
    * @param comptes Comptes associés.
    */
   public void setChargesDeFonctionnementCAFChargesFinancieres(RS comptes) {
      this.chargesDeFonctionnementCAFChargesFinancieres = comptes;
   }

   /**
    * Renvoyer les CHARGES DE FONCTIONNEMENT = B : CAF : Subventions versées
    * @return Comptes associés.
    */
   public RS getChargesDeFonctionnementCAFSubventionsVersees() {
      return this.chargesDeFonctionnementCAFSubventionsVersees;
   }

   /**
    * Fixer les CHARGES DE FONCTIONNEMENT = B : CAF : Subventions versées
    * @param montant Comptes associés.
    */
   public void setChargesDeFonctionnementCAFSubventionsVersees(RS montant) {
      this.chargesDeFonctionnementCAFSubventionsVersees = montant;
   }

   /**
    * Renvoyer les EMPLOIS D'INVESTISSEMENT. 
    * @return EMPLOIS D'INVESTISSEMENT.
    */
   public MR getEmploisInvestissement() {
      return this.emploisInvestissement;
   }

   /**
    * Fixer les EMPLOIS D'INVESTISSEMENT. 
    * @param comptes EMPLOIS D'INVESTISSEMENT.
    */
   public void setEmploisInvestissement(MR comptes) {
      this.emploisInvestissement = comptes;
   }

   /**
    * Renvoyer les EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement. 
    * @return EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement.
    */
   public RS getEmploisInvestissementDepensesEquipement() {
      return this.emploisInvestissementDepensesEquipement;
   }

   /**
    * Fixer les EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement. 
    * @param comptes EMPLOIS D'INVESTISSEMENT : Dépenses d'équipement.
    */
   public void setEmploisInvestissementDepensesEquipement(RS comptes) {
      this.emploisInvestissementDepensesEquipement = comptes;
   }

   /**
    * Renvoyer les EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées. 
    * @return EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées
    */
   public RS getEmploisInvestissementRemboursementEmpruntsDettesAssimilees() {
      return this.emploisInvestissementRemboursementEmpruntsDettesAssimilees;
   }

   /**
    * Fixer les EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées. 
    * @param comptes EMPLOIS D'INVESTISSEMENT : Remboursement d'emprunts et dettes assimilées.
    */
   public void setEmploisInvestissementRemboursementEmpruntsDettesAssimilees(RS comptes) {
      this.emploisInvestissementRemboursementEmpruntsDettesAssimilees = comptes;
   }

   /**
    * Renvoyer l'ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques. 
    * @return ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques.
    */
   public RS getEndettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques() {
      return this.endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques;
   }

   /**
    * Fixer l'ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques. 
    * @param comptes ENDETTEMENT : Encours dette bancaire nette soutien sortie emprunts toxiques.
    */
   public void setEndettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques(RS comptes) {
      this.endettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques = comptes;
   }

   /**
    * Renvoyer l'ENDETTEMENT : Encours des dettes bancaires et assimilées. 
    * @return ENDETTEMENT : Encours des dettes bancaires et assimilées.
    */
   public RS getEndettementEncoursDettesBancairesEtAssimilees() {
      return this.endettementEncoursDettesBancairesEtAssimilees;
   }

   /**
    * Fixer l'ENDETTEMENT : Encours des dettes bancaires et assimilées.
    * @param comptes ENDETTEMENT : Encours des dettes bancaires et assimilées.
    */
   public void setEndettementEncoursDettesBancairesEtAssimilees(RS comptes) {
      this.endettementEncoursDettesBancairesEtAssimilees = comptes;
   }

   /**
    * Renvoyer l'ENDETTEMENT : Encours total de la dette au 31 décembre N. 
    * @return ENDETTEMENT : Encours total de la dette au 31 décembre N.
    */
   public RS getEndettementEncoursTotalDetteAu31DecembreN() {
      return this.endettementEncoursTotalDetteAu31DecembreN;
   }

   /**
    * Fixer l'ENDETTEMENT : Encours total de la dette au 31 décembre N. 
    * @param comptes ENDETTEMENT : Encours total de la dette au 31 décembre N.
    */
   public void setEndettementEncoursTotalDetteAu31DecembreN(RS comptes) {
      this.endettementEncoursTotalDetteAu31DecembreN = comptes;
   }

   /**
    * Renvoyer l'ENDETTEMENT : Annuité de la dette. 
    * @return ENDETTEMENT : Annuité de la dette.
    */
   public RS getEndettementAnnuiteDette() {
      return this.endettementAnnuiteDette;
   }

   /**
    * Fixer l'ENDETTEMENT : Annuité de la dette. 
    * @param comptes ENDETTEMENT : Annuité de la dette.
    */
   public void setEndettementAnnuiteDette(RS comptes) {
      this.endettementAnnuiteDette = comptes;
   }

   /**
    * Renvoyer les ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV).
    * @return ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV).
    */
   public RV getFiscLocalBaseNetteTaxeHabitation() {
      return this.fiscLocalBaseNetteTaxeHabitation;
   }

   /**
    * Fixer les ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV). 
    * @param comptes ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe d'habitation (y compris THLV).
    */
   public void setFiscLocalBaseNetteTaxeHabitation(RV comptes) {
      this.fiscLocalBaseNetteTaxeHabitation = comptes;
   }

   /**
    * Renvoyer les ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties. 
    * @return ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties.
    */
   public RV getFiscLocalBaseNetteTaxeFonciereProprietesBaties() {
      return this.fiscLocalBaseNetteTaxeFonciereProprietesBaties;
   }

   /**
    * Fixer les ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties. 
    * @param comptes ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés bâties.
    */
   public void setFiscLocalBaseNetteTaxeFonciereProprietesBaties(RV comptes) {
      this.fiscLocalBaseNetteTaxeFonciereProprietesBaties = comptes;
   }

   /**
    * Renvoyer les ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties. 
    * @return ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties.
    */
   public RV getFiscLocalBaseNetteTaxeFonciereProprietesNonBaties() {
      return this.fiscLocalBaseNetteTaxeFonciereProprietesNonBaties;
   }

   /**
    * Fixer les ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties. 
    * @param comptes ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe foncière sur les propriétés non bâties.
    */
   public void setFiscLocalBaseNetteTaxeFonciereProprietesNonBaties(RV comptes) {
      this.fiscLocalBaseNetteTaxeFonciereProprietesNonBaties = comptes;
   }

   /**
    * Renvoyer les ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties. 
    * @return ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties
    */
   public MR getFiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties() {
      return this.fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties;
   }

   /**
    * Fixer les ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties. 
    * @param comptes ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Taxe additionnelle à la taxe foncière sur les propriétés non bâties
    */
   public void setFiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties(MR comptes) {
      this.fiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties = comptes;
   }

   /**
    * Renvoyer les ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises. 
    * @return ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises.
    */
   public RV getFiscLocalBaseNetteCotisationFonciereEntreprises() {
      return this.fiscLocalBaseNetteCotisationFonciereEntreprises;
   }

   /**
    * Fixer les ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises.
    * @param comptes ELEMENTS DE FISCALITE DIRECTE LOCALE : Base nette : Cotisation foncière des entreprises.
    */
   public void setFiscLocalBaseNetteCotisationFonciereEntreprises(RV comptes) {
      this.fiscLocalBaseNetteCotisationFonciereEntreprises = comptes;
   }

   /**
    * Renvoyer les ratios de niveaux et de structure (Direction Générale des Collectivités Locales).
    * @return Ratios de niveaux et de structure.
    */
   public RatiosNiveauxEtStructure getRatiosNiveauxEtStructure() {
      return this.ratiosNiveauxEtStructure;
   }

   /**
    * Fixer les ratios de niveaux et de structure (Direction Générale des Collectivités Locales).
    * @param ratiosNiveauxEtStructure Ratios de niveaux et de structure.
    */
   public void setRatiosNiveauxEtStructure(RatiosNiveauxEtStructure ratiosNiveauxEtStructure) {
      this.ratiosNiveauxEtStructure = ratiosNiveauxEtStructure;
   }

   /**
    * Renvoyer le total des ressources d'investissement (opérations d'investissement). 
    * @return Montant.
    */
   public MR getRessourcesInvestissement() {
      return this.ressourcesInvestissement;
   }

   /**
    * Fixer le total des ressources d'investissement (opérations d'investissement).
    * @param comptes Comptes associés.
    */
   public void setRessourcesInvestissement(MR comptes) {
      this.ressourcesInvestissement = comptes;
   }

   /**
    * Renvoyer le montant total des ressources d'investissement pour Emprunts bancaires et dettes assimilées.
    * @return Comptes associés.
    */
   public RS getRessourcesInvestissementEmpruntsBancairesDettesAssimilees() {
      return this.ressourcesInvestissementEmpruntsBancairesDettesAssimilees;
   }

   /**
    * Fixer le montant total des ressources d'investissement pour Emprunts bancaires et dettes assimilées.
    * @param comptes Comptes associés.
    */
   public void setRessourcesInvestissementEmpruntsBancairesDettesAssimilees(RS comptes) {
      this.ressourcesInvestissementEmpruntsBancairesDettesAssimilees = comptes;
   }

   /**
    * Renvoyer la Taxe d'habitation totale (impôts locaux).
    * @return Taxe d'habitation.
    */
   public TV getImpotsLocauxTaxeHabitation() {
      return this.impotsLocauxTaxeHabitation;
   }

   /**
    * Fixer la Taxe d'habitation totale (impôts locaux).
    * @param comptes Taxe d'habitation.
    */
   public void setImpotsLocauxTaxeHabitation(TV comptes) {
      this.impotsLocauxTaxeHabitation = comptes;
   }

   /**
    * Renvoyer le total de la taxe foncière sur les propriétés bâties (impôts locaux).
    * @return Total de la TFB.
    */
   public TV getImpotsLocauxTaxeFonciereProprietesBaties() {
      return this.impotsLocauxTaxeFonciereProprietesBaties;
   }

   /**
    * Renvoyer les Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties. 
    * @return les Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties.
    */
   public TV getImpotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties() {
      return this.impotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties;
   }

   /**
    * Fixer les Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties.
    * @param comptes les Impôts locaux : Taxe additionnelle à la taxe foncière sur les propriétés non bâties.
    */
   public void setImpotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties(TV comptes) {
      this.impotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties = comptes;
   }

   /**
    * Fixer le total de la taxe foncière sur les propriétés bâties (impôts locaux). 
    * @param comptes Montant de la TFB.
    */
   public void setImpotsLocauxTaxeFonciereProprietesBaties(TV comptes) {
      this.impotsLocauxTaxeFonciereProprietesBaties = comptes;
   }

   /**
    * Renvoyer le total de la taxe foncière sur les propriétés non bâties (impôts locaux).
    * @return Total de la TFB.
    */
   public TV getImpotsLocauxTaxeFonciereProprietesNonBaties() {
      return this.impotsLocauxTaxeFonciereProprietesNonBaties;
   }

   /**
    * Fixer le total de la taxe foncière sur les propriétés non bâties (impôts locaux). 
    * @param comptes Montant de la TFB.
    */
   public void setImpotsLocauxTaxeFonciereProprietesNonBaties(TV comptes) {
      this.impotsLocauxTaxeFonciereProprietesNonBaties = comptes;
   }

   /**
    * Renvoyer les Impôts locaux : Cotisation foncière des entreprises. 
    * @return Impôts locaux : Cotisation foncière des entreprises.
    */
   public TV getImpotsLocauxCotisationFonciereEntreprises() {
      return this.impotsLocauxCotisationFonciereEntreprises;
   }

   /**
    * Fixer les Impôts locaux : Cotisation foncière des entreprises. 
    * @param comptes Impôts locaux : Cotisation foncière des entreprises.
    */
   public void setImpotsLocauxCotisationFonciereEntreprises(TV comptes) {
      this.impotsLocauxCotisationFonciereEntreprises = comptes;
   }
   
   /**
    * Renvoyer les PRODUITS DE FONCTIONNEMENT = A
    * @return Comptes associés.
    */
   public MR getProduitsDeFonctionnement() {
      return this.produitsDeFonctionnement;
   }

   /**
    * Fixer les PRODUITS DE FONCTIONNEMENT = A
    * @param comptes Comptes associés.
    */
   public void setProduitsDeFonctionnement(MR comptes) {
      this.produitsDeFonctionnement = comptes;
   }

   /**
    * Renvoyer les Produits de fonctionnement CAF. 
    * @return Produits de fonctionnement CAF.
    */
   public MR getProduitsDeFonctionnementCAF() {
      return this.produitsDeFonctionnementCAF;
   }

   /**
    * Fixer les Produits de fonctionnement CAF. 
    * @param comptes Produits de fonctionnement CAF.
    */
   public void setProduitsDeFonctionnementCAF(MR comptes) {
      this.produitsDeFonctionnementCAF = comptes;
   }

   /**
    * Renvoyer les PRODUITS DE FONCTIONNEMENT = A : CAF  : Impôts Locaux
    * @return Comptes associés.
    */
   public RS getProduitsDeFonctionnementCAFImpotsLocaux() {
      return this.produitsDeFonctionnementCAFImpotsLocaux;
   }

   /**
    * Fixer les PRODUITS DE FONCTIONNEMENT = A : CAF  : Impôts Locaux
    * @param comptes Comptes associés.
    */
   public void setProduitsDeFonctionnementCAFImpotsLocaux(RS comptes) {
      this.produitsDeFonctionnementCAFImpotsLocaux = comptes;
   }

   /**
    * Renvoyer les PRODUITS DE FONCTIONNEMENT = A : CAF : Autres impôts et taxes
    * @return Comptes associés.
    */
   public RS getProduitsDeFonctionnementCAFAutresImpotsEtTaxes() {
      return this.produitsDeFonctionnementCAFAutresImpotsEtTaxes;
   }

   /**
    * Fixer les PRODUITS DE FONCTIONNEMENT = A : CAF : Autres impôts et taxes
    * @param comptes Comptes associés.
    */
   public void setProduitsDeFonctionnementCAFAutresImpotsEtTaxes(RS comptes) {
      this.produitsDeFonctionnementCAFAutresImpotsEtTaxes = comptes;
   }

   /**
    * Renvoyer les PRODUITS DE FONCTIONNEMENT = A : CAF : Dotation globale de fonctionnement 
    * @return Comptes associés.
    */
   public RS getProduitsDeFonctionnementCAFDGF() {
      return this.produitsDeFonctionnementCAFDGF;
   }

   /**
    * Fixer les PRODUITS DE FONCTIONNEMENT = A : CAF : Dotation globale de fonctionnement
    * @param comptes Comptes associés.
    */
   public void setProduitsDeFonctionnementCAFDGF(RS comptes) {
      this.produitsDeFonctionnementCAFDGF = comptes;
   }

   /**
    * Renvoyer les Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises. 
    * @return Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises.
    */
   public MR getProduitsImpotsRepartitionCotisationValeurAjouteeEntreprises() {
      return this.produitsImpotsRepartitionCotisationValeurAjouteeEntreprises;
   }

   /**
    * Fixer les Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises. 
    * @param comptes Produits des impôts de répartition : Cotisation sur la valeur ajoutée des entreprises.
    */
   public void setProduitsImpotsRepartitionCotisationValeurAjouteeEntreprises(MR comptes) {
      this.produitsImpotsRepartitionCotisationValeurAjouteeEntreprises = comptes;
   }

   /**
    * Renvoyer les Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau. 
    * @return Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau.
    */
   public MR getProduitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau() {
      return this.produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau;
   }

   /**
    * Fixer les Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau. 
    * @param comptes Produits des impôts de répartition : Imposition forfaitaire sur les entreprises de réseau.
    */
   public void setProduitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau(MR comptes) {
      this.produitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau = comptes;
   }

   /**
    * Renvoyer les Produits des impôts de répartition : Taxe sur les surfaces commerciales. 
    * @return Produits des impôts de répartition : Taxe sur les surfaces commerciales.
    */
   public MR getProduitsImpotsRepartitionTaxeSurfacesCommerciales() {
      return this.produitsImpotsRepartitionTaxeSurfacesCommerciales;
   }

   /**
    * Fixer les Produits des impôts de répartition : Taxe sur les surfaces commerciales. 
    * @param comptes Produits des impôts de répartition : Taxe sur les surfaces commerciales.
    */
   public void setProduitsImpotsRepartitionTaxeSurfacesCommerciales(MR comptes) {
      this.produitsImpotsRepartitionTaxeSurfacesCommerciales = comptes;
   }

   /**
    * Renvoyer le total des subventions reçues (ressources d'investissement). 
    * @return Total des subventions reçues.
    */
   public RS getRessourcesInvestissementSubventionsRecues() {
      return this.ressourcesInvestissementSubventionsRecues;
   }

   /**
    * Fixer le total des subventions reçues (ressources d'investissement). 
    * @param comptes Total des subventions reçues.
    */
   public void setRessourcesInvestissementSubventionsRecues(RS comptes) {
      this.ressourcesInvestissementSubventionsRecues = comptes;
   }

   /**
    * Renvoyer le Fonds de compensation de la taxe sur la valeur ajoutée (FCTVA)
    * @return Fonds de compensation de la taxe sur la valeur ajoutée (FCTVA)
    */
   public RS getRessourcesInvestissementFCTVA() {
      return this.ressourcesInvestissementFCTVA;
   }

   /**
    * Fixer le Fonds de compensation de la taxe sur la valeur ajoutée (FCTVA, investissement)
    * @param comptes Fonds de compensation de la taxe sur la valeur ajoutée (FCTVA)
    */
   public void setRessourcesInvestissementFCTVA(RS comptes) {
      this.ressourcesInvestissementFCTVA = comptes;
   }

   /**
    * Renvoyer le RÉSULTAT COMPTABLE total.
    * @return Résultat Comptable.
    */
   public MR getResultatComptable() {
      return this.resultatComptable;
   }

   /**
    * Fixer le RÉSULTAT COMPTABLE total.
    * @param resultatComptable Résultat Comptable.
    */
   public void setResultatComptable(MR resultatComptable) {
      this.resultatComptable = resultatComptable;
   }

   /**
    * Renvoyer le siren de la collectivité locale.
    * @return Siren de la collectivité locale.
    */
   public String getSiren() {
      return this.siren;
   }

   /**
    * Fixer le siren de la collectivité locale.
    * @param siren Siren de la collectivité locale.
    */
   public void setSiren(String siren) {
      this.siren = siren;
   }
   
   /**
    * Formatter une ligne texte pour qu'elle se place dans un tableau textuel d'analyse des équilibres financiers fondamentaux.
    * @param libelle Libellé à placer dans la ligne.
    * @return Ligne formatée. 
    */
   protected String toStringAnalyseEquilibresFinanciersFondamentaux(String libelle) {
      Object[] valeurs = new Object[] {
         libelle
      };

      return String.format("%s", valeurs);
   }
   
   /**
    * Formatter une ligne texte pour qu'elle se place dans un tableau textuel d'analyse des équilibres financiers fondamentaux.
    * @param format Format.
    * @param valeurs Valeurs.
    * @return Ligne formatée. 
    */
   protected String toStringAnalyseEquilibresFinanciersFondamentaux(String format, Object... valeurs) {
      return String.format(format, valeurs);
   }
}
