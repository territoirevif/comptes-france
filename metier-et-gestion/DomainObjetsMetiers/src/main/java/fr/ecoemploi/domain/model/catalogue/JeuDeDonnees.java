package fr.ecoemploi.domain.model.catalogue;

import fr.ecoemploi.domain.utils.objets.ObjetMetierIdentifiable;

import java.io.Serial;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;

/**
 * Jeu de données issu du catalogue datagouv.fr (Dataset)
 * @author Marc Le Bihan
 */
public class JeuDeDonnees extends ObjetMetierIdentifiable<CatalogueId> {
   @Serial
   private static final long serialVersionUID = -4770990934447430472L;

   /** Ressources (simplifiées) associées à ce jeu de données. */
   private Ressources ressources;

   /** Titre de ce jeu de données */
   private String titre;

   /** Accroche de publication */
   private String slug;

   /** Acronyme */
   private String acronyme;

   /** URL du jeu de données. */
   private String url;

   /** Identifiant de l'organisation émétrice. */
   private OrganisationId organisationId;

   /** Nom de l'organisation. */
   private String organisation;

   /** Description du jeu de données. */
   private String description;

   /** Fréquence. */
   private String frequence;

   /** Licence. */
   private String licence;

   /** Date de début. */
   private LocalDate dateDebut;

   /** Date de fin. */
   private LocalDate dateFin;

   /** Granularité spatiale. */
   private String granulariteSpatiale;

   /** Zones spatiales. */
   private String zonesSpatiales;

   /** Privé. */
   private Boolean prive;

   /** Featured = ? */
   private Boolean featured;

   /** Date de création. */
   private LocalDate dateCreation;

   /** Date de modification. */
   private LocalDate dateModification;

   /** Tags */
   private Set<String> tags;

   /** Archivé */
   private Boolean archive;

   /** Nombre de moissonneurs. */
   private Integer nombreMoissonneurs;

   /** Domaine du moissonneur. */
   private String moissonneurDomaine;

   /** Date de création du moissonneur. */
   private LocalDate moissonneurDateCreation;

   /** Date de modification du moissonneur. */
   private LocalDate moissonneurDateModification;

   /** Score de qualité. */
   private Double scoreQualite;

   /** Nombre de ressources. */
   private Integer nombreDeRessources;

   /** Métrique des discussions. */
   private Integer metriqueDiscussions;

   /** Métrique des réutilisations. */
   private Integer metriqueReutilisations;

   /** Métriques des suiveurs de ce dataset. */
   private Integer metriqueSuiveurs;

   /** Métrique nombre de vues. */
   private Integer metriqueVues;

   /**
    * Construire un jeu de données.
    */
   public JeuDeDonnees() {
   }

   /**
    * Construire un jeu de données.
    * @param catalogueId Identifiant du catalogue
    * @param titre Titre de ce jeu de données.
    * @param slug Accroche de publication
    * @param acronyme Acronyme
    * @param url URL du jeu de données.
    * @param organisationId Identifiant de l'organisation émétrice.
    * @param organisation Nom de l'organisation.
    * @param description Description du jeu de données.
    * @param frequence Fréquence.
    * @param licence Licence.
    * @param dateDebut Date de début.
    * @param dateFin Date de fin.
    * @param granulariteSpatiale Granularité spatiale.
    * @param zonesSpatiales Zones spatiales.
    * @param featured Featured = ?
    * @param dateCreation Date de création.
    * @param dateModification Date de modification.
    * @param tags Tags
    * @param archive Archivé
    * @param nombreDeRessources Nombre de ressources.
    * @param nombreMoissonneurs Nombre de moissonneurs.
    * @param moissonneurDomaine Domaine du moissonneur.
    * @param moissonneurDateCreation Date de création du moissonneur.
    * @param moissonneurDateModification Date de modification du moissonneur.
    * @param scoreQualite Score de qualité.
    * @param metriqueDiscussions Métrique des discussions.
    * @param metriqueReutilisation Métrique des réutilisations.
    * @param metriqueSuiveurs Métriques des suiveurs de ce dataset.
    * @param metriqueVues Métrique nombre de vues.
    */
   public JeuDeDonnees(CatalogueId catalogueId, String titre, String slug, String acronyme, String url,
      OrganisationId organisationId, String organisation, String description, String frequence, String licence,
      LocalDate dateDebut, LocalDate dateFin, String granulariteSpatiale, String zonesSpatiales,
      Boolean featured, LocalDate dateCreation, LocalDate dateModification, Set<String> tags, Boolean archive,
      Integer nombreMoissonneurs, String moissonneurDomaine, LocalDate moissonneurDateCreation, LocalDate moissonneurDateModification, Double scoreQualite,
      Integer nombreDeRessources, Integer metriqueDiscussions, Integer metriqueReutilisation, Integer metriqueSuiveurs, Integer metriqueVues) {
      super.setId(catalogueId);
      this.titre = titre;
      this.slug = slug;
      this.acronyme = acronyme;
      this.url = url;

      this.organisationId = organisationId;
      this.organisation = organisation;
      this.description = description;
      this.frequence = frequence;
      this.licence = licence;

      this.dateDebut = dateDebut;
      this.dateFin = dateFin;
      this.granulariteSpatiale = granulariteSpatiale;
      this.zonesSpatiales = zonesSpatiales;

      this.featured = featured;
      this.dateCreation = dateCreation;
      this.dateModification = dateModification;
      this.tags = tags;
      this.archive = archive;

      this.nombreMoissonneurs = nombreMoissonneurs;
      this.moissonneurDomaine = moissonneurDomaine;
      this.moissonneurDateCreation = moissonneurDateCreation;
      this.moissonneurDateModification = moissonneurDateModification;
      this.scoreQualite = scoreQualite;

      this.nombreDeRessources = nombreDeRessources;
      this.metriqueDiscussions = metriqueDiscussions;
      this.metriqueReutilisations = metriqueReutilisation;
      this.metriqueSuiveurs = metriqueSuiveurs;
      this.metriqueVues = metriqueVues;
   }

   /**
    * Renvoyer les ressources associées à ce jeu de données.
    * @return Ressources (simplifiées).
    */
   public Ressources ressources() {
      if (this.ressources == null) {
         this.ressources = new Ressources();
      }

      return this.ressources;
   }

   /**
    * Renvoyer les ressources associées à ce jeu de données.
    * @return Ressources (simplifiées).
    */
   public Map<RessourceJeuDeDonneesId, Ressource> getRessources() {
      return ressources();
   }

   /**
    * Renvoyer le titre du jeu de données.
    * @return titre du jeu de données.
    */
   public String getTitre() {
      return this.titre;
   }

   /**
    * Fixer le titre du jeu de données.
    * @param titre du jeu de données.
    */
   public void setTitre(String titre) {
      this.titre = titre;
   }

   /**
    * Renvoyer l'accroche du jeu de données.
    * @return Accroche
    */
   public String getSlug() {
      return this.slug;
   }

   /**
    * Fixer l'accroche du jeu de données.
    * @param slug Accroche.
    */
   public void setSlug(String slug) {
      this.slug = slug;
   }

   /**
    * Renvoyer l'accronyme.
    * @return Accronyme.
    */
   public String getAcronyme() {
      return this.acronyme;
   }

   /**
    * Fixer l'accronyme.
    * @param acronyme Accronyme.
    */
   public void setAcronyme(String acronyme) {
      this.acronyme = acronyme;
   }

   /**
    * Renvoyer l'URL du jeu de données.
    * @return URL.
    */
   public String getUrl() {
      return this.url;
   }

   /**
    * Fixer l'URL du jeu de données.
    * @param url URL.
    */
   public void setUrl(String url) {
      this.url = url;
   }

   /**
    * Renvoyer l'identifiant de l'organisation émétrice.
    * @return Identifiant de l'organisation
    */
   public OrganisationId getOrganisationId() {
      return this.organisationId;
   }

   /**
    * Fixer l'identifiant de l'organisation émétrice.
    * @param organisationId Identifiant de l'organisation
    */
   public void setOrganisationId(OrganisationId organisationId) {
      this.organisationId = organisationId;
   }

   /**
    * Renvoyer le nom de l'organisation émétrice.
    * @return Nom de l'organisation.
    */
   public String getOrganisation() {
      return this.organisation;
   }

   /**
    * Fixer le nom de l'organisation émétrice.
    * @param organisation Nom de l'organisation.
    */
   public void setOrganisation(String organisation) {
      this.organisation = organisation;
   }

   /**
    * Renvoyer la description du jeu de données.
    * @return Description du jeu de données.
    */
   public String getDescription() {
      return this.description;
   }

   /**
    * Fixer la description du jeu de données.
    * @param description Description du jeu de données.
    */
   public void setDescription(String description) {
      this.description = description;
   }

   /**
    * Renvoyer la fréquence de parution.
    * @return Fréquence : unknown, annual, punctual, irregular, monthly, daily, weekly, continuous, quarterly, semiannual
    * bimonthly, biweekly, threeTimesAYear, biennial, quinquennial, triennial, hourly, False, fourTimesAWeek
    * semidaily, semimonthly, semiweekly, threeTimesADay, fourTimesADay, threeTimesAWeek, threeTimesAMonth
    */
   public String getFrequence() {
      return this.frequence;
   }

   /**
    * Fixer la fréquence de parution.
    * @param frequence Fréquence de parution.
    */
   public void setFrequence(String frequence) {
      this.frequence = frequence;
   }

   /**
    * Renvoyer le type de licence.
    * @return Licence : Licence Ouverte / Open Licence version 2.0, License Not Specified, Licence Ouverte / Open Licence, Open Data Commons Open Database License (ODbL),
    * Other (Attribution), Other (Open), Other (Public Domain), Creative Commons Attribution, Creative Commons Attribution Share-Alike, Open Data Commons Attribution License,
    * Creative Commons CCZero, Open Data Commons Public Domain Dedication and Licence (PDDL)
    */
   public String getLicence() {
      return this.licence;
   }

   /**
    * Fixer le type de licence.
    * @param licence Licence.
    */
   public void setLicence(String licence) {
      this.licence = licence;
   }

   /**
    * Renvoyer la date de début.
    * @return Date de début.
    */
   public LocalDate getDateDebut() {
      return this.dateDebut;
   }

   /**
    * Fixer la date de début.
    * @param dateDebut Date de début.
    */
   public void setDateDebut(LocalDate dateDebut) {
      this.dateDebut = dateDebut;
   }

   /**
    * Renvoyer la date de fin.
    * @return Date de fin.
    */
   public LocalDate getDateFin() {
      return this.dateFin;
   }

   /**
    * Fixer la date de fin.
    * @param dateFin Date de fin.
    */
   public void setDateFin(LocalDate dateFin) {
      this.dateFin = dateFin;
   }

   /**
    * Renvoyer la granularité spatiale.
    * @return Granularité spatiale : other, fr:commune, country, fr:epci, fr:departement, poi, fr:region,
    * fr:canton, fr:collectivite, country-group, fr:iris, country-subset, False, fr:arrondissement
    */
   public String getGranulariteSpatiale() {
      return this.granulariteSpatiale;
   }

   /**
    * Fixer la granularité spatiale.
    * @param granulariteSpatiale Granularité spatiale.
    */
   public void setGranulariteSpatiale(String granulariteSpatiale) {
      this.granulariteSpatiale = granulariteSpatiale;
   }

   /**
    * Renvoyer les zones spatiales
    * @return Zones spatiales.
    */
   public String getZonesSpatiales() {
      return this.zonesSpatiales;
   }

   /**
    * Fixer les zones spatiales.
    * @param zonesSpatiales Zones spatiales.
    */
   public void setZonesSpatiales(String zonesSpatiales) {
      this.zonesSpatiales = zonesSpatiales;
   }

   /**
    * Déterminer si le jeu de données est privé.
    * @return true, s'il l'est.
    */
   public Boolean isPrive() {
      return this.prive;
   }

   /**
    * Indiquer si le jeu de données est privé.
    * @param prive true s'il l'est.
    */
   public void setPrive(Boolean prive) {
      this.prive = prive;
   }

   public Boolean isFeatured() {
      return this.featured;
   }

   public void setFeatured(Boolean featured) {
      this.featured = featured;
   }

   /**
    * Renvoyer la date de création du jeu de données.
    * @return Date de création.
    */
   public LocalDate getDateCreation() {
      return this.dateCreation;
   }

   /**
    * Fixer la date de création du jeu de données.
    * @param dateCreation Date de création.
    */
   public void setDateCreation(LocalDate dateCreation) {
      this.dateCreation = dateCreation;
   }

   /**
    * Renvoyer la date de modification du jeu de données.
    * @return Date de modification.
    */
   public LocalDate getDateModification() {
      return this.dateModification;
   }

   /**
    * Fixer la date de modification du jeu de données.
    * @param dateModification Date de création.
    */
   public void setDateModification(LocalDate dateModification) {
      this.dateModification = dateModification;
   }

   /**
    * Renvoyer les tags associés au jeu de données.
    * @return tags.
    */
   public Set<String> tags() {
      return this.tags;
   }

   /**
    * Fixer les tags associés au jeu de données.
    * @param tags tags.
    */
   public void tags(Set<String> tags) {
      this.tags = tags;
   }

   /**
    * Déterminer si ce jeu de données est archivé.
    * @return true, s'il l'est.
    */
   public Boolean isArchive() {
      return this.archive;
   }

   /**
    * Indiquer si ce jeu de données est archivé.
    * @param archive true, s'il l'est.
    */
   public void setArchive(Boolean archive) {
      this.archive = archive;
   }

   /**
    * Renvoyer le nombre de ressources qui composent ce jeu de données.
    * @return Nombre de ressources.
    */
   public Integer getNombreDeRessources() {
      return this.nombreDeRessources;
   }

   /**
    * Fixer le nombre de ressources qui composent ce jeu de données.
    * @param nombreDeRessources Nombre de ressources.
    */
   public void setNombreDeRessources(Integer nombreDeRessources) {
      this.nombreDeRessources = nombreDeRessources;
   }

   /**
    * Renvoyer la métrique du nombre de discussions associées à ce jeu de données.
    * @return Nombre de discussions.
    */
   public Integer getMetriqueDiscussions() {
      return this.metriqueDiscussions;
   }

   /**
    * Fixer la métrique du nombre de discussions associées à ce jeu de données.
    * @param metriqueDiscussions Nombre de discussions.
    */
   public void setMetriqueDiscussions(Integer metriqueDiscussions) {
      this.metriqueDiscussions = metriqueDiscussions;
   }

   /**
    * Renvoyer la métrique du nombre de réutilisations de ce jeu de données.
    * @return Nombre de réutilisations.
    */
   public Integer getMetriqueReutilisations() {
      return this.metriqueReutilisations;
   }

   /**
    * Fixer la métrique du nombre de réutilisations de ce jeu de données.
    * @param metriqueReutilisations Nombre de réutilisations.
    */
   public void setMetriqueReutilisations(Integer metriqueReutilisations) {
      this.metriqueReutilisations = metriqueReutilisations;
   }

   /**
    * Renvoyer la métrique du nombre de followers de ce jeu de données.
    * @return Nombre de followers.
    */
   public Integer getMetriqueSuiveurs() {
      return this.metriqueSuiveurs;
   }

   /**
    * Fixer la métrique du nombre de followers de ce jeu de données.
    * @param metriqueSuiveurs Nombre de followers.
    */
   public void setMetriqueSuiveurs(Integer metriqueSuiveurs) {
      this.metriqueSuiveurs = metriqueSuiveurs;
   }

   /**
    * Renvoyer la métrique du nombre de vues de ce jeu de données.
    * @return Nombre de vues.
    */
   public Integer getMetriqueVues() {
      return this.metriqueVues;
   }

   /**
    * Fixer la métrique du nombre de vues de ce jeu de données.
    * @param metriqueVues Nombre de vues.
    */
   public void setMetriqueVues(Integer metriqueVues) {
      this.metriqueVues = metriqueVues;
   }

   /**
    * Renvoyer le nombre de moissonneurs associés à ce jeu de données.
    * @return Nombre de moissonneurs.
    */
   public Integer getNombreMoissonneurs() {
      return this.nombreMoissonneurs;
   }

   /**
    * Fixer le nombre de moissonneurs associés à ce jeu de données.
    * @param nombreMoissonneurs Nombre de moissonneurs.
    */
   public void setNombreMoissonneurs(Integer nombreMoissonneurs) {
      this.nombreMoissonneurs = nombreMoissonneurs;
   }

   /**
    * Renvoyer l'url du moissonneur.
    * @return URL du moissonneur.
    */
   public String getMoissonneurDomaine() {
      return this.moissonneurDomaine;
   }

   /**
    * Fixer l'url du moissonneur.
    * @param moissonneurDomaine URL du moissonneur.
    */
   public void setMoissonneurDomaine(String moissonneurDomaine) {
      this.moissonneurDomaine = moissonneurDomaine;
   }

   /**
    * Renvoyer la date de création du moissonneur.
    * @return Date de création.
    */
   public LocalDate getMoissonneurDateCreation() {
      return this.moissonneurDateCreation;
   }

   /**
    * Fixer la date de création du moissonneur.
    * @param  moissonneurDateCreation Date de création.
    */
   public void setMoissonneurDateCreation(LocalDate moissonneurDateCreation) {
      this.moissonneurDateCreation = moissonneurDateCreation;
   }

   /**
    * Renvoyer la date de modification du moissonneur.
    * @return Date de modification.
    */
   public LocalDate getMoissonneurDateModification() {
      return this.moissonneurDateModification;
   }

   /**
    * Fixer la date de modification du moissonneur.
    * @param moissonneurDateModification Date de modification.
    */
   public void setMoissonneurDateModification(LocalDate moissonneurDateModification) {
      this.moissonneurDateModification = moissonneurDateModification;
   }

   /**
    * Renvoyer le score de qualité du jeu de données.
    * @return Score de qualité.
    */
   public Double getScoreQualite() {
      return this.scoreQualite;
   }

   /**
    * Fixer le score de qualité du jeu de données.
    * @param scoreQualite Score de qualité.
    */
   public void setScoreQualite(Double scoreQualite) {
      this.scoreQualite = scoreQualite;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      String format = "id : {0}, titre : {1}, description : {2}, organisation : {3} (id : {4}), " +
         "granularité spatiale : {5}, zones spatiales : {6}, tags : {7}, url : {8}, accronyme : {9}, " +
         "slug : {10}, date de création : {11}, date de modification : {12}, fréquence : {13}, licence : {14}, " +
         "date de début : {15}, date de fin : {16}, archivé : {17}, score de qualité : {18}, " +
         "featured : {19}, nombre de moissonneurs : {20}, domaine du moissonneur : {21}, date de création du moissonneur : {22}, date de modification du moissonneur : {23}, " +
         "métrique nombre de discussions : {24}, métrique nombre de réutilisation : {25}, métrique nombre de suiveurs : {26}, métrique nombre de vues : {27}, " +
         "nombre de ressources déclarées/portées par cet objet : {28}/{29}";

      Object[] args = {
         super.getId(), this.titre, this.description, this.organisation, this.organisationId,
         this.granulariteSpatiale, this.zonesSpatiales, this.tags, this.url, this.acronyme,
         this.slug, this.dateCreation, this.dateModification, this.frequence, this.licence,
         this.dateDebut, this.dateFin, this.archive, this.scoreQualite,
         this.featured, this.nombreMoissonneurs, this.moissonneurDomaine, this.moissonneurDateCreation, this.moissonneurDateModification,
         this.metriqueDiscussions, this.metriqueReutilisations, this.metriqueSuiveurs, this.metriqueVues,
         this.nombreDeRessources, this.ressources != null ? this.ressources.size() : null
      };

      return MessageFormat.format(format, args);
   }
}

/* Le Row auquel il correspond
|id_catalogue            |titre_catalogue                                                                                                         |slug_catalogue                                                                                                       |acronyme_catalogue|url_catalogue                                                                                                                                             |organisation_catalogue                    |id_organisation_catalogue|description_catalogue                                                                                                                        |frequence_catalogue|licence_catalogue              |date_debut_couverture_catalogue|date_fin_couverture_catalogue|granularite_spatiale|zones_spatiales |prive|featured|date_creation|date_modification|tags |archive|nombre_de_ressources|metrique_discussions|metrique_reutilisation|metrique_suiveurs|metrique_vues|
|53698e08a3a729239d20336f|2013-01 - Tableau synthétique de l’évolution de la demande et de la délivrance pour les principales catégories de visas |2013-01-tableau-synthetique-de-levolution-de-la-demande-et-de-la-delivrance-pour-les-principales-categories-de-visas |null              |http://www.data.gouv.fr/fr/datasets/2013-01-tableau-synthetique-de-levolution-de-la-demande-et-de-la-delivrance-pour-les-principales-categories-de-visas/ |Ministère de l'Intérieur et des Outre-Mer |534fff91a3a7292c64a77f53 |Politique de délivrance des visas : Évolution de la demande et de la délivrance de visas (Les orientations de la politique de l’immigration) |annual             |Licence Ouverte / Open Licence |2006-01-01                     |2010-12-31                   |other               |France          |false|false   |2013-07-08   |2015-06-12       |null |false  |2                   |null                |null                  |null             |null         |
|53698e09a3a729239d203370|01 - Tableau synthétique de l’évolution de la demande et de la délivrance pour les principales catégories de visas      |01-tableau-synthetique-de-l-evolution-de-la-demande-et-de-la-delivrance-pour-les-principales--572840                 |null              |http://www.data.gouv.fr/fr/datasets/01-tableau-synthetique-de-l-evolution-de-la-demande-et-de-la-delivrance-pour-les-principales-572840/                  |Ministère de l'Intérieur et des Outre-Mer |534fff91a3a7292c64a77f53 |Politique de délivrance des visas : Évolution de la demande et de la délivrance de visas (Les orientations de la politique de l’immigration) |annual             |Licence Ouverte / Open Licence |2006-01-01                     |2011-12-31                   |other               |null            |false|false   |2013-08-16   |2015-07-27       |null |false  |1                   |null                |null                  |null             |null         |
|53698e09a3a729239d203371|2013-02 - Évolution de la délivrance des visas de court séjour                                                          |2013-02-evolution-de-la-delivrance-des-visas-de-court-sejour                                                         |null              |http://www.data.gouv.fr/fr/datasets/2013-02-evolution-de-la-delivrance-des-visas-de-court-sejour/                                                         |Ministère de l'Intérieur et des Outre-Mer |534fff91a3a7292c64a77f53 |Politique de délivrance des visas : Évolution de la demande et de la délivrance de visas (Les orientations de la politique de l’immigration) |annual             |Licence Ouverte / Open Licence |2006-01-01                     |2011-12-31                   |other               |France          |false|false   |2013-07-08   |2015-07-20       |null |false  |1                   |null                |null                  |null             |null         |
|53698e0aa3a729239d203372|2013-03 - Visas délivrés aux étudiants                                                                                  |2013-03-visas-delivres-aux-etudiants                                                                                 |null              |http://www.data.gouv.fr/fr/datasets/2013-03-visas-delivres-aux-etudiants/                                                                                 |Ministère de l'Intérieur et des Outre-Mer |534fff91a3a7292c64a77f53 |Politique de délivrance des visas : Évolution de la demande et de la délivrance de visas (Les orientations de la politique de l’immigration) |annual             |Licence Ouverte / Open Licence |2006-01-01                     |2011-12-31                   |other               |null            |false|false   |2013-07-08   |2014-11-05       |null |false  |1                   |null                |null                  |null             |null         |
|53698e0aa3a729239d203373|2013-04 - Visas délivrés aux conjoints de Français                                                                      |2013-04-visas-delivres-aux-conjoints-de-francais                                                                     |null              |http://www.data.gouv.fr/fr/datasets/2013-04-visas-delivres-aux-conjoints-de-francais/                                                                     |Ministère de l'Intérieur et des Outre-Mer |534fff91a3a7292c64a77f53 |Politique de délivrance des visas : Évolution de la demande et de la délivrance de visas (Les orientations de la politique de l’immigration) |annual             |Licence Ouverte / Open Licence |2006-01-01                     |2010-12-31                   |other               |null            |false|false   |2013-07-08   |2015-03-28       |null |false  |1                   |null                |null                  |null             |null         |
 */
