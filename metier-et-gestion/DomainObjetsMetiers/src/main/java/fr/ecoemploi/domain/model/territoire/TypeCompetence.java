package fr.ecoemploi.domain.model.territoire;

/**
 * Type de compétence d'une intercommunalité.
 * @author Marc LE BIHAN
 */
public enum TypeCompetence {
   /** Obligatoire. */
   OBLIGATOIRE,
   
   /** Optionnelle (n parmi m). */
   OPTIONNELLE,
   
   /** Facultative. */
   FACULTATIVE
}
