package fr.ecoemploi.domain.model.territoire.entreprise;

import java.io.Serial;
import java.util.*;

/**
 * Un ensemble d'activités exercées dans une commune. 
 * @author Marc LE BIHAN
 */
public class ActivitesCommunes extends ArrayList<ActiviteCommune> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 1800678304209033245L;
   
   /**
    * Renvoyer le nombre d'entreprises total présent dans ces activités de commune.
    * @return Nombre d'entreprises.
    */
   public int getNombreEntreprises() {
      return stream().mapToInt(ActiviteCommune::getNombreEntreprises).sum();
   }
   
   /**
    * Renvoyer le nombre de salariés total présent dans ces activités de commune.
    * @return Nombre de salariés.
    */
   public double getNombreSalaries() {
      return stream().mapToDouble(ActiviteCommune::getNombreSalaries).sum();
   }
   
   /**
    * Trier les activités par nombre de salariés décroissant.
    */
   public void trierParNombreDeSalaries() {
      this.sort((a, b) -> - Double.compare(a.getNombreSalaries(), b.getNombreSalaries()));
   }

   /**
    * Trier les activités par nombre d'entreprises décroissant.
    */
   public void trierParNombreEntreprises() {
      this.sort((a, b) -> - Integer.compare(a.getNombreEntreprises(), b.getNombreEntreprises()));
   }
}
