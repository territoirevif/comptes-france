package fr.ecoemploi.domain.model.territoire.entreprise;

/**
 * Catégorie de l'entreprise.
 * @author Marc LE BIHAN
 */
public enum CategorieEntreprise {
   /** Petite ou Moyenne Entreprise */
   PME("Petite ou Moyenne Entreprise"),

   /** Entreprise de Taille Intermédiaire */
   ETI("Entreprise de Taille Intermédiaire"),

   /** Grande Entreprise */
   GE("Grande Entreprise");

   /** Libellé. */
   private final String libelle;

   /**
    * Construire une catégorie d'entreprise.
    */
   CategorieEntreprise(String libelle) {
      this.libelle = libelle;
   }
   
   /**
    * Renvoyer le libellé associé à cette catégorie d'entreprise.
    * @return Libellé.
    */
   public String getLibelle() {
      return this.libelle;
   }
   
   /**
    * @see java.lang.Enum#toString()
    */
   @Override
   public String toString() {
      return this.libelle;
   }
}
