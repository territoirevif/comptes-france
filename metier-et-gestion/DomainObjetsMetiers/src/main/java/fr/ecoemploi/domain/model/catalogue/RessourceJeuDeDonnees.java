package fr.ecoemploi.domain.model.catalogue;

import java.io.Serial;
import java.text.MessageFormat;
import java.time.LocalDate;

/**
 * Une ressource d'un jeu de données, avec le rappel de ses données parentes.
 * @author Marc Le Bihan
 */
public class RessourceJeuDeDonnees extends Ressource {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -7273776644185896314L;

   /** Titre de ce jeu de données */
   private String titreCatalogue;

   /** Accroche de publication */
   private String slugCatalogue;

   /** URL du jeu de données. */
   private String urlCatalogue;

   /** Nom de l'organisation. */
   private String organisationCatalogue;

   /** Identifiant de l'organisation émétrice. */
   private OrganisationId organisationCatalogueId;

   /** Licence. */
   private String licenceCatalogue;

   /** Privé. */
   private Boolean isCataloguePrive;

   /** Archivé */
   private Boolean isCatalogueArchive;

   /**
    * Construire un jeu de données.
    */
   public RessourceJeuDeDonnees() {
   }

   /**
    * Construire une ressource de jeu de données.
    * @param catalogueId Identifiant du jeu de données du catalogue
    * @param titreCatalogue Titre du jeu de données de cette ressource.
    * @param slugCatalogue Accroche de publication du catalogue auquel appartient cette ressource.
    * @param urlCatalogue URL du jeu de données de cette ressource.
    * @param organisationCatalogueId Identifiant de l'organisation émétrice qui produit ce jeu de données.
    * @param organisationCatalogue Nom de l'organisation qui produit ce jeu de données.
    * @param licenceCatalogue Licence du jeu de données.
    * @param isCataloguePrive Catalogue privé.
    * @param isCatalogueArchive Catalogue archivé.
    * @param ressourceJeuDeDonneesId Identifiant de la ressource du jeu de données.
    * @param url URL de la ressource.
    * @param titre Titre de la ressource.
    * @param description Description de la ressource.
    * @param typeDeFichier Type de fichier.
    * @param format Format du fichier.
    * @param mime Type MIME du fichier.
    * @param tailleDuFichier Taille du fichier.
    * @param typeDeChecksum Type de checksum.
    * @param valeurChecksum Valeur de checksum.
    * @param dateCreation Date de création de la ressource.
    * @param dateModification date de modification de la ressource.
    * @param nombreDeTelechargements Nombre de téléchargements de la ressource.
    * @param moissonneurDateCreation Date de création du moissonneur.
    * @param moissonneurDateModification Date de modification du moissonneur.
    */
   public RessourceJeuDeDonnees(CatalogueId catalogueId, String titreCatalogue, String slugCatalogue, String urlCatalogue, OrganisationId organisationCatalogueId,
      String organisationCatalogue, String licenceCatalogue, Boolean isCataloguePrive, Boolean isCatalogueArchive, RessourceJeuDeDonneesId ressourceJeuDeDonneesId,
      String url, String titre, String description, String typeDeFichier, String format,
      String mime, Integer tailleDuFichier, String typeDeChecksum, String valeurChecksum, LocalDate dateCreation,
      LocalDate dateModification, Integer nombreDeTelechargements, LocalDate moissonneurDateCreation, LocalDate moissonneurDateModification) {
      super(catalogueId, ressourceJeuDeDonneesId, url, titre, description, typeDeFichier, format,
         mime, tailleDuFichier, typeDeChecksum, valeurChecksum, dateCreation,
         dateModification, nombreDeTelechargements, moissonneurDateCreation, moissonneurDateModification);
      this.titreCatalogue = titreCatalogue;
      this.slugCatalogue = slugCatalogue;
      this.urlCatalogue = urlCatalogue;
      this.organisationCatalogueId = organisationCatalogueId;

      this.organisationCatalogue = organisationCatalogue;
      this.licenceCatalogue = licenceCatalogue;
      this.isCataloguePrive = isCataloguePrive;
      this.isCatalogueArchive = isCatalogueArchive;
   }

   /**
    * Renvoyer le titre du jeu de données dont cette ressource fait partie.
    * @return Titre du jeu de données.
    */
   public String getTitreCatalogue() {
      return this.titreCatalogue;
   }

   /**
    * Fixer le titre du jeu de données dont cette ressource fait partie.
    * @param titreCatalogue Titre du jeu de données.
    */
   public void setTitreCatalogue(String titreCatalogue) {
      this.titreCatalogue = titreCatalogue;
   }

   /**
    * Renvoyer le slug du jeu de données dont cette ressource fait partie.
    * @return Slug du jeu de données.
    */
   public String getSlugCatalogue() {
      return this.slugCatalogue;
   }

   /**
    * Fixer le slug du jeu de données dont cette ressource fait partie.
    * @param slugCatalogue Slug du jeu de données.
    */
   public void setSlugCatalogue(String slugCatalogue) {
      this.slugCatalogue = slugCatalogue;
   }

   /**
    * Renvoyer l'URL du jeu de données dont cette ressource fait partie.
    * @return URL du jeu de données.
    */
   public String getUrlCatalogue() {
      return this.urlCatalogue;
   }

   /**
    * Renvoyer le type de licence.
    * @return Licence : Licence Ouverte / Open Licence version 2.0, License Not Specified, Licence Ouverte / Open Licence, Open Data Commons Open Database License (ODbL),
    * Other (Attribution), Other (Open), Other (Public Domain), Creative Commons Attribution, Creative Commons Attribution Share-Alike, Open Data Commons Attribution License,
    * Creative Commons CCZero, Open Data Commons Public Domain Dedication and Licence (PDDL)
    */
   public String getLicenceCatalogue() {
      return this.licenceCatalogue;
   }

   /**
    * Fixer le type de licence.
    * @param licenceCatalogue Licence.
    */
   public void setLicenceCatalogue(String licenceCatalogue) {
      this.licenceCatalogue = licenceCatalogue;
   }

   /**
    * Fixer l'URL du jeu de données dont cette ressource fait partie.
    * @param urlCatalogue URL du jeu de données.
    */
   public void setUrlCatalogue(String urlCatalogue) {
      this.urlCatalogue = urlCatalogue;
   }

   /**
    * Renvoyer l'identifiant de l'organisation du jeu de données dont la ressource fait partie.
    * @return Identifiant de l'organisation.
    */
   public OrganisationId getOrganisationCatalogueId() {
      return this.organisationCatalogueId;
   }

   /**
    * Fixer l'identifiant de l'organisation du jeu de données dont la ressource fait partie.
    * @param organisationCatalogueId Identifiant de l'organisation.
    */
   public void setOrganisationCatalogueId(OrganisationId organisationCatalogueId) {
      this.organisationCatalogueId = organisationCatalogueId;
   }

   /**
    * Renvoyer le nom de l'organisation du jeu de données dont la ressource fait partie.
    * @return Nom de l'organisation.
    */
   public String getOrganisationCatalogue() {
      return organisationCatalogue;
   }

   /**
    * Fixer le nom de l'organisation du jeu de données dont la ressource fait partie.
    * @param organisationCatalogue Nom de l'organisation.
    */
   public void setOrganisationCatalogue(String organisationCatalogue) {
      this.organisationCatalogue = organisationCatalogue;
   }

   /**
    * Déterminer si le catalogue est privé.
    * @return true s'il est privé.
    */
   public Boolean isCataloguePrive() {
      return this.isCataloguePrive;
   }

   /**
    * Indiquer si le catalogue est privé.
    * @param isCataloguePrive true s'il est privé.
    */
   public void setIsCataloguePrive(Boolean isCataloguePrive) {
      this.isCataloguePrive = isCataloguePrive;
   }

   /**
    * Déterminer si ce catalogue est archivé.
    * @return true s'il est archivé.
    */
   public Boolean isCatalogueArchive() {
      return this.isCatalogueArchive;
   }

   /**
    * Indiquer si ce catalogue est archivé.
    * @param isCatalogueArchive true s'il est archivé.
    */
   public void setIsCatalogueArchive(Boolean isCatalogueArchive) {
      this.isCatalogueArchive = isCatalogueArchive;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      String fmt = "{0}, titre du jeu de données parent : {1}, slug du parent : {2}, url du parent : {3}, organisation émétrice {4} " +
         "(id : {5}), licence du jeu de données : {6}, jeu de données privé : {7}, archivé : {8}";

      Object[] args = {
         super.toString(), this.titreCatalogue, this.slugCatalogue, this.urlCatalogue, this.organisationCatalogue,
         this.organisationCatalogueId, this.licenceCatalogue, this.isCataloguePrive, this.isCatalogueArchive
      };

      return MessageFormat.format(fmt, args);
   }
}
