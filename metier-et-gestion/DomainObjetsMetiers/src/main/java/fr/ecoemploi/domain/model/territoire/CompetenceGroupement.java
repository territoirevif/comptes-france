package fr.ecoemploi.domain.model.territoire;

import java.text.MessageFormat;
import java.util.*;

/**
 * Compétence d'une intercommunalité.
 * @author Marc LE BIHAN
 */
public enum CompetenceGroupement {
   /** Electricité, Gaz */
   C1004, 
   
   /** Hydraulique */
   C1010,
   
   /** Production, distribution d'énergie (OBSOLETE) */
   C1015(true),
   
   /** Chauffage urbain */
   C1020,
   
   /** Soutien aux actions de MDE */
   C1025,
   
   /** Autres énergies */
   C1030,
   
   /** Eau (Traitement, Adduction, Distribution) */
   C1502(TypeCompetence.OPTIONNELLE),
   
   /** Assainissement collectif */
   C1505(TypeCompetence.OPTIONNELLE),
   
   /** Assainissement non collectif */
   C1507(TypeCompetence.OPTIONNELLE),
   
   /** Collecte des déchets des ménages et déchets assimilés */
   C1510(TypeCompetence.OBLIGATOIRE),
   
   /** Traitement des déchets des ménages et déchets assimilés */
   C1515,
   
   /** Lutte contre les nuisances sonores */
   C1520,
   
   /** Qualité de l'air */
   C1525,

   /** GEMAPI : Aménagement d'un bassin ou d'une fraction de bassin hydrographique */
   C1528,

   /** GEMAPI : Entretien et aménagement d'un cours d'eau, canal, lac ou plan d'eau */
   C1529,   

   /** Gestion des milieux aquatiques et prévention des inondations (GEMAPI) (OBSOLETE) */
   C1530,

   /** GEMAPI : Défense contre les inondations et contre la mer */
   C1531,

   /** GEMAPI : Protection et restauration des sites, des écosystèmes aquatiques, des zones humides et des formations boisées riveraines */
   C1532,

   /** Gestion des eaux pluviales urbaines */
   C1533,

   /** Maîtrise des eaux pluviales et de ruissellement ou la lutte contre l'érosion de sols */
   C1534,

   /** Parcs naturels régionaux */
   C1535,
   
   /** Autres actions environnementales */
   C1540,

   /** Autorité concessionnaire de l'Etat pour les plages, dans les conditions prévues à l'article L. 2124-4 du code général de la propriété des personnes publiques. */
   C1545, 

   /** Création et entretien des infrastructures de charge nécessaires à l'usage des véhicules électriques ou hybrides rechargeables, en application de l'article L. 2224-37 du CGCT. */
   C1550, 

   /** Elaboration et adoption du plan climat-air-énergie territorial en application de l'article L. 229-26 du code de l'environnement */
   C1555,

   /** Contribution à la transition énergétique */
   C1560,  

   /** Création/Suppression/Extension/Translation des cimetières et sites funéraires */
   C2005,
   
   /** Crématorium */
   C2010,
   
   /** Service extérieur de Pompes funèbres */
   C2015,
   
   /** Aide sociale facultative */
   C2510,
   
   /** Activités sanitaires */
   C2515,
   
   /** Action sociale */
   C2520,
   
   /** CIAS */
   C2525,

   /** Crèche, Relais assistance maternelle, aide à la petite enfance */
   C2521, 

   /** Maisons de santé pluridisciplinaires */
   C2526,

   /** Dispositifs contractuels de développement urbain, de développement local et d’insertion économique et sociale */
   C3005,

   /** Contrat local de sécurité transports */
   C3010, 
   
   /** PLIE (plan local pour l'insertion et l'emploi) */
   C3015,
   
   /** CUCS (contrat urbain de cohésion sociale) */
   C3020,
   
   /** Rénovation urbaine (ANRU) */
   C3025,
   
   /** Conseil intercommunal de sécurité et de prévention de la délinquance */
   C3210,
   
   /** Contrat local de sécurité transports */
   C3220,
   
   /** Création, aménagement, entretien et gestion de zone d’activités industrielle, commerciale, tertiaire, artisanale ou touristique */
   C3505(TypeCompetence.OBLIGATOIRE),
   
   /** Création, aménagement, entretien et gestion de zone d’activités portuaire ou aéroportuaire */
   C3510(TypeCompetence.OBLIGATOIRE),
   
   /** Action de développement économique (soutien des activités industrielles, commerciales ou de l'emploi, soutien des activités agricoles et forestières…) */
   C3515,
   
   /** Construction ou aménagement, entretien, gestion d’équipements ou d’établissements culturels, socioculturels, socio-éducatifs, sportifs (OBSOLETE) */
   C4005,
   
   /** Construction, aménagement, entretien, gestion d'équipements ou d'établissements culturels, socio-culturels, socio-éducatifs */
   C4006,
   
   /** Construction, aménagement, entretien, gestion d'équipements ou d'établissements sportifs */
   C4007,

   /** Construction, aménagement, entretien et gestion d'équipements culturels et sportifs */
   C4008,
   
   /** Établissements scolaires */
   C4010(TypeCompetence.OBLIGATOIRE),
   
   /** Activités péri-scolaires */
   C4015,

   /** Lycées et collèges */
   C4016,
   
   /** Actions de soutien à l'enseignement supérieur */
   C4017,
   
   /** Activités culturelles ou socioculturelles */
   C4020,
   
   /** Activités sportives */
   C4025,

   /** Schéma de cohérence territoriale (SCOT) */
   C4505(TypeCompetence.OBLIGATOIRE),
   
   /** Schéma de secteur */
   C4510,
   
   /** Plans locaux d’urbanisme */
   C4515(TypeCompetence.OBLIGATOIRE),
   
   /** Création et réalisation de zone d’aménagement concertée (ZAC) */
   C4520,
   
   /** Constitution de réserves foncières */
   C4525,
   
   /** Organisation des transports urbains */
   C4530,
   
   /** Transports scolaires */
   C4531(TypeCompetence.OBLIGATOIRE),
   
   /** Organisation des transports non urbains */
   C4532,
   
   /** Prise en considération d’un programme d’aménagement d’ensemble et détermination des secteurs d’aménagement au sens du code de l’urbanisme */
   C4535,
   
   /** Voies navigables et ports intérieurs (OBSOLETE) */
   C4540(true),
   
   /** Aménagement rural (OBSOLETE) */
   C4545(true),
   
   /** Plans de déplacement urbains */
   C4550,
   
   /** Études et programmation */
   C4555,
   
   /** Délivrance des autorisations d'occupation du sol (Permis de construire...) */
   C4560,
   
   /** Création, aménagement, entretien de la voirie */
   C5005,
   
   /** Signalisation (OBSOLETE) */
   C5010(true),
   
   /** Parcs de stationnement */
   C5015,
   
   /** Tourisme */
   C5210(TypeCompetence.OBLIGATOIRE),
   
   /** Thermalisme */
   C5220,
   
   /** Programme local de l’habitat */
   C5505,
   
   /** Politique du logement non social */
   C5510,
   
   /** Politique du logement social */
   C5515,
   
   /** Politique du logement étudiant */
   C5520,
   
   /** Action et aide financière en faveur du logement social d'intérêt communautaire */
   C5525,
   
   /** Action en faveur du logement des personnes défavorisées par des opérations d’intérêt communautaire */
   C5530,
   
   /** Opération programmée d'amélioration de l'habitat (OPAH) */
   C5535,
   
   /** Amélioration du parc immobilier bâti d'intérêt communautaire */
   C5540,
   
   /** Droit de préemption urbain (DPU) pour la mise en œuvre de la politique communautaire d'équilibre social de l'habitat */
   C5545,
   
   /** Actions de réhabilitation et résorption de l’habitat insalubre */
   C5550,
   
   /** Délégations des aides à la pierre (article 61 - Loi LRL) */
   C5555,
   
   /** Ports */
   C7005,
   
   /** Aérodromes */
   C7010,
   
   /** Participation à la gouvernance et à l'aménagement des gares situées sur le territoire métropilitain */
   C7012,

   /** Voies navigables */
   C7015,
   
   /** Eclairage public */
   C7020,
   
   /** Pistes cyclables */
   C7025,
   
   /** Abattoirs, abattoirs-marchés et marchés d'intérêt national, halles, foires */
   C7030,
   
   /** Préparation et réalisation des enquêtes de recensement de la population */
   C9905,
   
   /** Préfiguration et fonctionnement des Pays */
   C9910,
   
   /** Gestion de personnel (policiers-municipaux et garde-champêtre...) */
   C9915,
   
   /** Acquisition en commun de matériel */
   C9920,
   
   /** Gestion d'un centre de secours */
   C9922,

   /** Service public de défense extérieure contre l'incendie */
   C9923, 

   /** Collecte des contributions pour le financement du SDIS */
   C9924,
   
   /** Infrastructure de télécommunication (téléphonie mobile...) */
   C9925,
   
   /** NTIC (Internet, câble…) */
   C9930,
   
   /** Réalisation d'aire d'accueil ou de terrains de passage des gens du voyage */
   C9935(TypeCompetence.OBLIGATOIRE),
   
   /** Création et gestion des maisons de services au public */
   C9940,
   
   /** Archives */
   C9950,
   
   /** Autres */
   C9999;
   
   /** Indicateur d'obsolescence de cette compétence. */
   private final boolean obsolete;
   
   /** Libellé de l'entrée. */
   private final String libelle;
   
   /** Type de la compétence. */
   private final TypeCompetence type;
   
   /**
    * Construire une entrée d'énumération en recherchant son libellé public : la compétence est déclarée active, par défaut.
    */
   CompetenceGroupement() {
      this(TypeCompetence.FACULTATIVE, false);
   }
   
   /**
    * Construire une entrée d'énumération en recherchant son libellé public, et en spécifiant si elle est obsolète ou non.
    * @param obsolete true si elle est obsolète.
    */
   CompetenceGroupement(boolean obsolete) {
      this(TypeCompetence.FACULTATIVE, obsolete);
   }
   
   /**
    * Construire une entrée d'énumération en recherchant son libellé public, et en spécifiant si elle est obsolète ou non.
    * @param type Type de la compétence.
    */
   CompetenceGroupement(TypeCompetence type) {
      this(type, false);
   }
   
   /**
    * Construire une entrée d'énumération en recherchant son libellé public, et en spécifiant si elle est obsolète ou non.
    * @param type Type de la compétence.
    * @param obsolete true si elle est obsolète.
    */
   CompetenceGroupement(TypeCompetence type, boolean obsolete) {
      ResourceBundle rsc = ResourceBundle.getBundle("fr.ecoemploi.domain.model.territoire.CompetenceGroupement");

      try {
         String format = rsc.getString(name());
         this.libelle = MessageFormat.format(format, new Object[] {});
         this.type = type;
         this.obsolete = obsolete;
      }
      catch(MissingResourceException e) {
         throw new RuntimeException(e.getMessage(), e);
      }
   }
   
   /**
    * Renvoyer le libellé public de l'entrée.
    * @return Libellé.
    */
   public String getLibelle() {
      return this.libelle;
   }

   /**
    * Renvoyer le type de compétence.
    * @return Type de compétence.
    */
   public TypeCompetence getType() {
      return this.type;
   }
   
   /**
    * Déterminer si cette compétence est obsolète.
    * @return true, si elle l'est.
    */
   public boolean isObsolete() {
      return this.obsolete;
   }

   /**
    * @see java.lang.Enum#toString()
    */
   @Override
   public String toString() {
      return this.libelle;
   }
}
