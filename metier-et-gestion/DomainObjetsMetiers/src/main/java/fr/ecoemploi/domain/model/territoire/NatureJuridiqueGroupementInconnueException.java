package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.MetierException;

/**
 * Exception levée si une nature juridique de groupement est inconnue.
 * @author Marc LE BIHAN
 */
public class NatureJuridiqueGroupementInconnueException extends MetierException {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 8287474350045611065L;

   /**
    * Construire une exception.
    * @param message Message de l'exception.
    */
   public NatureJuridiqueGroupementInconnueException(String message) {
      super(message);
   }
}
