package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;
import java.text.*;

import fr.ecoemploi.domain.utils.objets.ObjetMetier;

/**
 * Une adresse.
 * @author Marc LE BIHAN.
 */
public class Adresse extends ObjetMetier {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -8214193790366856517L;

   /** Nom. */
   private String nom;

   /** Complément de nom. */
   private String complementNom;

   /** Complément de voie. */
   private String complementVoie;

   /** Numéro de voie. */
   private String numeroVoie;
   
   /** Type de voie. */
   private String typeVoie;
   
   /** Indice de répétition */
   private String repetitionNumeroDansVoie;

   /** Voie. */
   private String voie;

   /** Code postal. */
   private String codePostal;
   
   /** Code commune. */
   private CodeCommune codeCommune;
   
   /** Bureau distributeur. */
   private String bureauDistributeur;
   
   /** Code CEDEX */
   private String cedex;
   
   /** Libellé CEDEX. */
   private String libelleCedex;

   /** Ville. */
   private String ville;

   /** Code pays pour un établissement situé à l’étranger */
   private String codePaysEtranger;

   /** Libellé du pays pour un établissement situé à l’étranger */
   private String nomPaysEtranger;   

   /** Commentaire associé à cette adresse. */
   private String commentaire;
   
   /**
    * Construire une adrese.
    */
   public Adresse() {
   }
   
   /**
    * Construire une adrese.
    * @param nom Nom du destinataire.
    * @param complementNom Complément de nom (chez ...).
    * @param complementVoie Complément de voie.
    * @param numeroVoie Numéro dans la voie.
    * @param typeVoie Type de voie.
    * @param repetitionNumeroDansVoie Répétition dans la voie.
    * @param voie Voie.
    * @param codePostal Code postal.
    * @param codeCommune Code Commune.
    * @param bureauDistributeur Bureau distributeur.
    * @param cedex Cedex.
    * @param libelleCedex Libellé Cedex.
    * @param ville Commune.
    * @param codePaysEtranger Code du pays étranger.
    * @param nomPaysEtranger Nom du pays étranger.
    * @param commentaire Commentaire associé à l'adresse.
    */
   public Adresse(String nom, String complementNom, String complementVoie, String numeroVoie, String typeVoie,
         String repetitionNumeroDansVoie, String voie, String codePostal, CodeCommune codeCommune, String bureauDistributeur, 
         String cedex, String libelleCedex, String ville, String codePaysEtranger, String nomPaysEtranger, String commentaire) {
      this.nom = nom;
      this.complementNom = complementNom;
      this.complementVoie = complementVoie;
      this.numeroVoie = numeroVoie;
      this.typeVoie = typeVoie;
      
      this.repetitionNumeroDansVoie = repetitionNumeroDansVoie;
      this.voie = voie;
      this.codePostal = codePostal;
      this.codeCommune = codeCommune;
      this.bureauDistributeur = bureauDistributeur;
      
      this.cedex = cedex;
      this.libelleCedex = libelleCedex;
      this.ville = ville;
      this.codePaysEtranger = codePaysEtranger;
      this.nomPaysEtranger = nomPaysEtranger;
      
      this.commentaire = commentaire;
   }

   /**
    * Construire une adresse par copie.
    * @param adresse Adresse à copier.
    */
   public Adresse(Adresse adresse) {
      super(adresse);
      this.nom = adresse.nom;
      this.complementNom = adresse.complementNom;
      this.complementVoie = adresse.complementVoie;
      this.numeroVoie = adresse.numeroVoie;
      this.typeVoie = adresse.typeVoie;

      this.repetitionNumeroDansVoie = adresse.repetitionNumeroDansVoie;
      this.voie = adresse.voie;
      this.codePostal = adresse.codePostal;
      this.codeCommune = adresse.codeCommune;
      this.bureauDistributeur = adresse.bureauDistributeur;

      this.cedex = adresse.cedex;
      this.libelleCedex = adresse.libelleCedex;
      this.ville = adresse.ville;
      this.codePaysEtranger = adresse.codePaysEtranger;
      this.nomPaysEtranger = adresse.nomPaysEtranger;

      this.commentaire = adresse.commentaire;
   }
   
   /**
    * Renvoyer le code postal associé à cette ville.
    * @return Code postal.
    */
   public String getCodePostal() {
      return this.codePostal;
   }

   /**
    * Fixer le code postal associé à cette ville.
    * @param codePostal Code postal 
    */
   public void setCodePostal(String codePostal) {
      this.codePostal = codePostal;
   }

   /**
    * Renvoyer le commentaire associé à cette adresse.
    * @return Commentaire.
    */
   public String getCommentaire() {
      return this.commentaire;
   }

   /**
    * Renvoyer le complément de nom.
    * @return Complément de nom.
    */
   public String getComplementNom() {
      return this.complementNom;
   }

   /**
    * Renvoyer le complément de voie.
    * @return Complément de voie.
    */
   public String getComplementVoie() {
      return this.complementVoie;
   }

   /**
    * Renvoyer le nom.
    * @return Nom.
    */
   public String getNom() {
      return this.nom;
   }

   /**
    * Return la voie.
    * @return Ville.
    */
   public String getVoie() {
      return this.voie;
   }

   /**
    * Renvoyer la ville.
    * @return Ville.
    */
   public String getVille() {
      return this.ville;
   }

   /**
    * Fixer le nom.
    * @param nomAdresse Nom.
    */
   public void setNom(String nomAdresse) {
      this.nom = nomAdresse;
   }

   /**
    * Fixer le commentaire associé à cette adresse.
    * @param commentaireAdresse Commentaire.
    */
   public void setCommentaire(String commentaireAdresse) {
      this.commentaire = commentaireAdresse;
   }

   /**
    * Fixer le complément de nom.
    * @param complement Complément de nom.
    */
   public void setComplementNom(String complement) {
      this.complementNom = complement;
   }

   /**
    * Fixer le complément de voie.
    * @param complement Complément de voie.
    */
   public void setComplementVoie(String complement) {
      this.complementVoie = complement;
   }

   /**
    * Fixer la voie.
    * @param voieAdresse Voie.
    */
   public void setVoie(String voieAdresse) {
      this.voie = voieAdresse;
   }

   /**
    * Fixer la ville.
    * @param villeAdresse Ville.
    */
   public void setVille(String villeAdresse) {
      this.ville = villeAdresse;
   }

   /**
    * Renvoyer le numéro de voie.
    * @return Numéro de voie.
    */
   public String getNumeroVoie() {
      return this.numeroVoie;
   }

   /**
    * Fixer le numéro de voie.
    * @param numeroVoie Numéro de voie. 
    */
   public void setNumeroVoie(String numeroVoie) {
      this.numeroVoie = numeroVoie;
   }

   /**
    * Renvoyer le type de voie.
    * @return Type de voie.
    */
   public String getTypeVoie() {
      return this.typeVoie;
   }

   /**
    * Fixer le type de voie.
    * @param typeVoie Type de voie.
    */
   public void setTypeVoie(String typeVoie) {
      this.typeVoie = typeVoie;
   }

   /**
    * Renvoyer la répétition du numéro dans la voie.
    * @return Répétition du numéro dans la voie.
    */
   public String getRepetitionNumeroDansVoie() {
      return this.repetitionNumeroDansVoie;
   }

   /**
    * Fixer la répétition du numéro dans la voie.
    * @param repetitionNumeroDansVoie Répétition du numéro dans la voie.
    */
   public void setRepetitionNumeroDansVoie(String repetitionNumeroDansVoie) {
      this.repetitionNumeroDansVoie = repetitionNumeroDansVoie;
   }

   /**
    * Renvoyer le code de la commune.
    * @return Code de la commune.
    */
   public CodeCommune getCodeCommune() {
      return this.codeCommune;
   }

   /**
    * Fixer le code de la commune.
    * @param codeCommune Code de la commune.
    */
   public void setCodeCommune(CodeCommune codeCommune) {
      this.codeCommune = codeCommune;
   }

   /**
    * Renvoyer le bureau distributeur.
    * @return Bureau distributeur.
    */
   public String getBureauDistributeur() {
      return this.bureauDistributeur;
   }

   /**
    * Fixer le bureau distributeur.
    * @param bureauDistributeur Bureau distributeur. 
    */
   public void setBureauDistributeur(String bureauDistributeur) {
      this.bureauDistributeur = bureauDistributeur;
   }

   /**
    * Renvoyer le code Cedex.
    * @return Code Cedex.
    */
   public String getCedex() {
      return this.cedex;
   }

   /**
    * Fixer le code Cedex.
    * @param cedex Code Cedex.
    */
   public void setCedex(String cedex) {
      this.cedex = cedex;
   }

   /**
    * Renvoyer le libellé du code Cedex.
    * @return Libellé du code Cedex.
    */
   public String getLibelleCedex() {
      return this.libelleCedex;
   }

   /**
    * Fixer le libellé du code Cedex.
    * @param libelleCedex Libellé du code Cedex.
    */
   public void setLibelleCedex(String libelleCedex) {
      this.libelleCedex = libelleCedex;
   }

   /**
    * Renvoyer le code pays étranger.
    * @return Code pays étranger.
    */
   public String getCodePaysEtranger() {
      return this.codePaysEtranger;
   }

   /**
    * Fixer le code pays étranger.
    * @param codePaysEtranger Code pays étranger.
    */
   public void setCodePaysEtranger(String codePaysEtranger) {
      this.codePaysEtranger = codePaysEtranger;
   }

   /**
    * Renvoyer le nom du pays étranger.
    * @return Nom du pays étranger.
    */
   public String getNomPaysEtranger() {
      return this.nomPaysEtranger;
   }

   /**
    * Fixer le nom du pays étranger.
    * @param nomPaysEtranger Nom du pays étranger.
    */
   public void setNomPaysEtranger(String nomPaysEtranger) {
      this.nomPaysEtranger = nomPaysEtranger;
   }

   /**
    * @see java.lang.Object#toString()
    */
   @Override
   public String toString() {
      String format = "Adresse '{'"
         + "nom : {0}, complément de nom : {1}, complément de voie : {2}, type de voie : {13}, numéro dans la voie : {14}, répétition dans la voie : {15}, voie : {3}, "
         + "code postal : {6}, commune : {4}, commentaire : {5}, "
         + "code commune: {7}, bureau distributeur : {8}, code cedex : {9}, libellé cedex : {10}, code pays étranger : {11}, nom pays étranger : {12}"
         + "'}'";
      
      return MessageFormat.format(format, this.nom, this.complementNom, this.complementVoie, this.voie, this.ville, this.commentaire, this.codePostal,
         this.codeCommune, this.bureauDistributeur, this.cedex, this.libelleCedex, this.codePaysEtranger, this.nomPaysEtranger, this.typeVoie, 
         this.repetitionNumeroDansVoie, this.numeroVoie);   
   }
}
