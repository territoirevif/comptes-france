package fr.ecoemploi.domain.model.territoire.entreprise;

import java.io.Serial;
import java.util.*;

/**
 * Un ensemble d'entreprises.
 */
public class Entreprises extends LinkedHashMap<String, Entreprise> implements Iterable<Entreprise> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 4714802143695753615L;
   
   /**
    * Construire une liste d'entreprises vide.
    */
   public Entreprises() {
   }
   
   /**
    * Construire une liste d'entreprises alimentée.
    * @param source Source d'entreprises à copier. 
    */
   public Entreprises(Collection<Entreprise> source) {
      Objects.requireNonNull(source, "La collection des établissements à copier ne peut pas valoir null.");
      source.forEach(this::add);
   }
   
   /**
    * Ajouter un élément à la liste.
    * @param entreprise Entreprise à ajouter.
    */
   public void add(Entreprise entreprise) {
      Objects.requireNonNull(entreprise, "L'entreprise à ajouter ne peut pas valoir null.");
      put(entreprise.getSiren(), entreprise);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Iterator<Entreprise> iterator() {
      return values().iterator();
   }
}
