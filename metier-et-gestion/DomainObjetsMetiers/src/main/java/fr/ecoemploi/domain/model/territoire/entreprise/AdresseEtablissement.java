package fr.ecoemploi.domain.model.territoire.entreprise;

import fr.ecoemploi.domain.utils.objets.ObjetMetier;
import fr.ecoemploi.domain.model.territoire.*;

import java.io.Serial;
import java.text.MessageFormat;

/**
 * Adresse géographique d'un établissement.
 * @author Marc LE BIHAN
 */
public class AdresseEtablissement extends ObjetMetier {
   /** Adresse géographique */
   @Serial
   private static final long serialVersionUID = 8405727453998737634L;

   /** Distribution spéciale de l’établissement */
   private String distributionSpeciale;

   /** Numéro dans la voie */
   private String numeroVoie;
   
   /** Indice de répétition */
   private String repetitionNumeroDansVoie;
   
   /** Type de voie de localisation de l'établissement */
   private String typeDeVoie;
   
   /** Libellé de voie de localisation de l'établissement */
   private String libelleVoie;
   
   /** Complément d’adresse */
   private String complementAdresse;
   
   /** Code postal */
   private String codePostal;
   
   /** Code CEDEX */
   private String cedex;

   /** Libellé du code cedex */
   private String libelleCedex;
   
   /** Commune de localisation de l'établissement */
   private String codeCommune;
   
   /** Libellé de la commune de localisation de l'établissement */
   private String nomCommune;
   
   /** Libellé de la commune située à l’étranger */
   private String nomCommuneEtrangere;

   /** Code pays pour un établissement situé à l’étranger */
   private String codePaysEtranger;

   /** Libellé du pays pour un établissement situé à l’étranger */
   private String nomPaysEtranger;   
   
   /**
    * Construire une adresse d'établissement.
    */
   public AdresseEtablissement() {
   }

   /**
    * Construire une adresse d'établissement.
    * @param distributionSpeciale Distribution spéciale de l’établissement.
    * @param numeroVoie Numéro dans la voie.
    * @param repetitionNumeroDansVoie Indice de répétition.
    * @param typeDeVoie Type de voie de localisation de l'établissement. 
    * @param libelleDansVoie Libellé de voie de localisation de l'établissement.
    * @param complementAdresse Complément d’adresse.
    * @param codePostal Code postal.
    * @param cedex Cedex.
    * @param libelleCedex Libellé du code cedex. 
    * @param codeCommune Commune de l'établissement. 
    * @param libelleCommune Libellé de la commune de l'établissement.
    * @param libellecommuneetrangere Libellé de la commune située à l’étranger.
    * @param codePaysEtranger Code pays pour un établissement situé à l’étranger.
    * @param libellePaysEtranger Libellé du pays pour un établissement situé à l’étranger.
    */
   public AdresseEtablissement(String distributionSpeciale, String numeroVoie, String repetitionNumeroDansVoie, String typeDeVoie, String libelleDansVoie, 
         String complementAdresse, String codePostal, String cedex, String libelleCedex, String codeCommune, String libelleCommune, 
         String libellecommuneetrangere, String codePaysEtranger, String libellePaysEtranger) {
      this.distributionSpeciale = nullSiBlanc(distributionSpeciale);
      this.numeroVoie = nullSiBlanc(numeroVoie);
      this.repetitionNumeroDansVoie = nullSiBlanc(repetitionNumeroDansVoie);
      this.typeDeVoie = typeDeVoie;
      this.libelleVoie = nullSiBlanc(libelleDansVoie);
      this.complementAdresse = nullSiBlanc(complementAdresse);
      this.codePostal = nullSiBlanc(codePostal);
      this.cedex = nullSiBlanc(cedex);
      this.libelleCedex = nullSiBlanc(libelleCedex);
      this.codeCommune = codeCommune;
      this.nomCommune = nullSiBlanc(libelleCommune);
      this.nomCommuneEtrangere = nullSiBlanc(libellecommuneetrangere);
      this.codePaysEtranger = nullSiBlanc(codePaysEtranger);
      this.nomPaysEtranger = nullSiBlanc(libellePaysEtranger);
   }

   /**
    * Renvoyer la distribution spéciale de l’établissement.
    * @return Distribution spéciale de l’établissement.
    */
   public String getDistributionSpeciale() {
      return this.distributionSpeciale;
   }

   /**
    * Fixer la distribution spéciale de l’établissement.
    * @param distributionSpeciale Distribution spéciale de l’établissement.
    */
   public void setDistributionSpeciale(String distributionSpeciale) {
      this.distributionSpeciale = nullSiBlanc(distributionSpeciale);
   }
   
   /**
    * Renvoyer le numéro dans la voie figurant dans l'adresse de localisation de l'établissement (NUMVOIE) : il n'est pas toujours renseigné,
    * Lorsqu'elle est renseignée, elle est cadrée à droite pour être aisément associée à l'indice de répétition (INDREP).
    * @return Numéro de voie.
    */
   public String getNumeroVoie() {
      return this.numeroVoie;
   }

   /**
    * Fixer le numéro dans la voie figurant dans l'adresse de localisation de l'établissement (NUMVOIE) : il n'est pas toujours renseigné,
    * Lorsqu'elle est renseignée, elle est cadrée à droite pour être aisément associée à l'indice de répétition (INDREP).
    * @param numero Numéro de voie. 
    */
   public void setNumeroVoie(String numero) {
      this.numeroVoie = nullSiBlanc(numero);
   }
   
   /**
    * Renvoyer l'indicateur de répétition du numéro dans la voie (Bis, Ter...) (INDREP) associée à la variable numéro dans la voie (NUMVOIE).
    * @return Indicateur de répétition :<br> 
    * " " Par défaut<br>
    * B bis<br>
    * T ter<br>
    * Q quater<br>
    * C quinquies<br>
    * On peut avoir aussi exceptionnellement des répétitions séquentielles A, B, C, D,.....<br>
    */
   public String getRepetitionNumeroDansVoie() {
      return this.repetitionNumeroDansVoie;
   }

   /**
    * Fixer l'indicateur de répétition du numéro dans la voie (Bis, Ter...) associée à la variable numéro dans la voie (NUMVOIE) (INDREP) :<br>
    * @param repetition Indicateur de répétition :<br>
    * " " Par défaut<br>
    * B bis<br>
    * T ter<br>
    * Q quater<br>
    * C quinquies<br>
    * On peut avoir aussi exceptionnellement des répétitions séquentielles A, B, C, D,.....<br>
    */
   public void setRepetitionNumeroDansVoie(String repetition) {
      this.repetitionNumeroDansVoie = nullSiBlanc(repetition);
   }

   /**
    * Renvoyer le type de voie (TYPVOIE).
    * @return Type de voie. 
    */
   public TypeDeVoie asTypeDeVoie() {
      return TypeDeVoie.valueFromAbreviation(this.typeDeVoie);
   }

   /**
    * Renvoyer le type de voie (TYPVOIE).
    * @return Type de voie. 
    */
   public String getTypeDeVoie() {
      return this.typeDeVoie;
   }

   /**
    * Fixer le type de voie (TYPVOIE).
    * @param type Type de voie.
    */
   public void setTypeDeVoie(String type) {
      this.typeDeVoie = type;
   }
   
   /**
    * Renvoyer le libellé de voie de la commune de localisation de l'établissement (LIBVOIE) :<br>
    * Cette variable n'est pas toujours renseignée en particulier dans les petites communes.<br> 
    * Le lexique des abréviations autorisées est celui de la norme fonctionnelle d'échanges d'information et qui reprend celui de la norme AFNOR Z 10-011.
    * @return libellé de voie.
    */
   public String getLibelleVoie() {
      return this.libelleVoie;
   }

   /**
    * Fixer le libellé de voie de la commune de localisation de l'établissement (LIBVOIE).
    * @param libelle Libellé de voie. 
    */
   public void setLibelleVoie(String libelle) {
      this.libelleVoie = nullSiBlanc(libelle);
   }

   /**
    * Renvoyer le complément d’adresse.
    * @return Complément d’adresse.
    */
   public String getComplementAdresse() {
      return this.complementAdresse;
   }

   /**
    * Fixer le complément d’adresse.
    * @param complementAdresse Complément d’adresse.
    */
   public void setComplementAdresse(String complementAdresse) {
      this.complementAdresse = nullSiBlanc(complementAdresse);
   }

   /**
    * Renvoyer le code postal de l'adresse de l'établissement, utilisé par la Poste (CODPOS) :<br>
    * Il peut parfois être différent de celui contenu dans les variables L6_NORMALISEE et L6_DECLAREE (sixième ligne d'adressage normalisée ou déclarée) :<br>
    * Il s'agit du code postal rattaché à l'adresse de localisation. Cette situation se produit lorsque l'établissement a une adresse cédexée par exemple, 
    * ou qu'il est distribué par un bureau distributeur différent de celui de la commune de localisation.
    * @return Code postal. 
    */
   public String getCodePostal() {
      return this.codePostal;
   }
   
   /**
    * Fixer le code postal de l'établissement, utilisé par la Poste (CODPOS).
    * @param codePostal Code Postal.
    */
   public void setCodePostal(String codePostal) {
      this.codePostal = nullSiBlanc(codePostal);
   }

   /**
    * Renvoyer le cedex.
    * @return Cedex.
    */
   public String getCedex() {
      return this.cedex;
   }

   /**
    * Fixer le cedex.
    * @param cedex Cedex.
    */
   public void setCedex(String cedex) {
      this.cedex = nullSiBlanc(cedex);
   }

   /**
    * Renvoyer le libellé du code cedex.
    * @return Libellé du code cedex.
    */
   public String getLibelleCedex() {
      return this.libelleCedex;
   }

   /**
    * Fixer le libellé du code cedex.
    * @param libelleCedex Libellé du code cedex.
    */
   public void setLibelleCedex(String libelleCedex) {
      this.libelleCedex = nullSiBlanc(libelleCedex);
   }

   /**
    * Renvoyer la commune de l'établissement.
    * @return Commune de l'établissement.
    */
   public CodeCommune asCodeCommune() {
      return new CodeCommune(this.codeCommune);
   }

   /**
    * Renvoyer la commune de l'établissement.
    * @return Commune de l'établissement.
    */
   public String getCodeCommune() {
      return this.codeCommune;
   }

   /**
    * Fixer la commune de l'établissement.
    * @param codeCommune Commune de l'établissement.
    */
   public void setCodeCommune(String codeCommune) {
      this.codeCommune = codeCommune;
   }

   /**
    * Renvoyer le nom de la commune.
    * @return Nom de la commune.
    */
   public String getNomCommune() {
      return this.nomCommune;
   }

   /**
    * Fixer le nom de la commune.
    * @param nom Nom de la commune.
    */
   public void setNomCommune(String nom) {
      this.nomCommune = nullSiBlanc(nom);
   }

   /**
    * Renvoyer le nom de la commune étrangère.
    * @return Nom de la commune étrangère.
    */
   public String getNomCommuneEtrangere() {
      return this.nomCommuneEtrangere;
   }

   /**
    * Fixer le nom de la commune étrangère.
    * @param nom Nom de la commune étrangère.
    */
   public void setNomCommuneEtrangere(String nom) {
      this.nomCommuneEtrangere = nullSiBlanc(nom);
   }

   /**
    * Renvoyer le code du pays étranger.
    * @return Code du pays étranger.
    */
   public String getCodePaysEtranger() {
      return this.codePaysEtranger;
   }

   /**
    * Fixer le code du pays étranger.
    * @param codePaysEtranger Code du pays étranger.
    */
   public void setCodePaysEtranger(String codePaysEtranger) {
      this.codePaysEtranger = nullSiBlanc(codePaysEtranger);
   }

   /**
    * Renvoyer le nom du pays étranger.
    * @return Nom du pays étranger.
    */
   public String getNomPaysEtranger() {
      return this.nomPaysEtranger;
   }

   /**
    * Fixer le nom du pays étranger.
    * @param nom Nom du pays étranger.
    */
   public void setNomPaysEtranger(String nom) {
      this.nomPaysEtranger = nullSiBlanc(nom);
   }
   
   /**
    * Déterminer si cette adresse est vide.
    * @return true si ces champs la font considérer comme vide.
    */
   public boolean isEmpty() {
      boolean vide = this.distributionSpeciale == null;
      vide = vide && this.numeroVoie == null; 
      vide = vide && this.repetitionNumeroDansVoie == null; 
      vide = vide && this.typeDeVoie == null; 
      vide = vide && this.libelleVoie == null; 
      vide = vide && this.complementAdresse == null; 
      vide = vide && this.codePostal == null; 
      vide = vide && this.cedex == null; 
      vide = vide && this.libelleCedex == null; 
      vide = vide && this.codeCommune == null; 
      vide = vide && this.nomCommune == null; 
      vide = vide && this.nomCommuneEtrangere == null; 
      vide = vide && this.codePaysEtranger == null; 
      vide = vide && this.nomPaysEtranger == null;
      
      return vide;
   }
   
   /**
    * Mettre une chaîne nulle si valeur blanche ou vide.
    * @param candidat Candidat.
    * @return Valeur du candidat ou null s'il vaut blanc ou vide.
    */
   protected String nullSiBlanc(String candidat) {
      if (candidat == null || candidat.isBlank()) {
         return null;
      }
      
      return candidat;
   }
   
   /**
    * @see ObjetMetier#toString()
    */
   @Override
   public String toString() {
      String format = "'{'{0}, distribution spéciale : {1}, numéro dans la voie : {2}, répétition : {3}, type de voie : {4}, " +
         "libellé de voie : {5}, complément d''adresse : {6}, code postal : {7}, cedex : {8} - {9}, " +
         "commune : {10} - {11}, commune étrangère : {12}, pays : {13} - {14}'}'";

      return MessageFormat.format(format, super.toString(), this.distributionSpeciale, this.numeroVoie, this.repetitionNumeroDansVoie, this.typeDeVoie,
         this.libelleVoie, this.complementAdresse, this.codePostal, this.cedex, this.libelleCedex,
         this.codeCommune, this.nomCommune, this.nomCommuneEtrangere, this.codePaysEtranger, this.nomPaysEtranger);
   }
}
