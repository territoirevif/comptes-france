package fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels;

import java.io.Serial;
import java.util.*;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Comptes avec total, par habitant, total réduction votée, réduction par habitant, réduction moyenne strate.
 * @author Marc Le Bihan
 */
@Schema(description = "Comptes avec total, par habitant, total réduction votée, réduction par habitant, réduction moyenne strate",
   allOf = ComptesTotalOuBaseEtParHabitant.class, discriminatorProperty = "typeCompteIndividuel")
public class ComptesAvecReductionVotee extends ComptesTotalOuBaseEtParHabitant {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3392897826012994235L;

   /** Réductions de base accordées sur délibérations */
   @Schema(description = "Réductions de base accordées sur délibérations")
   private Double reductionVotee;

   /**
    * Construire de comptes montant (total ou base), par habitant, moyenne strate par habitant + réduction votée, réduction votée par habitant, réduction votée moyenne strate.
    */
   public ComptesAvecReductionVotee() {
   }

   /**
    * Construire de comptes montant (total ou base), par habitant, réduction votée.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    */
   public ComptesAvecReductionVotee(String libelleAnalyse) {
      super(libelleAnalyse);
   }
   
   /**
    * Construire de comptes montant (total ou base), par habitant, réduction votée.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    * @param montant Montant (total ou base).
    * @param parHabitant Montant par habitant.
    * @param reductionVotee Réduction votée.
    */
   public ComptesAvecReductionVotee(String libelleAnalyse, Double montant, Double parHabitant, Double reductionVotee) {
      super(libelleAnalyse, montant, parHabitant);
      this.reductionVotee = reductionVotee;
   }

   /**
    * Renvoyer la réduction votée. 
    * @return Réduction votée.
    */
   public Double getReductionVotee() {
      return this.reductionVotee;
   }

   /**
    * Fixer la réduction votée.
    * @param reductionVotee Réduction votée.
    */
   public void setReductionVotee(Double reductionVotee) {
      this.reductionVotee = reductionVotee;
   }
   
   /**
    * Formatter une ligne texte pour qu'elle se place dans un tableau textuel d'analyse des équilibres financiers fondamentaux.
    * @return Ligne formatée. 
    */
   @Override
   public String toStringAnalyseEquilibresFinanciersFondamentaux() {
      final String FMT_NOMBRE_ENTIER = "%10d\t";
      StringBuilder format = new StringBuilder();
      List<Object> valeurs = new ArrayList<>();

      format.append(getMontant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getMontant() != null ? Math.round(getMontant()) : "");

      format.append(getParHabitant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getParHabitant() != null ? Math.round(getParHabitant()) : "");

      format.append("%s\t");
      valeurs.add(getLibelleAnalyse());

      format.append(getReductionVotee() != null ? "%10.2f" : "%s\t");
      valeurs.add(getReductionVotee() != null ? getReductionVotee() : "");

      return String.format(format.toString(), valeurs.toArray());
   }
}
