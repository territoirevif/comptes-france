package fr.ecoemploi.domain.model.territoire.association;

/**
 * Regroupement d'association
 * (indique si une association est un regroupement d'association ou non).
 * @author Marc Le Bihan
 */
public enum RegroupementAssociation {
   /** Simple. */
   SIMPLE("S", "Simple"),
   
   /** Union. */
   UNION("U", "Union"),
   
   /** Fédération. */
   FEDERATION("F", "Fédération");
   
   /** Code regroupement. */
   private final String code;
   
   /** Libellé de ce regroupement d'association. */
   private final String libelle;
   
   /**
    * Construire une entrée d'énumération.
    * @param code Code du regroupement d'association.
    * @param libelle Libellé.
    */
   RegroupementAssociation(String code, String libelle) {
      this.code = code;
      this.libelle = libelle;
   }
   
   /**
    * Renvoyer le code de cette nature d'intercommunalité.
    * @return Code.
    */
   public String getCode() {
      return this.code;
   }

   /**
    * Renvoyer le libellé public de l'entrée.
    * @return Libellé.
    */
   public String getLibelle() {
      return this.libelle;
   }
}
