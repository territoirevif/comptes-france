package fr.ecoemploi.domain.model.territoire.entreprise;

import java.io.Serial;
import java.io.Serializable;
import java.text.MessageFormat;
import java.time.*;

import fr.ecoemploi.domain.utils.objets.ObjetMetierIdentifiable;
import fr.ecoemploi.domain.utils.autocontrole.ObjetMetierSpark;

/**
 * Classe de base des objets entreprise et établissement qui ont des points communs par SIRENE (INSEE).
 * @param <I> Identifiant de la classe concrète.
 * @author Marc LE BIHAN
 */
public abstract class AbstractSirene<I extends Serializable> extends ObjetMetierSpark {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -2440796329310111471L;

   /** Code SIREN. */
   private I sirenOuSiret;
   
   /** Code APE. */
   private String codeAPE;
   
   /** Nomclature de l'activité principale. */
   private String nomenclatureActivitePrincipale;
   
   /** Tranche d'effectif salarié. */
   private String trancheEffectifSalarie;
   
   /** Année de validité de l'effectif salarié. */
   private Integer anneeValiditeEffectifSalarie;
   
   /** Caractère employeur de l'entreprise ou de l'établissement. */
   private Boolean caractereEmployeur;
   
   /** Indique si l'entreprise est en activitée ou cessée. */
   private Boolean active;
   
   /** Date de début depuis laquelle les données d'historisation restent inchangées. */
   private String dateDebutHistorisation;
   
   /** Date de dernier traitement. */
   private String dateDernierTraitement;
   
   /** Indique le nombre de périodes depuis lequel l'historisation de ces données n'a pas bougé. */ 
   private Integer nombrePeriodes;

   /**
    * Construire la classe de base d'une entreprise ou d'un établissement.
    */
   protected AbstractSirene() {
   }

   /**
    * Construire la classe de base d'une entreprise ou d'un établissement.
    * @param sirenOuSiret Code SIREN.
    * @param codeAPE Code APE.
    * @param nomenclatureActivitePrincipale Nomclature de l'activité principale.
    * @param trancheEffectifSalarie Tranche d'effectif salarié.
    * @param anneeValiditeEffectifSalarie Année de validité de l'effectif salarié.
    * @param caractereEmployeur Caractère employeur de l'entreprise ou de l'établissement.
    * @param active Indique si l'entreprise est en activitée ou cessée.
    * @param dateDebutHistorisation Date de début depuis laquelle les données d'historisation restent inchangées.
    * @param dateDernierTraitement Date de dernier traitement.
    * @param nombrePeriodes Indique le nombre de périodes depuis lequel l'historisation de ces données n'a pas bougé.
    */
   protected AbstractSirene(I sirenOuSiret, String codeAPE, String nomenclatureActivitePrincipale,
                            String trancheEffectifSalarie, Integer anneeValiditeEffectifSalarie, Boolean caractereEmployeur,
                            Boolean active, String dateDebutHistorisation, String dateDernierTraitement, Integer nombrePeriodes) {
      super();
      this.sirenOuSiret = sirenOuSiret;
      this.codeAPE = codeAPE;
      this.nomenclatureActivitePrincipale = nomenclatureActivitePrincipale;
      this.trancheEffectifSalarie = trancheEffectifSalarie;
      this.anneeValiditeEffectifSalarie = anneeValiditeEffectifSalarie;
      this.caractereEmployeur = caractereEmployeur;
      this.active = active;
      this.dateDebutHistorisation = dateDebutHistorisation;
      this.dateDernierTraitement = dateDernierTraitement;
      this.nombrePeriodes = nombrePeriodes;
   }

   /**
    * Renvoyer le numéro de SIREN.
    * @return SIREN.
    */
   protected I sirenOuSiret() {
      return this.sirenOuSiret;
   }
   
   /**
    * Fixer le numéro de SIREN.
    * @param siren SIREN.
    */
   protected void sirenOuSiret(I siren) {
      this.sirenOuSiret = siren;
   }
   
   /**
    * Renvoyer le code caractérisant l'activité principale : le code APE (Activité Principale Exercée) (APEN700 / APET700).<br>
    * L'APE est codifiée selon la Nomenclature d'Activités Française (NAF : Rév2, 2008). Les établissements d'une même entreprise peuvent avoir des activités différentes.<br>
    * Cette variable est systématiquement renseignée : si une entreprise n'a qu'un seul établissement, l'APE de l'établissement (APET) est égal à l'APE de l'entreprise (APEN).<br>
    * Au moment de la déclaration de l'entreprise, il peut arriver que l'INSEE ne soit pas en mesure d'attribuer le bon code APE : la modalité 0000Z peut alors être affectée provisoirement. 
    * @return Code APE. 
    */
   public String getActivitePrincipale() {
      return this.codeAPE;
   }

   /**
    * Fixer le code caractérisant l'activité principale : le code APE (Activité Principale Exercée) (APEN700 / APET700).<br>
    * @param codeAPE Code APE. 
    */
   public void setActivitePrincipale(String codeAPE) {
      this.codeAPE = codeAPE;
   }

   /**
    * Renvoyer la tranche d'effectif salarié de l'entreprise (TEFEN).
    * @return tranche d'effectif salarié de l'entreprise.
    */
   public TrancheEffectif asTrancheEffectifSalarie() {
      return TrancheEffectif.valueFromCode(this.trancheEffectifSalarie);
   }

   /**
    * Renvoyer la tranche d'effectif salarié de l'entreprise (TEFEN).
    * @return tranche d'effectif salarié de l'entreprise.
    */
   public String getTrancheEffectifSalarie() {
      return this.trancheEffectifSalarie;
   }

   /**
    * Fixer la tranche d'effectif salarié de l'entreprise (TEFEN).
    * @param trancheEffectif tranche d'effectif salarié de l'entreprise.
    */
   public void setTrancheEffectifSalarie(String trancheEffectif) {
      this.trancheEffectifSalarie = trancheEffectif;
   }
   
   /**
    * Renvoyer l'année de validité de l'effectif salarié de l'entreprise (DEFEN).
    * @return Année de validité de l'effectif salarié de l'entreprise.
    */
   public Integer getAnneeValiditeEffectifSalarie() {
      return this.anneeValiditeEffectifSalarie;
   }

   /**
    * Fixer l'année de validité de l'effectif salarié de l'entreprise (DEFEN).
    * @param annee Année de validité de l'effectif salarié de l'entreprise.
    */
   public void setAnneeValiditeEffectifSalarie(Integer annee) {
      this.anneeValiditeEffectifSalarie = annee;
   }

   /**
    * Renvoyer la nomclature de l'activité principale.
    * @return Nomclature de l'activité principale.
    */
   public String getNomenclatureActivitePrincipale() {
      return this.nomenclatureActivitePrincipale;
   }

   /**
    * Fixer la nomclature de l'activité principale.
    * @param nomenclature Nomclature de l'activité principale.
    */
   public void setNomenclatureActivitePrincipale(String nomenclature) {
      this.nomenclatureActivitePrincipale = nomenclature;
   }

   /**
    * Renvoyer le caractère employeur de l'entreprise.
    * @return Caractère employeur de l'entreprise.
    */
   public Boolean isCaractereEmployeur() {
      return this.caractereEmployeur;
   }

   /**
    * Fixer le caractère employeur de l'entreprise.
    * @param caractereEmployeur Caractère employeur de l'entreprise.
    */
   public void setCaractereEmployeur(Boolean caractereEmployeur) {
      this.caractereEmployeur = caractereEmployeur;
   }

   /**
    * Renvoyer la date de début depuis laquelle les données d'historisation restent inchangées.
    * @return Date.
    */
   public LocalDate asDateDebutHistorisation() {
      return ObjetMetierSpark.fromINSEEtoLocalDate(this.dateDebutHistorisation);
   }

   /**
    * Renvoyer la date de début depuis laquelle les données d'historisation restent inchangées.
    * @return Date.
    */
   public String getDateDebutHistorisation() {
      return this.dateDebutHistorisation;
   }

   /**
    * Fixer la date de début depuis laquelle les données d'historisation restent inchangées.
    * @param dateDebutHistorisation Date de début de l'historisation.
    */
   public void setDateDebutHistorisation(String dateDebutHistorisation) {
      this.dateDebutHistorisation = dateDebutHistorisation;
   }
   
   /**
    * Renvoyer la date de dernier traitement.
    * @return Date de dernier traitement.
    */
   public LocalDateTime asDateDernierTraitement() {
      return ObjetMetierSpark.fromINSEEtoLocalDateTime(this.dateDernierTraitement);
   }

   /**
    * Renvoyer la date de dernier traitement.
    * @return Date de dernier traitement.
    */
   public String getDateDernierTraitement() {
      return this.dateDernierTraitement;
   }

   /**
    * Déterminer si l'entreprise est en activitée ou cessée.
    * @return true si elle est active, false si elle est cessée.
    */
   public Boolean isActive() {
      return this.active;
   }

   /**
    * Indiquer si l'entreprise est en activitée ou cessée.
    * @param active true si elle est active, false si elle est cessée.
    */
   public void setActive(Boolean active) {
      this.active = active;
   }

   /**
    * Fixer la date de dernier traitement.
    * @param date Date de dernier traitement.
    */
   public void setDateDernierTraitement(String date) {
      this.dateDernierTraitement = date;
   }
   
   /**
    * Renvoyer le nombre de périodes depuis lequel l'historisation de ces données n'a pas bougé.
    * @return Nombre de périodes.
    */
   public Integer getNombrePeriodes() {
      return this.nombrePeriodes;
   }

   /**
    * Fixer le nombre de périodes depuis lequel l'historisation de ces données n'a pas bougé.
    * @param nombrePeriodes Nombre de périodes.
    */
   public void setNombrePeriodes(Integer nombrePeriodes) {
      this.nombrePeriodes = nombrePeriodes;
   }

   /**
    * @see ObjetMetierIdentifiable#toString()
    */
   @Override
   public String toString() {
      String format = "'{'{0}, Activité principale : {1} ({2}), effectif salarié : {3} ({4,number,#0}, employeur : {5}), " +
         "active : {6}, dernier traitement : {7}, historisation débutée le {8}, nombre de périodes sans changement : {9}'}'";

      return MessageFormat.format(format, this.sirenOuSiret, this.codeAPE, this.nomenclatureActivitePrincipale,
         this.trancheEffectifSalarie != null ? this.trancheEffectifSalarie : null, 
         this.anneeValiditeEffectifSalarie, this.caractereEmployeur, this.active, 
         this.dateDernierTraitement != null ? asDateDernierTraitement().toLocalDate() : null,
         this.dateDebutHistorisation != null ? asDateDebutHistorisation() : null, this.nombrePeriodes);
   }
}
