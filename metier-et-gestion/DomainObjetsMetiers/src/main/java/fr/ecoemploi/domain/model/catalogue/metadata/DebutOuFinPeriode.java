package fr.ecoemploi.domain.model.catalogue.metadata;

/**
 * Indique si la source désigne des données se situant en début de période ou en fin de période. 
 * @author Marc LE BIHAN
 */
public enum DebutOuFinPeriode {
   /** Début de période. */
   DEBUT("Début de période (ex : 1er Janvier)"),
   
   /** Fin de période. */
   FIN("Fin de période (ex: 31 Décembre)"),
   
   /** Inconnu. */
   INCONNU("Inconnu");
   
   /** Libellé de l'entrée. */
   private final String libelle;
   
   /**
    * Construire une entrée d'énumération en recherchant son libellé public.
    * @param libelle Libellé
    */
   DebutOuFinPeriode(String libelle) {
      this.libelle = libelle;
   }

   /**
    * Renvoyer le libellé public de l'entrée.
    * @return Libellé.
    */
   public String getLibelle() {
      return this.libelle;
   }
   
   /**
    * @see java.lang.Enum#toString()
    */
   @Override
   public String toString() {
      return this.libelle;
   }
}
