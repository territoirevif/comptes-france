package fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels;

import java.io.Serial;
import java.util.*;

/**
 * Comptes avec montant, montant par habitant, taux voté.
 * @author Marc Le Bihan
 */
public class ComptesAvecTauxVote extends ComptesTotalOuBaseEtParHabitant {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 2514178891034412885L;

   /** Taux voté */
   private Double tauxVote;

   /**
    * Construire de comptes montant (total ou base), par habitant, taux voté.
    */
   public ComptesAvecTauxVote() {
   }

   /**
    * Construire de comptes montant (total ou base), par habitant, taux voté.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    */
   public ComptesAvecTauxVote(String libelleAnalyse) {
      super(libelleAnalyse);
   }

   /**
    * Construire de comptes montant (total ou base), par habitant, taux voté.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    * @param montant Montant (total ou base).
    * @param parHabitant Montant par habitant.
    * @param tauxVote Taux voté.
    */
   public ComptesAvecTauxVote(String libelleAnalyse, Double montant, Double parHabitant, Double tauxVote) {
      super(libelleAnalyse, montant, parHabitant);
      this.tauxVote = tauxVote;
   }
   
   /**
    * Renvoyer le taux voté.
    * @return Taux voté.
    */
   public Double getTauxVote() {
      return this.tauxVote;
   }

   /**
    * Fixer le taux voté.
    * @param taux Taux voté. 
    */
   public void setTauxVote(Double taux) {
      this.tauxVote = taux;
   }
   
   /**
    * Formatter une ligne texte pour qu'elle se place dans un tableau textuel d'analyse des équilibres financiers fondamentaux.
    * @return Ligne formatée. 
    */
   @Override
   public String toStringAnalyseEquilibresFinanciersFondamentaux() {
      final String FMT_NOMBRE_ENTIER = "%10d\t";
      StringBuilder format = new StringBuilder();
      List<Object> valeurs = new ArrayList<>();

      format.append(getMontant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getMontant() != null ? Math.round(getMontant()) : "");

      format.append(getParHabitant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getParHabitant() != null ? Math.round(getParHabitant()) : "");

      format.append("%s\t");
      valeurs.add(getLibelleAnalyse());

      format.append(getTauxVote() != null ? "%10.2f" : "%s");
      valeurs.add(getTauxVote() != null ? getTauxVote() : "");

      return String.format(format.toString(), valeurs.toArray());
   }
}
