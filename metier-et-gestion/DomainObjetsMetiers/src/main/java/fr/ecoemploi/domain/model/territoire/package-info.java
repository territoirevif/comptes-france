/**
 * Objets métiers de description du territoire.
 * @author Marc LE BIHAN
 */
package fr.ecoemploi.domain.model.territoire;