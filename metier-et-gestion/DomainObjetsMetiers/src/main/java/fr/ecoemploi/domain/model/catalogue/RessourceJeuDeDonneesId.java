package fr.ecoemploi.domain.model.catalogue;

import java.io.Serial;

import fr.ecoemploi.domain.utils.autocontrole.Id;

/**
 * Identifiant d'une ressource de jeu de données.
 * @author Marc Le Bihan
 */
public class RessourceJeuDeDonneesId extends Id {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5765525832838702717L;

   /**
    * Construire un identifiant vide.
    */
   public RessourceJeuDeDonneesId() {
   }

   /**
    * Construire un identifiant.
    * @param id Identifiant.
    */
   public RessourceJeuDeDonneesId(String id) {
      super(id);
   }

   /**
    * Copier un identifiant.
    * @param id Identifiant.
    */
   public RessourceJeuDeDonneesId(RessourceJeuDeDonneesId id) {
      super(id);
   }
}
