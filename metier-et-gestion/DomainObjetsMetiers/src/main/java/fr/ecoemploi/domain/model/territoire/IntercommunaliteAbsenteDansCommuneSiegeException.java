package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.MetierException;

/**
 * Exception levée si une intercommunalité est absente dans une commune siège.
 * @author Marc LE BIHAN
 */
public class IntercommunaliteAbsenteDansCommuneSiegeException extends MetierException {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -5504424040009072143L;

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    */
   public IntercommunaliteAbsenteDansCommuneSiegeException(String message) {
      super(message);
   }
}
