package fr.ecoemploi.domain.model.territoire.association;

import java.io.Serial;
import java.time.*;
import java.util.*;

import org.apache.commons.lang3.builder.*;

import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.utils.autocontrole.ObjetMetierSpark;

/**
 * Association du registre RNA / WALDEC.<br>
 * <pre>(issue du traitement RNA, exemple 2021) :
   |numero_waldec|ancien_numero_waldec|siret|numero_rup|gestion|nature|groupement|titre             |titre_court      
   |W012006198   |0012011036          |null |null      |073S   |D     |S         |LES ARDECHOUILLES |LES ARDECHOUILLES 
      
   |objet                                                                                              |code_objet_social1|code_objet_social2
   |réunir les passionnés de moto et organiser des manifestations entrant dans le cadre de son activité|011160            |000000            
      
   |adresse_siege_complement|adresse_siege_numero_voie|obsolete_adresse_siege_repetition|adresse_siege_type_voie|adresse_siege_libelle_voie   
   |null                    |null                     |null                             |COL                    |de Seyaret                   
      
   |adresse_siege_distribution|adresse_siege_code_postal|nom_declarant|adresse_gestion_complement_association|adresse_gestion_complement_geo
   |null                      |07290                    |null         |null                                  |null                          
      
   |adresse_gestion_libelle_voie |adresse_gestion_distribution_facturation|adresse_gestion_code_postal|adresse_gestion_achemine  
   |null                         |SEYARET                                 |07290                      |PREAUX                    
      
   |adresse_gestion_pays|civilite_declarant|site_web|autorisation_publication_web|observation                    |position_activite|maj_time               
   |FRANCE              |MS                |null    |0                           |Reprise  =>  date ag=0000-00-00|A                |2020-08-11 20:00:01    
          
   |libelle_objet_social1                            |libelle_objet_social2|codeCommune|nomCommune|codeRegion|codeDepartement|codeEPCI |nomEPCI           
   |Sports mécaniques (sport automobile, moto, trial)|null                 |07185      |Préaux    |84        |07             |240700716|CC du Val d'Ay    
          
   |populationTotale|date_creation|date_derniere_declaration|date_publication_JO_creation|date_dissolution|nombreAnneesExistence|nombreAnneesDerniereDeclaration|
    |705             |2001-05-16   |2020-02-10               |2001-06-23                  |1901-01-01      |20.304109589041097   |1.5534246575342465
 * </pre>
 * 
 * @author Marc Le Bihan
 */
public class Association extends ObjetMetierSpark {
   /** Serial ID.*/
   @Serial
   private static final long serialVersionUID = -7206954677114644630L;

   /** Numéro Waldec national unique de l’association. */
   private NumeroWaldec numeroWaldec;
   
   /** Ancien numéro waldec. */
   private AncienNumeroWaldec ancienNumeroWaldec;
   
   /** Siret de l'association (facultatif). */
   private SIRET siret;
   
   /** Numéro d'association reconnue d'utilité publique (RUP) attribué par le Ministère. */
   private NumeroRUP numeroRUP;

   /** Code du site gestionnaire de l’association */
   private String gestion;

   /** Nature de l'association : simplement déclarée 1901 ou autre */
   private String nature;

   /** Regroupement de l'association : simple ou union ou fédération (S, U, F). */
   private RegroupementAssociation groupement;

   /** Nom long de l'association. */
   private String titre;

   /** Titre utilisé par DJO comme destinataire de la facturation. */
   private String titreCourt;

   /** Objet social de l'association. */
   private String objet;

   /** Code objet social obligatoire dans la nomenclature nationale. */
   private String codeObjetSocial1;

   /** Second code objet social (facultatif) dans la nomenclature nationale. */
   private String codeObjetSocial2;

   /** Adresse du siège de l’association */
   private Adresse adresseSiege;
   
   /** Nom patronymique du déclarant (présence temporaire) */
   private String nomDeclarant;

   /** Adresse de gestion de l’association (compatibilité format postal) */
   private Adresse adresseGestion;

   /** Code de la civilié du dirigeant principal. */
   private String civiliteDeclarant;

   /** URL du site web de l'association. */
   private String siteWeb;

   /** Indicateur d’autorisation de publication sur le WEB */
   private Boolean autorisationPublicationWeb;

   /** Observation de traitement. */
   private String observation;

   /** Position d’activité de l’association (Active, Dissoute ou Supprimée). */
   private String positionActivite;

   /** Date de mise à jour de l’article */
   private Instant majTime;

   /** Libellé de l'objet social obligatoire. */
   private String libelleObjetSocial1;

   /** Libellé de l'objet social facultatif. */
   private String libelleObjetSocial2;

   /** Code commune du siège de l'association. */
   private CodeCommune codeCommune;

   /** Nom de la commune siège de l'association. */
   private String nomCommune;

   /** Code de la région du siège de l'association. */
   private CodeRegion codeRegion;

   /** Code du département du siège de l'association. */
   private CodeDepartement codeDepartement;

   /** Code EPCI de l'intercommunalité de la commune siège de l'association. */
   private SIRET codeEPCI;

   /** Nom de l'intercommunalité de la commune siège de l'association. */
   private String nomEPCI;

   /** Population totale de la commune siège de l'association. */
   private Integer populationTotale;
   
   /** Date de déclaration de création (date de dépôt du dossier en Préfecture). */
   private LocalDate dateCreation;

   /** Date de la dernière déclaration. */
   private LocalDate dateDerniereDeclaration;

   /** Date de publication JO de l’avis de création. */
   private LocalDate datePublicationJOcreation;

   /** Date de déclaration de dissolution de l’association. */
   private LocalDate dateDissolution;

   /** Nombre d'années d'existence de l'association. */
   private Double nombreAnneesExistence;

   /** Nombre d'années depuis la dernière déclaration de l'association. */
   private Double nombreAnneesDerniereDeclaration;

   /**
    * Construire une association.
    */
   public Association() {
   }

   /**
    * Construire une association.
    * @param numeroWaldec Numéro waldec de l'association.
    * @param ancienNumeroWaldec Ancien numéro waldec de l'association.
    * @param siret SIRET de l'association.
    * @param numeroRUP N° de RUP attribué par le Ministère.
    * @param gestion Code du site gestionnaire de l’association.

    * @param nature Nature de l'association : simplement déclarée 1901 ou autre.
    * @param groupement Regroupement de l'association : simple ou union ou fédération (S, U, F).
    * @param titre Nom long de l'association.
    * @param titreCourt Nom court de l'association (pour les courriers postaux ou facturation).
    * @param objet Objet social de l'association.

    * @param codeObjetSocial1 Code objet social obligatoire dans la nomenclature nationale.
    * @param codeObjetSocial2 Second code objet social (facultatif) dans la nomenclature nationale.
    * @param nomDeclarant Nom du dirigeant principal de l'association.
    * @param civiliteDeclarant Civilité du dirigeant principal de l'association.
    * @param siteWeb URL du site web de l'association.

    * @param autorisationPublicationWeb Autorisation de publication des informations de l'association sur le web.
    * @param observation Observation de traitement.
    * @param positionActivite Position d’activité de l’association (Active, Dissoute ou Supprimée).
    * @param majTime Date/heure de dernière mise à jour des informations de l'association.
    * @param libelleObjetSocial1 Libellé de l'objet social obligatoire.

    * @param libelleObjetSocial2 Libellé de l'objet social facultatif.
    * @param codeCommune Code commune du siège de l'association.
    * @param nomCommune Nom de la commune du siège de l'association.
    * @param codeRegion Code de la région du siège de l'association.
    * @param codeDepartement Code du département du siège de l'association.

    * @param codeEPCI Code EPCI de l'intercommunalité de la commune siège de l'association.
    * @param nomEPCI Nom de l'intercommunalité de la commune siège de l'association.
    * @param populationTotale Population totale de la commune siège de l'association.
    * @param dateCreation Date de déclaration de création (date de dépôt du dossier en Préfecture).
    * @param dateDerniereDeclaration Date de la dernière déclaration.

    * @param datePublicationJOcreation Date de déclaration de création (date de dépôt du dossier en Préfecture).
    * @param dateDissolution Date de déclaration de dissolution de l’association.
    * @param nombreAnneesExistence Nombre d'années d'existence de l'association.
    * @param nombreAnneesDerniereDeclaration Nombre d'années depuis la dernière déclaration de l'association.
    * @param adresseSiegeComplement Complément de nom du siège de l'association.
    * 
    * @param adresseSiegeNumeroVoie Numéro de voie du siège de l'association.
    * @param adresseSiegeTypeVoie Type de voie du siège de l'association.
    * @param adresseSiegeLibelleVoie Libellé de voie du siège de l'association.
    * @param adresseSiegeDistribution Bureau distributeur du siège de l'association.
    * @param adresseSiegeCodePostal Code postal du siège de l'association.
    *
    * @param adresseGestionComplementAssociation Complément de nom de l'adresse de gestion de l'association.
    * @param adresseGestionComplementGeo Complément géographique de l'adresse de gestion de l'association.
    * @param adresseGestionLibelleVoie Libellé de voie de l'adresse de gestion de l'association.
    * @param adresseGestionDistributionFacturation Bureau distributeur de l'adresse de gestion de l'association.
    * @param adresseGestionCodePostal Code postal de l'adresse de gestion de l'association.
    *  
    * @param adresseGestionAchemine Acheminement de l'adresse de gestion de l'association. 
    * @param adresseGestionPays Pays de l'adresse de gestion de l'association.
    */
   public Association(String numeroWaldec, String ancienNumeroWaldec, String siret, String numeroRUP, String gestion,
         String nature, String groupement, String titre, String titreCourt, String objet, 
         String codeObjetSocial1, String codeObjetSocial2, String nomDeclarant, String civiliteDeclarant, String siteWeb, 
         String autorisationPublicationWeb, String observation, String positionActivite, Instant majTime, String libelleObjetSocial1, 
         String libelleObjetSocial2, String codeCommune, String nomCommune, String codeRegion, String codeDepartement, 
         String codeEPCI, String nomEPCI, Integer populationTotale, LocalDate dateCreation, LocalDate dateDerniereDeclaration,
         LocalDate datePublicationJOcreation, LocalDate dateDissolution, Double nombreAnneesExistence, Double nombreAnneesDerniereDeclaration, String adresseSiegeComplement, 
         String adresseSiegeNumeroVoie, String adresseSiegeTypeVoie, String adresseSiegeLibelleVoie, String adresseSiegeDistribution, 
         String adresseSiegeCodePostal, String adresseGestionComplementAssociation, String adresseGestionComplementGeo, String adresseGestionLibelleVoie, String adresseGestionDistributionFacturation, 
         String adresseGestionCodePostal, String adresseGestionAchemine, String adresseGestionPays) {
      this(numeroWaldec != null ? new NumeroWaldec(numeroWaldec) : null, 
         ancienNumeroWaldec != null ? new AncienNumeroWaldec(ancienNumeroWaldec) : null, 
         siret != null ? new SIRET(siret) : null, 
         numeroRUP != null ? new NumeroRUP(numeroRUP) : null, 
         gestion,
         
         nature,
         groupement != null ? Arrays.stream(RegroupementAssociation.values()).filter(e -> groupement.equals(e.getCode())).findFirst().orElse(null) : null,
         titre, 
         titreCourt, 
         objet, 
         
         codeObjetSocial1, 
         codeObjetSocial2,
         
         new Adresse(titreCourt, null, adresseSiegeComplement, adresseSiegeNumeroVoie, adresseSiegeTypeVoie,
               null, adresseSiegeLibelleVoie, adresseSiegeCodePostal, null, adresseSiegeDistribution, 
               null, codeCommune, nomCommune, null, null, null),
         
         nomDeclarant,
         
         new Adresse(adresseGestionComplementGeo, adresseGestionAchemine, adresseGestionComplementAssociation, null, null,
               null, adresseGestionLibelleVoie, adresseGestionCodePostal, null, adresseGestionDistributionFacturation, 
               null, null, null, null, adresseGestionPays, null),
         
         civiliteDeclarant, 
         siteWeb, 
         toBoolean(autorisationPublicationWeb), 
         observation, 
         positionActivite, 
         
         majTime, 
         libelleObjetSocial1, 
         libelleObjetSocial2,
         codeCommune != null ? new CodeCommune(codeCommune) : null, 
         nomCommune, 
         
         codeRegion != null ? new CodeRegion(codeRegion) : null, 
         codeDepartement != null ? new CodeDepartement(codeDepartement) : null, 
         codeEPCI != null ? new SIRET(codeEPCI) : null,
         nomEPCI, 
         populationTotale,
         
         dateCreation, 
         dateDerniereDeclaration, 
         datePublicationJOcreation, 
         dateDissolution, 
         nombreAnneesExistence, 
         nombreAnneesDerniereDeclaration);
   }
   
   /**
    * Construire une association.
    * @param numeroWaldec Numéro waldec de l'association.
    * @param ancienNumeroWaldec Ancien numéro waldec de l'association.
    * @param siret SIRET de l'association.
    * @param numeroRUP N° de RUP attribué par le Ministère.
    * @param gestion Code du site gestionnaire de l’association.
    * @param nature Nature de l'association : simplement déclarée 1901 ou autre.
    * @param groupement Regroupement de l'association : simple ou union ou fédération (S, U, F).
    * @param titre Nom long de l'association.
    * @param titreCourt Nom court de l'association (pour les courriers postaux ou facturation).
    * @param objet Objet social de l'association.
    * @param codeObjetSocial1 Code objet social obligatoire dans la nomenclature nationale.
    * @param codeObjetSocial2 Second code objet social (facultatif) dans la nomenclature nationale.
    * @param adresseSiege Adresse du siège.
    * @param nomDeclarant Nom du dirigeant principal de l'association.
    * @param adresseGestion Adresse de gestion 
    * @param civiliteDeclarant Civilité du dirigeant principal de l'association.
    * @param siteWeb URL du site web de l'association.
    * @param autorisationPublicationWeb Autorisation de publication des informations de l'association sur le web.
    * @param observation Observation de traitement.
    * @param positionActivite Position d’activité de l’association (Active, Dissoute ou Supprimée).
    * @param majTime Date/heure de dernière mise à jour des informations de l'association.
    * @param libelleObjetSocial1 Libellé de l'objet social obligatoire.
    * @param libelleObjetSocial2 Libellé de l'objet social facultatif.
    * @param codeCommune Code commune du siège de l'association.
    * @param nomCommune Nom de la commune du siège de l'association.
    * @param codeRegion Code de la région du siège de l'association.
    * @param codeDepartement Code du département du siège de l'association.
    * @param codeEPCI Code EPCI de l'intercommunalité de la commune siège de l'association.
    * @param nomEPCI Nom de l'intercommunalité de la commune siège de l'association.
    * @param populationTotale Population totale de la commune siège de l'association.
    * @param dateCreation Date de déclaration de création (date de dépôt du dossier en Préfecture).
    * @param dateDerniereDeclaration Date de la dernière déclaration.
    * @param datePublicationJOcreation Date de déclaration de création (date de dépôt du dossier en Préfecture).
    * @param dateDissolution Date de déclaration de dissolution de l’association.
    * @param nombreAnneesExistence Nombre d'années d'existence de l'association.
    * @param nombreAnneesDerniereDeclaration Nombre d'années depuis la dernière déclaration de l'association.
    */
   public Association(NumeroWaldec numeroWaldec, AncienNumeroWaldec ancienNumeroWaldec, SIRET siret, NumeroRUP numeroRUP, String gestion, 
         String nature, RegroupementAssociation groupement, String titre, String titreCourt, String objet, 
         String codeObjetSocial1, String codeObjetSocial2, Adresse adresseSiege, String nomDeclarant, Adresse adresseGestion, 
         String civiliteDeclarant, String siteWeb, boolean autorisationPublicationWeb, String observation, String positionActivite, 
         Instant majTime, String libelleObjetSocial1, String libelleObjetSocial2, CodeCommune codeCommune, String nomCommune, 
         CodeRegion codeRegion, CodeDepartement codeDepartement, SIRET codeEPCI, String nomEPCI, Integer populationTotale, 
         LocalDate dateCreation, LocalDate dateDerniereDeclaration, LocalDate datePublicationJOcreation, LocalDate dateDissolution, Double nombreAnneesExistence, 
         Double nombreAnneesDerniereDeclaration) {
      this.numeroWaldec = numeroWaldec;
      this.ancienNumeroWaldec = ancienNumeroWaldec;
      this.siret = siret;
      this.numeroRUP = numeroRUP;
      this.gestion = gestion;
      
      this.nature = nature;
      this.groupement = groupement;
      this.titre = titre;
      this.titreCourt = titreCourt;
      this.objet = objet;
      
      this.codeObjetSocial1 = codeObjetSocial1;
      this.codeObjetSocial2 = codeObjetSocial2;
      this.adresseSiege = adresseSiege;
      this.nomDeclarant = nomDeclarant;
      this.adresseGestion = adresseGestion;

      this.civiliteDeclarant = civiliteDeclarant;
      this.siteWeb = siteWeb;
      this.autorisationPublicationWeb = autorisationPublicationWeb;
      this.observation = observation;
      this.positionActivite = positionActivite;

      this.majTime = majTime;
      this.libelleObjetSocial1 = libelleObjetSocial1;
      this.libelleObjetSocial2 = libelleObjetSocial2;
      this.codeCommune = codeCommune;
      this.nomCommune = nomCommune;

      this.codeRegion = codeRegion;
      this.codeDepartement = codeDepartement;
      this.codeEPCI = codeEPCI;
      this.nomEPCI = nomEPCI;
      this.populationTotale = populationTotale;

      this.dateCreation = dateCreation;
      this.dateDerniereDeclaration = dateDerniereDeclaration;
      this.datePublicationJOcreation = datePublicationJOcreation;
      this.dateDissolution = dateDissolution;
      this.nombreAnneesExistence = nombreAnneesExistence;

      this.nombreAnneesDerniereDeclaration = nombreAnneesDerniereDeclaration;
   }
   
   /**
    * Renvoyer le numéro waldec de l'association.
    * @return Numéro waldec de l'association.
    */
   public NumeroWaldec getNumeroWaldec() {
      return this.numeroWaldec;
   }

   /**
    * Fixer le numéro waldec de l'association.
    * @param numeroWaldec Numéro waldec de l'association.
    */
   public void setNumeroWaldec(NumeroWaldec numeroWaldec) {
      this.numeroWaldec = numeroWaldec;
   }

   /**
    * Renvoyer l'ancien numéro waldec de l'association.
    * @return Ancien numéro waldec de l'association.
    */
   public AncienNumeroWaldec getAncienNumeroWaldec() {
      return this.ancienNumeroWaldec;
   }

   /**
    * Fixer l'ancien numéro waldec de l'association.
    * @param ancienNumeroWaldec Ancien numéro waldec de l'association.
    */
   public void setAncienNumeroWaldec(AncienNumeroWaldec ancienNumeroWaldec) {
      this.ancienNumeroWaldec = ancienNumeroWaldec;
   }

   /**
    * Renvoyer le siret de l'association.
    * @return SIRET.
    */
   public SIRET getSiret() {
      return this.siret;
   }

   /**
    * Fixer le siret de l'association.
    * @param siret SIRET.
    */
   public void setSiret(SIRET siret) {
      this.siret = siret;
   }

   /**
    * Renvoyer le numéro d'association reconnue d'utilité publique (RUP) attribué par le Ministère.
    * @return Numéro d'association reconnue d'utilité publique (RUP) attribué par le Ministère..
    */
   public NumeroRUP getNumeroRUP() {
      return this.numeroRUP;
   }

   /**
    * Fixer le numéro d'association reconnue d'utilité publique (RUP) attribué par le Ministère.
    * @param numeroRUP Numéro d'association reconnue d'utilité publique (RUP) attribué par le Ministère.
    */
   public void setNumeroRUP(NumeroRUP numeroRUP) {
      this.numeroRUP = numeroRUP;
   }

   /**
    * Renvoyer le Code du site gestionnaire de l’association
    * @return Code du site gestionnaire de l’association
    */
   public String getGestion() {
      return this.gestion;
   }

   /**
    * Fixer le code du site gestionnaire de l’association
    * @param gestion Code du site gestionnaire de l’association
    */
   public void setGestion(String gestion) {
      this.gestion = gestion;
   }

   /**
    * Renvoyer la nature de l'association : simplement déclarée 1901 ou autre.
    * @return Nature de l'association : simplement déclarée 1901 ou autre.
    */
   public String getNature() {
      return this.nature;
   }

   /**
    * Fixer la nature de l'association : simplement déclarée 1901 ou autre.
    * @param nature Nature de l'association : simplement déclarée 1901 ou autre.
    */
   public void setNature(String nature) {
      this.nature = nature;
   }

   /**
    * Renvoyer le regroupement de l'association : simple ou union ou fédération (S, U, F).
    * @return Regroupement de l'association : simple ou union ou fédération (S, U, F).
    */
   public RegroupementAssociation getGroupement() {
      return this.groupement;
   }

   /**
    * Fixer le egroupement de l'association : simple ou union ou fédération (S, U, F).
    * @param groupement Regroupement de l'association : simple ou union ou fédération (S, U, F).
    */
   public void setGroupement(RegroupementAssociation groupement) {
      this.groupement = groupement;
   }

   /**
    * Renvoyer le nom long de l'association.
    * @return Nom long de l'association.
    */
   public String getTitre() {
      return this.titre;
   }

   /**
    * Fixer le nom long de l'association.
    * @param titre Nom long de l'association.
    */
   public void setTitre(String titre) {
      this.titre = titre;
   }

   /**
    * Renvoyer le nom court de l'association (pour les courriers postaux).
    * @return Nom court de l'association (pour les courriers postaux).
    */
   public String getTitreCourt() {
      return this.titreCourt;
   }

   /**
    * Fixer le nom court de l'association (pour les courriers postaux).
    * @param titreCourt Nom court de l'association (pour les courriers postaux).
    */
   public void setTitreCourt(String titreCourt) {
      this.titreCourt = titreCourt;
   }

   /**
    * Renvoyer l'objet social de l'association.
    * @return Objet social de l'association.
    */
   public String getObjet() {
      return this.objet;
   }

   /**
    * Fixer l'objet social de l'association.
    * @param objet Objet social de l'association.
    */
   public void setObjet(String objet) {
      this.objet = objet;
   }

   /**
    * Renvoyer le code objet social obligatoire dans la nomenclature nationale.
    * @return Code objet social obligatoire dans la nomenclature nationale.
    */
   public String getCodeObjetSocial1() {
      return this.codeObjetSocial1;
   }

   /**
    * Fixer le code objet social obligatoire dans la nomenclature nationale.
    * @param codeObjetSocial1 Code objet social obligatoire dans la nomenclature nationale.
    */
   public void setCodeObjetSocial1(String codeObjetSocial1) {
      this.codeObjetSocial1 = codeObjetSocial1;
   }

   /**
    * Renvoyer le second code objet social (facultatif) dans la nomenclature nationale.
    * @return Second code objet social (facultatif) dans la nomenclature nationale.
    */
   public String getCodeObjetSocial2() {
      return this.codeObjetSocial2;
   }

   /**
    * Fixer le second code objet social (facultatif) dans la nomenclature nationale.
    * @param codeObjetSocial2 Second code objet social (facultatif) dans la nomenclature nationale.
    */
   public void setCodeObjetSocial2(String codeObjetSocial2) {
      this.codeObjetSocial2 = codeObjetSocial2;
   }

   /**
    * Renvoyer le nom du dirigeant principal de l'association.
    * @return Nom du dirigeant principal de l'association.
    */
   public String getNomDeclarant() {
      return this.nomDeclarant;
   }

   /**
    * Fixer le nom du dirigeant principal de l'association.
    * @param nomDeclarant Nom du dirigeant principal de l'association.
    */
   public void setNomDeclarant(String nomDeclarant) {
      this.nomDeclarant = nomDeclarant;
   }

   /**
    * Renvoyer la civilité du dirigeant principal de l'association.
    * @return Civilité du dirigeant principal de l'association.
    */
   public String getCiviliteDeclarant() {
      return this.civiliteDeclarant;
   }

   /**
    * Fixer la civilité du dirigeant principal de l'association.
    * @param civiliteDeclarant Civilité du dirigeant principal de l'association.
    */
   public void setCiviliteDeclarant(String civiliteDeclarant) {
      this.civiliteDeclarant = civiliteDeclarant;
   }

   /**
    * Renvoyer l'URL du site web de l'association.
    * @return URL du site web de l'association.
    */
   public String getSiteWeb() {
      return this.siteWeb;
   }

   /**
    * Fixer l'URL du site web de l'association.
    * @param siteWeb URL du site web de l'association.
    */
   public void setSiteWeb(String siteWeb) {
      this.siteWeb = siteWeb;
   }

   /**
    * Déterminer si la publication des informations de l'association sur le web est autorisée.
    * @return Autorisation de publication des informations de l'association sur le web.
    */
   public Boolean isAutorisationPublicationWeb() {
      return this.autorisationPublicationWeb;
   }

   /**
    * Indiquer si la publication des informations de l'association sur le web est autorisée.
    * @param autorisationPublicationWeb Autorisation de publication des informations de l'association sur le web.
    */
   public void setAutorisationPublicationWeb(boolean autorisationPublicationWeb) {
      this.autorisationPublicationWeb = autorisationPublicationWeb;
   }

   /**
    * Renvoyer l'observation de traitement.
    * @return Observation de traitement.
    */
   public String getObservation() {
      return this.observation;
   }

   /**
    * Fixer l'observation de traitement.
    * @param observation Observation de traitement.
    */
   public void setObservation(String observation) {
      this.observation = observation;
   }

   /**
    * Renvoyer la position d’activité de l’association (Active, Dissoute ou Supprimée).
    * @return Position d’activité de l’association (Active, Dissoute ou Supprimée).
    */
   public String getPositionActivite() {
      return this.positionActivite;
   }

   /**
    * Fixer la position d’activité de l’association (Active, Dissoute ou Supprimée).
    * @param positionActivite Position d’activité de l’association (Active, Dissoute ou Supprimée).
    */
   public void setPositionActivite(String positionActivite) {
      this.positionActivite = positionActivite;
   }

   /**
    * Renvoyer la date/heure de dernière mise à jour des informations de cette association.
    * @return Date/heure de dernière mise à jour des informations de cette association.
    */
   public Instant getMajTime() {
      return this.majTime;
   }

   /**
    * Fixer la date/heure de dernière mise à jour des informations de cette association.
    * @param majTime Date/heure de dernière mise à jour des informations de cette association.
    */
   public void setMajTime(Instant majTime) {
      this.majTime = majTime;
   }

   /**
    * Renvoyer le libellé de l'objet social obligatoire.
    * @return Libellé de l'objet social obligatoire.
    */
   public String getLibelleObjetSocial1() {
      return this.libelleObjetSocial1;
   }

   /**
    * Fixer le libellé de l'objet social obligatoire.
    * @param libelleObjetSocial1 Libellé de l'objet social obligatoire.
    */
   public void setLibelleObjetSocial1(String libelleObjetSocial1) {
      this.libelleObjetSocial1 = libelleObjetSocial1;
   }

   /**
    * Renvoyer le libellé de l'objet social facultatif.
    * @return Libellé de l'objet social facultatif.
    */
   public String getLibelleObjetSocial2() {
      return this.libelleObjetSocial2;
   }

   /**
    * Fixer le libellé de l'objet social facultatif.
    * @param libelleObjetSocial2 Libellé de l'objet social facultatif.
    */
   public void setLibelleObjetSocial2(String libelleObjetSocial2) {
      this.libelleObjetSocial2 = libelleObjetSocial2;
   }

   /**
    * Renvoyer le code commune du siège de l'association.
    * @return Code commune du siège de l'association.
    */
   public CodeCommune getCodeCommune() {
      return this.codeCommune;
   }

   /**
    * Fixer le code commune du siège de l'association.
    * @param codeCommune Code commune du siège de l'association.
    */
   public void setCodeCommune(CodeCommune codeCommune) {
      this.codeCommune = codeCommune;
   }

   /**
    * Renvoyer le nom de la commune siège de l'association.
    * @return Nom de la commune siège de l'association.
    */
   public String getNomCommune() {
      return this.nomCommune;
   }

   /**
    * Fixer le nom de la commune siège de l'association.
    * @param nomCommune Nom de la commune siège de l'association.
    */
   public void setNomCommune(String nomCommune) {
      this.nomCommune = nomCommune;
   }

   /**
    * Renvoyer le code de la région du siège de l'association.
    * @return Code de la région du siège de l'association.
    */
   public CodeRegion getCodeRegion() {
      return this.codeRegion;
   }

   /**
    * Fixer le code de la région du siège de l'association.
    * @param codeRegion Code de la région du siège de l'association.
    */
   public void setCodeRegion(CodeRegion codeRegion) {
      this.codeRegion = codeRegion;
   }

   /**
    * Renvoyer le code du département du siège de l'association.
    * @return Code du département du siège de l'association.
    */
   public CodeDepartement getCodeDepartement() {
      return this.codeDepartement;
   }

   /**
    * Fixer le code du département du siège de l'association.
    * @param codeDepartement Code du département du siège de l'association.
    */
   public void setCodeDepartement(CodeDepartement codeDepartement) {
      this.codeDepartement = codeDepartement;
   }

   /**
    * Renvoyer le code EPCI de l'intercommunalité de la commune siège de l'association.
    * @return Code EPCI de l'intercommunalité de la commune siège de l'association.
    */
   public SIRET getCodeEPCI() {
      return this.codeEPCI;
   }

   /**
    * Fixer le code EPCI de l'intercommunalité de la commune siège de l'association.
    * @param codeEPCI Code EPCI de l'intercommunalité de la commune siège de l'association.
    */
   public void setCodeEPCI(SIRET codeEPCI) {
      this.codeEPCI = codeEPCI;
   }

   /**
    * Renvoyer le nom de l'intercommunalité de la commune siège de l'association.
    * @return Nom de l'intercommunalité de la commune siège de l'association.
    */
   public String getNomEPCI() {
      return this.nomEPCI;
   }

   /**
    * Fixer le nom de l'intercommunalité de la commune siège de l'association.
    * @param nomEPCI Nom de l'intercommunalité de la commune siège de l'association.
    */
   public void setNomEPCI(String nomEPCI) {
      this.nomEPCI = nomEPCI;
   }

   /**
    * Renvoyer la population totale de la commune siège de l'association.
    * @return Population totale de la commune siège de l'association.
    */
   public Integer getPopulationTotale() {
      return this.populationTotale;
   }

   /**
    * Fixer la population totale de la commune siège de l'association.
    * @param populationTotale Population totale de la commune siège de l'association.
    */
   public void setPopulationTotale(Integer populationTotale) {
      this.populationTotale = populationTotale;
   }

   /**
    * Renvoyer la date de déclaration de création (date de dépôt du dossier en Préfecture).
    * @return Date de déclaration de création (date de dépôt du dossier en Préfecture).
    */
   public LocalDate getDateCreation() {
      return this.dateCreation;
   }

   /**
    * Fixer la date de déclaration de création (date de dépôt du dossier en Préfecture).
    * @param dateCreation Date de déclaration de création (date de dépôt du dossier en Préfecture).
    */
   public void setDateCreation(LocalDate dateCreation) {
      this.dateCreation = dateCreation;
   }

   /**
    * Renvoyer la date de la dernière déclaration.
    * @return Date de la dernière déclaration.
    */
   public LocalDate getDateDerniereDeclaration() {
      return this.dateDerniereDeclaration;
   }

   /**
    * Fixer la date de la dernière déclaration.
    * @param dateDerniereDeclaration Date de la dernière déclaration.
    */
   public void setDateDerniereDeclaration(LocalDate dateDerniereDeclaration) {
      this.dateDerniereDeclaration = dateDerniereDeclaration;
   }

   /**
    * Renvoyer la date de publication JO de l’avis de création.
    * @return Date de publication JO de l’avis de création.
    */
   public LocalDate getDatePublicationJOcreation() {
      return this.datePublicationJOcreation;
   }

   /**
    * Fixer la date de publication JO de l’avis de création.
    * @param datePublicationJOcreation Date de publication JO de l’avis de création.
    */
   public void setDatePublicationJOcreation(LocalDate datePublicationJOcreation) {
      this.datePublicationJOcreation = datePublicationJOcreation;
   }

   /**
    * Renvoyer la date de déclaration de dissolution de l’association.
    * @return Date de déclaration de dissolution de l’association.
    */
   public LocalDate getDateDissolution() {
      return this.dateDissolution;
   }

   /**
    * Fixer la date de déclaration de dissolution de l’association.
    * @param dateDissolution Date de déclaration de dissolution de l’association.
    */
   public void setDateDissolution(LocalDate dateDissolution) {
      this.dateDissolution = dateDissolution;
   }

   /**
    * Renvoyer le nombre d'années d'existence de l'association.
    * @return Nombre d'années d'existence de l'association.
    */
   public Double getNombreAnneesExistence() {
      return this.nombreAnneesExistence;
   }

   /**
    * Fixer le nombre d'années d'existence de l'association.
    * @param nombreAnneesExistence Nombre d'années d'existence de l'association.
    */
   public void setNombreAnneesExistence(Double nombreAnneesExistence) {
      this.nombreAnneesExistence = nombreAnneesExistence;
   }

   /**
    * Renvoyer le nombre d'années depuis la dernière déclaration de l'association.
    * @return Nombre d'années depuis la dernière déclaration de l'association.
    */
   public Double getNombreAnneesDerniereDeclaration() {
      return this.nombreAnneesDerniereDeclaration;
   }

   /**
    * Fixer le nombre d'années depuis la dernière déclaration de l'association.
    * @param nombreAnneesDerniereDeclaration Nombre d'années depuis la dernière déclaration de l'association.
    */
   public void setNombreAnneesDerniereDeclaration(Double nombreAnneesDerniereDeclaration) {
      this.nombreAnneesDerniereDeclaration = nombreAnneesDerniereDeclaration;
   }

   /**
    * Renvoyer l'adresse du siège de l'association. 
    * @return Adresse du siège de l'association.
    */
   public Adresse getAdresseSiege() {
      return this.adresseSiege;
   }

   /**
    * Fixer l'adresse du siège de l'association.
    * @param adresseSiege Adresse du siège de l'association. 
    */
   public void setAdresseSiege(Adresse adresseSiege) {
      this.adresseSiege = adresseSiege;
   }

   /**
    * Renvoyer l'adresse de gestion de l'association.
    * @return Adresse de gestion de l'association.
    */
   public Adresse getAdresseGestion() {
      return this.adresseGestion;
   }

   /**
    * Fixer l'adresse de gestion de l'association.
    * @param adresseGestion Adresse de gestion de l'association.
    */
   public void setAdresseGestion(Adresse adresseGestion) {
      this.adresseGestion = adresseGestion;
   }
   
   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      return ReflectionToStringBuilder.toString(this, ToStringStyle.DEFAULT_STYLE);
   }
}
