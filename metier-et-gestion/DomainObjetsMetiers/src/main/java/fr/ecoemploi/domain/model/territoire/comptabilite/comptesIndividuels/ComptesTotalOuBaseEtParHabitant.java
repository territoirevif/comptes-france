package fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels;

import java.io.*;
import java.util.*;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Forme de comptes individuels : comptes total ou base, et par habitant.
 * @author Marc Le Bihan
 */
@Schema(description = "Forme de comptes individuels : comptes total ou base, et par habitant",
   discriminatorProperty = "typeCompteIndividuel")
public class ComptesTotalOuBaseEtParHabitant implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -6505708589401923644L;

   /** Type de compte Individuel */
   @Schema(description = "Type de compte individuel", requiredMode = Schema.RequiredMode.REQUIRED)
   private String typeCompteIndividuel = getClass().getSimpleName();

   /** Montant total ou base */
   @Schema(description = "Montant total ou base")
   private Double montant;

   /** Montant par habitant */
   @Schema(description = "Montant par habitant")
   private Double parHabitant;

   /** Libellé de l'analyse apportée par cette ligne de comptes individuels. */
   @Schema(description = "Libellé de l'analyse apportée par cette ligne de comptes individuels")
   private String libelleAnalyse;

   /**
    * Construire des comptes individuels.
    */
   public ComptesTotalOuBaseEtParHabitant() {
   }

   /**
    * Renvoyer le type de compte Individuel
    * @return Type de compte Individuel
    */
   public String getTypeCompteIndividuel() {
      return this.typeCompteIndividuel;
   }

   /**
    * Fixer le type de compte Individuel
    * @param typeCompteIndividuel Type de compte Individuel
    */
   public void setTypeCompteIndividuel(String typeCompteIndividuel) {
      this.typeCompteIndividuel = typeCompteIndividuel;
   }

   /**
    * Construire des comptes individuels.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    */
   public ComptesTotalOuBaseEtParHabitant(String libelleAnalyse) {
      this.libelleAnalyse = libelleAnalyse;
   }

   /**
    * Construire des comptes individuels.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    * @param montant Montant (total ou base).
    * @param parHabitant Montant par habitant.
    */
   public ComptesTotalOuBaseEtParHabitant(String libelleAnalyse, Double montant, Double parHabitant) {
      this(libelleAnalyse);
      this.montant = montant;
      this.parHabitant = parHabitant;
   }
   
   /**
    * Renvoyer le libellé de l'analyse apportée par cette ligne de comptes individuels.
    * @return Libellé de l'analyse.
    */
   public String getLibelleAnalyse() {
      return this.libelleAnalyse;
   }

   /**
    * Fixer le libellé de l'analyse apportée par cette ligne de comptes individuels.
    * @param libelleAnalyse Libellé de l'analyse.
    */
   public void setLibelleAnalyse(String libelleAnalyse) {
      this.libelleAnalyse = libelleAnalyse;
   }

   /**
    * Renvoyer le montant total ou base.
    * @return Montant.
    */
   public Double getMontant() {
      return this.montant;
   }

   /**
    * Fixer le montant total ou base.
    * @param montant Montant. 
    */
   public void setMontant(Double montant) {
      this.montant = montant;
   }

   /**
    * Renvoyer le montant par habitant.
    * @return Montant par habitant.
    */
   public Double getParHabitant() {
      return this.parHabitant;
   }

   /**
    * Fixer le montant par habitant.
    * @param parHabitant Montant par habitant.
    */
   public void setParHabitant(Double parHabitant) {
      this.parHabitant = parHabitant;
   }
   
   /**
    * Formatter une ligne texte pour qu'elle se place dans un tableau textuel d'analyse des équilibres financiers fondamentaux.
    * @return Ligne formatée. 
    */
   public String toStringAnalyseEquilibresFinanciersFondamentaux() {
      final String FMT_NOMBRE_ENTIER = "%10d\t";
      StringBuilder format = new StringBuilder();
      List<Object> valeurs = new ArrayList<>();

      format.append(getMontant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getMontant() != null ? Math.round(getMontant()) : "");

      format.append(getParHabitant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getParHabitant() != null ? Math.round(getParHabitant()) : "");

      format.append("\t");

      format.append("%s");
      valeurs.add(getLibelleAnalyse());

      return String.format(format.toString(), valeurs.toArray());
   }
}
