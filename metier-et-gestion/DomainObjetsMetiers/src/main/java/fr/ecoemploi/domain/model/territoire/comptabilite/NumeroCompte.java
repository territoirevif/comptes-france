package fr.ecoemploi.domain.model.territoire.comptabilite;

import java.io.Serial;

import fr.ecoemploi.domain.utils.autocontrole.Id;

/**
 * Un numéro de compte de comptabilité.
 * @author Marc LE BIHAN
 */
public class NumeroCompte extends Id {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -1274309029957385979L;
   
   /**
    * Consturire un numéro de compte vierge.
    */
   public NumeroCompte() {
      super();
   }
   
   /**
    * Construire un numéro de compte.
    * @param numeroCompte Numéro de compte.
    */
   public NumeroCompte(String numeroCompte) {
      super(numeroCompte);
   }
}
