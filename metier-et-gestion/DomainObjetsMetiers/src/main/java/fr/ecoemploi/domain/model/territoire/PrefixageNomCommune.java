package fr.ecoemploi.domain.model.territoire;

/**
 * Pour le formattage de nom de commune, indique dans quel but on va employer cette commune :
 * (Bienvenue à Ermainville, Voici les évènements pour les Ullis). 
 * @author Marc LE BIHAN
 */
public enum PrefixageNomCommune {
   /** Pas de préfixage. */
   AUCUN,

   /** à */
   A,

   /** pour */
   POUR
}
