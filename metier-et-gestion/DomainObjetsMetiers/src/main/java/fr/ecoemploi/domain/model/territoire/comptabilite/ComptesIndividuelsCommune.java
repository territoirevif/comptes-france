package fr.ecoemploi.domain.model.territoire.comptabilite;

import fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels.*;

import java.io.Serial;
import java.time.format.DateTimeFormatter;

/**
 * Comptes individuels d'une commune.
 * @author Marc Le Bihan
 */
public class ComptesIndividuelsCommune extends ComptesIndividuels<ComptesAvecMoyenneStrate, ComptesAvecStrateEtRatioStructureEtStrate, ComptesAvecStrateEtReductionVotee, ComptesAvecStrateEtTauxVoteEtStrate> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5433746504583682924L;

   /** Code de la région */
   private String codeRegion;
   
   /** Code du département */
   private String codeDepartement;

   /** Type groupement d'appartenance */
   private String typeGroupementAppartenance;
   
   /** Libellé de la strate communale. */
   private String libelleStrateCommune;

   /** CHARGES DE FONCTIONNEMENT = B : CAF : Contingents */
   private ComptesAvecStrateEtRatioStructureEtStrate chargesDeFonctionnementCAFContingents;

   /** RESSOURCES D'INVESTISSEMENT : Retour de biens affectés concédés ... */
   private ComptesAvecStrateEtRatioStructureEtStrate ressourcesInvestissementRetourDeBiensAffectesConcedes;

   /** EMPLOIS D'INVESTISSEMENT : Charges à répartir */
   private ComptesAvecStrateEtRatioStructureEtStrate emploisInvestissementChargesARepartir;

   /** EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ... */
   private ComptesAvecStrateEtRatioStructureEtStrate emploisInvestissementImmobilisationsAffecteesConcedees;

   /** AUTOFINANCEMENT : Excédent brut de fonctionnement */
   private ComptesAvecStrateEtRatioStructureEtStrate autoFinancementExcedentBrutDeFonctionnement;

   /** ENDETTEMENT : FONDS DE ROULEMENT */
   private ComptesAvecMoyenneStrate endettementFondsDeRoulement;
   
   /** OPERATIONS D'INVESTISSEMENT : Besoin ou capacité de financement résiduel de la section d'investissement = D - C */ 
   private ComptesAvecMoyenneStrate operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissement;

   /** OPERATIONS D'INVESTISSEMENT : Solde des opérations pour le compte de tiers */
   private ComptesAvecMoyenneStrate operationsInvestissementSoldeDesOperationsPourCompteDeTiers;

   /** OPERATIONS D'INVESTISSEMENT : Besoin ou capacité de financement de la section d'investissement = E */ 
   private ComptesAvecMoyenneStrate operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissement;

   /** OPERATIONS D'INVESTISSEMENT : Résultat d'ensemble = R - E */
   private ComptesAvecMoyenneStrate operationsInvestissementResultatEnsemble;
   
   /**
    * Construire des comptes individuels de commune.
    */
   public ComptesIndividuelsCommune() {
   }
   
   /**
    * Construire des comptes individuels de commune. 
    * @param comptesIndividuels Comptes individuels de la collectivité locale qui sont communs à toutes les collectivités locales.
    * @param codeRegion Code de la région
    * @param codeDepartement Code du département
    * @param typeGroupementAppartenance Type groupement d'appartenance
    * @param strateCommune Strate commune.
    * @param chargesDeFonctionnementCAFContingents CHARGES DE FONCTIONNEMENT = B : CAF : Contingents
    * @param ressourcesInvestissementRetourDeBiensAffectesConcedes RESSOURCES D'INVESTISSEMENT : Retour de biens affectés concédés...
    * @param emploisInvestissementChargesARepartir EMPLOIS D'INVESTISSEMENT : Charges à répartir
    * @param emploisInvestissementImmobilisationsAffecteesConcedees EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ...
    * @param operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissement OPERATIONS D'INVESTISSEMENT : Besoin ou capacité de financement résiduel de la section d'investissement = D - C
    * @param operationsInvestissementSoldeDesOperationsPourCompteDeTiers OPERATIONS D'INVESTISSEMENT : Solde des opérations pour le compte de tiers
    * @param operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissement OPERATIONS D'INVESTISSEMENT : Besoin ou capacité de financement de la section d'investissement = E
    * @param operationsInvestissementResultatEnsemble OPERATIONS D'INVESTISSEMENT : Résultat d'ensemble = R - E
    * @param autoFinancementExcedentBrutDeFonctionnement AUTOFINANCEMENT : Excédent brut de fonctionnement
    * @param endettementFondsDeRoulement ENDETTEMENT : FONDS DE ROULEMENT
    */
   public ComptesIndividuelsCommune(ComptesIndividuels<ComptesAvecMoyenneStrate, ComptesAvecStrateEtRatioStructureEtStrate, ComptesAvecStrateEtReductionVotee, ComptesAvecStrateEtTauxVoteEtStrate> comptesIndividuels, String codeRegion, String codeDepartement, String typeGroupementAppartenance, String strateCommune,
      ComptesAvecStrateEtRatioStructureEtStrate ressourcesInvestissementRetourDeBiensAffectesConcedes,
      ComptesAvecStrateEtRatioStructureEtStrate chargesDeFonctionnementCAFContingents,
      ComptesAvecStrateEtRatioStructureEtStrate emploisInvestissementChargesARepartir, ComptesAvecStrateEtRatioStructureEtStrate emploisInvestissementImmobilisationsAffecteesConcedees,
      ComptesAvecStrateEtRatioStructureEtStrate autoFinancementExcedentBrutDeFonctionnement,  
      ComptesAvecMoyenneStrate endettementFondsDeRoulement,
      ComptesAvecMoyenneStrate operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissement, 
      ComptesAvecMoyenneStrate operationsInvestissementSoldeDesOperationsPourCompteDeTiers, 
      ComptesAvecMoyenneStrate operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissement, 
      ComptesAvecMoyenneStrate operationsInvestissementResultatEnsemble) {
      super(comptesIndividuels);
      this.codeRegion = codeRegion;
      this.codeDepartement = codeDepartement;
      this.typeGroupementAppartenance = typeGroupementAppartenance;
      this.libelleStrateCommune = strateCommune;

      this.chargesDeFonctionnementCAFContingents = chargesDeFonctionnementCAFContingents;
      
      this.ressourcesInvestissementRetourDeBiensAffectesConcedes = ressourcesInvestissementRetourDeBiensAffectesConcedes;
      
      this.emploisInvestissementChargesARepartir = emploisInvestissementChargesARepartir;
      this.emploisInvestissementImmobilisationsAffecteesConcedees = emploisInvestissementImmobilisationsAffecteesConcedees;
      
      this.operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissement = operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissement;
      this.operationsInvestissementSoldeDesOperationsPourCompteDeTiers = operationsInvestissementSoldeDesOperationsPourCompteDeTiers;
      this.operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissement = operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissement;
      this.operationsInvestissementResultatEnsemble = operationsInvestissementResultatEnsemble;
      
      this.autoFinancementExcedentBrutDeFonctionnement = autoFinancementExcedentBrutDeFonctionnement;
      
      this.endettementFondsDeRoulement = endettementFondsDeRoulement;
   }

   /**
    * Renvoyer le code du département.
    * @return Code du département.
    */
   public String getCodeDepartement() {
      return this.codeDepartement;
   }

   /**
    * Fixer le code du département.
    * @param codeDepartement Code du département.
    */
   public void setCodeDepartement(String codeDepartement) {
      this.codeDepartement = codeDepartement;
   }

   /**
    * Renvoyer le code de la région.
    * @return Code de la région.
    */
   public String getCodeRegion() {
      return this.codeRegion;
   }

   /**
    * Fixer le code de la région.
    * @param codeRegion Code de la région.
    */
   public void setCodeRegion(String codeRegion) {
      this.codeRegion = codeRegion;
   }

   /**
    * Renvoyer la population légale en vigueur au 1er janvier de l'exercice.
    * @return Population légale en vigueur au 1er janvier de l'exercice.
    */
   public Integer getPopulationCommune() {
      return super.getPopulation();
   }

   /**
    * Fixer la population légale en vigueur au 1er janvier de l'exercice.
    * @param populationCommune Population légale en vigueur au 1er janvier de l'exercice. 
    */
   public void setPopulationCommune(Integer populationCommune) {
      super.setPopulation(populationCommune);
   }

   /**
    * Renvoyer le type groupement d'appartenance.
    * @return Type groupement d'appartenance.
    */
   public String getTypeGroupementAppartenance() {
      return this.typeGroupementAppartenance;
   }

   /**
    * Fixer le type groupement d'appartenance.
    * @param typeGroupementAppartenance Type groupement d'appartenance.
    */
   public void setTypeGroupementAppartenance(String typeGroupementAppartenance) {
      this.typeGroupementAppartenance = typeGroupementAppartenance;
   }

   /**
    * Renvoyer le siren de la commune.
    * @return Siren de la commune.
    */
   public String getSirenCommune() {
      return getSiren();
   }

   /**
    * Fixer le siren de la commune.
    * @param siren Siren de la commune.
    */
   public void setSirenCommune(String siren) {
      setSiren(siren);
   }

   /**
    * Renvoyer le code de la commune.
    * @return Code de la commune.
    */
   public String getCodeCommune() {
      return getCode();
   }

   /**
    * Fixer le code de la commune.
    * @param codeCommune Code de la commune.
    */
   public void setCodeCommune(String codeCommune) {
      setCode(codeCommune);
   }

   /**
    * Renvoyer le nom de la commune.
    * @return Nom de la commune.
    */
   public String getNomCommune() {
      return getNom();
   }

   /**
    * Fixer le nom de la commune.
    * @param nomCommune Nom de la commune.
    */
   public void setNomCommune(String nomCommune) {
      setNom(nomCommune);
   }

   /**
    * Renvoyer le libellé de la strate de la commune.
    * @return Libellé de la strate de la commune.
    */
   public String getLibelleStrateCommune() {
      return this.libelleStrateCommune;
   }

   /**
    * Fixer le libellé de la strate de la commune.
    * @param strateCommune Libellé de la strate de la commune.
    */
   public void setLibelleStrateCommune(String strateCommune) {
      this.libelleStrateCommune = strateCommune;
   }
   
   /**
    * Renvoyer les CHARGES DE FONCTIONNEMENT = B : CAF : Contingents
    * @return Comptes associés.
    */
   public ComptesAvecStrateEtRatioStructureEtStrate getChargesDeFonctionnementCAFContingents() {
      return this.chargesDeFonctionnementCAFContingents;
   }

   /**
    * Fixer les CHARGES DE FONCTIONNEMENT = B : CAF : Contingents
    * @param comptes Comptes associés.
    */
   public void setChargesDeFonctionnementCAFContingents(ComptesAvecStrateEtRatioStructureEtStrate comptes) {
      this.chargesDeFonctionnementCAFContingents = comptes;
   }

   /**
    * Renvoyer le Retour de biens affectés concédés... (RESSOURCES D'INVESTISSEMENT)
    * @return Retour de biens affectés concédés...
    */
   public ComptesAvecStrateEtRatioStructureEtStrate getRessourcesInvestissementRetourDeBiensAffectesConcedes() {
      return this.ressourcesInvestissementRetourDeBiensAffectesConcedes;
   }

   /**
    * Fixer le Retour de biens affectés concédés... (RESSOURCES D'INVESTISSEMENT)
    * @param comptes Retour de biens affectés concédés...
    */
   public void setRessourcesInvestissementRetourDeBiensAffectesConcedes(ComptesAvecStrateEtRatioStructureEtStrate comptes) {
      this.ressourcesInvestissementRetourDeBiensAffectesConcedes = comptes;
   }

   /**
    * Renvoyer les EMPLOIS D'INVESTISSEMENT : Charges à répartir. 
    * @return EMPLOIS D'INVESTISSEMENT : Charges à répartir.
    */
   public ComptesAvecStrateEtRatioStructureEtStrate getEmploisInvestissementChargesARepartir() {
      return this.emploisInvestissementChargesARepartir;
   }

   /**
    * Fixer les EMPLOIS D'INVESTISSEMENT : Charges à répartir. 
    * @param comptes EMPLOIS D'INVESTISSEMENT : Charges à répartir.
    */
   public void setEmploisInvestissementChargesARepartir(ComptesAvecStrateEtRatioStructureEtStrate comptes) {
      this.emploisInvestissementChargesARepartir = comptes;
   }

   /**
    * Renvoyer les EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ...
    * @return EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ...
    */
   public ComptesAvecStrateEtRatioStructureEtStrate getEmploisInvestissementImmobilisationsAffecteesConcedees() {
      return this.emploisInvestissementImmobilisationsAffecteesConcedees;
   }

   /**
    * Fixer les EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ... 
    * @param comptes EMPLOIS D'INVESTISSEMENT : Immobilisations affectées, concédées, ...
    */
   public void setEmploisInvestissementImmobilisationsAffecteesConcedees(ComptesAvecStrateEtRatioStructureEtStrate comptes) {
      this.emploisInvestissementImmobilisationsAffecteesConcedees = comptes;
   }

   /**
    * Renvoyer le OPERATIONS D'INVESTISSEMENT : Besoin ou capacité de financement résiduel de la section d'investissement = D - C. 
    * @return OPERATIONS D'INVESTISSEMENT : Besoin ou capacité de financement résiduel de la section d'investissement = D - C.
    */
   public ComptesAvecMoyenneStrate getOperationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissement() {
      return this.operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissement;
   }

   /**
    * Fixer le OPERATIONS D'INVESTISSEMENT : Besoin ou capacité de financement résiduel de la section d'investissement = D - C.
    * @param comptes OPERATIONS D'INVESTISSEMENT : Besoin ou capacité de financement résiduel de la section d'investissement = D - C.
    */
   public void setOperationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissement(ComptesAvecMoyenneStrate comptes) {
      this.operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissement = comptes;
   }

   /**
    * Renvoyer le OPERATIONS D'INVESTISSEMENT : Solde des opérations pour le compte de tiers. 
    * @return OPERATIONS D'INVESTISSEMENT : Solde des opérations pour le compte de tiers.
    */
   public ComptesAvecMoyenneStrate getOperationsInvestissementSoldeDesOperationsPourCompteDeTiers() {
      return this.operationsInvestissementSoldeDesOperationsPourCompteDeTiers;
   }

   /**
    * Fixer le OPERATIONS D'INVESTISSEMENT : Solde des opérations pour le compte de tiers.
    * @param comptes OPERATIONS D'INVESTISSEMENT : Solde des opérations pour le compte de tiers.
    */
   public void setOperationsInvestissementSoldeDesOperationsPourCompteDeTiers(ComptesAvecMoyenneStrate comptes) {
      this.operationsInvestissementSoldeDesOperationsPourCompteDeTiers = comptes;
   }

   /**
    * Renvoyer les OPERATIONS D'INVESTISSEMENT : Besoin ou capacité de financement de la section d'investissement = E. 
    * @return OPERATIONS D'INVESTISSEMENT : Besoin ou capacité de financement de la section d'investissement = E.
    */
   public ComptesAvecMoyenneStrate getOperationsInvestissementBesoinOuCapaciteFinancementSectionInvestissement() {
      return this.operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissement;
   }

   /**
    * Fixer les OPERATIONS D'INVESTISSEMENT : Besoin ou capacité de financement de la section d'investissement = E.
    * @param comptes OPERATIONS D'INVESTISSEMENT : Besoin ou capacité de financement de la section d'investissement = E.
    */
   public void setOperationsInvestissementBesoinOuCapaciteFinancementSectionInvestissement(ComptesAvecMoyenneStrate comptes) {
      this.operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissement = comptes;
   }

   /**
    * Renvoyer les OPERATIONS D'INVESTISSEMENT : Résultat d'ensemble = R - E. 
    * @return OPERATIONS D'INVESTISSEMENT : Résultat d'ensemble = R - E.
    */
   public ComptesAvecMoyenneStrate getOperationsInvestissementResultatEnsemble() {
      return this.operationsInvestissementResultatEnsemble;
   }

   /**
    * Fixer les OPERATIONS D'INVESTISSEMENT : Résultat d'ensemble = R - E. 
    * @param comptes OPERATIONS D'INVESTISSEMENT : Résultat d'ensemble = R - E.
    */
   public void setOperationsInvestissementResultatEnsemble(ComptesAvecMoyenneStrate comptes) {
      this.operationsInvestissementResultatEnsemble = comptes;
   }

   /**
    * Renvoyer l'AUTOFINANCEMENT : Excédent brut de fonctionnement. 
    * @return AUTOFINANCEMENT : Excédent brut de fonctionnement.
    */
   public ComptesAvecStrateEtRatioStructureEtStrate getAutoFinancementExcedentBrutDeFonctionnement() {
      return this.autoFinancementExcedentBrutDeFonctionnement;
   }

   /**
    * Fixer l'AUTOFINANCEMENT : Excédent brut de fonctionnement.
    * @param comptes AUTOFINANCEMENT : Excédent brut de fonctionnement.
    */
   public void setAutoFinancementExcedentBrutDeFonctionnement(ComptesAvecStrateEtRatioStructureEtStrate comptes) {
      this.autoFinancementExcedentBrutDeFonctionnement = comptes;
   }

   /**
    * Renvoyer l'ENDETTEMENT : FONDS DE ROULEMENT. 
    * @return ENDETTEMENT : FONDS DE ROULEMENT..
    */
   public ComptesAvecMoyenneStrate getEndettementFondsDeRoulement() {
      return this.endettementFondsDeRoulement;
   }

   /**
    * Fixer l'ENDETTEMENT : FONDS DE ROULEMENT. 
    * @param comptes ENDETTEMENT : FONDS DE ROULEMENT.
    */
   public void setEndettementFondsDeRoulement(ComptesAvecMoyenneStrate comptes) {
      this.endettementFondsDeRoulement = comptes;
   }
   
/*
En milliers d'Euros  Euros par habitant   Moyenne de la strate OPERATIONS DE FONCTIONNEMENT  Ratios de structure  Moyenne de la strate (en % des produits)

     21442        1477        1354  TOTAL DES PRODUITS DE FONCTIONNEMENT = A
     21240        1463        1303     Produits de fonctionnement CAF
     12379         853         563        dont : Impôts Locaux            58,28       43,19
      1418          98         115        Autres impôts et taxes                nu        8,82
      1794         124         177        Dotation globale de fonctionnement        8,44       13,57


     20000        1377        1213  TOTAL DES CHARGES DE FONCTIONNEMENT = B
     18156        1250        1107     Charges de fonctionnement CAF
     11226         773         655        dont : Charges de personnel           61,83       59,18
      4493         309         272        achats et charges externes         24,75       24,58
       911          63          24        Charges financières              5,02        2,14
       229          16          33        Contingents                1,26        2,98
      1024          71          89        Subventions versées              5,64        8,02
      1442          99         141  Résultat comptable = A - B = R

                  OPERATIONS D'INVESTISSEMENT

      7099         489         482  TOTAL DES RESSOURCES D'INVESTISSEMENT = C
      1800         124          79     dont : Emprunts bancaires et dettes assimilées       25,36       16,33
       526          36          72     Subventions reçues                  7,42       14,84
       450          31          40     Fonds de compensation de la TVA (FCTVA)            6,34        8,28
         0           0           0     Retour de biens affectés, concédés, ...            0,00        0,00

      6481         446         500  TOTAL DES EMPLOIS D'INVESTISSEMENT = D
      4515         311         376     dont : Dépenses d'équipement             69,66       75,37
      1926         133          86     Remboursement d'emprunts et dettes assimilées        29,72       17,18
         0           0           1     Charges à répartir                  0,00        0,11
         0           0           0     Immobilisations affectées, concédées, ...       0,00        0,00

      -618         -43          17  Besoin ou capacité de financement résiduel de la section d'investissement = D - C
         0           0           1  + Solde des opérations pour le compte de tiers
      -617         -43          18  Besoin ou capacité de financement de la section d'investissement = E
      2059         142         123  Résultat d'ensemble = R - E

                  AUTOFINANCEMENT

      3855         266         214  Excédent brut de fonctionnement                18,15       16,43
      3084         212         196  Capacité d'autofinancement = CAF            14,52       15,01
      1158          80         110  CAF nette du remboursement en capital des emprunts       5,45        8,43

                  ENDETTEMENT
     25092        1728         850  Encours total de la dette au 31 décembre N          118,13       65,23
     25092        1728         118  Encours des dettes bancaires et assimilées          837,34       64,27
     25064        1726         118  Encours des dettes bancaires net de l'aide du fonds de soutien pour la sortie des emprunts toxiques       827,62       63,52
      2837         195         108  Annuité de la dette                   13,36        8,28
      1516         104         286  FONDS DE ROULEMENT


                  ELEMENTS DE FISCALITE DIRECTE LOCALE
Les bases imposées et les réductions (exonérations, abattements) accordées sur délibérations
Bases nettes imposées au profit de la commune               Taxe     Réductions de base accordées sur délibérations
En milliers d'Euros  Euros par habitant         Moyenne de la strate          En milliers d'Euros     Euros par habitant   Moyenne de la strate
     25249        1739        1441  Taxe d'habitation (y compris THLV)           2142,17      147,53      116,45
     23211        1599        1384  Taxe foncière sur les propriétés bâties               0,00        0,00        0,86
       177          12          11  Taxe foncière sur les propriétés non bâties           0,00        0,00        0,02
         0           0           0  Taxe additionnelle à la taxe foncière sur les propriétés non bâties
         0           0           0  Cotisation foncière des entreprises             0,00        0,00        0,00

Les taux et les produits de la fiscalité directe locale
Produits des impôts locaux                Taxe                                Taux voté (%)  Taux moyen de la strate (%)
      6133         422         240  Taxe d'habitation (y compris THLV)             24,27       16,66
      6135         423         313  Taxe foncière sur les propriétés bâties              26,38       22,58
       116           8           6  Taxe foncière sur les propriétés non bâties          65,39       53,69
         0           0           0  Taxe additionnelle à la taxe foncière sur les propriétés non bâties           0,00        0,00
         0           0           0  Cotisation foncière des entreprises             0,00        0,00

Les produits des impôts de répartition
Produits des impôts de répartition              Taxe
         0           0           0  Cotisation sur la valeur ajoutée des entreprises         0,00        0,00
         0           0           0  Imposition forfaitaire sur les entreprises de réseau
         0           0           0  Taxe sur les surfaces commerciales
 */
   
   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      String[] analyse = {
         toStringAnalyseEquilibresFinanciersFondamentaux("\nLibellé du budget : %s", getNomCommune()),
         toStringAnalyseEquilibresFinanciersFondamentaux("Population légale en vigueur au 1er janvier de l'exercice : %d\tBudget principal seul", getPopulationCommune()),
         toStringAnalyseEquilibresFinanciersFondamentaux("Strate : Strate commune : %s\t\tType groupement d'appartenance : %s", this.libelleStrateCommune, this.typeGroupementAppartenance),
         toStringAnalyseEquilibresFinanciersFondamentaux("Exercice : %s\t\tN° de département : %s\t\tN° insee de la commune : %s\t\tN° de région : %s", DateTimeFormatter.ofPattern("dd/MM/yyyy").format(getExercice()), getCodeDepartement(), getCodeCommune(), getCodeRegion()),
         toStringAnalyseEquilibresFinanciersFondamentaux("\n\nANALYSE DES EQUILIBRES FINANCIERS FONDAMENTAUX\n\nEn milliers d'Euros\tEuros par habitant\tMoyenne de la strate\tOPERATIONS DE FONCTIONNEMENT\tRatios de structure\tMoyenne de la strate (en %% des produits)\n"),

         toStringAnalyseEquilibresFinanciersFondamentaux("\n\t\t\t\t\t\tOPERATIONS DE FONCTIONNEMENT\n"),
         getProduitsDeFonctionnement().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getProduitsDeFonctionnementCAF().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getProduitsDeFonctionnementCAFImpotsLocaux().toStringAnalyseEquilibresFinanciersFondamentaux(), 
         getProduitsDeFonctionnementCAFAutresImpotsEtTaxes().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getProduitsDeFonctionnementCAFDGF().toStringAnalyseEquilibresFinanciersFondamentaux(),
         
         toStringAnalyseEquilibresFinanciersFondamentaux(""),

         getChargesDeFonctionnement().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getChargesDeFonctionnementCAF().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getChargesDeFonctionnementCAFChargesDePersonnel().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getChargesDeFonctionnementCAFAchatsEtChargesExternes().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getChargesDeFonctionnementCAFChargesFinancieres().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getChargesDeFonctionnementCAFContingents().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getChargesDeFonctionnementCAFSubventionsVersees().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getResultatComptable().toStringAnalyseEquilibresFinanciersFondamentaux(),
         
         toStringAnalyseEquilibresFinanciersFondamentaux("\n\n\t\t\t\t\t\tOPERATIONS D'INVESTISSEMENT\n"),
         getRessourcesInvestissement().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getRessourcesInvestissementEmpruntsBancairesDettesAssimilees().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getRessourcesInvestissementSubventionsRecues().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getRessourcesInvestissementFCTVA().toStringAnalyseEquilibresFinanciersFondamentaux(),
         this.ressourcesInvestissementRetourDeBiensAffectesConcedes.toStringAnalyseEquilibresFinanciersFondamentaux(),

         toStringAnalyseEquilibresFinanciersFondamentaux(""),
         getEmploisInvestissement().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getEmploisInvestissementDepensesEquipement().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getEmploisInvestissementRemboursementEmpruntsDettesAssimilees().toStringAnalyseEquilibresFinanciersFondamentaux(),
         this.emploisInvestissementChargesARepartir.toStringAnalyseEquilibresFinanciersFondamentaux(),
         this.emploisInvestissementImmobilisationsAffecteesConcedees.toStringAnalyseEquilibresFinanciersFondamentaux(),

         toStringAnalyseEquilibresFinanciersFondamentaux(""),
         this.operationsInvestissementBesoinOuCapaciteFinancementResiduelSectionInvestissement.toStringAnalyseEquilibresFinanciersFondamentaux(),
         this.operationsInvestissementSoldeDesOperationsPourCompteDeTiers.toStringAnalyseEquilibresFinanciersFondamentaux(),
         this.operationsInvestissementBesoinOuCapaciteFinancementSectionInvestissement.toStringAnalyseEquilibresFinanciersFondamentaux(),
         this.operationsInvestissementResultatEnsemble.toStringAnalyseEquilibresFinanciersFondamentaux(),
         
         toStringAnalyseEquilibresFinanciersFondamentaux("\n\n\t\t\t\t\t\tAUTOFINANCEMENT\t\t\t\t\t\t\ten % des produits CAF\n"),
         this.autoFinancementExcedentBrutDeFonctionnement.toStringAnalyseEquilibresFinanciersFondamentaux(),
         getAutoFinancementCAF().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getAutoFinancementCAFNette().toStringAnalyseEquilibresFinanciersFondamentaux(),
         
         toStringAnalyseEquilibresFinanciersFondamentaux( "\n\t\t\t\t\t\tENDETTEMENT\t\t\t\t\t\t\ten % des produits CAF"),
         getEndettementEncoursTotalDetteAu31DecembreN().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getEndettementEncoursDettesBancairesEtAssimilees().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getEndettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getEndettementAnnuiteDette().toStringAnalyseEquilibresFinanciersFondamentaux(),
         this.endettementFondsDeRoulement.toStringAnalyseEquilibresFinanciersFondamentaux(),
         
         toStringAnalyseEquilibresFinanciersFondamentaux("\n\n\t\t\t\t\t\tELEMENTS DE FISCALITE DIRECTE LOCALE"),
         toStringAnalyseEquilibresFinanciersFondamentaux("Les bases imposées et les réductions (exonérations, abattements) accordées sur délibérations"),
         toStringAnalyseEquilibresFinanciersFondamentaux("Bases nettes imposées au profit de la commune               Taxe\t\t\t\t\tRéductions de base accordées sur délibérations"),
         toStringAnalyseEquilibresFinanciersFondamentaux("En milliers d'Euros  Euros par habitant\t\tMoyenne de la strate\t\t\tEn milliers d'Euros     Euros par habitant   Moyenne de la strate"),
         getFiscLocalBaseNetteTaxeHabitation().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getFiscLocalBaseNetteTaxeFonciereProprietesBaties().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getFiscLocalBaseNetteTaxeFonciereProprietesNonBaties().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getFiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getFiscLocalBaseNetteCotisationFonciereEntreprises().toStringAnalyseEquilibresFinanciersFondamentaux(),

         toStringAnalyseEquilibresFinanciersFondamentaux("\nLes taux et les produits de la fiscalité directe locale"),
         toStringAnalyseEquilibresFinanciersFondamentaux("Produits des impôts locaux            Taxe\t\t\t\t\t\t\t\tTaux voté (%)\tTaux moyen de la strate (%)"),
         getImpotsLocauxTaxeHabitation().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getImpotsLocauxTaxeFonciereProprietesBaties().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getImpotsLocauxTaxeFonciereProprietesNonBaties().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getImpotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getImpotsLocauxCotisationFonciereEntreprises().toStringAnalyseEquilibresFinanciersFondamentaux(),

         toStringAnalyseEquilibresFinanciersFondamentaux("\nLes produits des impôts de répartition"),
         toStringAnalyseEquilibresFinanciersFondamentaux("Produits des impôts de répartition      Taxe"),
         getProduitsImpotsRepartitionCotisationValeurAjouteeEntreprises().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getProduitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getProduitsImpotsRepartitionTaxeSurfacesCommerciales().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getRatiosNiveauxEtStructure().toString()
      };
      
      StringBuilder analyseFinanciere = new StringBuilder();

      for(String ligne : analyse) {
         analyseFinanciere.append(ligne).append("\n");
      }
      
      return analyseFinanciere.toString();
   }
}
