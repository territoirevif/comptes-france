package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;
import java.text.MessageFormat;
import java.util.*;

import fr.ecoemploi.domain.utils.autocontrole.ObjetMetierSpark;

/**
 * Une intercommunalité.
 * Elle est basée sur sa commune de siège.
 * @author Marc LE BIHAN
 */
public class Intercommunalite extends ObjetMetierSpark {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -6464561300183675625L;

   /** Numéro SIREN de l'intercommunalité. */
   private String siren;
   
   /** Commune du siège de l'intercommunalité, celle dont est repris le code de référence. */
   private String codeCommuneSiege;
   
   /** SIREN de la commune siège de l'intercommunalité, celle dont est repris le code de référence. */
   private String sirenCommuneSiege;
   
   /** Nom de la commune. */
   private String nomCommuneSiege;
   
   /** Commune siège de l'intercommunalité. */
   private Commune communeSiege;
   
   /** Codes communes membres de l'intercommunalité. */
   private Set<String> codeCommunesMembres = new HashSet<>();
   
   /** Communes membres de l'intercommunalité. */
   private Map<String, Commune> communesMembres = new LinkedHashMap<>();
   
   /** Nom du groupement. */
   private String nomGroupement;
   
   /** Nature juridique de l'intercommunalité. */
   private NatureJuridiqueIntercommunalite natureJuridique;
   
   /** Compétences de l'intercommunalité. */
   private Set<CompetenceGroupement> competences = new HashSet<>();
   
   /** Population totale des communes de l'intercommunalité. */
   private Integer population;

   /** Population totale des communes de l'intercommunalité. */
   private Map<String, Integer> populationsMembres = new HashMap<>();

   /** Nombre de communes membres de l'intercommunalité déclaré. */
   private Integer nombreCommunesMembresDeclare;
   
   /** Civilité président(e). */
   private String civilite;
   
   /** Nom du (de la) président(e). */
   private String nomPresident;
   
   /** Prénom du (de la) président(e). */
   private String prenomPresident;
   
   /** Adresse du siège. */
   private Adresse adresseSiege;
   
   /** Adresse annexe. */
   private Adresse adresseAnnexe;
   
   /** Numéro de téléphone du siège. */
   private String telephoneSiege;
   
   /** E-mail du siège. */
   private String emailSiege;
   
   /** Site Internet du siège. */
   private String siteInternet;
   
   /** True si cette intercommunalité a pu être complètement restituée. */
   private boolean complete = true;

   /**
    * Renvoyer l'adresse de l'annexe du siège.
    * @return Adresse de l'annexe.
    */
   public Adresse getAdresseAnnexe() {
      return this.adresseAnnexe;
   }

   /**
    * Fixer l'adresse de l'annexe du siège.
    * @param adresseAnnexe Adresse annexe.
    */
   public void setAdresseAnnexe(Adresse adresseAnnexe) {
      this.adresseAnnexe = adresseAnnexe;
   }

   /**
    * Renvoyer l'adresse du siège de l'intercommunalité.
    * @return Adresse du siège.
    */
   public Adresse getAdresseSiege() {
      return this.adresseSiege;
   }

   /**
    * Fixer l'adresse du siège de l'intercommunalité.
    * @param adresseSiege Adresse du siège. 
    */
   public void setAdresseSiege(Adresse adresseSiege) {
      this.adresseSiege = adresseSiege;
   }

   /**
    * Renvoyer la civilté du président de l'intercommunalité.
    * @return Civilté du président de l'intercommunalité.
    */
   public String getCivilite() {
      return this.civilite;
   }

   /**
    * Fixer la civilté du président de l'intercommunalité.
    * @param civilite Civilté du président de l'intercommunalité.
    */
   public void setCivilite(String civilite) {
      this.civilite = civilite;
   }

   /**
    * Renvoyer la liste des codes communes membres de cette intercommunalité.
    * @return Liste des communes membres.
    */
   public Set<String> getCodesCommunesMembres() {
      return this.codeCommunesMembres;
   }

   /**
    * Renvoyer la liste des codes communes membres de cette intercommunalité.
    * @param communesMembres Liste des communes.
    */
   public void setCodesCommunesMembres(Set<String> communesMembres) {
      this.codeCommunesMembres = communesMembres;
   }

   /**
    * Renvoyer la liste des communes membres de l'intercommunalité.
    * @return Liste des communes membres.
    */
   public Map<String, Commune> getCommunesMembres() {
      return this.communesMembres;
   }

   /**
    * Fixer la liste des communes membres de l'intercommunalité.
    * @param communesMembres Communes membres.
    */
   public void setCommunesMembres(Communes communesMembres) {
      this.communesMembres = communesMembres;
   }

   /**
    * Renvoyer le code commune du siège de cette intercommunalité. 
    * @return Commune du siège.
    */
   public CodeCommune asCodeCommuneSiege() {
      return new CodeCommune(this.codeCommuneSiege);
   }

   /**
    * Renvoyer le code de la commune du siège de cette intercommunalité. 
    * @return Commune du siège.
    */
   public String getCodeCommuneSiege() {
      return this.codeCommuneSiege;
   }

   /**
    * Fixer le code de la commune du siège de cette intercommunalité. 
    * @param communeSiege Commune du siège. 
    */
   public void setCodeCommuneSiege(String communeSiege) {
      this.codeCommuneSiege = communeSiege;
   }

   /**
    * Renvoyer la commune siège de l'intercommunalité.
    * @return Commune siège.
    */
   public Commune getCommuneSiege() {
      return this.communeSiege;
   }

   /**
    * Fixer la commune siège de l'intercommunalité.
    * @param communeSiege Commune siège.
    */
   public void setCommuneSiege(Commune communeSiege) {
      this.communeSiege = communeSiege;
   }

   /**
    * Renvoyer les compétences de l'intercommunalité.
    * @return Liste des compétences.
    */
   public Set<CompetenceGroupement> getCompetences() {
      return this.competences;
   }

   /**
    * Renvoyer les compétences de l'intercommunalité triées par ordre alphabétique.
    * @return Liste des compétences.
    */
   public List<CompetenceGroupement> getCompetencesTriees() {
      List<CompetenceGroupement> competencesTriees = new ArrayList<>(this.competences);
      competencesTriees.sort(Comparator.comparing(CompetenceGroupement::getLibelle));
      
      return competencesTriees;
   }

   /**
    * Renvoyer les compétences de l'intercommunalité d'un type particulier triées par ordre alphabétique.
    * @param type Restriction à un type d'intercommunalité.
    * @return Liste des compétences.
    */
   public List<CompetenceGroupement> getCompetencesTriees(TypeCompetence type) {
      if (type == null) {
         return getCompetencesTriees();
      }
      
      return this.competences.stream().filter(c -> c.getType().equals(type)).sorted(Comparator.comparing(CompetenceGroupement::getLibelle)).toList();
   }

   /**
    * Fixer la liste des compétences de l'intercommunalité.
    * @param competences Liste des compétences.
    */
   public void setCompetences(Set<CompetenceGroupement> competences) {
      this.competences = competences;
   }
   
   /**
    * Renvoyer l'e-mail du siège. 
    * @return l'e-mail du siège.
    */
   public String getEmailSiege() {
      return this.emailSiege;
   }

   /**
    * Fixer l'e-mail du siège.
    * @param emailSiege E-mail du siège. 
    */
   public void setEmailSiege(String emailSiege) {
      this.emailSiege = emailSiege;
   }

   /**
    * Déterminer si cette intercommunalité est complète.
    * @return true si elle a pu être entièrement constituée.
    */
   public boolean isComplete() {
      return this.complete;
   }

   /**
    * Indiquer si cette intercommunalité est complète.
    * @param complete true si elle a pu être entièrement constituée.
    */
   public void setComplete(boolean complete) {
      this.complete = complete;
   }
   
   /**
    * Déterminer si cette nature d'intercommunalité est à fiscalité propre.
    * @return true si elle l'est.
    */
   public boolean isFiscalitePropre() {
      return this.natureJuridique.isFiscalitePropre();
   }

   /**
    * Renvoyer la nature juridique de l'intercommunalité.
    * @return Nature juridique.
    */
   public NatureJuridiqueIntercommunalite getNatureJuridique() {
      return this.natureJuridique;
   }

   /**
    * Renvoyer la nature juridique de l'intercommunalité.
    * @param natureJuridique Nature juridique.
    */
   public void setNatureJuridique(NatureJuridiqueIntercommunalite natureJuridique) {
      this.natureJuridique = natureJuridique;
   }

   /**
    * Renvoyer le nombre de communes membres que l'intercommunalité déclare posséder.
    * @return Nombre de communes membres.
    */
   public Integer getNombreCommunesMembresDeclare() {
      return this.nombreCommunesMembresDeclare;
   }

   /**
    * Fixer le nombre de communes membres que l'intercommunalité déclare posséder.
    * @param nombreCommunes Nombre de communes membres.
    */
   public void setNombreCommunesMembresDeclare(Integer nombreCommunes) {
      this.nombreCommunesMembresDeclare = nombreCommunes;
   }

   /**
    * Renvoyer le nom de la commune siège. 
    * @return Nom de la commune siège.
    */
   public String getNomCommuneSiege() {
      return this.nomCommuneSiege;
   }

   /**
    * Fixer le nom de la commune siège.
    * @param nomCommuneSiege Nom de la commune siège.
    */
   public void setNomCommuneSiege(String nomCommuneSiege) {
      this.nomCommuneSiege = nomCommuneSiege;
   }

   /**
    * Renvoyer le nom du groupement.
    * @return Nom du groupement.
    */
   public String getNomGroupement() {
      return this.nomGroupement;
   }

   /**
    * Fixer le nom du groupement.
    * @param nomGroupement Nom du groupement.
    */
   public void setNomGroupement(String nomGroupement) {
      this.nomGroupement = nomGroupement;
   }

   /**
    * Renvoyer le nom du président de l'intercommunalité.
    * @return Nom du président de l'intercommunalité.
    */
   public String getNomPresident() {
      return this.nomPresident;
   }

   /**
    * Fixer le nom du président de l'intercommunalité.
    * @param nomPresident Nom du président de l'intercommunalité. 
    */
   public void setNomPresident(String nomPresident) {
      this.nomPresident = nomPresident;
   }

   /**
    * Obtenir la population totale des communes de l'intercommunalité une année particulière.
    * @return population totale des communes de l'intercommunalité.
    */
   public Integer getPopulation() {
      return this.population;
   }

   /**
    * Fixer la population totale des communes de l'intercommunalité une année donnée.
    * @param population Population.
    */
   public void setPopulation(Integer population) {
      this.population = population;
   }

   /**
    * Renvoyer la liste des populations des communes membres au moment où cette représentation intercommunale a été faite. 
    * @return Liste des populations membres par code de commune membre (ou siège). 
    */
   public Map<String, Integer> getPopulationsMembres() {
      return this.populationsMembres;
   }

   /**
    * Fixer la liste des populations des communes membres au moment où cette représentation intercommunale a été faite.
    * @param populationsMembres Liste des populations membres par code de commune membre (ou siège).
    */
   public void setPopulationsMembres(Map<String, Integer> populationsMembres) {
      this.populationsMembres = populationsMembres;
   }

   /**
    * Renvoyer le prénom du président de l'intercommunalité.
    * @return Prénom du président de l'intercommunalité.
    */
   public String getPrenomPresident() {
      return this.prenomPresident;
   }

   /**
    * Fixer le prénom du président de l'intercommunalité.
    * @param prenomPresident Prénom du président de l'intercommunalité. 
    */
   public void setPrenomPresident(String prenomPresident) {
      this.prenomPresident = prenomPresident;
   }

   /**
    * Renvoyer le numéro de SIREN de l'intercommunalité.
    * @return Numéro de SIREN.
    */
   public SIRENCommune asSiren() {
      return new SIRENCommune(this.siren);
   }

   /**
    * Renvoyer le numéro de SIREN de l'intercommunalité.
    * @return Numéro de SIREN.
    */
   public String getSiren() {
      return this.siren;
   }

   /**
    * Fixer le numéro de SIREN de l'intercommunalité.
    * @param siren Numéro de SIREN.
    */
   public void setSiren(String siren) {
      this.siren = siren;
   }

   /**
    * Renvoyer le SIREN de la commune siège.
    * @return SIREN de la commune siège.
    */
   public String getSirenCommuneSiege() {
      return this.sirenCommuneSiege;
   }

   /**
    * Fixer le SIREN de la commune siège.
    * @param siren SIREN de la commune siège.
    */
   public void setSirenCommuneSiege(String siren) {
      this.sirenCommuneSiege = siren;
   }

   /**
    * Renvoyer le site internet du siège. 
    * @return Site internet du siège.
    */
   public String getSiteInternet() {
      return this.siteInternet;
   }

   /**
    * Fixer le site internet du siège.
    * @param siteInternet Site internet du siège. 
    */
   public void setSiteInternet(String siteInternet) {
      this.siteInternet = siteInternet;
   }

   /**
    * Renvoyer le numéro de téléphone du siège.
    * @return Numéro de téléphone du siège.
    */
   public String getTelephoneSiege() {
      return this.telephoneSiege;
   }

   /**
    * Fixer le numéro de téléphone du siège.
    * @param telephoneSiege Numéro de téléphone du siège. 
    */
   public void setTelephoneSiege(String telephoneSiege) {
      this.telephoneSiege = telephoneSiege;
   }
   
   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      // #0 : Nom de l'intercommunalité ou du groupement.
      // #1 : Nature de l'intercommunalité.
      // #2 : Code commune de son siège.
      // #3 : Siren de son siège.
      // #4 : Nom de sa commune siège.
      // #5 : Nombre de communes membres qu'elle possède.
      // #6 : Intercommunalité considérée comme complète ou non.
      // #7 : Civilité du (de la) président(e) de l'intercommunalité.
      // #8 : Nom du président de l'intercommunalité.
      // #9 : Prénom du président de l'intercommunalité.
      // #10 : Adresse du siège.
      // #11 : Compétences.
      // #12 : Téléphone.
      // #13 : E-mail.
      // #14 : Site Internet.
      String format = "nom : {0}, nature : {1}, siège : (code commune : {2}, siren : {3}, {4}), nombre de membres : {5}, complète : {6}, " +
         "président(e) : (civilité : {7}, prénom : {8}, nom : {9}), adresse du siège : {10}, compétences de l''intercommunalité : {11}, téléphone : {12}, " +
         "e-mail : {13}, site internet : {14}";

      try {
         return MessageFormat.format(format, this.nomGroupement, this.getNatureJuridique().getLibelle(), this.codeCommuneSiege,
            this.sirenCommuneSiege, this.nomCommuneSiege, this.codeCommunesMembres != null ? this.codeCommunesMembres.size() : 0, this.complete,
            this.civilite, this.prenomPresident, this.nomPresident, this.adresseSiege, this.competences, this.telephoneSiege, this.emailSiege, this.siteInternet);
      }
      catch(RuntimeException e) {
         return Intercommunalite.class.getName() + " - toString - " + e.getMessage();
      }
   }
}
