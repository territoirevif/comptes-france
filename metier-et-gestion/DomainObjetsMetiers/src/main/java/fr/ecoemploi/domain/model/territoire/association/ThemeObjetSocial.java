package fr.ecoemploi.domain.model.territoire.association;

import java.util.*;

/**
 * Thème des objets sociaux. 
 * @author Marc Le Bihan
 */
public enum ThemeObjetSocial {
   /** Retour à l'emploi. */
   RETOUR_A_EMPLOI("Retour à l'emploi", new String[] {
      "019014", // Lutte contre l'illettrisme
      "019016", // Aide à l'insertion des jeunes
      "019020", // Groupements de chômeurs, aide aux chômeurs
      "019050", // Réinsertion des délinquants
      "019055", // Soutien, reclassement des détenus
      "030000", // Aide à l'emploi, développement local, promotion de solidarités économiques, vie locale
      "030010", // Entreprises d'insertion, associations intermédiaires, régies de quartier
      "030020", // Aide à la création d'activités économiques individuelles
      "030050", // Promotion d'initiatives de développement durable
   }),

   /** Professionnels. */
   PROFESSIONNELS_EMPLOI("Professionnels", new String[] {
      "014025", // Organisation de professions (hors caractère syndical)
      "030005", // Comité, défense d'un emploi
      "030015", // Groupement d'employeurs
   }),

   /** Formations */
   FORMATIONS("Formations", new String[] {
      "015000", // Éducation formation
      "015010", // Organisation de professions enseignantes, amicales de personnel
      "015025", // Associations périscolaires, coopération, aide à l'enseignement
      "015030", // Œuvres sociales en faveur des élèves, œuvres en faveur pupilles de la nation
      "015035", // Organisme de gestion d'établissement d'enseignement général et technique
      "015040", // Organisme de gestion d'établissement d'enseignement supérieur
      "015045", // Établissement de formation professionnelle, formation continue
      "015050", // Centre d'enseignement et de formation associations d’étudiants, d’élèves
      "015087", // Études et formations linguistiques
      "015100", // Apprentissage
   }),

   /** Aspects sociaux liés à l'emploi */
   SOCIAL_EMPLOI("Aspects sociaux liés à l'emploi", new String[] {
      "002005", // Associations philanthropiques
      "002010", // Amicales laïques
      "004010", // Médiation, prévention
      "014000", // Amicales, groupements affinitaires, groupements d'entraide (hors défense de droits fondamentaux)
      "014035", // Groupements d'entraide et de solidarité
      "018030", // Prévention et lutte contre l'alcoolisme, la toxicomanie
      "019000", // Interventions sociales
      "019004", // Aide et conseils aux familles
      "019005", // Associations familiales, services sociaux pour les familles
      "019012", // Lutte contre le surendettement
      "019040", // Aide aux personnes en danger, solitude, désespoir, soutien psychologique et moral
      "020000", // Associations caritatives, humanitaires, aide au développement, développement du bénévolat
      "020005", // Secours financiers et autres services aux personnes en difficulté
      "020010", // Secours en nature, distribution de nourriture et de vêtements
      "020015", // Associations caritatives à buts multiples
      "020020", // Associations caritatives intervenant au plan international
      "021005", // Crèches, garderies, haltes garderies
      "003060", // Activités civiques, information civique
      "030012", // Comités de défense et d'animation de quartier, association locale ou municipale
      "032000", // Logement
      "032510", // Aide au logement
      "032520", // Associations et comités de locataires, de propriétaires, comités de logement
   }),

   /** Soutien publics particuliers */
   SOUTIEN_PUBLICS_PARTICULIERS("Soutien publics particuliers", new String[] {
      "019025", // Aide aux réfugiés et aux immigrés (hors droits fondamentaux)
      "003020", // Défense des droits des femmes, condition féminine
      "003025", // Défense des droits des personnes homosexuelles
      "003030", // Défense des droits des personnes en situation de handicap
      "003035", // Association pour la défense de droits de minorités
      "003045", // Défense des droits des personnes rapatriées
      "003050", // Défense des droits des personnes étrangères ou immigrées, de personnes réfugiées
      "014050", // Associations féminines pour l'entraide et la solidarité (hors défense de droits fondamentaux)
      "014060", // Associations de personnes homosexuelles pour l'entraide et la solidarité (hors défense de droits fondamentaux)
      "014070", // Associations de personnes en situation de handicap pour l'entraide et la solidarité (hors défense de droits fondamentaux)
      "014080", // Associations de classe d'âge
      "003040", // Lutte contre les discriminations
      "018050", // Aide sociale aux personnes en situation de handicap
      "018015", // Établissements, services pour personnes handicapées (y compris c.a.t)
      "018025", // Établissements et services pour adultes en difficulté, chrs (centres d'hébergement et de réadaptation sociale)
      "018040", // Aide aux accidentés du travail
   }),
   
   /** Développement touristique. */
   DEVELOPPEMENT_TOURISTIQUE("Développement touristique", new String[] {
      "007000", // Clubs de loisirs, relations
      "007040", // Activités festives (soirées...)
      "007080", // Centres de loisirs, clubs de loisirs multiples
      "009010", // Loisirs pour personnes en situation de handicap
      "009020", // Centres aérés, colonies de vacances
      "009025", // Mouvements éducatifs de jeunesse et d'éducation populaire
      "011000", // Sports, activités de plein air
      "011005", // Associations multisports locales
      "011018", // Handisport
      "011125", // Natation - baignade (natation, plongée)
      "011192", // Associations pour la promotion du sport, médailles, mérite sportif
      "011400", // Activités de plein air (dont saut à l'élastique)
      "023045", // Représentation d'intérêts régionaux et locaux
      "024050", // Actions de sensibilisation et d'éducation à l'environnement et au développement durable
      "030012", // Comités de défense et d'animation de quartier, association locale ou municipale
      "030050", // Promotion d'initiatives de développement durable
      "034000", // Tourisme
      "034230", // Gîtes ruraux, camping, caravaning, naturisme
      "034240", // Syndicats d'initiative, offices de tourisme, salons du tourisme
      "036520", // Sauvetage, secourisme, protection civile
   });
   
   /** Liste des codes objets sociaux associés. */
   private final String[] codesObjetsSociaux;
   
   /** Libellé de ce thème d'objet social. */
   private final String libelle;

   /**
    * Construire un thème d'objet social.
    * @param libelle Libellé du thème.
    * @param codesObjetsSociaux Liste des codes objets sociaux liés.
    */
   ThemeObjetSocial(String libelle, String[] codesObjetsSociaux) {
      this.libelle = libelle;
      this.codesObjetsSociaux = codesObjetsSociaux;
   }
   
   /**
    * Renvoyer le libellé du thème.
    * @return Libellé.
    */
   public String getLibelle() {
      return this.libelle;
   }
   
   /**
    * Renvoyer la liste des codes objets sociaux.
    * @return Codes objets Sociaux.
    */
   public String[] getCodesObjetsSociaux() {
      return this.codesObjetsSociaux;
   }
   
   /**
    * Renvoyer une liste combinée de plusieurs natures d'objets sociaux.
    * @param themesSociaux Liste des thèmes sociaux désirés.
    * @return Liste des codes sociaux correspondants.
    */
   public static List<String> union(ThemeObjetSocial... themesSociaux) {
      List<String> themes = new ArrayList<>();
      
      for(ThemeObjetSocial theme : themesSociaux) {
         Collections.addAll(themes, theme.getCodesObjetsSociaux());
      }
      
      return themes;
   }
}
