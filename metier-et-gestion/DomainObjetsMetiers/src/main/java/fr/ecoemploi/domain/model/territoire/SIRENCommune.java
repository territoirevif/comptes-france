package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;

/**
 * Un numéro SIREN, de commune : ces numéros débutent par un 1 ou un 2, ont un numéro de département.
 * @author Marc LE BIHAN
 */
@Deprecated
public class SIRENCommune extends SIREN {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -8619741739926161742L;

   /**
    * Construire un numéro de SIREN vide (invalide).
    */
   public SIRENCommune() {
   }
   
   /**
    * Construire un numéro de SIREN alimenté.
    * @param siren SIREN.
    */
   public SIRENCommune(String siren) {
      super(siren);
   }
   
   /**
    * Construire un numéro de SIREN alimenté.
    * @param siren SIREN.
    */
   public SIRENCommune(SIREN siren) {
      this(siren.getId());
   }
   
   /**
    * Construire un numéro de SIREN alimenté.
    * @param siret SIREN.
    * @throws SIRETInvalideException si le numéro de SIRET transmis est invalide. 
    */
   public SIRENCommune(SIRET siret) throws SIRETInvalideException {
      this(siret.siren());
   }
}
