package fr.ecoemploi.domain.model.territoire.entreprise.ape;

import org.apache.commons.lang3.builder.*;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.ObjetMetierIdentifiable;

/**
 * Une activité principale.
 * @author Marc LE BIHAN
 */
public class ActivitePrincipale extends ObjetMetierIdentifiable<CodeAPE> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -781353590828637563L;

   /**
    * Construire une activité principale.
    * @param code Code.
    */
   public ActivitePrincipale(CodeAPE code) {
      setId(code);
   }
   
   /**
    * @see ObjetMetierIdentifiable#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object o) {
      if (o instanceof ActivitePrincipale == false) {
         return false;
      }
      
      // Le contrôle d'égalité est restreint au code APE.
      ActivitePrincipale ape = (ActivitePrincipale)o;
      
      if (getId() == null) {
         return false;
      }
      
      return ape.getId().equals(getId());
   }
   
   /**
    * @see java.lang.Object#hashCode()
    */
   @Override
   public int hashCode() {
      // La liste des anomalies n'est pas discriminante dans le test de l'égalité et ne participe pas au calcul du hashcode.
      HashCodeBuilder hashcode = new HashCodeBuilder();
      hashcode.append("activitePrincipale" + getId()); // Le code est le vrai discriminant, mais l'on veut distinguer le hashcode de celui de CodeAPE.
      return hashcode.toHashCode();
   }
   
   /**
    * @see ObjetMetierIdentifiable#toString()
    */
   @Override
   public String toString() {
      return getId() != null ? getId().toString() : null;
   }
}
