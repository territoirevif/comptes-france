package fr.ecoemploi.domain.model.territoire.comptabilite;

import java.io.Serial;
import java.text.*;

import fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels.*;

/**
 * Comptes individuels d'une intercommunalité.
 * @author Marc Le Bihan
 */
public class ComptesIndividuelsIntercommunalite extends ComptesIndividuels<ComptesAvecRatioStructure, ComptesAvecRatioStructure, ComptesAvecReductionVotee, ComptesAvecTauxVote> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3381567510474866525L;
   
   /**
    * Construire des comptes individuels d'intercommunalité.
    */
   public ComptesIndividuelsIntercommunalite() {
   }
   
   /**
    * Construire des comptes individuels d'intercommunalité. 
    * @param comptesIndividuels Comptes individuels de la collectivité locale qui sont communs à toutes les collectivités locales.
    */
   public ComptesIndividuelsIntercommunalite(ComptesIndividuels<ComptesAvecRatioStructure, ComptesAvecRatioStructure, ComptesAvecReductionVotee, ComptesAvecTauxVote> comptesIndividuels) {
      super(comptesIndividuels);
   }
   
   /**
    * Renvoyer la population légale en vigueur au 1er janvier de l'exercice.
    * @return Population légale en vigueur au 1er janvier de l'exercice.
    */
   public Integer getPopulationIntercommunalite() {
      return super.getPopulation();
   }

   /**
    * Fixer la population légale en vigueur au 1er janvier de l'exercice.
    * @param populationIntercommunalite Population légale en vigueur au 1er janvier de l'exercice. 
    */
   public void setPopulationIntercommunalite(Integer populationIntercommunalite) {
      super.setPopulation(populationIntercommunalite);
   }

   /**
    * Renvoyer le siren de l'intercommunalité.
    * @return Siren de l'intercommunalité.
    */
   public String getSirenIntercommunalite() {
      return getSiren();
   }

   /**
    * Fixer le siren de l'intercommunalité.
    * @param siren Siren de l'intercommunalité.
    */
   public void setSirenIntercommunalite(String siren) {
      setSiren(siren);
   }

   /**
    * Renvoyer le code de l'intercommunalité.
    * @return Code de l'intercommunalité.
    */
   public String getCodeIntercommunalite() {
      return getCode();
   }

   /**
    * Fixer le code de l'intercommunalité.
    * @param codeIntercommunalite Code de l'intercommunalité.
    */
   public void setCodeIntercommunalite(String codeIntercommunalite) {
      setCode(codeIntercommunalite);
   }

   /**
    * Renvoyer le nom de l'intercommunalité.
    * @return Nom de l'intercommunalité.
    */
   public String getNomIntercommunalite() {
      return getNom();
   }

   /**
    * Fixer le nom de l'intercommunalité.
    * @param nomIntercommunalite Nom de l'intercommunalité.
    */
   public void setNomIntercommunalite(String nomIntercommunalite) {
      setNom(nomIntercommunalite);
   }
   
   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      String[] analyse = {
         toStringAnalyseEquilibresFinanciersFondamentaux("\nLibellé du budget : %s", getNomIntercommunalite()),
         toStringAnalyseEquilibresFinanciersFondamentaux("Population légale en vigueur au 1er janvier de l'exercice : %d\tBudget principal seul", getPopulationIntercommunalite()),
         toStringAnalyseEquilibresFinanciersFondamentaux("Exercice : %d\t\tCode EPCI : %s", getAnneeExercice(), getCodeIntercommunalite()),

         toStringAnalyseEquilibresFinanciersFondamentaux("\n\nANALYSE DES EQUILIBRES FINANCIERS FONDAMENTAUX\n\nEn milliers d'Euros\tEuros par habitant\tMoyenne de la strate\tOPERATIONS DE FONCTIONNEMENT\tRatios de structure\tMoyenne de la strate (en % des produits)\n"),

         toStringAnalyseEquilibresFinanciersFondamentaux("\n\t\t\t\t\t\tOPERATIONS DE FONCTIONNEMENT\n"),
         getProduitsDeFonctionnement().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getProduitsDeFonctionnementCAF().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getProduitsDeFonctionnementCAFImpotsLocaux().toStringAnalyseEquilibresFinanciersFondamentaux(), 
         getProduitsDeFonctionnementCAFAutresImpotsEtTaxes().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getProduitsDeFonctionnementCAFDGF().toStringAnalyseEquilibresFinanciersFondamentaux(),
         
         toStringAnalyseEquilibresFinanciersFondamentaux(""),

         getChargesDeFonctionnement().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getChargesDeFonctionnementCAF().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getChargesDeFonctionnementCAFChargesDePersonnel().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getChargesDeFonctionnementCAFAchatsEtChargesExternes().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getChargesDeFonctionnementCAFChargesFinancieres().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getChargesDeFonctionnementCAFSubventionsVersees().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getResultatComptable().toStringAnalyseEquilibresFinanciersFondamentaux(),
         
         toStringAnalyseEquilibresFinanciersFondamentaux("\n\n\t\t\t\t\t\tOPERATIONS D'INVESTISSEMENT\n"),
         getRessourcesInvestissement().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getRessourcesInvestissementEmpruntsBancairesDettesAssimilees().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getRessourcesInvestissementSubventionsRecues().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getRessourcesInvestissementFCTVA().toStringAnalyseEquilibresFinanciersFondamentaux(),

         toStringAnalyseEquilibresFinanciersFondamentaux(""),
         getEmploisInvestissement().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getEmploisInvestissementDepensesEquipement().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getEmploisInvestissementRemboursementEmpruntsDettesAssimilees().toStringAnalyseEquilibresFinanciersFondamentaux(),

         toStringAnalyseEquilibresFinanciersFondamentaux(""),
         
         toStringAnalyseEquilibresFinanciersFondamentaux("\n\n\t\t\t\t\t\tAUTOFINANCEMENT\t\t\t\t\t\t\ten % des produits CAF\n"),
         getAutoFinancementCAF().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getAutoFinancementCAFNette().toStringAnalyseEquilibresFinanciersFondamentaux(),
         
         toStringAnalyseEquilibresFinanciersFondamentaux( "\n\t\t\t\t\t\tENDETTEMENT\t\t\t\t\t\t\ten % des produits CAF"),
         getEndettementEncoursTotalDetteAu31DecembreN().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getEndettementEncoursDettesBancairesEtAssimilees().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getEndettementEncoursDetteBancaireNetteSoutienSortieEmpruntsToxiques().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getEndettementAnnuiteDette().toStringAnalyseEquilibresFinanciersFondamentaux(),
         
         toStringAnalyseEquilibresFinanciersFondamentaux("\n\n\t\t\t\t\t\tELEMENTS DE FISCALITE DIRECTE LOCALE"),
         toStringAnalyseEquilibresFinanciersFondamentaux("Les bases imposées et les réductions (exonérations, abattements) accordées sur délibérations"),
         toStringAnalyseEquilibresFinanciersFondamentaux("Bases nettes imposées au profit de la commune               Taxe\t\t\t\t\tRéductions de base accordées sur délibérations"),
         toStringAnalyseEquilibresFinanciersFondamentaux("En milliers d'Euros  Euros par habitant\t\tMoyenne de la strate\t\t\tEn milliers d'Euros     Euros par habitant   Moyenne de la strate"),
         getFiscLocalBaseNetteTaxeHabitation().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getFiscLocalBaseNetteTaxeFonciereProprietesBaties().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getFiscLocalBaseNetteTaxeFonciereProprietesNonBaties().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getFiscLocalBaseNetteTaxeAdditionnelleFonciereProprietesNonBaties().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getFiscLocalBaseNetteCotisationFonciereEntreprises().toStringAnalyseEquilibresFinanciersFondamentaux(),

         toStringAnalyseEquilibresFinanciersFondamentaux("\nLes taux et les produits de la fiscalité directe locale"),
         toStringAnalyseEquilibresFinanciersFondamentaux("Produits des impôts locaux            Taxe\t\t\t\t\t\t\t\tTaux voté (%)\tTaux moyen de la strate (%)"),
         getImpotsLocauxTaxeHabitation().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getImpotsLocauxTaxeFonciereProprietesBaties().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getImpotsLocauxTaxeFonciereProprietesNonBaties().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getImpotsLocauxTaxeAdditionnelleFonciereProprietesNonBaties().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getImpotsLocauxCotisationFonciereEntreprises().toStringAnalyseEquilibresFinanciersFondamentaux(),

         toStringAnalyseEquilibresFinanciersFondamentaux("\nLes produits des impôts de répartition"),
         toStringAnalyseEquilibresFinanciersFondamentaux("Produits des impôts de répartition      Taxe"),
         getProduitsImpotsRepartitionCotisationValeurAjouteeEntreprises().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getProduitsImpotsRepartitionImpositionForfaitaireEntreprisesReseau().toStringAnalyseEquilibresFinanciersFondamentaux(),
         getProduitsImpotsRepartitionTaxeSurfacesCommerciales().toStringAnalyseEquilibresFinanciersFondamentaux()
      };
      
      StringBuilder analyseFinanciere = new StringBuilder();

      for(String ligne : analyse) {
         analyseFinanciere.append(ligne).append("\n");
      }
      
      analyseFinanciere.append(MessageFormat.format("\nCapacité de désendettement, en années, encours dette sur épargne(caf) : {0}\n", getRatiosNiveauxEtStructure().getCapaciteDesendettement()));
      return analyseFinanciere.toString();
   }
}
