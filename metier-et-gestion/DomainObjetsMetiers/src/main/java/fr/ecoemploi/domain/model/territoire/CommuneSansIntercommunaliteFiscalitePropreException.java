package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.MetierException;

/**
 * Exception levée si une commune n'est pas rattachée à une intercommunalité à fiscalité propre.
 * @author Marc LE BIHAN
 */
public class CommuneSansIntercommunaliteFiscalitePropreException extends MetierException {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -2322582936442865023L;

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    */
   public CommuneSansIntercommunaliteFiscalitePropreException(String message) {
      super(message);
   }
}
