package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;
import java.util.*;

import fr.ecoemploi.domain.utils.objets.*;

/**
 * Une liste de régions.
 * @author Marc LE BIHAN
 */
public class Regions extends AbstractObjetsMetiersIndexes<CodeRegion, Region> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 1229587166861687069L;

   /**
    * Construire une liste de régions vide.
    */
   public Regions() {
   }

   /**
    * Construire une liste de régions vide.
    * @param regions Liste de régions à copier.
    */
   public Regions(Regions regions) {
      this(regions, false);
   }

   /**
    * Construire une liste de régions vide.
    * @param regions Liste de régions à copier.
    * @param deep true : copie complète et distincte de la région<br>
    *    false : seul le pointeur de région est repris.
    */
   public Regions(Regions regions, boolean deep) {
      for(Region region : regions) {
         put(region.getCodeRegion(), deep ? new Region(region) : region);
      }
   }

   /**
    * Comparateur avec Locale.
    */
   static class ComparateurLocale extends AbstractComparatorLocale implements Comparator<Region> {
      /**
       * Construire un comparateur.
       * @param locale Locale.
       */
      public ComparateurLocale(Locale locale) {
         super(locale);
      }

      /**
       * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
       */
      @Override
      public int compare(Region a, Region b) {
         return this.collator.compare(a.getNom(), b.getNom());
      }
   }

   /**
    * Trier la liste des régions.
    * @param locale Locale.
    */
   public void sort(Locale locale) {
      Objects.requireNonNull(locale, "La locale pour le tri ne peut pas valoir null.");
      sort(this, new ComparateurLocale(locale));
   }
}
