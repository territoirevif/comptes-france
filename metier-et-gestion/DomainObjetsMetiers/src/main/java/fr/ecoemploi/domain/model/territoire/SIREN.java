package fr.ecoemploi.domain.model.territoire;

import org.apache.commons.lang3.*;

import java.io.Serial;

import fr.ecoemploi.domain.utils.autocontrole.Id;

/**
 * Un numéro de SIREN.
 * @author Marc LE BIHAN
 */
public class SIREN extends Id {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 42938733565726770L;
   
   /**
    * Construire un objet SIREN.
    */
   public SIREN() {
   }

   /**
    * Construire un numéro de SIREN alimenté.
    * @param siren SIREN.
    */
   public SIREN(String siren) {
      setId(siren);
   }

   /**
    * Construire un code SIREN par copie.
    * @param siren Code SIREN à copier.
    */
   public SIREN(SIREN siren) {
      super(siren);
   }

   /**
    * Déterminer si le numéro de SIREN est valide.
    * @return true si c'est le cas.
    */
   public boolean valide() {
      String siren = getId();
      return StringUtils.isNotBlank(siren) && siren.length() == 9 && StringUtils.isNumeric(siren);
   }
}
