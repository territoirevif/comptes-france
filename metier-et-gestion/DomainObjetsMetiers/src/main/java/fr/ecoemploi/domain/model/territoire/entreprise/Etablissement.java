package fr.ecoemploi.domain.model.territoire.entreprise;

import java.io.Serial;
import java.text.MessageFormat;
import java.time.*;

import fr.ecoemploi.domain.utils.autocontrole.*;
import fr.ecoemploi.domain.utils.objets.ObjetMetierIdentifiable;
import fr.ecoemploi.domain.model.territoire.*;

/**
 * Etablissement d'entreprise.
 * @author Marc LE BIHAN
 */
public class Etablissement extends AbstractSirene<SIRET> implements Comparable<Etablissement> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 2451240618966775942L;
   
   /** Année et mois de création de l'établissement. */
   private String dateCreation;
   
   /** Qualité de siège ou non de l'établissement */
   private boolean siege;

   /** Enseigne 1 ou nom de l'exploitation */
   private String enseigne1;
   
   /** Enseigne 2 ou nom de l'exploitation */
   private String enseigne2;
   
   /** Enseigne 3 ou nom de l'exploitation */
   private String enseigne3;

   /** Adresse géographique. */
   private AdresseEtablissement adressePrincipale = new AdresseEtablissement();
   
   /** Adresse géographique secondaire. */
   private AdresseEtablissement adresseSecondaire = new AdresseEtablissement();
   
   /** Activité principale au registre des métiers. */
   private String apeRegistreDesMetiers;
   
   /** Denomination de l'établissement. */
   private String denominationEtablissement;

   /**
    * Renvoyer le SIRET de l'entreprise.
    * @return SIRET.
    */
   public SIRET aetSiret() {
      return sirenOuSiret();
   }

   /**
    * Renvoyer le SIREN de l'entreprise.
    * @return SIREN.
    */
   public String siren() {
      return sirenOuSiret().toString().substring(0, 9);
   }
   
   /**
    * Renvoyer le SIRET de l'établissement.
    * @return SIRET.
    */
   public String getSiret() {
      return sirenOuSiret().toString();
   }

   /**
    * Fixer le SIRET de l'entreprise.
    * @param siret SIRET de l'entreprise.
    */
   public void setSiret(String siret) {
      sirenOuSiret(new SIRET(siret));
   }
   
   /**
    * Renvoyer la date de création de l'établissement (DCRET).
    * @return Date de création.
    */
   public LocalDate asDateCreationEtablissement() {
      return ObjetMetierSpark.fromINSEEtoLocalDate(this.dateCreation);
   }
   
   /**
    * Renvoyer la date de création de l'établissement (DCRET).
    * @return Date de création.
    */
   public String getDateCreationEtablissement() {
      return this.dateCreation;
   }

   /**
    * Fixer la date de création de l'établissement (DCRET).
    * @param date Date de création.
    */
   public void setDateCreationEtablissement(String date) {
      this.dateCreation = date;
   }

   /**
    * Déterminer si l'établissement a la qualité de siège ou non (SIEGE).
    * @return true s'il est un siège.
    */
   public boolean getSiege() {
      return this.siege;
   }

   /**
    * Indiquer si l'établissement a la qualité de siège ou non (SIEGE).
    * @param siege true s'il est un siège. 
    */
   public void setSiege(boolean siege) {
      this.siege = siege;
   }
   
   /**
    * Renvoyer l'adresse principale de l'établissement. 
    * @return adresse.
    */
   public AdresseEtablissement adressePrincipale() {
      return this.adressePrincipale;
   }

   /**
    * Renvoyer l'adresse secondaire de l'établissement. 
    * @return adresse.
    */
   public AdresseEtablissement adresseSecondaire() {
      return this.adressePrincipale;
   }
   
   /**
    * Renvoyer l'adresse principale de l'établissement. 
    * @param adresse Adresse principale.
    */
   public void adressePrincipale(AdresseEtablissement adresse) {
      this.adressePrincipale = adresse;
   }

   /**
    * Renvoyer l'adresse secondaire de l'établissement. 
    * @param adresse Adresse secondaire.
    */
   public void adresseSecondaire(AdresseEtablissement adresse) {
      this.adresseSecondaire = adresse;
   }

   /**
    * Renvoyer le complément d’adresse.
    * @return Complément d'adrese.
    */
   public String getComplementAdresse() {
      return this.adressePrincipale.getComplementAdresse();
   }

   /**
    * Fixer le complément d’adresse.
    * @param complementAdresse Complément d'adrese.
    */
   public void setComplementAdresse(String complementAdresse) {
      this.adressePrincipale.setComplementAdresse(complementAdresse);
   }

   /** 
    * Renvoyer la distribution spéciale de l’établissement.
    * @return Distribution spéciale de l’établissement.
    */
   public String getDistributionSpeciale() {
      return this.adressePrincipale.getDistributionSpeciale();
   }

   /** 
    * Fixer la distribution spéciale de l’établissement.
    * @param distributionSpeciale Distribution spéciale de l’établissement.
    */
   public void setDistributionSpeciale(String distributionSpeciale) {
      this.adressePrincipale.setDistributionSpeciale(distributionSpeciale);
   }

   /** 
    * Renvoyer le libellé du code cedex.
    * @return le libellé du code cedex.
    */
   public String getLibelleCedex() {
      return this.adressePrincipale.getLibelleCedex();
   }

   /** 
    * Fixer le libellé du code cedex.
    * @param libelleCedex le libellé du code cedex.
    */
   public void setLibelleCedex(String libelleCedex) {
      this.adressePrincipale.setLibelleCedex(libelleCedex);
   }

   /** 
    * Renvoyer le code pays pour un établissement situé à l’étranger.
    * @return Code pays de l'établissement situé à l’étranger.
    */
   public String getCodePaysEtranger() {
      return this.adressePrincipale.getCodePaysEtranger();
   }

   /** 
    * Fixer le code pays pour un établissement situé à l’étranger.
    * @param codePaysEtranger Code pays de l'établissement situé à l’étranger.
    */
   public void setCodePaysEtranger(String codePaysEtranger) {
      this.adressePrincipale.setCodePaysEtranger(codePaysEtranger);
   }

   /** 
    * Renvoyer le libellé du pays pour un établissement situé à l’étranger.
    * @return Renvoyer le libellé du pays pour un établissement situé à l’étranger.
    */
   public String getNomPaysEtranger() {
      return this.adressePrincipale.getNomPaysEtranger();
   }

   /** 
    * Fixer le libellé du pays pour un établissement situé à l’étranger.
    * @param libellePaysEtranger Fixer le libellé du pays pour un établissement situé à l’étranger.
    */
   public void setNomPaysEtranger(String libellePaysEtranger) {
      this.adressePrincipale.setNomPaysEtranger(libellePaysEtranger);
   }

   /** 
    * Renvoyer le numéro dans la voie de l'adresse.
    * @return Numéro dans la voie de l'adresse principale.
    */
   public String getNumeroVoie() {
      return this.adressePrincipale.getNumeroVoie();
   }

   /** 
    * Fixer le numéro dans la voie de l'adresse.
    * @param numeroVoie Numéro dans la voie de l'adresse principale.
    */
   public void setNumeroVoie(String numeroVoie) {
      this.adressePrincipale.setNumeroVoie(numeroVoie);
   }

   /** 
    * Renvoyer le numéro dans la voie de l'adresse secondaire.
    * @return Numéro dans la voie de l'adresse secondaire.
    */
   public String getNumeroVoieSecondaire() {
      return this.adresseSecondaire.getNumeroVoie();
   }

   /** 
    * Fixer le numéro dans la voie de l'adresse secondaire.
    * @param numeroVoieSecondaire Numéro dans la voie de l'adresse secondaire.
    */
   public void setNumeroVoieSecondaire(String numeroVoieSecondaire) {
      this.adresseSecondaire.setNumeroVoie(numeroVoieSecondaire);
   }

   /** 
    * Renvoyer l'indice de répétition du numéro de rue dans l'adresse principale.
    * @return Indice de répétition
    */
   public String getIndiceRepetition() {
      return this.adressePrincipale.getRepetitionNumeroDansVoie();
   }

   /** 
    * Fixer l'indice de répétition du numéro de rue dans l'adresse principale.
    * @param indiceRepetition Indice de répétition.
    */
   public void setIndiceRepetition(String indiceRepetition) {
      this.adressePrincipale.setRepetitionNumeroDansVoie(indiceRepetition);
   }

   /** 
    * Renvoyer l'indice de répétition du numéro de rue dans l'adresse secondaire.
    * @return Indice de répétition
    */
   public String getIndiceRepetitionSecondaire() {
      return this.adresseSecondaire.getRepetitionNumeroDansVoie();
   }

   /** 
    * Fixer l'indice de répétition du numéro de rue dans l'adresse secondaire.
    * @param indiceRepetitionSecondaire Indice de répétition
    */
   public void setIndiceRepetitionSecondaire(String indiceRepetitionSecondaire) {
      this.adresseSecondaire.setRepetitionNumeroDansVoie(indiceRepetitionSecondaire);
   }

   /** 
    * Renvoyer le type de voie de localisation de l'établissement.
    * @return Type de voie de localisation de l'établissement.
    */
   public TypeDeVoie asTypeDeVoie() {
      return TypeDeVoie.valueFromAbreviation(this.adressePrincipale.getTypeDeVoie());
   }

   /** 
    * Renvoyer le type de voie de localisation de l'établissement.
    * @return Type de voie de localisation de l'établissement.
    */
   public String getTypeDeVoie() {
      return this.adressePrincipale.getTypeDeVoie();
   }

   /** 
    * Fixer le type de voie secondaire de localisation de l'établissement.
    * @param typeVoie Type de voie de localisation de l'établissement.
    */
   public void setTypeDeVoie(String typeVoie) {
      this.adressePrincipale.setTypeDeVoie(typeVoie);
   }

   /** 
    * Renvoyer le type de voie de localisation de l'établissement.
    * @return Type de voie de localisation de l'établissement.
    */
   public String getTypeDeVoieSecondaire() {
      return this.adresseSecondaire.getTypeDeVoie();
   }

   /** 
    * Fixer le type de voie secondaire de localisation de l'établissement.
    * @param typeVoieSecondaire Type de voie de localisation de l'établissement.
    */
   public void setTypeDeVoieSecondaire(String typeVoieSecondaire) {
      this.adresseSecondaire.setTypeDeVoie(typeVoieSecondaire);
   }

   /** 
    * Renvoyer le libellé de voie de localisation de l'établissement.
    * @return Libellé de voie de localisation de l'établissement.
    */
   public String getLibelleVoie() {
      return this.adressePrincipale.getLibelleVoie();
   }

   /** 
    * Fixer le libellé de voie de localisation de l'établissement.
    * @param libelleVoie Libellé de voie de localisation de l'établissement.
    */
   public void setLibelleVoie(String libelleVoie) {
      this.adressePrincipale.setLibelleVoie(libelleVoie);
   }

   /** 
    * Renvoyer le libellé de voie secondaire de localisation de l'établissement.
    * @return Libellé de voie de localisation de l'établissement.
    */
   public String getLibelleVoieSecondaire() {
      return this.adresseSecondaire.getLibelleVoie();
   }

   /** 
    * Fixer le libellé de voie secondaire de localisation de l'établissement.
    * @param libelleVoieSecondaire Libellé de voie de localisation de l'établissement.
    */
   public void setLibelleVoieSecondaire(String libelleVoieSecondaire) {
      this.adresseSecondaire.setLibelleVoie(libelleVoieSecondaire);
   }

   /** 
    * Renvoyer le code postal.
    * @return Code postal.
    */
   public String getCodePostal() {
      return this.adressePrincipale.getCodePostal();
   }

   /** 
    * Fixer le code postal.
    * @param codePostal Code postal.
    */
   public void setCodePostal(String codePostal) {
      this.adressePrincipale.setCodePostal(codePostal);
   }

   /** 
    * Renvoyer le code postal.
    * @return Code postal.
    */
   public String getCodePostalSecondaire() {
      return this.adresseSecondaire.getCodePostal();
   }

   /** 
    * Fixer le code postal.
    * @param codePostalSecondaire Code postal.
    */
   public void setCodePostalSecondaire(String codePostalSecondaire) {
      this.adresseSecondaire.setCodePostal(codePostalSecondaire);
   }

   /** 
    * Renvoyer le Code CEDEX de l'adresse principale.
    * @return Le code cedex. 
    */
   public String getCedex() {
      return this.adressePrincipale.getCedex();
   }

   /** 
    * Fixer le Code CEDEX de l'adresse principale.
    * @param codeCedex Fixer le code cedex. 
    */
   public void setCedex(String codeCedex) {
      this.adressePrincipale.setCedex(codeCedex);
   }

   /** 
    * Renvoyer le Code CEDEX de l'adresse secondaire.
    * @return Le code cedex. 
    */
   public String getCedexSecondaire() {
      return this.adresseSecondaire.getCedex();
   }

   /** 
    * Fixer le Code CEDEX de l'adresse secondaire.
    * @param codeCedexSecondaire Fixer le code cedex. 
    */
   public void setCedexSecondaire(String codeCedexSecondaire) {
      this.adresseSecondaire.setCedex(codeCedexSecondaire);
   }
   
   /** 
    * Renvoyer le code commune de localisation de l'établissement.
    * @return Code commune.
    */
   public String getCodeCommune() {
      return this.adressePrincipale.getCodeCommune();
   }

   /** 
    * Fixer le code commune de localisation de l'établissement.
    * @param codeCommune Code commune.
    */
   public void setCodeCommune(String codeCommune) {
      this.adressePrincipale.setCodeCommune(codeCommune);
   }

   /** 
    * Renvoyer le code commune de localisation secondaire de l'établissement.
    * @return Code commune.
    */
   public String getCodeCommuneSecondaire() {
      return this.adresseSecondaire.getCodeCommune();
   }

   /** 
    * Fixer le code commune de localisation secondaire de l'établissement.
    * @param codeCommuneSecondaire Code commune.
    */
   public void setCodeCommuneSecondaire(String codeCommuneSecondaire) {
      this.adresseSecondaire.setCodeCommune(codeCommuneSecondaire);
   }

   /** 
    * Renvoyer le nom de la commune de localisation de l'établissement.
    * @return Libellé de la commune.
    */
   public String getNomCommune() {
      return this.adressePrincipale.getNomCommune();
   }

   /** 
    * Fixer le nom de la commune de localisation de l'établissement.
    * @param libelleCommune Libellé de la commune.
    */
   public void setNomCommune(String libelleCommune) {
      this.adressePrincipale.setNomCommune(libelleCommune);
   }

   /** 
    * Renvoyer le nom de la commune de localisation secondaire de l'établissement.
    * @return Libellé de la commune.
    */
   public String getNomCommuneSecondaire() {
      return this.adresseSecondaire.getNomCommune();
   }

   /** 
    * Fixer le nom de la commune de localisation secondaire de l'établissement.
    * @param libelleCommuneSecondaire Libellé de la commune.
    */
   public void setNomCommuneSecondaire(String libelleCommuneSecondaire) {
      this.adresseSecondaire.setNomCommune(libelleCommuneSecondaire);
   }

   /** 
    * Renvoyer le complément d’adresse secondaire.
    * @return Complément d’adresse secondaire.
    */
   public String getComplementAdresseSecondaire() {
      return this.adresseSecondaire.getComplementAdresse();
   }

   /** 
    * Fixer le complément d’adresse secondaire.
    * @param complementAdresseSecondaire Complément d’adresse secondaire.
    */
   public void setComplementAdresseSecondaire(String complementAdresseSecondaire) {
      this.adresseSecondaire.setComplementAdresse(complementAdresseSecondaire);
   }

   /** 
    * Renvoyer la distribution spéciale secondaire de l’établissement.
    * @return Distribution spéciale.
    */
   public String getDistributionSpecialeSecondaire() {
      return this.adresseSecondaire.getDistributionSpeciale();
   }

   /** 
    * Fixer la distribution spéciale secondaire de l’établissement.
    * @param distributionSpecialeSecondaire Distribution spéciale.
    */
   public void setDistributionSpecialeSecondaire(String distributionSpecialeSecondaire) {
      this.adresseSecondaire.setDistributionSpeciale(distributionSpecialeSecondaire);
   }

   /** 
    * Renvoyer le libellé du code cedex.
    * @return Libellé du code cedex.
    */
   public String getLibelleCedexSecondaire() {
      return this.adresseSecondaire.getLibelleCedex();
   }

   /** 
    * Fixer le libellé du code cedex.
    * @param libelleCedexSecondaire Libellé du code cedex.
    */
   public void setLibelleCedexSecondaire(String libelleCedexSecondaire) {
      this.adresseSecondaire.setLibelleCedex(libelleCedexSecondaire);
   }

   /** 
    * Renvoyer le code pays pour un établissement situé à l’étranger.
    * @return Code Pays.
    */
   public String getCodePaysEtrangerSecondaire() {
      return this.adresseSecondaire.getCodePaysEtranger();
   }

   /** 
    * Fixer le code pays pour un établissement situé à l’étranger.
    * @param codePaysEtrangerSecondaire Code Pays.
    */
   public void setCodePaysEtrangerSecondaire(String codePaysEtrangerSecondaire) {
      this.adresseSecondaire.setCodePaysEtranger(codePaysEtrangerSecondaire);
   }

   /**
    * Renvoyer la dénomination de l'établissement.
    * @return Dénomination.
    */
   public String getDenominationEtablissement() {
      return this.denominationEtablissement;
   }

   /**
    * Fixer la dénomination de l'établissement.
    * @param denomination Dénomination.
    */
   public void setDenominationEtablissement(String denomination) {
      this.denominationEtablissement = denomination;
   }

   /**
    * Renvoyer la première enseigne de l'établissement.
    * @return Première enseigne de l'établissement.
    */
   public String getEnseigne1() {
      return this.enseigne1;
   }

   /**
    * Fixer la première enseigne de l'établissement.
    * @param enseigne Première enseigne de l'établissement.
    */
   public void setEnseigne1(String enseigne) {
      this.enseigne1 = enseigne;
   }

   /**
    * Renvoyer la seconde enseigne de l'établissement.
    * @return Seconde enseigne de l'établissement.
    */
   public String getEnseigne2() {
      return this.enseigne2;
   }

   /**
    * Fixer la seconde enseigne de l'établissement.
    * @param enseigne Seconde enseigne de l'établissement.
    */
   public void setEnseigne2(String enseigne) {
      this.enseigne2 = enseigne;
   }

   /**
    * Renvoyer la troisième enseigne de l'établissement.
    * @return Troisième enseigne de l'établissement.
    */
   public String getEnseigne3() {
      return this.enseigne3;
   }

   /**
    * Fixer la troisième enseigne de l'établissement.
    * @param enseigne Troisième enseigne de l'établissement.
    */
   public void setEnseigne3(String enseigne) {
      this.enseigne3 = enseigne;
   }

   /** 
    * Renvoyer le libellé du pays pour un établissement situé à l’étranger.
    * @return Libellé du pays.
    */
   public String getNomPaysEtrangerSecondaire() {
      return this.adresseSecondaire.getNomPaysEtranger();
   }

   /** 
    * Renvoyer le libellé du pays pour un établissement situé à l’étranger.
    * @param libellePaysEtrangerSecondaire Libellé du pays.
    */
   public void setNomPaysEtrangerSecondaire(String libellePaysEtrangerSecondaire) {
      this.adresseSecondaire.setNomPaysEtranger(libellePaysEtrangerSecondaire);
   }
   
   
   /**
    * Renvoyer le code de l'activité exercée par l'artisan inscrit au registre des métiers (RM) (APRM).
    * @return code de l'activité exercée.
    */
   public String getActiviteArtisanRegistreDesMetiers() {
      return this.apeRegistreDesMetiers;
   }

   /**
    * Fixer le code de l'activité exercée par l'artisan inscrit au registre des métiers (RM) (APRM).
    * @param codeActivite code de l'activité exercée.
    */
   public void setActiviteArtisanRegistreDesMetiers(String codeActivite) {
      this.apeRegistreDesMetiers = codeActivite;
   }

   /**
    * Renvoyer d'éventuelles anomalies constatées.
    * @return Anomalies.
    */
   public Anomalies anomalies() {
      Anomalies anomalies = new Anomalies();
      
      // S'il n'y a aucune ligne d'adresse, ou si la ligne d'adresse de l'établissement n'a pas de commune, il sera inexploitable.
      if (this.adressePrincipale.isEmpty() || this.adressePrincipale.getCodeCommune() == null) {
         anomalies.declare(SeveriteAnomalie.ERREUR,
            "L''établissement de code SIRET {0} a {1} adresses associées. Dans la première d''entre-elles, il n''est pas possible de trouver de code commune disant où se trouve cet établissement.",
            getSiret(), 0);
      }
      
      return anomalies;
   }

   /**
    * @see java.lang.Comparable#compareTo(java.lang.Object)
    */
   @Override
   public int compareTo(Etablissement o) { //NOSONAR Pas besoin d'equals
      return getSiret().compareTo(o.getSiret());
   }
   
   /**
    * @see ObjetMetierIdentifiable#toString()
    */
   @Override
   public String toString() {
      String format = "'{'{0}, activité au registre des métiers : {1}, date de création de l''établissement : {2}, établissement siège : {3}, " +
         "dénomination de l''établissement : {6}, enseigne 1 : {4}, 2 : {7}, 3 : {8}, adresses : {5}'}'";

      return MessageFormat.format(format, super.toString(),
         this.apeRegistreDesMetiers, asDateCreationEtablissement(), this.siege, this.enseigne1, this.adressePrincipale,
         this.denominationEtablissement, this.enseigne2, this.enseigne3);
   }
}
