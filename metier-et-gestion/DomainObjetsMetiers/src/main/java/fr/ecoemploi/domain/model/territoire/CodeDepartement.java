package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import fr.ecoemploi.domain.utils.autocontrole.*;
import fr.ecoemploi.domain.utils.objets.ObjetMetierIdentifiable;

/**
 * Un code de département.
 * @author Marc LE BIHAN
 */
public class CodeDepartement extends Id {
   /** Code de département INSEE */
   @Serial
   private static final long serialVersionUID = 3687574686114772527L;

   /**
    * Construire un id null.
    */
   public CodeDepartement() {
   }

   /**
    * Construire un id alimenté.
    * @param code Code de l'id.
    */
   public CodeDepartement(String code) {
      super(code);
   }

   /**
    * Construire un code département par copie.
    * @param codeDepartement Code département.
    */
   public CodeDepartement(CodeDepartement codeDepartement) {
      super(codeDepartement);
   }

   /**
    * Renvoyer le code région d'un code département.
    * @return Code région ou null s'il ne peut être déterminé (code trop court).
    */
   public CodeRegion codeRegion() {
      if (porteCodeRegion() == false)
         return null;

      return new CodeRegion(getId().length() < 2 ? getId() : getId().substring(0, 2));
   }

   /**
    * Renvoyer le numéro du département.
    * @return Numéro de département ou null s'il ne peut être déterminé (code trop court).
    */
   public String numeroDepartement() {
      if (getId() == null)
         return null;

      if (porteCodeRegion()) {
         return estDepartementOutremer() ? getId().substring(2, 5) : getId().substring(2, 4);
      }
      else {
         return estDepartementOutremer() ? getId().substring(0, 3) : getId().substring(0, 2);
      }
   }

   /**
    * Déterminer si un département est d'outremer.
    * @param numeroDepartement Numéro de département.
    * @return true s'il l'est.
    */
   public static boolean estDepartementOutremer(String numeroDepartement) {
      return numeroDepartement.startsWith("97", 2);
   }

   /**
    * Déterminer si le département est d'outremer.
    * @return true s'il l'est.
    */
   public boolean estDepartementOutremer() {
      return estDepartementOutremer(getId());
   }

   /**
    * Déterminer si ce code département porte un code région ou non.
    * @return true s'il a un code région.
    */
   public boolean porteCodeRegion() {
      return (getId() != null && getId().length() >= 4);
   }

   /**
    * Renvoyer les codes départements distincts présents dans une liste de codes communes
    * @param codesCommunes Codes communes dont on veut les codes départements
    * @return Ensemble de codes départements distincts.
    */
   public static Set<String> codesDepartements(String... codesCommunes) {
      Objects.requireNonNull(codesCommunes, "La liste des codes communes dont on veut les départements distincts ne peut pas valoir null");

      Set<String> codesDepartements = new HashSet<>();

      for(String codeCommune : codesCommunes) {
         codesDepartements.add(new CodeCommune(codeCommune).numeroDepartement());
      }

      return codesDepartements;
   }

   /**
    * Renvoyer les codes départements distincts présents dans une liste de codes communes
    * @param codesCommunes Codes communes dont on veut les codes départements
    * @return Ensemble de codes départements distincts.
    */
   public static Set<CodeDepartement> codesDepartements(Collection<CodeCommune> codesCommunes) {
      Objects.requireNonNull(codesCommunes, "La collection de codes communes dont on veut les départements distincts ne peut pas valoir null");

      Set<CodeDepartement> codesDepartements = new HashSet<>();

      for(CodeCommune codeCommune : codesCommunes) {
         String departement = new CodeCommune(codeCommune).numeroDepartement();
         codesDepartements.add(new CodeDepartement(departement));
      }

      return codesDepartements;
   }

   /**
    * @see ObjetMetierIdentifiable#anomalies(Anomalies)
    */
   @Override
   public void anomalies(Anomalies anomalies) {
      super.anomalies(anomalies);

      // Le code département doit être alimenté.
      if (StringUtils.isBlank(getId()))
         anomalies.declare(SeveriteAnomalie.ERREUR, "Le département n''est pas renseigné.");
      else {
         // Le code département doit être associé à un code région.
         if (codeRegion() == null || StringUtils.isBlank(codeRegion().getId()))
            anomalies.declare(SeveriteAnomalie.AVERTISSEMENT, "Le département {0} est dépourvu de région.", this);
         else {
            // La région du département doit être valide.
            anomalies.examine(this.codeRegion());
         }
      }
   }
}
