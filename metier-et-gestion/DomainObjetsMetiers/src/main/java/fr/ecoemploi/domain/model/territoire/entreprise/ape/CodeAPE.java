package fr.ecoemploi.domain.model.territoire.entreprise.ape;

import java.io.Serial;
import java.text.MessageFormat;
import java.util.*;

import org.apache.commons.lang3.*;

import fr.ecoemploi.domain.utils.autocontrole.Id;

/**
 * Un code APE.
 * @author Marc LE BIHAN
 */
public class CodeAPE extends Id {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -9078415480878725641L;
   
   /** Niveaux NAF. */
   private static final NiveauxNAF NIVEAUX_NAF = new NiveauxNAF();
   
   /**
    * Construire un code vide.
    */
   public CodeAPE() {
      super();
   }
   
   /**
    * Construire un code APE.
    * @param code Code APE.
    */
   public CodeAPE(String code) {
      super(code);
   }
   
   /**
    * Renvoyer le code NAF d'un certain niveau de ce code APE.
    * @param niveau rang Niveau NAF cible.
    * @return Niveau NAF.
    * @throws NiveauNAFInexistantException si le code APE porte est invalide.
    */
   public NiveauNAF toNAF(int niveau) throws NiveauNAFInexistantException {
      return toNAF(getId(), niveau);
   }
   
   /**
    * Renvoyer le code NAF d'un certain niveau de ce code APE.
    * @param codeAPE Code APE.
    * @param niveau rang Niveau NAF cible.
    * @return Niveau NAF.
    * @throws NiveauNAFInexistantException si le code APE porte est invalide.
    */
   public static NiveauNAF toNAF(String codeAPE, int niveau) throws NiveauNAFInexistantException {
      Objects.requireNonNull(codeAPE, "Le code APE/NAF ne peut pas valoir null.");
      
      // Pour obtenir le niveau 1 (section) d'un code, il faut demander son niveau 2 puis prendre son parent.
      String code = niveau > 1 ? NiveauxNAF.getPartieDiscriminante(codeAPE, niveau) : NiveauxNAF.getPartieDiscriminante(codeAPE, 2);
      NiveauNAF candidat = NIVEAUX_NAF.getTousNAF(niveau > 1 ? niveau : 2).get(code);
      
      // Si notre code n'a pas été trouvé, lever une exception.
      if (candidat == null) {
         String message = MessageFormat.format("Le code APE ''{2}'' désigne un code NAF ''{0}'' de niveau {1} qui est inconnu.",
            code, niveau > 1 ? niveau : 2, codeAPE);

         throw new NiveauNAFInexistantException(message);
      }
      
      if (niveau == 1) {
         candidat = candidat.getParent();
      }
      
      return candidat;
   }   

   /**
    * Déterminer si un code NAF est valide.
    * @param autoriserNull true s'il l'on accepte qu'il vaille null.
    * @return true s'il l'est.
    */
   public boolean valide(boolean autoriserNull) {
      return valide(getId(), autoriserNull);
   }

   /**
    * Déterminer si un code NAF est valide.
    * @param codeAPE Code APE.
    * @param autoriserNull true s'il l'on accepte qu'il vaille null.
    * @return true s'il l'est.
    */
   public static boolean valide(String codeAPE, boolean autoriserNull) {
      // Il ne doit pas être blanc, valoir "00.00Z", avoir une longueur de six caractères, un caractère final et un point central.
      if (codeAPE == null && autoriserNull) {
         return true;
      }
      
      @SuppressWarnings("null") // Le test de nullité est fait par isNotBlank.
      boolean ok = StringUtils.isNotBlank(codeAPE) && "00.00Z".equals(codeAPE) == false && codeAPE.length() == 6 && codeAPE.charAt(2) == '.' && Character.isUpperCase(codeAPE.charAt(5));
      
      if (ok == false) {
         return false;
      }
      
      // Le point central et le caractère final mis à part, le reste doit être numérique.
      char[] caracteres = new char[4];
      caracteres[0] = codeAPE.charAt(0);
      caracteres[1] = codeAPE.charAt(1);
      caracteres[2] = codeAPE.charAt(3);
      caracteres[3] = codeAPE.charAt(4);
      
      String partieNumerique = new String(caracteres);
      ok = StringUtils.isNumeric(partieNumerique);
      
      if (ok == false) {
         return false;
      }
      
      // Le code NAF de niveau 2 doit être valide.
      try {
         toNAF(codeAPE, 2);
         return true;
      }
      catch(@SuppressWarnings("unused") NiveauNAFInexistantException e) {
         return false;
      }
   }
   
   /**
    * Renvoyer la section d'un code APE.
    * @param codeAPE Section.
    * @return Section.
    * @throws NiveauNAFInexistantException si le code section n'existe pas.
    */
   public static String toSection(String codeAPE) throws NiveauNAFInexistantException {
      Objects.requireNonNull(codeAPE, "Le code APE ne peut pas valoir null.");
      return toNAF(codeAPE, 1).getCodeNAF();      
   }
}
