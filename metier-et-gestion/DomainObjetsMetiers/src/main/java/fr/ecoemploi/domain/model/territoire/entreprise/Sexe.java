package fr.ecoemploi.domain.model.territoire.entreprise;

/**
 * Sexe d'une personne.
 * @author Marc LE BIHAN
 */
public enum Sexe {
   /** Féminin */
   F("Féminin"),

   /** Masculin */
   M("Masculin");
   
   /** Libellé. */
   private final String libelle;

   /**
    * Construire un sexe de personne.
    */
   Sexe(String libelle) {
      this.libelle = libelle;
   }
   
   /**
    * Renvoyer le libellé associé à ce sexe de personne.
    * @return Libellé.
    */
   public String getLibelle() {
      return this.libelle;
   }
   
   /**
    * @see java.lang.Enum#toString()
    */
   @Override
   public String toString() {
      return this.libelle;
   }
}
