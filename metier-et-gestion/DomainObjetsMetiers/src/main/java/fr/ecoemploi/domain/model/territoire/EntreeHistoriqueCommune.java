package fr.ecoemploi.domain.model.territoire;

import org.apache.commons.lang3.builder.*;

import java.io.Serial;
import java.time.LocalDate;
import java.util.Arrays;

import fr.ecoemploi.domain.utils.objets.ObjetMetier;

/**
 * Entrée d'historique de commune.
 * @author Marc Le Bihan
 */
public class EntreeHistoriqueCommune extends ObjetMetier {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 1118674612959046799L;

   /** Type d'évènement communal. */
   private TypeEvenementCommunal typeEvenementCommunal;

   /** Date d'effet (AAAA-MM-JJ) */
   private LocalDate dateEffetEvenement;

   /** Commune avant. */
   private CommuneHistorique communeAvant;

   /** Commune après. */
   private CommuneHistorique communeApres;

   /**
    * Construire une entrée d'historique de commune.
    */
   public EntreeHistoriqueCommune() {
   }

   /**
    * Construire une entrée d'historique de commune.
    * @param typeEvenement Type d'évènement communal.
    * @param dateEffetEvenement Date d'effet (AAAA-MM-JJ)
    * @param communeAvant La commune qui existait avant.
    * @param communeApres La commune qui fût ensuite.
    */
   public EntreeHistoriqueCommune(int typeEvenement, LocalDate dateEffetEvenement, CommuneHistorique communeAvant, CommuneHistorique communeApres) {
      this.typeEvenementCommunal = Arrays.stream(TypeEvenementCommunal.values()).filter(type -> type.getType() == typeEvenement).findFirst().orElse(null);
      this.dateEffetEvenement = dateEffetEvenement;
      this.communeAvant = communeAvant;
      this.communeApres = communeApres;
   }

   /**
    * Renvoyer la commune qui existait avant.
    * @return Commune qui existait avant.
    */
   public CommuneHistorique getCommuneAvant() {
      return this.communeAvant;
   }

   /**
    * Fixer la commune qui existait avant.
    * @param communeAvant Commune qui existait.
    */
   public void setCommuneAvant(CommuneHistorique communeAvant) {
      this.communeAvant = communeAvant;
   }

   /**
    * Renvoyer la commune qui exista après l'évènement.
    * @return Commune qui exista après.
    */
   public CommuneHistorique getCommuneApres() {
      return this.communeApres;
   }

   /**
    * Fixer la commune qui exista après l'évènement.
    * @param communeApres Commune qui exista après.
    */
   public void setCommuneApres(CommuneHistorique communeApres) {
      this.communeApres = communeApres;
   }

   /**
    * Renvoyer la date d'effet.
    * @return Date d'effet.
    */
   public LocalDate getDateEffetEvenement() {
      return this.dateEffetEvenement;
   }

   /**
    * Fixer la date d'effet.
    * @param dateEffetEvenement Date d'effet.
    */
   public void setDateEffetEvenement(LocalDate dateEffetEvenement) {
      this.dateEffetEvenement = dateEffetEvenement;
   }

   /**
    * Renvoyer le type d'évènement qui a affecté la commune.
    * @return Type d'évènement.
    */
   public TypeEvenementCommunal getTypeEvenement() {
      return this.typeEvenementCommunal;
   }

   /**
    * Fixer le type d'évènement qui a affecté la commune.
    * @param typeEvenement Type d'évènement.
    */
   public void setTypeEvenement(TypeEvenementCommunal typeEvenement) {
      this.typeEvenementCommunal = typeEvenement;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean equals(Object o) {
      if (o instanceof EntreeHistoriqueCommune entreeHistorique) {

         EqualsBuilder equals = new EqualsBuilder()
            .append(this.typeEvenementCommunal, entreeHistorique.typeEvenementCommunal)
            .append(this.dateEffetEvenement, entreeHistorique.dateEffetEvenement)
            .append(this.communeAvant, entreeHistorique.communeAvant)
            .append(this.communeApres, entreeHistorique.communeApres);

         return equals.isEquals();
      }

      return false;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public int hashCode() {
      HashCodeBuilder hash = new HashCodeBuilder()
         .append(this.typeEvenementCommunal)
         .append(this.dateEffetEvenement)
         .append(this.communeAvant)
         .append(this.communeApres);

      return hash.toHashCode();
   }
}
