package fr.ecoemploi.domain.model.territoire.entreprise;

import fr.ecoemploi.domain.utils.objets.MetierException;
import fr.ecoemploi.domain.model.territoire.SIRET;

import java.io.Serial;

/**
 * Exception levée lorsqu'un établissement n'existe pas.
 * @author Marc LE BIHAN
 */
public class EtablissementInexistantException extends MetierException {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 1353323206921636786L;

   /** SIRET de l'établissement absent. */
   private SIRET siret;
   
   /** Année de recherche dans les bases SIRENE. */
   private int annee;
   
   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    */
   public EtablissementInexistantException(String message) {
      super(message);
   }
   
   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception.
    */
   public EtablissementInexistantException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception.
    * @param siret Code SIRET de l'établissement manquant.
    * @param annee Année de recherche dans les bases SIRENE.
    */
   public EtablissementInexistantException(SIRET siret, int annee, String message, Throwable cause) {
      super(message, cause);
      this.siret = siret;
      this.annee = annee;
   }
   
   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param siret Code SIRET de l'établissement manquant.
    * @param annee Année de recherche dans les bases SIRENE.
    */
   public EtablissementInexistantException(SIRET siret, int annee, String message) {
      this(siret, annee, message, null);
   }

   /**
    * Renvoyer l'année de recherche.
    * @return Année.
    */
   public int getAnnee() {
      return this.annee;
   }

   /**
    * Renvoyer le code SIREN de l'entreprise manquante.
    * @return SIREN.
    */
   public SIRET getSiret() {
      return this.siret;
   }
}
