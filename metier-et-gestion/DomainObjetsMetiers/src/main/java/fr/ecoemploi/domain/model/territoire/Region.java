package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;
import java.text.MessageFormat;

import fr.ecoemploi.domain.utils.objets.ObjetMetierIdentifiable;

/**
 * Une région ou province.
 * @author Marc LE BIHAN.
 */
public class Region extends ObjetMetierIdentifiable<CodeRegion> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -4677590362008767402L;

   /** Nom de la région. */
   private String nom;

   /** Nom en majuscule. */
   private String nomMajuscules;

   /** Pays auquel appartient cette région. */
   private Pays pays;

   /** Code INSEE de la région. */
   private CodeRegion codeRegion;

   /** Code de la commune de chef lieu. */
   private CodeCommune communeChefLieu;

   /** Type et nom charnière. */
   private ArticleEtCharniereINSEE typeNomEtCharniere;

   /**
    * Construire une région
    */
   public Region() {
   }

   /**
    * Construire une région par copie.
    * @param region Région à copier.
    */
   public Region(Region region) {
      super(region);
      this.setPays(region.pays != null ? new Pays(region.pays) : null);
      this.setCodeRegion(region.codeRegion != null ? new CodeRegion(region.codeRegion) : null);
      this.setCodeCommuneChefLieu(region.communeChefLieu != null ? new CodeCommune(region.communeChefLieu) : null);
      this.nom = region.nom;
      this.nomMajuscules = region.nomMajuscules;
      this.typeNomEtCharniere = region.typeNomEtCharniere;
   }

   /**
    * Renvoyer le code de la région.
    * @return Code de la région.
    */
   public CodeRegion getCodeRegion() {
      return this.codeRegion;
   }

   /**
    * Renvoyer le code de la commune de chef lieu.
    * @return Code de la commune.
    */
   public CodeCommune getCodeCommuneChefLieu() {
      return this.communeChefLieu;
   }

   /**
    * Renvoyer le nom de la région.
    * @return Nom de la région.
    */
   public String getNom() {
      return this.nom;
   }

   /**
    * Renvoyer le nom de la région en majuscules.
    * @return Nom de la région.
    */
   public String getNomMajuscules() {
      return this.nomMajuscules;
   }

   /**
    * Renvoyer le pays dans lequel se situe cette région.
    * @return Pays.
    */
   public Pays getPays() {
      return this.pays;
   }

   /**
    * Renvoyer l'article et la charnière utilisés pour désigner la région.
    * @return Type de nom et charnière.
    */
   public ArticleEtCharniereINSEE getTypeNomEtCharniere() {
      return this.typeNomEtCharniere;
   }

   /**
    * Fixer le code de la région.
    * @param code Code de la région.
    */
   public void setCodeRegion(CodeRegion code) {
      this.codeRegion = code;
   }

   /**
    * Fixer le code de la commune de chef lieu.
    * @param code Code de la commune de chef lieu.
    */
   public void setCodeCommuneChefLieu(CodeCommune code) {
      this.communeChefLieu = code;
   }

   /**
    * Fixer le nom de la région.
    * @param nomRegion Nom de la région.
    */
   public void setNom(String nomRegion) {
      this.nom = nomRegion;
   }

   /**
    * Fixer le nom de la région en majuscules.
    * @param nomRegion Nom de la région en majuscules.
    */
   public void setNomMajuscules(String nomRegion) {
      this.nomMajuscules = nomRegion;
   }

   /**
    * Fixer le pays dans lequel se trouve cette région.
    * @param p Pays.
    */
   public void setPays(Pays p) {
      this.pays = p;
   }

   /**
    * Fixer le type de charnière à utiliser pour nommer la ville.
    * @param type Type de charnière.
    */
   public void setTypeNomEtCharniere(ArticleEtCharniereINSEE type) {
      this.typeNomEtCharniere = type;
   }

   /**
    * @see ObjetMetierIdentifiable#toString()
    */
   @Override
   public String toString() {
      String format = "'{'{0}, Nom : {1}, Nom en majuscules : {2}, Pays : {3}, Code région : {4}, " +
         "Commune chef lieu : {5}, Article et charnière : {6}'}'";

      return MessageFormat.format(format, super.toString(), this.nom, this.nomMajuscules, this.pays, this.codeRegion,
         this.communeChefLieu, this.typeNomEtCharniere);
   }
}
