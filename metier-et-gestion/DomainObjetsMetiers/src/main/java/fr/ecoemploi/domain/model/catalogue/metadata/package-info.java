/**
 * Métadonnées sur une source de données.
 * @author Marc LE BIHAN
 */
package fr.ecoemploi.domain.model.catalogue.metadata;