package fr.ecoemploi.domain.model.territoire;

import java.text.*;
import java.util.*;

import org.apache.commons.lang3.*;

/**
 * Tri de pays par nom de pays.
 */
public class TriParNomDePays implements Comparator<Pays> {
   /** Locale utilisée pour le tri. */
   private final Locale localePourTri;

   /** Ordre de tri. */
   private final Collator collator;

   /**
    * Construire un tri par nom.
    * @param locale Locale à utiliser.
    */
   public TriParNomDePays(Locale locale) {
      Validate.notNull(locale, "La locale ne peut pas valoir null.");
      this.localePourTri = locale;

      this.collator = Collator.getInstance(locale);
      this.collator.setStrength(Collator.PRIMARY);
   }

   /**
    * Compare two countries.
    * @param a First country.
    * @param b Second country.
    * @return Order of their names.
    */
   @Override
   public int compare(Pays a, Pays b) {
      String countryNameA = new Locale("", a.getCodePays()).getDisplayCountry(this.localePourTri);
      String countryNameB = new Locale("", b.getCodePays()).getDisplayCountry(this.localePourTri);

      return(this.collator.compare(countryNameA, countryNameB));
   }
}
