package fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels;

import java.io.Serial;
import java.util.*;

/**
 * Comptes avec montant, montant par habitant, strate, taux voté et taux moyen strate.
 * @author Marc Le Bihan
 */
public class ComptesAvecStrateEtTauxVoteEtStrate extends ComptesAvecTauxVote {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3205978414270636748L;
   
   /** Moyenne de strate */
   private Double moyenneStrate;

   /** Taux de la strate */
   private Double tauxStrate;

   /**
    * Construire de comptes montant (total ou base), par habitant, moyenne strate par habitant + taux voté, taux strate.
    */
   public ComptesAvecStrateEtTauxVoteEtStrate() {
   }

   /**
    * Construire de comptes montant (total ou base), par habitant, moyenne strate par habitant + taux voté, taux strate.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    */
   public ComptesAvecStrateEtTauxVoteEtStrate(String libelleAnalyse) {
      super(libelleAnalyse);
   }

   /**
    * Construire de comptes montant (total ou base), par habitant, moyenne strate par habitant + taux voté, taux strate.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    * @param montant Montant (total ou base).
    * @param parHabitant Montant par habitant.
    * @param moyenneStrate Moyenne de la strate par habitant.
    * @param tauxVote Taux voté.
    * @param tauxStrate Taux strate.
    */
   public ComptesAvecStrateEtTauxVoteEtStrate(String libelleAnalyse, Double montant, Double parHabitant, Double moyenneStrate, Double tauxVote, Double tauxStrate) {
      super(libelleAnalyse, montant, parHabitant, tauxVote);
      this.moyenneStrate = moyenneStrate;
      this.tauxStrate = tauxStrate;
   }

   /**
    * Renvoyer la moyenne de strate.
    * @return Moyenne de strate.
    */
   public Double getMoyenneStrate() {
      return this.moyenneStrate;
   }

   /**
    * Fixer la moyenne de strate.
    * @param moyenneStrate Moyenne de strate.
    */
   public void setMoyenneStrate(Double moyenneStrate) {
      this.moyenneStrate = moyenneStrate;
   }

   /**
    * Renvoyer le taux de la strate.
    * @return Taux de la strate.
    */
   public Double getTauxStrate() {
      return this.tauxStrate;
   }

   /**
    * Fixer le taux de la strate.
    * @param taux Taux de la strate 
    */
   public void setTauxStrate(Double taux) {
      this.tauxStrate = taux;
   }
   
   /**
    * Formatter une ligne texte pour qu'elle se place dans un tableau textuel d'analyse des équilibres financiers fondamentaux.
    * @return Ligne formatée. 
    */
   @Override
   public String toStringAnalyseEquilibresFinanciersFondamentaux() {
      final String FMT_NOMBRE_ENTIER = "%10d\t";
      StringBuilder format = new StringBuilder();
      List<Object> valeurs = new ArrayList<>();

      format.append(getMontant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getMontant() != null ? Math.round(getMontant()) : "");

      format.append(getParHabitant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getParHabitant() != null ? Math.round(getParHabitant()) : "");

      format.append(getMoyenneStrate() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getMoyenneStrate() != null ? Math.round(getMoyenneStrate()) : "");

      format.append("%s\t");
      valeurs.add(getLibelleAnalyse());

      format.append(getTauxVote() != null ? "%10.2f\t" : "%s\t");
      valeurs.add(getTauxVote() != null ? getTauxVote() : "");

      format.append(getTauxStrate() != null ? "%10.2f" : "%s");
      valeurs.add(getTauxStrate() != null ? getTauxStrate() : "");

      return String.format(format.toString(), valeurs.toArray());
   }
}
