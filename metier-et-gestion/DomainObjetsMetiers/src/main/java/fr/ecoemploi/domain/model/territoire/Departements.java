package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;
import java.util.*;

import fr.ecoemploi.domain.utils.objets.*;

/**
 * Une liste de départements.
 * @author Marc LE BIHAN
 */
public class Departements extends AbstractObjetsMetiersIndexes<CodeDepartement, Departement> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -8945913799872175089L;

   /**
    * Construire une liste de départements vides.
    */
   public Departements() {
   }

   /**
    * Construire une liste de départements par copie.
    * @param departements Départements.
    */
   public Departements(Departements departements) {
      this(departements, false);
   }

   /**
    * Construire une liste de départements par copie.
    * @param departements Départements.
    * @param deep true si la copie doit aussi faire une copie complète des objets département<br>
    *    false, si elle n'en prend que les pointeurs.
    */
   public Departements(Departements departements, boolean deep) {
      for(Departement departement : departements) {
         put(departement.getCodeDepartement(), deep ? new Departement(departement) : departement);
      }
   }

   /**
    * Comparateur avec Locale.
    */
   static class ComparateurLocale extends AbstractComparatorLocale implements Comparator<Departement> {
      /**
       * Construire un comparateur.
       * @param locale Locale.
       */
      public ComparateurLocale(Locale locale) {
         super(locale);
      }

      /**
       * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
       */
      @Override
      public int compare(Departement a, Departement b) {
         return this.collator.compare(a.getNom(), b.getNom());
      }
   }

   /**
    * Trier la liste des départements.
    * @param locale Locale.
    */
   public void sort(Locale locale) {
      Objects.requireNonNull(locale, "La locale pour le tri ne peut pas valoir null.");
      sort(this, new ComparateurLocale(locale));
   }
}
