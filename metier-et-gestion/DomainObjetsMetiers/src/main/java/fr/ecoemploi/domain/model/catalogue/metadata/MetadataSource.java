package fr.ecoemploi.domain.model.catalogue.metadata;

import java.io.Serial;
import java.net.*;
import java.text.*;
import java.time.*;

import fr.ecoemploi.domain.utils.objets.ObjetMetier;

/**
 * Metadata d'une source de données. 
 * @author Marc LE BIHAN
 */
public class MetadataSource extends ObjetMetier {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5330066831105022202L;

   /** Nom public de la source de données. */
   private String nomPublic;
   
   /** Description du contenu. */
   private String description;
   
   /** URL (lien de téléchargement direct). */
   private URL source;

   /** URL du site d'origine. */
   private URL urlSiteOrigine;
   
   /** Origine de l'information. */
   private String origine;
   
   /** Date de publication. */
   private LocalDate datePublication;
   
   /** Période sur laquelle porte ces données. */
   private Periode periode;
   
   /** Date d'effet des données (alternative à la période). */
   private LocalDate dateEffet;
   
   /** Début ou fin de période (alternative à la période). */
   private DebutOuFinPeriode debutOuFinPeriode;
   
   /** Périodicité (périodicité). */
   private PeriodiciteSource periodicite;
   
   /** Nature physique de la source. */
   private NaturePhysiqueSource naturePhysique;
   
   /** Nature de la donnée. */
   private NatureDonnee natureDonnee;

   /**
    * Construire une méta-datasource
    */
   public MetadataSource() {
   }

   /**
    * Constructeur par copie.
    * @param m Méta donnée source à copier.
    */
   public MetadataSource(MetadataSource m) {
      super(m);

      // Clonage de l'URL source.
      if (m.getSource() != null) {
         try {
            URL url = new URL(m.getSource().toString());
            this.setSource(url);
         }
         catch(MalformedURLException e) {
            throw new RuntimeException("Incident lors de la copie de l'URL source : " + e.getMessage());
         }
      }

      // Clonage de l'URL site d'origine.
      if (m.getUrlSiteOrigine() != null) {
         try {
            URL url = new URL(m.getUrlSiteOrigine().toString());
            this.setUrlSiteOrigine(url);
         }
         catch(MalformedURLException e) {
            throw new RuntimeException("Incident lors de la copie de l'URL du site d'origine : " + e.getMessage());
         }
      }

      this.setDateEffet(m.dateEffet != null ? LocalDate.parse(m.dateEffet.toString()) : null);
      this.setDatePublication(m.datePublication != null ? LocalDate.parse(m.datePublication.toString()) : null);
      this.setPeriode(m.periode != null ? new Periode(m.periode) : null);

      this.nomPublic = m.nomPublic;
      this.description = m.description;
      this.origine = m.origine;
      this.datePublication = m.datePublication;
      this.dateEffet = m.dateEffet;
      this.debutOuFinPeriode = m.debutOuFinPeriode;
      this.periodicite = m.periodicite;
      this.naturePhysique = m.naturePhysique;
      this.natureDonnee = m.natureDonnee;
   }

   /**
    * Renvoyer le nom public de la source.
    * @return Nom public.
    */
   public String getNomPublic() {
      return this.nomPublic;
   }

   /**
    * Fixer le nom public de la source.
    * @param nomPublic Nom public.
    */
   public void setNomPublic(String nomPublic) {
      this.nomPublic = nomPublic;
   }

   /**
    * Renvoyer la description de la source de données.
    * @return Description.
    */
   public String getDescription() {
      return this.description;
   }

   /**
    * Fixer la description de la source de données.
    * @param description Description.
    */
   public void setDescription(String description) {
      this.description = description;
   }

   /**
    * Renvoyer l'URL de la source de données : le lien précis d'où elle a été téléchargée.
    * @return Lien de téléchargement direct.
    */
   public URL getSource() {
      return this.source;
   }

   /**
    * Fixer l'URL de la source de données : le lien précis d'où elle a été téléchargée.
    * @param source Lien de téléchargement direct.
    */
   public void setSource(URL source) {
      this.source = source;
   }

   /**
    * Renvoyer l'URL publique utile du site web d'origine.
    * @return URL publique du site.
    */
   public URL getUrlSiteOrigine() {
      return this.urlSiteOrigine;
   }

   /**
    * Fixer l'URL publique utile du site web d'origine.
    * @param urlSiteOrigine URL publique du site.
    */
   public void setUrlSiteOrigine(URL urlSiteOrigine) {
      this.urlSiteOrigine = urlSiteOrigine;
   }

   /**
    * Renvoyer la description de l'origine de la donnée.
    * @return Origine.
    */
   public String getOrigine() {
      return this.origine;
   }

   /**
    * Fixer la description de l'origine de la donnée.
    * @param origine Origine.
    */
   public void setOrigine(String origine) {
      this.origine = origine;
   }

   /**
    * Renvoyer la date de publication de la source de données sur le site.
    * @return Date de publication.
    */
   public LocalDate getDatePublication() {
      return this.datePublication;
   }

   /**
    * Fixer la date de publication de la source de données sur le site.
    * @param datePublication Date de publication.
    */
   public void setDatePublication(LocalDate datePublication) {
      this.datePublication = datePublication;
   }

   /**
    * Renvoyer la période sur laquelle porte ces données.
    * @return Période.
    */
   public Periode getPeriode() {
      return this.periode;
   }

   /**
    * Fixer la période sur laquelle porte ces données.
    * @param periode Période.
    */
   public void setPeriode(Periode periode) {
      this.periode = periode;
   }

   /**
    * Renvoyer la date d'effet des donnée.
    * @return Date d'effet des données.
    */
   public LocalDate getDateEffet() {
      return this.dateEffet;
   }

   /**
    * Fixer la date d'effet des donnée.
    * @param dateEffet Date d'effet des données.
    */
   public void setDateEffet(LocalDate dateEffet) {
      this.dateEffet = dateEffet;
   }

   /**
    * Renvoyer un indicateur de s'il s'agit d'une date d'effet sitée en début ou fin de période.
    * @return Indicateur de début ou fin de période.
    */
   public DebutOuFinPeriode getDebutOuFinPeriode() {
      return this.debutOuFinPeriode;
   }

   /**
    * Fixer un indicateur de s'il s'agit d'une date d'effet sitée en début ou fin de période.
    * @param debutOuFinPeriode Indicateur de début ou fin de période.
    */
   public void setDebutOuFinPeriode(DebutOuFinPeriode debutOuFinPeriode) {
      this.debutOuFinPeriode = debutOuFinPeriode;
   }

   /**
    * Renvoyer la périodicité de la source (annelle, mensuelle, inconnue).
    * @return Périodicité.
    */
   public PeriodiciteSource getPeriodicite() {
      return this.periodicite;
   }

   /**
    * Fixer la périodicité de la source (annelle, mensuelle, inconnue).
    * @param periodicite Pérodicité.
    */
   public void setPeriodicite(PeriodiciteSource periodicite) {
      this.periodicite = periodicite;
   }

   /**
    * Renvoyer la nature physique de la source.
    * @return Nature physique.
    */
   public NaturePhysiqueSource getNaturePhysique() {
      return this.naturePhysique;
   }

   /**
    * Fixer la nature physique de la source.
    * @param naturePhysique Nature physique.
    */
   public void setNaturePhysique(NaturePhysiqueSource naturePhysique) {
      this.naturePhysique = naturePhysique;
   }

   /**
    * Renvoyer la nature de la donnée.
    * @return Nature de la donnée.
    */
   public NatureDonnee getNatureDonnee() {
      return this.natureDonnee;
   }

   /**
    * Renvoyer la nature de la donnée.
    * @param natureDonnee Nature de la donnée.
    */
   public void setNatureDonnee(NatureDonnee natureDonnee) {
      this.natureDonnee = natureDonnee;
   }

   /**
    * @see ObjetMetier#toString()
    */
   @Override
   public String toString() {
      try {
         String format = "nom public : {0}, description : {1}, origine : {2} (url directe : {3}, url site public : {4}), " +
            "date de publication : {5,date,medium}, date d''effet : {6,date,medium} et périodicité : {7} avec date d''effet dans la période en position : {8}, " +
            "ou période : {9}, nature des données : {10}, nature physique de la source : {11}";

         return MessageFormat.format(format, this.nomPublic, this.description, this.origine, this.source, this.urlSiteOrigine,
            this.datePublication != null ? LocalDate.from(this.datePublication.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()) : null,
            this.dateEffet != null ? LocalDate.from(this.dateEffet.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()) : null,
            this.periodicite, this.debutOuFinPeriode, this.periode, this.natureDonnee, this.naturePhysique);
      }
      catch(RuntimeException e) {
         return MessageFormat.format("toString {0} : {1}", getClass().getSimpleName(), e.getMessage()); 
      }
   }
}
