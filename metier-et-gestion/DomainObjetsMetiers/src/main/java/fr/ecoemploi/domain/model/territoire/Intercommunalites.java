package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;
import java.text.MessageFormat;
import java.util.*;

/**
 * Un ensemble d'intercommunalités.
 * @author Marc LE BIHAN
 */
public class Intercommunalites extends LinkedHashMap<String, Intercommunalite> implements Iterable<Intercommunalite> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -1741158745944209245L;

   /**
    * Construire une liste d'intercommunalités vide.
    */
   public Intercommunalites() {
   }

   /**
    * Construire une liste d'intercommunalités par copie.
    * @param intercommunalites Intercommunalités.
    */
   public Intercommunalites(Intercommunalites intercommunalites) {
      for(Intercommunalite intercommunalite : intercommunalites) {
         put(intercommunalite.getSiren(), intercommunalite);
      }
   }

   /**
    * Ajouter une intercommunalité à la liste.
    * @param intercommunalite Intercommunalité à ajouter.
    */
   public void add(Intercommunalite intercommunalite) {
       Objects.requireNonNull(intercommunalite, "L'intercommunalité à ajouter à la liste ne peut pas valoir null.");
       Objects.requireNonNull(intercommunalite.getSiren(), "L'intercommunalité à ajouter n'a pas de SIREN, qui est l'index de la liste.");
       
       put(intercommunalite.getSiren(), intercommunalite);
   }

   /**
    * Retrouver une intercommunalité ou groupement de SIREN particulier dans cette liste.
    * @param siren SIREN recherché.
    * @return Intercommunalité.
    * @throws IntercommunaliteAbsenteDansCommuneSiegeException si l'intercommunalité ou groupement ayant le SIREN recherché est absent dans la liste.
    */
   public Intercommunalite getIntercommunalite(String siren) throws IntercommunaliteAbsenteDansCommuneSiegeException {
      Objects.requireNonNull(siren, "Le SIREN pour la recherche de l'intercommunalité ne peut pas valoir null.");

      Intercommunalite intercommunalite = get(siren);
      
      if (intercommunalite == null) {
         String message = MessageFormat.format("L''intercommunalité ou groupement de SIREN ''{0}'' n''est pas présent dans cette ensemble.", siren);
         throw new IntercommunaliteAbsenteDansCommuneSiegeException(message);
      }
      
      return intercommunalite;
   }

   /**
    * Retrouver une intercommunalité ou groupement de SIREN particulier dans cette liste.
    * @param siren SIREN recherché.
    * @return Intercommunalité.
    * @throws IntercommunaliteAbsenteDansCommuneSiegeException si l'intercommunalité ou groupement ayant le SIREN recherché est absent dans la liste.
    */
   public Intercommunalite getIntercommunalite(SIRENCommune siren) throws IntercommunaliteAbsenteDansCommuneSiegeException {
      return getIntercommunalite(siren.toString());
   }

   /**
    * Renvoyer l'intercommunalité à fiscalité propre dont la commune est membre ou siège.
    * @param commune Code de la commune dont on veut savoir à quelle intercommunalité à fiscalité propre elle appartient.
    * @return Intercommunalité à fiscalité propre.
    * @throws CommuneSansIntercommunaliteFiscalitePropreException si la commune n'est pas rattachée à une intercommunalité à fiscalité propre. 
    */
   public Intercommunalite getIntercommunaliteFiscalitePropre(Commune commune) throws CommuneSansIntercommunaliteFiscalitePropreException {
      Objects.requireNonNull(commune, "La commune dont on veut l'intercommunalité à fiscalité propre à laquelle elle appartient ne peut pas valoir null.");
      
      // Déterminer si c'est parmi les intercommunalités dont la commune est siège que se trouve celle-ci.
      for(String siren : commune.siegeGroupements()) {
         Intercommunalite intercommunalite = get(siren);
         
         if (intercommunalite.isFiscalitePropre()) {
            return intercommunalite;
         }
      }

      // Puis si c'est parmi les intercommunalités dont elle est membre.
      for(String siren : commune.membreGroupements()) {
         Intercommunalite intercommunalite = get(siren);
         
         if (intercommunalite.isFiscalitePropre()) {
            return intercommunalite;
         }
      }

      // Si nous sommes ici, c'est que n'avons pas trouvé d'intercommunalité à fiscalité propre pour cette commune.
      String message = MessageFormat.format("La commune {0} ({1}) n''est pas rattachée à une intercommunalité à fiscalité propre.",
         commune.getNomCommune(), commune.getCodeCommune());

      throw new CommuneSansIntercommunaliteFiscalitePropreException(message);
   }

   /**
    * Déterminer si une intercommunalité ou groupement de SIREN particulier dans cette liste.
    * @param siren SIREN recherché.
    * @return Intercommunalité.
    */
   public boolean hasIntercommunalite(String siren) {
      Objects.requireNonNull(siren, "Le SIREN pour la recherche de l'intercommunalité ne peut pas valoir null.");
      return this.containsKey(siren);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Iterator<Intercommunalite> iterator() {
      return values().iterator();
   }
}
