package fr.ecoemploi.domain.model.territoire.entreprise;

import java.io.*;
import java.text.*;
import java.util.*;

import fr.ecoemploi.domain.utils.objets.ObjetMetierIdentifiable;
import fr.ecoemploi.domain.model.territoire.CodeCommune;

/**
 * Activités communales par section NAF.
 * @author Marc LE BIHAN
 */
public class ActivitesCommunaleParNAF implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -2266648593508806118L;
   
   /** Code de la commune. */
   private String codeCommune;
   
   /** Nom de la commune */
   private String nom;
   
   /** Population de la commune, au dernier recensement. */
   private Integer population;
   
   /** Nombre d'entreprises présentes dans la commune. */
   private Integer nombreEntreprises;
   
   /** Nombre de salariés exerçant dans des entreprises de la commune. */
   private Integer nombreSalaries;
   
   /** Activités communales par code NAF. */
   private final Map<String, ActiviteCommunaleNAF> activitesCommunales =  new HashMap<>();
   
   /**
    * Compléter d'éléments vides les postes manquants NAF.
    * @param codesNAF Ensemble des codes NAF.
    */
   public void fill(List<String> codesNAF) {
      Objects.requireNonNull(codesNAF, "La liste des codes NAF à examiner pour compléter des activités communales avec des postes à zéro peut être vide mais ne peut pas valoir null.");
      
      for(String codeNAF : codesNAF) {
         if (this.activitesCommunales.containsKey(codeNAF) == false) {
            this.activitesCommunales.put(codeNAF, new ActiviteCommunaleNAF(codeNAF));
         }
      }
   }

   /**
    * Renvoyer les chiffres des activités NAF de la commune.
    * @return Activités NAF de la commune.
    */
   public Map<String, ActiviteCommunaleNAF> getActivitesNAF() {
      return this.activitesCommunales;
   }
   
   /**
    * Déclarer les chiffres d'une activité NAF dans la commune.
    * @param activiteCommunaleNAF Activité NAF.
    */
   public void putActiviteNAF(ActiviteCommunaleNAF activiteCommunaleNAF) {
      this.activitesCommunales.put(activiteCommunaleNAF.getCodeNAF(), activiteCommunaleNAF);
   }

   /**
    * Renvoyer le code de la commune.
    * @return Code de la commune.
    */
   public CodeCommune asCodeCommune() {
      return new CodeCommune(this.codeCommune);
   }

   /**
    * Renvoyer le code de la commune.
    * @return Code de la commune.
    */
   public String getCodeCommune() {
      return this.codeCommune;
   }

   /**
    * Fixer le code de la commune.
    * @param codeCommune Code de la commune. 
    */
   public void setCodeCommune(String codeCommune) {
      this.codeCommune = codeCommune;
   }

   /**
    * Renvoyer le nom de la commune.
    * @return Nom de la commune.
    */
   public String getNom() {
      return this.nom;
   }

   /**
    * Fixer le nom de la commune.
    * @param nom Nom de la commune. 
    */
   public void setNom(String nom) {
      this.nom = nom;
   }

   /**
    * Renvoyer la population de la commune. 
    * @return Population.
    */
   public Integer getPopulation() {
      return this.population;
   }

   /**
    * Fixer la population de la commune. 
    * @param population Population.
    */
   public void setPopulation(Integer population) {
      this.population = population;
   }

   /**
    * Renvoyer le nombre d'entreprises présentes dans la commune.
    * @return nbr d'entreprises.
    */
   public Integer getNombreEntreprises() {
      return this.nombreEntreprises;
   }

   /**
    * Renvoyer le nombre d'entreprises présentes dans la commune.
    * @param nombreEntreprises nbr d'entreprises.
    */
   public void setNombreEntreprises(Integer nombreEntreprises) {
      this.nombreEntreprises = nombreEntreprises;
   }

   /**
    * Renvoyer le nombre de salariés exerçant dans des entreprises de la commune.
    * @return nbr de salariés.
    */
   public Integer getNombreSalaries() {
      return this.nombreSalaries;
   }

   /**
    * Renvoyer le nombre de salariés exerçant dans des entreprises de la commune.
    * @param nombreSalaries nbr de salariés.
    */
   public void setNombreSalaries(Integer nombreSalaries) {
      this.nombreSalaries = nombreSalaries;
   }
   
   /**
    * @see ObjetMetierIdentifiable#toString()
    */
   @Override
   public String toString() {
      String format = "'{'"
            + "code commune : {0}, nom : {1}, population : {2}, nombre d''entreprises : {3}, nombre de salariés : {4}, activités par NAF : {5}"
            + "'}'";

      return MessageFormat.format(format, this.codeCommune, this.nom, this.population, this.nombreEntreprises, this.nombreSalaries, this.activitesCommunales);
   }
}
