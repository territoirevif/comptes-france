package fr.ecoemploi.domain.model.territoire.entreprise;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.MetierException;

/**
 * Exception levée lorsqu'une entreprise n'existe pas.
 * @author Marc LE BIHAN
 */
public class EntrepriseInexistanteException extends MetierException {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -7931375031387179169L;
   
   /** SIREN de l'entreprise absente. */
   private String siren;
   
   /** Année de recherche dans les bases SIRENE. */
   private int annee;
   
   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    */
   public EntrepriseInexistanteException(String message) {
      super(message);
   }
   
   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception.
    */
   public EntrepriseInexistanteException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception.
    * @param siren Code SIREN de l'entreprise manquante.
    * @param annee Année de recherche dans les bases SIRENE.
    */
   public EntrepriseInexistanteException(String siren, int annee, String message, Throwable cause) {
      super(message, cause);
      this.siren = siren;
      this.annee = annee;
   }
   
   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param siren Code SIREN de l'entreprise manquante.
    * @param annee Année de recherche dans les bases SIRENE.
    */
   public EntrepriseInexistanteException(String siren, int annee, String message) {
      this(siren, annee, message, null);
   }

   /**
    * Renvoyer l'année de recherche.
    * @return Année.
    */
   public int getAnnee() {
      return this.annee;
   }

   /**
    * Renvoyer le code SIREN de l'entreprise manquante.
    * @return SIREN.
    */
   public String getSiren() {
      return this.siren;
   }
}
