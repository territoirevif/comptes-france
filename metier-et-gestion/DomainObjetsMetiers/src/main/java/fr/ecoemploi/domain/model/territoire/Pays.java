package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;
import java.util.*;

import org.apache.commons.lang3.*;
import org.apache.commons.lang3.builder.*;

import fr.ecoemploi.domain.utils.objets.ObjetMetier;

/**
 * Un pays.
 * @author Marc LE BIHAN.
 */
public class Pays extends ObjetMetier {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 2074115458084932487L;

   /** Code du pays. */
   private String codePays;

   /**
    * Construire un pays.
    */
   public Pays() {
   }

   /**
    * Construire un pays avec un code pays particulier.
    * @param code Code du pays.
    */
   public Pays(String code) {
      this.codePays = code;
   }

   /**
    * Construire un pays par copie.
    * @param pays Pays.
    */
   public Pays(Pays pays) {
      super(pays);
      this.codePays = pays.codePays;
   }

   /**
    * Renvoyer une liste de pays, ordonnée par une locale spécifique.
    * @param locale Locale à utiliser pour le tri.
    * @return Liste des pays ordonnée par la locale désirée.
    */
   public static List<Pays> liste(Locale locale) {
      Validate.notNull(locale, "The locale cannot be null.");

      // Get the country code list.
      String[] countryCodes = Locale.getISOCountries();
      Vector<Pays> countries = new Vector<>();

      for(String code : countryCodes)
         countries.add(new Pays(code));

      // Order the countries by their names.
      TriParNomDePays sorter = new TriParNomDePays(locale);
      countries.sort(sorter);
      return(countries);
   }

   /**
    * Return the country code of this country.
    * @return Country code.
    */
   public String getCodePays() {
      return this.codePays;
   }

   /**
    * Renvoyer le nom d'un pays dans une locale spécifique.
    * @param locale Locale.
    * @return Nom du pays dans cette locale.
    */
   public String getNom(Locale locale) {
      Validate.notNull(locale, "La locale ne peut pas valoir null.");

      return new Locale("", getCodePays()).getDisplayCountry(locale);
   }

   /**
    * Déterminer si deux pays sont les mêmes : ils le sont s'ils ont le même code pays.
    * @param o Pays à comparer avec le notre.
    * @return true, s'ils ont le même code pays.
    */
   @Override
   public boolean equals(Object o) {
      if (o instanceof Pays == false)
         return(false);

      if (o == this)
         return(true);

      Pays p = (Pays)o;

      EqualsBuilder equals = new EqualsBuilder();
      equals.append(p.getCodePays(), getCodePays());

      return(equals.build());
   }

   /**
    * @see java.lang.Object#hashCode()
    */
   @Override
   public int hashCode() {
      return(getCodePays().toLowerCase().hashCode());
   }

   /**
    * Set the country code.
    * @param countryCode Country code.
    */
   public void setCodePays(String countryCode) {
      this.codePays = countryCode;
   }
}
