package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.MetierException;

/**
 * Exception levée lorsqu'une commune n'existe pas.
 * @author Marc LE BIHAN
 */
public class CommuneInexistanteException extends MetierException {
   /** Serial ID */
   @Serial
   private static final long serialVersionUID = 615905151744215371L;

   /** Code de la commune qui n'existe pas. */
   private CodeCommune codeCommune;    // NOSONAR
   
   /** SIREN de la commune qui n'existe pas. */
   private SIRENCommune sirenCommune;  // NOSONAR

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    */
   public CommuneInexistanteException(String message) {
      super(message);
   }

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception.
    */
   public CommuneInexistanteException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Construire une exception.
    * @param code Code de la commune qui n'existe pas.
    * @param message Message accompagnant l'exception.
    */
   public CommuneInexistanteException(CodeCommune code, String message) {
      super(message);
      this.codeCommune = code;
   }

   /**
    * Construire une exception.
    * @param code Code de la commune qui n'existe pas.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception.
    */
   public CommuneInexistanteException(CodeCommune code, String message, Throwable cause) {
      super(message, cause);
      this.codeCommune = code;
   }

   /**
    * Construire une exception.
    * @param siren SIREN de la commune qui n'existe pas.
    * @param message Message accompagnant l'exception.
    */
   public CommuneInexistanteException(SIRENCommune siren, String message) {
      super(message);
      this.sirenCommune = siren;
   }

   /**
    * Construire une exception.
    * @param siren SIREN de la commune qui n'existe pas.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception.
    */
   public CommuneInexistanteException(SIRENCommune siren, String message, Throwable cause) {
      super(message, cause);
      this.sirenCommune = siren;
   }

   /**
    * Construire une exception.
    * @param code Code de la commune qui n'existe pas.
    * @param siren SIREN de la commune qui n'existe pas.
    * @param message Message accompagnant l'exception.
    */
   public CommuneInexistanteException(CodeCommune code, SIRENCommune siren, String message) {
      super(message);
      this.codeCommune = code;
      this.sirenCommune = siren;
   }

   /**
    * Construire une exception.
    * @param code Code de la commune qui n'existe pas.
    * @param siren SIREN de la commune qui n'existe pas.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception.
    */
   public CommuneInexistanteException(CodeCommune code, SIRENCommune siren, String message, Throwable cause) {
      super(message, cause);
      this.codeCommune = code;
      this.sirenCommune = siren;
   }
   
   /**
    * Renvoyer le code de la commune qui n'a pas été trouvée.
    * @return Code de la commune.
    */
   public CodeCommune getCodeCommune() {
      return this.codeCommune;
   }
   
   /**
    * Renvoyer le SIREN de la commune qui n'a pas été trouvée.
    * @return SIREN de la commune.
    */
   public SIRENCommune getSIRENCommune() {
      return this.sirenCommune;
   }
}
