package fr.ecoemploi.domain.model.imposition;

import java.io.Serial;

import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.utils.autocontrole.ObjetMetierSpark;

/**
 * Revenu et imposition des habitants d'une commune.
 */
public class RevenuImpositionCommune extends ObjetMetierSpark {
   @Serial
   private static final long serialVersionUID = 4246740118769495209L;

   /** Code de la commune */
   private CodeCommune codeCommune;

   /** Nom de la Commune */
   private String nomCommune;

   /** Code du département */
   private CodeDepartement codeDepartement;

   /** Nom du département */
   private String nomDepartement;

   /** Code de la région */
   private CodeRegion codeRegion;

   /** Population municipale de la commune */
   private int populationMunicipale;

   /** Code de l'intercommunalité */
   private SIREN codeEPCI;

   /** Nom de l'intercommunalité */
   private String nomEPCI;

   /** Nombre de déclarations déposées par l’ensemble des foyers fiscaux imposés et non imposés. */
   private Integer nombreDeFoyersFiscaux;

   /**
    * Foyers fiscaux ayant un impôt sur le revenu à acquitter (foyers dont l'impôt final est strictement positif)
    * pour la partie correspondant à l’émission sur rôle
    */
   private Integer nombreDeFoyersFiscauxImposes;

   /** Fréquence (taux) d'imposition des ménages */
   private Double frequenceImpositionMenages;

   /** Montant fiscal par foyer dans la commune */
   private Integer montantFiscalParFoyer;

   /** Montant fiscal par foyer imposé dans la commune */
   private Integer montantFiscalParFoyerImpose;

   /**
    * Nombre de foyers fiscaux pour lesquels une des cases au moins des lignes « Revenus d'activité connus »
    * et « Autres revenus imposables connus » de la rubrique « Traitements, salaires »,
    * de la ligne « Gains de levée d'option sur titres en cas de cession ou de conversion au porteur dans le délai d'indisponibilité »,
    * de la ligne « Gains imposables sur option dans la catégorie des salaires » et de la rubrique « Revenus exceptionnels et différés » est servie.
    */
   private Integer traitementSalairesNombreFoyers;

   /**
    * Revenus d'activité connus et autres revenus imposables connus déclarés
    * par l’ensemble des foyers concernés avant application de la déduction forfaitaire de 10 %,
    * gains de levée d'option sur titres en cas de cession ou de conversion au porteur dans le délai d'indisponibilité,
    * gains imposables sur option dans la catégorie des salaires et revenus exceptionnels et différés imposés suivant le système du quotient.
    */
   private Integer montantSalaireParFoyer;

   /**
    * Nombre de foyers fiscaux pour lesquels une des cases au moins de la ligne « Total des pensions, retraites et rentes connues »
    * de la rubrique « Pensions, retraites, rentes » de la déclaration d’impôt sur le revenu est servie.
    */
   private Integer retraitesPensionsNombreFoyers;

   /**
    * Total des pensions, retraites et rentes connues déclarées par l’ensemble des foyers concernés
    * (hors pensions de retraite en capital taxable à 7,5 %, hors pensions alimentaires et hors rentes viagères à titre onéreux),
    * avant application de la déduction forfaitaire de 10 %.
    */
   private Integer montantRetraiteParFoyer;

   /**
    * Construire une information de revenus et d'imposition des habitants d'une commune
    */
   public RevenuImpositionCommune() {
   }

   /**
    * Construire une information de revenus et d'imposition des habitants d'une commune
    * @param codeCommune Code de la commune
    * @param nomCommune Nom de la commune
    * @param codeDepartement Code du département
    * @param nomDepartement Nom du département
    * @param codeRegion Code région
    * @param populationMunicipale Population municipale de la commune
    * @param codeEPCI Code de l'intercommunalité
    * @param nomEPCI Nom de l'intercommunalité
    * @param nombreDeFoyersFiscaux Nombre de foyers fiscaux
    * @param nombreDeFoyersFiscauxImposes Nombre de foyers fiscaux imposés
    * @param frequenceImpositionMenages Fréquence (taux) d'imposition des ménages
    * @param montantFiscalParFoyer Montant fiscal par foyer
    * @param montantFiscalParFoyerImpose  Montant fiscal par foyer imposé
    * @param traitementSalairesNombreFoyers Nombre de foyers percevant un traitement ou des salaires
    * @param montantSalaireParFoyer Montant des traitements ou salaire par foyer
    * @param retraitesPensionsNombreFoyers Nombre de foyers touchant retraite ou pension
    * @param montantRetraiteParFoyer Montant de la retraite ou pension
    */
   public RevenuImpositionCommune(String codeCommune, String nomCommune, String codeDepartement, String nomDepartement, String codeRegion,
      int populationMunicipale, String codeEPCI, String nomEPCI, Integer nombreDeFoyersFiscaux, Integer nombreDeFoyersFiscauxImposes,
      Double frequenceImpositionMenages, Integer montantFiscalParFoyer, Integer montantFiscalParFoyerImpose, Integer traitementSalairesNombreFoyers, Integer montantSalaireParFoyer,
      Integer retraitesPensionsNombreFoyers, Integer montantRetraiteParFoyer) {
      this.codeCommune = codeCommune != null ? new CodeCommune(codeCommune) : null;
      this.nomCommune = nomCommune;
      this.codeDepartement = codeDepartement != null ? new CodeDepartement(codeDepartement) : null;
      this.nomDepartement = nomDepartement;
      this.codeRegion = codeRegion != null ? new CodeRegion(codeRegion) : null;
      this.populationMunicipale = populationMunicipale;
      this.codeEPCI = codeEPCI != null ? new SIREN(codeEPCI) : null;
      this.nomEPCI = nomEPCI;
      this.nombreDeFoyersFiscaux = nombreDeFoyersFiscaux;
      this.nombreDeFoyersFiscauxImposes = nombreDeFoyersFiscauxImposes;
      this.frequenceImpositionMenages = frequenceImpositionMenages;
      this.montantFiscalParFoyer = montantFiscalParFoyer;
      this.montantFiscalParFoyerImpose = montantFiscalParFoyerImpose;
      this.traitementSalairesNombreFoyers = traitementSalairesNombreFoyers;
      this.montantSalaireParFoyer = montantSalaireParFoyer;
      this.retraitesPensionsNombreFoyers = retraitesPensionsNombreFoyers;
      this.montantRetraiteParFoyer = montantRetraiteParFoyer;
   }

   /**
    * Renvoyer le code de la commune
    * @return Code de la commune
    */
   public CodeCommune getCodeCommune() {
      return this.codeCommune;
   }

   /**
    * Fixer le code de la commune
    * @param codeCommune Code de la commune
    */
   public void setCodeCommune(CodeCommune codeCommune) {
      this.codeCommune = codeCommune;
   }

   /**
    * Renvoyer le nom de la commune
    * @return nom de la commune
    */
   public String getNomCommune() {
      return this.nomCommune;
   }

   /**
    * Fixer le nom de la commune
    * @param nomCommune Nom de la commune
    */
   public void setNomCommune(String nomCommune) {
      this.nomCommune = nomCommune;
   }

   /**
    * Renvoyer le code du département
    * @return Code du département
    */
   public CodeDepartement getCodeDepartement() {
      return this.codeDepartement;
   }

   /**
    * Fixer le code du département
    * @param codeDepartement Code du département
    */
   public void setCodeDepartement(CodeDepartement codeDepartement) {
      this.codeDepartement = codeDepartement;
   }

   /**
    * Renvoyer le nom du département
    * @return Nom du département
    */
   public String getNomDepartement() {
      return this.nomDepartement;
   }

   /**
    * Fixer le nom du département
    * @param nomDepartement Nom du département
    */
   public void setNomDepartement(String nomDepartement) {
      this.nomDepartement = nomDepartement;
   }

   /**
    * Renvoyer le code de la région
    * @return Code de la région
    */
   public CodeRegion getCodeRegion() {
      return this.codeRegion;
   }

   /**
    * Fixer le code de la région
    * @param codeRegion Code de la région
    */
   public void setCodeRegion(CodeRegion codeRegion) {
      this.codeRegion = codeRegion;
   }

   /**
    * Renvoyer la population municipale de la commune
    * @return Population municipale
    */
   public int getPopulationMunicipale() {
      return this.populationMunicipale;
   }

   /**
    * Fixer la population municipale de la commune
    * @param populationMunicipale Population municipale
    */
   public void setPopulationMunicipale(int populationMunicipale) {
      this.populationMunicipale = populationMunicipale;
   }

   /**
    * Renvoyer le code de l'intercommunalité
    * @return Code de l'intercommunalité
    */
   public SIREN getCodeEPCI() {
      return this.codeEPCI;
   }

   /**
    * Fixer le code de l'intercommunalité
    * @param codeEPCI Code de l'intercommunalité
    */
   public void setCodeEPCI(SIREN codeEPCI) {
      this.codeEPCI = codeEPCI;
   }

   /**
    * Renvoyer le nom de l'intercommunalité
    * @return Nom de l'intercommunalité
    */
   public String getNomEPCI() {
      return this.nomEPCI;
   }

   /**
    * Fixer le nom de l'intercommunalité
    * @param nomEPCI Nom de l'intercommunalité
    */
   public void setNomEPCI(String nomEPCI) {
      this.nomEPCI = nomEPCI;
   }

   /**
    * Renvoyer le nombre de foyers fiscaux.<br>
    * Nombre de déclarations déposées par l’ensemble des foyers fiscaux imposés et non imposés.
    * @return Nombre de foyers fiscaux.
    */
   public Integer getNombreDeFoyersFiscaux() {
      return this.nombreDeFoyersFiscaux;
   }

   /**
    * Renvoyer le nombre de foyers fiscaux.
    * @param nombreDeFoyersFiscaux Nombre de foyers fiscaux
    */
   public void setNombreDeFoyersFiscaux(int nombreDeFoyersFiscaux) {
      this.nombreDeFoyersFiscaux = nombreDeFoyersFiscaux;
   }

   /**
    * Renvoyer le nombre de foyers fiscaux imposés<br>
    * Foyers fiscaux ayant un impôt sur le revenu à acquitter (foyers dont l'impôt final est strictement positif)
    * pour la partie correspondant à l’émission sur rôle
    * @return Nombre de foyers fiscaux imposés
    */
   public Integer getNombreDeFoyersFiscauxImposes() {
      return this.nombreDeFoyersFiscauxImposes;
   }

   /**
    * Fixer le nombre de foyers fiscaux imposés
    * @param nombreDeFoyersFiscauxImposes Nombre de foyers fiscaux imposés
    */
   public void setNombreDeFoyersFiscauxImposes(Integer nombreDeFoyersFiscauxImposes) {
      this.nombreDeFoyersFiscauxImposes = nombreDeFoyersFiscauxImposes;
   }

   /**
    * Renvoyer la fréquence (taux) d'imposition des ménages
    * @return Fréquence d'imposition des ménages
    */
   public Double getFrequenceImpositionMenages() {
      return this.frequenceImpositionMenages;
   }

   /**
    * Fixer la fréquence (taux) d'imposition des ménages
    * @param frequenceImpositionMenages Fréquence d'imposition des ménages
    */
   public void setFrequenceImpositionMenages(Double frequenceImpositionMenages) {
      this.frequenceImpositionMenages = frequenceImpositionMenages;
   }

   /**
    * Renvoyer le montant fiscal, par foyer, dans la commune
    * @return Montant fiscal par foyer dans la commune
    */
   public Integer getMontantFiscalParFoyer() {
      return this.montantFiscalParFoyer;
   }

   /**
    * Fixer le montant fiscal, par foyer, dans la commune
    * @param montantFiscalParFoyer Montant fiscal par foyer dans la commune
    */
   public void setMontantFiscalParFoyer(Integer montantFiscalParFoyer) {
      this.montantFiscalParFoyer = montantFiscalParFoyer;
   }

   /**
    * Renvoyer le montant fiscal, par foyer imposé, dans la commune
    * @return Montant fiscal par foyer dans la commune
    */
   public Integer getMontantFiscalParFoyerImpose() {
      return this.montantFiscalParFoyerImpose;
   }

   /**
    * Fixer le montant fiscal, par foyer imposé, dans la commune
    * @param montantFiscalParFoyerImpose Montant fiscal par foyer dans la commune
    */
   public void setMontantFiscalParFoyerImpose(Integer montantFiscalParFoyerImpose) {
      this.montantFiscalParFoyerImpose = montantFiscalParFoyerImpose;
   }

   /**
    * Renvoyer le nombre de foyers fiscaux pour lesquels une des cases au moins des lignes « Revenus d'activité connus »
    * et « Autres revenus imposables connus » de la rubrique « Traitements, salaires »,
    * de la ligne « Gains de levée d'option sur titres en cas de cession ou de conversion au porteur dans le délai d'indisponibilité »,
    * de la ligne « Gains imposables sur option dans la catégorie des salaires » et de la rubrique « Revenus exceptionnels et différés » est servie.
    * @return Nombre de foyers
    */
   public Integer getTraitementSalairesNombreFoyers() {
      return this.traitementSalairesNombreFoyers;
   }

   /**
    * Fixer le nombre de foyers dans la commune touchant un salaire ou traitement
    * @param traitementSalairesNombreFoyers Nombre de foyers
    */
   public void setTraitementSalairesNombreFoyers(Integer traitementSalairesNombreFoyers) {
      this.traitementSalairesNombreFoyers = traitementSalairesNombreFoyers;
   }

   /**
    * Renvoyer les revenus d'activité connus et autres revenus imposables connus déclarés
    * par l’ensemble des foyers concernés avant application de la déduction forfaitaire de 10 %,
    * gains de levée d'option sur titres en cas de cession ou de conversion au porteur dans le délai d'indisponibilité,
    * gains imposables sur option dans la catégorie des salaires et revenus exceptionnels et différés imposés suivant le système du quotient.
    * @return Montants des salaires, par foyer, dans la commune.
    */
   public Integer getMontantSalaireParFoyer() {
      return this.montantSalaireParFoyer;
   }

   /**
    * Fixer les montants et salaires, par foyer, dans la commune
    * @param montantSalaireParFoyer Montants des salaires, par foyer, dans la commune.
    */
   public void setMontantSalaireParFoyer(Integer montantSalaireParFoyer) {
      this.montantSalaireParFoyer = montantSalaireParFoyer;
   }

   /**
    * Renvoyer le nombre de foyers fiscaux pour lesquels une des cases au moins de la ligne « Total des pensions, retraites et rentes connues »
    * de la rubrique « Pensions, retraites, rentes » de la déclaration d’impôt sur le revenu est servie.
    * @return Nombre de foyers
    */
   public Integer getRetraitesPensionsNombreFoyers() {
      return this.retraitesPensionsNombreFoyers;
   }

   /**
    * Fixer le nombre de foyers avec pensions ou retraites dans la commune
    * @param retraitesPensionsNombreFoyers Nombre de foyers
    */
   public void setRetraitesPensionsNombreFoyers(Integer retraitesPensionsNombreFoyers) {
      this.retraitesPensionsNombreFoyers = retraitesPensionsNombreFoyers;
   }

   /**
    * Renvoyer le total des pensions, retraites et rentes connues déclarées par l’ensemble des foyers concernés
    * (hors pensions de retraite en capital taxable à 7,5 %, hors pensions alimentaires et hors rentes viagères à titre onéreux),
    * avant application de la déduction forfaitaire de 10 %.
    * @return Montant des retraites, par foyer, dans la commune
    */
   public Integer getMontantRetraiteParFoyer() {
      return this.montantRetraiteParFoyer;
   }

   /**
    * Fixer le montant des retraites, par foyer, dans la commune
    * @param montantRetraiteParFoyer Montant, par foyer, dans la commune
    */
   public void setMontantRetraiteParFoyer(Integer montantRetraiteParFoyer) {
      this.montantRetraiteParFoyer = montantRetraiteParFoyer;
   }
}
