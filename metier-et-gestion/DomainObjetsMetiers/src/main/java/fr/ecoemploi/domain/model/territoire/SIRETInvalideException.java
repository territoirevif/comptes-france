package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.MetierException;

/**
 * Exception levée si un SIRET est invalide.
 * @author Marc LE BIHAN
 */
public class SIRETInvalideException extends MetierException {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -6780345682677699283L;
   
   /** Siret. */
   private final String siret;
   
   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param siret SIRET qui est invalide.
    */
   public SIRETInvalideException(String siret, String message) {
      super(message);
      this.siret = siret;
   }
   
   /**
    * Renvoyer le SIRET invalide.
    * @return SIRET.
    */
   public String getSIRET() {
      return this.siret;
   }
}
