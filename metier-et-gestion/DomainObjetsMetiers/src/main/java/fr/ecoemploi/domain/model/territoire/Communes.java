package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;
import java.text.*;
import java.util.*;
import java.util.stream.Stream;

import fr.ecoemploi.domain.utils.objets.AbstractComparatorLocale;

/**
 * Une liste de communes.
 * @author Marc LE BIHAN
 */
public class Communes extends LinkedHashMap<String, Commune> implements Iterable<Commune> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -3259244837403430541L;

   /** Collator utilisé pour comparer les noms de communes, sans accents. */
   private static final Collator collator = Collator.getInstance(Locale.FRENCH);
   
   /** Codes communes par SIREN. */
   private Map<String, String> codesCommunesParSiren = new HashMap<>();
   
   static {
      collator.setStrength(Collator.PRIMARY);
   }

   /**
    * Renvoyer la commune correspondant à un code, en recherchant éventuellement parmi ses arrondissements.
    * @param code Code de la commune recherchée.
    * @return Commune ou null si elle n'a pas été trouvée.
    */
   public Commune get(CodeCommune code) {
      return get(code.toString());
   }

   /**
    * Renvoyer la commune correspondant à un code, en recherchant éventuellement parmi ses arrondissements.
    * @param code Code de la commune recherchée.
    * @return Commune ou null si elle n'a pas été trouvée.
    */
   public Commune get(String code) {
      Objects.requireNonNull(code, "Le code de la commune ne peut pas valoir null.");
      
      Commune commune = super.get(code);
      
      // Si la commune n'a pas été trouvée, la rechercher dans quelques arrondissements.
      if (commune == null) {
         // TODO Rechercher la commune dans des arrondissements, si elle manque.
      }
      
      return commune;
   }
   
   /**
    * Recherche d'une commune par numéro de SIREN.
    * @param siren Numéro de SIREN.
    * @return Commune.
    */
   public Commune getParSiren(String siren) {
      Objects.requireNonNull(siren, "Le numéro SIREN de recherche d'une commune ne peut pas valoir null.");
      
      String codeCommune = this.codesCommunesParSiren.get(siren);
      return codeCommune != null ? this.get(codeCommune) : null; 
   }
   
   /**
    * Ajouter une comune à la liste des communes.
    * @param codeCommune Code de la commune.
    * @param commune Commune.
    */
   @Override
   public Commune put(String codeCommune, Commune commune) {
      super.put(codeCommune, commune);
      this.codesCommunesParSiren.put(commune.getSirenCommune(), commune.getCodeCommune());
      return commune;
   }
   
   /**
    * Effacer le contenu de la liste des communes.
    */
   @Override
   public void clear() {
      super.clear();
      this.codesCommunesParSiren = new HashMap<>();
   }

   /**
    * Renvoyer la liste des codes communes des communes d'une liste.
    * @return Codes communes.
    */
   public Set<CodeCommune> getCodesCommunes() {
      Set<CodeCommune> codesCommunes = new HashSet<>();
      this.values().forEach(commune -> codesCommunes.add(new CodeCommune(commune.getCodeCommune())));
      return codesCommunes;
   }
   
   /**
    * Renvoyer la liste des codes communes des communes d'une liste sous forme de chaînes de caractères.
    * @return Codes communes.
    */
   public Set<String> getCodesCommunesAsString() {
      Set<String> codesCommunes = new HashSet<>();
      this.values().forEach(commune -> codesCommunes.add(new CodeCommune(commune.getCodeCommune()).departementEtCommune()));
      return codesCommunes;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Iterator<Commune> iterator() {
      return values().iterator();
   }

   /**
    * Retourner un flux de communes.
    * @return Flux de communes.
    */
   public Stream<Commune> stream() {
      return this.values().stream();
   }

   /**
    * Comparateur avec Locale.
    */
   static class ComparateurLocale extends AbstractComparatorLocale implements Comparator<Commune> {
      /**
       * Construire un comparateur.
       * @param locale Locale.
       */
      public ComparateurLocale(Locale locale) {
         super(locale);
      }

      /**
       * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
       */
      @Override
      public int compare(Commune a, Commune b) {
         return this.collator.compare(a.getNomCommune(), b.getNomCommune());
      }
   }

   /**
    * Trier les communes par nom.
    * @param locale Locale.
    * @return Communes triées par nom.
    */
   public Communes triParNomDeCommune(Locale locale) {
      Objects.requireNonNull(locale, "La locale pour le tri ne peut pas valoir null.");
      
      Communes triee = new Communes();
      
      this.entrySet().stream()
         .sorted((a,b) -> Communes.collator.compare(a.getValue().getNomCommune(), b.getValue().getNomCommune()))
         .forEach(e -> triee.put(e.getKey(), e.getValue()));

      return triee;
   }
}
