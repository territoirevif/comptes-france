package fr.ecoemploi.domain.model.catalogue.metadata;

import java.io.Serial;
import java.text.MessageFormat;
import java.time.*;
import java.util.*;

import org.apache.commons.lang3.builder.*;
import fr.ecoemploi.domain.utils.objets.ObjetMetier;

/**
 * Une période.
 * @author Marc LE BIHAN
 */
public class Periode extends ObjetMetier {
   /** Serial ID.  */
   @Serial
   private static final long serialVersionUID = 939106276443378921L;

   /** Début de la période. */
   private LocalDateTime debut = LocalDateTime.MIN;

   /** Fin de la période. */
   private LocalDateTime fin = LocalDateTime.MAX;

   /**
    * Construire une période infinie.
    */
   public Periode() {
   }

   /**
    * Constructeur par copie.
    * @param periode Période à copier.
    */
   public Periode(Periode periode) {
      super(periode);
      this.setDebut(periode.debut != null ? LocalDateTime.parse(periode.debut.toString()) : null);
      this.setFin(periode.fin != null ? LocalDateTime.parse(periode.fin.toString()) : null);
   }

   /**
    * Construire une période.
    * @param debutPeriode Date de début.
    * @param finPeriode Date de fin.
    */
   public Periode(Date debutPeriode, Date finPeriode) {
      Objects.requireNonNull(debutPeriode, "La date de début de période ne peut pas valoir null.");
      Objects.requireNonNull(finPeriode, "La date de fin de période ne peut pas valoir null.");

      this.debut = toDate(debutPeriode);
      this.fin = toDate(finPeriode);
   }

   /**
    * Construire une période.
    * @param debutPeriode Date de début.
    * @param finPeriode Date de fin.
    */
   public Periode(LocalDateTime debutPeriode, LocalDateTime finPeriode) {
      Objects.requireNonNull(debutPeriode, "La date de début de période ne peut pas valoir null.");
      Objects.requireNonNull(finPeriode, "La date de fin de période ne peut pas valoir null.");

      this.debut = debutPeriode;
      this.fin = finPeriode;
   }

   /**
    * Renvoyer le début de la période.
    * @return Début de la période.
    */
   public LocalDateTime getDebut() {
      return this.debut;
   }

   /**
    * Renvoyer la fin de la période.
    * @return Fin de la période.
    */
   public LocalDateTime getFin() {
      return this.fin;
   }

   /**
    * Renvoyer le début de la période.
    * @return Début de la période.
    */
   public Date getDebutAsDate() {
      return toDate(this.debut);
   }

   /**
    * Renvoyer la fin de la période.
    * @return Fin de la période.
    */
   public Date getFinAsDate() {
      return toDate(this.fin);
   }

   /**
    * Déterminer si la période est infinie.
    * @return true si la période a pour bornes MIN et MAX.
    */
   public boolean isInfini() {
      return LocalDateTime.MIN.equals(this.debut) && LocalDateTime.MAX.equals(this.fin);
   }

   /**
    * Déterminer si la période a un horaire (une heure/minute/seconde différent de zéro et n'est ni MIN ni MAX) dans l'une de ses bornes ou non. 
    * @return true s'il a un horaire.
    */
   public boolean hasHoraire() {
      // Si la période est infinie, il n'y a pas d'horaire.
      if (isInfini())
         return false;

      // Si ni la borne de début ni celle de fin n'ont une heure, minute, seconde ou nano alimentées, il n'y a pas d'horaire.
      return hasHoraire(this.debut) != false || hasHoraire(this.fin) != false;
   }

   /**
    * Déterminer si le local time en question a un horaire (une heure/minute/seconde différent de zéro et n'est ni MIN ni MAX) ou non.
    * @param instant Horaire à observer. 
    * @return true s'il a un horaire.
    */
   public static boolean hasHoraire(LocalDateTime instant) {
      Objects.requireNonNull(instant, "Le local time dont on veut déterminer s'il a un horaire ou pas ne peut pas valoir null.");

      return instant.getHour() != 0 || instant.getMinute() != 0 || instant.getSecond() != 0 || instant.getNano() != 0;
   }

   /**
    * Fixer le début de la période.
    * @param debutPeriode Début de la période.
    */
   public void setDebut(LocalDateTime debutPeriode) {
      this.debut = debutPeriode;
   }

   /**
    * Fixer la fin de la période.
    * @param finPeriode Fin de la période.
    */
   public void setFin(LocalDateTime finPeriode) {
      this.fin = finPeriode;
   }

   /**
    * Fixer le début de la période.
    * @param debutPeriode Début de la période.
    */
   public void setDebutAsDate(Date debutPeriode) {
      this.debut = toDate(debutPeriode);
   }

   /**
    * Fixer la fin de la période.
    * @param finPeriode Fin de la période.
    */
   public void setFinAsDate(Date finPeriode) {
      this.fin = toDate(finPeriode);
   }

   /**
    * Convertit une LocalDateTime en Date. 
    * @param localDateTime LocalDateTime Date.
    * @return Date.
    */
   public static Date toDate(LocalDateTime localDateTime) {
      if (localDateTime == null)
         return null;

      if (localDateTime == LocalDateTime.MAX)
         return new Date(Long.MAX_VALUE);

      if (localDateTime == LocalDateTime.MIN)
         return new Date(0);

      return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
   }

   /**
    * Convertit une Date en LocalDateTime. 
    * @param date Date Date.
    * @return Date.
    */
   public static LocalDateTime toDate(Date date) {
      if (date == null)
         return null;

      if (date.getTime() == Long.MAX_VALUE)
         return LocalDateTime.MAX;

      if (date.getTime() == 0)
         return LocalDateTime.MIN;

      return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
   }

   /**
    * @see java.lang.Object#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object o) {
      if (o instanceof Periode == false)
         return false;

      Periode candidat = (Periode)o;

      EqualsBuilder equals = new EqualsBuilder();
      equals.append(this.debut, candidat.debut);
      equals.append(this.fin, candidat.fin);
      return equals.build();
   }

   /**
    * @see java.lang.Object#hashCode()
    */
   @Override
   public int hashCode() {
      HashCodeBuilder hash = new HashCodeBuilder();
      hash.append(this.debut);
      hash.append(this.fin);
      return hash.build();
   }

   /**
    * @see java.lang.Object#toString()
    */
   @Override
   public String toString() {
      String format = getFormatPeriode();

      Instant i1 = this.debut.atZone(ZoneId.systemDefault()).toInstant();
      Instant i2 = this.fin.atZone(ZoneId.systemDefault()).toInstant();

      return MessageFormat.format(format, LocalDateTime.MIN.equals(this.debut) ? null : Date.from(i1),
            LocalDateTime.MIN.equals(this.debut) ? null : Date.from(i1), LocalDateTime.MAX.equals(this.fin) ? null : Date.from(i2),
            LocalDateTime.MAX.equals(this.fin) ? null : Date.from(i2));
   }

   /**
    * Renvoyer le format d'affichage de la période.
    * @return Format d'affichage.
    */
   private String getFormatPeriode() {
      // #0 : Date de début de la période.
      // #1 : Heure de début de la période.
      // #2 : Date de fin de la période.
      // #3 : Heure de fin de la période.
      if (isInfini())
         return "(min, max)";

      // Période avec minimum.
      if (LocalDateTime.MIN.equals(this.debut))
         return hasHoraire(this.fin) ? "(min, {2,date,short} - {3,time,short})" : "(min, {2,date,short})";

      // Période avec maximum.
      if (LocalDateTime.MAX.equals(this.fin))
         return hasHoraire(this.debut) ? "({0,date,short} - {1,time,short}, max)" : "({0,date,short}, max)";

      // Période avec deux dates distinctes de l'infini.
      return hasHoraire() ? "{0,date,short} - {1,time,short}, {2,date,short} - {3,time,short})" : "({0,date,short}, {2,date,short})";
   }
}
