package fr.ecoemploi.domain.model.territoire.entreprise.ape;

import java.io.Serial;
import java.text.MessageFormat;

import fr.ecoemploi.domain.utils.autocontrole.ObjetMetierSpark;

/**
 * Une entreprise par APE.
 * @author Marc Le Bihan
 */
public class StatistiqueEntrepriseParAPE extends ObjetMetierSpark {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 2091319195493284441L;

   /** Code APE. */
   private String codeAPE;
   
   /** Nombre d'entreprises par code APE. */
   private int nombreEntreprises;
   
   /** Nombre de salariés par code APE. */
   private double nombreSalaries;
   
   /** Nombre de salariés moyen par entreprise et code APE. */
   private double moyenneSalaries;

   /**
    * Renvoyer le code APE.
    * @return Code APE.
    */
   public String getCodeAPE() {
      return this.codeAPE;
   }

   /**
    * Fixer le code APE.
    * @param codeAPE Code APE.
    */
   public void setCodeAPE(String codeAPE) {
      this.codeAPE = codeAPE;
   }

   /**
    * Renvoyer le nombre d'entreprises ayant ce code APE.
    * @return Nombre d'entreprises.
    */
   public int getNombreEntreprises() {
      return this.nombreEntreprises;
   }

   /**
    * Fixer le nombre d'entreprises ayant ce code APE.
    * @param nombreEntreprises Nombre d'entreprises.
    */
   public void setNombreEntreprises(int nombreEntreprises) {
      this.nombreEntreprises = nombreEntreprises;
   }

   /**
    * Renvoyer le nombre de salariés travaillant dans un établissement ayant ce code APE.
    * @return le nombre de salariés travaillant dans un établissement ayant ce code APE.
    */
   public double getNombreSalaries() {
      return this.nombreSalaries;
   }

   /**
    * Renvoyer le nombre de salariés travaillant dans un établissement ayant ce code APE.
    * @param nombreSalaries Nombre de salariés.
    */
   public void setNombreSalaries(double nombreSalaries) {
      this.nombreSalaries = nombreSalaries;
   }

   /**
    * Renvoyer le nombre de salariés moyen travaillant chaque établissement ayant ce code APE.
    * @return Nombre de salariés moyen travaillant chaque établissement ayant ce code APE.
    */
   public double getMoyenneSalaries() {
      return this.moyenneSalaries;
   }

   /**
    * Renvoyer le nombre de salariés moyen travaillant chaque établissement ayant ce code APE.
    * @param moyenneSalaries Nombre de salariés moyen travaillant chaque établissement ayant ce code APE.
    */
   public void setMoyenneSalaries(double moyenneSalaries) {
      this.moyenneSalaries = moyenneSalaries;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      String format = "'{'Code APE : {0},  moyenne salariés / entreprise : {1,number,#0.00}, nombre total d''entreprises : {2}, nombre total de salariés : {3}'}'";
      return MessageFormat.format(format, this.moyenneSalaries, this.nombreEntreprises, this.nombreSalaries);
   }
}
