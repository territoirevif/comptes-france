package fr.ecoemploi.domain.model.territoire.entreprise;

import java.io.Serial;
import java.text.MessageFormat;
import java.time.*;
import java.util.*;

import fr.ecoemploi.domain.utils.objets.ObjetMetierIdentifiable;
import fr.ecoemploi.domain.model.territoire.*;

/**
 * Entreprise
 * @author Marc LE BIHAN
 */
public class Entreprise extends AbstractSirene<SIREN> implements Comparable<Entreprise> {
   /**
    * Construre une entreprise.
    */
   public Entreprise() {
   }
   
   /**
    * Construre une entreprise.
    * @param etablissements Liste des établissements de l'entreprise.
    * @param sigle Sigle de l'entreprise.
    * @param nomNaissance Nom de naissance.
    * @param nomUsage Nom d'usage.
    * @param prenomUsuel Prénom usuel.
    * @param prenom1 Autre prénom 1.
    * @param prenom2 Autre prénom 2.
    * @param prenom3 Autre prénom 3.
    * @param prenom4 Autre prénom 4.
    * @param pseudonyme Pseudonyme de la personne physique.
    * @param sexe Sexe de la personne physique.
    * @param rna Numéro d’identification au répertoire national des associations.
    * @param denominationEntreprise Denomination de l'entreprise.
    * @param denominationUsuelle1 Dénomination usuelle de l'entreprise 1.
    * @param denominationUsuelle2 Dénomination usuelle de l'entreprise 2.
    * @param denominationUsuelle3 Dénomination usuelle de l'entreprise 3.
    * @param economieSocialeEtSolidaire Appartenance au champ de l’économie sociale et solidaire.
    * @param categorieEntreprise Catégorie d'entreprise.
    * @param dateCreationEntreprise Année et mois de création de l'entreprise.
    * @param anneeCategorie Année de mise à jour de la catégorie.
    * @param nicsiege Numéro interne de classement de l'établissement siège.
    * @param categorieJuridique Catégorie juridique de l'entreprise.
    * @param purgee Indique si l'entreprise a été purgée du fichier.
    * @param diffusable true si l'entreprise a des informations diffusables.
    * @param siren Code SIREN.
    * @param codeAPE Code APE.
    * @param nomenclatureActivitePrincipale Nomclature de l'activité principale.
    * @param trancheEffectifSalarie Tranche d'effectif salarié.
    * @param anneeValiditeEffectifSalarie Année de validité de l'effectif salarié.
    * @param caractereEmployeur Caractère employeur de l'entreprise ou de l'établissement.
    * @param active Indique si l'entreprise est en activitée ou cessée.
    * @param dateDebutHistorisation Date de début depuis laquelle les données d'historisation restent inchangées.
    * @param dateDernierTraitement Date de dernier traitement.
    * @param nombrePeriodes Indique le nombre de périodes depuis lequel l'historisation de ces données n'a pas bougé.
    */
   public Entreprise(Map<String, Etablissement> etablissements, String sigle, String nomNaissance, String nomUsage, String prenomUsuel, 
         String prenom1, String prenom2, String prenom3, String prenom4, String pseudonyme,
         String sexe, String rna, String denominationEntreprise, String denominationUsuelle1, String denominationUsuelle2, 
         String denominationUsuelle3, Boolean economieSocialeEtSolidaire, String categorieEntreprise, String dateCreationEntreprise, Integer anneeCategorie, 
         String nicsiege, String categorieJuridique, Boolean purgee, Boolean diffusable,
         SIREN siren, String codeAPE, String nomenclatureActivitePrincipale, String trancheEffectifSalarie, Integer anneeValiditeEffectifSalarie, 
         Boolean caractereEmployeur, Boolean active, String dateDebutHistorisation, String dateDernierTraitement, Integer nombrePeriodes) {
      super(siren, codeAPE, nomenclatureActivitePrincipale, trancheEffectifSalarie, anneeValiditeEffectifSalarie, 
          caractereEmployeur, active, dateDebutHistorisation, dateDernierTraitement, nombrePeriodes);
      this.etablissements = etablissements;
      this.sigle = sigle;
      this.nomNaissance = nomNaissance;
      this.nomUsage = nomUsage;
      this.prenomUsuel = prenomUsuel;
      this.prenom1 = prenom1;
      this.prenom2 = prenom2;
      this.prenom3 = prenom3;
      this.prenom4 = prenom4;
      this.pseudonyme = pseudonyme;
      this.sexe = sexe;
      this.rna = rna;
      this.denominationEntreprise = denominationEntreprise;
      this.denominationUsuelle1 = denominationUsuelle1;
      this.denominationUsuelle2 = denominationUsuelle2;
      this.denominationUsuelle3 = denominationUsuelle3;
      this.economieSocialeEtSolidaire = economieSocialeEtSolidaire;
      this.categorieEntreprise = categorieEntreprise;
      this.dateCreationEntreprise = dateCreationEntreprise;
      this.anneeCategorie = anneeCategorie;
      this.nicsiege = nicsiege;
      this.categorieJuridique = categorieJuridique;
      this.purgee = purgee;
      this.diffusable = diffusable;
   }

   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 2451240618966775942L;
   
   /** Liste des établissements de l'entreprise. */
   private Map<String, Etablissement> etablissements = new HashMap<>();
   
   /** Sigle de l'entreprise */
   private String sigle;
   
   /** Nom de naissance */
   private String nomNaissance;
   
   /** Nom d'usage */
   private String nomUsage;
   
   /** Prénom usuel */
   private String prenomUsuel;
   
   /** Autre prénom 1. */
   private String prenom1;

   /** Autre prénom 2. */
   private String prenom2;

   /** Autre prénom 3. */
   private String prenom3;

   /** Autre prénom 4. */
   private String prenom4;
   
   /** Pseudonyme de la personne physique. */
   private String pseudonyme;
   
   /** Sexe de la personne physique. */
   private String sexe;

   /** Numéro d’identification au répertoire national des associations */
   private String rna;
   
   /** Denomination de l'entreprise. */
   private String denominationEntreprise;

   /** Dénomination usuelle de l'entreprise 1. */
   private String denominationUsuelle1;

   /** Dénomination usuelle de l'entreprise 2. */
   private String denominationUsuelle2;

   /** Dénomination usuelle de l'entreprise 3. */
   private String denominationUsuelle3;
   
   /** Appartenance au champ de l’économie sociale et solidaire */
   private Boolean economieSocialeEtSolidaire;
   
   /** Catégorie d'entreprise */
   private String categorieEntreprise;
   
   /** Année et mois de création de l'entreprise */
   private String dateCreationEntreprise;

   /** Année de mise à jour de la catégorie. */
   private Integer anneeCategorie;
   
   /** Numéro interne de classement de l'établissement siège */
   private String nicsiege;
   
   /** Catégorie juridique de l'entreprise. */
   private String categorieJuridique;

   /** Indique si l'entreprise est une société à mission (Loi Pacte) : elle lui permet de faire publiquement état de cette qualité et de ses objectifs sociaux et environnementaux. */
   private Boolean societeMission;
   
   /** Indique si l'entreprise a été purgée du fichier. */
   private Boolean purgee;
   
   /** Déterminer si l'entreprise a des informations diffusables. */
   private Boolean diffusable;

   /**
    * Renvoyer le SIREN de l'entreprise.
    * @return SIREN.
    */
   public SIREN asSiren() {
      return sirenOuSiret();
   }
   
   /**
    * Renvoyer le SIREN de l'entreprise.
    * @return SIREN.
    */
   public String getSiren() {
      return sirenOuSiret() != null ? sirenOuSiret().toString() : null;
   }
   
   /**
    * Fixer le SIREN de l'entreprise.
    * @param siren SIREN.
    */
   public void setSiren(String siren) {
      sirenOuSiret(new SIREN(siren));
   }

   /**
    * Renvoyer la liste des établissements de l'entreprise.
    * @return Liste des établissements.
    */
   public Map<String, Etablissement> getEtablissements() {
      if (this.etablissements == null) {
         this.etablissements = new HashMap<>();
      }

      return this.etablissements;
   }

   /**
    * Fixer la liste des établissements de l'entreprise.
    * @param etablissementsEntreprise Liste des établissements.
    */
   public void setEtablissements(Map<String, Etablissement> etablissementsEntreprise) {
      this.etablissements = etablissementsEntreprise;
   }

   /**
    * Renvoyer le sigle (forme réduite de la raison sociale ou de la dénomination d'une personne morale ou d'un organisme public) (SIGLE).
    * @return Sigle. 
    */
   public String getSigle() {
      return this.sigle;
   }

   /**
    * Fixer le sigle (forme réduite de la raison sociale ou de la dénomination d'une personne morale ou d'un organisme public) (SIGLE).
    * @param sigle Sigle. 
    */
   public void setSigle(String sigle) {
      this.sigle = sigle;
   }

   /**
    * Renvoyer le nom de naissance pour une personne physique (NOM).
    * @return Nom de naissance pour une personne physique.
    */
   public String getNomNaissance() {
      return this.nomNaissance;
   }

   /**
    * Fixer le nom de naissance pour une personne physique (NOM).
    * @param nom Nom de naissance pour une personne physique.
    */
   public void setNomNaissance(String nom) {
      this.nomNaissance = nom;
   }

   /**
    * Renvoyer le prénom usuel pour une personne physique (PRENOM).
    * @return Prénom usuel pour une personne physique.
    */
   public String getPrenomUsuel() {
      return this.prenomUsuel;
   }

   /**
    * Fixer le prénom usuel pour une personne physique (PRENOM).
    * @param prenom Prénom usuel pour une personne physique. 
    */
   public void setPrenomUsuel(String prenom) {
      this.prenomUsuel = prenom;
   }

   /**
    * Renvoyer le premier autre prénom de la personne physique.
    * @return Prénoms.
    */
   public String getPrenom1() {
      return this.prenom1;
   }

   /**
    * Fixer le premier autre prénom de la personne physique.
    * @param prenom Prénoms.
    */
   public void setPrenom1(String prenom) {
      this.prenom1 = prenom;
   }

   /**
    * Renvoyer le deuxième autre prénom de la personne physique.
    * @return Prénoms.
    */
   public String getPrenom2() {
      return this.prenom2;
   }

   /**
    * Fixer le deuxième autre prénom de la personne physique.
    * @param prenom Prénoms.
    */
   public void setPrenom2(String prenom) {
      this.prenom2 = prenom;
   }

   /**
    * Renvoyer le troisième autre prénom de la personne physique.
    * @return Prénoms.
    */
   public String getPrenom3() {
      return this.prenom3;
   }

   /**
    * Fixer le troisième autre prénom de la personne physique.
    * @param prenom Prénoms.
    */
   public void setPrenom3(String prenom) {
      this.prenom3 = prenom;
   }

   /**
    * Renvoyer le quatrième autre prénom de la personne physique.
    * @return Prénoms.
    */
   public String getPrenom4() {
      return this.prenom4;
   }

   /**
    * Fixer le quatrième autre prénom de la personne physique.
    * @param prenom Prénoms.
    */
   public void setPrenom4(String prenom) {
      this.prenom4 = prenom;
   }

   /**
    * Renvoyer le pseudonyme de la personne physique.
    * @return Pseudonyme.
    */
   public String getPseudonyme() {
      return this.pseudonyme;
   }

   /**
    * Fixer le pseudonyme de la personne physique.
    * @param pseudonyme Pseudonyme.
    */
   public void setPseudonyme(String pseudonyme) {
      this.pseudonyme = pseudonyme;
   }
   
   /**
    * Renvoyer le sexe de la personne physique.
    * @return sexe de la personne physique.
    */
   public Sexe asSexe() {
      return this.sexe != null ? Sexe.valueOf(this.sexe) : null;
   }
   
   /**
    * Renvoyer le sexe de la personne physique.
    * @return sexe de la personne physique.
    */
   public String getSexe() {
      return this.sexe;
   }

   /**
    * Fixer le sexe de la personne physique.
    * @param sexe sexe de la personne physique.
    */
   public void setSexe(String sexe) {
      this.sexe = sexe;
   }

   /**
    * Renvoyer le numéro d’identification au répertoire national des associations (RNA).
    * @return W suivi de neuf chiffres.
    */
   public String getRna() {
      return this.rna;
   }

   /**
    * Fixer le numéro d’identification au répertoire national des associations (RNA).
    * @param rna W suivi de neuf chiffres.
    */
   public void setRna(String rna) {
      this.rna = rna;
   }

   /**
    * Déterminer si l'entreprise appartient au champ de l’économie sociale et solidaire (ESS).
    * @return Appartenance ou non à l'ESS.
    */
   public Boolean getEconomieSocialeSolidaire() {
      return this.economieSocialeEtSolidaire;
   }

   /**
    * Fixer si l'entreprise appartient au champ de l’économie sociale et solidaire (ESS).
    * @param ess Appartenance ou non à l'ESS.
    */
   public void setEconomieSocialeSolidaire(Boolean ess) {
      this.economieSocialeEtSolidaire = ess;
   }

   /**
    * Renvoyer la catégorie de l'entreprise (CATEGORIE).
    * @return Catégorie de l'entreprise :<br>
    * PME ou Petite ou Moyenne Entreprise : La catégorie des petites et moyennes entreprises (PME) est constituée des entreprises qui occupent moins de 250 personnes, et qui ont un chiffre d'affaires annuel inférieur à 50 millions d'euros ou un total de bilan n'excédant pas 43 millions d'euros. Cette catégorie inclut les microentreprises.<br>
    * ETI ou Entreprise de Taille Intermédiaire : Une entreprise de taille intermédiaire est une entreprise qui a entre 250 et 4999 salariés, et soit un chiffre d'affaires n'excédant pas 1,5 milliards d'euros soit un total de bilan n'excédant pas 2 milliards d'euros.<br>
    * Une entreprise qui a moins de 250 salariés, mais plus de 50 millions d'euros de chiffre d'affaires et plus de 43 millions d'euros de total de bilan est aussi considérée comme une ETI.<br>
    * GE ou Grande Entreprise : Une grande entreprise est une entreprise qui a au moins 5000 salariés. Une entreprise qui a moins de 5000 salariés mais plus de 1,5 milliards d'euros de chiffre d'affaires et plus de 2 milliards d'euros de total de bilan est aussi considérée comme une grande entreprise.
    */
   public CategorieEntreprise asCategorieEntreprise() {
      return CategorieEntreprise.valueOf(this.categorieEntreprise);
   }

   /**
    * Renvoyer la catégorie de l'entreprise (CATEGORIE).
    * @return Catégorie de l'entreprise :<br>
    * PME ou Petite ou Moyenne Entreprise : La catégorie des petites et moyennes entreprises (PME) est constituée des entreprises qui occupent moins de 250 personnes, et qui ont un chiffre d'affaires annuel inférieur à 50 millions d'euros ou un total de bilan n'excédant pas 43 millions d'euros. Cette catégorie inclut les microentreprises.<br>
    * ETI ou Entreprise de Taille Intermédiaire : Une entreprise de taille intermédiaire est une entreprise qui a entre 250 et 4999 salariés, et soit un chiffre d'affaires n'excédant pas 1,5 milliards d'euros soit un total de bilan n'excédant pas 2 milliards d'euros.<br>
    * Une entreprise qui a moins de 250 salariés, mais plus de 50 millions d'euros de chiffre d'affaires et plus de 43 millions d'euros de total de bilan est aussi considérée comme une ETI.<br>
    * GE ou Grande Entreprise : Une grande entreprise est une entreprise qui a au moins 5000 salariés. Une entreprise qui a moins de 5000 salariés mais plus de 1,5 milliards d'euros de chiffre d'affaires et plus de 2 milliards d'euros de total de bilan est aussi considérée comme une grande entreprise.
    */
   public String getCategorieEntreprise() {
      return this.categorieEntreprise;
   }

   /**
    * Fixer la catégorie de l'entreprise (CATEGORIE).
    * @param categorie Catégorie de l'entreprise.
    */
   public void setCategorieEntreprise(String categorie) {
      this.categorieEntreprise = categorie;
   }

   /**
    * Déterminer si l'entreprise a des informations diffusables.
    * @return true, si c'est le cas.
    */
   public Boolean isDiffusable() {
      return this.diffusable;
   }

   /**
    * Indiquer si l'entreprise a des informations diffusables.
    * @param diffusable true, si c'est le cas.
    */
   public void setDiffusable(Boolean diffusable) {
      this.diffusable = diffusable;
   }

   /**
    * Renvoyer le numéro Interne de Classement de l'établissement siège de l'entreprise (NICSIEGE).
    * @return Numéro interne de classement.
    */
   public NIC asNicSiege() {
      return new NIC(this.nicsiege);
   }

   /**
    * Renvoyer le numéro Interne de Classement de l'établissement siège de l'entreprise (NICSIEGE).
    * @return Numéro interne de classement.
    */
   public String getNicSiege() {
      return this.nicsiege;
   }

   /**
    * Fixer le numéro Interne de Classement de l'établissement siège de l'entreprise (NICSIEGE).
    * @param nicsiege Numéro interne de classement.
    */
   public void setNicSiege(String nicsiege) {
      this.nicsiege = nicsiege;
   }

   /**
    * Renvoyer le nom d'usage du détenteur de l'entreprise.
    * @return Nom d'usage.
    */
   public String getNomUsage() {
      return this.nomUsage;
   }

   /**
    * Fixer le nom d'usage du détenteur de l'entreprise.
    * @param nomUsage Nom d'usage.
    */
   public void setNomUsage(String nomUsage) {
      this.nomUsage = nomUsage;
   }

   /**
    * Renvoyer la dénomination de l'entreprise.
    * @return Dénomination.
    */
   public String getDenominationEntreprise() {
      return this.denominationEntreprise;
   }

   /**
    * Fixer la dénomination de l'entreprise.
    * @param denomination Dénomination.
    */
   public void setDenominationEntreprise(String denomination) {
      this.denominationEntreprise = denomination;
   }

   /**
    * Renvoyer la première dénomination de l'entreprise.
    * @return Dénominations de l'entreprise.
    */
   public String getDenominationUsuelle1() {
      return this.denominationUsuelle1;
   }

   /**
    * Fixer la première dénomination de l'entreprise.
    * @param denomination Dénomination de l'entreprise.
    */
   public void setDenominationUsuelle1(String denomination) {
      this.denominationUsuelle1 = denomination;
   }

   /**
    * Renvoyer la deuxième dénomination de l'entreprise.
    * @return Dénominations de l'entreprise.
    */
   public String getDenominationUsuelle2() {
      return this.denominationUsuelle2;
   }

   /**
    * Fixer la deuxième dénomination de l'entreprise.
    * @param denomination Dénomination de l'entreprise.
    */
   public void setDenominationUsuelle2(String denomination) {
      this.denominationUsuelle2 = denomination;
   }

   /**
    * Renvoyer la troisième dénomination de l'entreprise.
    * @return Dénominations de l'entreprise.
    */
   public String getDenominationUsuelle3() {
      return this.denominationUsuelle3;
   }

   /**
    * Fixer la troisième dénomination de l'entreprise.
    * @param denomination Dénomination de l'entreprise.
    */
   public void setDenominationUsuelle3(String denomination) {
      this.denominationUsuelle3 = denomination;
   }

   /**
    * Renvoyer Année de mise à jour de la catégorie.
    * @return Année.
    */
   public Integer getAnneeCategorie() {
      return this.anneeCategorie;
   }

   /**
    * Fixer l'année de mise à jour de la catégorie.
    * @param annee Année.
    */
   public void setAnneeCategorie(Integer annee) {
      this.anneeCategorie = annee;
   }
   
   /**
    * Renvoyer la catégorie juridique de l'entreprise.
    * @return Catégorie juridique de l'entreprise.
    */
   public String getCategorieJuridique() {
      return this.categorieJuridique;
   }

   /**
    * Fixer la catégorie juridique de l'entreprise.
    * @param categorieJuridique Catégorie juridique de l'entreprise.
    */
   public void setCategorieJuridique(String categorieJuridique) {
      this.categorieJuridique = categorieJuridique;
   }

   /**
    * Déterminer si l'entreprise est une société à mission (Loi Pacte) : elle lui permet de faire publiquement état de cette qualité et de ses objectifs sociaux et environnementaux.
    * @return true si elle est une société à mission (loi Pacte).
    */
   public Boolean isSocieteMission() {
      return this.societeMission;
   }

   /**
    * Indiquer si l'entreprise est une société à mission (Loi Pacte) : elle lui permet de faire publiquement état de cette qualité et de ses objectifs sociaux et environnementaux.
    * @param societeMission true si elle est une société à mission (loi Pacte).
    */
   public void setSocieteMission(Boolean societeMission) {
      this.societeMission = societeMission;
   }

   /**
    * Renvoyer la date de création de l'entreprise (DCREN).
    * @return Date de création de l'entreprise.
    */
   public LocalDate asDateCreationEntreprise() {
      return fromINSEEtoLocalDate(this.dateCreationEntreprise);
   }
   
   /**
    * Renvoyer la date de création de l'entreprise (DCREN).
    * @return Date de création de l'entreprise.
    */
   public String getDateCreationEntreprise() {
      return this.dateCreationEntreprise;
   }

   /**
    * Fixer la date de création de l'entreprise (DCREN).
    * @param date Date de création de l'entreprise.
    */
   public void setDateCreationEntreprise(String date) {
      this.dateCreationEntreprise = date;
   }

   /**
    * Déterminer si l'entreprise a été purgée.
    * @return true si elle l'a été.
    */
   public Boolean isPurgee() {
      return this.purgee;
   }

   /**
    * Indiquer si l'entreprise a été purgée.
    * @param purgee true si elle l'a été.
    */
   public void setPurgee(Boolean purgee) {
      this.purgee = purgee;
   }

   /**
    * @see java.lang.Comparable#compareTo(java.lang.Object)
    */
   @Override
   public int compareTo(Entreprise o) { //NOSONAR : Pas besoin d'equals
      return getSiren().compareTo(o.getSiren());
   }

   /**
    * @see ObjetMetierIdentifiable#toString()
    */
   @Override
   public String toString() {
      String format = "'{'{0}, nombre d''établissements : {16}, catégorie entreprise : {1} ({2}), catégorie juridique : {3}, n° répertoire national des associations : {4}, " +
         "Economie Sociale et Solidaire : {5}, NIC de l''établissement siège : {6}, sigle : {7}, dénomination de l''entreprise : {18}, dénominations usuelles 1 : {8}, " +
         "2 :{19}, 3 : {20}, 4 : {21} , Nom de naissance : {9}, Nom d''usage : {10}, " +
         "prénom usuel : {11}, autres prénoms : {12}, pseudonyme : {13}, sexe : {14}, purgée : {15}, " +
         "date de création : {17}'}'";

      return MessageFormat.format(format, super.toString(), this.categorieEntreprise, this.anneeCategorie, this.categorieJuridique,
         this.rna, this.economieSocialeEtSolidaire, this.nicsiege, this.sigle, this.denominationUsuelle1,
         this.nomNaissance, this.nomUsage, this.prenomUsuel, this.prenom1, this.pseudonyme,
         this.sexe, this.purgee, this.etablissements != null ? this.etablissements.size() : null, this.dateCreationEntreprise != null ? this.dateCreationEntreprise : null,
         this.denominationEntreprise, this.denominationUsuelle1, this.denominationUsuelle2, this.denominationUsuelle3);
   }
}
