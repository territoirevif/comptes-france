/**
 * Objets métiers sur les entreprises.
 * @author Marc LE BIHAN
 */
package fr.ecoemploi.domain.model.territoire.entreprise;