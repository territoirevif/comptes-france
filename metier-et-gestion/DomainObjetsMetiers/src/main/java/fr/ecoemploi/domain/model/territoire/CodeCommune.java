package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;

import org.apache.commons.lang3.StringUtils;

import fr.ecoemploi.domain.utils.autocontrole.*;
import fr.ecoemploi.domain.utils.objets.ObjetMetierIdentifiable;

/**
 * Un code de commune.
 * @author Marc LE BIHAN
 */
public class CodeCommune extends Id implements Comparable<CodeCommune> {
   /** Code de région INSEE */
   @Serial
   private static final long serialVersionUID = -5454709306625806462L;

   /**
    * Construire un id null.
    */
   public CodeCommune() {
   }

   /**
    * Construire un id alimenté.
    * @param code Code de l'id.
    */
   public CodeCommune(String code) {
      super(code);
   }

   /**
    * Construire un code commune par copie.
    * @param codeCommune Code commune.
    */
   public CodeCommune(CodeCommune codeCommune) {
      super(codeCommune);
   }

   /**
    * Renvoyer le numéro d'un département d'un code commune.
    * @return Numéro de département ou null s'il ne peut être déterminé (code trop court).
    */
   public String numeroDepartement() {
      // Si le code commune ne fait pas cinq caractères (pour une commune en métropole) ou six (pour une commune d'outremer), il est invalide et l'on ne peut rien en extraire.
      if (getId() == null || (getId().length() != 5 && getId().length() != 6)) {
         return null;
      }

      // S'il est valide, prendre les deux premières positions, à l'exception des départements d'outremer, où il est fait de trois caractères.
      return getId().startsWith("97") ? getId().substring(0, 3) : getId().substring(0, 2);
   }

   /**
    * Renvoyer le numéro de la commune dans le département : la partie terminale de RRDDCCC.
    * @return Numéro de la commune dans le département ou null s'il ne peut être déterminé (code trop court).
    */
   public String numeroCommune() {
      // Si le code commune ne fait pas cinq caractères, il est invalide et l'on ne peut rien en extraire.
      if (getId() == null || (getId().length() != 5 && getId().length() != 6)) {
         return null;
      }

      // S'il est valide, prendre les trois premières positions, à l'exception des départements d'outremer, où il est fait de deux caractères.
      return getId().startsWith("97") ? getId().substring(3) : getId().substring(2);
   }

   /**
    * Renvoyer la seule concaténation du département et du numéro de commune, sans mentionner la région : DDCCC.
    * @return Département concaténé au numéro de la commune dans le département.
    */
   public String departementEtCommune() {
      if (getId() == null)
         return null;

      return getId();
   }

   /**
    * Déterminer si ce code commune désigne une collectivité d'outre-mer :<br>
    * Attention, il s'agit des départements 975, 977, 978 et 98, et pas les départements d'outremer 971, 972, 973, 974 et 976.
    * @return true si c'est le cas.
    */
   public boolean estCollectiviteOutremer() {
      return getId() != null && (getId().startsWith("975") || getId().startsWith("977") || getId().startsWith("978") || getId().startsWith("98"));
   }
   
   /**
    * @see ObjetMetierIdentifiable#anomalies(Anomalies)
    */
   @Override
   public void anomalies(Anomalies anomalies) {
      super.anomalies(anomalies);

      // Le code de la commune doit être alimenté.
      if (StringUtils.isBlank(getId())) {
         anomalies.declare(SeveriteAnomalie.ERREUR, "Le code commune n''est pas renseigné.");
      }
      
      // Elle doit avoir un département.
      if (numeroDepartement() == null) {
         anomalies.declare(SeveriteAnomalie.ERREUR, "Le code commune {0} est dépourvu de département.", this.getId());
      }
   }

   /**
    * @see java.lang.Comparable#compareTo(java.lang.Object)
    */
   @Override
   public int compareTo(CodeCommune o) { //NOSONAR : pas besoin d'equals, ici
      return getId().compareTo(o.getId());
   }
}
