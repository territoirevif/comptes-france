package fr.ecoemploi.domain.model.territoire.association;

import java.io.*;

/**
 * Numéro d'association reconnue d'utilité publique (RUP), attribué par le Ministère.
 * @author Marc Le Bihan
 */
public class NumeroRUP implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5639586757722126670L;
   
   /** Numéro RUP. */
   private String numero;

   /**
    * Construire un numéro RUP.
    */
   public NumeroRUP() {
   }
   
   /**
    * Construire un numéro RUP.
    * @param numero Numéro RUP.
    */
   public NumeroRUP(String numero) {
      this.numero = numero;
   }

   /**
    * Construire un numéro RUP par copie.
    * @param numeroRUP Numéro RUP.
    */
   public NumeroRUP(NumeroRUP numeroRUP) {
      this.numero = numeroRUP.numero;
   }

   /**
    * Renvoyer le numéro d'association reconnue d'utilité publique (RUP), attribué par le Ministère.
    * @return Numéro d'association reconnue d'utilité publique (RUP).
    */
   public String getNumero() {
      return this.numero;
   }
   
   /**
    * Fixer le numéro d'association reconnue d'utilité publique (RUP), attribué par le Ministère.
    * @param numero Numéro d'association reconnue d'utilité publique (RUP).
    */
   public void setNumero(String numero) {
      this.numero = numero;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      return this.numero;
   }
}
