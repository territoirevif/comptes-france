package fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels;

import java.io.Serial;
import java.text.MessageFormat;

import fr.ecoemploi.domain.utils.objets.ObjetMetier;

/**
 * Ratios de niveaux et de structure d'une collectivité locale.<br>
 * D'après les 11 ratios de niveaux et de structure de la <a href="https://www.collectivites-locales.gouv.fr/">Direction Générale des Collectivités Locales</a>,
 * obligatoires pour les communes de 3 500 habitants et plus (article R.2313-1, CGCL).
 * (<a href="https://www.collectivites-locales.gouv.fr/files/files/statistiques/brochures/chapitre_4_-_les_finances_des_collectivites_locales_2.pdf">Les 11 ratios</a> ; le ratio 8 n'est plus calculé).
 * @author Marc Le Bihan
 */
public class RatiosNiveauxEtStructure extends ObjetMetier {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 201780620949573228L;

   /** Capacité de désendettement (encours dette sur épargne(= caf) brute). */
   private Double capaciteDesendettement;

   /** Dépenses réelles de fonctionnement (DRF) / population (ratio 1). */ 
   private Double depensesReellesFonctionnementSurPopulation;
   
   /*
Ratio 2 = Produit des impositions directes / population (recettes hors fiscalité reversée).

Ratio 2 bis = Produit des impositions directes / population. 
En plus des impositions directes, ce ratio intègre les prélèvements pour reversements de fiscalité et la fiscalité reversée aux communes par les groupements à fiscalité propre.

Ratio 3 = Recettes réelles de fonctionnement (RRF) / population :
montant total des recettes de fonctionnement en mouvements réels.
Ressources dont dispose la collectivité, à comparer aux dépenses de fonctionnement dans leur rythme de croissance.

Ratio 4 = Dépenses brutes d’équipement / population : 
dépenses des comptes 20 (immobilisations incorporelles) sauf 204 (subventions d’équipement versées), 21 (immobilisations corporelles), 
23 (immobilisations en cours), 454 (travaux effectués d’office pour le compte de tiers), 456 (opérations d’investissement sur établissement d’enseignement) 
et 458 (opérations d’investissement sous mandat). 
Les travaux en régie sont ajoutés au calcul. Pour les départements et les régions, on rajoute le débit du compte correspondant aux opérations d’investissement sur établissements publics locaux d’enseignement (455 en M14).

Ratio 5 = Dette / population : capital restant dû au 31 décembre de l’exercice. Endettement d’une collectivité à compléter avec un ratio de capacité de désendettement (dette / épargne brute) et le taux d’endettement (ratio 11).

Ratio 6 = DGF / population : recettes du compte 741 en mouvements réels, part de la contribution de l’État au fonctionnement de la collectivité.

Ratio 7 = Dépenses de personnel / DRF : mesure la charge de personnel de la collectivité ; c’est un coefficient de rigidité car c’est une dépense incompressible à court terme, quelle que soit la population de la collectivité.

Ratio 9 = Marge d’autofinancement courant (MAC) = (DRF + Remboursement de dette) / RRF : capacité de la collectivité à financer l’investissement une fois les charges obligatoires payées. Les remboursements de dette sont calculés hors gestion active de la dette. 
Plus le ratio est faible, plus la capacité à financer l’investissement est élevée ; a contrario, un ratio supérieur à 100 % indique un recours nécessaire à l’emprunt pour financer l’investissement. Les dépenses liées à des travaux en régie sont exclues des DRF.

Ratio 10 = Dépenses brutes d’équipement / RRF = taux d’équipement : effort d’équipement de la collectivité au regard de sa richesse. À relativiser sur une année donnée car les programmes d’équipement se jouent souvent sur plusieurs années. 
Les dépenses liées à des travaux en régie, ainsi que celles pour compte de tiers sont ajoutées aux dépenses d’équipement brut.

Ratio 11 = Dette / RRF = taux d’endettement : mesure la charge de la dette d’une collectivité relativement à sa richesse.

Population « municipale », « comptée à part », « totale », et population « DGF » :
Dans le recensement de la population, la « population totale » est égale à la « population municipale » 
augmentée de la « population comptée à part », c’est-à-dire les personnes recensées sur d’autres communes mais qui ont conservé un lien avec une résidence sur la commune (par exemple les étudiants). Pour le calcul des dotations on inclut ces habitants comptés à part ; on considère en effet que ces personnes pèsent sur le budget de fonctionnement de la commune même si elles résident habituellement dans une autre commune.

Pour tenir compte des conditions particulières de certaines communes, qui pèsent sur leur fonctionnement, cette population totale est, en plus, majorée en fonction de deux critères particuliers. 
Il ne s’agit plus d’habitants « réels » recensés, mais d’une attribution forfaitaire exprimée en nombre d’habitants par commodité de calcul.
- majoration en fonction du nombre de résidences secondaires : la population totale issue du recensement est forfaitairement majorée d’un habitant par résidence secondaire. Elles sont particulièrement nombreuses dans les zones touristiques. Cela aboutit à majorer la population nationale à ce titre d’un forfait de plus de 3 millions « d’habitants ».
- majoration pour places de caravanes dans les aires d’accueil des gens du voyage. Selon la même logique, la population totale est majorée d’un habitant par place de caravane située sur une aire d’accueil des gens du voyage. La majoration de population est portée à deux habitants par place de caravane pour les communes éligibles l’année précédente à la dotation de solidarité urbaine (DSU) ou à la première fraction (bourg-centre) de la dotation de solidarité rurale (DSR). Les caravanes de campings dans le cadre de l’hôtellerie de plein air ne sont pas concernées.
Une fois effectuées ces deux majorations de la population totale, on obtient une population forfaitaire, dite « population DGF », car utilisée pour calculer la principale dotation que lui verse l’État pour son fonctionnement, la dotation globale de fonctionnement (DGF). Dans les tableaux du chapitre 4.7b, les ratios relatifs aux communes touristiques sont calculés par rapport à la population DGF. 
Les ratios des autres chapitres sont calculés par rapport à la population totale.
    */
   
   /**
    * Construire des ratios de niveaux et de structure.
    */
   public RatiosNiveauxEtStructure() {
   }

   /**
    * Construire des ratios de niveaux et de structure.
    * @param capaciteDesendettement Capacité de désendettement (encours dette sur épargne brute).
    * @param depensesReellesFonctionnementSurPopulation Dépenses réelles de fonctionnement (DRF) / population (ratio 1).
    */
   public RatiosNiveauxEtStructure(Double capaciteDesendettement, Double depensesReellesFonctionnementSurPopulation) {
      this.capaciteDesendettement = capaciteDesendettement;
      this.depensesReellesFonctionnementSurPopulation = depensesReellesFonctionnementSurPopulation;
   }

   /**
    * Renvoyer la capacité de désendettement (encours dette au 31/12 rapportée à l’épargne brute), en années.<br>
    * En combien d’années une collectivité peut-elle rembourser sa dette si elle utilise pour cela son épargne brute ? Un indicateur qui augmente indique donc une situation qui se dégrade. 
    * @return Capacité de désendettement.
    */
   public Double getCapaciteDesendettement() {
      return this.capaciteDesendettement;
   }

   /**
    * Fixer la capacité de désendettement (encours dette sur épargne(caf) brute), en années.
    * @param capaciteDesendettement la capacité de désendettement, en années.
    */
   public void setCapaciteDesendettement(Double capaciteDesendettement) {
      this.capaciteDesendettement = capaciteDesendettement;
   }

   /**
    * Renvoyer le ratio Dépenses réelles de fonctionnement (DRF) / population (ratio 1) :<br>
    * montant total des dépenses de fonctionnement en mouvement réels. 
    * Les dépenses liées à des travaux en régie (crédit du compte 72) sont soustraites aux DRF.
    * @return Ratio. 
    */
   public Double getDepensesReellesFonctionnementSurPopulation() {
      return this.depensesReellesFonctionnementSurPopulation;
   }

   /**
    * Fixer Renvoyer le ratio Dépenses réelles de fonctionnement (DRF) / population (ratio 1). 
    * @param depensesReellesFonctionnementSurPopulation ratio.
    */
   public void setDepensesReelesFonctionnementSurPopulation(Double depensesReellesFonctionnementSurPopulation) {
      this.depensesReellesFonctionnementSurPopulation = depensesReellesFonctionnementSurPopulation;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      String[] analyse = {
         MessageFormat.format("\nCapacité de désendettement, en années : {0}", getCapaciteDesendettement()),
         MessageFormat.format("Dépenses réelles de fonctionnement (DRF) / population : {0}", getDepensesReellesFonctionnementSurPopulation())
      };
         
      StringBuilder analyseFinanciere = new StringBuilder();

      for(String ligne : analyse) {
         analyseFinanciere.append(ligne).append("\n");
      }
      
      return analyseFinanciere.toString();
   }
}
