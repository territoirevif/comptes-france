package fr.ecoemploi.domain.model.catalogue.metadata;

/**
 * Périodicité de la source.
 * @author Marc LE BIHAN
 */
public enum PeriodiciteSource {
   /** Périodicité annuelle */
   ANNUELLE("Annuelle"),

   /** Périodicité mensuelle */
   MENSUELLE("Mensuelle"),
   
   /** À date */
   DATE("À date");
   
   /** Libellé de l'entrée. */
   private final String libelle;
   
   /**
    * Construire une entrée d'énumération en recherchant son libellé public.
    * @param libelle Libelle de cette périodicité.
    */
   PeriodiciteSource(String libelle) {
      this.libelle = libelle;
   }
   
   /**
    * Renvoyer le libellé public de l'entrée.
    * @return Libellé.
    */
   public String getLibelle() {
      return this.libelle;
   }
   
   /**
    * @see java.lang.Enum#toString()
    */
   @Override
   public String toString() {
      return this.libelle;
   }
}
