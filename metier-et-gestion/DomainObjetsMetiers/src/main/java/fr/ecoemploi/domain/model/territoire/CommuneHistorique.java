package fr.ecoemploi.domain.model.territoire;

import org.apache.commons.lang3.builder.*;

import java.io.Serial;
import java.time.LocalDate;
import java.util.Arrays;

import fr.ecoemploi.domain.utils.objets.ObjetMetier;

/**
 * Une commune historique.
 * @author Marc Le Bihan
 */
public class CommuneHistorique extends ObjetMetier {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -2191025099960572722L;

   /** Type d'évènement communal. */
   private TypeEvenementCommunal typeEvenementCommunal;

   /** Date d'effet (AAAA-MM-JJ) */
   private LocalDate dateEffetEvenement;

   /** Type de la commune */
   private String typeCommune;

   /** Code de la commune historique (celle dont on veut savoir si elle s'est transformée) */
   private String codeCommuneHistorique;

   /** Nom de la commune historique (celle dont on veut savoir si elle s'est transformée) */
   private String nomCommuneHistorique;

   /** Type de la commune historique (celle dont on veut savoir si elle s'est transformée) */
   private String typeCommuneHistorique;

   /** Code de la commune (le code que la commune historique a, à la date d'effet) */
   private String codeCommune;

   /** Type de nom en clair */
   private String typeDeNom;

   /** Nom en clair (majuscules) */
   private String nomCommuneMajuscules;

   /** Nom en clair (typographie riche) */
   private String nomCommune;

   /** Nom en clair (typographie riche) avec article */
   private String nomCommuneAvecArticle;

   /**
    * Construire une commune historique.
    */
   public CommuneHistorique() {
   }

   /**
    * Construire une commune historique.
    * @param codeCommuneHistorique Code de la commune historique (celle dont on veut savoir si elle s'est transformée)
    * @param nomCommuneHistorique Nom de la commune historique (celle dont on veut savoir si elle s'est transformée)
    * @param typeCommuneHistorique Type de la commune historique (celle dont on veut savoir si elle s'est transformée)
    * @param typeEvenement Type d'évenèment qui a affecté la commune.
    * @param dateEffetEvenement Date d'effet de l'évènement.
    * @param typeCommune Type de la commune
    * @param codeCommune Code de la commune (le code que la commune historique a, à la date d'effet)
    * @param typeDeNom Type de nom en clair
    * @param nomCommuneMajuscules  Nom en clair (majuscules)
    * @param nomCommune Nom en clair (typographie riche)
    * @param nomCommuneAvecArticle Nom en clair (typographie riche) avec article
    */
   public CommuneHistorique(String codeCommuneHistorique, String nomCommuneHistorique, String typeCommuneHistorique,
       int typeEvenement, LocalDate dateEffetEvenement,
       String typeCommune, String codeCommune, String typeDeNom, String nomCommuneMajuscules, String nomCommune, String nomCommuneAvecArticle) {
      this.typeEvenementCommunal = Arrays.stream(TypeEvenementCommunal.values()).filter(type -> type.getType() == typeEvenement).findFirst().orElse(null);
      this.dateEffetEvenement = dateEffetEvenement;
      this.typeCommune = typeCommune;
      this.codeCommuneHistorique = codeCommuneHistorique;
      this.codeCommune = codeCommune;
      this.typeDeNom = typeDeNom;
      this.nomCommuneMajuscules = nomCommuneMajuscules;
      this.nomCommune = nomCommune;
      this.nomCommuneAvecArticle = nomCommuneAvecArticle;
   }

   /**
    * Renvoyer le code de la commune.
    * @return Code de la commune.
    */
   public String getCodeCommune() {
      return this.codeCommune;
   }

   /**
    * Fixer le code de la commune.
    * @param codeCommune Code de la commune.
    */
   public void setCodeCommune(String codeCommune) {
      this.codeCommune = codeCommune;
   }

   /**
    * Renvoyer le code de la commune historique (celle dont on veut savoir si elle s'est transformée).
    * @return Code de la commune.
    */
   public String getCodeCommuneHistorique() {
      return this.codeCommuneHistorique;
   }

   /**
    * Fixer le code de la commune historique (celle dont on veut savoir si elle s'est transformée).
    * @param codeCommune Code de la commune.
    */
   public void setCodeCommuneHistorique(String codeCommune) {
      this.codeCommuneHistorique = codeCommune;
   }

   /**
    * Renvoyer le nom de la commune historique (celle dont on veut savoir si elle s'est transformée).
    * @return Nom de la commune.
    */
   public String getNomCommuneHistorique() {
      return this.nomCommuneHistorique;
   }

   /**
    * Fixer le nom de la commune historique (celle dont on veut savoir si elle s'est transformée).
    * @param nomCommune Nom de la commune.
    */
   public void setNomCommuneHistorique(String nomCommune) {
      this.nomCommuneHistorique = nomCommune;
   }

   /**
    * Renvoyer le type de la commune historique (celle dont on veut savoir si elle s'est transformée).
    * @return Code de la commune.
    */
   public String getTypeCommuneHistorique() {
      return this.typeCommuneHistorique;
   }

   /**
    * Fixer le type de la commune historique (celle dont on veut savoir si elle s'est transformée).
    * @param typeCommune Type de la commune.
    */
   public void setTypeCommuneHistorique(String typeCommune) {
      this.typeCommuneHistorique = typeCommune;
   }

   /**
    * Renvoyer la date d'effet.
    * @return Date d'effet.
    */
   public LocalDate getDateEffetEvenement() {
      return this.dateEffetEvenement;
   }

   /**
    * Fixer la date d'effet.
    * @param dateEffetEvenement Date d'effet.
    */
   public void setDateEffetEvenement(LocalDate dateEffetEvenement) {
      this.dateEffetEvenement = dateEffetEvenement;
   }

   /**
    * Renvoyer le nom en clair (typographie riche).
    * @return Nom en clair (typographie riche).
    */
   public String getNomCommune() {
      return this.nomCommune;
   }

   /**
    * Fixer le nom en clair (typographie riche).
    * @param nomCommune Nom en clair (typographie riche).
    */
   public void setNomCommune(String nomCommune) {
      this.nomCommune = nomCommune;
   }

   /**
    * Renvoyer le nom en clair (typographie riche) avec article.
    * @return Nom en clair (typographie riche) avec article.
    */
   public String getNomCommuneAvecArticle() {
      return this.nomCommuneAvecArticle;
   }

   /**
    * Fixer le nom en clair (typographie riche) avec article.
    * @param nomCommuneAvecArticle Nom en clair (typographie riche) avec article.
    */
   public void setNomCommuneAvecArticle(String nomCommuneAvecArticle) {
      this.nomCommuneAvecArticle = nomCommuneAvecArticle;
   }

   /**
    * Renvoyer le nom en clair (majuscules).
    * @return Nom en clair (majuscules).
    */
   public String getNomCommuneMajuscules() {
      return this.nomCommuneMajuscules;
   }

   /**
    * Fixer le nom en clair (majuscules).
    * @param nomCommuneMajuscules Nom en clair (majuscules).
    */
   public void setNomCommuneMajuscules(String nomCommuneMajuscules) {
      this.nomCommuneMajuscules = nomCommuneMajuscules;
   }

   /**
    * Renvoyer le type de la commune.
    * @return Type de la commune.
    */
   public String getTypeCommune() {
      return this.typeCommune;
   }

   /**
    * Fixer le type de la commune.
    * @param typeCommune Type de la commune.
    */
   public void setTypeCommune(String typeCommune) {
      this.typeCommune = typeCommune;
   }

   /**
    * Renvoyer le type de nom en clair.
    * @return Type de nom en clair.
    */
   public String getTypeDeNom() {
      return this.typeDeNom;
   }

   /**
    * Fixer le type de nom en clair.
    * @param typeDeNom Type de nom en clair.
    */
   public void setTypeDeNom(String typeDeNom) {
      this.typeDeNom = typeDeNom;
   }

   /**
    * Renvoyer le type d'évènement qui a affecté la commune.
    * @return Type d'évènement.
    */
   public TypeEvenementCommunal getTypeEvenement() {
      return this.typeEvenementCommunal;
   }

   /**
    * Fixer le type d'évènement qui a affecté la commune.
    * @param typeEvenement Type d'évènement.
    */
   public void setTypeEvenement(TypeEvenementCommunal typeEvenement) {
      this.typeEvenementCommunal = typeEvenement;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean equals(Object o) {
      if (o instanceof CommuneHistorique candidat) {
         // Les champ codeCommuneHistorique, nomCommuneHistorique, typeCommuneHistorique sont informatifs, ils ne font pas partie du test d'égalité.
         EqualsBuilder equals = new EqualsBuilder()
            .append(this.typeEvenementCommunal, candidat.typeEvenementCommunal)
            .append(this.dateEffetEvenement, candidat.dateEffetEvenement)
            .append(this.typeCommune, candidat.typeCommune)
            .append(this.codeCommune, candidat.codeCommune)
            .append(this.typeDeNom, candidat.typeDeNom)
            .append(this.nomCommuneMajuscules, candidat.nomCommuneMajuscules)
            .append(this.nomCommune, candidat.nomCommune)
            .append(this.nomCommuneAvecArticle, candidat.nomCommuneAvecArticle);

         return equals.isEquals();
      }

      return false;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public int hashCode() {
      // Le champ codeCommuneHistorique est informatif, il ne fait pas partie du calcul du hashcode.
      HashCodeBuilder hash = new HashCodeBuilder()
         .append(this.typeEvenementCommunal)
         .append(this.dateEffetEvenement)
         .append(this.typeCommune)
         .append(this.codeCommune)
         .append(this.typeDeNom)
         .append(this.nomCommuneMajuscules)
         .append(this.nomCommune)
         .append(this.nomCommuneAvecArticle);

      return hash.toHashCode();
   }
}
