package fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels;

import java.io.Serial;
import java.util.*;

/**
 * Comptes individuels de la forme : montant total ou base, montant par habitant, moyenne strate par habitant.
 * @author Marc Le Bihan
 */
public class ComptesAvecMoyenneStrate extends ComptesTotalOuBaseEtParHabitant {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 58408951372328746L;
   
   /** Moyenne de strate */
   private Double moyenneStrate;

   /**
    * Construire de comptes montant (total ou base), par habitant + moyenne strate par habitant.
    */
   public ComptesAvecMoyenneStrate() {
   }

   /**
    * Construire de comptes montant (total ou base), par habitant + moyenne strate par habitant.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    */
   public ComptesAvecMoyenneStrate(String libelleAnalyse) {
      super(libelleAnalyse);
   }
   
   /**
    * Construire de comptes montant (total ou base), par habitant + moyenne strate par habitant.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    * @param montant Montant (total ou base).
    * @param parHabitant Montant par habitant.
    * @param moyenneStrate Moyenne de la strate par habitant.
    */
   public ComptesAvecMoyenneStrate(String libelleAnalyse, Double montant, Double parHabitant, Double moyenneStrate) {
      super(libelleAnalyse, montant, parHabitant);
      this.moyenneStrate = moyenneStrate;
   }

   /**
    * Renvoyer la moyenne de strate.
    * @return Moyenne de strate.
    */
   public Double getMoyenneStrate() {
      return this.moyenneStrate;
   }

   /**
    * Fixer la moyenne de strate.
    * @param moyenneStrate Moyenne de strate.
    */
   public void setMoyenneStrate(Double moyenneStrate) {
      this.moyenneStrate = moyenneStrate;
   }
   
   /**
    * Formatter une ligne texte pour qu'elle se place dans un tableau textuel d'analyse des équilibres financiers fondamentaux.
    * @return Ligne formatée. 
    */
   @Override
   public String toStringAnalyseEquilibresFinanciersFondamentaux() {
      final String FMT_NOMBRE_ENTIER = "%10d\t";
      StringBuilder format = new StringBuilder();
      List<Object> valeurs = new ArrayList<>();

      format.append(getMontant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getMontant() != null ? Math.round(getMontant()) : "");

      format.append(getParHabitant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getParHabitant() != null ? Math.round(getParHabitant()) : "");

      format.append(getMoyenneStrate() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getMoyenneStrate() != null ? Math.round(getMoyenneStrate()) : "");

      format.append("%s");
      valeurs.add(getLibelleAnalyse());

      return String.format(format.toString(), valeurs.toArray());
   }
}
