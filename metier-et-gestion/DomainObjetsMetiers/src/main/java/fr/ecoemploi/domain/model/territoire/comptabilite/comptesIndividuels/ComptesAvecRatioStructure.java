package fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels;

import java.io.Serial;
import java.util.*;

/**
 * Comptes individuels de la forme : montant total ou base, montant par habitant et ratio de structure.
 * @author Marc Le Bihan
 */
public class ComptesAvecRatioStructure extends ComptesTotalOuBaseEtParHabitant {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -8909467857178828816L;
   
   /** Ratio de structure */
   private Double ratioStructure;

   /**
    * Construire de comptes montant (total ou base), par habitant, ratio de structure.
    */
   public ComptesAvecRatioStructure() {
   }

   /**
    * Construire de comptes montant (total ou base), par habitant, ratio de structure.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    */
   public ComptesAvecRatioStructure(String libelleAnalyse) {
      super(libelleAnalyse);
   }
   
   /**
    * Construire de comptes montant (total ou base), par habitant, ratio de structure.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    * @param montant Montant (total ou base).
    * @param parHabitant Montant par habitant.
    * @param ratioStructure Ratio de structure.
    */
   public ComptesAvecRatioStructure(String libelleAnalyse, Double montant, Double parHabitant, Double ratioStructure) {
      super(libelleAnalyse, montant, parHabitant);
      this.ratioStructure = ratioStructure;
   }

   /**
    * Renvoyer le ratio de structure.
    * @return Ratio de structure.
    */
   public Double getRatioStructure() {
      return this.ratioStructure;
   }

   /**
    * Fixer le ratio de structure.
    * @param ratioStructure Ratio de structure.
    */
   public void setRatioStructure(Double ratioStructure) {
      this.ratioStructure = ratioStructure;
   }
   
   /**
    * Formatter une ligne texte pour qu'elle se place dans un tableau textuel d'analyse des équilibres financiers fondamentaux.
    * @return Ligne formatée. 
    */
   @Override
   public String toStringAnalyseEquilibresFinanciersFondamentaux() {
      StringBuilder format = new StringBuilder();
      List<Object> valeurs = new ArrayList<>();

      format.append(getMontant() != null ? "%10d\t" : "%s\t");
      valeurs.add(getMontant() != null ? Math.round(getMontant()) : "");

      format.append(getParHabitant() != null ? "%10d\t" : "%s\t");
      valeurs.add(getParHabitant() != null ? Math.round(getParHabitant()) : "");

      format.append("%s\t");
      valeurs.add(getLibelleAnalyse());

      format.append(getRatioStructure() != null ? "%10.2f" : "%s");
      valeurs.add(getRatioStructure() != null ? getRatioStructure() : "");

      return String.format(format.toString(), valeurs.toArray());
   }
}
