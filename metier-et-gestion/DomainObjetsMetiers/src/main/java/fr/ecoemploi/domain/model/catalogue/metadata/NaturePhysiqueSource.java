package fr.ecoemploi.domain.model.catalogue.metadata;

/**
 * Nature physique d'une source de données.
 * @author Marc LE BIHAN
 */
public enum NaturePhysiqueSource {
   /** Fichier CSV */
   CSV("Fichier CSV"),
   
   /** Fichier Excel */
   EXCEL("Fichier Excel"),
   
   /** Fichier Plat */
   FICHIER_PLAT("Fichier plat"),
   
   /** Fichier DBF (associé au fichier Shapefile) */
   DBF("Fichier DBase III"),
   
   /** Shapefile */
   SHAPEFILE("Shapefile");

   /** Libellé de l'entrée. */
   private final String libelle;
   
   /**
    * Construire une entrée d'énumération en recherchant son libellé public.
    * @param libelle Libellé de la nature physique de la source.
    */
   NaturePhysiqueSource(String libelle) {
      this.libelle = libelle;
   }
   
   /**
    * Renvoyer le libellé public de l'entrée.
    * @return Libellé.
    */
   public String getLibelle() {
      return this.libelle;
   }
   
   /**
    * @see java.lang.Enum#toString()
    */
   @Override
   public String toString() {
      return this.libelle;
   }
}
