package fr.ecoemploi.domain.model.catalogue;

import fr.ecoemploi.domain.utils.autocontrole.Id;

import java.io.Serial;

/**
 * Identifiant d'un catalogue.
 */
public class CatalogueId extends Id {
   @Serial
   private static final long serialVersionUID = -7167450129673215370L;

   /**
    * Construire un identifiant.
    */
   public CatalogueId() {
   }

   /**
    * Construire un identifiant.
    * @param id Identifiant.
    */
   public CatalogueId(String id) {
      super(id);
   }

   /**
    * Construire un identifiant par copie.
    * @param id Identifiant.
    */
   public CatalogueId(CatalogueId id) {
      super(id);
   }
}
