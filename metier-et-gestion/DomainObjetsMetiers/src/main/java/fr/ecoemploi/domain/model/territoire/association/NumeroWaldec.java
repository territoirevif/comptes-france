package fr.ecoemploi.domain.model.territoire.association;

import java.io.*;

/**
 * Numéro waldec d'association.
 * @author Marc Le Bihan
 */
public class NumeroWaldec implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3980352298482144937L;

   /** Numéro waldec. */
   private String numero;
   
   /**
    * Construire un numéro waldec.
    */
   public NumeroWaldec() {
   }
   
   /**
    * Construire un numéro waldec.
    * @param numero Numéro waldec.
    */
   public NumeroWaldec(String numero) {
      this.numero = numero;
   }

   /**
    * Construire un numéro waldec par copie.
    * @param numero Numéro waldec.
    */
   public NumeroWaldec(NumeroWaldec numero) {
      this.numero = numero.getNumero();
   }

   /**
    * Renvoyer le numéro waldec.
    * @return Numéro waldec.
    */
   public String getNumero() {
      return this.numero;
   }
   
   /**
    * Fixer le numéro waldec.
    * @param numero waldec.
    */
   public void setNumero(String numero) {
      this.numero = numero;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      return this.numero;
   }
}
