package fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels;

import java.io.Serial;
import java.util.*;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Comptes avec total, par habitant, moyenne strate, total réduction votée, réduction par habitant, réduction moyenne strate.
 * @author Marc Le Bihan
 */
@Schema(description = "Comptes avec total, par habitant, moyenne strate, total réduction votée, réduction par habitant, réduction moyenne strate",
   allOf = ComptesAvecReductionVotee.class, discriminatorProperty = "typeCompteIndividuel")
public class ComptesAvecStrateEtReductionVotee extends ComptesAvecReductionVotee {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 7669861827075561433L;
   
   /** Moyenne de strate */
   @Schema(description = "Moyenne de strate")
   private Double moyenneStrate;
   
   /** Réductions de base accordées sur délibérations : par habitant */
   @Schema(description = "Réductions de base accordées sur délibérations : par habitant")
   private Double reductionVoteeParHabitant;
   
   /** Réductions de base accordées sur délibérations : moyenne strate */
   @Schema(description = "Réductions de base accordées sur délibérations : moyenne strate")
   private Double reductionVoteeMoyenneStrate;

   /**
    * Construire de comptes montant (total ou base), par habitant, moyenne strate par habitant + réduction votée, réduction votée par habitant, réduction votée moyenne strate.
    */
   public ComptesAvecStrateEtReductionVotee() {
   }

   /**
    * Construire de comptes montant (total ou base), par habitant, moyenne strate par habitant + réduction votée, réduction votée par habitant, réduction votée moyenne strate.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    */
   public ComptesAvecStrateEtReductionVotee(String libelleAnalyse) {
      super(libelleAnalyse);
   }
   
   /**
    * Construire de comptes montant (total ou base), par habitant, moyenne strate par habitant + réduction votée, réduction votée par habitant, réduction votée moyenne strate.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    * @param montant Montant (total ou base).
    * @param parHabitant Montant par habitant.
    * @param moyenneStrate Moyenne de la strate par habitant.
    * @param reductionVotee Réduction votée.
    * @param reductionVoteeParHabitant Réduction votée par habitant.
    * @param reductionVoteeMoyenneStrate Réduction votée moyenne strate.
    */
   public ComptesAvecStrateEtReductionVotee(String libelleAnalyse,  Double montant, Double parHabitant, Double moyenneStrate, Double reductionVotee, Double reductionVoteeParHabitant, Double reductionVoteeMoyenneStrate) {
      super(libelleAnalyse, montant, parHabitant, reductionVotee);
      this.moyenneStrate = moyenneStrate;
      this.reductionVoteeParHabitant = reductionVoteeParHabitant;
      this.reductionVoteeMoyenneStrate = reductionVoteeMoyenneStrate; 
   }

   /**
    * Renvoyer la moyenne de strate.
    * @return Moyenne de strate.
    */
   public Double getMoyenneStrate() {
      return this.moyenneStrate;
   }

   /**
    * Fixer la moyenne de strate.
    * @param moyenneStrate Moyenne de strate.
    */
   public void setMoyenneStrate(Double moyenneStrate) {
      this.moyenneStrate = moyenneStrate;
   }

   /**
    * Renvoyer la réduction votée par habitant.
    * @return Réduction votée par habitant.
    */
   public Double getReductionVoteeParHabitant() {
      return this.reductionVoteeParHabitant;
   }

   /**
    * Fixer la réduction votée par habitant.
    * @param reductionVoteeParHabitant Réduction votée par habitant. 
    */
   public void setReductionVoteeParHabitant(Double reductionVoteeParHabitant) {
      this.reductionVoteeParHabitant = reductionVoteeParHabitant;
   }

   /**
    * Renvoyer la réduction votée par habitant, moyenne strate.
    * @return Réduction votée par habitant, moyenne strate.
    */
   public Double getReductionVoteeMoyenneStrate() {
      return this.reductionVoteeMoyenneStrate;
   }

   /**
    * Fixer la réduction votée par habitant, moyenne strate.
    * @param reductionVoteeMoyenneStrate Réduction votée par habitant, moyenne strate. 
    */
   public void setReductionVoteeMoyenneStrate(Double reductionVoteeMoyenneStrate) {
      this.reductionVoteeMoyenneStrate = reductionVoteeMoyenneStrate;
   }
   
   /**
    * Formatter une ligne texte pour qu'elle se place dans un tableau textuel d'analyse des équilibres financiers fondamentaux.
    * @return Ligne formatée. 
    */
   @Override
   public String toStringAnalyseEquilibresFinanciersFondamentaux() {
      final String FMT_NOMBRE_ENTIER = "%10d\t";
      StringBuilder format = new StringBuilder();
      List<Object> valeurs = new ArrayList<>();

      format.append(getMontant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getMontant() != null ? Math.round(getMontant()) : "");

      format.append(getParHabitant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getParHabitant() != null ? Math.round(getParHabitant()) : "");

      format.append(getMoyenneStrate() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getMoyenneStrate() != null ? Math.round(getMoyenneStrate()) : "");

      format.append("%s\t");
      valeurs.add(getLibelleAnalyse());

      format.append(getReductionVotee() != null ? "%10.2f\t" : "%s\t");
      valeurs.add(getReductionVotee() != null ? getReductionVotee() : "");

      format.append(getReductionVoteeParHabitant() != null ? "%10.2f\t" : "%s\t");
      valeurs.add(getReductionVoteeParHabitant() != null ? getReductionVoteeParHabitant() : "");

      format.append(getReductionVoteeMoyenneStrate() != null ? "%10.2f" : "%s");
      valeurs.add(getReductionVoteeMoyenneStrate() != null ? getReductionVoteeMoyenneStrate() : "");

      return String.format(format.toString(), valeurs.toArray());
   }
}
