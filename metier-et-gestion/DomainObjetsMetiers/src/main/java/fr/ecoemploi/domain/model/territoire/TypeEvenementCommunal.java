package fr.ecoemploi.domain.model.territoire;

/**
 * Type d'évènement communal
 * @author Marc Le Bihan
 */
public enum TypeEvenementCommunal {
   /** Aucun changement */
   AUCUN_CHANGEMENENT(0),

   /** Changement de nom */
   CHANGEMENT_DE_NOM(10),

   /** Création. */
   CREATION(20),

   /** Rétablissement. */
   RETABLISSEMENT(21),

   /** Suppression. */
   SUPPRESSION(30),

   /** Fusion simple. */
   FUSION_SIMPLE(31),

   /** Création de commune nouvelle. */
   CREATION_DE_COMMUNE_NOUVELLE(32),

   /** Fusion association. */
   FUSION_ASSOCIATION(33),

   /** Transformation de fusion-association en fusion simple : la commune associée (COMA) disparaît. */
   TRANSFORMATION_DE_FUSION_ASSOCIATION_EN_FUSION_SIMPLE(34),

   /** Suppression de commune déléguée : elle retrouve son statut de commune à part entière. */
   SUPPRESSION_DE_COMMUNE_DELEGUEE(35),

   /** Changement de code dû à un changement de département. */
   CHANGEMENT_DE_CODE_DU_A_UN_CHANGEMENT_DE_DEPARTEMENT(41),

   /** Changement de code dû à un transfert de chef lieu. */
   CHANGEMENT_DE_CODE_DU_A_UN_TRANSFERT_DE_CHEF_LIEU(50),

   /** Transformation de commune associée en commune déléguée. */
   TRANSFORMATION_DE_COMMUNE_ASSOCIE_EN_COMMUNE_DELEGUEE(70);

   /** Type de l'évènement. */
   private final int type;

   /**
    * Construire un Type d'évènement communal.
    * @param type Type de l'évènement.
    */
   TypeEvenementCommunal(int type) {
      this.type = type;
   }

   /**
    * Renvoyer le type de l'évènement communal.
    * @return type de l'évènement communal.
    */
   public int getType() {
      return this.type;
   }
}
