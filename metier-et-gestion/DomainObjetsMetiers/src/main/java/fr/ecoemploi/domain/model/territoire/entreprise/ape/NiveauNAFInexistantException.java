package fr.ecoemploi.domain.model.territoire.entreprise.ape;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.MetierException;

/**
 * Exception levée si un niveau NAF n'existe pas.
 * @author Marc LE BIHAN
 */
public class NiveauNAFInexistantException extends MetierException {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 4351896583643125521L;

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    */
   public NiveauNAFInexistantException(String message) {
      super(message);
   }

   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param cause Cause racine de l'exception.
    */
   public NiveauNAFInexistantException(String message, Throwable cause) {
      super(message, cause);
   }
}
