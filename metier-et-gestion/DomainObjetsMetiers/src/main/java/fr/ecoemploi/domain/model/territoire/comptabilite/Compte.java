package fr.ecoemploi.domain.model.territoire.comptabilite;

import fr.ecoemploi.domain.utils.objets.ObjetMetierIdentifiable;
import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.domain.utils.autocontrole.ObjetMetierSpark;

import java.io.Serial;
import java.text.MessageFormat;

/**
 * Compte de comptabilité.
 * @author Marc LE BIHAN
 */
public class Compte extends ObjetMetierSpark {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -6681028994974971945L;

   /** Numéro de compte. */
   private String numeroCompte;
   
   /** Année d'exercice. */
   private Integer anneeExercice;
   
   /** Code de la commune à laquelle est associé ce compte. */
   private String codeCommune;
   
   /** SIRET de l'établissement de la commune ou de l'intercommunalité à laquelle est associée ce compte. */
   private String siret;
   
   /** Libellé du budget du compte. */
   private String libelleBudget;
   
   /** Libellé du compte. */
   private String libelleCompte;
   
   /** Indique s'il s'agit d'un compte lié à un budget principal. */
   private boolean budgetPrincipal;
   
   /** Débit. */
   private double soldeCrediteur;
   
   /** Crédit. */
   private double soldeDebiteur;
   
   /**
    * Renvoyer l'année d'exercice. 
    * @return Année d'exercice.
    */
   public Integer getAnneeExercice() {
      return this.anneeExercice;
   }

   /**
    * Fixer l'année d'exercice.
    * @param anneeExercice Année d'exercice.
    */
   public void setAnneeExercice(Integer anneeExercice) {
      this.anneeExercice = anneeExercice;
   }

   /**
    * Déterminer s'il s'agit d'un compte lié à un budget principal.
    * @return true si c'est le cas<br>
    * false, s'il s'agit d'un budget annexe.
    */
   public boolean isBudgetPrincipal() {
      return this.budgetPrincipal;
   }

   /**
    * Indique s'il s'agit d'un compte lié à un budget principal.
    * @param budgetPrincipal true si c'est le cas<br>
    * false, s'il s'agit d'un budget annexe.
    */
   public void setBudgetPrincipal(boolean budgetPrincipal) {
      this.budgetPrincipal = budgetPrincipal;
   }

   /**
    * Renvoyer le code commune détentrice de ce compte.
    * @return Code commune, peut valoir null.
    */
   public CodeCommune asCodeCommune() {
      return new CodeCommune(this.codeCommune);
   }

   /**
    * Renvoyer le code commune détentrice de ce compte.
    * @return Code commune, peut valoir null.
    */
   public String getCodeCommune() {
      return this.codeCommune;
   }

   /**
    * Fixer le code commune.
    * @param codeCommune Code commune, peut valoir null. 
    */
   public void setCodeCommune(String codeCommune) {
      this.codeCommune = codeCommune;
   }

   /**
    * Renvoyer le libellé du budget associé à ce compte.
    * @return Libellé.
    */
   public String getLibelleBudget() {
      return this.libelleBudget;
   }

   /**
    * Fixer le libellé du budget associé à ce compte.
    * @param libelle Libellé. 
    */
   public void setLibelleBudget(String libelle) {
      this.libelleBudget = libelle;
   }
   
   /**
    * Renvoyer le libellé du compte, dans le plan comptable.
    * @return Libellé du compte.
    */
   public String getLibelleCompte() {
      return this.libelleCompte;
   }

   /**
    * Fixer le libellé du compte.
    * @param libelleCompte Libellé du compte.
    */
   public void setLibelleCompte(String libelleCompte) {
      this.libelleCompte = libelleCompte;
   }

   /**
    * Renvoyer le numéro de compte.
    * @return Numéro de compte.
    */
   public NumeroCompte asNumeroCompte() {
      return new NumeroCompte(this.numeroCompte);
   }
   
   /**
    * Renvoyer le numéro de compte.
    * @return Numéro de compte.
    */
   public String getNumeroCompte() {
      return this.numeroCompte;
   }
   
   /**
    * Fixer le numéro de compte.
    * @param numeroCompte Numéro de compte.
    */
   public void setNumeroCompte(String numeroCompte) {
      this.numeroCompte = numeroCompte;
   }

   /**
    * Renvoyer le montant du crébit. 
    * @return Crébit.
    */
   public double getSoldeCrediteur() {
      return this.soldeCrediteur;
   }

   /**
    * Fixer le montant du crédit. 
    * @param credit Crédit.
    */
   public void setSoldeCrediteur(double credit) {
      this.soldeCrediteur = credit;
   }

   /**
    * Renvoyer le montant du débit. 
    * @return Débit.
    */
   public double getSoldeDebiteur() {
      return this.soldeDebiteur;
   }

   /**
    * Fixer le montant du débit. 
    * @param debit Débit.
    */
   public void setSoldeDebiteur(double debit) {
      this.soldeDebiteur = debit;
   }

   /**
    * Renvoyer le code SIRET de l'établissement de la commune ou de l'intercommunalité détentrice de ce compte.
    * @return SIREN, peut valoir null.
    */
   public SIRET asSiret() {
      return new SIRET(this.siret);
   }

   /**
    * Renvoyer le code SIRET de l'établissement de la commune ou de l'intercommunalité détentrice de ce compte.
    * @return SIREN, peut valoir null.
    */
   public String getSiret() {
      return this.siret;
   }

   /**
    * Fixer le code SIRET de l'établissement de la commune ou de l'intercommunalité détentrice de ce compte.
    * @param siret SIRET, peut valoir null.
    */
   public void setSiret(String siret) {
      this.siret = siret;
   }
   
   /**
    * @see ObjetMetierIdentifiable#toString()
    */
   @Override
   public String toString() {
      try {
         String format = "'{'Numéro de compte : {0}, code commune : {1}, siret : {2}, année : {3},  budget principal : {4}, " +
            "libellé du budget : {5}, solde créditeur : {6,number,#0.00}, solde débiteur : {7,number,#0.00}'}'";

         return MessageFormat.format(format, this.numeroCompte, this.codeCommune, this.siret, this.anneeExercice, this.budgetPrincipal,
            this.libelleBudget, this.soldeCrediteur, this.soldeDebiteur);
      }
      catch(RuntimeException e) {
         return Compte.class.getName() + " - toString - " + e.getMessage();
      }
   }
}
