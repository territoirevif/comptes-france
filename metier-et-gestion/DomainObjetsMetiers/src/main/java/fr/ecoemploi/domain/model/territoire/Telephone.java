package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.ObjetMetier;

/**
 * Un numéro de téléphone.
 * @author Marc LE BIHAN.
 */
public class Telephone extends ObjetMetier {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 4311735573086415683L;

   /** Commentaire associé à ce numéro de téléphone. */
   private String commentaire;

   /** Numéro de téléphone. */
   private String numeroTelephone;

   /**
    * Renvoyer le commentaire associé à ce numéro de téléphone.
    * @return commentaire, peut valoir null.
    */
   public String getCommentaire() {
      return this.commentaire;
   }

   /**
    * Renvoyer le numéro de téléphone.
    * @return Numéro de téléphone.
    */
   public String getNumeroTelephone() {
      return this.numeroTelephone;
   }

   /**
    * Fixer le commentaire associé.
    * @param texte Commentaire, peut valoir null.
    */
   public void setCommentaire(String texte) {
      this.commentaire = texte;
   }

   /**
    * Fixer le numéro de téléphone.
    * @param numero Numéro de téléphone.
    */
   public void setPhoneNumber(String numero) {
      this.numeroTelephone = numero;
   }
}
