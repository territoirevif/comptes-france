package fr.ecoemploi.domain.model.territoire.entreprise;

import java.util.*;

/**
 * Tranche d'effectif d'une entreprise.
 * @author Marc LE BIHAN
 */
public enum TrancheEffectif {
   /** Unités non employeuses (pas de salarié au cours de l'année de référence et pas d'effectif au 31/12) ou unités sans mise à jour d'effectif. */
   NON_EMPLOYEUSE("NN", 0, 0),
   
   /** 0 salarié (unités ayant eu des salariés au cours de l'année de référence mais plus d'effectif au 31/12). */
   SANS_SALARIE("00", 0, 0),

   /** 1 ou 2 salariés */
   SALARIE_1_2("01", 1, 2),
   
   /** 3 à 5 salariés */   
   SALARIE_3_5("02", 3, 5),
   
   /** 6 à 9 salariés */
   SALARIE_6_9("03", 6, 9),
   
   /** 10 à 19 salariés */
   SALARIE_10_19("11", 10, 19),
   
   /** 20 à 49 salariés */
   SALARIE_20_49("12", 20, 49),
   
   /** 50 à 99 salariés */
   SALARIE_50_99("21", 50, 99),
   
   /** 100 à 199 salariés */
   SALARIE_100_199("22", 100, 199),
   
   /** 200 à 249 salariés */
   SALARIE_200_249("31", 200, 249),
   
   /** 250 à 499 salariés */
   SALARIE_250_499("32", 250, 499),
   
   /** 500 à 999 salariés */
   SALARIE_500_999("41", 500, 999),

   /** 1000 à 1999 salariés */
   SALARIE_1000_1999("42", 1000, 1999),

   /** 2000 à 4999 salariés */
   SALARIE_2000_4999("51", 2000, 4999),

   /** 5000 à 9999 salariés */
   SALARIE_5000_9999("52", 5000, 9999),

   /** 10000 salariés et plus */
   SALARIE_10000_ET_PLUS("53", 10000, Integer.MAX_VALUE);

   /** Correspondance rapide code et enum. */
   private static final Map<String, TrancheEffectif> MAP = new HashMap<>();
   
   static {
      Arrays.stream(TrancheEffectif.values()).forEach(v -> MAP.put(v.getCode(), v));
   }
   
   /** Code de la tranche d'effectif. */
   private final String code;
   
   /** Borne inférieure de la classe, inclue. */
   private final int borneInferieure;
   
   /** Borne supérieure de la classe, inclue. */
   private final int borneSuperieure;
   
   /**
    * Construire une tranche d'effectif.
    * @param code Code.
    * @param borneInferieure Borne inférieure.
    * @param borneSuperieure Borne supérieure.
    */
   TrancheEffectif(String code, int borneInferieure, int borneSuperieure) {
      this.code = code;
      this.borneInferieure = borneInferieure;
      this.borneSuperieure = borneSuperieure;
   }

   /**
    * Renvoyer le code de cette tranche.
    * @return Code.
    */
   public String getCode() {
      return this.code;
   }

   /**
    * Renvoyer la borne inférieure de cette tranche, inclue.
    * @return Borne inférieure.
    */
   public int getBorneInferieure() {
      return this.borneInferieure;
   }

   /**
    * Renvoyer la borne supérieure de cette tranche, exclue.
    * @return Borne supérieure.
    */
   public int getBorneSuperieure() {
      return this.borneSuperieure;
   }
   
   /**
    * Renvoyer la moyenne de la tranche.
    * @param compterCommeActifs true s'il faut compter un établissement non employeur ou à zéro salarié comme ayant un actif (son dirigeant)<br>
    * false s'il faut rapporter strictement le nombre de salariés, et alors il n'y en a aucun.
    * @return Moyenne.
    */
   public double getMoyenne(boolean compterCommeActifs) {
      // Si l'on compte les actifs, on ajoute le dirigeant : un actif.
      if (this.borneInferieure == this.borneSuperieure) {
         return compterCommeActifs ? this.borneInferieure + 1 : this.borneInferieure;
      }
      
      // FIXME Effectif estimé arbitraire pour plus de 10 000 salariés sans borne de fin.
      if (this.borneSuperieure == Integer.MAX_VALUE) {
         return 15000.0;
      }
      
      double somme = (double)this.borneInferieure + (double)this.borneSuperieure;
      return compterCommeActifs ? (somme / 2.0) + 1 : (somme / 2.0);
   }
   
   /**
    * Renvoyer une tranche d'effectif d'après un code.
    * @param code Code.
    * @return Tranche d'effectif.
    */
   public static TrancheEffectif valueFromCode(String code) {
      if (code == null || code.isBlank()) {
         return null;
      }

      TrancheEffectif valeur = MAP.get(code);
      
      if (valeur == null) {
         throw new NoSuchElementException("TrancheEffectif : '" + code + "' invalide.");
      }
      
      return valeur;
   }
}
