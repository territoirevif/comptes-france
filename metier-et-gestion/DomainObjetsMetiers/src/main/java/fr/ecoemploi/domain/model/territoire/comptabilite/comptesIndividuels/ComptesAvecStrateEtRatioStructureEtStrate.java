package fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels;

import java.io.Serial;
import java.util.*;

/**
 * Comptes avec montant, montant par habitant, strate, ratio et ratio moyenne strate.
 * @author Marc Le Bihan
 */
public class ComptesAvecStrateEtRatioStructureEtStrate extends ComptesAvecRatioStructure {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 8740762465642324263L;
   
   /** Moyenne de strate */
   private Double moyenneStrate;

   /** Ratio de la strate */
   private Double ratioStrate;

   /**
    * Construire de comptes montant (total ou base), par habitant + moyenne strate par habitant.
    */
   public ComptesAvecStrateEtRatioStructureEtStrate() {
   }

   /**
    * Construire de comptes montant (total ou base), par habitant + moyenne strate par habitant.
    * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
    */
   public ComptesAvecStrateEtRatioStructureEtStrate(String libelleAnalyse) {
      super(libelleAnalyse);
   }

  /**
   * Construire de comptes montant (total ou base), par habitant + moyenne strate par habitant.
   * @param libelleAnalyse Libellé de l'analyse apportée par cette ligne de comptes individuels.
   * @param montant Montant (total ou base).
   * @param parHabitant Montant par habitant.
   * @param moyenneStrate Moyenne de la strate par habitant.
   * @param ratioStructure Ratio de la structure.
   * @param ratioStrate Ratio de la strate.
   */
   public ComptesAvecStrateEtRatioStructureEtStrate(String libelleAnalyse, Double montant, Double parHabitant, Double moyenneStrate, Double ratioStructure, Double ratioStrate) {
      super(libelleAnalyse, montant, parHabitant, ratioStructure);
      this.moyenneStrate = moyenneStrate; 
      this.ratioStrate = ratioStrate; 
   }

   /**
    * Renvoyer la moyenne de strate.
    * @return Moyenne de strate.
    */
   public Double getMoyenneStrate() {
      return this.moyenneStrate;
   }

   /**
    * Fixer la moyenne de strate.
    * @param moyenneStrate Moyenne de strate.
    */
   public void setMoyenneStrate(Double moyenneStrate) {
      this.moyenneStrate = moyenneStrate;
   }

   /**
    * Renvoyer le ratio de la strate.
    * @return Ratio de la strate.
    */
   public Double getRatioStrate() {
      return this.ratioStrate;
   }

   /**
    * Fixer le ratio de la strate.
    * @param ratioStrate Ratio de la strate 
    */
   public void setRatioStrate(Double ratioStrate) {
      this.ratioStrate = ratioStrate;
   }
   
   /**
    * Formatter une ligne texte pour qu'elle se place dans un tableau textuel d'analyse des équilibres financiers fondamentaux.
    * @return Ligne formatée. 
    */
   @Override
   public String toStringAnalyseEquilibresFinanciersFondamentaux() {
      final String FMT_NOMBRE_ENTIER = "%10d\t";
      StringBuilder format = new StringBuilder();
      List<Object> valeurs = new ArrayList<>();

      format.append(getMontant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getMontant() != null ? Math.round(getMontant()) : "");

      format.append(getParHabitant() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getParHabitant() != null ? Math.round(getParHabitant()) : "");

      format.append(getMoyenneStrate() != null ? FMT_NOMBRE_ENTIER : "%s\t");
      valeurs.add(getMoyenneStrate() != null ? Math.round(getMoyenneStrate()) : "");

      format.append("%s\t");
      valeurs.add(getLibelleAnalyse());

      format.append(getRatioStructure() != null ? "%10.2f\t" : "%s\t");
      valeurs.add(getRatioStructure() != null ? getRatioStructure() : "");

      format.append(getRatioStrate() != null ? "%10.2f" : "%s");
      valeurs.add(getRatioStrate() != null ? getRatioStrate() : "");

      return String.format(format.toString(), valeurs.toArray());
   }
}
