package fr.ecoemploi.domain.model.territoire.entreprise.ape;

import java.io.Serial;
import java.text.MessageFormat;
import java.util.*;

import org.apache.commons.lang3.builder.*;

import fr.ecoemploi.domain.utils.objets.ObjetMetier;

/**
 * Classe de base des niveaux NAF. 
 * @author Marc LE BIHAN.
 */
public class NiveauNAF extends ObjetMetier {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 6053757571078668929L;

   /** Code NAF. */
   private String codeNAF;
   
   /** Libellé. */
   private String libelle;
   
   /** Code NAF parent. */
   private NiveauNAF parent;
   
   /** Rang de ce niveau NAF. */
   private int niveau;

   /** Sous-niveaux NAF. */
   private LinkedHashMap<String, NiveauNAF> sousNiveauxNAF = new LinkedHashMap<>();
   
   /**
    * Construire une entrée de niveau NAF.
    */
   public NiveauNAF() {
   }

   /**
    * Construire une entrée de niveau NAF.
    * @param codeNAF Code NAF.
    * @param libelle Libellé.
    * @param parent Code NAF parent.
    * @param niveau Rang de ce niveau NAF.
    * @param sousNiveauxNAF Sous-niveaux NAF.
    */
   public NiveauNAF(String codeNAF, String libelle, NiveauNAF parent, int niveau, LinkedHashMap<String, NiveauNAF> sousNiveauxNAF) {
      this.codeNAF = codeNAF;
      this.libelle = libelle;
      this.parent = parent;
      this.niveau = niveau;
      this.sousNiveauxNAF = sousNiveauxNAF;
   }
   
   /**
    * Renvoyer le code NAF.
    * @return Code NAF.
    */
   public String getCodeNAF() {
      return this.codeNAF;
   }

   /**
    * Fixer le code NAF.
    * @param codeNAF Code NAF.
    */
   public void setCodeNAF(String codeNAF) {
      this.codeNAF = codeNAF;
   }

   /**
    * Renvoyer le libellé du code NAF.
    * @return Libellé du code NAF.
    */
   public String getLibelle() {
      return this.libelle;
   }

   /**
    * Fixer le libellé du code NAF.
    * @param libelle Libellé du code NAF.
    */
   public void setLibelle(String libelle) {
      this.libelle = libelle;
   }

   /**
    * Renvoyer le niveau NAF.
    * @return Niveau NAF.
    */
   public int getNiveau() {
      return this.niveau;
   }

   /**
    * Fixer le niveau NAF.
    * @param niveau Niveau NAF. 
    */
   public void setNiveau(int niveau) {
      this.niveau = niveau;
   }

   /**
    * Renvoyer le NAF parent de cet élément.
    * @return NAF parent.
    */
   public NiveauNAF getParent() {
      return this.parent;
   }

   /**
    * Fixer le NAF parent de cet élément.
    * @param parent NAF parent. 
    */
   public void setParent(NiveauNAF parent) {
      this.parent = parent;
   }

   /**
    * Renvoyer les sous-niveaux NAF de ce niveau.
    * @return Sous-niveaux NAF.
    */
   public LinkedHashMap<String, NiveauNAF> getSousNiveauxNAF() {
      return this.sousNiveauxNAF;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean equals(Object o) {
      if (o instanceof NiveauNAF == false) {
         return false;
      }
      
      NiveauNAF candidat = (NiveauNAF)o;
      
      EqualsBuilder equals = new EqualsBuilder();
      equals.append(this.codeNAF, candidat.getCodeNAF());
      equals.append(this.niveau, candidat.getNiveau());
      return equals.isEquals();
   }
   
   /**
    * {@inheritDoc}
    */
   @Override
   public int hashCode() {
      HashCodeBuilder hash = new HashCodeBuilder();
      hash.append(this.codeNAF);
      hash.append(this.niveau);
      return hash.toHashCode();
   }
   
   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      // #0 : Code NAF
      // #1 : Libellé
      // #2 : Niveau
      // #3 : Parent
      // #4 : Nombre de valeurs du sous-niveau.
      String format = "code : {0}, libellé : {1}, niveau : {2}, parent : {3}, nombre de valeurs du sous-niveau : {4}";
      return MessageFormat.format(format, this.codeNAF, this.libelle, this.niveau, this.parent, this.sousNiveauxNAF != null ? this.sousNiveauxNAF.size() : null);
   }
}
