package fr.ecoemploi.domain.model.territoire;

import java.util.*;

import org.apache.commons.lang3.*;

/**
 * Type de voie.
 * @author Marc LE BIHAN
 */
public enum TypeDeVoie {
   /** Aire */
   AIRE("AIRE", "Aire"),

   /** Allée */
   ALLEE("ALL", "Allée"),
   
   /** Avenue */
   AVENUE("AV", "Avenue"),

   /** Base */
   BASE("BASE", "Base"),

   /** Boulevard */
   BOULEVARD("BD", "Boulevard"),

   /** Cami */
   CAMI("CAMI", "Cami"),

   /** Carrefour */
   CARREFOUR("CAR", "Carrefour"),
   
   /** Chemin */
   CHEMIN("CHE", "Chemin"),

   /** Cheminement */
   CHEMINENEMT("CHEM", "Cheminement"),

   /** Chaussée */
   CHAUSSEE("CHS", "Chaussée"),
   
   /** Cité */
   CITE("CITE", "Cité"),

   /** Corniche */
   CORNICHE("COR", "Corniche"),

   /** Corniche */
   CLOS("CLOS", "Clos"),

   /** Corniche */
   COIN("COIN", "Coin"),

   /** Cote */
   COTE("COTE", "Cote"),

   /** Cour */
   COUR("COUR", "Cour"),

   /** Cours */
   COURS("CRS", "Cours"),

   /** Domaine */
   DOMAINE("DOM", "Domaine"),
   
   /** Descente */
   DESCENTE("DSC", "Descente"),
   
   /** Ecart */
   ECART("ECA", "Ecart"),
   
   /** Esplanade */
   ESPLANADE("ESP", "Esplanade"),
   
   /** Faubourg */
   FAUBOURG("FG", "Faubourg"),

   /** Grande Rue */
   GRANDE_RUE("GR", "Grande Rue"),

   /** Gare */
   GARE("GARE", "Gare"),
   
   /** Hameau */
   HAMEAU("HAM", "Hameau"),
   
   /** Halle */
   HALLE("HLE", "Halle"),

   /** Impasse */
   IMPASSE("IMP", "Impasse"),

   /** Ilôt */
   ILOT("ILOT", "Ilôt"),
   
   /** Lieu-dit */
   LIEU_DIT("LD", "Lieu-dit"),
   
   /** Lotissement */
   LOTISSEMENT("LOT", "Lotissement"),
   
   /** Marché */
   MARCHE("MAR", "Marché"),
   
   /** Montée */
   MONTEE("MTE", "Montée"),

   /** Parc */
   PARC("PARC", "Parc"),

   /** Passage */
   PASSAGE("PAS", "Passage"),
   
   /** Place */
   PLACE("PL", "Place"),
   
   /** Plaine */
   PLAINE("PLN", "Plaine"),

   /** Plan */
   PLAN("PLAN", "Plan"),

   /** Plateau */
   PLATEAU("PLT", "Plateau"),

   /** Promenade */
   PROMENADE("PRO", "Promenade"),

   /** Pont */
   PONT("PONT", "Pont"),

   /** Port */
   PORT("PORT", "Port"),
   
   /** Parvis */
   PARVIS("PRV", "Parvis"),
   
   /** Quartier */
   QUARTIER("QUA", "Quartier"),
   
   /** Quai */
   QUAI("QUAI", "Quai"),
   
   /** Résidence */
   RESIDENCE("RES", "Résidence"),
   
   /** Ruelle */
   RUELLE("RLE", "Ruelle"),
   
   /** Rocade */
   ROCADE("ROC", "Rocade"),
   
   /** Rond-point */
   ROND_POINT("RPT", "Rond-point"),
   
   /** Route */
   ROUTE("RTE", "Route"),

   /** Rue */
   RUE("RUE", "Rue"),
   
   /** Sente - Sentier */
   SENTE_SENTIER("SEN", "Sente - Sentier"),
   
   /** Square */
   SQUARE("SQ", "Square"),
   
   /** Terre-plein */
   TERRE_PLEIN("TPL", "Terre-plein"),

   /** Tour */
   TOUR("TOUR", "Tour"),

   /** Traverse */
   TRAVERSE("TRA", "Traverse"),

   /** Villa */
   VILLA("VLA", "Villa"),
   
   /** Village */
   VILLAGE("VLGE", "Village"),

   /** Voie */
   VOIE("VOIE", "Voie"),

   /** Zone artisanale */
   ZA("ZA", "Zone artisanale"),

   /** Zone d'aménagement concerté */
   ZAC("ZAC", "Zone d'aménagement concerté"),

   /** Zone d'aménagement différé */
   ZAD("ZAD", "Zone d'aménagement différé"),

   /** Zone industrielle */
   ZI("ZI", "Zone industrielle"),

   /** Zone */
   ZONE("ZONE", "Zone");

   /** Non diffusable */
   //NON_DIFFUSABLE("[ND]", "Non diffusable");

   /** Abréviation. */
   private String abreviation;
   
   /** Type de voie complet. */
   private String complet;
   
   /** Correspondance rapide code et enum. */
   private static final Map<String, TypeDeVoie> MAP = new HashMap<>();
   
   static {
      Arrays.stream(TypeDeVoie.values()).forEach(v -> MAP.put(v.getAbreviation(), v));
   }

   /**
    * Construire un type de voie.
    * @param abreviation Abréviation.
    * @param complet Type de voie complet. 
    */
   TypeDeVoie(String abreviation, String complet) {
      this.setAbreviation(abreviation);
      this.setComplet(complet);
   }

   /**
    * Renvoyer l'abréviation du type de voie.
    * @return Abréviation du type de voie.
    */
   public String getAbreviation() {
      return this.abreviation;
   }

   /**
    * Fixer l'abréviation du type de voie.
    * @param abreviation Abréviation du type de voie. 
    */
   void setAbreviation(String abreviation) {
      this.abreviation = abreviation;
   }

   /**
    * Renvoyer le type de voie complet.
    * @return Type de voie complet.
    */
   public String getComplet() {
      return this.complet;
   }

   /**
    * Fixer le type de voie complet.
    * @param complet Type de voie complet.
    */
   void setComplet(String complet) {
      this.complet = complet;
   }
   
   /**
    * Déterminer si une voie est de ce type.
    * @param voie Voie.
    * @return true si elle a ce type de voie.
    */
   public boolean hasTypeVoie(String voie) {
      if (voie == null) {
         return false;
      }
      
      String candidatCourt = voie.substring(0, this.abreviation.length());
      String candidatLong = voie.substring(0, this.complet.length());
      
      return candidatCourt.equalsIgnoreCase(this.abreviation) || candidatLong.equalsIgnoreCase(this.complet);
   }
   
   /**
    * Renvoyer un type de voie d'après son abréviation.
    * @param abreviation Abréviation.
    * @return Type de voie.
    */
   public static TypeDeVoie valueFromAbreviation(String abreviation) {
      if (StringUtils.isBlank(abreviation)) {
         return null;
      }
      
      TypeDeVoie valeur = MAP.get(abreviation);
      
      if (valeur == null) {
         throw new NoSuchElementException("TypeDeVoie : '" + abreviation + "' invalide.");
      }
      
      return valeur;
   }

   /**
    * @see java.lang.Enum#toString()
    */
   @Override
   public String toString() {
      return this.complet;
   }
}
