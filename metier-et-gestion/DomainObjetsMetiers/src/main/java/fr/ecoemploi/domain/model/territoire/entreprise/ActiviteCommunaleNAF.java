package fr.ecoemploi.domain.model.territoire.entreprise;

import java.io.Serial;
import java.text.*;

import fr.ecoemploi.domain.utils.objets.ObjetMetier;

/**
 * Activité communale NAF.
 * @author Marc LE BIHAN
 */
public class ActiviteCommunaleNAF extends ObjetMetier {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 1519063612190300952L;
   
   /** Code de la section NAF */
   private String codeNAF;
   
   /** nbr d'entreprises ayant ce code NAF. */
   private Integer nombreEntreprises = 0;
   
   /** pct d'entreprises ayant ce code NAF dans la commune. */
   private Double pourcentageEntreprises = 0.0;
   
   /** nbr de salariés appartenant à des entreprises ayant ce code NAF. */
   private Integer nombreSalaries = 0;
   
   /** pct de salariés appartenant à des entreprises ayant ce code NAF dans la commune */
   private Double pourcentageSalaries = 0.0;

   /**
    * Construire une activité communale.
    */
   public ActiviteCommunaleNAF() {
   }
   
   /**
    * Construire une activité communale.
    * @param codeNAF Code d'activité NAF.
    */
   public ActiviteCommunaleNAF(String codeNAF) {
      this.codeNAF = codeNAF;
   }
   
   /**
    * Renvoyer le code NAF.
    * @return Code NAF.
    */
   public String getCodeNAF() {
      return this.codeNAF;
   }

   /**
    * Fixer le code NAF.
    * @param codeNAF Code NAF.
    */
   public void setCodeNAF(String codeNAF) {
      this.codeNAF = codeNAF;
   }

   /**
    * Renvoyer le nombre d'entreprises qui ont ce code NAF.
    * @return Nombre d'entreprises.
    */
   public Integer getNombreEntreprises() {
      return this.nombreEntreprises;
   }

   /**
    * Fixer le nombre d'entreprises qui ont ce code NAF.
    * @param nombreEntreprises Nombre d'entreprises.
    */
   public void setNombreEntreprises(Integer nombreEntreprises) {
      this.nombreEntreprises = nombreEntreprises;
   }

   /**
    * Renvoyer le pourcentage d'entreprises qui ont ce code NAF.
    * @return Pourcentage d'entreprises.
    */
   public Double getPourcentageEntreprises() {
      return this.pourcentageEntreprises;
   }

   /**
    * Fixer le pourcentage d'entreprises qui ont ce code NAF.
    * @param pourcentageEntreprises Pourcentage d'entreprises.
    */
   public void setPourcentageEntreprises(Double pourcentageEntreprises) {
      this.pourcentageEntreprises = pourcentageEntreprises;
   }

   /**
    * Renvoyer le nombre de salariés qui ont ce code NAF.
    * @return Nombre de salariés.
    */
   public Integer getNombreSalaries() {
      return this.nombreSalaries;
   }

   /**
    * Fixer le nombre de salariés qui ont ce code NAF.
    * @param nombreSalaries Nombre de salariés.
    */
   public void setNombreSalaries(Integer nombreSalaries) {
      this.nombreSalaries = nombreSalaries;
   }

   /**
    * Renvoyer le pourcentage de salariés qui ont ce code NAF.
    * @return Pourcentage de salariés.
    */
   public Double getPourcentageSalaries() {
      return this.pourcentageSalaries;
   }

   /**
    * Fixer le pourcentage de salariés qui ont ce code NAF.
    * @param pourcentageSalaries Pourcentage de salariés.
    */
   public void setPourcentageSalaries(Double pourcentageSalaries) {
      this.pourcentageSalaries = pourcentageSalaries;
   }
   
   /**
    * @see ObjetMetier#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object o) {
      if (o instanceof ActiviteCommunaleNAF == false) {
         return false;
      }
      
      if (o == this) {
         return true;
      }
      
      ActiviteCommunaleNAF a = (ActiviteCommunaleNAF)o;
      return this.codeNAF.equals(a.getCodeNAF());
   }
   
   /**
    * @see ObjetMetier#hashCode()
    */
   @Override
   public int hashCode() {
      return this.codeNAF == null ? 805598524 : this.codeNAF.hashCode();
   }
   
   /**
    * @see ObjetMetier#toString()
    */
   @Override
   public String toString() {
      String format = "'{'"
         + "NAF : {0}, nombre d''entreprises : {1} ({2}%), nombre de salariés : {3} ({4}%)" 
         + "'}'";
      
      return MessageFormat.format(format, this.codeNAF, this.nombreEntreprises, this.pourcentageEntreprises, this.nombreSalaries, this.pourcentageSalaries);
   }
}
