package fr.ecoemploi.domain.model.catalogue;

import fr.ecoemploi.domain.utils.autocontrole.Id;

import java.io.Serial;

/**
 * Identifiant d'une organisation (émetrice de données open data, par exemple).
 */
public class OrganisationId extends Id {
   @Serial
   private static final long serialVersionUID = -5355861463589696483L;

   /** INSEE */
   public static final String INSEE = "534fff81a3a7292c64a77e5c";

   /**
    * Construire un identifiant.
    */
   public OrganisationId() {
   }

   /**
    * Construire un identifiant.
    * @param id Identifiant.
    */
   public OrganisationId(String id) {
      super(id);
   }

   /**
    * Construire un identifiant par copie.
    * @param id Identifiant.
    */
   public OrganisationId(OrganisationId id) {
      super(id);
   }
}
