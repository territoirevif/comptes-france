package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;
import java.text.MessageFormat;

import fr.ecoemploi.domain.utils.objets.ObjetMetierIdentifiable;

/**
 * Un departement (Division territoriale française).
 * @author Marc LE BIHAN.
 */
public class Departement extends ObjetMetierIdentifiable<CodeDepartement> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 1922673079550155427L;

   /** Nom du département. */
   private String nom;

   /** Nom en majuscule. */
   private String nomMajuscules;

   /** Code INSEE de la région auquel appartient ce département. */
   private CodeRegion codeRegion;

   /** Code de la commune de chef lieu. */
   private CodeCommune communeChefLieu;

   /** Type et nom charnière. */
   private ArticleEtCharniereINSEE typeNomEtCharniere;

   /**
    * Construire un département.
    */
   public Departement() {
   }

   /**
    * Construire un département par copie.
    * @param departement Département.
    */
   public Departement(Departement departement) {
      super(departement);
      this.setCodeRegion(departement.codeRegion != null ? new CodeRegion(departement.codeRegion) : null);
      this.setCodeCommuneChefLieu(departement.communeChefLieu != null ? new CodeCommune(departement.communeChefLieu) : null);
      this.nom = departement.nom;
      this.nomMajuscules = departement.nomMajuscules;
      this.typeNomEtCharniere = departement.typeNomEtCharniere;
   }

   /**
    * Renvoyer le code de la commune de chef lieu.
    * @return Code de la commune.
    */
   public CodeCommune getCodeCommuneChefLieu() {
      return this.communeChefLieu;
   }

   /**
    * Renvoyer le code du département.
    * @return Code du dépatement.
    */
   public CodeDepartement getCodeDepartement() {
      return getId();
   }

   /**
    * Renvoyer le code de la région dans laquelle se situe ce département.
    * @return Code de la région.
    */
   public CodeRegion getCodeRegion() {
      return this.codeRegion;
   }

   /**
    * Renvoyer le nom du département. 
    * @return Nom du département.
    */
   public String getNom() {
      return this.nom;
   }

   /**
    * Renvoyer le nom du département en majuscules.
    * @return Nom du département.
    */
   public String getNomMajuscules() {
      return this.nomMajuscules;
   }

   /**
    * Renvoyer l'article et la charnière utilisés pour désigner le département.
    * @return Type de nom et charnière.
    */
   public ArticleEtCharniereINSEE getTypeNomEtCharniere() {
      return this.typeNomEtCharniere;
   }

   /**
    * Fixer le code de la commune de chef lieu.
    * @param code Code de la commune de chef lieu.
    */
   public void setCodeCommuneChefLieu(CodeCommune code) {
      this.communeChefLieu = code;
   }

   /**
    * Fixer le code du département.
    * @param code Code du département.
    */
   public void setCodeDepartement(CodeDepartement code) {
      setId(code);
   }

   /**
    * Fixer le code de la région dans laquelle se trouve cette région.
    * @param code Code de la région.
    */
   public void setCodeRegion(CodeRegion code) {
      this.codeRegion = code;
   }

   /**
    * Fixer le nom du département.
    * @param nomDepartement Nom du département.
    */
   public void setNom(String nomDepartement) {
      this.nom = nomDepartement;
   }

   /**
    * Fixer le nom du département en majuscules.
    * @param nomDepartement Nom du département en majuscules.
    */
   public void setNomMajuscules(String nomDepartement) {
      this.nomMajuscules = nomDepartement;
   }

   /**
    * Fixer le type de charnière à utiliser pour nommer le département.
    * @param type Type de charnière.
    */
   public void setTypeNomEtCharniere(ArticleEtCharniereINSEE type) {
      this.typeNomEtCharniere = type;
   }

   /**
    * @see ObjetMetierIdentifiable#toString()
    */
   @Override
   public String toString() {
      String format = "'{'{0}, Nom : {1}, Nom en majuscules : {2}, Code département : {3}, Code région : {4}, " +
         "Commune chef lieu : {5}, Article et charnière : {6}'}'";

      return MessageFormat.format(format, super.toString(), this.nom, this.nomMajuscules, getId(), this.codeRegion,
         this.communeChefLieu, this.typeNomEtCharniere);
   }
}
