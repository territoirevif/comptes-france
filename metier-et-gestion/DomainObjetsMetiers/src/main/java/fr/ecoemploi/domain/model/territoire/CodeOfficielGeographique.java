package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;
import java.text.MessageFormat;
import java.util.*;

import fr.ecoemploi.domain.utils.objets.ObjetMetier;

/**
 * Code officiel géographique (COG) d'une année.
 * @author Marc LE BIHAN
 */
public class CodeOfficielGeographique extends ObjetMetier {
   /** Code officiel géographique  */
   @Serial
   private static final long serialVersionUID = -3711306634544727416L;

   /** Année représentée. */
   private int annee;

   /** Régions. */
   private Regions regions;
   
   /** Départements. */
   private Departements departements;
   
   /** Communes. */
   private Communes communes;
   
   /** Intercommunalités. */
   private Intercommunalites intercommunalites;

   /**
    * Renvoyer l'année de ce code officiel géographique.
    * @return Année.
    */
   public int getAnnee() {
      return this.annee;
   }

   /**
    * Fixer l'année du ce code officiel géographique.
    * @param annee Année.
    */
   public void setAnnee(int annee) {
      this.annee = annee;
   }
   
   /**
    * Renvoyer les codes communes membres d'une intercommunalité.
    * @param codeIntercommunalite Code SIREN / EPCI de l'intercommunalité.
    * @return liste des codes communes membres de cet intercommunalité.
    * @throws IntercommunaliteAbsenteDansCommuneSiegeException si cette intercommunalité n'existe pas.
    */
   public Set<String> getCodesCommunesMembres(SIRENCommune codeIntercommunalite) throws IntercommunaliteAbsenteDansCommuneSiegeException {
      Objects.requireNonNull(codeIntercommunalite, "Le code EPCI / SIREN de l'intercommunalité ne peut pas valoir null.");
      return this.intercommunalites.getIntercommunalite(codeIntercommunalite).getCodesCommunesMembres();      
   }
   
   /**
    * Renvoyer les codes communes membres d'une intercommunalité.
    * @param codeIntercommunalite Code SIREN / EPCI de l'intercommunalité.
    * @return liste des codes communes membres de cet intercommunalité.
    * @throws IntercommunaliteAbsenteDansCommuneSiegeException si cette intercommunalité n'existe pas.
    */
   public Communes getCommunesMembres(SIRENCommune codeIntercommunalite) throws IntercommunaliteAbsenteDansCommuneSiegeException {
      Objects.requireNonNull(codeIntercommunalite, "Le code EPCI / SIREN de l'intercommunalité ne peut pas valoir null.");
      
      Communes communesMembres = new Communes();
      this.intercommunalites.getIntercommunalite(codeIntercommunalite).getCodesCommunesMembres().forEach(codeCommune -> communesMembres.put(codeCommune, this.communes.get(codeCommune)));
      return communesMembres;
   }
 
   /**
    * Renvoyer les régions de ce COG.
    * @return Régions.
    */
   public Regions getRegions() {
      return this.regions;
   }

   /**
    * Fixer les régions de ce COG.
    * @param regions Régions.
    */
   public void setRegions(Regions regions) {
      this.regions = regions;
   }

   /**
    * Renvoyer les départements.
    * @return Départements.
    */
   public Departements getDepartements() {
      return this.departements;
   }

   /**
    * Fixer les départements.
    * @param departements Départements.
    */
   public void setDepartements(Departements departements) {
      this.departements = departements;
   }

   /**
    * Renvoyer les communes de ce COG.
    * @return communes.
    */
   public Communes getCommunes() {
      return this.communes;
   }

   /**
    * Renvoyer les communes sous forme de Map.
    * @return Communes.
    */
   public Map<String, Commune> getCommunesAsMap() {
      return this.communes;
   }
   
   /**
    * Renvoyer une commune de ce COG.
    * @param code Code de la commune.
    * @return commune désirée.
    * @throws CommuneInexistanteException si la commune recherchée n'existe pas dans ce COG.
    */
   public Commune getCommune(CodeCommune code) throws CommuneInexistanteException {
      return getCommune(code.toString());
   }
   
   /**
    * Renvoyer une commune de ce COG.
    * @param code Code de la commune.
    * @return commune désirée.
    * @throws CommuneInexistanteException si la commune recherchée n'existe pas dans ce COG.
    */
   public Commune getCommune(String code) throws CommuneInexistanteException {
      Objects.requireNonNull(code, "Le code de la commune à rechercher dans le COG ne peut pas valoir null.");
      
      Commune commune = this.communes.get(code);

      // Si la commune n'existe pas, lever une exception.
      if (commune == null) {
         String message = MessageFormat.format("La commune de code ''{0}'' n''existe pas dans le Code Officiel Géographique de l''année {1,number,#0}.", code, this.annee);
         throw new CommuneInexistanteException(message);
      }
      
      return this.communes.get(code);
   }
   
   /**
    * Déterminer si une commune existe dans ce COG.
    * @param code Code de la commune.
    * @return true si cette commune y est présente.
    */
   public boolean hasCommune(CodeCommune code) {
      return hasCommune(code.toString());
   }
   
   /**
    * Déterminer si une commune existe dans ce COG.
    * @param code Code de la commune.
    * @return true si cette commune y est présente.
    */
   public boolean hasCommune(String code) {
      Objects.requireNonNull(code, "Le code de la commune dont on veut déterminer la présence dans le COG ne peut pas valoir null.");
      return this.communes.get(code) != null;
   }

   /**
    * Fixer les communes.
    * @param communes Communes.
    */
   public void setCommunes(Communes communes) {
      this.communes = communes;
   }

   /**
    * Renvoyer les intercommunalités.
    * @return Intercommunalités.
    */
   public Intercommunalites getIntercommunalites() {
      return this.intercommunalites;
   }

   /**
    * Fixer les intercommunalités.
    * @param intercommunalites Intercommunalités. 
    */
   public void setIntercommunalites(Intercommunalites intercommunalites) {
      this.intercommunalites = intercommunalites;
   }
}
