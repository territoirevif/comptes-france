package fr.ecoemploi.domain.model.territoire;

import org.apache.commons.lang3.*;

import java.io.Serial;
import java.util.*;

import fr.ecoemploi.domain.utils.autocontrole.*;
import fr.ecoemploi.domain.utils.objets.ObjetMetierIdentifiable;

/**
 * Un code de région.
 * @author Marc LE BIHAN
 */
public class CodeRegion extends Id {
   /** Code de région INSEE */
   @Serial
   private static final long serialVersionUID = 2395467574929691444L;

   /** Code région Auvergne-Rhône-Alpes */
   public static final CodeRegion AUVERGNE_RHONE_ALPES = new CodeRegion("84");

   /** Code région Bourgogne-Franche-Comté */
   public static final CodeRegion BOURGOGNE_FRANCHE_COMTE = new CodeRegion("27");

   /** Code région Bretagne */
   public static final CodeRegion BRETAGNE = new CodeRegion("53");

   /** Code région Centre-Val de Loire */
   public static final CodeRegion CENTRE_VAL_DE_LOIRE = new CodeRegion("24");

   /** Code région Corse */
   public static final CodeRegion CORSE = new CodeRegion("94");

   /** Code région Guadeloupe. */
   public static final CodeRegion GUADELOUPE = new CodeRegion("01");

   /** Code région Grand Est */
   public static final CodeRegion GRAND_EST = new CodeRegion("44");

   /** Code région Guyane. */
   public static final CodeRegion GUYANE = new CodeRegion("03");

   /** Code région Hauts-de-France */
   public static final CodeRegion HAUTS_DE_FRANCE = new CodeRegion("32");

   /** Code région Île-de-France */
   public static final CodeRegion ILE_DE_FRANCE = new CodeRegion("11");

   /** Code région La Réunion. */
   public static final CodeRegion LA_REUNION = new CodeRegion("04");

   /** Code région Martinique. */
   public static final CodeRegion MARTINIQUE = new CodeRegion("02");

   /** Code région Mayotte. */
   public static final CodeRegion MAYOTTE = new CodeRegion("06");

   /** Code région Normandie */
   public static final CodeRegion NORMANDIE = new CodeRegion("28");

   /** Code région Nouvelle-Aquitaine */
   public static final CodeRegion NOUVELLE_AQUITAINE = new CodeRegion("75");

   /** Code région Occitanie */
   public static final CodeRegion OCCITANIE = new CodeRegion("76");

   /** Code région Pays de la Loire */
   public static final CodeRegion PAYS_DE_LA_LOIRE = new CodeRegion("52");

   /** Code région Provence-Alpes-Côte d'Azur */
   public static final CodeRegion PROVENCE_ALPES_COTE_D_AZUR = new CodeRegion("93");

   /** Code région Saint-Barthélémy. */
   public static final CodeRegion SAINT_BARTHELEMY = new CodeRegion("07");
   
   /** Code région Saint-Martin */
   public static final CodeRegion SAINT_MARTIN = new CodeRegion("08");

   /**
    * Construire un code région null.
    */
   public CodeRegion() {
   }

   /**
    * Construire un code région alimenté.
    * @param code Code de la région.
    */
   public CodeRegion(String code) {
      super(code);
   }

   /**
    * Construire un code région par copie.
    * @param codeRegion Code région.
    */
   public CodeRegion(CodeRegion codeRegion) {
      super(codeRegion);
   }

   /**
    * Toutes les régions, y compris les spéciales : Saint-Barthélémy, Saint-Martin
    * @return toutes les régions
    */
   public static Iterable<CodeRegion> toutesRegions() {
      List<CodeRegion> codeRegions = new ArrayList<>();
      codeRegions.add(AUVERGNE_RHONE_ALPES);
      codeRegions.add(BOURGOGNE_FRANCHE_COMTE);
      codeRegions.add(BRETAGNE);
      codeRegions.add(CENTRE_VAL_DE_LOIRE);
      codeRegions.add(CORSE);

      codeRegions.add(GUADELOUPE);
      codeRegions.add(GRAND_EST);
      codeRegions.add(GUYANE);
      codeRegions.add(HAUTS_DE_FRANCE);
      codeRegions.add(ILE_DE_FRANCE);

      codeRegions.add(LA_REUNION);
      codeRegions.add(MARTINIQUE);
      codeRegions.add(MAYOTTE);
      codeRegions.add(NORMANDIE);
      codeRegions.add(NOUVELLE_AQUITAINE);

      codeRegions.add(OCCITANIE);
      codeRegions.add(PAYS_DE_LA_LOIRE);
      codeRegions.add(PROVENCE_ALPES_COTE_D_AZUR);
      codeRegions.add(SAINT_BARTHELEMY);
      codeRegions.add(SAINT_MARTIN);
      return codeRegions;
   }

   /**
    * Toutes les régions métropolitaines et d'outre-mer
    * @return Régions métropolitaines et d'outre-mer
    */
   public static Iterable<CodeRegion> regionsMetropolitainesEtOutremer() {
      List<CodeRegion> codeRegions = new ArrayList<>();
      codeRegions.add(AUVERGNE_RHONE_ALPES);
      codeRegions.add(BOURGOGNE_FRANCHE_COMTE);
      codeRegions.add(BRETAGNE);
      codeRegions.add(CENTRE_VAL_DE_LOIRE);
      codeRegions.add(CORSE);

      codeRegions.add(GUADELOUPE);
      codeRegions.add(GRAND_EST);
      codeRegions.add(GUYANE);
      codeRegions.add(HAUTS_DE_FRANCE);
      codeRegions.add(ILE_DE_FRANCE);

      codeRegions.add(LA_REUNION);
      codeRegions.add(MARTINIQUE);
      codeRegions.add(MAYOTTE);
      codeRegions.add(NORMANDIE);
      codeRegions.add(NOUVELLE_AQUITAINE);

      codeRegions.add(OCCITANIE);
      codeRegions.add(PAYS_DE_LA_LOIRE);
      codeRegions.add(PROVENCE_ALPES_COTE_D_AZUR);
      return codeRegions;
   }

   /**
    * Toutes les régions métropolitaines
    * @return Régions métropolitaines
    */
   public static Iterable<CodeRegion> regionsMetropolitaines() {
      List<CodeRegion> codeRegions = new ArrayList<>();
      codeRegions.add(AUVERGNE_RHONE_ALPES);
      codeRegions.add(BOURGOGNE_FRANCHE_COMTE);
      codeRegions.add(BRETAGNE);
      codeRegions.add(CENTRE_VAL_DE_LOIRE);
      codeRegions.add(CORSE);

      codeRegions.add(GRAND_EST);
      codeRegions.add(HAUTS_DE_FRANCE);
      codeRegions.add(ILE_DE_FRANCE);
      codeRegions.add(NORMANDIE);
      codeRegions.add(NOUVELLE_AQUITAINE);

      codeRegions.add(OCCITANIE);
      codeRegions.add(PAYS_DE_LA_LOIRE);
      codeRegions.add(PROVENCE_ALPES_COTE_D_AZUR);
      return codeRegions;
   }

   /**
    * Toutes les régions d'outre-mer
    * @return Régions d'outre-mer
    */
   public static Iterable<CodeRegion> regionsOutremer() {
      List<CodeRegion> codeRegions = new ArrayList<>();
      codeRegions.add(GUADELOUPE);
      codeRegions.add(GUYANE);
      codeRegions.add(LA_REUNION);
      codeRegions.add(MARTINIQUE);
      codeRegions.add(MAYOTTE);
      return codeRegions;
   }

   /**
    * Renvoyer le nom de la région.
    * @return Nom de la région
    */
   public String getNom() {
      return switch(this.getId()) {
         case "84" -> "Auvergne-Rhône-Alpes";
         case "27" -> "Bourgogne-Franche-Comté";
         case "53" -> "Bretagne";
         case "24" -> "Centre-Val de Loir";
         case "94" -> "Corse";

         case "01" -> "Guadeloupe";
         case "44" -> "Grand Est";
         case "03" -> "Guyane";
         case "32" -> "Hauts-de-France";
         case "11" -> "Île-de-France";

         case "04" -> "La Réunion";
         case "02" -> "Martinique";
         case "06" -> "Mayotte";
         case "28" -> "Normandie";
         case "75" -> "Nouvelle-Aquitaine";

         case "76" -> "Occitanie";
         case "52" -> "Pays de la Loire";
         case "93" -> "Provence-Alpes-Côte d'Azur";
         case "07" -> "Saint-Barthélémy";
         case "08" -> "Saint-Martin";

         default -> "Région Inconnue";
      };
   }

   /**
    * @see ObjetMetierIdentifiable#anomalies(Anomalies)
    */
   @Override
   public void anomalies(Anomalies anomalies) {
      super.anomalies(anomalies);

      // Le code région doit être alimenté.
      if (StringUtils.isBlank(this.getId())) {
         anomalies.declare(SeveriteAnomalie.ERREUR, "Le code de la région n''est pas renseigné.");
      }
   }
}
