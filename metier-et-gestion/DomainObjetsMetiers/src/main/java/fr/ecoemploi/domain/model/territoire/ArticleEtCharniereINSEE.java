package fr.ecoemploi.domain.model.territoire;

/**
 * Article Charnière : dit l'article et la charnière à utiliser pour nommer une ville ou une région.
 * @author Marc LE BIHAN
 */
public enum ArticleEtCharniereINSEE {
   /** Pas d'article et le nom commence par une consonne sauf H muet. charnière = DE */
   AUCUN_CONSONNE_DE("0"),

   /** Pas d'article et le nom commence par une voyelle ou un H muet. charnière = D' */
   AUCUN_D_APOSTROPHE("1"),

   /** article = LE charnière = DU */
   LE_DU("2"),

   /** article = LA charnière = DE LA */
   LA_DE_LA("3"),

   /** article = LES charnière = DES */
   LES_DES("4"),

   /** article = L' charnière = DE L' */
   L_APOSTROPHE_DE_L_APOSTROPHE("5"),

   /** article = AUX charnière = DES */
   AUX_DES("6"),

   /** article = LAS charnière = DE LAS */
   LAS_DE_LAS("7"),

   /** article = LOS charnière = DE LOS */
   LOS_DE_LOS("8");

   /** Code de l'article - charnière. */
   private final String code;

   /**
    * Construire l'article charnière INSEE.
    * @param codeCharniereINSEE Code associé.
    */
   ArticleEtCharniereINSEE(String codeCharniereINSEE) {
      this.code = codeCharniereINSEE;
   }

   /**
    * Renvoyer le code de l'article charnière.
    * @return Code.
    */
   public String getCode() {
      return this.code;
   }
}
