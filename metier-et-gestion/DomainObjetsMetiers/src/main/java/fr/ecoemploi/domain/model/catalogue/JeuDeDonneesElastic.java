package fr.ecoemploi.domain.model.catalogue;

import java.io.*;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.*;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

/**
 * Entrée Elastic pour un Jeu de données du catalogue data.gouv.fr
 * @author Marc Le Bihan
 */
public class JeuDeDonneesElastic implements Serializable {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -2049351782182200359L;

   /** Identifiant du jeu de données (catalogue). */
   private String catalogueId;

   /** Titre de ce jeu de données */
   private String titre;

   /** Accroche de publication */
   private String slug;

   /** Acronyme */
   private String acronyme;

   /** URL du jeu de données. */
   private String url;

   /** Identifiant de l'organisation émétrice. */
   private String organisationId;

   /** Nom de l'organisation. */
   private String organisation;

   /** Description du jeu de données. */
   private String description;

   /** Fréquence. */
   private String frequence;

   /** Licence. */
   private String licence;

   /** Date de début. */
   @JsonDeserialize(using = LocalDateDeserializer.class)
   @JsonSerialize(using = LocalDateSerializer.class)
   private LocalDate dateDebut;

   /** Date de fin. */
   @JsonDeserialize(using = LocalDateDeserializer.class)
   @JsonSerialize(using = LocalDateSerializer.class)
   private LocalDate dateFin;

   /** Granularité spatiale. */
   private String granulariteSpatiale;

   /** Zones spatiales. */
   private String zonesSpatiales;

   /** Privé. */
   private Boolean prive;

   /** Date de création. */
   @JsonDeserialize(using = LocalDateDeserializer.class)
   @JsonSerialize(using = LocalDateSerializer.class)
   private LocalDate dateCreation;

   /** Date de modification. */
   @JsonDeserialize(using = LocalDateDeserializer.class)
   @JsonSerialize(using = LocalDateSerializer.class)
   private LocalDate dateModification;

   /** Tags */
   private Set<String> tags;

   /**
    * Construire un jeu de données pour un index Elastic.
    */
   public JeuDeDonneesElastic() {
   }

   /**
    * Construire un jeu de données pour un index Elastic.
    * @param jeuDeDonnees Jeu de données de référence.
    */
   public JeuDeDonneesElastic(JeuDeDonnees jeuDeDonnees) {
      this.catalogueId = jeuDeDonnees.getId() != null ? jeuDeDonnees.getId().toString() : null;
      this.titre = jeuDeDonnees.getTitre();
      this.slug = jeuDeDonnees.getSlug();
      this.acronyme = jeuDeDonnees.getAcronyme();
      this.url = jeuDeDonnees.getUrl();

      this.organisationId = jeuDeDonnees.getOrganisationId() != null ? jeuDeDonnees.getOrganisationId().toString() : null;
      this.organisation = jeuDeDonnees.getOrganisation();
      this.description = jeuDeDonnees.getDescription();
      this.frequence = jeuDeDonnees.getFrequence();
      this.licence = jeuDeDonnees.getLicence();

      this.dateDebut = jeuDeDonnees.getDateDebut();
      this.dateFin = jeuDeDonnees.getDateFin();
      this.granulariteSpatiale = jeuDeDonnees.getGranulariteSpatiale();
      this.zonesSpatiales = jeuDeDonnees.getZonesSpatiales();
      this.prive = jeuDeDonnees.isPrive();

      this.dateCreation = jeuDeDonnees.getDateCreation();
      this.dateModification = jeuDeDonnees.getDateModification();
      this.tags = jeuDeDonnees.tags();
   }

   /**
    * Renvoyer l'identifiant du jeu de données (catalogue).
    * @return Identifiant du jeu de données
    */
   public String getCatalogueId() {
      return this.catalogueId;
   }

   /**
    * Fixer l'identifiant du jeu de données (catalogue).
    * @param catalogueId Identifiant du jeu de données
    */
   public void setCatalogueId(String catalogueId) {
      this.catalogueId = catalogueId;
   }

   /**
    * Renvoyer le titre du jeu de données.
    * @return titre du jeu de données.
    */
   public String getTitre() {
      return this.titre;
   }

   /**
    * Fixer le titre du jeu de données.
    * @param titre du jeu de données.
    */
   public void setTitre(String titre) {
      this.titre = titre;
   }

   /**
    * Renvoyer l'accroche du jeu de données.
    * @return Accroche
    */
   public String getSlug() {
      return this.slug;
   }

   /**
    * Fixer l'accroche du jeu de données.
    * @param slug Accroche.
    */
   public void setSlug(String slug) {
      this.slug = slug;
   }

   /**
    * Renvoyer l'accronyme.
    * @return Accronyme.
    */
   public String getAcronyme() {
      return this.acronyme;
   }

   /**
    * Fixer l'accronyme.
    * @param acronyme Accronyme.
    */
   public void setAcronyme(String acronyme) {
      this.acronyme = acronyme;
   }

   /**
    * Renvoyer l'URL du jeu de données.
    * @return URL.
    */
   public String getUrl() {
      return this.url;
   }

   /**
    * Fixer l'URL du jeu de données.
    * @param url URL.
    */
   public void setUrl(String url) {
      this.url = url;
   }

   /**
    * Renvoyer l'identifiant de l'organisation émétrice.
    * @return Identifiant de l'organisation
    */
   public String getOrganisationId() {
      return this.organisationId;
   }

   /**
    * Fixer l'identifiant de l'organisation émétrice.
    * @param organisationId Identifiant de l'organisation
    */
   public void setOrganisationId(String organisationId) {
      this.organisationId = organisationId;
   }

   /**
    * Renvoyer le nom de l'organisation émétrice.
    * @return Nom de l'organisation.
    */
   public String getOrganisation() {
      return this.organisation;
   }

   /**
    * Fixer le nom de l'organisation émétrice.
    * @param organisation Nom de l'organisation.
    */
   public void setOrganisation(String organisation) {
      this.organisation = organisation;
   }

   /**
    * Renvoyer la description du jeu de données.
    * @return Description du jeu de données.
    */
   public String getDescription() {
      return this.description;
   }

   /**
    * Fixer la description du jeu de données.
    * @param description Description du jeu de données.
    */
   public void setDescription(String description) {
      this.description = description;
   }

   /**
    * Renvoyer la fréquence de parution.
    * @return Fréquence : unknown, annual, punctual, irregular, monthly, daily, weekly, continuous, quarterly, semiannual
    * bimonthly, biweekly, threeTimesAYear, biennial, quinquennial, triennial, hourly, False, fourTimesAWeek
    * semidaily, semimonthly, semiweekly, threeTimesADay, fourTimesADay, threeTimesAWeek, threeTimesAMonth
    */
   public String getFrequence() {
      return this.frequence;
   }

   /**
    * Fixer la fréquence de parution.
    * @param frequence Fréquence de parution.
    */
   public void setFrequence(String frequence) {
      this.frequence = frequence;
   }

   /**
    * Renvoyer le type de licence.
    * @return Licence : Licence Ouverte / Open Licence version 2.0, License Not Specified, Licence Ouverte / Open Licence, Open Data Commons Open Database License (ODbL),
    * Other (Attribution), Other (Open), Other (Public Domain), Creative Commons Attribution, Creative Commons Attribution Share-Alike, Open Data Commons Attribution License,
    * Creative Commons CCZero, Open Data Commons Public Domain Dedication and Licence (PDDL)
    */
   public String getLicence() {
      return this.licence;
   }

   /**
    * Fixer le type de licence.
    * @param licence Licence.
    */
   public void setLicence(String licence) {
      this.licence = licence;
   }

   /**
    * Renvoyer la date de début.
    * @return Date de début.
    */
   public LocalDate getDateDebut() {
      return this.dateDebut;
   }

   /**
    * Fixer la date de début.
    * @param dateDebut Date de début.
    */
   public void setDateDebut(LocalDate dateDebut) {
      this.dateDebut = dateDebut;
   }

   /**
    * Renvoyer la date de fin.
    * @return Date de fin.
    */
   public LocalDate getDateFin() {
      return this.dateFin;
   }

   /**
    * Fixer la date de fin.
    * @param dateFin Date de fin.
    */
   public void setDateFin(LocalDate dateFin) {
      this.dateFin = dateFin;
   }

   /**
    * Renvoyer la granularité spatiale.
    * @return Granularité spatiale : other, fr:commune, country, fr:epci, fr:departement, poi, fr:region,
    * fr:canton, fr:collectivite, country-group, fr:iris, country-subset, False, fr:arrondissement
    */
   public String getGranulariteSpatiale() {
      return this.granulariteSpatiale;
   }

   /**
    * Fixer la granularité spatiale.
    * @param granulariteSpatiale Granularité spatiale.
    */
   public void setGranulariteSpatiale(String granulariteSpatiale) {
      this.granulariteSpatiale = granulariteSpatiale;
   }

   /**
    * Renvoyer les zones spatiales
    * @return Zones spatiales.
    */
   public String getZonesSpatiales() {
      return this.zonesSpatiales;
   }

   /**
    * Fixer les zones spatiales.
    * @param zonesSpatiales Zones spatiales.
    */
   public void setZonesSpatiales(String zonesSpatiales) {
      this.zonesSpatiales = zonesSpatiales;
   }

   /**
    * Déterminer si le jeu de données est privé.
    * @return true, s'il l'est.
    */
   public Boolean isPrive() {
      return this.prive;
   }

   /**
    * Indiquer si le jeu de données est privé.
    * @param prive true s'il l'est.
    */
   public void setPrive(Boolean prive) {
      this.prive = prive;
   }

   /**
    * Renvoyer la date de création du jeu de données.
    * @return Date de création.
    */
   public LocalDate getDateCreation() {
      return this.dateCreation;
   }

   /**
    * Fixer la date de création du jeu de données.
    * @param dateCreation Date de création.
    */
   public void setDateCreation(LocalDate dateCreation) {
      this.dateCreation = dateCreation;
   }

   /**
    * Renvoyer la date de modification du jeu de données.
    * @return Date de modification.
    */
   public LocalDate getDateModification() {
      return this.dateModification;
   }

   /**
    * Fixer la date de modification du jeu de données.
    * @param dateModification Date de création.
    */
   public void setDateModification(LocalDate dateModification) {
      this.dateModification = dateModification;
   }

   /**
    * Renvoyer les tags associés au jeu de données.
    * @return tags.
    */
   public Set<String> tags() {
      return this.tags;
   }

   /**
    * Fixer les tags associés au jeu de données.
    * @param tags tags.
    */
   public void tags(Set<String> tags) {
      this.tags = tags;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      String format = "id : {0}, titre : {1}, description : {2}, organisation : {3} (id : {4}), " +
         "granularité spatiale : {5}, zones spatiales : {6}, tags : {7}, url : {8}, accronyme : {9}, " +
         "slug : {10}, date de création : {11}, date de modification : {12}, fréquence : {13}, licence : {14}, " +
         "date de début : {15}, date de fin : {16}, jeu de données privé : {17}";

      return MessageFormat.format(format, this.catalogueId, this.titre, this.description, this.organisation, this.organisationId,
         this.granulariteSpatiale, this.zonesSpatiales, this.tags, this.url, this.acronyme,
         this.slug, this.dateCreation, this.dateModification, this.frequence, this.licence,
         this.dateDebut, this.dateFin, this.prive);
   }
}
