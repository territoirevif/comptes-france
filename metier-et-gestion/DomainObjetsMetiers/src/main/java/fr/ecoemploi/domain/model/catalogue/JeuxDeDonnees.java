package fr.ecoemploi.domain.model.catalogue;

import java.io.Serial;
import java.util.*;

/**
 * Un ensemble de Jeux de données du catalogue datagouv.
 * @author Marc Le Bihan
 */
public class JeuxDeDonnees extends LinkedHashMap<CatalogueId, JeuDeDonnees> implements Iterable<JeuDeDonnees> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3430489459116736919L;

   /**
    * Construire une liste de jeux de données vides.
    */
   public JeuxDeDonnees() {
   }

   /**
    * Construire une liste de jeux de données alimentée.
    * @param source Source de jeux de données à copier.
    */
   public JeuxDeDonnees(Collection<JeuDeDonnees> source) {
      Objects.requireNonNull(source, "La collection de jeux de données à copier ne peut pas valoir null.");
      source.forEach(this::add);
   }
   
   /**
    * Ajouter un élément à la liste.
    * @param jeuDeDonnees jeu de données à ajouter.
    */
   public void add(JeuDeDonnees jeuDeDonnees) {
      Objects.requireNonNull(jeuDeDonnees, "Le jeu de données à ajouter ne peut pas valoir null.");
      put(jeuDeDonnees.getId(), jeuDeDonnees);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Iterator<JeuDeDonnees> iterator() {
      return values().iterator();
   }
}
