package fr.ecoemploi.domain.model.territoire;

import java.io.Serial;

import fr.ecoemploi.domain.utils.objets.MetierException;

/**
 * Exception levée si un SIREN de commune ne porte pas d'information de département.
 * @author Marc LE BIHAN
 */
public class SIRENCommunalSansDepartementException extends MetierException {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3535560571269179813L;

   /** Siren. */
   private final SIRENCommune siren;
   
   /**
    * Construire une exception.
    * @param message Message accompagnant l'exception.
    * @param siren SIREN sans département.
    */
   public SIRENCommunalSansDepartementException(String message, SIRENCommune siren) {
      super(message);
      this.siren = siren;
   }
   
   /**
    * Renvoyer le SIREN qui ne mentionne pas le département.
    * @return SIREN.
    */
   public SIRENCommune getSIREN() {
      return this.siren;
   }
}
