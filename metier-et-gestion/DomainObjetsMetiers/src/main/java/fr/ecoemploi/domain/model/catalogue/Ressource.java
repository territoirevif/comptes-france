package fr.ecoemploi.domain.model.catalogue;

import java.io.Serial;
import java.text.MessageFormat;
import java.time.LocalDate;

import fr.ecoemploi.domain.utils.objets.ObjetMetierIdentifiable;

/**
 * Une ressource d'un jeu de données, mais sans ses données parentes dupliquées du "dataset" du catalogue.
 * @author Marc Le Bihan
 */
public class Ressource extends ObjetMetierIdentifiable<RessourceJeuDeDonneesId> {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -8335805745536187034L;

   /** Identifiant du catalogue auquel appartient cette ressource. */
   private CatalogueId catalogueId;

   /** URL de la ressource. */
   private String url;

   /** Titre de la ressource. */
   private String titre;

   /** Description de la ressource. */
   private String description;

   /** Type de fichier. */
   private String typeDeFichier;

   /** Format du fichier. */
   private String format;

   /** Type MIME du fichier. */
   private String mime;

   /** Taille du fichier. */
   private Integer tailleDuFichier;

   /** Type de checksum. */
   private String typeDeChecksum;

   /** Valeur de checksum. */
   private String valeurChecksum;

   /** Date de création de la ressource. */
   private LocalDate dateCreation;

   /** Date de modification de la ressource. */
   private LocalDate dateModification;

   /** Nombre de téléchargements. */
   private Integer nombreDeTelechargements;

   /** Date de création du moissonneur. */
   private LocalDate moissonneurDateCreation;

   /** Date de modification du moissonneur. */
   private LocalDate moissonneurDateModification;

   /**
    * Construire un jeu de données.
    */
   public Ressource() {
   }

   /**
    * Construire un jeu de données.
    * @param catalogueId Identifiant du catalogue (jeu de données) auquel appartient cette ressource.
    * @param ressourceJeuDeDonneesId Identifiant de la ressource du jeu de données.
    * @param url URL de la ressource.
    * @param titre Titre de la ressource.
    * @param description Description de la ressource.
    * @param typeDeFichier Type de fichier.
    * @param format Format du fichier.
    * @param mime Type MIME du fichier.
    * @param tailleDuFichier Taille du fichier.
    * @param typeDeChecksum Type de checksum.
    * @param valeurChecksum Valeur de checksum.
    * @param dateCreation Date de création de la ressource.
    * @param dateModification date de modification de la ressource.
    * @param nombreDeTelechargements Nombre de téléchargements de la ressource.
    * @param moissonneurDateCreation Date de création du moissonneur.
    * @param moissonneurDateModification Date de modification du moissonneur.
    */
   public Ressource(CatalogueId catalogueId, RessourceJeuDeDonneesId ressourceJeuDeDonneesId, String url, String titre, String description, String typeDeFichier,
        String format, String mime, Integer tailleDuFichier, String typeDeChecksum, String valeurChecksum,
        LocalDate dateCreation, LocalDate dateModification, Integer nombreDeTelechargements, LocalDate moissonneurDateCreation, LocalDate moissonneurDateModification) {
      super.setId(ressourceJeuDeDonneesId);
      this.catalogueId = catalogueId;

      this.url = url;
      this.titre = titre;
      this.description = description;
      this.typeDeFichier = typeDeFichier;
      this.format = format;

      this.mime = mime;
      this.tailleDuFichier = tailleDuFichier;
      this.typeDeChecksum = typeDeChecksum;
      this.valeurChecksum = valeurChecksum;
      this.dateCreation = dateCreation;

      this.dateModification = dateModification;
      this.nombreDeTelechargements = nombreDeTelechargements;
      this.moissonneurDateCreation = moissonneurDateCreation;
      this.moissonneurDateModification = moissonneurDateModification;
   }

   /**
    * Renvoyer l'identifiant du catalogue auquel appartient cette ressource.
    * @return Identifiant du catalogue.
    */
   public CatalogueId getCatalogueId() {
      return this.catalogueId;
   }

   /**
    * Fixer l'identifiant du catalogue auquel appartient cette ressource.
    * @param id Identifiant du catalogue.
    */
   public void setCatalogueId(CatalogueId id) {
      this.catalogueId = id;
   }

   /**
    * Renvoyer la date de création du jeu de données.
    * @return Date de création.
    */
   public LocalDate getDateCreation() {
      return this.dateCreation;
   }

   /**
    * Fixer la date de création du jeu de données.
    * @param dateCreation Date de création.
    */
   public void setDateCreation(LocalDate dateCreation) {
      this.dateCreation = dateCreation;
   }

   /**
    * Renvoyer la date de modification du jeu de données.
    * @return Date de modification.
    */
   public LocalDate getDateModification() {
      return this.dateModification;
   }

   /**
    * Fixer la date de modification du jeu de données.
    * @param dateModification Date de création.
    */
   public void setDateModification(LocalDate dateModification) {
      this.dateModification = dateModification;
   }

   /**
    * Renvoyer la description du jeu de données.
    * @return Description du jeu de données.
    */
   public String getDescription() {
      return this.description;
   }

   /**
    * Fixer la description du jeu de données.
    * @param description Description du jeu de données.
    */
   public void setDescription(String description) {
      this.description = description;
   }

   /**
    * Renvoyer le format du fichier portant la ressource.
    * @return Format du fichier.
    */
   public String getFormat() {
      return this.format;
   }

   /**
    * Fixer le format du fichier portant la ressource.
    * @param format Format du fichier.
    */
   public void setFormat(String format) {
      this.format = format;
   }

   /**
    * Renvoyer le type MIME du fichier portant la ressource.
    * @return Type MIME du fichier.
    */
   public String getMime() {
      return this.mime;
   }

   /**
    * Fixer le type MIME du fichier portant la ressource.
    * @param mime Type MIME du fichier.
    */
   public void setMime(String mime) {
      this.mime = mime;
   }

   /**
    * Renvoyer la date de création du moissonneur.
    * @return Date de création.
    */
   public LocalDate getMoissonneurDateCreation() {
      return this.moissonneurDateCreation;
   }

   /**
    * Fixer la date de création du moissonneur.
    * @param  moissonneurDateCreation Date de création.
    */
   public void setMoissonneurDateCreation(LocalDate moissonneurDateCreation) {
      this.moissonneurDateCreation = moissonneurDateCreation;
   }

   /**
    * Renvoyer la date de modification du moissonneur.
    * @return Date de modification.
    */
   public LocalDate getMoissonneurDateModification() {
      return this.moissonneurDateModification;
   }

   /**
    * Fixer la date de modification du moissonneur.
    * @param moissonneurDateModification Date de modification.
    */
   public void setMoissonneurDateModification(LocalDate moissonneurDateModification) {
      this.moissonneurDateModification = moissonneurDateModification;
   }

   /**
    * Renvoyer le nombre de ressources qui composent ce jeu de données.
    * @return Nombre de ressources.
    */
   public Integer getNombreDeTelechargements() {
      return this.nombreDeTelechargements;
   }

   /**
    * Fixer le nombre de ressources qui composent ce jeu de données.
    * @param nombreDeTelechargements Nombre de ressources.
    */
   public void setNombreDeTelechargements(Integer nombreDeTelechargements) {
      this.nombreDeTelechargements = nombreDeTelechargements;
   }

   /**
    * Renvoyer la taille du fichier.
    * @return Taille du fichier.
    */
   public Integer getTailleDuFichier() {
      return tailleDuFichier;
   }

   /**
    * Fixer la taille du fichier.
    * @param tailleDuFichier Taille du fichier.
    */
   public void setTailleDuFichier(Integer tailleDuFichier) {
      this.tailleDuFichier = tailleDuFichier;
   }

   /**
    * Renvoyer le titre du jeu de données.
    * @return titre du jeu de données.
    */
   public String getTitre() {
      return this.titre;
   }

   /**
    * Fixer le titre du jeu de données.
    * @param titre du jeu de données.
    */
   public void setTitre(String titre) {
      this.titre = titre;
   }

   /**
    * Renvoyer le type de checksum de contrôle de la ressource.
    * @return Type de checksum.
    */
   public String getTypeDeChecksum() {
      return this.typeDeChecksum;
   }

   /**
    * Fixer le type de checksum de contrôle de la ressource.
    * @param typeDeChecksum Type de checksum.
    */
   public void setTypeDeChecksum(String typeDeChecksum) {
      this.typeDeChecksum = typeDeChecksum;
   }

   /**
    * Renvoyer le type de fichier qui porte cette ressource.
    * @return Type de fichier.
    */
   public String getTypeDeFichier() {
      return this.typeDeFichier;
   }

   /**
    * Fixer le type de fichier qui porte cette ressource.
    * @param typeDeFichier Type de fichier.
    */
   public void setTypeDeFichier(String typeDeFichier) {
      this.typeDeFichier = typeDeFichier;
   }

   /**
    * Renvoyer l'URL du jeu de données.
    * @return URL.
    */
   public String getUrl() {
      return this.url;
   }

   /**
    * Fixer l'URL du jeu de données.
    * @param url URL.
    */
   public void setUrl(String url) {
      this.url = url;
   }

   /**
    * Renvoyer la valeur de checksum de contrôle de la ressource.
    * @return Checksum de contrôle.
    */
   public String getValeurChecksum() {
      return this.valeurChecksum;
   }

   /**
    * Fixer la valeur de checksum de contrôle de la ressource.
    * @param valeurChecksum Checksum de contrôle.
    */
   public void setValeurChecksum(String valeurChecksum) {
      this.valeurChecksum = valeurChecksum;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String toString() {
      String fmt = "id : {0} (parent : {1}), titre: {2}, description: {3}, date de création : {4}, " +
         "date de modification : {5}, url de la ressource : {6}, type de fichier/format/mime : {7}/{8}/{9}, " +
         "taille du fichier : {10}, type/valeur de checksum : {11}/{12}, nombre de téléchargements : {13}, date de création du moissonneur : {14}, " +
         "date de modification du moissonneur : {15}";

      Object[] args = {super.getId(), this.catalogueId, this.titre, this.description, this.dateCreation,
         this.dateModification, this.url, this.typeDeFichier, this.format, this.mime,
         this.tailleDuFichier, this.typeDeChecksum, this.valeurChecksum, this.nombreDeTelechargements, this.moissonneurDateCreation,
         this.moissonneurDateModification
      };

      return MessageFormat.format(fmt, args);
   }
}
