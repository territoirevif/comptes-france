package fr.ecoemploi.domain.model.catalogue.metadata;

/**
 * Nature de la donnée. 
 * @author Marc LE BIHAN
 */
public enum NatureDonnee {
   /** Territoire (région). */
   TERRITOIRE_REGIONS("Régions"),
   
   /** Territoire (départements). */
   TERRITOIRE_DEPARTEMENTS("Départements"),
   
   /** Territoire (communes) */
   TERRITOIRE_COMMUNES("Communes"),
   
   /** Territoire (intercommunalités). */
   TERRITOIRE_INTERCOMMUNALITES("Intercommunalités"),
   
   /** Comptes (communes) */
   COMPTES_COMMUNES("Balance des comptes des communes"),
   
   /** Comptes (intercommunalités) */
   COMPTES_INERCOMMUNALITES("Balance des comptes des intercommunalités"),
   
   /** Contour des communes */
   CONTOURS_COMMUNES("Contours des communes");

   /** Libellé de l'entrée. */
   private final String libelle;
   
   /**
    * Construire une entrée d'énumération en recherchant son libellé public.
    * @param libelle Libellé de la nature de la donnée.
    */
   NatureDonnee(String libelle) {
      this.libelle = libelle;
   }
   
   /**
    * Renvoyer le libellé public de l'entrée.
    * @return Libellé.
    */
   public String getLibelle() {
      return this.libelle;
   }
   
   /**
    * @see java.lang.Enum#toString()
    */
   @Override
   public String toString() {
      return this.libelle;
   }
}
