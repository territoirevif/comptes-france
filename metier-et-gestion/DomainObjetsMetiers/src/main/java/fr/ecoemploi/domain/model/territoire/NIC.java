package fr.ecoemploi.domain.model.territoire;

import org.apache.commons.lang3.*;

import java.io.Serial;

import fr.ecoemploi.domain.utils.autocontrole.*;

/**
 * Numéro Interne de Classement qui permet de distinguer les établissements d'une même entreprise :<br>
 * Il est composé de 5 chiffres, par association au SIREN, il forme le SIRET.
 * @author Marc LE BIHAN
 */
public class NIC extends Id {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 3160995146436245816L;
   
   /**
    * Constuire un Numéro Interne de Classement (NIC).
    * @param nic Numéro Interne de Classement.
    */
   public NIC(String nic) {
      setId(nic);
   }
   
   /**
    * @see Id#anomalies(Anomalies)
    */
   @Override
   public void anomalies(Anomalies anomalies) {
      // Le numéro interne de classement doit être alimenté.
      if (StringUtils.isBlank(getId())) {
         anomalies.declare(SeveriteAnomalie.ERREUR, "Le Numéro Interne de Classement (NIC) n'est pas alimenté.");
         return; // Et il n'est pas possible d'aller plus loin dans les contrôles dans ce cas.
      }
      
      // Il doit faire 5 caractères.
      if (getId().length() != 5) {
         anomalies.declare(SeveriteAnomalie.ERREUR, "Ce Numéro Interne de Classement (NIC) ''{0}'' a {1} caractère(s) au lieu des 5 attendus.", getId(), getId().length());
      }
   }
}
