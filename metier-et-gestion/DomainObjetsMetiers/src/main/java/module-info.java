/**
 * Module des utilitaires de bas niveau.
 */
module fr.ecoemploi.domain.model {
   /* Pour Spring-Boot 3
   requires jakarta.activation;
   requires jakarta.validation;
    */

   // Pour Spring-Boot 2
   requires java.validation;

   requires io.swagger.v3.oas.annotations;
   requires org.apache.commons.lang3;
   requires org.slf4j;
   requires com.fasterxml.jackson.databind;
   requires com.fasterxml.jackson.datatype.jsr310;

   requires fr.ecoemploi.domain.utils;

   exports fr.ecoemploi.domain.model.catalogue;
   exports fr.ecoemploi.domain.model.catalogue.metadata;

   exports fr.ecoemploi.domain.model.territoire;
   exports fr.ecoemploi.domain.model.territoire.association;
   exports fr.ecoemploi.domain.model.territoire.comptabilite;
   exports fr.ecoemploi.domain.model.territoire.comptabilite.comptesIndividuels;
   exports fr.ecoemploi.domain.model.territoire.entreprise;
   exports fr.ecoemploi.domain.model.territoire.entreprise.ape;
}
