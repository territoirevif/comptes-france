package fr.ecoemploi.domain.model.territoire;

import fr.ecoemploi.domain.model.catalogue.metadata.Periode;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

import java.time.*;
import java.util.*;

/**
 * Test des périodes.
 * @author Marc LE BIHAN
 */
class TestPeriode {
   /**
    * Détection d'une période infinie.
    */
   @Test
   void infini() {
      assertTrue(new Periode().isInfini(), "Une période créée par défaut est infinie (A).");
      assertTrue(new Periode(LocalDateTime.MIN, LocalDateTime.MAX).isInfini(), "Une période créée par défaut est infinie (B).");
      assertFalse(new Periode(LocalDateTime.MAX, LocalDateTime.MIN).isInfini(), "Une période (MAX, MIN) n'est pas infinie parce qu'elle est invalide.");
      assertFalse(new Periode(LocalDateTime.MIN, LocalDateTime.now()).isInfini(), "Une période (MIN, now()) ne doit pas être infinie.");
      assertFalse(new Periode(LocalDateTime.now(), LocalDateTime.MAX).isInfini(), "Une période (now(), MAX) ne doit pas être infinie.");
      assertFalse(new Periode(LocalDateTime.now().minusMinutes(1), LocalDateTime.now()).isInfini(), "Une période (now(), now()) ne doit pas être infinie.");
   }

   /**
    * Test de la présence ou de l'absence d'horaire.
    */
   @Test
   void horaires() {
      LocalDateTime d1 = LocalDateTime.parse("2007-12-03T00:00:00.00");
      LocalDateTime d2 = LocalDateTime.parse("2009-02-15T00:00:00.00");
      LocalDateTime h1 = LocalDateTime.parse("2007-12-03T10:15:30.00");
      LocalDateTime h2 = LocalDateTime.parse("2009-02-15T21:40:55.00");

      assertFalse(Periode.hasHoraire(d1), "d1 ne doit pas avoir d'horaire.");
      assertTrue(Periode.hasHoraire(h1), "h1 doit avoir un horaire.");

      assertTrue(new Periode(d1, h2).hasHoraire(), "(d1,h2) doit avoir un horaire.");
      assertTrue(new Periode(h1, d2).hasHoraire(), "(h1,d2) doit avoir un horaire.");
      assertTrue(new Periode(h1, h2).hasHoraire(), "(h1,h2) doit avoir un horaire.");

      assertFalse(new Periode(d1, d2).hasHoraire(), "(d1,d2) ne doit pas avoir d'horaire.");
   }

   /**
    * Test des strings.
    */
   @Test
   void strings() {
      LocalDateTime d1 = LocalDateTime.parse("2007-12-13T00:00:00.00");
      LocalDateTime d2 = LocalDateTime.parse("2009-11-15T00:00:00.00");
      LocalDateTime h1 = LocalDateTime.parse("2007-12-13T10:15:30.00");
      LocalDateTime h2 = LocalDateTime.parse("2009-11-15T21:40:55.00");

      Periode infini = new Periode();
      assertEquals("(min, max)", infini.toString(), "Mauvais toString() de période infinie");

      Periode min_sans_max_sans_horaire = new Periode(LocalDateTime.MIN, d2);
      assertTrue(min_sans_max_sans_horaire.toString().contains("min"), "Mauvais toString de période (min, d2) (A)");
      assertTrue(min_sans_max_sans_horaire.toString().contains("11"), "Mauvais toString de période (min, d2) (B)");

      Periode min_sans_max_avec_horaire = new Periode(LocalDateTime.MIN, h2);
      assertTrue(min_sans_max_avec_horaire.toString().contains("min"), "Mauvais toString de période (min, h2) (C)");
      assertTrue(min_sans_max_avec_horaire.toString().contains("11"), "Mauvais toString de période (min, h2) (D)");
      assertTrue(min_sans_max_avec_horaire.toString().contains("40"), "Mauvais toString de période (min, h2) (E)");

      Periode max_sans_min_sans_horaire = new Periode(d1, LocalDateTime.MAX);
      assertTrue(max_sans_min_sans_horaire.toString().contains("max"), "Mauvais toString de période (d1, max) (A)");
      assertTrue(max_sans_min_sans_horaire.toString().contains("12"), "Mauvais toString de période (d1, max) (B)");

      Periode max_sans_min_avec_horaire = new Periode(h1, LocalDateTime.MAX);
      assertTrue(max_sans_min_avec_horaire.toString().contains("max"), "Mauvais toString de période (h1, max) (C)");
      assertTrue(max_sans_min_avec_horaire.toString().contains("12"), "Mauvais toString de période (h1, max) (D)");
      assertTrue(max_sans_min_avec_horaire.toString().contains("15"), "Mauvais toString de période (h1, max) (E)");

      Periode d1d2 = new Periode(d1, d2);
      assertTrue(d1d2.toString().contains("12"), "Mauvais toString de période (d1, d2) (A).");
      assertTrue(d1d2.toString().contains("11"), "Mauvais toString de période (d1, d2) (B).");

      Periode d1h2 = new Periode(d1, h2);
      assertTrue(d1h2.toString().contains("12"), "Mauvais toString de période (d1, h2) (A).");
      assertTrue(d1h2.toString().contains("11"), "Mauvais toString de période (d1, h2) (B).");
      assertTrue(d1h2.toString().contains("40"), "Mauvais toString de période (d1, h2) (C).");

      Periode h1h2 = new Periode(d1, h2);
      assertTrue(h1h2.toString().contains("12"), "Mauvais toString de période (h1, h2) (A).");
      assertTrue(h1h2.toString().contains("11"), "Mauvais toString de période (h1, h2) (B).");
      assertTrue(h1h2.toString().contains("40"), "Mauvais toString de période (h1, h2) (C).");
      assertTrue(h1h2.toString().contains("15"), "Mauvais toString de période (h1, h2) (C).");
   }

   /**
    * Conversion de LocalDateTime en dates.
    */
   @Test
   @SuppressWarnings("deprecation")
   void conversionDates() {
      LocalDateTime ld1 = LocalDateTime.parse("2007-12-03T10:15:30.00");
      LocalDateTime ld2 = LocalDateTime.of(2017, 11, 30, 14, 0);
      Periode p1 = new Periode(ld1, ld2);

      Date d1 = p1.getDebutAsDate();

      // LocalDateTime vers date.
      assertEquals(ld1.getYear(), d1.getYear() + 1900, "L'année de la LocalDateTime de début est mal convertie en date");
      assertEquals(ld1.getMonthValue(), d1.getMonth() + 1, "Le mois de la LocalDateTime de début est mal convertie en date");
      assertEquals(ld1.getDayOfMonth(), d1.getDate(), "Le jour de la LocalDateTime de début est mal convertie en date");
      assertEquals(ld1.getHour(), d1.getHours(), "L'heure de la LocalDateTime de début est mal convertie en date");
      assertEquals(ld1.getMinute(), d1.getMinutes(), "Les minutes de la LocalDateTime de début sont mal convertie en date");
      assertEquals(ld1.getSecond(), d1.getSeconds(), "Les secondes de la LocalDateTime de début sont mal convertie en date");

      // Date vers LocalDateTime.
      p1.setDebutAsDate(d1);
      assertEquals(d1.getYear() + 1900, ld1.getYear(), "L'année de la date de début est mal convertie en LocalDateTime");
      assertEquals(d1.getMonth() + 1, ld1.getMonthValue(), "Le mois de la date de début est mal convertie en LocalDateTime");
      assertEquals(d1.getDate(), ld1.getDayOfMonth(), "Le jour de la date de début est mal convertie en LocalDateTime");
      assertEquals(d1.getHours(), ld1.getHour(), "L'heure de la date de début est mal convertie en LocalDateTime");
      assertEquals(d1.getMinutes(), ld1.getMinute(), "Les minutes de la date de début sont mal convertie en LocalDateTime");
      assertEquals(d1.getSeconds(), ld1.getSecond(), "Les secondes de la date de début sont mal convertie en LocalDateTime");
   }

   /**
    * Test de clonage.
    */
   @Test
   void clonage() {
      LocalDateTime h1 = LocalDateTime.parse("2007-12-03T10:15:30.00");
      LocalDateTime h2 = LocalDateTime.parse("2009-02-15T21:40:55.00");

      Periode p1 = new Periode(h1, h2);
      Periode p1Clone = new Periode(p1);

      assertEquals(p1, p1Clone, "Les périodes p1 et p1Clone devrait être égales");
      assertNotSame(p1, p1Clone, "Les périodes p1 et p1Clone ne devraient pas partager le même pointeur.");
   }

   /**
    * Test d'égalité.
    */
   @Test
   void egalite() {
      LocalDateTime h1 = LocalDateTime.parse("2007-12-03T10:15:30.00");
      LocalDateTime h2 = LocalDateTime.parse("2009-02-15T21:40:55.00");
      LocalDateTime h3 = LocalDateTime.parse("2011-08-22T10:16:32.17");

      Periode p1 = new Periode(h1, h2);
      Periode p2 = new Periode(h1, h2);
      Periode p3 = new Periode(h1, h3);

      assertEquals(p1, p2, "Les périodes p1 et p2 devrait être égales");
      assertNotEquals(p1, p3, "Les périodes p1 et p3 devraient être différentes");
   }
}
