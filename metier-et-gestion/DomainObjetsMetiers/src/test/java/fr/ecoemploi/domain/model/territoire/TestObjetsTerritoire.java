package fr.ecoemploi.domain.model.territoire;

import org.junit.jupiter.api.*;

import static fr.ecoemploi.domain.utils.autocontrole.Anomalies.anomalies;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test unitaire des objets produits pour l'analyse du territoire.
 * @author Marc LE BIHAN
 */
class TestObjetsTerritoire {
   /**
    * Test des codes régions constants de l'outre-mer.
    */
   @Test
   void codesRegionsOutremer() {
      assertNotNull(CodeRegion.GUADELOUPE.getId(), "Le code région GUADELOUPE ne doit pas valoir null.");
      assertNotNull(CodeRegion.GUYANE.getId(), "Le code région GUYANE ne doit pas valoir null.");
      assertNotNull(CodeRegion.MARTINIQUE.getId(), "Le code région MARTINIQUE ne doit pas valoir null.");
      assertNotNull(CodeRegion.MAYOTTE.getId(), "Le code région MAYOTTE ne doit pas valoir null.");
      assertNotNull(CodeRegion.LA_REUNION.getId(), "Le code région REUNION ne doit pas valoir null.");
   }

   /**
    * Test de la validité d'un code commune.
    */
   @Test
   void validiteCommune() {
      CodeCommune codeVide = new CodeCommune();
      assertTrue(anomalies(codeVide).hasErreur(), "Un code commune vide doit être considéré comme faux.");

      // Test avec 82 01 138 Culoz
      CodeCommune codeSansDepartement = new CodeCommune("138");
      assertTrue(anomalies(codeSansDepartement).hasErreur(), "Un code commune sans département doit être considéré comme faux.");
   }

   /**
    * Test de la validité d'un code département.
    */
   @Test
   void validiteDepartement() {
      CodeDepartement codeVide = new CodeDepartement();
      assertTrue(anomalies(codeVide).hasErreur(), "Un département vide doit être considéré comme faux.");

      CodeDepartement codeSansRegion = new CodeDepartement("89");
      assertFalse(anomalies(codeSansRegion).hasErreur(), "Un département sans région est admis.");

      CodeDepartement codeCorrect = new CodeDepartement("5329");
      assertFalse(anomalies(codeCorrect).hasErreur(), "Un département correct ne doit pas être détecté en erreur. Incident survenu : " + anomalies(codeCorrect));
   }

   /**
    * Test de la validité d'un code région.
    */
   @Test
   void validiteRegion() {
      CodeRegion codeVide = new CodeRegion();
      assertTrue(anomalies(codeVide).hasErreur(), "Un code de région vide doit être considéré comme faux.");

      CodeRegion codeFaux = new CodeRegion("89");
      assertFalse(anomalies(codeFaux).hasErreur(), "Un code de région faux ne peut pas être détecté en erreur si le référentiel n'est pas connu.");

      CodeRegion codeCorrect = new CodeRegion("01");
      assertFalse(anomalies(codeCorrect).hasErreur(), "Un code de région correct ne doit pas être détecté en erreur.");

      reference(codeCorrect);
      assertEquals("02", codeCorrect.getId());
   }

   void reference(CodeRegion code) {
      code.setId("02");
      assertEquals("02", code.getId());
   }
}
