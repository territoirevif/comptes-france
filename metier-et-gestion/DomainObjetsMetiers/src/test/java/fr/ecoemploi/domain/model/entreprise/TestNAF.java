package fr.ecoemploi.domain.model.entreprise;

import java.util.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import org.slf4j.*;
import org.springframework.context.annotation.*;

import fr.ecoemploi.domain.model.territoire.entreprise.ape.*;

/**
 * Test des nomenclatures NAF.
 * @author Marc LE BIHAN
 */
class TestNAF {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(TestNAF.class);

   /**
    * Examen des niveaux 2.
    */
   @Test
   @Description("Examen des nomenclature NAF de niveau 2")
   void niveau2() {
      List<NiveauNAF> liste = new NiveauxNAF().getTousCodesNAF(2);
      assertNotEquals(0L, liste.size(), "Aucun code NAF de niveau 2 n'a été trouvé");

      for(NiveauNAF niveau : liste) {
         LOGGER.info("{} de code {} a pour section {}", niveau.getLibelle(), niveau.getCodeNAF(), niveau.getParent().getCodeNAF());
      }
   }
   
   /**
    * Déclinaison des niveaux 1 en niveaux 2.
    */
   @Test
   @Description("Déclinaison des niveaux 1 en niveaux 2")
   void niveau1() {
      List<NiveauNAF> liste = new NiveauxNAF().getTousCodesNAF(1);
      assertNotEquals(0L, liste.size(), "Aucun code NAF de niveau 1 n'a été trouvé");

      for(NiveauNAF niveau : liste) {
         LOGGER.info("Le niveau 1 {} de code {} a ces sous-niveaux 2 :", niveau.getLibelle(), niveau.getCodeNAF());

         Map<String, NiveauNAF> sousNiveaux = niveau.getSousNiveauxNAF();
         assertNotEquals(0L, sousNiveaux.size(), "Aucun sous-code NAF de niveau 2 n'a été trouvé derrière le niveau 1");

         niveau.getSousNiveauxNAF().values().forEach(n2 -> LOGGER.info("\t{} ({})", n2.getLibelle(), n2.getCodeNAF()));
      }
   }
}
