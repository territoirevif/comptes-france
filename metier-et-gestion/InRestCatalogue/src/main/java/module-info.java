/**
 * Inbound Rest du catalogue data.gouv.fr
 */
module fr.ecoemploi.inbound.rest.catalogue {
   // requires jakarta.ws.rs;
   requires java.ws.rs;
   requires spring.beans;
   requires spring.web;
   requires spring.context;
   requires io.swagger.v3.oas.annotations;

   requires fr.ecoemploi.domain.model;
   requires fr.ecoemploi.inbound.rest.core;

   requires fr.ecoemploi.application.port.catalogue;
   requires fr.ecoemploi.inbound.port.catalogue;

   exports fr.ecoemploi.adapters.inbound.rest.catalogue;
}
