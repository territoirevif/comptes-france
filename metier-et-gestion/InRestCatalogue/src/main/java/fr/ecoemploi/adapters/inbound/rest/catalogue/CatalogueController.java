package fr.ecoemploi.adapters.inbound.rest.catalogue;

import java.io.Serial;
import java.util.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.*;
import io.swagger.v3.oas.annotations.tags.Tag;

import fr.ecoemploi.adapters.inbound.port.catalogue.CatalogueApi;
import fr.ecoemploi.adapters.inbound.rest.core.AbstractRestController;
import fr.ecoemploi.application.port.catalogue.CataloguePort;
import fr.ecoemploi.domain.model.catalogue.JeuDeDonneesElastic;

/**
 * Contrôleur REST pour la gestion des catalogues data.gouv.fr
 * @author Marc LE BIHAN
 */
@RestController
@RequestMapping(value = "/catalogue", name="catalogue")
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:4200", "http://localhost:9091"})
@Tag(name = "Catalogue", description = "Gestion des catalogues de jeux de données de data.gouv.fr")
public class CatalogueController extends AbstractRestController implements CatalogueApi {
   /** Serial UUID. */
   @Serial
   private static final long serialVersionUID = -3977996839730082059L;

   /** Catalogue des jeux de données data.gouv.fr. */
   @Autowired
   private CataloguePort cataloguePort;

   /**
    * Charger les catalogues data.gouv.fr et les mettre en cache.
    */
   @Operation(description = "Charger les catalogues (jeux de données, ressource, etc.) dans les fichiers de cache et dans Elastic")
   @GetMapping(value = "/chargerCatalogueDataGouvFr")
   @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Catalogues data.gouv.fr chargés en cache."),
      @ApiResponse(responseCode = "403", description = "Si la génération de données par l'application est interdite."),
      @ApiResponse(responseCode = "500", description = "Un incident durant le chargement.")
   })
   public void chargerCatalogueDataGouvFr() {
      refusSiGenerationInterdite();
      this.cataloguePort.chargerCatalogueDataGouvFr();
   }

   /**
    * {@inheritDoc}
    */
   @GetMapping(value = "/recherche")
   @Operation(summary = "Faire une recherche dans le catalogue data.gouv.fr")
   @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "La recherche s'est exécutée. Note : elle peut avoir retourné 0 résultat."),
      @ApiResponse(responseCode = "403", description = "- Si l'exécution de méthodes REST renvoyant des données volumineuses n'est pas autorisée.\n" +
         "- Si les données raffinées requises pour l'exécution de cette méthode, avec ces paramètres, n'ont pas été générées, et que l'application n'a pas le droit de les produire."),
      @ApiResponse(responseCode = "500", description = "Un incident est survenu durant la requête.")
   })
   public List<JeuDeDonneesElastic> recherche(
      @Parameter(name = "searchText", description = "mots à rechercher dans le catalogue data.gouv.fr<br>" +
         "le format de query est celui d'Elastic", required = true,

         examples = {
            @ExampleObject(value = "quimper", name = "quimper", summary = "sur la ville de Quimper",
               description = "Ce qui a trait à la ville de Quimper"),

            @ExampleObject(value = "+quimper +navettes", name = "quimper_navettes", summary = "quimper ET navettes",
               description = "Recherche des mots quimper ET navettes")
         })
      String searchText,

      @RequestParam(required = false)
      @Parameter(name = "field", description = "Champ optionnel dans lequel faire la recherche")
      String field)
   {
      assertGenerationAutorisee(true);
      return this.cataloguePort.recherche(searchText, field);
   }

   /**
    * {@inheritDoc}
    */
   @GetMapping(value = "/enumererValeursDistinctesChampDataGouvFr")
   @Operation(summary = "Enumérer les valeurs distinctes d'un champ du catalogue data.gouv.fr",
      description = "Les champs <code>frequence</code>, <code>licence</code>, <code>granularité spatiale</code>, <code>zones spatiales</code>, <code>tags (étiquettes)</code> " +
         "du catalogue <a href='https://www.data.gouv.fr/fr/datasets/catalogue-des-donnees-de-data-gouv-fr/'>data.gouv.fr</a> " +
         "contiennent des valeurs énumérées qui ne sont pas documentées par des métadonnées.<br>" +
         "Cette opération permet de retrouver les valeurs distinctes qu'elles peuvent prendre.")
   @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "La liste des valeurs distinctes pour ce champ a été obtenues, sous la forme de <code>(valeur distincte, nombre d'occurrences)</code>, en ordre décroissant d'occurrences"),
      @ApiResponse(responseCode = "403", description = "- Si l'exécution de méthodes REST renvoyant des données volumineuses n'est pas autorisée.\n" +
         "- Si les données raffinées requises pour l'exécution de cette méthode, avec ces paramètres, n'ont pas été générées, et que l'application n'a pas le droit de les produire."),
      @ApiResponse(responseCode = "500", description = "Un incident est survenu durant la requête.")
   })
   public Map<String, Long> enumererValeursDistinctes(
      @Parameter(name = "champ", description = "Nom du champ à valeurs énumérées du catalogue data.gouv.fr",
         required = true,

         examples = {
            @ExampleObject(value = "frequence_catalogue", name="Fréquence de parution", summary = "la fréquence de parution du jeu de données",
               description = "Une année du code officiel géographique<br>"),

            @ExampleObject(value = "licence_catalogue", name="Licence", summary = "Licence",
               description = "Une année du code officiel géographique<br>"),

            @ExampleObject(value = "granularite_spatiale_catalogue", name="Granularite", summary = "Granularité spatiale",
               description = "Une année du code officiel géographique<br>"),

            @ExampleObject(value = "zones_spatiales_catalogue", name="Zones spatiales", summary = "Zones spatiales",
               description = "Une année du code officiel géographique<br>"),

            @ExampleObject(value = "tags_catalogue", name="Etiquettes", summary = "Tags (étiquettes)",
               description = "Une année du code officiel géographique<br>")
         }
      )
      String champ) {
      return this.cataloguePort.enumererValeursDistinctes(champ);
   }
}
