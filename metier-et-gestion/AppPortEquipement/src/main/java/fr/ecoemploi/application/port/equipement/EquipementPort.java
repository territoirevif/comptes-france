package fr.ecoemploi.application.port.equipement;

import java.io.Serializable;

/**
 * Actions possibles sur les équipements
 * @author Marc Le Bihan
 */
public interface EquipementPort extends Serializable {
   /**
    * Charger la base équipements.
    * @param anneeEquipement Année de la base équipement à charger.<br>
    * 0 si toutes les années connues doivent l'être (à partir de 2018 pour la base équipement).
    */
   void chargerEquipements(int anneeEquipement);
}
