/**
 * Application Port de la base équipement
 */
module fr.ecoemploi.application.port.equipement {
   requires fr.ecoemploi.domain.model;

   exports fr.ecoemploi.application.port.equipement;
}
