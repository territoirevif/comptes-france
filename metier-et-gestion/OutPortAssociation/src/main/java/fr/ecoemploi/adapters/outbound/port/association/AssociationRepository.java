package fr.ecoemploi.adapters.outbound.port.association;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.ecoemploi.domain.model.territoire.CodeCommune;
import fr.ecoemploi.domain.model.territoire.association.*;

/**
 * Repository sur les associations.
 */
@Repository
public interface AssociationRepository extends Serializable {
   /**
    * Charger le ou les référentiels des associations.
    * @param anneeRNA Année du Registre National des Associations<br>
    *     0: pour toutes les années disponibles
    * @param anneeCOG Année du Code Officiel Géographique.
    */
   void chargerAssociations(int anneeRNA, int anneeCOG);

   /**
    * Obtenir la liste des associations d'une commune.
    * @param anneeRNA Année du Registre National des Associations
    * @param anneeCOG Année du Code Officiel Géographique.
    * @param codeCommune Code Commune.
    * @return Liste des associations.
    */
   List<Association> obtenirAssociations(int anneeRNA, int anneeCOG, CodeCommune codeCommune);
}
