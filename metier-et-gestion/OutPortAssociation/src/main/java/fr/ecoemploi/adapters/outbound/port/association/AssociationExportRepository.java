package fr.ecoemploi.adapters.outbound.port.association;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import fr.ecoemploi.domain.model.territoire.association.*;

/**
 * Repository d'export d'associations.
 */
@Repository
public interface AssociationExportRepository extends Serializable {
   /**
    * Exporter les associations touristique présentes en France.
    * @param anneeRNA Année de référence des associations.
    * @param anneeCOG Année du COG à considérer.
    * @param themeObjetSocial Thème de l'objet social associatif.
    * @return Fichier d'exportation CSV des associations.
    */
   String exporterAssociationsCSV(ThemeObjetSocial themeObjetSocial, int anneeRNA, int anneeCOG);
}
