/**
 * Outbound Port des Associations
 */
module fr.ecoemploi.outbound.port.association {
   requires fr.ecoemploi.domain.model;
   requires spring.context;

   exports fr.ecoemploi.adapters.outbound.port.association;
}
