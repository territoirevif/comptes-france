/**
 * Outbound Port de l'emploi et de la formation
 */
module fr.ecoemploi.outbound.port.emploi {
   requires fr.ecoemploi.domain.model;
   requires spring.context;

   exports fr.ecoemploi.adapters.outbound.port.emploi;
}
