Instructions de compilation
===========================

Pour réussir une compilation, il faut que _Kafka_ ait été lancé.  
Par exemple, par: `./kafka-dev.sh`

## 1) Les profils de compilation Maven

### Par défaut

Construction normale.

  - lance les tests unitaires suffixés par _Test_.

### Assemblage

`assemblage` lance les tests d'assemblage suffixés par `_IT`

- Lancer un unique test d'intégration par Maven :

  `mvn clean install -Dit.test=ActivitesCommuneIT#datasetSalaries* -DfailIfNoTests=false -Passemblage`

### Avec couverture de code (Sonarcube)

- Lance les tests avec sonar et couverture de code :

  `mvn integration-test verify sonar:sonar -Passemblage,coverage -Dmaven.test.failure.ignore=true -Dsonar.projectKey=ecoemploi -Dsonar.projectName='ecoemploi' -Dsonar.host.url=http://localhost:9000 -Dsonar.token=sqp_0d82694f5f8a3be4f9a6d2455d33fd4c7ebb3a52 -fn`

## 2) Services REST

- [API de génération OpenAPI](http://localhost:9090/v3/api-docs)  
- [Documentation services REST](http://localhost:9090/swagger-ui.html)  

## 3) Heap dump pour Memory Analysis

   1. Récupérer le pid du process à débogguer par `jps -l`

   2. Créer un dump par `jmap -dump:format=b,file=heap.hprof <pid>`
 `jvisualvm -J-Xms1024m -J-Xmx5g`
