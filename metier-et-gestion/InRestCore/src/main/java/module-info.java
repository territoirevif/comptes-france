/**
 * Inbound Rest core
 */
module fr.ecoemploi.inbound.rest.core {
   requires fr.ecoemploi.domain.utils;
   requires fr.ecoemploi.domain.model;
   requires fr.ecoemploi.postgis;

   // requires jakarta.ws.rs;
   requires java.ws.rs;
   requires spring.beans;
   requires spring.web;
   requires io.swagger.v3.oas.annotations;
   requires spring.context;
   requires org.slf4j;

   exports fr.ecoemploi.adapters.inbound.rest.core;
}
