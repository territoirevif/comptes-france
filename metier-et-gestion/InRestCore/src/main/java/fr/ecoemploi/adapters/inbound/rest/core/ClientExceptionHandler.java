package fr.ecoemploi.adapters.inbound.rest.core;

import java.nio.charset.*;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import fr.ecoemploi.domain.utils.objets.GenerationDonneesNonAutoriseeException;

/**
 * Réaction à des exceptions types
 * @author Marc Le Bihan
 */
@ControllerAdvice
public class ClientExceptionHandler {
    /**
     * Génération de données non autorisée => FORBIDDEN
     * @param e Exception levée
     * @param request Requête
     * @return entité portant le refus
     */
    @ExceptionHandler(value = GenerationDonneesNonAutoriseeException.class)
    public ResponseEntity<String> donneesNonGenerables(GenerationDonneesNonAutoriseeException e, @SuppressWarnings("unused") WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("text", "html", StandardCharsets.UTF_8);
        headers.setContentType(mediaType);
        headers.set("Accept-Language", "fr");

        return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.FORBIDDEN);
    }
}
