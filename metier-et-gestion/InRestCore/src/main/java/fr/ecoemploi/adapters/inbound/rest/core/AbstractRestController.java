package fr.ecoemploi.adapters.inbound.rest.core;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import fr.ecoemploi.domain.utils.objets.GenerationDonneesNonAutoriseeException;

import static fr.ecoemploi.domain.utils.console.ConsoleEscapeCodes.BYELLOW;

/**
 * Contrôleur de base des services REST
 * @author Marc Le Bihan
 */
@Component
public abstract class AbstractRestController {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRestController.class);

   /** Indique si la génération de données (Apache Parquet ou autre) est autorisée */
   @Value("${generation.donnees.autorise:true}")
   public boolean generationDonneesAutorisee;

   /** Indique si l'émission de données volumineuses est autorisée */
   @Value("${emission.donnees.volumineuses.autorise:true}")
   public boolean emissionDonneesVolumineusesAutorisee;

   /**
    * Construire un service REST
    */
   protected AbstractRestController() {
   }

   /**
    * Indique si la génération de données (Apache Parquet ou autre) est autorisée.
    * @return <code>false</code> si seuls les fichiers de cache déjà présents peuvent être exploités.
    */
   public boolean isGenerationDonneesAutorisee() {
      return this.generationDonneesAutorisee;
   }

   /**
    * Indique si l'émission de données volumineuses est autorisée.
    * @return <code>true</code> si c'est le cas.
    */
   public boolean isEmissionDonneesVolumineusesAutorisee() {
      return this.emissionDonneesVolumineusesAutorisee;
   }

   /**
    * Vérifier que la génération de données est autorisée.
    * @param donneesVolumineuses <code>true</code> s'il est suspecté que cette méthode puisse renvoyer des données volumineuses
    * @throws GenerationDonneesNonAutoriseeException si cet appel n'est pas permis
    */
   public void assertGenerationAutorisee(boolean donneesVolumineuses) {
      // Vérifier que nous sommes autorisés à émettre des données volumineuses
      if (isEmissionDonneesVolumineusesAutorisee() == false && donneesVolumineuses) {
         String message = "L'application n'est pas autorisée à exécuter par appel REST cette méthode qui peut ramener des données volumieuses.";
         LOGGER.warn(BYELLOW.color(message));

         throw new GenerationDonneesNonAutoriseeException(message);
      }
   }

   /**
    * Refuser la demande si la génération de données est interdite.
    * @throws GenerationDonneesNonAutoriseeException si cet appel n'est pas permis
    */
   public void refusSiGenerationInterdite() {
      // Vérifier que nous sommes autorisés à émettre des données volumineuses
      if (isGenerationDonneesAutorisee() == false) {
         String message = "L'application n'est pas autorisée à exécuter par appel REST cette méthode qui provoque une grande génération de données.";
         LOGGER.warn(BYELLOW.color(message));

         throw new GenerationDonneesNonAutoriseeException(message);
      }
   }
}
