package fr.ecoemploi.adapters.outbound.spark.dataset.competence;

import org.junit.jupiter.api.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.cog.SparkCogTestApplication;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.groupement.*;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Test du dataset des compétences de groupements ou communales
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkCogTestApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CompetenceITCase {
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CompetenceITCase.class);

   /** Dataset des compétences de groupements ou communales */
   @Autowired
   private CompetenceDatatset competenceDatatset;

   /** Dataset des groupements et périmètres */
   @Autowired
   private EPCIPerimetreDataset perimetreDataset;

   /** Année du cog de départ. */
   @Value("${annee.cog.debut}")
   private int anneeCogDebut;

   /** Année du cog de fin. */
   @Value("${annee.cog.fin}")
   private int anneeCogFin;

   /**
    * Lecture du fichier des compétences.
    */
   @DisplayName("Constitution du dataset des compétences")
   @Test
   @Order(10)
   void competences() {
      OptionsCreationLecture options = this.competenceDatatset.optionsCreationLecture();

      LOGGER.info("Lecture des compétences de groupements ou communales");
      Dataset<Row> competences = this.competenceDatatset.rowCompetences(options, new HistoriqueExecution(), 2024, true, true, new CompetenceTriCode());

      long count = competences.count();
      assertNotEquals(0, competences.count(), "Plusieurs compétences auraient du être lues.");
      competences.show(1000, false);
      LOGGER.info("{} compétences de groupements ou communales ont été lues.", count);
   }

   @DisplayName("Taux d'utilisation des compétences de groupements ou périmètres")
   @Test
   @Order(20)
   void tauxUtilisationDesCompetences() {
      for(int annee = this.anneeCogFin; annee >= this.anneeCogDebut; annee--) {
         LOGGER.info("Année {}", annee);

         OptionsCreationLecture options = this.perimetreDataset.optionsCreationLecture();
         Dataset<Row> perimetres = this.perimetreDataset.rowPerimetres(options, new HistoriqueExecution(), annee, false, new EPCIPerimetreTriSirenGroupementCommuneMembreSirenMembre());

         Dataset<Row> competencesNonUtilisees = this.competenceDatatset.tauxUtilisationDesCompetences(annee, perimetres);
         competencesNonUtilisees.show(1000, false);
         assertNotEquals(0L, competencesNonUtilisees.count(), "Au moins un taux de compétence aurait du être extrait");
      }
   }
}
