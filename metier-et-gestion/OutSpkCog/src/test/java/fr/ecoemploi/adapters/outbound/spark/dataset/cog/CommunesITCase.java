package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import static org.apache.spark.sql.functions.col;
import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.groupement.RestrictionsArgumentProvider;
import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;

/**
 * Test sur la reconstitution du maillage du territoire.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkCogTestApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CommunesITCase extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5878582886504165631L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CommunesITCase.class);
   
   /** Dataset du Code Offciel Géographique. */
   @Autowired
   private CogDataset cogDataset;

   /** Dataset des départements. */
   @Autowired
   private DepartementDataset departementDataset;

   /** Année du cog de départ pour les tests. */
   @Value("${test.annee.cog.debut:2024}")
   private int cogAnneeDebut;

   /** Année du cog de fin pour les tests. */
   @Value("${test.annee.cog.fin:2024}")
   private int cogAnneeFin;

   /**
    * Obtention des Dataset de row communes.
    */
   @DisplayName("Row communes")
   @ParameterizedTest(name = "Communes {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   @Order(10)
   void rowCommune(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.cogDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      LOGGER.info("Restriction : {} → {}, {}", typeRestriction, communes, annee);
      rowCommunes(options, true);
   }

   /**
    * Obtention des Dataset de row communes.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *        false si seules les communes de type COM sont lues.
    */
   private void rowCommunes(OptionsCreationLecture options, boolean inclureCommunesDelegueesAssociees) {
      for(int cog = this.cogAnneeDebut; cog <= this.cogAnneeFin; cog ++) {
         if (toutesAnnees() == false && cog != this.cogAnneeFin) {
            continue;
         }

         HistoriqueExecution historique = new HistoriqueExecution();

         LOGGER.info("Lecture du COG communes en Row {}, communes déléguées ou associées {}...", cog, inclureCommunesDelegueesAssociees);

         // Vérifier la présence d'au moins une commune.
         Dataset<Row> communes = this.cogDataset.rowCommunes(options, historique, cog, inclureCommunesDelegueesAssociees /*, COMPTAGES_ET_STATISTIQUES, SHOW_SCHEMAS, SHOW_DATASET, SHOW_REJETS */);
         assertNotEquals(0, communes.count(), MessageFormat.format("Plusieurs communes auraient du être lues pour le COG communes {0}.", cog));
         LOGGER.info("En {}, il y a {} communes Row.", cog, communes.count());
         communes.show(false);

         communes.printSchema();
      }
   }

   /**
    * Obtention des Dataset en Communes.
    */
   @DisplayName("Objets Commune")
   @ParameterizedTest(name = "Communes {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   @Order(20)
   void communes(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.cogDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      LOGGER.info("Restriction : {} → {}, {}", typeRestriction, communes, annee);

      HistoriqueExecution historique = new HistoriqueExecution();

      communes(options, historique, false);
      communes(options, historique, true);
   }

   /**
    * Obtention des Dataset en Communes.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *        false si seules les communes de type COM sont lues.
    */
   private void communes(OptionsCreationLecture options, HistoriqueExecution historique, boolean inclureCommunesDelegueesAssociees) {
      for(int cog = this.cogAnneeDebut; cog <= this.cogAnneeFin; cog ++) {
         if (toutesAnnees() == false && cog != this.cogAnneeFin) {
            continue;
         }

         LOGGER.info("Lecture du COG communes en objets {}, communes déléguées ou associées {}...", cog, inclureCommunesDelegueesAssociees);

         Dataset<Commune> datasetCommunes = this.cogDataset.obtenirCommunes(options, historique, cog, inclureCommunesDelegueesAssociees /*, COMPTAGES_ET_STATISTIQUES, SHOW_SCHEMAS, SHOW_DATASET, SHOW_REJETS */);
         assertNotEquals(0, datasetCommunes.count(), MessageFormat.format("Plusieurs communes auraient du être lues pour le COG communes {0}.", cog));
         LOGGER.info("En {}, il y a objets {} communes.", cog, datasetCommunes.count());
         dump(datasetCommunes, 20, LOGGER);

         List<Commune> communes = datasetCommunes.collectAsList();

         // Chercher un arrondissement.
         if (cog >= 2019 && options.hasRestrictionAuxCommunes() == false) {
            // La détection des arrondissements n'est pas détectée dans les anciens flux.
            Commune huitiemeArrondissementParis = communes.stream().filter(c -> "75108".equals(c.getCodeCommune())).findAny().orElse(null);
            assertNotNull(huitiemeArrondissementParis, MessageFormat.format("Le 8ème arrondissement de Paris aurait dû être trouvé dans le COG {0}.", cog));
            LOGGER.info(huitiemeArrondissementParis.toString());
         }

         // Vérification de la présence d'une commune en particulier.
         Commune brest = communes.stream().filter(c -> "29019".equals(c.getCodeCommune())).findAny().orElse(null);
         assertNotNull(brest, MessageFormat.format("Brest aurait dû être trouvé dans le COG {0}.", cog));

         // Aucune commune ne doit avoir une population nulle. Elle doit être à 0, au pire.
         datasetCommunes.foreach(commune -> {
            assertNotNull(commune.getPopulation(), MessageFormat.format("La population de la commune {0} ({1}) ne devrait pas être nulle.", commune.getCodeCommune(), commune.getNomCommune()));
         });
      }
   }

   /**
    * Charger le Code Officiel Géographique entier.
    * @throws CommuneInexistanteException si la commune recherchée n'existe pas.
    */
   @DisplayName("COG sérialisé, COM seulement")
   @ParameterizedTest(name = "Communes {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   @Order(30)
   void cog_com(String typeRestriction, Set<String> communes, Integer annee) throws CommuneInexistanteException {
      OptionsCreationLecture options = this.cogDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      LOGGER.info("Restriction : {} → {}, {}", typeRestriction, communes, annee);

      HistoriqueExecution historique = new HistoriqueExecution();

      cog(options, historique, false);
   }

   /**
    * Charger le Code Officiel Géographique entier.
    * @throws CommuneInexistanteException si la commune recherchée n'existe pas.
    */
   @DisplayName("COG sérialisé, toutes communes dont COMD, COMA")
   @ParameterizedTest(name = "Communes {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   @Order(30)
   void cog_com_comd_coma(String typeRestriction, Set<String> communes, Integer annee) throws CommuneInexistanteException {
      OptionsCreationLecture options = this.cogDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      LOGGER.info("Restriction : {} → {}, {}", typeRestriction, communes, annee);

      HistoriqueExecution historique = new HistoriqueExecution();

      cog(options, historique, true);
   }

   /**
    * Charger le Code Officiel Géographique entier.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *        false si seules les communes de type COM sont lues.
    * @throws CommuneInexistanteException si la commune recherchée n'existe pas.
    */
   private void cog(OptionsCreationLecture options, HistoriqueExecution historique, boolean inclureCommunesDelegueesAssociees) throws CommuneInexistanteException {
      for(int cog = this.cogAnneeDebut; cog <= this.cogAnneeFin; cog ++) {
         if (toutesAnnees() == false && cog != this.cogAnneeFin) {
            continue;
         }

         LOGGER.info("Lecture du COG communes sérialisé {}, communes déléguées et associées {}...", cog, inclureCommunesDelegueesAssociees);

         // Vérifier qu'une commune au moins est lue
         CodeOfficielGeographique maillage = this.cogDataset.obtenirCodeOfficielGeographique(cog, inclureCommunesDelegueesAssociees);
         LOGGER.info("Code Officiel Géographique : {} communes, {} intercommunalités.", maillage.getCommunes().size(), maillage.getIntercommunalites().size());

         // Lire une intercommunalité
         Intercommunalite intercommunalite = maillage.getIntercommunalites().values().iterator().next();
         LOGGER.info("La première intercommunalité : {}", intercommunalite);

         // Vérifier la présence d'une commune
         if (options.hasRestrictionAuxCommunes() == false) {
            Commune cambremer = maillage.getCommune("14126");
            assertNotNull(cambremer, MessageFormat.format("Cambremer aurait dû être trouvé dans le COG {0}.", cog));
            LOGGER.info(cambremer.toString());

            // Vérifier que ce maillage et la liste des communes par département concorde
            maillageEtCommunes(options, historique, maillage, inclureCommunesDelegueesAssociees, cog);
         }
      }
   }

   /**
    * Comparer le maillage lu sous forme sérialisée, issu de la base géographique, avec la liste des communes venue du COG.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *        false si seules les communes de type COM sont lues.
    * @param maillage Maillage à comparer.
    * @param cog année du COG
    */
   private void maillageEtCommunes(OptionsCreationLecture options, HistoriqueExecution historique, CodeOfficielGeographique maillage, boolean inclureCommunesDelegueesAssociees, int cog) {
      LOGGER.info("Comparaison du nombre de communes du Dataset objet {} {} avec sa version sérialisée pour chaque département... ", cog, inclureCommunesDelegueesAssociees);
      List<Row> rowDepartements = this.departementDataset.rowDepartements(options, historique, cog).collectAsList();
      Dataset<Commune> datasetCommunes = this.cogDataset.obtenirCommunes(options, historique, cog, inclureCommunesDelegueesAssociees).where(col("typeCommune").notEqual("ARM"));

      for(Row rowDepartement : rowDepartements) {
         String departement = rowDepartement.getAs("codeDepartement");

         List<Commune> communes = datasetCommunes.where(col("codeDepartement").equalTo(departement)).collectAsList();
         List<Commune> maillageCommunesDansDepartement = maillage.getCommunes().stream().filter(commune -> commune.getCodeDepartement().equals(departement)).toList();

         // S'assurer que les communes sont triées dans l'ordre
         assertOrdreCodeCommunes(communes, cog);
         assertOrdreCodeCommunes(maillageCommunesDansDepartement, cog);

         differencesCommunesEtMaillage(communes, maillageCommunesDansDepartement, cog, departement, inclureCommunesDelegueesAssociees);
      }
   }

   /**
    * Vérifier l'ordre et la non duplication des codes communes.
    * @param communes Liste des communes
    * @param cog année du COG
    */
   private void assertOrdreCodeCommunes(List<Commune> communes, int cog) {
      // S'assurer que les communes sont triées dans l'ordre
      for(int index=1; index < communes.size(); index ++) {
         Commune precedente = communes.get(index-1);
         Commune actuelle = communes.get(index);

         // Soit l'ordre des codes communes est strictement décroissant
         boolean ordreRespecte = precedente.getCodeCommune().compareTo(actuelle.getCodeCommune()) < 0;

         // Soit les codes communes sont égaux, mais les types de communes ne sont pas les mêmes
         boolean typesDeCommunesDifferents = precedente.getTypeCommune().equals(actuelle.getTypeCommune()) == false
            && precedente.getCodeCommune().compareTo(actuelle.getCodeCommune()) == 0;

         if ((ordreRespecte || typesDeCommunesDifferents) == false) {
            List<Commune> fautives = communes.stream().filter(c -> c.getCodeCommune().equals(actuelle.getCodeCommune())).toList();
            LOGGER.error(fautives.toString());
         }

         // Avant 2017, des communes pouvaient ne pas avoir d'intercommunalité.
         boolean sansEPCI = actuelle.getNatureJuridiqueEPCI() == null || precedente.getNatureJuridiqueEPCI() == null;
         boolean eptMetro = false;

         if (sansEPCI == false) {
            // le couple Métropole et EPT est le seul admis
            eptMetro = (precedente.getNatureJuridiqueEPCI().equals(NatureJuridiqueIntercommunalite.METROPOLE.getCode())
                    && actuelle.getNatureJuridiqueEPCI().equals(NatureJuridiqueIntercommunalite.ETABLISSEMENT_PUBLIC_TERRITORIAL.getCode()))
               || (precedente.getNatureJuridiqueEPCI().equals(NatureJuridiqueIntercommunalite.ETABLISSEMENT_PUBLIC_TERRITORIAL.getCode())
                    && actuelle.getNatureJuridiqueEPCI().equals(NatureJuridiqueIntercommunalite.METROPOLE.getCode()));
         }

         assertTrue(ordreRespecte || typesDeCommunesDifferents || eptMetro,
            MessageFormat.format("dans les communes du dataset {6,number,#0}, le code commune {0} ({1}, {2}, {7}, {8}) " +
                  "devrait être inférieur à celui {3} ({4}, {5}, {9}, {10}), qui vient après",
               precedente.getCodeCommune(), precedente.getNomCommune(), precedente.getTypeCommune(),
               actuelle.getCodeCommune(), actuelle.getNomCommune(), actuelle.getTypeCommune(), cog,
               precedente.getNomEPCI(), precedente.getNatureJuridiqueEPCI(), actuelle.getNomEPCI(), actuelle.getNatureJuridiqueEPCI()));
      }
   }

   /**
    * Enumérer les différences entre les deux jeux de communes.
    * @param communes Communes du COG
    * @param maillageCommunesDansDepartement Communes du maillage géographique
    * @param cog Année du COG
    * @param departement Code département
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *        false si seules les communes de type COM sont lues.
    */
   private void differencesCommunesEtMaillage(List<Commune> communes, List<Commune> maillageCommunesDansDepartement, int cog, String departement, boolean inclureCommunesDelegueesAssociees) {
      long a = communes.size();
      long b = maillageCommunesDansDepartement.size();

      if (a != b) {
         int compte = 0;
         Iterator<Commune> itA = communes.iterator();
         Iterator<Commune> itB = maillageCommunesDansDepartement.iterator();

         boolean standStill = false;
         Commune communeA = null, communeB = null;

         do {
            if (standStill == false) {
               communeA = itA.next();
               communeB = itB.next();
               compte ++;
            }

            if (communeA.getCodeCommune().equals(communeB.getCodeCommune()) == false) {
               boolean memeTypes = communeA.getTypeCommune().equals(communeB.getTypeCommune());

               LOGGER.warn("à la {} commune comparée du département {} :", compte, departement);

               if (memeTypes) {
                  LOGGER.error("Commune du dataset  {}: {} - {} ({})", cog, communeA.getCodeCommune(), communeA.getNomCommune(), communeA.getTypeCommune());
                  LOGGER.error("Commune du maillage {}: {} - {} ({})", cog, communeB.getCodeCommune(), communeB.getNomCommune(), communeB.getTypeCommune());
               }
               else {
                  LOGGER.warn("Commune du dataset  {}: {} - {} ({})", cog, communeA.getCodeCommune(), communeA.getNomCommune(), communeA.getTypeCommune());
                  LOGGER.warn("Commune du maillage {}: {} - {} ({})", cog, communeB.getCodeCommune(), communeB.getNomCommune(), communeB.getTypeCommune());
               }

               standStill = true;

               if (communeA.getCodeCommune().compareTo(communeB.getCodeCommune()) < 0) {
                  while(itA.hasNext() && communeA.getCodeCommune().compareTo(communeB.getCodeCommune()) < 0)
                     communeA = itA.hasNext() ? itA.next() : communeA;
               }
               else {
                  if (itB.hasNext() && communeA.getCodeCommune().compareTo(communeB.getCodeCommune()) > 0) {
                     while (itB.hasNext() && communeA.getCodeCommune().compareTo(communeB.getCodeCommune()) > 0)
                        communeB = itB.hasNext() ? itB.next() : communeB;
                  }
               }
            }
            else {
               standStill = false;
            }
         }
         while(itA.hasNext() && itB.hasNext());
      }

      // Les communes déléguées et associées ne sont pas présentes dans la couche géographique lue
      // Mais pour le reste, les communes de type COM, doivent toutes être retrouvées dans la couche géographique.
      if (inclureCommunesDelegueesAssociees == false) {
         assertToutLeDatasetCOMDansLeMaillage(communes, maillageCommunesDansDepartement, cog, departement);
      }
   }

   /**
    * S'assurer que toutes les communes de type "COM" du dataset sont présentes dans la représentation géographique (maillage)
    * @param communes Dataset des communes
    * @param maillageCommunesDansDepartement Représentation géographique des communes
    * @param cog Année du COG
    * @param departement Code département
    */
   private void assertToutLeDatasetCOMDansLeMaillage(List<Commune> communes, List<Commune> maillageCommunesDansDepartement, int cog, String departement) {
      Set<String> codesCommunesDataset = communes.stream().map(Commune::getCodeCommune).collect(Collectors.toSet());
      Set<String> codesCommunesMaillage = maillageCommunesDansDepartement.stream().map(Commune::getCodeCommune).collect(Collectors.toSet());

      for(String codeCommuneDuDataset : codesCommunesDataset) {
         if (codesCommunesMaillage.contains(codeCommuneDuDataset) == false) {
            LOGGER.error("Dans le département {} en {}, le code commune du Dataset (issu du COG) {} n'a pas été retrouvé dans la couche géographique.",
               departement, cog, codeCommuneDuDataset);
         }

         /* assertTrue(codesCommunesMaillage.contains(codeCommuneDuDataset),
            MessageFormat.format("Dans le département {0} en {1,number,#0}, le code commune du Dataset (issu du COG) {2} n''a pas été retrouvé dans la couche géographique.",
               departement, cog, codeCommuneDuDataset)); */
      }
   }
}
