package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;

import org.junit.jupiter.api.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.SIREN_COMMUNE;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.groupement.RestrictionsArgumentProvider;

/**
 * Tests sur les SIREN communaux.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkCogTestApplication.class)
class SirenCommunauxITCase extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 4266946344884233962L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(SirenCommunauxITCase.class);

   /** Dataset des SIREN communaux. */
   @Autowired
   private SirenCommunesDataset sirenCommunaux;

   /** Année sirene de départ pour les tests. */
   @Value("${test.annee.siren.debut:2016}")
   private int sireneAnneeDebut;

   /** Année sirene de fin pour les tests. */
   @Value("${test.annee.siren.fin:2024}")
   private int sireneAnneeFin;

   /**
    * Lecture du fichier des SIREN communaux.
    */
   @DisplayName("Dataset du fichier des SIREN communaux.")
   @ParameterizedTest(name = "Siren communaux {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   void sirenCommunaux(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.sirenCommunaux.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      for(int annneCog = this.sireneAnneeDebut; annneCog <= this.sireneAnneeFin; annneCog ++) {
         if (toutesAnnees() == false && annneCog != this.sireneAnneeFin) {
            continue;
         }

         LOGGER.info("Lecture des siren communaux pour l'année {}, restriction : {} → {}, {}", annneCog, typeRestriction, communes, annee);

         sirenCommunaux(options, annneCog, new SirenCommunesTriSiren());
         sirenCommunaux(options, annneCog, new SirenCommunesTriDepartementCommuneSiren());
      }
   }

   /**
    * Constitution et relecture du fichier des siren communaux pour une année et un tri.
    * @param annneCog Année du COG
    * @param tri Tri choisi
    */
   private void sirenCommunaux(OptionsCreationLecture options, int annneCog, SirenCommunesTri tri) {
      LOGGER.info("***************************");
      LOGGER.info("* TEST DATASET ANNEE {} *", annneCog);
      LOGGER.info("***************************");

      Dataset<Row> sirens = this.sirenCommunaux.rowSirenCommunes(options, new HistoriqueExecution(), tri, annneCog);
      chrono("Chrono création store" , LOGGER);

      assertNotEquals(0, sirens.count(), "Plusieurs siren communaux auraient du être lus.");
      chrono("Chrono vérification", LOGGER);

      testShow(sirens, showTake(), explainPlan(), false, false, LOGGER);
      chrono("Chrono show/explain", LOGGER);

      if (options.hasRestrictionAuxCommunes() == false) {
         assertRelecture(sirens, RELECTURE_PAR_SIREN);
         assertRelecture(sirens, RELECTURE_PAR_DEPARTEMENT_COMMUNE_SIREN);
      }
   }

   private static final int RELECTURE_PAR_SIREN = 0;
   private static final int RELECTURE_PAR_DEPARTEMENT_COMMUNE_SIREN = 1;

   /**
    * Relecture avec vérification d'un dataset de sirens communaux.
    * @param sirensCommunaux Dataset de sirens communaux.
    * @param choixRelecture Choix de relecture.
    */
   private <T> void assertRelecture(Dataset<T> sirensCommunaux, int choixRelecture) {
      // Relecture avec Château-Thierry (Aisne (02), code commune 02168)
      String[] sirens = {"210201554"};
      String[] departements = {"02"};
      String[] codesCommunes = {"02168"};

      Column whereClause = switch(choixRelecture) {
         case RELECTURE_PAR_SIREN -> new SirenCommunesTriSiren().equalTo(sirens[0]);

         case RELECTURE_PAR_DEPARTEMENT_COMMUNE_SIREN -> new SirenCommunesTriDepartementCommuneSiren()
            .equalTo(departements[0], codesCommunes[0], sirens[0]);

         default -> throw new UnsupportedOperationException(MessageFormat.format("Il n''existe pas de relecture avec le choix {0}", choixRelecture));
      };

      LOGGER.info("Lecture des siren communaux {} avec la requête : '{}'", sirens[0], whereClause);
      Dataset<T> lecture = sirensCommunaux.where(whereClause);
      List<Row> rowsLus = lecture.toDF().collectAsList();
      chrono("Chrono lecture", LOGGER);

      testShow(lecture, showTake(), explainPlan(), false, false, LOGGER);
      chrono("Chrono show/explain", LOGGER);

      assertEquals(sirens.length, rowsLus.size(), "En relecture sur des sirets de test, il n'y a pas autant d'établissements que demandés");

      Set<String> resultats = Set.of((String)rowsLus.get(0).getAs(SIREN_COMMUNE.champ()));
      Arrays.stream(sirens).forEach(siren -> assertTrue(resultats.contains(siren), "Un des siren demandé n'est pas parmi les résultats"));
   }

   /**
    * Avant chaque test
    */
   @BeforeEach
   void beforeEach() {
      super.chrono.start();
   }
}
