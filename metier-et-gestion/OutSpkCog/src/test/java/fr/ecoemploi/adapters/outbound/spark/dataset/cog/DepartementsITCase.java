package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.text.*;

import org.apache.spark.sql.*;
import org.junit.jupiter.api.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

/**
 * Tests d'intégration sur les départements. 
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkCogTestApplication.class)
class DepartementsITCase extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -1090665134571323566L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(DepartementsITCase.class);

   /** Dataset des départements. */
   @Autowired
   private DepartementDataset departementDataset;

   /** Année du cog de départ pour les tests. */
   private final static int COG_START = 2016;
   
   /** Année du cog de fin pour les tests. */
   private final static int COG_END = 2024;

   /**
    * Désactiver les caches des datasets.
    */
   @BeforeEach
   public void desactiverCaches() {
      // this.departementDataset.loader().setCache(activerCache());
   }

   /**
    * Obtention des Dataset de row départements.
    */
   @Test
   @DisplayName("Dataset départements (row).")
   void rowDepartements() {
      for(int cog = COG_START; cog <= COG_END; cog ++) {
         if (toutesAnnees() == false && cog != COG_END) {
            continue;
         }

         OptionsCreationLecture options = this.departementDataset.optionsCreationLecture();
         
         LOGGER.info("Lecture du COG départements {} : ", cog);
         
         Dataset<Row> departements = this.departementDataset.rowDepartements(options, new HistoriqueExecution(), cog /*, COMPTAGES_ET_STATISTIQUES, SHOW_SCHEMAS, SHOW_DATASET, SHOW_REJETS */);
         
         if (useExplainPlan()) {
            departements.explain(explainPlan());
         }
         
         assertNotEquals(0, departements.count(), MessageFormat.format("Plusieurs communes auraient du être lues pour le COG départements {0}.", cog));
         departements.show(200, false);

         super.assertFields(departements.schema(), this.departementDataset.verificateur(),
            new String[] { "codeDepartement", "codeRegionDepartement", "codeCommuneChefLieuDepartement", "typeNomEtCharniereDepartement", "nomMajusculesDepartement",
               "nomDepartement", "libelleDepartement"}, true);

         LOGGER.info("{} communes existent en {}.", departements.count(), cog);
      }
   }
}
