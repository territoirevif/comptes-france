package fr.ecoemploi.adapters.outbound.spark.dataset.groupement;

import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.*;

import static org.junit.jupiter.params.provider.Arguments.arguments;

/**
 * Compétences obligatoires ou obsolètes demandées lors des tests
 * @author Marc Le Bihan
 */
public class CompetencesObligatoiresOuObsoletesArgumentProvider implements ArgumentsProvider {
   @Override
   public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
      return Stream.of(
         arguments("Communautés de Communes", new String[] {"CC"}, false, 2024),
         arguments("Communautés de Communes", new String[] {"CC"}, true, 2024),
         arguments("Communautés d'Aglomération", new String[] {"CA"}, false, 2024),
         arguments("Communautés d'Aglomération", new String[] {"CA"}, true, 2024)
      );
   }
}
