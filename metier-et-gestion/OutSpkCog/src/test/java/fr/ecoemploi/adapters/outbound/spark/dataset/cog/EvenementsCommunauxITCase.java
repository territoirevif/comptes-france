package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.*;
import java.time.LocalDate;
import java.util.List;

import org.apache.spark.sql.*;
import org.slf4j.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.domain.model.territoire.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests d'interprétation des évènements communaux.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkCogTestApplication.class)
class EvenementsCommunauxITCase extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 5878582886504165631L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EvenementsCommunauxITCase.class);
   
   /** Dataset des évènements communaux. */
   @Autowired
   private EvenenemtsCommunauxDataset evenenementsCommunauxDataset;

   /** Dataset des communes. */
   @Autowired
   private CogDataset cogDataset;

   /**
    * Obtention des Dataset de row d'évènements communaux.
    */
   @Test
   @DisplayName("Dataset des évènenements communaux (row).")
   void rowEvenementsCommunaux() {
      Dataset<Row> evenements = this.evenenementsCommunauxDataset.rowEvenementsCommunaux(null, null, new EvenementsCommunauxTriCodeCommuneAvantDate());
      assertNotEquals(0, evenements.count(), "Plusieurs évènements auraient du être lus pour les communes.");

      evenements.show(200, false);
   }

   /**
    * Suivi de l'évolution de la commune de Saint-Bois qui devient Arbignieu.
    */
   @Test
   @DisplayName("Ajustement de la commune Saint-Bois qui devient Arbignieu en 2016")
   void ajustementCommuneArbignieu() {
      /*
         La création de la commune nouvelle d’Arboys-en-Bugey donne :
         Mod 	Date d'effet 	Type commune avant 	Id commune avant 	Libellé avant 	Type commune après 	Code commune après 	Libellé après
         32 	2016-01-01 	COM 	01015 	Arbignieu 	COM 	01015 	Arboys en Bugey
         32 	2016-01-01 	COM 	01015 	Arbignieu 	COMD 	01015 	Arbignieu
         32 	2016-01-01 	COM 	01340 	Saint-Bois 	COM 	01015 	Arboys en Bugey
         32 	2016-01-01 	COM 	01340 	Saint-Bois 	COMD 	01340 	Saint-Bois
       */
      List<EntreeHistoriqueCommune> evenementsCommunaux = this.evenenementsCommunauxDataset.evenements(null, null).collectAsList();
      LocalDate au1erJanvier2016 = LocalDate.of(2016,1,1);

      // Saint-Bois
      CommuneHistorique commune = this.evenenementsCommunauxDataset.estDevenue("01340", evenementsCommunaux, au1erJanvier2016, au1erJanvier2016);

      assertNotNull(commune, "La commune de Saint-Bois (de code 01340, de 2015 et avant) aurait dû être transformée au 1er Janvier 2016");
      assertEquals("01015", commune.getCodeCommune(), "La commune résultante n'a pas le bon code commune");
      assertEquals("Arboys en Bugey", commune.getNomCommune(), "La commune résultante n'a pas le bon nom de commune");
      assertEquals("COM", commune.getTypeCommune(), "La commune résultante n'a pas le type de commune COM escompté");
   }

   @Test
   @DisplayName("Changement entre le COG 2023 et 2024")
   void cog2023_2024() {
      Dataset<Row> communes2023 = this.cogDataset.rowCommunes(null, null, 2023, false);     // Les communes que l'on veut redresser
      Dataset<Row> cogReference2024 = this.cogDataset.rowCommunes(null, null, 2024, false); // Le COG de référence

      Dataset<CommuneHistorique> communesTransformees = this.evenenementsCommunauxDataset.redresser(null, null,
         communes2023, 2024, cogReference2024, LocalDate.of(2010, 1, 1));

      long count = communesTransformees.count();
      assertNotEquals(0L, count, "Au moins une transformation de commune aurait dû avoir lieu");
      LOGGER.info("Il y a {} communes transformées entre 2023 et 2024.", count);

      communesTransformees
         .select("dateEffetEvenement",
            "codeCommuneHistorique", "nomCommuneHistorique", "typeCommuneHistorique",
            "typeEvenement",
            "codeCommune", "nomCommune", "typeCommune",
            "nomCommuneAvecArticle","nomCommuneMajuscules","typeDeNom")

         .show(5000, false);
   }
}
