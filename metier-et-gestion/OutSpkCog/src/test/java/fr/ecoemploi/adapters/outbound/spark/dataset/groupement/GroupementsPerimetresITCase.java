package fr.ecoemploi.adapters.outbound.spark.dataset.groupement;

import java.io.*;
import java.util.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.*;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.SparkCogTestApplication;
import fr.ecoemploi.adapters.outbound.spark.dataset.competence.AnalyseurCompetences;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Test sur les groupements et périmètres.
 * @author Marc LE BIHAN
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkCogTestApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GroupementsPerimetresITCase extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 1977713931841424375L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(GroupementsPerimetresITCase.class);

   /** Dataset des périmètres */
   @Autowired
   private EPCIPerimetreDataset perimetreEPCIDataset;

   /** Analyseur de compétences. */
   @Autowired
   private AnalyseurCompetences analyseurCompetences;

   /** Année du cog de départ pour les tests. */
   public static final int COG_START = 2016;
   
   /** Année du cog de fin pour les tests. */
   public static final int COG_END = 2024;

   /**
    * Lecture du fichier des périmètres.
    * @param typeRestriction Type de la restriction appliquée
    * @param communes Restriction aux communes, si non vide ou null
    * @param annee Restriction à l'année, si non null
    */
   @DisplayName("Dataset du fichier des périmètres")
   @ParameterizedTest(name = "Périmètres {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   @Order(10)
   void perimetres(String typeRestriction, Set<String> communes, Integer annee) {
      OptionsCreationLecture options = this.perimetreEPCIDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      for(int cog = COG_END; cog >= COG_START; cog --) {
         if (toutesAnnees() == false && cog != COG_END) {
            continue;
         }

         LOGGER.info("Lecture des périmètres pour l'année {}, restriction : {} → {}, {}", cog, typeRestriction, communes, annee);

         perimetres(options, cog, false, new EPCIPerimetreTriCommuneSiegeCommuneMembreSirenMembre());
         perimetres(options, cog, true, new EPCIPerimetreTriCommuneSiegeCommuneMembreSirenMembre());

         if (tousTris() == false) {
            break;
         }

         perimetres(options, cog, false, new EPCIPerimetreTriCommuneMembreSirenMembre());
         perimetres(options, cog, true, new EPCIPerimetreTriCommuneMembreSirenMembre());

         perimetres(options, cog, false, new EPCIPerimetreTriSirenGroupementCommuneMembreSirenMembre());
         perimetres(options, cog, true, new EPCIPerimetreTriSirenGroupementCommuneMembreSirenMembre());

         perimetres(options, cog, false, new EPCIPerimetreTriSirenSiegeCommuneMembreSirenMembre());
         perimetres(options, cog, true, new EPCIPerimetreTriSirenSiegeCommuneMembreSirenMembre());

         perimetres(options, cog, false, new EPCIPerimetreTriSirenMembre());
         perimetres(options, cog, true, new EPCIPerimetreTriSirenMembre());
      }
   }

   /**
    * Exécution d'un test sur un périmètre pour une année de COG et un tri.
    * @param options Options de constitution ou de lecture de dataset.
    * @param cog Année du COG
    * @param epciSeulement true, si l'on désire seulement les intercommunalités. false, pour avoir aussi les autres groupements (syndicats).
    * @param tri Tri
    */
   private void perimetres(OptionsCreationLecture options, int cog, boolean epciSeulement, EPCIPerimetreTri tri) {
      Dataset<Row> perimetres = this.perimetreEPCIDataset.rowPerimetres(options, new HistoriqueExecution(), cog, epciSeulement, tri);

      if (useExplainPlan()) {
         perimetres.explain(explainPlan());
      }

      long count = perimetres.count();
      assertNotEquals(0, perimetres.count(), "Plusieurs périmètres EPCI auraient du être lus.");
      perimetres.show(100, false);
      LOGGER.info("{} périmètres d'intercommunalités ont été lus pour l'année {}.", count, cog);
   }

   /**
    * Lecture des groupements au complet.
    * @param typeRestriction Type de la restriction appliquée
    * @param communes Restriction aux communes, si non vide ou null
    * @param annee Restriction à l'année, si non null
    */
   @DisplayName("Vérification des Etablissements Publics Territoriaux (EPT) de la Métropole du Grand Paris")
   @ParameterizedTest(name = "EPT {0} : {1}, année : {2})")
   @ArgumentsSource(RestrictionsArgumentProvider.class)
   @Order(20)
   void ept(String typeRestriction, Set<String> communes, Integer annee) {
      // Ne s'applique qu'à la France entière
      if ("France entière".equals(typeRestriction) == false) {
         return;
      }

      Column mgp = CODE_DEPARTEMENT.col().isin("92", "93", "94");

      Column grp = col("sirenGroupement").isin(
         "200057941", "200058006", "200058014", "200057867", "200057875",
         "200058097", "200058790", "200057966", "200057974", "200057982",
         "200057990"
      );

      OptionsCreationLecture options = this.perimetreEPCIDataset.optionsCreationLecture()
         .restrictionAuxCommunes(communes)
         .restrictionAnnee(annee);

      Dataset<Row> perimetres = this.perimetreEPCIDataset.rowPerimetres(options, new HistoriqueExecution(), COG_END, true, new EPCIPerimetreTriCommuneSiegeCommuneMembreSirenMembre())
         .where(mgp);

      LOGGER.info("Périmètres présents dans les départements des EPTs (1) :");
      perimetres.show(2000, false);
      assertNotEquals(0, perimetres.count(), "Plusieurs communes membres d'EPT auraient du être lus");

      Dataset<Row> perimetresAutourSiege = this.perimetreEPCIDataset.rowPerimetres(options, new HistoriqueExecution(), COG_END, true, new EPCIPerimetreTriCommuneSiegeCommuneMembreSirenMembre())
         .where(grp);

      LOGGER.info("Périmètres présents dans les départements des EPTs (2) :");
      perimetresAutourSiege.show(2000, false);

      Dataset<Row> compositionEPTs = this.perimetreEPCIDataset.rowPerimetres(options, new HistoriqueExecution(), COG_END, true, new EPCIPerimetreTriCommuneSiegeCommuneMembreSirenMembre())
         .where(mgp);

      LOGGER.info("Composition des EPTs et autres groupements dans les départements des EPTs :");
      compositionEPTs.show(2000, false);
      assertNotEquals(0, compositionEPTs.count(), "La composition des EPTs par groupements + pérmiètres est incomplet et ne donne pas les EPTs");
   }

   /**
    * Les compétences communes d'intercommunalités
    */
   @DisplayName("Compétences exclusives d'intercommunalités ou exclues")
   @Order(30)
   @Test
   void competencesExclusivesEtExclues() {
      this.analyseurCompetences.competencesExclusivesEtExclues("247900798", "CC", 2024).show(2000, false);
   }

   /**
    * Lecture des groupements au complet.
    * @param typeRestriction Type de la restriction appliquée
    * @param obligatoires <code>true</code> Pour les compétences obligatoires, <code>false</code> pour celles obsolètes
    * @param naturesJuridiques Restriction à ces natures juridiques, si non vide ou <code>null</code>
    * @param annee Année pour les compétences en vigueur
    */
   @DisplayName("Vérification des Etablissements Publics Territoriaux (EPT) de la Métropole du Grand Paris")
   @ParameterizedTest(name = "Compétences des {0} obligatoires : {2}, année : {3})")
   @ArgumentsSource(CompetencesObligatoiresOuObsoletesArgumentProvider.class)
   @Order(40)
   void competencesObligatoiresOuObsoletes(String typeRestriction, String[] naturesJuridiques, boolean obligatoires, Integer annee) {
      OptionsCreationLecture options = this.perimetreEPCIDataset.optionsCreationLecture();
      Dataset<Row> perimetres = this.perimetreEPCIDataset.rowPerimetres(options, new HistoriqueExecution(), annee, false, new EPCIPerimetreTriCommuneSiegeCommuneMembreSirenMembre());

      Set<String> codesCompetences = this.perimetreEPCIDataset.competencesObligatoiresOuObsoletes(annee, perimetres, obligatoires, naturesJuridiques);
      LOGGER.info("Les compétences {} pour les {} l'année {} sont : {}", obligatoires ? "obligatoires" : "obsolètes", naturesJuridiques, annee, codesCompetences);
   }
}
