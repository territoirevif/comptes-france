package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import org.junit.jupiter.api.*;
import org.junit.platform.suite.api.*;

/**
 * Suite de tests pour le Code Officiel Géographique
 */
@Suite
@SelectClasses({
   DepartementsITCase.class, SirenCommunauxITCase.class, CommunesITCase.class, EvenementsCommunauxITCase.class
})
@DisplayName("Code Officiel Géographique")
public class CogSuiteIT {
}
