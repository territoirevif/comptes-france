package fr.ecoemploi.adapters.outbound.spark.dataset.groupement;

import java.io.*;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.*;
import org.springframework.stereotype.Component;

import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.HistoriqueExecution;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.validation.AbstractRowValidator;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Validateur de périmètre de groupement
 * @author Marc Le Bihan
 */
@Component
public class EPCIPerimetreRowValidator extends AbstractRowValidator implements ApplicationContextAware {
   @Serial
   private static final long serialVersionUID = -7596710858513976260L;

   /** Type de groupement inconnu */
   private static final String VLD_TYPE_GROUPEMENT_INCONNU = "TYPE_GROUPEMENT_INCONNU";

   /** Commune inconnue du COG */
   private static final String VLD_COMMUNE_MEMBRE_INCONNUE = "COMMUNE_MEMBRE_INCONNUE";

   /** Type du groupement détenteur : une commune */
   private static final String TYPE_COMMUNE = "Commune";

   /** Type du groupement détenteur : un autre groupement */
   private static final String TYPE_GROUPEMENT = "Groupement";

   /** Type de détenteur : autre organisme */
   private static final String TYPE_AUTRE_ORGANISME = "Autre organisme";

   /**
    * Valider un périmètre de groupement
    * @param historique Historique d'exécution
    * @param perimetre Périmètre à valider
    * @param anneeCOG Année du Code Officiel Géographique considérée
    * @return true s'il est valide
    */
   public boolean validerPerimetre(HistoriqueExecution historique, Row perimetre, int anneeCOG) {
      String codeCommune = CODE_COMMUNE_MEMBRE.getAs(perimetre);
      String type = perimetre.getAs("type");
      String siren = SIREN_COMMUNE_SIEGE.getAs(perimetre);
      String sirenGroupement = SIREN_GROUPEMENT.getAs(perimetre);
      String nomGroupement = NOM_GROUPEMENT.getAs(perimetre);
      String nomCommuneMembre = NOM_COMMUNE_MEMBRE.getAs(perimetre);

      return switch(type) {
         case TYPE_COMMUNE ->
            valideSi(VLD_COMMUNE_MEMBRE_INCONNUE, historique, perimetre, e -> codeCommune != null,
               () -> new Serializable[]{nomGroupement, siren, nomCommuneMembre, anneeCOG});

         case TYPE_GROUPEMENT, TYPE_AUTRE_ORGANISME -> true;

         default -> {
            invalider(VLD_TYPE_GROUPEMENT_INCONNU, historique,
               () -> new Serializable[]{nomGroupement, type, siren, sirenGroupement});

            yield false;
         }
      };
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
      declareRegle(VLD_TYPE_GROUPEMENT_INCONNU, this.messageSource, "Périmètre de groupement désignant un type de groupement inconnu",
         String.class, String.class, String.class, String.class);

      declareRegle(VLD_COMMUNE_MEMBRE_INCONNUE, this.messageSource, "Périmètre de groupement dont la commune membre est inconnue du Code Officiel Géographique",
         String.class, String.class, String.class, Integer.class);
   }
}
