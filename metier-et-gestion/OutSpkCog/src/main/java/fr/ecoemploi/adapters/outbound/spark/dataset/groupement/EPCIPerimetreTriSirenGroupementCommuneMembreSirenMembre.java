package fr.ecoemploi.adapters.outbound.spark.dataset.groupement;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Partitionnement par département et tri par siren du groupement (exemple : celui de l'EPCI), code de la commune membre (si applicable), et siren du membre
 * @author Marc Le Bihan
 */
public class EPCIPerimetreTriSirenGroupementCommuneMembreSirenMembre extends EPCIPerimetreTri {
   @Serial
   private static final long serialVersionUID = -5927631088111309520L;

   /**
    * Constuire cette définition de tri.
    */
   public EPCIPerimetreTriSirenGroupementCommuneMembreSirenMembre() {
      super("PARTITIONNEMENT-departement-TRI-siren_groupement-commune_membre-siren_membre", true,
         CODE_DEPARTEMENT.champ(), false,
         CODE_DEPARTEMENT.champ(), SIREN_GROUPEMENT.champ(), CODE_COMMUNE_MEMBRE.champ(), SIREN_MEMBRE.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @param sirenGroupement SIREN d'un groupement, si null sera testé avec <code>isNull</code>
    * @param codeCommuneMembre Code commune membre, si null sera testé avec <code>isNull</code>
    * @param sirenMembre SIREN membre, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String sirenGroupement, String codeCommuneMembre, String sirenMembre) {
      return equalTo(codeDepartement, sirenGroupement, codeCommuneMembre)
         .and(sirenMembre != null ? SIREN_MEMBRE.col().equalTo(sirenMembre) : SIREN_MEMBRE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @param sirenGroupement SIREN d'un groupement, si null sera testé avec <code>isNull</code>
    * @param codeCommuneMembre Code commune membre, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String sirenGroupement, String codeCommuneMembre) {
      return equalTo(codeDepartement, sirenGroupement)
         .and(codeCommuneMembre != null ? CODE_COMMUNE_MEMBRE.col().equalTo(codeCommuneMembre) : CODE_COMMUNE_MEMBRE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @param sirenGroupement SIREN d'un groupement, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String sirenGroupement) {
      return equalTo(codeDepartement)
         .and(sirenGroupement != null ? SIREN_GROUPEMENT.col().equalTo(sirenGroupement) : SIREN_GROUPEMENT.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
