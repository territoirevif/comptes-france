package fr.ecoemploi.adapters.outbound.spark.dataset.competence;

import java.io.*;
import java.util.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.api.java.UDF3;
import org.apache.spark.sql.expressions.*;
import org.apache.spark.sql.types.DataTypes;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.groupement.*;

import scala.collection.immutable.ArraySeq;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;
import static org.apache.spark.sql.functions.*;
import static scala.collection.JavaConverters.asJava;

/**
 * Analyse de compétences
 * @author Marc Le Bihan
 */
@Service
public class AnalyseurCompetences extends AbstractSparkDataset {
   @Serial
   private static final long serialVersionUID = -7865155687881403189L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(AnalyseurCompetences.class);

   /** Dataset des périmètres et groupements */
   private final EPCIPerimetreDataset perimetreEPCIDataset;

   /** Les codes des compétences étudiées */
   private final String[] codesCompetences = {
      "C1010", "C1015", "C1020", "C1025", "C1030", "C1502", "C1505", "C1507", "C1510", "C1515",
      "C1520", "C1525", "C1528", "C1529", "C1530", "C1531", "C1532", "C1533", "C1534", "C1535",
      "C1540", "C1545", "C1550", "C1555", "C1560", "C2005", "C2010", "C2015", "C2510", "C2515",
      "C2520", "C2521", "C2525", "C2526", "C3005", "C3010", "C3015", "C3020", "C3025", "C3210",
      "C3220", "C3505", "C3510", "C3515", "C4005", "C4006", "C4007", "C4008", "C4010", "C4015",
      "C4016", "C4017", "C4020", "C4025", "C4505", "C4510", "C4515", "C4520",
      "C4525", "C4530", "C4531", "C4532", "C4535", "C4540", "C4545", "C4550", "C4555", "C4560",
      "C5005", "C5010", "C5015", "C5210", "C5220", "C5505", "C5510", "C5515", "C5520", "C5525",
      "C5530", "C5535", "C5540", "C5545", "C5550", "C5555", "C7005", "C7010", "C7012", "C7015",
      "C7020", "C7025", "C7030", "C9905", "C9910", "C9915", "C9920", "C9922", "C9923", "C9924",
      "C9925", "C9930", "C9935", "C9940", "C9950", "C9999"
   };

   /**
    * Construire un analyseur de compétences
    * @param perimetreEPCIDataset Dataset des groupements
    */
   @Autowired
   public AnalyseurCompetences(EPCIPerimetreDataset perimetreEPCIDataset) {
      this.perimetreEPCIDataset = perimetreEPCIDataset;
   }

   /**
    * Les compétences communes d'intercommunalités
    * @param sirenGroupementCandidat Le siren du groupement dont on veut analyser les compétences
    * @param anneeCog Année du code officiel géographique
    * @param natureJuridique Nature Juridique des groupements à comparer
    * @return Dataset
    */
   public Dataset<Row> competencesExclusivesEtExclues(String sirenGroupementCandidat, String natureJuridique, int anneeCog) {
      /*
|C1010|C1015|C1020|C1025|C1030|C1502|C1505|C1507|C1510|C1515|C1520|C1525|C1528|C1529|C1530|C1531|C1532|C1533|C1534|C1535|C1540|C1545|C1550|C1555|C1560|C2005|C2010|C2015|C2510|C2515|C2520|C2521|C2525|C2526|C3005|C3010|C3015|C3020|C3025|C3210|C3220|C3505|C3510|C3515|C4005|C4006|C4007|C4008|C4010|C4015|C4016|C4017|C4020|C4025|C4505|C4510|C4515|C4520|C4525|C4530|C4531|C4532|C4535|C4540|C4545|C4550|C4555|C4560|C5005|C5010|C5015|C5210|C5220|C5505|C5510|C5515|C5520|C5525|C5530|C5535|C5540|C5545|C5550|C5555|C7005|C7010|C7012|C7015|C7020|C7025|C7030|C9905|C9910|C9915|C9920|C9922|C9923|C9924|C9925|C9930|C9935|C9940|C9950|C9999|nomGroupement   |sirenGroupement|nombreDeCompetencesExercees|populationGroupement|codeDepartement|
+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+----------------+---------------+---------------------------+--------------------+---------------+
|0    |0    |0    |1    |0    |1    |1    |1    |1    |0    |0    |0    |1    |1    |0    |1    |1    |0    |1    |0    |1    |0    |1    |0    |0    |0    |0    |0    |0    |1    |1    |0    |1    |0    |0    |0    |0    |0    |0    |0    |0    |1    |0    |0    |0    |0    |0    |1    |1    |0    |0    |0    |1    |1    |1    |1    |1    |1    |0    |1    |0    |0    |0    |0    |0    |0    |1    |0    |1    |0    |0    |1    |0    |1    |1    |1    |0    |0    |1    |1    |0    |0    |0    |0    |0    |0    |0    |0    |0    |0    |0    |0    |0    |0    |0    |0    |0    |1    |0    |1    |1    |0    |0    |1    |CC du Thouarsais|247900798      |37                         |36317               |79             |
       */

      List<Column> competences = new ArrayList<>();

      for(String codeCompetence : this.codesCompetences) {
         competences.add(new Column(codeCompetence));
      }

      List<Column> selectClause = new ArrayList<>(competences);
      selectClause.add(NOM_GROUPEMENT.col());
      selectClause.add(SIREN_GROUPEMENT.col());
      selectClause.add(col("nombreDeCompetencesExercees"));

      OptionsCreationLecture options = this.perimetreEPCIDataset.optionsCreationLecture();

      // Compétences des communautés de communes
      Dataset<Row> competencesCC = this.perimetreEPCIDataset.rowPerimetres(options, new HistoriqueExecution(), anneeCog, true, new EPCIPerimetreTriCommuneSiegeCommuneMembreSirenMembre())
         .where(NATURE_JURIDIQUE.col().equalTo(natureJuridique)
            .and(CODE_COMMUNE_SIEGE.col().equalTo(CODE_COMMUNE_MEMBRE.col()))) // Seulement la ligne de périmètre se rapportant au groupement siège
         .select(selectClause.toArray(new Column[]{}));

      Dataset<Row> intercoReference = competencesCC.where(SIREN_GROUPEMENT.col().equalTo(sirenGroupementCandidat));

      LOGGER.info("Analyse du groupement de SIREN {} avec les autres groupements de nature {} de l'année {}", sirenGroupementCandidat, natureJuridique, anneeCog);
      return differencesDeChoixDeCompetencesParUDF(intercoReference, competencesCC);
   }

   /**
    * Renvoyer le dataset des différences de choix de compétences, ordonné par taux de similarité
    * @param intercommunalites Compétences des intercommunalités du jeu de données
    * @return Dataset des différences de choix de compétences, avec une colonne "similarite" supplémentaire, ordonné par taux de similarité
    */
   private Dataset<Row> differencesDeChoixDeCompetencesParUDF(Dataset<Row> intercoDeReference, Dataset<Row> intercommunalites) {
      this.session.udf().register("similariteCompetences", (UDF3<String, ArraySeq.ofRef<Boolean>, ArraySeq.ofRef<Boolean>, Double>)
         (sirenGroupement, competencesIntercoCandidate, competencesIntercoReference) -> {
            scala.collection.Iterator<Boolean> itCandidat = competencesIntercoCandidate.iterator();
            scala.collection.Iterator<Boolean> itReference = competencesIntercoReference.iterator();

            int nombreCompetencesEquivalentes = 0;

            while(itCandidat.hasNext() && itReference.hasNext()) {
               nombreCompetencesEquivalentes += itCandidat.next().equals(itReference.next()) ? 1 : 0;
            }

            return (double)nombreCompetencesEquivalentes / (double) AnalyseurCompetences.this.codesCompetences.length;
         }, DataTypes.DoubleType);


      // FIXME Un appel UDF ne paraît pas pouvoir prendre de valeurs de deux différents datasets au même moment. Contournement.
      Column colIntercoReference = lit(asJava(intercoDeReference.select(columnCompetences()).first().getSeq(0)).toArray(new Boolean[]{}));
      Column similarite = callUDF("similariteCompetences", SIREN_GROUPEMENT.col(), columnCompetences(), colIntercoReference);

      return intercommunalites.withColumn("similarite", similarite).orderBy(similarite.desc());
   }

   /**
    * Renvoyer une colonne rapportant toutes les compétences d'une intercommunalité
    * @return Colonne de type <code>Array</code> rapportant toutes les compétences
    */
   private Column columnCompetences() {
      Column[] colonnesCompetences = new Column[this.codesCompetences.length];

      for(int index=0; index < this.codesCompetences.length; index++) {
         colonnesCompetences[index] = col(this.codesCompetences[index]);
      }

      return array(colonnesCompetences).name("arrayCompetences");
   }

   /**
    * Renvoyer le dataset des différences de choix de compétences, ordonné par taux de similarité
    * @param intercoDeReference Intercommunalité de référence
    * @param intercommunalites Toutes les intercommunalités du jeu de données
    * @return Dataset des différences de choix de compétences, avec une colonne "similarite" supplémentaire, ordonné par taux de similarité
    */
   private Dataset<Row> differencesDeChoixDeCompetences(Row intercoDeReference, Dataset<Row> intercommunalites) {
      WindowSpec w = Window.orderBy(SIREN_GROUPEMENT.col());

      for(String codeCompetence : this.codesCompetences) {
         intercommunalites = intercommunalites.withColumn("X" + codeCompetence, lag(codeCompetence, 0).over(w));
      }

      return intercommunalites; //.orderBy(similarite.desc());
   }
}
