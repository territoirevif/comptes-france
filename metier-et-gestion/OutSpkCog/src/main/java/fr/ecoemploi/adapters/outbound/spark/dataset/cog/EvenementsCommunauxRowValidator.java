package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.*;
import java.time.LocalDate;
import java.util.*;
import java.util.function.BooleanSupplier;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.*;
import org.springframework.stereotype.Component;

import org.apache.spark.sql.Row;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.HistoriqueExecution;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.validation.AbstractRowValidator;
import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Validateur d'évènements communaux.
 * @author Marc Le Bihan
 */
@Component
public class EvenementsCommunauxRowValidator extends AbstractRowValidator implements ApplicationContextAware {
   @Serial
   private static final long serialVersionUID = 4968627320188273041L;

   /** Un évènement est invalide : il mentionne une commune après, mais pas de commune avant */
   public static final String VLD_EVT_COMMUNAL_SANS_COM_AVANT_INVALIDE = "EVT_SANS_COM_AVANT";

   /**
    * Valider un Row de Dataset d'évènement communaux
    * @param historique Historique d'exécution
    * @param evenement Row d'évènement communal
    * @return true si l'évènement est valide.
    */
   public boolean validerRowEvenementCommunal(HistoriqueExecution historique, Row evenement) {
      List<BooleanSupplier> validations = new ArrayList<>();

      // L'évènement mentionne une commune après, mais pas de commune avant.
      validations.add(() -> invalideSi(VLD_EVT_COMMUNAL_SANS_COM_AVANT_INVALIDE, historique, evenement,
         evt -> StringUtils.isBlank(CODE_COMMUNE_AVANT_EVENEMENT.getAs(evenement)),
         () -> new Serializable[] {DATE_EFFET_EVENEMENT_COMMUNAL.getAs(evenement), CODE_COMMUNE_APRES_EVENEMENT.getAs(evenement), evenement.getAs("nomCommuneApresEvenement"), evenement.getAs("typeCommuneApresEvenement")}));

      return validations.stream().allMatch(BooleanSupplier::getAsBoolean);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
      declareRegle(VLD_EVT_COMMUNAL_SANS_COM_AVANT_INVALIDE, this.messageSource,
         "Un évènement mentionne une commune après qu'il ait eu lieu, mais pas avant qu'il ait pris place", LocalDate.class, String.class, String.class, String.class);
   }
}
