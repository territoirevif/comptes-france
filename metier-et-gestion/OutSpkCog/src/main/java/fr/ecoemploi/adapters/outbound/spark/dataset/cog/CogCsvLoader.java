package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.*;
import java.text.MessageFormat;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.domain.model.catalogue.OrganisationId;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Chargeur de Code Officiel Géographique depuis un fichier CSVS.
 * @author Marc Le Bihan
 */
@Component
public class CogCsvLoader extends AbstractSparkCsvLoader {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 8055937663293130635L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CogCsvLoader.class);

   /** Nom du fichier CSV. */
   private final String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   private final String repertoireFichier;

   /** Session Spark. */
   private final SparkSession session;

   /**
    * Construire un chargeur de fichier CSV du Code Officiel Géographique
    * @param nomFichier Nom du fichier à lire
    * @param repertoireFichier Répertoire du fichier
    * @param session Session Spark
    */
   @Autowired
   public CogCsvLoader(@Value("${communes.fichier.csv.nom}") String nomFichier, @Value("${territoire.dir}") String repertoireFichier, SparkSession session) {
      this.nomFichier = nomFichier;
      this.repertoireFichier = repertoireFichier;
      this.session = session;
   }

   /**
    * Charger le fichier CSV des communes et renommer ses champs.
    * @param anneeCOG Année à charger.
    * @param inclureCommunesDelegueeOuAssociees true s'il faut inclure aux communes celles déléguées ou associées.
    * @return Dataset des communes.
    * @throws IllegalArgumentException s'il n'existe pas de Code Officiel Géograhique pour l'année désirée.
    */
   public Dataset<Row> loadOpenData(int anneeCOG, boolean inclureCommunesDelegueeOuAssociees) {
      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, anneeCOG, this.nomFichier);

      AbstractSparkDataset.setStageDescription(this.session, "csv cog {0,number,#0}", anneeCOG);

      StructType schema = schema(anneeCOG);
      Dataset<Row> csv = load(schema, anneeCOG, source);
      csv = rename(csv, anneeCOG);
      csv = cast(csv, anneeCOG);
      csv = select(csv, anneeCOG);

      if (inclureCommunesDelegueeOuAssociees == false) {
         csv = csv.filter("typeCommune <> 'COMA' AND typeCommune <> 'COMD'"); // Si la commune est une commune déléguée ou associée, elle est éjectée.
      }

      return csv;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getCatalogueId() {
      return "58c984b088ee386cdb1261f3";
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getOrganisationId() {
      return OrganisationId.INSEE;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getResourceId() {
      // https://www.insee.fr/fr/statistiques/fichier/7766585/v_commune_2024.csv
      // Millésime 2024 : Liste des communes, arrondissements municipaux, communes déléguées et communes associées au 01/01/2024
      // Liste des communes, arrondissements municipaux, communes déléguées et communes associées au 1er janvier 2024, avec le code des niveaux supérieurs (canton ou pseudo-canton, département, région)
      return "8262de72-138f-4596-ad2f-10079e5f4d7c";
   }
   
/*
"4b073be0-d818-43eb-a813-4d45b90e2596";"https://www.insee.fr/fr/information/7766169";"Millésime 2024 : Liste des modalités des fichiers téléchargeables du COG";"Liste des modalités des fichiers téléchargeables du COG.";"remote";"main";"html";"2024-02-20T16:37:15.508000";"2024-07-20T21:18:22.036656";1091;False

"e31e4f85-635e-4ec9-8d88-c4d6c0c586bb";"https://www.insee.fr/fr/statistiques/fichier/7766585/v_pays_territoire_2024.csv";"Millésime 2024 : Pays et territoires étrangers au 01/01/2024";"Liste des pays et territoires étrangers au 1er janvier 2024.

"8262de72-138f-4596-ad2f-10079e5f4d7c";"https://www.insee.fr/fr/statistiques/fichier/7766585/v_commune_2024.csv";"Millésime 2024 : Liste des communes, arrondissements municipaux, communes déléguées et communes associées au 01/01/2024";"Liste des communes, arrondissements municipaux, communes déléguées et communes associées au 1er janvier 2024, avec le code des niveaux supérieurs (canton ou pseudo-canton, département, région).";"remote";"main";"csv";"2024-02-20T16:29:05.480000";"2024-02-20T17:02:33.460000";1879;"https://explore.data.gouv.fr/fr/resources/8262de72-138f-4596-ad2f-10079e5f4d7c"
"56be3980-13c1-4c04-91fd-60dc92e8ceb8";"https://www.insee.fr/fr/statistiques/fichier/7766585/v_canton_2024.csv";"Millésime 2024 : Liste des cantons (et pseudo-cantons) au 01/01/2024";"Liste des cantons et des pseudo-cantons (canton fictif reprenant le contour de la commune entière quand celle-ci est partagée entre plusieurs cantons) au 1er janvier 2024.";"remote";"main";"csv";"2024-02-20T16:26:46.489000";"2024-02-21T07:03:16.671000";605;"https://explore.data.gouv.fr/fr/resources/56be3980-13c1-4c04-91fd-60dc92e8ceb8"
"21fdff26-33a9-4b8e-bfd9-ce6d2ed5659e";"https://www.insee.fr/fr/statistiques/fichier/7766585/v_arrondissement_2024.csv";"Millésime 2024 : Liste des arrondissements au 01/01/2024";"Liste des arrondissements au 1er janvier 2024.";"remote";"main";"csv";"2024-02-20T16:24:34.884000";"2024-02-20T17:01:39.370000";512;"https://explore.data.gouv.fr/fr/resources/21fdff26-33a9-4b8e-bfd9-ce6d2ed5659e"
"e436f772-b05d-47f8-b246-265faab8679f";"https://www.insee.fr/fr/statistiques/fichier/7766585/v_departement_2024.csv";"Millésime 2024 : Liste des départements au 01/01/2024";"Liste des départements au 1er janvier 2024.";"remote";"main";"csv";"2024-02-20T16:21:47.142000";"2024-02-20T17:01:07.149000";511;"https://explore.data.gouv.fr/fr/resources/e436f772-b05d-47f8-b246-265faab8679f"
"53cb77ce-8a93-4924-9d5d-920bbe7c679f";"https://www.insee.fr/fr/statistiques/fichier/7766585/v_region_2024.csv";"Millésime 2024 : Liste des régions au 01/01/2024";"Liste des régions au 1er janvier 2024.";"remote";"main";"csv";"2024-02-20T16:18:28.504000";"2024-05-14T10:23:49.853000";416;"https://explore.data.gouv.fr/fr/resources/53cb77ce-8a93-4924-9d5d-920bbe7c679f"
"2da99392-eac0-41a2-acfb-7187033fdcd2";"https://www.insee.fr/fr/statistiques/fichier/7766585/v_mvt_commune_2024.csv";"Millésime 2024 : Liste des événements survenus aux communes, arrondissements municipaux, communes associées et communes déléguées depuis 1943";"Liste des événements survenus aux communes, arrondissements municipaux, communes associées et communes déléguées depuis 1943.";"remote";"main";"csv";"2024-02-20T16:08:27.366000";"2024-02-20T16:54:08.236000";173;"https://explore.data.gouv.fr/fr/resources/2da99392-eac0-41a2-acfb-7187033fdcd2"

"981ff860-b10b-4ac7-b7e1-3c907b525d55";"https://www.insee.fr/fr/statistiques/fichier/7766585/v_ctcd_2024.csv";"Millésime 2024 : Liste des collectivités territoriales ayant les compétences départementales au 01/01/2024";"Liste des collectivités territoriales ayant les compétences départementales (conseils départementaux et collectivités territoriales à statut particulier) au 1er janvier 2024.

"7560b253-fedb-45c5-88fe-f060b0986a9c";"https://www.insee.fr/fr/statistiques/fichier/7766585/v_codes_extension_2024.csv";"Millésime 2024 : Codes extension et territoire de rattachement";"Une utilisation des codes des communes, pays et territoires étrangers est l’inscription des personnes au sein du Répertoire national d’identification des personnes physiques (RNIPP) : pour les besoins de gestion de cette base, des codes extension ont été créés pour enregistrer plus de 1 000 naissances sur un mois dans un territoire donné. Ce fichier donne la liste complète de ces codes extension et de leur territoire de rattachement.";"remote";"main";"csv";"2024-02-20T16:11:46.781000";"2024-05-14T10:22:38.920000";119;"https://explore.data.gouv.fr/fr/resources/7560b253-fedb-45c5-88fe-f060b0986a9c"


"9e31f4ee-b65b-49d9-aedd-0f5f583551f8";"https://www.insee.fr/fr/statistiques/fichier/7766585/v_commune_depuis_1943.csv";"Millésime 2024 : Communes depuis 1943";"Liste de tous les couples code - libellé de commune ayant existé depuis 1943 avec la période durant laquelle le code correspondait au libellé.
"21c99987-5c72-4591-80e1-a9a92ca1c5c6";"https://www.insee.fr/fr/statistiques/fichier/7766585/v_comer_2024.csv";"Millésime 2024 : Liste des collectivités et territoires français d'outre-mer au 01/01/2024";"Liste des collectivités et territoires français d’outre-mer au 1er janvier 2024. Des informations spécifiques sur leur codification sont consultables [**ici**](https://www.insee.fr/fr/information/2028040).";"remote";"main";"csv";"2024-02-20T15:56:33.566000";"2024-02-20T16:58:53.802000";151;"https://explore.data.gouv.fr/fr/resources/21c99987-5c72-4591-80e1-a9a92ca1c5c6"
"09f50ab9-f5b6-400a-b599-643e283d7268";"https://www.insee.fr/fr/statistiques/fichier/7766585/v_commune_comer_2024.csv";"Millésime 2024 : Liste des communes des collectivités et territoires français d'outre-mer au 01/01/2024";"Liste des communes des collectivités et territoires français d’outre-mer au 1er janvier 2024. Pour Wallis-et-Futuna, les Terres australes et antarctiques françaises et La Passion-Clipperton non découpées en commune sont respectivement listés : les circonscriptions territoriales, les districts et le code à 5 chiffres de La Passion-Clipperton.";"remote";"main";"csv";"2024-02-20T15:52:37.748000";"2024-02-21T06:58:37.168000";170;"https://explore.data.gouv.fr/fr/resources/09f50ab9-f5b6-400a-b599-643e283d7268"
"150889d3-ffda-4ea5-8709-6ee67415294f";"https://www.insee.fr/fr/statistiques/fichier/7766585/v_pays_et_territoire_depuis_1943.csv";"Millésime 2024 : Pays et territoires étrangers depuis 1943";"Liste de tous les couples code - libellé de pays ou territoire étranger ayant existé depuis 1943 avec la période durant laquelle le code correspondait au libellé.";"remote";"main";"csv";"2024-02-20T16:31:03.701000";"2024-05-05T21:17:06.597048";482;"https://explore.data.gouv.fr/fr/resources/150889d3-ffda-4ea5-8709-6ee67415294f"

"f246bd12-128d-4f35-9bb4-8d80aa11da7a";"https://www.insee.fr/fr/information/6800685";"Millésime 2023 : Liste des modalités des fichiers téléchargeables du COG";"Liste des modalités des fichiers téléchargeables du COG";"remote";"main";"html";"2023-02-20T14:45:09.904000";"2024-07-27T23:05:44.772194";1354;False
"629fb844-f04c-4ea2-8670-82797ba3b630";"https://www.insee.fr/fr/information/2560452";"Millésimes 1999 à 2022 : Téléchargement des fichiers et liste et modalités des variables";"";"remote";"main";"html";"2017-03-15T19:27:08.017000";"2024-07-20T14:37:59.573311";388;False
 */

   /**
    * {@inheritDoc}
    */
   @Override
   protected StructType schema(int annee) {
      StructType schema = new StructType();

      switch(annee) {
         /*
         Format 2018 et avant :
         CDC   CHEFLIEU REG   DEP   COM   AR CT TNCC  ARTMAJ   NCC                     ARTMIN   NCCENR
         0     0        84    01    001   2  08 5     (L')     ABERGEMENT-CLEMENCIAT   (L')     Abergement-Clémenciat

            CDC      Découpage de la commune en cantons
            CHEFLIEU Chef-lieu d'arrondissement, de département, de région ou bureau centralisateur de canton
            REG      Code région
            DEP      Code département
            COM      Code commune
            AR       Code arrondissement
            CT       Code canton
            TNCC     Type de nom en clair
            ARTMAJ   Article (majuscules)
            NCC      Nom en clair (majuscules)
            ARTMIN   Article (typographie riche)
            NCCENR   Nom en clair (typographie riche)

            - typeCommune est systématiquement alimenté à COM
            - Dans le libellé, les charnières manquent : Abergement-Clémenciat au lieu de L'Abergement-Clémenciat
            - Le code commune parent est à null.
          */
         case 2016, 2017, 2018 -> schema = schema
            .add("CDC", StringType, true)
            .add("CHEFLIEU", StringType, true)
            .add("REG", StringType, false)
            .add("DEP", StringType, false)
            .add("COM", StringType, false)
            .add("AR", StringType, false)
            .add("CT", StringType, false)
            .add("TNCC", StringType, false)
            .add("ARTMAJ", StringType, false)
            .add("NCC", StringType, true)
            .add("ARTMIN", StringType, true)
            .add("NCCENR", StringType, false)
            .add(CODE_COMMUNE_PARENTE.champ(), StringType, true)
            .add("strateCommune", IntegerType, false);

         /* Format 2019 et 2020 :
            "typecom",  "com",   "reg",   "dep",   "arr",   "tncc",  "ncc",                  "nccenr",               "libelle",                 "can",   "comparent"
            "COM",      "01001", "84",    "01",    "012",   "5",     "ABERGEMENT CLEMENCIAT","Abergement-Clémenciat","L'Abergement-Clémenciat", "0108",
         */
         case 2019, 2020 -> schema = schema
            .add("typecom", StringType, false)
            .add("com", StringType, false)
            .add("reg", StringType, false)
            .add("dep", StringType, false)
            .add("arr", StringType, true)
            .add("tncc", StringType, false)
            .add("ncc", StringType, false)
            .add("nccenr", StringType, false)
            .add("libelle", StringType, false)
            .add("can", StringType, true)
            .add("comparent", StringType, true);

         /* Format 2021 et après
            "TYPECOM",  "COM",   "REG",   "DEP",   "CTCD",  "ARR",   "TNCC",  "NCC",                     "NCCENR",                  "LIBELLE",                 "CAN",   "COMPARENT"
            "COM",      "01001", "84",    "01",    "01D",   "012",   "5",     "ABERGEMENT CLEMENCIAT",   "Abergement-Clémenciat",   "L'Abergement-Clémenciat", "0108",
         */
         case 2021, 2022, 2023, 2024 -> schema = schema
            .add("TYPECOM", StringType, false)
            .add("COM", StringType, false)
            .add("REG", StringType, false)
            .add("DEP", StringType, false)
            .add("CTCD", StringType, false)
            .add("ARR", StringType, true)
            .add("TNCC", StringType, false)
            .add("NCC", StringType, false)
            .add("NCCENR", StringType, false)
            .add("LIBELLE", StringType, false)
            .add("CAN", StringType, true)
            .add("COMPARENT", StringType, true);

         default -> cogAbsent(annee);
      }

      return schema;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Dataset<Row> load(StructType schema, int annee, File source) {
      Dataset<Row> csv = null;

      switch(annee) {
         case 2016, 2017, 2018 -> csv = this.session.read().schema(schema).format("csv")
            .option("header", "true")
            .option("delimiter", "\t")
            .option("encoding", "ISO-8859-1")
            .option("enforceSchema", false)
            .load(source.getAbsolutePath());

         /* Format 2019 et 2020 :
            "typecom",  "com",   "reg",   "dep",   "arr",   "tncc",  "ncc",                  "nccenr",               "libelle",                 "can",   "comparent"
            "COM",      "01001", "84",    "01",    "012",   "5",     "ABERGEMENT CLEMENCIAT","Abergement-Clémenciat","L'Abergement-Clémenciat", "0108",
         */
         case 2019, 2020, 2021, 2022, 2023, 2024 -> csv = this.session.read().schema(schema).format("csv")
            .option("header", "true")
            .option("quote", "\"")
            .option("escape", "\"")
            .option("enforceSchema", false)
            .load(source.getAbsolutePath());

         default -> cogAbsent(annee);
      }

      return csv;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Dataset<Row> rename(Dataset<Row> csv, int annee) {
      switch(annee) {
         case 2016, 2017, 2018 -> csv = csv.withColumnRenamed("REG", CODE_REGION.champ())
            .withColumnRenamed("DEP", CODE_DEPARTEMENT.champ())
            .withColumnRenamed("libelle", NOM_COMMUNE.champ())
            .withColumnRenamed("TNCC", "typeNomEtCharniere")
            .withColumnRenamed("NCC", "nomMajuscules")
            .withColumnRenamed("NCCENR", NOM_COMMUNE.champ());

         /* Format 2019 et 2020 :
            "typecom",  "com",   "reg",   "dep",   "arr",   "tncc",  "ncc",                  "nccenr",               "libelle",                 "can",   "comparent"
            "COM",      "01001", "84",    "01",    "012",   "5",     "ABERGEMENT CLEMENCIAT","Abergement-Clémenciat","L'Abergement-Clémenciat", "0108",
         */
         case 2019, 2020 -> csv = csv.withColumnRenamed("typecom", TYPE_COMMUNE.champ())
            .withColumnRenamed("com", CODE_COMMUNE.champ())
            .withColumnRenamed("reg", CODE_REGION.champ())
            .withColumnRenamed("dep", CODE_DEPARTEMENT.champ())
            .withColumnRenamed("arr", ARRONDISSEMENT.champ())
            .withColumnRenamed("tncc", "typeNomEtCharniere")
            .withColumnRenamed("ncc", "nomMajuscules")
            .withColumnRenamed("nccenr", NOM_COMMUNE.champ())
            .withColumnRenamed("can", CODE_CANTON.champ())
            .withColumnRenamed("comparent", CODE_COMMUNE_PARENTE.champ());

         /* Format 2021 et après
            "TYPECOM",  "COM",   "REG",   "DEP",   "CTCD",  "ARR",   "TNCC",  "NCC",                     "NCCENR",                  "LIBELLE",                 "CAN",   "COMPARENT"
            "COM",      "01001", "84",    "01",    "01D",   "012",   "5",     "ABERGEMENT CLEMENCIAT",   "Abergement-Clémenciat",   "L'Abergement-Clémenciat", "0108",
         */
         case 2021, 2022, 2023, 2024 -> csv = csv.withColumnRenamed("TYPECOM", TYPE_COMMUNE.champ())
            .withColumnRenamed("COM", CODE_COMMUNE.champ())
            .withColumnRenamed("REG", CODE_REGION.champ())
            .withColumnRenamed("DEP", CODE_DEPARTEMENT.champ())
            // TODO CTCD n'est pas encore renommé ni interprété.
            .withColumnRenamed("ARR", ARRONDISSEMENT.champ())
            .withColumnRenamed("TNCC", "typeNomEtCharniere")
            .withColumnRenamed("NCC", "nomMajuscules")
            .withColumnRenamed("NCCENR", NOM_COMMUNE.champ())
            .withColumnRenamed("CAN", CODE_CANTON.champ())
            .withColumnRenamed("COMPARENT", CODE_COMMUNE_PARENTE.champ());

         default -> cogAbsent(annee);
      }

      return csv;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Dataset<Row> cast(Dataset<Row> csv, int annee) {
      switch(annee) {
         case 2016, 2017, 2018 -> csv = csv.withColumn("typeCommune", lit("COM"))
            .withColumn(CODE_COMMUNE.champ(), concat(CODE_DEPARTEMENT.col(), col("COM")))
            .withColumn("arrondissement", concat(CODE_DEPARTEMENT.col(), col("AR")))
            .drop(col("AR"))
            .drop(col("CDC"))
            .drop(col("CHEFLIEU"))
            .drop(col("ARTMIN"))
            .drop(col("ARTMAJ"))

            .withColumn(CODE_CANTON.champ(), concat(CODE_DEPARTEMENT.col(), col("CT")))
            .drop(col("COM"))
            .drop(col("CT"))

            .withColumn(CODE_COMMUNE_PARENTE.champ(), lit(null).cast(StringType));

         case 2019, 2020, 2021, 2022, 2023, 2024 -> {
            // Rien à faire pour ces années.
         }

         default -> cogAbsent(annee);
      }

      return csv;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Dataset<Row> select(Dataset<Row> csv, int annee) {
      return csv.select(TYPE_COMMUNE.champ(), CODE_COMMUNE.champ(), CODE_REGION.champ(), CODE_DEPARTEMENT.champ(), ARRONDISSEMENT.champ(),
         NOM_COMMUNE.champ(), CODE_CANTON.champ(), CODE_COMMUNE_PARENTE.champ(), "typeNomEtCharniere", "nomMajuscules");
   }

   /**
    * Emettre une exception de COG absent pour une année donnée.
    * @param annee Année où le COG est absent
    * @throws IllegalArgumentException Exception émise
    */
   private void cogAbsent(int annee) throws IllegalArgumentException {
      String format = "Il n''existe pas de Code Officiel Géographique pour les communes de l''année {0,number,#0}.";
      String message = MessageFormat.format(format, annee);
      IllegalArgumentException ex = new IllegalArgumentException(message);

      LOGGER.warn(message, ex);
      throw ex;
   }
}
