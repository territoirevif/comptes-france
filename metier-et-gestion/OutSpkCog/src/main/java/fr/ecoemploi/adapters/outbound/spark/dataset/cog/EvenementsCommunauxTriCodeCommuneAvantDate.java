package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.Serial;
import java.time.*;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Tri d'évènements communaux par Code Commune avant changement, puis date d'effet.
 * @author Marc LE BIHAN
 */
public class EvenementsCommunauxTriCodeCommuneAvantDate extends EvenementsCommunauxTri {
   @Serial
   private static final long serialVersionUID = 1151960669795723397L;

   /**
    * Constuire cette définition de tri.
    */
   public EvenementsCommunauxTriCodeCommuneAvantDate() {
      super("TRI-commune_avant_evenement-dateEvenement", true, null, false, CODE_COMMUNE_AVANT_EVENEMENT.champ(), DATE_EFFET_EVENEMENT_COMMUNAL.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeCommune Code commune, si null sera testé avec isNull
    * @param dateEffet Date d'effet de l'évènement, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeCommune, LocalDate dateEffet) {
      return equalTo(codeCommune)
         .and(dateEffet != null ? DATE_EFFET_EVENEMENT_COMMUNAL.col().equalTo(dateEffet) : DATE_EFFET_EVENEMENT_COMMUNAL.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeCommune Code commune, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeCommune) {
      return codeCommune != null ? CODE_COMMUNE_AVANT_EVENEMENT.col().equalTo(codeCommune) : CODE_COMMUNE_AVANT_EVENEMENT.col().isNull();
   }
}
