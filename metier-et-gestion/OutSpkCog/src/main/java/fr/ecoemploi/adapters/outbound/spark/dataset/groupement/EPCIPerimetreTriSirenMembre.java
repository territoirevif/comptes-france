package fr.ecoemploi.adapters.outbound.spark.dataset.groupement;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Partitionnement par département et tri par siren membre de périmètres d'EPCI ou de syndicats
 * @author Marc Le Bihan
 */
public class EPCIPerimetreTriSirenMembre extends EPCIPerimetreTri {
   @Serial
   private static final long serialVersionUID = -9150037537669778197L;

   /**
    * Constuire cette définition de tri.
    */
   public EPCIPerimetreTriSirenMembre() {
      super("PARTITIONNEMENT-departement-TRI-siren_membre", true,
         CODE_DEPARTEMENT.champ(), false,
         CODE_DEPARTEMENT.champ(), SIREN_MEMBRE.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @param sirenMembre Siren membre, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String sirenMembre) {
      return equalTo(codeDepartement)
         .and(sirenMembre != null ? SIREN_MEMBRE.col().equalTo(sirenMembre) : SIREN_MEMBRE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
