package fr.ecoemploi.adapters.outbound.spark.dataset.groupement;

import java.io.Serial;

import org.apache.spark.sql.Column;
import static org.apache.spark.sql.functions.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Partitionnement par département et tri par siren de la commune siège, code de la commune membre (si applicable) et siren du membre
 * @author Marc Le Bihan
 */
public class EPCIPerimetreTriSirenSiegeCommuneMembreSirenMembre extends EPCIPerimetreTri {
   @Serial
   private static final long serialVersionUID = -3062521482673344512L;

   /**
    * Constuire cette définition de tri.
    */
   public EPCIPerimetreTriSirenSiegeCommuneMembreSirenMembre() {
      super("PARTITIONNEMENT-departement-TRI-siren_commune_siege-commune_membre-siren_membre", true,
         CODE_DEPARTEMENT.champ(), false,
         CODE_DEPARTEMENT.champ(), SIREN_COMMUNE_SIEGE.champ(), CODE_COMMUNE_MEMBRE.champ(), SIREN_MEMBRE.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @param sirenCommuneSiege SIREN de la commune siège, si null sera testé avec <code>isNull</code>
    * @param codeCommuneMembre Code Commune membre, si null sera testé avec <code>isNull</code>
    * @param sirenMembre Siren membre, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String sirenCommuneSiege, String codeCommuneMembre, String sirenMembre) {
      return equalTo(codeDepartement, sirenCommuneSiege, codeCommuneMembre)
         .and(sirenMembre != null ? SIREN_MEMBRE.col().equalTo(sirenMembre) : SIREN_MEMBRE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @param sirenCommuneSiege SIREN de la commune siège, si null sera testé avec <code>isNull</code>
    * @param codeCommuneMembre Code Commune membre, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String sirenCommuneSiege, String codeCommuneMembre) {
      return equalTo(codeDepartement, sirenCommuneSiege)
         .and(codeCommuneMembre != null ? CODE_COMMUNE_MEMBRE.col().equalTo(codeCommuneMembre) : CODE_COMMUNE_MEMBRE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @param sirenCommuneSiege SIREN de la commune siège, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String sirenCommuneSiege) {
      return equalTo(codeDepartement)
         .and(sirenCommuneSiege != null ? SIREN_COMMUNE_SIEGE.col().equalTo(sirenCommuneSiege) : SIREN_COMMUNE_SIEGE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
