package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.*;
import java.text.MessageFormat;
import java.util.Map;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.types.DataTypes.StringType;

/**
 * Chargeur de données open data départementales depuis un fichier CSV.
 * @author Marc Le Bihan
 */
@Component
public class DepartementCsvLoader extends AbstractSparkCsvLoader {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 6846443686519646113L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(DepartementCsvLoader.class);

   /** Nom du fichier CSV. */
   @Value("${departements.fichier.csv.nom}")
   private String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${territoire.dir}")
   private String repertoireFichier;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Charger le fichier CSV des départements et renommer ses champs.
    * @param anneeCOG Année à charger.
    * @return Dataset des départements.
    * @throws IllegalArgumentException s'il n'existe pas de Code Officiel Géograhique pour l'année désirée.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> loadOpenData(int anneeCOG) {
      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, anneeCOG, this.nomFichier);

      AbstractSparkDataset.setStageDescription(this.session, "csv départements {0,number,#0}", anneeCOG);

      Dataset<Row> csv = load(schema(anneeCOG), source, anneeCOG);
      return rename(csv, anneeCOG);
   }

   /**
    * Renvoyer le schéma du CSV source.
    * @param anneeCOG Année du code officiel géographique.
    * @return Schema.
    */
   @Override
   @SuppressWarnings("java:S1192") // Avertissement pour duplication de noms de colonnes
   protected StructType schema(int anneeCOG) {
      switch(anneeCOG) {
         /*
         Format 2018 et avant :
         REGION   DEP   CHEFLIEU TNCC  NCC   NCCENR
         84       01    01053    5     AIN   Ain

            REGION   Code région
            DEP      Code département
            CHEFLIEU Chef-lieu d'arrondissement, de département, de région ou bureau centralisateur de canton
            TNCC     Type de nom en clair
            NCC      Nom en clair (majuscules)
            NCCENR   Nom en clair (typographie riche)
          */
         case 2016, 2017, 2018 -> {
            return new StructType()
               .add("REGION", StringType, false)
               .add("DEP", StringType, false)
               .add("CHEFLIEU", StringType, true)
               .add("TNCC", StringType, false)
               .add("NCC", StringType, true)
               .add("NCCENR", StringType, false);
         }

         /* Format 2019 et 2020 :
            dep   reg   cheflieu tncc  ncc   nccenr   libelle
            01    84    01053    5     AIN   Ain      Ain

            Le libellé est le nom du département, avec son article
         */
         case 2019, 2020 -> {
            return new StructType()
               .add("dep", StringType, false)
               .add("reg", StringType, false)
               .add("cheflieu", StringType, false)
               .add("tncc", StringType, false)
               .add("ncc", StringType, false)
               .add("nccenr", StringType, false)
               .add("libelle", StringType, false);
         }

         /* Format 2021 et après :
            DEP   REG   CHEFLIEU TNCC  NCC   NCCENR   LIBELLE
            01    84    01053    5     AIN   Ain      Ain
         */
         case 2021, 2022, 2023, 2024 -> {
            return new StructType()
               .add("DEP", StringType, false)
               .add("REG", StringType, false)
               .add("CHEFLIEU", StringType, false)
               .add("TNCC", StringType, false)
               .add("NCC", StringType, false)
               .add("NCCENR", StringType, false)
               .add("LIBELLE", StringType, false);

         }
         default -> {
            String format = "Il n''existe pas de schéma du Code Officiel Géographique pour les départements de l''année {0}.";
            String message = MessageFormat.format(format, anneeCOG);
            IllegalArgumentException ex = new IllegalArgumentException(message);

            LOGGER.warn(message, ex);
            throw ex;
         }
      }
   }

   /**
    * Provoquer le chargement effectif de la source de données.
    * @param anneeCOG Année du code officiel géographique.
    * @param schema Schéma.
    */
   protected Dataset<Row> load(StructType schema, File source, int anneeCOG) {
      switch(anneeCOG) {
         // 2018 et avant : tabulation, ISO-8859-1
         case 2016, 2017, 2018 -> {
            return this.session.read().schema(schema).format("csv")
               .option("header", "true")
               .option("delimiter", "\t")
               .option("encoding", "ISO-8859-1")
               .option("enforceSchema", false)
               .load(source.getAbsolutePath());
         }

         /* Format 2019 et 2020 :
            dep   reg   cheflieu tncc  ncc   nccenr   libelle
            01    84    01053    5     AIN   Ain      Ain

            Le libellé est le nom du département, avec son article
         */
         // Depuis 2019 : point-virgule, chaînes encloses, UTF-8
         case 2019, 2020, 2021, 2022, 2023, 2024 -> {
            return this.session.read().schema(schema).format("csv")
               .option("header", "true")
               .option("quote", "\"")
               .option("escape", "\"")
               .option("enforceSchema", false)
               .load(source.getAbsolutePath());
         }

         default -> {
            String format = "Il n''existe pas d''options de format ou de chargement du Code Officiel Géographique pour les départements de l''année {0}.";
            String message = MessageFormat.format(format, anneeCOG);
            IllegalArgumentException ex = new IllegalArgumentException(message);

            LOGGER.warn(message, ex);
            throw ex;
         }
      }
   }

   /**
    * Renommer les champs du dataset.
    * @param dataset Dataset.
    */
   @Override
   @SuppressWarnings("java:S1192") // Avertissement pour duplication de noms de colonnes
   protected Dataset<Row> rename(Dataset<Row> dataset, int anneeCOG) {
      Map<String, String> renommagesMajuscules = Map.of(
         "REG", "codeRegionDepartement",
         "REGION", "codeRegionDepartement",
         "DEP", "codeDepartement",
         "CHEFLIEU", "codeCommuneChefLieuDepartement",
         "TNCC", "typeNomEtCharniereDepartement",
         "NCC", "nomMajusculesDepartement",
         "NCCENR", "nomDepartement",
         "LIBELLE", "libelleDepartement"
      );

      Map<String, String> renommagesMinuscules = Map.of(
         "dep", "codeDepartement",
         "reg", "codeRegionDepartement",
         "cheflieu", "codeCommuneChefLieuDepartement",
         "tncc", "typeNomEtCharniereDepartement",
         "ncc", "nomMajusculesDepartement",
         "nccenr", "nomDepartement",
         "libelle", "libelleDepartement"
      );

      Dataset<Row> csv;

      switch(anneeCOG) {
         case 2016, 2017, 2018 ->
            csv = super.renameFields(dataset, renommagesMajuscules)
               .withColumn("libelleDepartement", col("nomDepartement"));

         case 2019, 2020 ->
            csv = super.renameFields(dataset, renommagesMinuscules);

         case 2021, 2022, 2023, 2024 ->
            csv = super.renameFields(dataset, renommagesMajuscules);

         default -> {
            String format = "Il n''existe pas de règle de renommage pour champs CSV des départements de l''année {0,number,#0}.";
            String message = MessageFormat.format(format, anneeCOG);
            IllegalArgumentException ex = new IllegalArgumentException(message);

            LOGGER.warn(message, ex);
            throw ex;
         }
      }

      return csv.select("codeDepartement", "codeRegionDepartement", "codeCommuneChefLieuDepartement", "typeNomEtCharniereDepartement", "nomMajusculesDepartement",
         "nomDepartement", "libelleDepartement");
   }
}
