package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.function.Supplier;

import org.apache.spark.api.java.function.ForeachPartitionFunction;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.commons.lang3.*;
import org.apache.spark.sql.*;
import org.apache.spark.sql.api.java.*;

import static org.apache.spark.sql.functions.*;
import org.apache.spark.sql.types.*;
import static org.apache.spark.sql.types.DataTypes.*;

import fr.ecoemploi.adapters.outbound.port.cog.CogRepository;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.verifications.Verification;
import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.groupement.*;
import fr.ecoemploi.domain.model.territoire.*;

/**
 * Dataset du Code officiel géographique.
 * @author Marc Le Bihan
 */
@Service
public class CogDataset extends AbstractSparkDataset implements CogRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 6060925017993338097L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CogDataset.class);
   
   /** Dataset des SIREN communaux. */
   private final SirenCommunesDataset datasetSirenCommunaux;
   
   /** Dataset des départements. */
   private final DepartementDataset datasetDepartements;

   /** Dataset des périmètres d'EPCI. */
   private final EPCIPerimetreDataset datasetPerimetres;

   /** Dataset des évènements communaux. */
   private final EvenenemtsCommunauxDataset datasetEvenementsCommunaux;

   /** Chargeur de fichier Csv de Code Officiel Géographique. */
   private final CogCsvLoader cogCsvLoader;

   /** Validateur. */
   private final CogRowValidator validator;

   /** Année du cog de départ. */
   @Value("${annee.cog.debut}")
   private int anneeCogDebut;

   /** Année du cog de fin. */
   @Value("${annee.cog.fin}")
   private int anneeCogFin;

   /** Nom du fichier de cache. */
   private static final String CACHE2 = "cog-objet-complet";

   /**
    * Construire un Code Officiel Géographique
    * @param datasetSirenCommunaux Dataset des SIREN communaux
    * @param datasetDepartements Dataset des départements
    * @param datasetPerimetres Dataset des périmètres d'EPCI
    * @param datasetEvenementsCommunaux Dataset des évènements communaux
    * @param cogCsvLoader Chargeur de fichier Csv de Code Officiel Géographique
    * @param validator Validateur de Row
    */
   @Autowired
   public CogDataset(SirenCommunesDataset datasetSirenCommunaux, DepartementDataset datasetDepartements,
         EPCIPerimetreDataset datasetPerimetres,
         EvenenemtsCommunauxDataset datasetEvenementsCommunaux,
         CogCsvLoader cogCsvLoader, CogRowValidator validator) {
      this.datasetSirenCommunaux = datasetSirenCommunaux;
      this.datasetDepartements = datasetDepartements;
      this.datasetPerimetres = datasetPerimetres;
      this.datasetEvenementsCommunaux = datasetEvenementsCommunaux;
      this.cogCsvLoader = cogCsvLoader;
      this.validator = validator;
   }

   /**
    * Charger le référentiel territorial dans les fichiers de cache.
    * @param anneeCOG Année de référence du Code Officiel Géographique à charger.<br>
    * 0 si toutes les années connues doivent être chargées (à partir de 2016).
    */
   @Override
   public void chargerReferentiels(int anneeCOG) {
      OptionsCreationLecture options = optionsCreationLecture();
      int anneeDebut = anneeCOG != 0 ? anneeCOG : this.anneeCogDebut;
      int anneeFin = anneeCOG != 0 ? anneeCOG : this.anneeCogFin;

      // Seuls les datasets stockant leurs données sont appelés.
      for(int annee = anneeDebut; annee <= anneeFin; annee ++) {
         this.datasetDepartements.rowDepartements(null, new HistoriqueExecution(), annee);
         rowCommunes(options, new HistoriqueExecution(), annee, true);  // Avec communes déléguées et associées
         rowCommunes(options, new HistoriqueExecution(), annee, false); // Sans : uniquement les COM et les arrondissements
         obtenirCodeOfficielGeographique(annee, true);  // Avec communes déléguées et associées
         obtenirCodeOfficielGeographique(annee, false); // Sans : uniquement les COM et les arrondissements

         this.datasetSirenCommunaux.rowSirenCommunes(options, new HistoriqueExecution(), new SirenCommunesTriSiren(), annee);
         this.datasetSirenCommunaux.rowSirenCommunes(options, new HistoriqueExecution(), new SirenCommunesTriDepartementCommuneSiren(), annee);

         // La constitution par groupement se sert des périmètres, qui gagne à être consituée avant.
         this.datasetPerimetres.rowPerimetres(options, new HistoriqueExecution(), annee, false, new EPCIPerimetreTriCommuneMembreSirenMembre());
         this.datasetPerimetres.rowPerimetres(options, new HistoriqueExecution(), annee, true, new EPCIPerimetreTriCommuneMembreSirenMembre());

         this.datasetPerimetres.rowPerimetres(options, new HistoriqueExecution(), annee, false, new EPCIPerimetreTriCommuneSiegeCommuneMembreSirenMembre());
         this.datasetPerimetres.rowPerimetres(options, new HistoriqueExecution(), annee, true, new EPCIPerimetreTriCommuneSiegeCommuneMembreSirenMembre());

         this.datasetPerimetres.rowPerimetres(options, new HistoriqueExecution(), annee, false, new EPCIPerimetreTriSirenGroupementCommuneMembreSirenMembre());
         this.datasetPerimetres.rowPerimetres(options, new HistoriqueExecution(), annee, true, new EPCIPerimetreTriSirenGroupementCommuneMembreSirenMembre());

         this.datasetPerimetres.rowPerimetres(options, new HistoriqueExecution(), annee, false, new EPCIPerimetreTriSirenSiegeCommuneMembreSirenMembre());
         this.datasetPerimetres.rowPerimetres(options, new HistoriqueExecution(), annee, true, new EPCIPerimetreTriSirenSiegeCommuneMembreSirenMembre());

         this.datasetPerimetres.rowPerimetres(options, new HistoriqueExecution(), annee, false, new EPCIPerimetreTriSirenMembre());
         this.datasetPerimetres.rowPerimetres(options, new HistoriqueExecution(), annee, true, new EPCIPerimetreTriSirenMembre());
      }
   }

   /**
    * Obtenir le code officiel géographique.
    * @param anneeCOG année du COG recherché.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *        false si seules les communes de type COM sont lues et les arrondissements.
    * @return Code Officiel Géographique.
    */
   @Override
   public CodeOfficielGeographique obtenirCodeOfficielGeographique(int anneeCOG, boolean inclureCommunesDelegueesAssociees) {
      OptionsCreationLecture options = optionsCreationLecture();

      String store = MessageFormat.format("{0}/{1}_{2,number,#0}_{3}", options.tempDir(), CACHE2, anneeCOG, inclureCommunesDelegueesAssociees);
      CodeOfficielGeographique cog = Loader.loadFromStoreSerialise(options, CodeOfficielGeographique.class, store);

      if (cog != null) {
         return cog;
      }

      LOGGER.info("Constitution de Code Officiel Geographique (en objet) pour l'année {}...", anneeCOG);

      cog = obtenirCodeOfficielGeographique(anneeCOG, true, inclureCommunesDelegueesAssociees);
      Loader.saveSerialisationToStore(options, cog, store);

      LOGGER.info("Le code Officiel Geographique (en objet) pour l'année {} est prêt et stocké.", anneeCOG);
      return cog;
   }

   /**
    * Renvoyer le code officiel géographique.
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param chargerIntercommunalites true, s'il faut charger les intercommunalités.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *        false si seules les communes de type COM sont lues et les arrondissements.
    * @return Code officiel géographique.
    */
   public CodeOfficielGeographique obtenirCodeOfficielGeographique(int anneeCOG, boolean chargerIntercommunalites, boolean inclureCommunesDelegueesAssociees) {
      OptionsCreationLecture options = optionsCreationLecture();
      HistoriqueExecution historique = new HistoriqueExecution();

      Dataset<Commune> dsCommunes = obtenirCommunes(options, historique, anneeCOG, inclureCommunesDelegueesAssociees);

      CodeOfficielGeographique cog = new CodeOfficielGeographique();

      // Chargement des communes.
      Communes communes = new Communes();
      dsCommunes.collectAsList().forEach(c -> communes.put(c.getCodeCommune(), c));
      cog.setCommunes(communes);

      // Association des intercommunalités à fiscalité propre.
      if (chargerIntercommunalites) {
         Intercommunalites intercommunalites = this.datasetPerimetres.intercommunalites(communes, anneeCOG);
         cog.setIntercommunalites(intercommunalites);
      }

      return cog;
   }

   /**
    * Obtenir un Dataset des communes (comme objets communes, incluant les données siren communaux).
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *       false si seules les communes de type COM sont lues et les arrondissements.
    * @param optionsCreationLecture Options de constitution du dataset.
    * @param historique Historique d'exécution
    * @return Dataset des communes.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Commune> obtenirCommunes(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeCOG, boolean inclureCommunesDelegueesAssociees) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.addReglesValidation(this.session, this.validator);
      }

      Dataset<Row> communes = rowCommunes(options, null, anneeCOG, inclureCommunesDelegueesAssociees);

      return communes
         .select(communes.col("typeCommune"), communes.col("codeCommune"), communes.col("codeRegion"), communes.col("codeDepartement"),
            communes.col("arrondissement"), communes.col("typeNomEtCharniere"), communes.col("nomMajuscules"), communes.col("nomCommune"),
            communes.col("codeCanton"), communes.col("codeCommuneParente"), communes.col("sirenCommune"), communes.col("populationTotale").alias("population"),
            communes.col("strateCommune"), communes.col("codeEPCI"), communes.col("surface"), communes.col("longitude"), communes.col("latitude"),
            communes.col("nomDepartement"), communes.col("nomEPCI"), communes.col("natureJuridiqueEPCI"))
         .as(Encoders.bean(Commune.class));
   }

   /**
    * Obtenir un Dataset des communes.
    * @param optionsCreationLecture Options de constitution du dataset.
    * @param historique Historique d'exécution.
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param inclureCommunesDelegueesAssociees true s'il faut intégrer les communes déléguées et associées<br>
    *        false si seules les communes de type COM sont lues et les arrondissements.
    * @param verifications Vérifications demandées.
    * @return Dataset des communes.
    */
   public Dataset<Row> rowCommunes(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeCOG, boolean inclureCommunesDelegueesAssociees, Verification... verifications) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.addReglesValidation(this.session, this.validator);
      }

      Supplier<Dataset<Row>> worker = () -> {
         super.setStageDescription(this.messageSource, "row.cog.libelle.long", "row.cog.libelle.court", anneeCOG, inclureCommunesDelegueesAssociees);
         LOGGER.info(this.messageSource.getMessage("row.cog.libelle.long", new Object[] {anneeCOG, inclureCommunesDelegueesAssociees}, Locale.getDefault()));

         // Lier les codes communes et codes communes parents à leurs siren
         Dataset<Row> c = this.cogCsvLoader.loadOpenData(anneeCOG, inclureCommunesDelegueesAssociees);
         Dataset<Row> s = this.datasetSirenCommunaux.rowSirenCommunes(options, historique, new SirenCommunesTriDepartementCommuneSiren(), anneeCOG);
         Dataset<Row> communes = lierCommunesEtSiren(c,s).union(lierCommunesParentesEtSiren(c,s));

         communes = communes.dropDuplicates("codeCommune", "typeCommune", "nomCommune");

         communes = populationEtStrateCommunale(communes);
         communes = liercontoursCommunes(communes, anneeCOG, verifications);

         // Associer à chaque commune son code intercommunalité, si elle en a un (les communes-communautés peuvent ne pas en avoir).
         Dataset<Row> perimetres = this.datasetPerimetres.rowPerimetres(options, historique, anneeCOG, true, new EPCIPerimetreTriCommuneMembreSirenMembre())
            .selectExpr(SIREN_COMMUNE_MEMBRE.champ(), "natureJuridique as natureJuridiqueEPCI", "sirenGroupement as codeEPCI", "nomGroupement as nomEPCI");

         Column conditionJoinPerimetres = SIREN_COMMUNE.col(communes).equalTo(SIREN_COMMUNE_MEMBRE.col(perimetres));

         verificateur().verifications("jonction communes et périmètres", communes, null, perimetres, conditionJoinPerimetres, verifications, Verification.SHOW_REJETS, Verification.COMPTAGES_ET_STATISTIQUES);
         communes = communes.join(perimetres, conditionJoinPerimetres, "left");

         // Y associer les départements.
         communes = this.datasetDepartements.withDepartement(options, historique, "codeDepartementRetabli", communes, CODE_DEPARTEMENT.champ(), null, true, anneeCOG)
            .drop("codeRegionDepartement")
            .drop("codeDepartementRetabli");

         communes.foreachPartition((ForeachPartitionFunction<Row>)partition ->
            partition.forEachRemaining(commune -> this.validator.validerRowCommune(historique, commune))
         );

         communes = communes.select(TYPE_COMMUNE.col(), CODE_COMMUNE.col(), NOM_COMMUNE.col(), POPULATION_TOTALE.col(), POPULATION_MUNICIPALE.col(),
            POPULATION_COMPTEE_A_PART.col(), CODE_REGION.col(), NOM_DEPARTEMENT.col(), ARRONDISSEMENT.col(), CODE_CANTON.col(),
            CODE_COMMUNE_PARENTE.col(), SIREN_COMMUNE.col(), CODE_EPCI.col(), NOM_EPCI.col(), NATURE_JURIDIQUE.col("EPCI"),
            STRATE_COMMUNE.col(), LATITUDE.col(), LONGITUDE.col(), SURFACE.col(), communes.col("typeNomEtCharniere"),
            communes.col("nomMajuscules"), communes.col("typeNomEtCharniereDepartement"), communes.col("codeCommuneChefLieuDepartement"), CODE_DEPARTEMENT.col());

         return communes;
      };

      // Si une restriction à quelques communes a été demandée, prendre toutes celles de leur départements
      Column postFiltre = restrictionAuxDepartementsDeCommunes(options, CODE_DEPARTEMENT, null, true);
      boolean anneeEligibleCache = options.anneeEligibleMiseEnCache(anneeCOG);

      return constitutionStandard(options, historique, worker, anneeEligibleCache, new ConditionPostFiltrageConstante(postFiltre),
         new CacheParqueteur<>(options, this.session, this.exportCSV(),
            "cog-communes", "FILTRE-annee_{0,number,#0}_COMD-COMA_{1}",
            new CogTriDepartementCommune(), anneeCOG, inclureCommunesDelegueesAssociees));
   }

   /**
    * Lier communes et siren
    * @param c Dataset Row des communes (lues du CSV)
    * @param s Dataset Row des siren de communes (lues du CSV)
    * @param verifications Vérifications à appliquer
    * @return Dataset de communes résultantes
    */
   private Dataset<Row> lierCommunesEtSiren(Dataset<Row> c, Dataset<Row> s, Verification... verifications) {
      return lierCetS(c, s, "codeCommune", verifications);
   }

   /**
    * Lier communes parentes et siren
    * @param c Dataset Row des communes (lues du CSV)
    * @param s Dataset Row des siren de communes (lues du CSV)
    * @param verifications Vérifications à appliquer
    * @return Dataset de communes résultantes
    */
   private Dataset<Row> lierCommunesParentesEtSiren(Dataset<Row> c, Dataset<Row> s, Verification... verifications) {
      return lierCetS(c, s, "codeCommuneParente", verifications);
   }

   /**
    * Lier communes et siren
    * @param c Dataset Row des communes (lues du CSV)
    * @param s Dataset Row des siren de communes (lues du CSV)
    * @param verifications Vérifications à appliquer
    * @return Dataset de communes résultantes<br>
    *    Attention, pour les COMD, COMA le CTCD (Code de la collectivité territoriale ayant les compétences) et l'arrondissement seront vides.
    */
   private Dataset<Row> lierCetS(Dataset<Row> c, Dataset<Row> s, String champ, Verification... verifications) {
      Column condition = c.col(champ).equalTo(s.col("codeCommune"));

      verificateur().verifications(MessageFormat.format("jonction communes et siren par {0}", champ),
         c, null, s, condition, verifications, Verification.SHOW_REJETS, Verification.COMPTAGES_ET_STATISTIQUES);

      // Le code région disparaît pour une COMD ou COMA dans le fichier des communes
      // mais il reste présent dans le fichier des conversions SIREN - Codes communes
      Dataset<Row> join = c.join(s, condition)
         .drop(s.col("codeCommune"))
         .drop(s.col("nomCommune"))
         .drop(c.col("codeRegion"))
         .drop(c.col("codeDepartement"));

      verificateur().verifications(MessageFormat.format("jonction communes et siren par {0}", champ),
         c, null, null, null, verifications);

      return join;
   }

   /**
    * Lier le contour des communes
    * @param communes Communes
    * @param anneeCOG Année du code officiel géographique
    * @param verifications Vérifications à appliquer
    * @return Dataset de communes résultantes, avec leur surface et contour géométrique
    */
   private Dataset<Row> liercontoursCommunes(Dataset<Row> communes, int anneeCOG, Verification... verifications) {
      // Obtenir les contours des communes.
      // "(requête SQL) contours" est la forme de substitution pour Spark. cf https://stackoverflow.com/questions/38376307/create-spark-dataframe-from-sql-query
      String format = "(select insee_com as codecommuneosm, nom as nomcommuneosm, surf_ha as surface2, " +
         "st_x(st_centroid(geom)) as longitude, st_y(st_centroid(geom)) as latitude from communes_{0,number,#0}) contours";

      String sql = MessageFormat.format(format, anneeCOG);

      // Code et nom commune OSM sont des champs transitoires qui vont être supprimés.
      final String RPL_CODE_COMMUNE_OSM = "codecommuneosm";
      final String RPL_NOM_COMMUNE_OSM = "nomcommuneosm";

      Dataset<Row> contours = sql(this.session, sql).load();
      contours = contours.withColumn("surface", col("surface2").cast(DoubleType)).drop(col("surface2"))
         .orderBy(RPL_CODE_COMMUNE_OSM);

      Column conditionJoinContours = CODE_COMMUNE.col().equalTo(col(RPL_CODE_COMMUNE_OSM));

      verificateur().verifications("jonction communes et contours communaux OSM (centroïde, surface)", communes, null, contours, conditionJoinContours, verifications, Verification.SHOW_REJETS, Verification.COMPTAGES_ET_STATISTIQUES);

      communes = communes.join(contours, conditionJoinContours, "left_outer")
         .drop(col(RPL_CODE_COMMUNE_OSM))
         .drop(col(RPL_NOM_COMMUNE_OSM));

      verificateur().verifications("jonction communes et contours communaux OSM (centroïde, surface)", communes, null, null, null, verifications);

      return communes;
   }

   /**
    * Vérifier les populations et déterminer la strate communale.
    * @param communes Dataset Row des communes.
    * @return Dataset de communes avec leurs strates communales.
    */
   private Dataset<Row> populationEtStrateCommunale(Dataset<Row> communes) {
      // Quand la population totale, municipale, ou comptée à part est à null, elles passent à zéro
      communes = communes.withColumn("populationTotale",
         when(communes.col("populationTotale").isNull(), lit(0))
            .otherwise(communes.col("populationTotale")));

      communes = communes.withColumn(POPULATION_MUNICIPALE.champ(),
         when(POPULATION_MUNICIPALE.col(communes).isNull(), lit(0))
         .otherwise(POPULATION_MUNICIPALE.col(communes)));

      communes = communes.withColumn(POPULATION_COMPTEE_A_PART.champ(),
         when(POPULATION_COMPTEE_A_PART.col(communes).isNull(), lit(0))
         .otherwise(POPULATION_COMPTEE_A_PART.col(communes)));

      // La strate communale doit concorder avec celle des comptes individuels des communes.
      communes = communes.withColumn("strateCommune",
         when(communes.col("populationTotale").between(0, 249), lit(1))           // communes de moins de 250 hab
            .when(communes.col("populationTotale").between(250, 499), lit(2))     // communes de 250 à 500 hab
            .when(communes.col("populationTotale").between(500, 1999), lit(3))    // communes de 500 à 2 000 hab
            .when(communes.col("populationTotale").between(2000, 3499), lit(4))   // communes de 2 000 à 3 500 hab
            .when(communes.col("populationTotale").between(3500, 4999), lit(5))   // communes de 3 500 à 5 000 hab
            .when(communes.col("populationTotale").between(5000, 9999), lit(6))   // communes de 5 000 à 10 000 hab
            .when(communes.col("populationTotale").between(10000, 19999), lit(7)) // communes de 10 000 à 20 000 hab
            .when(communes.col("populationTotale").between(20000, 49999), lit(8)) // communes de 20 000 à 50 000 hab
            .when(communes.col("populationTotale").between(50000, 99999), lit(9)) // communes de 50 000 à 100 000 hab
            .otherwise(lit(10)));                                                 // communes de plus de 100 000 hab

      return communes;
   }

   /**
    * Corriger les codes départements et établir les codes communes à partir du numéro de commune INSEE.
    * @param historique Historique d'exécution
    * @param nomChampCodeCommune Nom du champ code commune à alimenter, rectifié.
    * @param candidats Dataset candidats.
    * @param nomChampCodeDepartement Nom du champ code département.
    * @param nomChampCodeCommuneINSEE nom du champ code commune INSEE.
    * @return Dataset aux codeCommune établis.
    */
   public Dataset<Row> withCodeCommune(HistoriqueExecution historique, String nomChampCodeCommune, Dataset<Row> candidats, String nomChampCodeDepartement, String nomChampCodeCommuneINSEE) {
      super.session.udf().register("etablirCodeCommune", (UDF2<String, String, String>)(codeDepartement, codeINSEE) -> {
         if (StringUtils.isBlank(codeINSEE) || StringUtils.isBlank(codeDepartement)) {
            return null;
         }

         return this.validator.toCodeCommune(historique, codeDepartement, codeINSEE, true);
      }, DataTypes.StringType);

      return candidats.withColumn(nomChampCodeCommune, callUDF("etablirCodeCommune", col(nomChampCodeDepartement), col(nomChampCodeCommuneINSEE)));
   }
}
