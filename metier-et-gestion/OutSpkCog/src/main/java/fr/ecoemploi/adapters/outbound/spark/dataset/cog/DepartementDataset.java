package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;

import org.apache.commons.lang3.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.api.java.*;
import static org.apache.spark.sql.functions.*;
import org.apache.spark.sql.types.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

/**
 * Dataset des départements.
 * @author Marc Le Bihan
 */
@Service
public class DepartementDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 7887065127737403739L;
   
   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(DepartementDataset.class);

   /** Chargeur de données open data depuis le fichier CSV des départements. */
   @Autowired
   private DepartementCsvLoader loader;

   /** Nom du fichier de cache. */
   private static final String CACHE = "cog-departements";

   /**
    * Obtenir un Dataset des départements.
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param optionsCreationLecture Options de lecture et de création de dataset.
    * @param historique Historique d'exécution
    * @return Dataset des départements (en une unique partition).
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> rowDepartements(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeCOG) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.aucuneDeRegleValidation(this.session);
      }

      String store = MessageFormat.format("{0}/{1}_{2,number,#0}", options.tempDir(), CACHE, anneeCOG);

      Dataset<Row> departements = Loader.loadFromStoreParquet(options, this.session, store);
      
      if (departements != null) {
         return departements;
      }

      LOGGER.info("Constitution du dataset des départements depuis pour le Code Officiel Géographique (COG) de l'année {}...", anneeCOG);
      
      departements = this.loader.loadOpenData(anneeCOG)
         .coalesce(1) // Le dataset département n'a que cent enregistrements.
         .sort(col("codeDepartement"))
         .cache();

      Loader.saveDatasetToStore(options, departements, null, store);
      LOGGER.info("Le dataset des départements du Code Officiel Géographique de l'année {} est prêt et stocké.", anneeCOG);

      return departements;
   }
   
   /**
    * Corriger les codes départements et établir les codes communes à partir du numéro de commune INSEE.
    * @param nomChampDepartementRetabli Nom du champ du département rétabli, peut valoir null et il ne sera pas adjoint.
    * @param candidats Dataset candidat.
    * @param nomChampCodeDepartementSur3 Nom du champ contenant le code département sur trois caractères (String).
    * @param nomChampCodeCommuneINSEE Nom du champ contenant le code commune INSEE (String), peut valoir null mais alors la correction sera moins éfficace.
    * @param adjoindreDatasetDepartements true s'il faut ajoindre les informations sur les départements, depuis leur dataset, au dataset soumis.
    * @param anneeCOG Année du COG pour le chargement du dataset des départements.
    * @param optionsCreationLecture Options de lecture et de création de dataset.
    * @param historique Historique d'exécution
    * @return Dataset avec un champ codeDepartement valant 01 à 19, {2A, 2B, ou 20}, 21 à 95, 971, 972, 973, 974, 976.
    */
   public Dataset<Row> withDepartement(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique,
         String nomChampDepartementRetabli, Dataset<Row> candidats, String nomChampCodeDepartementSur3,
         String nomChampCodeCommuneINSEE, boolean adjoindreDatasetDepartements, int anneeCOG) {
      Objects.requireNonNull(candidats, "Le dataset où vérifier les codes départements ne peut pas valoir null.");
      Objects.requireNonNull(nomChampCodeDepartementSur3, "Le code département à vérifer ne peut pas valoir null.");
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.aucuneDeRegleValidation(this.session);
      }

      this.session.udf().register("corrigerCodeDepartement", (UDF2<String, String, String>)(codeDepartementSur3, codeCommuneInsee) -> {
         if (StringUtils.isBlank(codeDepartementSur3)) {
            return null;
         }
         
         // Certains encodages définissent ces départements pour l'outremer. On les corrige directement.
         // 101 Guadeloupe 
         // 102 Guyane 
         // 103 Martinique 
         // 104 Réunion
         // 106 Mayotte
         String codeDepartement;

         switch(codeDepartementSur3) {
            case "097", "97" -> {
               return corrigerCodeDepartementOutremerMalForme(codeDepartementSur3, "97", codeCommuneInsee);
            }

            case "098", "98" -> {
               return corrigerCodeDepartementOutremerMalForme(codeDepartementSur3, "98", codeCommuneInsee);
            }

            case "975" -> {
               LOGGER.warn("Code département {} invalide observé.", codeDepartementSur3);
               codeDepartement = codeDepartementSur3;
            }

            // 101 à 106 sont ceux de la balance des comptes des communes.
            case "101" -> codeDepartement = "971";
            case "103" -> codeDepartement = "972"; // 103 = Martinique !
            case "102" -> codeDepartement = "973"; // 102 = Guyane !
            case "104" -> codeDepartement = "974";
            case "105" -> {
               LOGGER.warn("Code département {} invalide observé.", codeDepartementSur3);
               codeDepartement = "975";
            }

            case "106" -> codeDepartement = "976";

            default -> codeDepartement = corrigerCodeDepartementInconnu(codeDepartementSur3);
         }
         
         return codeDepartement;
      }, DataTypes.StringType); 

      if (nomChampDepartementRetabli != null) {
         candidats = candidats.withColumn(nomChampDepartementRetabli, callUDF("corrigerCodeDepartement", col(nomChampCodeDepartementSur3),
            nomChampCodeCommuneINSEE != null ? col(nomChampCodeCommuneINSEE) : lit(null)));
         
         // Lier le contenu du dataset des départements, si désiré.
         if (adjoindreDatasetDepartements) {
            Dataset<Row> departements = rowDepartements(options, historique, anneeCOG);
            Column departementRetabli = candidats.col(nomChampDepartementRetabli);
            Column departementReferentiel = departements.col("codeDepartement");
            
            candidats = candidats.join(departements, departementRetabli.equalTo(departementReferentiel), "left_outer");
            candidats = candidats.drop(departements.col("codeDepartement"));
         }
      }
      
      return candidats;
   }

   /**
    * Tenter de corriger un code département d'outremer mal formé.
    * @param codeDepartementSur3 Code du département d'outremer, qui est mal formé.
    * @param codeDepartementRetabli Code du département que l'on rétablit.
    * @param codeCommuneInsee Code commune INSEE.
    * @return Code département corrigé.
    */
   private String corrigerCodeDepartementOutremerMalForme(String codeDepartementSur3, String codeDepartementRetabli, String codeCommuneInsee) {
      String codeDepartement = codeDepartementRetabli;

      if (codeCommuneInsee != null) {
         codeDepartement = codeDepartement + codeCommuneInsee.charAt(0);
      }

      LOGGER.warn("Un département d'outremer {} est incomplet et est retenu comme {}. Il n'y pas d'autre information pour le compléter.", codeDepartementSur3, codeDepartement);
      return codeDepartement;
   }

   /**
    * Tenter de corriger un code département inconnu.
    * @param codeDepartementSur3 Code département inconnu.
    * @return Code département corrigé.
    */
   private String corrigerCodeDepartementInconnu(String codeDepartementSur3) {
      String codeDepartement;

      switch(codeDepartementSur3.length()) {
         case 1 -> {
            codeDepartement = StringUtils.leftPad(codeDepartementSur3, 2, '0');
            LOGGER.warn("Le code département métropolitain {} devient {} : il n'est pas sur deux positions et est complété d'un zéro initial.", codeDepartementSur3, codeDepartement);
         }

         case 2 -> codeDepartement = codeDepartementSur3;

         case 3 -> {
            if (codeDepartementSur3.charAt(0) == ' ' || codeDepartementSur3.charAt(0) == '0') {
               codeDepartement = codeDepartementSur3.substring(1); // "029" ou " 29" -> "29".
               break;
            }

            if (codeDepartementSur3.charAt(2) == ' ' || codeDepartementSur3.charAt(2) == '0') {
               codeDepartement = codeDepartementSur3.substring(0, 2); // "290" ou "29 " -> "29" (on espère cette situation rare...).
               break;
            }

            if (codeDepartementSur3.startsWith("97") == false && codeDepartementSur3.startsWith("98") == false) {
               codeDepartement = codeDepartementSur3;
               LOGGER.warn("Le code département {} est incorrect, mais incorrigible (Observation 2).", codeDepartementSur3);
               break;
            }

            // Il est normal qu'un département d'outremer soit sur trois positions.
            codeDepartement = codeDepartementSur3;
         }

         default -> {
            codeDepartement = codeDepartementSur3;
            LOGGER.warn("Le code département {} est incorrect, mais incorrigible (Observation 1).", codeDepartementSur3);
         }
      }

      return codeDepartement;
   }
}
