package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.*;
import java.util.*;
import java.util.function.BooleanSupplier;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.sql.Row;
import org.jetbrains.annotations.NotNull;
import org.slf4j.*;
import org.springframework.beans.BeansException;
import org.springframework.context.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.HistoriqueExecution;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.validation.AbstractRowValidator;

import fr.ecoemploi.domain.model.territoire.*;

/**
 * Validateur de communes
 * @author Marc Le Bihan
 */
@Component
public class CogRowValidator extends AbstractRowValidator implements ApplicationContextAware {
   @Serial
   private static final long serialVersionUID = 2380346741458224161L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CogRowValidator.class);

   /** Code commune et code département sur deux caractères */
   public static final String VLD_CODE_COMMUNE_DEPT2_INSEE2 = "COG_CODE_COMMUNE_DEPT2_INSEE2";

   /** Code commune et code département sur trois caractères */
   public static final String VLD_CODE_COMMUNE_DEPT3_INSEE3 = "COG_CODE_COMMUNE_DEPT3_INSEE3";

   /** Code commune d'outremer sur trois caractères avec code département sur trois caractères */
   public static final String VLD_CODE_COMMUNE_OUTREMER_DEPT3_INSEE3 = "COG_CODE_COMMUNE_OUTREMER_DEPT3_INSEE3";

   /** Code commune et code département sont définitivement invalides, pour un autre motif */
   public static final String VLD_CODE_COMMUNE_INVALIDE = "COG_CODE_COMMUNE_DEPT_INSEE_INVALIDES";

   /** Commune active dont la population est nulle. */
   public static final String VLD_POPULATION_NULLE = "COG_POPULATION_NULLE";

   /**
    * Valider un Row de Dataset Commune
    * @param historique Historique d'exécution
    * @param commune Row commune
    * @return true si la commune est valide.
    */
   public boolean validerRowCommune(HistoriqueExecution historique, Row commune) {
      Integer populationMunicipale = commune.getAs("populationMunicipale");
      String typeCommune = commune.getAs("typeCommune");

      List<BooleanSupplier> validations = new ArrayList<>();

      // Commune active (type "COM") dont la population est nulle.
      validations.add(() -> valideSi(VLD_POPULATION_NULLE, historique, commune,
         c -> (populationMunicipale != null && populationMunicipale != 0) || "COM".equals(typeCommune) == false,
         () -> new Serializable[] {commune.getAs("nomCommune"), commune.getAs("codeCommune"), typeCommune}));

      return validations.stream().allMatch(BooleanSupplier::getAsBoolean);
   }

   /**
    * Etablir un code commune à partir de son code département et de son code INSEE.
    * @param codeDepartement Code département.
    * @param codeINSEE Code INSEE de la commune.
    * @param corriger true s'il faut corriger la valeur, si possible.<br>
    *                 false s'il faut seulement la dénoncer, si elle est invalide.
    * @return Code commune rétabli ou null s'il ne peut pas être corrigé.
    */
   protected String toCodeCommune(HistoriqueExecution historique, String codeDepartement, String codeINSEE, boolean corriger) {
      int longueurCodeDepartement = codeDepartement.length();
      int longueurCodeINSEE = codeINSEE.length();

      // Commune métropolitaine
      if (longueurCodeDepartement == 2 && longueurCodeINSEE == 3) {
         return codeDepartement + codeINSEE;
      }

      // Commune d'outremer
      if (longueurCodeDepartement == 3 && longueurCodeINSEE == 2) {
         return codeDepartement + codeINSEE;
      }

      String codeCommune = problemeINSEE2etDepartement2(historique, codeINSEE, longueurCodeINSEE, codeDepartement, longueurCodeDepartement, corriger);
      codeCommune = codeCommune == null ? problemeINSEE3etDepartementSur3(historique, codeINSEE, longueurCodeINSEE, codeDepartement, longueurCodeDepartement, corriger) : null;

      // Si nous n'avons pas su rétablir le code commune, c'est que le code département et code INSEE sont si invalides que l'on ne peut rien faire
      if (codeCommune == null)
         invalider(VLD_CODE_COMMUNE_INVALIDE, historique, () -> new Serializable[]{codeDepartement + codeINSEE, codeDepartement, codeINSEE});

      return codeCommune;
   }

   /**
    * Détecter et corriger si demandé un code INSEE et un code département tous deux de deux caractères de long.
    * @param historique Historique d'exécution.
    * @param codeDepartement Code département.
    * @param codeINSEE Code INSEE de la commune.
    * @param corriger true s'il faut corriger la valeur, si possible.<br>
    *                 false s'il faut seulement la dénoncer, si elle est invalide.
    * @param longueurCodeINSEE Longueur du code INSEE
    * @param longueurCodeDepartement longueur du code département
    * @return Code commune corrigé (on padde à gauche le code INSEE avec un zéro)<br>
    *         ou null s'il ne peut ou ne doit pas l'être
    */
   private String problemeINSEE2etDepartement2(HistoriqueExecution historique, String codeINSEE, int longueurCodeINSEE, String codeDepartement, int longueurCodeDepartement, boolean corriger) {
      if (longueurCodeDepartement == 2 && longueurCodeINSEE == 2) {
         invalider(VLD_CODE_COMMUNE_DEPT2_INSEE2, historique, () -> new Serializable[]{codeDepartement + codeINSEE, codeDepartement, codeINSEE});

         if (corriger == false)
            return null;

         // Corriger le code commune invalide.
         String codeCommune = codeDepartement + StringUtils.leftPad(codeINSEE, 3, '0');
         LOGGER.info("Le code commune est rétabli en {}. (A)", codeCommune);
         return codeCommune;
      }

      return null;
   }

   /**
    * Détecter et corriger si demandé un code INSEE et un code département tous deux de trois caractères de long.
    * @param historique Historique d'exécution.
    * @param codeDepartement Code département.
    * @param codeINSEE Code INSEE de la commune.
    * @param corriger true s'il faut corriger la valeur, si possible.<br>
    *                 false s'il faut seulement la dénoncer, si elle est invalide.
    * @param longueurCodeINSEE Longueur du code INSEE
    * @param longueurCodeDepartement longueur du code département
    * @return Code commune corrigé (on padde à gauche le code INSEE avec un zéro)<br>
    *         ou null s'il ne peut ou ne doit pas l'être
    */
   private String problemeINSEE3etDepartementSur3(HistoriqueExecution historique, String codeINSEE, int longueurCodeINSEE, String codeDepartement, int longueurCodeDepartement, boolean corriger) {
      // Si nous ne répondons pas à ce cas, nous quittons.
      if (longueurCodeDepartement != 3 || longueurCodeINSEE != 3) {
         return null;
      }

      // Commune d'outremer où le dernier chiffre (repérant le département d'outremer) est aussi le premier d'une commune à trois chiffres :
      // Si nous sommes valides du point de vue de cette règle, rien à faire
      if (codeDepartement.charAt(2) == codeINSEE.charAt(0)) {
         invalider(VLD_CODE_COMMUNE_DEPT3_INSEE3, historique, () -> new Serializable[]{codeDepartement + codeINSEE, codeDepartement, codeINSEE});

         if (corriger == false)
            return null;

         String codeCommune = codeDepartement + codeINSEE.substring(1);
         LOGGER.info("Le code commune est rétabli en {}. (B)", codeCommune);
         return codeCommune;
      }

      // TODO voir si ces codes ne sont pas plutôt des extensions de communes ou des collectivités dans ADMIN EXPRESS
      // Si le département est d'outremer, à trois caractères et que le code INSEE en a trois aussi, le premier caractère du code INSEE disparaît.
      if (CodeDepartement.estDepartementOutremer(codeDepartement)) {
         invalider(VLD_CODE_COMMUNE_OUTREMER_DEPT3_INSEE3, historique, () -> new Serializable[]{codeDepartement + codeINSEE, codeDepartement, codeINSEE});

         String codeCommune = codeDepartement + codeINSEE.substring(1);
         LOGGER.info("Le code commune est rétabli en {} (C).", codeCommune);
         return codeCommune;
      }

      return null;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
      declareRegle(VLD_CODE_COMMUNE_DEPT2_INSEE2, this.messageSource, "Code commune INSEE et code département sont sur deux caractères", String.class, String.class, String.class);
      declareRegle(VLD_CODE_COMMUNE_DEPT3_INSEE3, this.messageSource, "Code commune INSEE et code département sont sur trois caractères", String.class, String.class, String.class);
      declareRegle(VLD_CODE_COMMUNE_OUTREMER_DEPT3_INSEE3, this.messageSource, "Code commune INSEE et code département sont sur trois caractères pour un département d'Outremer", String.class, String.class, String.class);
      declareRegle(VLD_CODE_COMMUNE_INVALIDE, this.messageSource, "Code commune INSEE et code département sont défintivement invalides", String.class, String.class, String.class);
      declareRegle(VLD_POPULATION_NULLE, this.messageSource, "Une commune a une population nulle sans être déléguée ou associée", String.class, String.class, String.class);
   }
}
