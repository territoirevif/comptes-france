package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Partionnement du Code Officiel Géographique par code département<br>
 * Tri par code département et code commune.
 * @author Marc LE BIHAN
 */
public class CogTriDepartementCommune extends CogTri {
   @Serial
   private static final long serialVersionUID = -8268981108177667349L;

   /**
    * Constuire cette définition de tri.
    */
   public CogTriDepartementCommune() {
      super("PARTITION-departement-TRI-commune", true, CODE_DEPARTEMENT.champ(), false, CODE_DEPARTEMENT.champ(), CODE_COMMUNE.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec isNull
    * @param codeCommune Code commune, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommune) {
      return equalTo(codeDepartement)
         .and(codeCommune != null ? CODE_COMMUNE.col().equalTo(codeCommune) : CODE_COMMUNE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
