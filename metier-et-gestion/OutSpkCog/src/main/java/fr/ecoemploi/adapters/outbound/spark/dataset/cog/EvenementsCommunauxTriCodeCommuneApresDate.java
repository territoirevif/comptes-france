package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.Serial;
import java.time.LocalDate;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Tri d'évènements communaux par Code Commune après changement, puis date d'effet.
 * @author Marc LE BIHAN
 */
public class EvenementsCommunauxTriCodeCommuneApresDate extends EvenementsCommunauxTri {
   @Serial
   private static final long serialVersionUID = -2630506371776683410L;

   /**
    * Constuire cette définition de tri.
    */
   public EvenementsCommunauxTriCodeCommuneApresDate() {
      super("TRI-commune_apres_evenement-dateEvenement", true, null, false, CODE_COMMUNE_APRES_EVENEMENT.champ(), DATE_EFFET_EVENEMENT_COMMUNAL.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeCommune Code commune, si null sera testé avec isNull
    * @param dateEffet Date d'effet de l'évènement, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeCommune, LocalDate dateEffet) {
      return equalTo(codeCommune)
         .and(dateEffet != null ? DATE_EFFET_EVENEMENT_COMMUNAL.col().equalTo(dateEffet) : DATE_EFFET_EVENEMENT_COMMUNAL.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeCommune Code commune, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeCommune) {
      return codeCommune != null ? CODE_COMMUNE_APRES_EVENEMENT.col().equalTo(codeCommune) : CODE_COMMUNE_APRES_EVENEMENT.col().isNull();
   }
}
