package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.Serial;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.*;

import org.apache.spark.api.java.function.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.port.cog.EvenementsCommunauxRepository;
import fr.ecoemploi.domain.model.territoire.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Dataset d'évènements communaux
 * @author Marc Le Bihan
 */
@Service
public class EvenenemtsCommunauxDataset extends AbstractSparkDataset implements EvenementsCommunauxRepository {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -2830889722095385905L;

   /** Chargeur de données brutes open data */
   private final EvenementsCommunauxRowCsvLoader rowLoader;

   /** Validateur d'évènements communaux */
   private final EvenementsCommunauxRowValidator validator;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EvenenemtsCommunauxDataset.class);

   /**
    * Construire un dataset de lecture des évènements communaux.
    * @param rowLoader Chargeur de données brutes.
    * @param validator Validateur d'évènements communaux.
    */
   public EvenenemtsCommunauxDataset(EvenementsCommunauxRowCsvLoader rowLoader, EvenementsCommunauxRowValidator validator) {
      this.rowLoader = rowLoader;
      this.validator = validator;
   }

   /**
    * Obtenir le dataset des évènements communaux.
    * @param tri Tri préféré des données.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historique Historique d'exécution.
    * @return Dataset des évènements communaux.
    */
   public Dataset<Row> rowEvenementsCommunaux(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, EvenementsCommunauxTri tri) {
      OptionsCreationLecture options = optionsCreationLecture != null ? optionsCreationLecture : optionsCreationLecture();

      if (historique != null) {
         historique.addReglesValidation(this.session, this.validator);
      }

      int annee = 0;

      Supplier<Dataset<Row>> worker = () -> {
         LOGGER.info("Début de l'acquisition des évènements communaux non filtrés pour l'année {}...", annee);
         Dataset<Row> evenements = this.rowLoader.loadOpenData(annee, 0.0 /* options.sampleFraction() */, 0L/* options.sampleSeed() */);

         return evenements.filter(
            (FilterFunction<Row>) evenementCommunal -> this.validator.validerRowEvenementCommunal(historique, evenementCommunal));
      };

      return constitutionStandard(options, historique, worker,
         new CacheParqueteur<>(options, this.session, exportCSV(), "evenements_communaux", "FILTRE-annee_{0,number,#0}-bruts", tri, annee));
   }

   /* La fusion-association de Belmont avec Luthézieu donne :
      Mod 	Date d'effet 	Type commune avant 	Id commune avant 	Libellé avant 	Type commune après 	Code commune après 	Libellé après
      33 	1974-11-01 	COM 	01036 	Belmont 	COM 	01036 	Belmont-Luthézieu
      33 	1974-11-01 	COM 	01226 	Luthézieu 	COM 	01036 	Belmont-Luthézieu
      33 	1974-11-01 	COM 	01226 	Luthézieu 	COMA 	01226 	Luthézieu

      La création de la commune nouvelle d’Arboys-en-Bugey donne :
      Mod 	Date d'effet 	Type commune avant 	Id commune avant 	Libellé avant 	Type commune après 	Code commune après 	Libellé après
      32 	2016-01-01 	COM 	01015 	Arbignieu 	COM 	01015 	Arboys en Bugey
      32 	2016-01-01 	COM 	01015 	Arbignieu 	COMD 	01015 	Arbignieu
      32 	2016-01-01 	COM 	01340 	Saint-Bois 	COM 	01015 	Arboys en Bugey
      32 	2016-01-01 	COM 	01340 	Saint-Bois 	COMD 	01340 	Saint-Bois
    */

   /**
    * Redresser des communes à une date d'effet, en ramenant la commune active à cette date.
    * @param communes Communes à observer pour les ajuster si elles le réclament.
    * @param annee Année vers laquelle redresser les communes (les changements affectant celles-ci prenant place au 1er Janvier).
    * @param cogReference Communes du Code Officiel Géographique de référence pour cette année.
    * @param seuil Date de début de consultation de l'historique : les années précédent 2010 peuvent avoir des mouvements très complexes.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution.
    * @return Communes historiques associées.
    */
   public Dataset<CommuneHistorique> redresser(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution,
      Dataset<Row> communes, int annee, Dataset<Row> cogReference, LocalDate seuil) {

      // Remarque : certains évènements à prendre en compte pour l'année n sont attribués, dans les mouvements à la date d'effet 31/12/[année - 1]
      LocalDate dateEffet = LocalDate.of(annee, 1, 1);

      // Les communes qui sont restées les mêmes dans le COG de référence par rapport à celles que nous soumettons.
      Column communesIdentiques = CODE_COMMUNE.col(communes).equalTo(CODE_COMMUNE.col(cogReference))
         .and(NOM_COMMUNE.col(communes).equalTo(NOM_COMMUNE.col(cogReference)))
         .and(TYPE_COMMUNE.col(communes).equalTo(TYPE_COMMUNE.col(cogReference)));

      // Il est nécessaire de ne traiter que celles-là, car le fichier des mouvements n'est pas explicite sur les défusions
      // Et l'on peut se retrouver perdus avec une commune qui aura fusionné puis défusionné pour revenir à l'identique.
      Dataset<Row> communesQuiOntEvolue = communes.join(cogReference, communesIdentiques, "left_anti");

      Dataset<EntreeHistoriqueCommune> datasetEntreesHistorique = evenements(optionsCreationLecture, historiqueExecution);
      List<EntreeHistoriqueCommune> entreesHistorique = datasetEntreesHistorique.collectAsList();

      return communesQuiOntEvolue.map((MapFunction<Row, CommuneHistorique>) candidat -> {
         String codeCommune = CODE_COMMUNE.getAs(candidat);
         String nomCommune = NOM_COMMUNE.getAs(candidat);
         String typeCommune = TYPE_COMMUNE.getAs(candidat);

         CommuneHistorique communeHistorique = estDevenue(codeCommune, nomCommune, typeCommune, entreesHistorique, candidat, dateEffet, seuil);
         communeHistorique.setCodeCommuneHistorique(codeCommune);
         communeHistorique.setNomCommuneHistorique(nomCommune);
         communeHistorique.setTypeCommuneHistorique(typeCommune);
         return communeHistorique;
      }, Encoders.bean(CommuneHistorique.class)).orderBy("codeCommuneHistorique");
   }

   /**
    * Déterminer ce qu'est devenue une commune au 1er Janvier d'une année.
    * @param codeCommune Code de la commune initiale.
    * @param annee Année considérée.
    * @return Commune résultante ou null, s'il n'y a aucun changement.
    */
   @Override
   public CommuneHistorique estDevenue(String codeCommune, int annee) {
      LocalDate dateEffet = LocalDate.of(annee, 1, 1);
      LocalDate seuil = LocalDate.of(2010, 1, 1);

      return estDevenue(codeCommune, evenements(null, null).collectAsList(), dateEffet, seuil);
   }

   /**
    * Déterminer ce qu'est devenue une commune.
    * @param codeCommune Code de la commune initiale.
    * @param evenements Liste d'évènements.
    * @param dateEffet Date d'effet.
    * @param seuil Date de début de consultation de l'historique : les années précédent 2010 peuvent avoir des mouvements très complexes.
    * @return Commune résultante ou null, s'il n'y a aucun changement.
    */
   public CommuneHistorique estDevenue(String codeCommune, List<EntreeHistoriqueCommune> evenements, LocalDate dateEffet, LocalDate seuil) {
      // Parcourir les évènements, si notre commune est affectée par un évènement, elle devient celle d'après.
      // (la liste d'évènements est attendue triée par date d'effet)
      CommuneHistorique communeTransformee = null;
      String codeCommuneSuivi = codeCommune;

      // Ne retenir que les évènements qui sont dans la période intiale d'examen [seuil, dateEffet]
      Predicate<EntreeHistoriqueCommune> dansPeriodeInitiale =
         evenement -> (evenement.getDateEffetEvenement().isBefore(seuil) || evenement.getDateEffetEvenement().isAfter(dateEffet)) == false;

      List<EntreeHistoriqueCommune> evenementsConsideres = evenements.stream().filter(dansPeriodeInitiale).toList();

      LocalDate dernierEvenement = seuil;
      Set<EntreeHistoriqueCommune> evenementsTraites = new HashSet<>();
      boolean unEvenementTraite;

      do {
         unEvenementTraite = false;

         for(EntreeHistoriqueCommune evenement : evenementsConsideres) {
            // Si on rebalaie la liste après la prise en compte d'un évènement,
            // et que l'on tombe sur un évènement déjà traité, on l'ignore pour ne pas le reprendre en charge de nouveau.
            if (evenementsTraites.contains(evenement)) {
               continue;
            }

            // Déterminer quand débutent les changements qui portent sur la commune considérée.
            if (communeTransformee == null && evenement.getCommuneAvant().getCodeCommune().equals(codeCommuneSuivi)) {
               communeTransformee = evenement.getCommuneAvant();
            }

            // Prendre en compte, alors, tous les changements.
            if (communeTransformee != null && evenement.getCommuneAvant().getCodeCommune().equals(codeCommuneSuivi)) {
               communeTransformee = evenement.getCommuneApres();

               if (LOGGER.isDebugEnabled()) {
                  LOGGER.debug("{} : {} {} {} → {} {} {} : {}", evenement.getDateEffetEvenement().format(DateTimeFormatter.ISO_LOCAL_DATE),
                     evenement.getCommuneAvant().getCodeCommune(), evenement.getCommuneAvant().getNomCommune(), evenement.getCommuneAvant().getTypeCommune(),
                     evenement.getCommuneApres().getCodeCommune(), evenement.getCommuneApres().getNomCommune(), evenement.getCommuneApres().getTypeCommune(),
                     evenement.getTypeEvenement());
               }

               unEvenementTraite = true;
               codeCommuneSuivi = evenement.getCommuneApres().getCodeCommune();
               dernierEvenement = evenement.getDateEffetEvenement();
               evenementsTraites.add(evenement);
            }
         }

         // Les évènements portant sur le code commune maintenant suivi peuvent être parmi ceux déjà traités précédement à la même date
         // il faut donc reparcourir la liste, limitée à la nouvelle période.
         if (unEvenementTraite) {
            final LocalDate dateEvenementTraite = dernierEvenement;

            Predicate<EntreeHistoriqueCommune> dansPeriodeExamen =
               evenement -> (evenement.getDateEffetEvenement().isBefore(dateEvenementTraite) || evenement.getDateEffetEvenement().isAfter(dateEffet)) == false;

            evenementsConsideres = evenements.stream().filter(dansPeriodeExamen).toList();
         }
      }
      while(unEvenementTraite);

      if (communeTransformee != null) {
         communeTransformee.setCodeCommuneHistorique(codeCommune);
      }

      return communeTransformee;
   }

   /**
    * Déterminer ce qu'est devenue une commune (helper pour traitement de dataset de communes entier).
    * @param codeCommune Code de la commune initiale.
    * @param nomCommune Nom de la commune initiale (optionnel, peut valoir null, car informatif).
    * @param typeCommune Type de la commune initiale (optionnel, peut valoir null, car informatif).
    * @param evenements Liste d'évènements.
    * @param commune Le Row commune considéré.
    * @param dateEffet Date d'effet.
    * @param seuil Date de début de consultation de l'historique : les années précédent 2010 peuvent avoir des mouvements très complexes.
    * @return Commune résultante ou null, s'il n'y a aucun changement.
    */
   private CommuneHistorique estDevenue(String codeCommune, String nomCommune, String typeCommune, List<EntreeHistoriqueCommune> evenements, Row commune, LocalDate dateEffet, LocalDate seuil) {
      CommuneHistorique communeTransformee = estDevenue(codeCommune, evenements, dateEffet, seuil);

      // Si aucune transformation n'a eu lieu, créer une transformation sans changement.
      if (communeTransformee == null) {
         communeTransformee = new CommuneHistorique(codeCommune, nomCommune, typeCommune,
            TypeEvenementCommunal.AUCUN_CHANGEMENENT.getType(), dateEffet,
            commune.getAs("typeCommune"), codeCommune, commune.getAs("typeNomEtCharniere"), commune.getAs("nomMajuscules"), commune.getAs("nomCommune"), commune.getAs("nomCommune"));
      }

      return communeTransformee;
   }

   /**
    * Renvoyer la liste des évènements ayant affecté les communes.
    * @param optionsCreationLecture Options de création et de lecture du dataset.
    * @param historiqueExecution Historique d'exécution.
    * @return Liste des évènements.
    */
   public Dataset<EntreeHistoriqueCommune> evenements(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historiqueExecution) {
      Dataset<Row> evenements = rowEvenementsCommunaux(optionsCreationLecture, historiqueExecution, new EvenementsCommunauxTriCodeCommuneAvantDate())
         .orderBy(DATE_EFFET_EVENEMENT_COMMUNAL.champ());

      return evenements.map((MapFunction<Row, EntreeHistoriqueCommune>) evenement -> {
         int typeEvenementCommunal = TYPE_EVENEMENT_COMMUNAL.getAs(evenement);
         LocalDate dateEffetEvenement = DATE_EFFET_EVENEMENT_COMMUNAL.getAs(evenement);

         CommuneHistorique communeAvant = new CommuneHistorique(CODE_COMMUNE_AVANT_EVENEMENT.getAs(evenement), evenement.getAs("nomCommuneAvantEvenement"), evenement.getAs("typeCommuneAvantEvenement"),
            typeEvenementCommunal, dateEffetEvenement,
            evenement.getAs("typeCommuneAvantEvenement"), CODE_COMMUNE_AVANT_EVENEMENT.getAs(evenement), evenement.getAs("typeDeNomAvantEvenement"),
            evenement.getAs("nomCommuneMajusculesAvantEvenement"), evenement.getAs("nomCommuneAvantEvenement"), evenement.getAs("nomCommuneAvecArticleAvantEvenement"));

         CommuneHistorique communeApres = new CommuneHistorique(CODE_COMMUNE_AVANT_EVENEMENT.getAs(evenement), evenement.getAs("nomCommuneAvantEvenement"), evenement.getAs("typeCommuneAvantEvenement"),
            typeEvenementCommunal, dateEffetEvenement,
            evenement.getAs("typeCommuneApresEvenement"), CODE_COMMUNE_APRES_EVENEMENT.getAs(evenement), evenement.getAs("typeDeNomApresEvenement"),
            evenement.getAs("nomCommuneMajusculesApresEvenement"), evenement.getAs("nomCommuneApresEvenement"), evenement.getAs("nomCommuneAvecArticleApresEvenement"));

         return new EntreeHistoriqueCommune(typeEvenementCommunal, dateEffetEvenement, communeAvant, communeApres);
      }, Encoders.bean(EntreeHistoriqueCommune.class));
   }
}
