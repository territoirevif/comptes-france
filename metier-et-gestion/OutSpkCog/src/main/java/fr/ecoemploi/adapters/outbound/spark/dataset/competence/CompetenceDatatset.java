package fr.ecoemploi.adapters.outbound.spark.dataset.competence;

import java.io.Serial;
import java.text.MessageFormat;
import java.util.*;
import java.util.function.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;
import org.apache.spark.sql.types.StructType;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.AbstractSparkDataset;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Dataset de compétences de groupements ou de communes
 * @author Marc Le Bihan
 */
@Service
public class CompetenceDatatset extends AbstractSparkDataset {
   @Serial
   private static final long serialVersionUID = 28562560255151268L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(CompetenceDatatset.class);

   /** Chargeur de fichier CSV de compétences */
   private final CompetenceCsvLoader loader;

   /** Année du cog de départ. */
   @Value("${annee.cog.debut}")
   private int anneeCogDebut;

   /** Année du cog de fin. */
   @Value("${annee.cog.fin}")
   private int anneeCogFin;

   /**
    * Construire un dataset de compétences.
    * @param loader Chargeur de fichier CSV de compétences
    */
   @Autowired
   public CompetenceDatatset(CompetenceCsvLoader loader) {
      this.loader = loader;
   }

   /**
    * Obtenir un Dataset Row des compétences de groupements ou de communes.
    * @param tri Tri préféré des données.
    * @param optionsCreationLecture Options de constitution du dataset.
    * @param historique Historique de constitution du dataset.
    * @param anneeCog Année du COG à considérer si les paramètres <code>inclureNonEncoreApparues</code> ou <code>inclureObsoletes</code> sont à <code>true</code>
    * @param inclureNonEncoreApparues <code>true</code> pour inclure les compétences qui n'étauebt pas encore en vigueur cette année-là, et qui apparurent des années plus tard. Par défaut, à <code>false</code>
    * @param inclureObsoletes <code>true</code> pour inclure les compétences qui sont obsolètes : ont supprimées ou remplacées à cette date. Par défaut, à <code>false</code>
    * @return Dataset des compétences.
    */
   public Dataset<Row> rowCompetences(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeCog, boolean inclureNonEncoreApparues, boolean inclureObsoletes, CompetenceTri tri) {
      OptionsCreationLecture options = (optionsCreationLecture == null) ? optionsCreationLecture() : optionsCreationLecture;

      if (historique != null) {
         historique.aucuneDeRegleValidation(this.session);
      }

      Supplier<Dataset<Row>> worker = () -> {
         LOGGER.info("Constitution du dataset des compétences de groupements ou de communes par {}...", tri);
         Dataset<Row> competences = this.loader.loadOpenData(anneeCog);

         // Lister les compétences actives
         List<Row> rowCompetencesActives = competences.select(CODE_COMPETENCE.col()).collectAsList();
         List<String> codesCompetencesActives = rowCompetencesActives.stream().map(row -> (String)CODE_COMPETENCE.getAs(row)).toList();

         // Déterminer les compétences qui n'étaient pas encore apparues à cette date
         Set<String> nonEncoreApparues = new HashSet<>();

         for(int annee = anneeCog+1; annee < this.anneeCogFin; annee++) {
            List<String> arriverontCetteAnneeLa = new ArrayList<>(codesCompetencesAsList(anneeCog));
            arriverontCetteAnneeLa.removeAll(codesCompetencesActives);
            nonEncoreApparues.addAll(arriverontCetteAnneeLa);
         }

         // Et celles qui n'étaient plus en vigueur et sont obsolètes
         Set<String> obsoletes = new HashSet<>();

         for(int annee = anneeCog-1; annee >= this.anneeCogDebut; annee--) {
            List<String> disparuesCetteAnneeLa = new ArrayList<>(codesCompetencesAsList(anneeCog));
            disparuesCetteAnneeLa.removeAll(codesCompetencesActives);
            obsoletes.addAll(disparuesCetteAnneeLa);
         }

         // TODO reste à traiter le cas non applicable quand la nature du groupement l'interdit
         Column enVigueur = when(CODE_COMPETENCE.col(competences).isin(nonEncoreApparues.toArray()), lit("inconnue"))
            .when(CODE_COMPETENCE.col(competences).isin(obsoletes.toArray()), lit("obsolete"))
            .otherwise(lit("active"));

         competences = competences.withColumn(EN_VIGUEUR_COMPETENCE.champ(), enVigueur);
         return competences;
      };

      return constitutionStandard(options, historique, worker, true, null,
         new CacheParqueteur<>(options, this.session, exportCSV(),
            "cog-competences", "FILTRE-annee_{0,number,#0}-nonApparues_{1}-obsoletes_{2}",
            tri, anneeCog, inclureNonEncoreApparues, inclureObsoletes)); // Pas de filtre
   }

   /**
    * Renvoyer le tableau des codes compétences actives une année donnée.
    * @param anneeCog Année COG
    * @return Compétences qui sont effectivement mentionnés dans les fichier BANATIC des périmètres.<br>
    * Le fichier <code>codes-competences.csv</code> (BANATIC) ne les rapporte pas tous: il vaut pour les années 2020 et suivantes.
    */
   @SuppressWarnings("java:S1192") // Les codes compétences sont répétés dans tous les tableaux
   public String[] codesCompetences(int anneeCog) {
      return switch(anneeCog) {
         case 2016, 2017 -> new String[] {
            "C1004", "C1010", "C1015", "C1020", "C1025",
            "C1030", "C1502", "C1505", "C1507", "C1510",
            "C1515", "C1520", "C1525", "C1530", "C1540",
            "C2005", "C2010", "C2015", "C2510", "C2515",
            "C2520", "C2525", "C3005", "C3015", "C3020",
            "C3025", "C3210", "C3220", "C3505", "C3510",
            "C3515", "C4005", "C4006", "C4007", "C4010",
            "C4015", "C4017", "C4020", "C4025", "C4505",
            "C4510", "C4515", "C4520", "C4525", "C4530",
            "C4531", "C4532", "C4535", "C4540", "C4545",
            "C4550", "C4555", "C4560", "C5005", "C5010",
            "C5015", "C5210", "C5220", "C5505", "C5510",
            "C5515", "C5520", "C5525", "C5530", "C5535",
            "C5540", "C5545", "C5550", "C5555", "C7005",
            "C7010", "C7015", "C7020", "C7025", "C7030",
            "C9905", "C9910", "C9915", "C9920", "C9922",
            "C9925", "C9930", "C9935", "C9950", "C9999"
         };

         case 2018, 2019 -> new String[] {
            "C1004", "C1010", "C1015", "C1020", "C1025",
            "C1030", "C1502", "C1505", "C1507", "C1510",
            "C1515", "C1520", "C1525", "C1530", "C1535",
            "C1540", "C1545", "C1550", "C1555", "C1560",
            "C2005", "C2010", "C2015", "C2510", "C2515",
            "C2520", "C2525", "C3005", "C3010", "C3015",
            "C3020", "C3025", "C3210", "C3220", "C3505",
            "C3510", "C3515", "C4005", "C4006", "C4007",
            "C4008", "C4010", "C4015", "C4016", "C4017",
            "C4020", "C4025", "C4505", "C4510", "C4515",
            "C4520", "C4525", "C4530", "C4531", "C4532",
            "C4535", "C4540", "C4545", "C4550", "C4555",
            "C4560", "C5005", "C5010", "C5015", "C5210",
            "C5220", "C5505", "C5510", "C5515", "C5520",
            "C5525", "C5530", "C5535", "C5540", "C5545",
            "C5550", "C5555", "C7005", "C7010", "C7012",
            "C7015", "C7020", "C7025", "C7030", "C9905",
            "C9910", "C9915", "C9920", "C9922", "C9923",
            "C9925", "C9930", "C9935", "C9940", "C9950",
            "C9999"
         };

         case 2020, 2021, 2022, 2023, 2024 -> new String[] {
            "C1004", "C1010", "C1015", "C1020", "C1025",
            "C1030", "C1502", "C1505", "C1507", "C1510",
            "C1515", "C1520", "C1525", "C1528", "C1529",
            "C1530", "C1531", "C1532", "C1533", "C1534",
            "C1535", "C1540", "C1545", "C1550", "C1555",
            "C1560", "C2005", "C2010", "C2015", "C2510",
            "C2515", "C2520", "C2521", "C2525", "C2526",
            "C3005", "C3010", "C3015", "C3020", "C3025",
            "C3210", "C3220", "C3505", "C3510", "C3515",
            "C4005", "C4006", "C4007", "C4008", "C4010",
            "C4015", "C4016", "C4017", "C4020", "C4025",
            "C4505", "C4510", "C4515", "C4520", "C4525",
            "C4530", "C4531", "C4532", "C4535", "C4540",
            "C4545", "C4550", "C4555", "C4560", "C5005",
            "C5010", "C5015", "C5210", "C5220", "C5505",
            "C5510", "C5515", "C5520", "C5525", "C5530",
            "C5535", "C5540", "C5545", "C5550", "C5555",
            "C7005", "C7010", "C7012", "C7015", "C7020",
            "C7025", "C7030", "C9905", "C9910", "C9915",
            "C9920", "C9922", "C9923", "C9924", "C9925",
            "C9930", "C9935", "C9940", "C9950", "C9999"
         };

         default -> {
            String format = "Il n''y a pas de liste de compétences actives établie pour l''année COG {0,number,#0}.";
            String message = MessageFormat.format(format, anneeCog);
            LOGGER.error(message);

            throw new IllegalArgumentException(message);
         }
      };
   }

   /**
    * Obtenir les taux d'utilisation des compétences dans un ensemble de groupements ou périmètres, ou d'enregistrements qui contiennent
    * les champs des compétences en vigueur une année.
    * @param annee Année du COG à considérer.
    * @param perimetresOuDetenteursCompetences Dataset de périmètres ou de détenteurs de compétences
    *        (détient des champs de compétences (comme <code>C1020</code>) qui étaient en vigueur l'année considérée.
    * @return Dataset rapportant les taux d'utilisation des compétences dans ce dataset en entrée.<br>
    *         ce dataset renvoyé en retour, a ces colonnes : <code>codeCompetence</code> de type <code>String</code> et <code>tauxUtilisation</code> de type <code>Double</code>
    */
   public Dataset<Row> tauxUtilisationDesCompetences(int annee, Dataset<Row> perimetresOuDetenteursCompetences) {
      String[] codesCompetences = codesCompetences(annee);
      perimetresOuDetenteursCompetences = perimetresOuDetenteursCompetences.select(competencesAsColumns(annee));

      // Convertir les compétences en entier
      for(String codeCompetence : codesCompetences) {
         perimetresOuDetenteursCompetences = perimetresOuDetenteursCompetences.withColumn(codeCompetence, col(codeCompetence).cast(IntegerType));
      }

      // L'appel de la fonction agg(col, col[]) n'est pas facile à faire. Il faut séparer le premier paramètre de tous les autres
      Column colCumulPermiereCompetence = sum(codesCompetences[0]).as(codesCompetences[0]);
      List<Column> colCumulAutresCompetences = new ArrayList<>();

      for(int index=1; index < codesCompetences.length; index++) {
         colCumulAutresCompetences.add(sum(codesCompetences[index]).as(codesCompetences[index]));
      }

      // Réaliser le cumul de chaque compétence sur tout le dataset pour les compter
      Dataset<Row> tauxCompetences = perimetresOuDetenteursCompetences.agg(colCumulPermiereCompetence, colCumulAutresCompetences.toArray(new Column[0]));
      long count = perimetresOuDetenteursCompetences.count();

      // Passer du nombre au taux
      for(String codeCompetence : codesCompetences) {
         tauxCompetences = tauxCompetences.withColumn(codeCompetence, col(codeCompetence).cast(DoubleType).divide(count));
      }

      tauxCompetences = tauxCompetences.withColumn("arrayTauxCompetences", competencesAsColumnSparkArray(annee, "c"));

      StructType schema = new StructType()
         .add("codeCompetence", StringType)
         .add("tauxUtilisation", DoubleType);

      tauxCompetences = tauxCompetences.transform(dsTauxUtilisationsCompetences -> {
         Row rowTauxUtilisationsCompetences = dsTauxUtilisationsCompetences.first();
         List<Row> rows = new ArrayList<>();

         for (String codeCompetence : codesCompetences(annee)) {
            rows.add(RowFactory.create(codeCompetence, rowTauxUtilisationsCompetences.getAs(codeCompetence)));
         }

         return this.session.createDataset(rows, Encoders.row(schema));
      });

      return tauxCompetences;
   }

   /**
    * Renvoyer la liste des codes compétences actives une année donnée.
    * @param anneeCog Année COG
    * @return Compétences qui sont effectivement mentionnés dans les fichier BANATIC des périmètres.<br>
    * Le fichier <code>codes-competences.csv</code> (BANATIC) ne les rapporte pas tous: il vaut pour les années 2020 et suivantes.
    */
   public List<String> codesCompetencesAsList(int anneeCog) {
      return Arrays.asList(codesCompetences(anneeCog));
   }

   /**
    * Renvoyer une colonne rapportant toutes les compétences des groupements sous la forme d'un <code>array Scala/Spark</code>.
    * @param anneeCog Année COG
    * @param nomChampArray Nom du champ de type <code>array</code> à produire.
    * @return Colonne de type <code>Array</code> rapportant toutes les compétences
    */
   public Column competencesAsColumnSparkArray(int anneeCog, String nomChampArray) {
      Column[] colonnesCompetences = competencesAsColumns(anneeCog);
      return array(colonnesCompetences).name(nomChampArray);
   }

   /**
    * Renvoyer un tableau de colonnes rapportant toutes les compétences des groupements ou périmètres.
    * @param anneeCog Année COG
    * @return tableau de <code>Column Spark</code> rapportant toutes les compétences.
    */
   public Column[] competencesAsColumns(int anneeCog) {
      String[] codesCompetences = codesCompetences(anneeCog);
      Column[] colonnesCompetences = new Column[codesCompetences.length];

      for(int index=0; index < codesCompetences.length; index++) {
         colonnesCompetences[index] = col(codesCompetences[index]);
      }

      return colonnesCompetences;
   }

   /**
    * Renvoyer une liste de colonnes rapportant toutes les compétences des groupements ou périmètres.
    * @param anneeCog Année COG
    * @return Liste de <code>Column Spark</code> rapportant toutes les compétences
    */
   public List<Column> competencesAsColumnsList(int anneeCog) {
      String[] codesCompetences = codesCompetences(anneeCog);
      Column[] colonnesCompetences = new Column[codesCompetences.length];

      for(int index=0; index < codesCompetences.length; index++) {
         colonnesCompetences[index] = col(codesCompetences[index]);
      }

      return Arrays.asList(colonnesCompetences);
   }

   /* Remarque : les codes compétences du fichier codes-competences.csv de BANATIC
      ne présentent pas toutes les compétences actives (de 2020 et après)
      "C1004", "C1010", "C1020", "C1025", "C1030",
      "C1502", "C1505", "C1507", "C1510", "C1520",
      "C1525", "C1528", "C1529", "C1531", "C1532",
      "C1533", "C1534", "C1535", "C1540", "C1545",
      "C1550", "C1555", "C1560", "C2005", "C2010",
      "C2015", "C2510", "C2515", "C2520", "C2521",
      "C2525", "C2526", "C3005", "C3010", "C3505",
      "C4008", "C4010", "C4015", "C4016", "C4017",
      "C4020", "C4025", "C4505", "C4510", "C4515",
      "C4520", "C4525", "C4530", "C4531", "C4532",
      "C4535", "C4550", "C4555", "C4560", "C5005",
      "C5010", "C5015", "C5210", "C5220", "C5505",
      "C5510", "C5515", "C5520", "C5525", "C5530",
      "C5535", "C5540", "C5545", "C5550", "C5555",
      "C7005", "C7010", "C7012", "C7015", "C7020",
      "C7025", "C7030", "C9905", "C9910", "C9915",
      "C9920", "C9922", "C9923", "C9924", "C9925",
      "C9930", "C9935", "C9940", "C9950", "C9999"

      Il leur manque de 2020 à 2024 l'énumération de celles-ci :
      "C1015", "C1515", "C1530",
      "C3015", "C3020", "C3025", "C3210", "C3220", "C3510", "C3515",

      "C4005", "C4006", "C4007", "C4020", "C4025",
      "C4540", "C4545"

C1015=Production, distribution d''énergie (OBSOLETE)
C1515=Traitement des déchets des ménages et déchets assimilés
C1530=Gestion des milieux aquatiques et prévention des inondations (GEMAPI) (OBSOLETE)
C3015=PLIE (plan local pour l''insertion et l''emploi)
C3020=CUCS (contrat urbain de cohésion sociale)
C3025=Rénovation urbaine (ANRU)
C3210=Conseil intercommunal de sécurité et de prévention de la délinquance
C3220=Contrat local de sécurité transports
C3515=Action de développement économique (soutien des activités industrielles, commerciales ou de l''emploi, soutien des activités agricoles et forestières)
C4005=Construction ou aménagement, entretien, gestion d''équipements ou d''établissements culturels, socioculturels, socio-éducatifs, sportifs (OBSOLETE)
C4006=Construction, aménagement, entretien, gestion d''équipements ou d''établissements culturels, socio-culturels, socio-éducatifs
C4007=Construction, aménagement, entretien, gestion d''équipements ou d''établissements sportifs
C4025=Activités sportives
C4540=Voies navigables et ports intérieurs (OBSOLETE)
C4545=Aménagement rural (OBSOLETE)
    */
}
