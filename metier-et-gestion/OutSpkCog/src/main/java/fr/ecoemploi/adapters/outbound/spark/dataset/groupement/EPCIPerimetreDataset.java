package fr.ecoemploi.adapters.outbound.spark.dataset.groupement;

import java.io.*;
import java.util.*;
import java.util.function.Supplier;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;
import org.apache.spark.sql.api.java.*;
import static org.apache.spark.sql.functions.*;
import org.apache.spark.sql.types.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.cog.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.competence.CompetenceDatatset;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;
import fr.ecoemploi.domain.model.territoire.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;
import static org.apache.spark.sql.types.DataTypes.StringType;

/**
 * Dataset de périmètres d'intercommunalités
 * @author Marc Le Bihan
 */
@Service
public class EPCIPerimetreDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -3009769848885096473L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EPCIPerimetreDataset.class);
   
   /** Dataset des SIREN communaux. */
   private final SirenCommunesDataset datasetSirenCommunaux;

   /** Dataset des compétences de groupements ou communes */
   private final CompetenceDatatset datasetCompetence;

   /** Chargeur de fichier CSV de périmètres */
   private final EPCIPerimetreCsvLoader loader;

   /** Validateur d'enregistrement de périmètre */
   private final EPCIPerimetreRowValidator validator;

   /**
    * Créer un dataset de périmètres d'EPCI et de syndicats
    * @param datasetSirenCommunaux Dataset des siren communaux
    * @param datasetCompetence Dataset des compétences des groupements ou communes
    * @param loader Chargeur de fichier CSV
    * @param validator Validateur d'enregistrement de périmètre
    */
   @Autowired
   public EPCIPerimetreDataset(SirenCommunesDataset datasetSirenCommunaux, CompetenceDatatset datasetCompetence, EPCIPerimetreCsvLoader loader, EPCIPerimetreRowValidator validator) {
      this.datasetSirenCommunaux = datasetSirenCommunaux;
      this.datasetCompetence = datasetCompetence;
      this.loader = loader;
      this.validator = validator;
   }

   // TODO Voir mise en forme proposée par BANATIC :
   // https://www.banatic.interieur.gouv.fr/V5/recherche-de-groupements/fiche-raison-sociale.php?siren=242900645&arch=01/04/2021&dcou=

   /**
    * Obtenir un Dataset Row des périmètres d'intercommunalités et de syndicats.
    * @param anneeCOG année du COG.
    * @param epciSeulement true, si l'on désire seulement les intercommunalités. false, pour avoir aussi les autres groupements (syndicats).
    * @param tri Tri préféré des données.
    * @param optionsCreationLecture Options de constitution du dataset.
    * @param historique Historique de constitution du dataset.
    * @return Dataset des groupements d'intercommunalités.
    */
   public Dataset<Row> rowPerimetres(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, int anneeCOG, boolean epciSeulement, EPCIPerimetreTri tri) {
      OptionsCreationLecture options = (optionsCreationLecture == null) ? optionsCreationLecture() : optionsCreationLecture;

      if (historique != null) {
         historique.aucuneDeRegleValidation(this.session);
      }

      Supplier<Dataset<Row>> worker = () -> {
         LOGGER.info("Constitution du dataset des périmètres de groupements (intercommunalités seulement : {}) par {} pour l'année {}...", epciSeulement, tri, anneeCOG);

         // Obtention des SIREN des communes d'après le COG.
         Dataset<Row> sirenCommunes = this.datasetSirenCommunaux.rowSirenCommunes(options, historique, new SirenCommunesTriSiren(), anneeCOG);
         final String EXTRAIRE_CODE = "extraireCode";

         this.session.udf().register(EXTRAIRE_CODE, (UDF1<String, String>) valeur -> {
            if (valeur == null) {
               return null;
            }

            String[] parties = valeur.split(" - ");
            return parties[0].trim();
         }, DataTypes.StringType);

         Dataset<Row> perimetres = this.loader.loadOpenData(anneeCOG)
            .withColumn(CODE_REGION.champ(), callUDF(EXTRAIRE_CODE, CODE_REGION.col()))
            .withColumn(CODE_DEPARTEMENT.champ(), callUDF(EXTRAIRE_CODE, CODE_DEPARTEMENT.col()))
            .withColumn("arrondissementSiege", callUDF(EXTRAIRE_CODE, col("arrondissementSiege")))
            .withColumn(SIREN_COMMUNE_SIEGE.champ(), callUDF(EXTRAIRE_CODE, SIREN_COMMUNE_SIEGE.col()));

         Column joinCommuneMembre = SIREN_COMMUNE_MEMBRE.col(perimetres).equalTo(SIREN_COMMUNE.col(sirenCommunes));

         perimetres = perimetres.join(sirenCommunes, joinCommuneMembre, "left_outer")
            .withColumnRenamed(CODE_COMMUNE.champ(), CODE_COMMUNE_MEMBRE.champ())
            .withColumnRenamed(NOM_COMMUNE.champ(), NOM_COMMUNE_MEMBRE.champ())
            .withColumnRenamed(POPULATION_TOTALE.champ(), POPULATION_COMMUNE_MEMBRE_TOTALE.champ())
            .withColumnRenamed(POPULATION_MUNICIPALE.champ(), POPULATION_COMMUNE_MEMBRE_MUNICIPALE.champ())
            .withColumnRenamed(POPULATION_COMPTEE_A_PART.champ(), POPULATION_COMMUNE_MEMBRE_COMPTEE_A_PART.champ())

            .drop(CODE_REGION.col(perimetres))
            .drop(CODE_DEPARTEMENT.col(perimetres))
            .drop(SIREN_COMMUNE.col(sirenCommunes));

         Column joinCommuneSiege = SIREN_COMMUNE_SIEGE.col(perimetres).equalTo(SIREN_COMMUNE.col(sirenCommunes));

         perimetres = perimetres.join(sirenCommunes, joinCommuneSiege, "left_outer")
            .withColumnRenamed(CODE_COMMUNE.champ(), CODE_COMMUNE_SIEGE.champ())
            .withColumnRenamed(NOM_COMMUNE.champ(), NOM_COMMUNE_SIEGE.champ())
            .withColumnRenamed(POPULATION_TOTALE.champ(), POPULATION_COMMUNE_SIEGE_TOTALE.champ())
            .withColumnRenamed(POPULATION_MUNICIPALE.champ(), POPULATION_COMMUNE_SIEGE_MUNICIPALE.champ())
            .withColumnRenamed(POPULATION_COMPTEE_A_PART.champ(), POPULATION_COMMUNE_SIEGE_COMPTEE_A_PART.champ())

            .drop(CODE_REGION.col(perimetres))
            .drop(CODE_DEPARTEMENT.col(perimetres))
            .drop(SIREN_COMMUNE.col(sirenCommunes));

         // FIXME En 2019, Choisy-le-Roi est inscrit comme EPT lié à la fois à Grand-Orly Seine, ce qui est exact, mais aussi d'Est-Ensemble comme EPT, ce qui n'est pas vrai.
         Column eptChoisyLeRoiConfusion = NATURE_JURIDIQUE.col().equalTo("EPT").and(SIREN_GROUPEMENT.col(perimetres).equalTo("200057875"));

         if (epciSeulement) {
            perimetres = perimetres.where(NATURE_JURIDIQUE.col().isin("CA", "CC", "CU", "EPT", "METRO", "MET69")
               .and(not(eptChoisyLeRoiConfusion)));
         }

         perimetres = perimetres.filter((FilterFunction<Row>)perimetre -> this.validator.validerPerimetre(historique, perimetre, anneeCOG));
         perimetres = ajouterCompetencesPasEncoreExistantesOuDisparues(perimetres, anneeCOG);

         perimetres = perimetres.select(SIREN_GROUPEMENT.col(), NOM_GROUPEMENT.col(), POPULATION_GROUPEMENT.col(), NATURE_JURIDIQUE.col(), CODE_COMMUNE_SIEGE.col(), NOM_COMMUNE_SIEGE.col(),
            SIREN_COMMUNE_SIEGE.col(), ARRONDISSEMENT_SIEGE.col(), POPULATION_COMMUNE_SIEGE_TOTALE.col(), POPULATION_COMMUNE_SIEGE_MUNICIPALE.col(), POPULATION_COMMUNE_SIEGE_COMPTEE_A_PART.col(),
            col("nombreCommunesMembres"), col("nombreDeCompetencesExercees"), col("dateCreation"), col("dateEffet"),
            col("modeRepartitionSieges"), col("autreModeRepartitionSieges"), col("modeDeFinancement"), col("syndicatCarte"), col("interdepartemental"),
            col("DGFBonifiee"), col("DSC"), col("REOM"), col("TEOM"), col("autreTaxe"),
            col("autreRedevance"), col("competenceConservee"), col("type"), SIREN_MEMBRE.col(), NOM_MEMBRE.col(),
            POPULATION_MEMBRE.col(), CODE_COMMUNE_MEMBRE.col(), NOM_COMMUNE_MEMBRE.col(), SIREN_COMMUNE_MEMBRE.col(), POPULATION_COMMUNE_MEMBRE_TOTALE.col(),
            POPULATION_COMMUNE_MEMBRE_MUNICIPALE.col(), POPULATION_COMMUNE_MEMBRE_COMPTEE_A_PART.col(), SIREN_ADHESION.col(), NOM_ADHESION.col(),
            POPULATION_ADHESION.col(), col("adresseSiege1"), col("adresseSiege2"), col("adresseSiege3"), col("codePostalEtVilleSiege"),
            col("telephoneSiege"), col("faxSiege"), col("courrielSiege"), col("siteInternet"), col("annexeAdresse1"),
            col("annexeAdresse2"), col("annexeAdresse3"), col("codePostalEtVilleAnnexe"), col("telephoneAnnexe"), col("faxAnnexe"),
            col("civilitePresident"), col("prenomPresident"), col("nomPresident"), col("representationSubstitution"), col("C1004"),
            col("C1010"), col("C1015"), col("C1020"), col("C1025"), col("C1030"),
            col("C1502"), col("C1505"), col("C1507"), col("C1510"), col("C1515"),
            col("C1520"), col("C1525"), col("C1528"), col("C1529"), col("C1530"),
            col("C1531"), col("C1532"), col("C1533"), col("C1534"), col("C1535"),
            col("C1540"), col("C1545"), col("C1550"), col("C1555"), col("C1560"),
            col("C2005"), col("C2010"), col("C2015"), col("C2510"), col("C2515"),
            col("C2520"), col("C2521"), col("C2525"), col("C2526"), col("C3005"),
            col("C3010"), col("C3015"), col("C3020"), col("C3025"), col("C3210"),
            col("C3220"), col("C3505"), col("C3510"), col("C3515"), col("C4005"),
            col("C4006"), col("C4007"), col("C4008"), col("C4010"), col("C4015"),
            col("C4016"), col("C4017"), col("C4020"),
            col("C4025"), col("C4505"), col("C4510"), col("C4515"), col("C4520"),
            col("C4525"), col("C4530"), col("C4531"), col("C4532"), col("C4535"),
            col("C4540"), col("C4545"), col("C4550"), col("C4555"), col("C4560"),
            col("C5005"), col("C5010"), col("C5015"), col("C5210"), col("C5220"),
            col("C5505"), col("C5510"), col("C5515"), col("C5520"), col("C5525"),
            col("C5530"), col("C5535"), col("C5540"), col("C5545"), col("C5550"),
            col("C5555"), col("C7005"), col("C7010"), col("C7012"), col("C7015"),
            col("C7020"), col("C7025"), col("C7030"), col("C9905"), col("C9910"),
            col("C9915"), col("C9920"), col("C9922"), col("C9923"), col("C9924"),
            col("C9925"), col("C9930"), col("C9935"), col("C9940"), col("C9950"),
            col("C9999"), CODE_REGION.col(), CODE_DEPARTEMENT.col()
         );

         return perimetres;
      };

      Column postFiltre = options.hasRestrictionAuxCommunes() ?
         CODE_COMMUNE_MEMBRE.col().isin(options.restrictionAuxCommunes().toArray()).or(CODE_COMMUNE_SIEGE.col().isin(options.restrictionAuxCommunes().toArray())) : null;

      boolean anneeEligibleCache = options.anneeEligibleMiseEnCache(anneeCOG);

      return constitutionStandard(options, historique, worker, anneeEligibleCache, new ConditionPostFiltrageConstante(postFiltre),
         new CacheParqueteur<>(options, this.session, exportCSV(),
            "cog-intercommunalites-perimetres", "FILTRE-annee_{0,number,#0}-epci_seulement_{1}",
            tri, anneeCOG, epciSeulement));
   }

   /**
    * Obtenir les compétences obligatoires ou obsolètes d'un ensemble de groupements ou périmètres parmi des compétences, considérant l'année en vigueur.
    * @param annee Année du COG à considérer.
    * @param perimetres Dataset des groupements et périmètres.
    * @param obligatoires Les compétences l'on veut énumérer :
    *        <li><code>true</code> celles qui sont servies par <u>tous</u> les groupements ou périmètres (taux d'utilisation = 1.0) parmi les groupements ou périmètres</li>
    *        <li><code>false</code> celles qui le ne sont par aucune (taux d'utilisation = 0.0)</li>
    * @param naturesJuridiques Natures juridiques des groupements des périmètres auxquels se restreindre, si souhaité.
    * @return Ensemble des codes compétences qui répondent aux critères.
    */
   public Set<String> competencesObligatoiresOuObsoletes(int annee, Dataset<Row> perimetres, boolean obligatoires, String... naturesJuridiques) {
      return competencesObligatoiresOuObsoletes(annee, perimetres, obligatoires, obligatoires ? 1 : 0, naturesJuridiques);
   }

   /**
    * Obtenir les compétences obligatoires ou obsolètes d'un ensemble de groupements ou périmètres parmi des compétences, considérant l'année en vigueur.
    * @param annee Année du COG à considérer.
    * @param perimetres Dataset des groupements et périmètres.
    * @param superieurOuEgal Sens de comparaison du seuil pour filtrer les compétences :
    *        <li><code>true</code> celles servies par un taux supérieur ou égal au <code>seuil</code> parmi les groupements ou périmètres</li>
    *        <li><code>false</code> celles servies par un taux inférieur ou égal</li>
    * @param seuil Seuil à considérer.
    * @param naturesJuridiques Natures juridiques des groupements des périmètres auxquels se restreindre, si souhaité.
    * @return Ensemble des codes compétences qui répondent aux critères.
    */
   public Set<String> competencesObligatoiresOuObsoletes(int annee, Dataset<Row> perimetres, boolean superieurOuEgal, double seuil, String... naturesJuridiques) {
      Dataset<Row> tauxUtilisationCompetences = tauxUtilisationDesCompetences(annee, perimetres, naturesJuridiques);

      // Filter les utilisations de compétences avec le seuil et le sens demandés
      Column conditionSeuil =  superieurOuEgal ? col("tauxUtilisation").geq(seuil) : col("tauxUtilisation").leq(seuil);
      List<Row> rowCompetences = tauxUtilisationCompetences.where(conditionSeuil).select("codeCompetence").collectAsList();

      Set<String> competences = new HashSet<>();
      rowCompetences.forEach(row -> competences.add(row.getAs("codeCompetence")));
      return competences;
   }

   /**
    * Obtenir les taux d'utilisation des compétences dans un ensemble de groupements ou périmètres, ou d'enregistrements qui contiennent
    * les champs des compétences en vigueur une année.
    * @param annee Année du COG à considérer.
    * @param perimetres Dataset des groupements et périmètres.
    * @param naturesJuridiques Natures juridiques des groupements des périmètres auxquels se restreindre, si souhaité.
    * @return Dataset rapportant les taux d'utilisations des compétences dans ce dataset en entrée.<br>
    *         ce dataset renvoyé en retour, a ces colonnes : <code>codeCompetence</code> de type <code>String</code> et <code>tauxUtilisation</code> de type <code>Double</code>
    */
   public Dataset<Row> tauxUtilisationDesCompetences(int annee, Dataset<Row> perimetres, String... naturesJuridiques) {
      // Filtrer les périmètres en entrée, si des natures juridiques nous restreignent
      if (naturesJuridiques != null && naturesJuridiques.length > 0) {
         perimetres = perimetres.where(NATURE_JURIDIQUE.col().isin((Object[])naturesJuridiques));
      }

      return this.datasetCompetence.tauxUtilisationDesCompetences(annee, perimetres);
   }

   /**
    * Ajouter les compétences qui à une année COG données n'existaient pas encore ou ont disparues, en les valorisant à vide.
    * @param perimetres Périmètres de groupement à compléter des compétences.
    * @param anneeCOG Année du COG.
    * @return Périmètres de groupements avec les compétences qui n'existaient pas encore ou n'existe plus, à cette date là.
    */
   private Dataset<Row> ajouterCompetencesPasEncoreExistantesOuDisparues(Dataset<Row> perimetres, int anneeCOG) {
      if (anneeCOG < 2020) {
         perimetres = perimetres.withColumn("C1528", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C1529", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C1531", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C1532", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C1533", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C1534", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C2521", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C2526", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C9924", lit(null).cast(StringType));
      }

      if (anneeCOG < 2018) {
         perimetres = perimetres.withColumn("C1535", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C1545", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C1550", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C1555", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C1560", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C3010", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C4008", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C4016", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C7012", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C9923", lit(null).cast(StringType));
         perimetres = perimetres.withColumn("C9940", lit(null).cast(StringType));
      }

      return perimetres;
   }

   /**
    * Charger la liste des intercommunalités.
    * @param communes Communes.
    * @param anneeCOG Année du COG.
    * @return Intercommunalités.
    */
   public Intercommunalites intercommunalites(Communes communes, int anneeCOG) {
      // Obtention des périmètres et construction de la liste des intercommunalités.
      Dataset<Row> groupementsEtPerimetres = rowPerimetres(optionsCreationLecture(), new HistoriqueExecution(), anneeCOG, true, new EPCIPerimetreTriCommuneSiegeCommuneMembreSirenMembre());
      List<Row> rowsIntercommunalites = groupementsEtPerimetres.collectAsList();

      Intercommunalites intercommunalites = new Intercommunalites();

      for(Row row : rowsIntercommunalites) {
         String sirenIntercommunalite = row.getAs("sirenGroupement");
         NatureJuridiqueIntercommunalite natureJuridique;

         try {
            natureJuridique = NatureJuridiqueIntercommunalite.fromCode(NATURE_JURIDIQUE.getAs(row));
         }
         catch(@SuppressWarnings("unused") NatureJuridiqueGroupementInconnueException e) {
            LOGGER.warn("La nature juridique de l'intercommunalité, '{}', est inconnue pour le groupement '{}' de SIREN {}.",
               NATURE_JURIDIQUE.getAs(row), NOM_GROUPEMENT.getAs(row), SIREN_COMMUNE_SIEGE.getAs(row));
            continue;
         }

         String codeCommuneSiege = CODE_COMMUNE_SIEGE.getAs(row);
         Commune communeSiege = communes.get(codeCommuneSiege);

         String codeCommuneMembre = CODE_COMMUNE_MEMBRE.getAs(row);
         Commune communeMembre = communes.get(codeCommuneMembre);

         // Si la commune siège ou membre ne sont pas retrouvées, c'est une erreur, car on les a recherchées par jointure auparavant.
         if (communeSiege == null) {
            LOGGER.error("La commune siège de code {} (siren : {}) de l'intercommunalité {} aurait dû être trouvée dans la liste des communes du COG {}.",
               codeCommuneSiege, SIREN_COMMUNE_SIEGE.getAs(row), sirenIntercommunalite, anneeCOG);
            continue;
         }

         if (communeMembre == null) {
            LOGGER.error("La commune membre de code {} (siren : {}), membre de l'intercommunalité {} (commune siège {}, de siren {}) aurait dû être trouvée dans la liste des communes du COG {}.",
               codeCommuneMembre, SIREN_COMMUNE_MEMBRE.getAs(row), sirenIntercommunalite, codeCommuneSiege, SIREN_COMMUNE_SIEGE.getAs(row), anneeCOG);
            continue;
         }

         communeSiege.ajouterSiegeGroupement(sirenIntercommunalite);
         communeMembre.ajouterMembreGroupement(sirenIntercommunalite);

         // Si l'intercommunalité n'est pas encore connue, l'initialiser et l'ajouter à notre liste.
         Intercommunalite intercommunalite = intercommunalites.get(sirenIntercommunalite);

         if (intercommunalite == null) {
            intercommunalite = new Intercommunalite();
            intercommunalite.setCodeCommuneSiege(codeCommuneSiege);
            Adresse adresseSiege = new Adresse();

            String codePostalEtVilleSiege = row.getAs("codePostalEtVilleSiege");

            if (codePostalEtVilleSiege != null) {
               int indexSpace = codePostalEtVilleSiege.indexOf(" ");
               String codePostal = indexSpace != -1 ? codePostalEtVilleSiege.substring(indexSpace) : codePostalEtVilleSiege;
               adresseSiege.setCodePostal(codePostal);
            }

            intercommunalite.setNomCommuneSiege(NOM_COMMUNE_SIEGE.getAs(row));
            intercommunalite.setAdresseSiege(adresseSiege);
            intercommunalite.setCivilite(row.getAs("civilitePresident"));

            // Attribuer ses compétences à l'intercommunalité.
            // TODO Ces codes sont dans un fichier à télécharger, à cette adresse : https://www.data.gouv.fr/fr/datasets/base-nationale-sur-les-intercommunalites/
            String[] codesCompetences = {
               "C1004", "C1010", "C1015", "C1020", "C1025",
               "C1030", "C1502", "C1505", "C1507", "C1510",
               "C1515", "C1520", "C1525", "C1530", /*"C1535",*/
               "C1540", /* "C1545",*/ /*"C1550", */ /* "C1555", */ /* "C1560", */
               "C2005", "C2010", "C2015", "C2510", "C2515",
               "C2520", "C2525", "C3005", /*"C3010", */ "C3015",
               "C3020", "C3025", "C3210", "C3220", "C3505",
               "C3510", "C3515", "C4005", "C4006", "C4007",
               /* "C4008", */ "C4010", "C4015", /* "C4016", */ "C4017",
               "C4020", "C4025", "C4505", "C4510", "C4515",
               "C4520", "C4525", "C4530", "C4531", "C4532",
               "C4535", "C4540", "C4545", "C4550", "C4555",
               "C4560", "C5005", "C5010", "C5015", "C5210",
               "C5220", "C5505", "C5510", "C5515", "C5520",
               "C5525", "C5530", "C5535", "C5540", "C5545",
               "C5550", "C5555", "C7005", "C7010", /* "C7012", */
               "C7015", "C7020", "C7025", "C7030", "C9905",
               "C9910", "C9915", "C9920", "C9922", /* "C9923", */
               "C9925", "C9930", "C9935", /* "C9940", */ "C9950", "C9999"
            };

            /* Les codes compétences du fichier codes-competences.csv de BANATIC sont :
               "C1004", "C1010", "C1020", "C1025", "C1030",
               "C1502", "C1505", "C1507", "C1510", "C1520",
               "C1525", "C1528", "C1529", "C1531", "C1532",
               "C1533", "C1534", "C1535", "C1540", "C1545",
               "C1550", "C1555", "C1560", "C2005", "C2010",
               "C2015", "C2510", "C2515", "C2520", "C2521",
               "C2525", "C2526", "C3005", "C3010", "C3505",
               "C4008", "C4010", "C4015", "C4016", "C4017",
               "C4020", "C4025", "C4505", "C4510", "C4515",
               "C4520", "C4525", "C4530", "C4531", "C4532",
               "C4535", "C4550", "C4555", "C4560", "C5005",
               "C5010", "C5015", "C5210", "C5220", "C5505",
               "C5510", "C5515", "C5520", "C5525", "C5530",
               "C5535", "C5540", "C5545", "C5550", "C5555",
               "C7005", "C7010", "C7012", "C7015", "C7020",
               "C7025", "C7030", "C9905", "C9910", "C9915",
               "C9920", "C9922", "C9923", "C9924", "C9925",
               "C9930", "C9935", "C9940", "C9950", "C9999"
             */

            /* Ceux observés dans le dernier fichier des périmètres de 2024 :
               C1004	C1010	C1015	C1020	C1025
               C1030	C1502	C1505	C1507	C1510
               C1515	C1520	C1525	C1528	C1529
               C1530	C1531	C1532	C1533	C1534
               C1535	C1540	C1545	C1550	C1555
               C1560	C2005	C2010	C2015	C2510
               C2515	C2520	C2521	C2525	C2526
               C3005	C3010	C3015	C3020	C3025
               C3210	C3220	C3505	C3510	C3515
               C4005	C4006	C4007	C4008	C4010
               C4015	C4016	C4017	C4020	C4025
               C4505	C4510	C4515	C4520	C4525
               C4530	C4531	C4532	C4535	C4540
               C4545	C4550	C4555	C4560	C5005
               C5010	C5015	C5210	C5220	C5505
               C5510	C5515	C5520	C5525	C5530
               C5535	C5540	C5545	C5550	C5555
               C7005	C7010	C7012	C7015	C7020
               C7025	C7030	C9905	C9910	C9915
               C9920	C9922	C9923	C9924	C9925
               C9930	C9935	C9940	C9950	C9999
             */

            for(String codeCompetence : codesCompetences) {
               detecterCompetenceIntercommunalite(row, intercommunalite, codeCompetence);
            }

            intercommunalite.setEmailSiege(row.getAs("courrielSiege"));
            intercommunalite.setNatureJuridique(natureJuridique);

            intercommunalite.setNombreCommunesMembresDeclare(row.getAs("nombreCommunesMembres"));
            intercommunalite.setNomGroupement(NOM_GROUPEMENT.getAs(row));
            intercommunalite.setNomPresident(row.getAs("nomPresident"));
            intercommunalite.setPopulation(row.getAs("populationGroupement"));
            intercommunalite.setPrenomPresident(row.getAs("prenomPresident"));
            intercommunalite.setSiren(sirenIntercommunalite);
            intercommunalite.setSirenCommuneSiege(SIREN_COMMUNE_SIEGE.getAs(row));
            intercommunalite.setSiteInternet(row.getAs("siteInternet"));
            intercommunalite.setTelephoneSiege(row.getAs("telephoneSiege"));

            adresseSiege.setNom(intercommunalite.getNomGroupement());
            adresseSiege.setVoie(row.getAs("adresseSiege1"));
            adresseSiege.setComplementNom(row.getAs("adresseSiege2"));
            adresseSiege.setComplementVoie(row.getAs("adresseSiege3"));
            adresseSiege.setVille(NOM_COMMUNE_SIEGE.getAs(row));

            // Alimenter l'adresse annexe.
            Adresse adresseAnnexeSiege = new Adresse(adresseSiege);
            String codePostalEtVilleAnnexe = row.getAs("codePostalEtVilleAnnexe");

            if (codePostalEtVilleAnnexe != null) {
               int indexSpace = codePostalEtVilleAnnexe.indexOf(" ");
               String codePostal = indexSpace != -1 ? codePostalEtVilleAnnexe.substring(indexSpace) : codePostalEtVilleAnnexe;
               adresseAnnexeSiege.setCodePostal(codePostal);
            }

            adresseAnnexeSiege.setVoie(row.getAs("annexeAdresse1"));
            adresseAnnexeSiege.setComplementNom(row.getAs("annexeAdresse2"));
            adresseAnnexeSiege.setComplementVoie(row.getAs("annexeAdresse3"));
            intercommunalite.setAdresseAnnexe(adresseAnnexeSiege);

            intercommunalites.add(intercommunalite);
         }

         intercommunalite.setCodeCommuneSiege(codeCommuneSiege);
         intercommunalite.setCommuneSiege(communeSiege);
         intercommunalite.getCodesCommunesMembres().add(codeCommuneMembre);
         intercommunalite.getCommunesMembres().put(codeCommuneMembre, communeMembre);
         intercommunalite.getPopulationsMembres().put(codeCommuneMembre, POPULATION_MEMBRE.getAs(row));
      }

      // Vérifier que le nombre des communes membres déclaré des intercommunalités correspond à celui que nous avons obtenu.
      for(Intercommunalite intercommunalite : intercommunalites.values()) {
         int nombreMembresObtenus = intercommunalite.getCodesCommunesMembres().size();

         if (nombreMembresObtenus != intercommunalite.getNombreCommunesMembresDeclare()) {
            LOGGER.warn("L'intercommunalité {} - {} ({}) n'a reconstitué que {} communes membres sur les {} qu'elle aurait dû posséder.",
               intercommunalite.getSiren(), intercommunalite.getNomGroupement(), intercommunalite.getNatureJuridique(),
               nombreMembresObtenus, intercommunalite.getNombreCommunesMembresDeclare());
            intercommunalite.setComplete(false);
         }
         else {
            intercommunalite.setComplete(true);
         }
      }

      return intercommunalites;
   }

   /**
    * Détecter si une intercommunalité a une compétence, et alors lui l'attribuer.
    * @param row Row.
    * @param intercommunalite Intercommunalité.
    * @param codeCompetence Code de la compétence à tester.
    */
   private void detecterCompetenceIntercommunalite(Row row, Intercommunalite intercommunalite, String codeCompetence) {
      if (row.isNullAt(row.fieldIndex(codeCompetence)) == false) {
         if ("1".equals(row.getAs(codeCompetence))) {
            CompetenceGroupement competence = CompetenceGroupement.valueOf(codeCompetence);
            intercommunalite.getCompetences().add(competence);
         }
      }
   }

}
