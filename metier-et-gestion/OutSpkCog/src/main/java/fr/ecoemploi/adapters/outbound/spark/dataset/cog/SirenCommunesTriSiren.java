package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Tri des codes siren de communes par code siren<br>
 * @author Marc LE BIHAN
 */
public class SirenCommunesTriSiren extends SirenCommunesTri {
   @Serial
   private static final long serialVersionUID = 1151960669795723397L;

   /**
    * Constuire cette définition de tri.
    */
   public SirenCommunesTriSiren() {
      super("TRI-siren", false, null, false, SIREN_COMMUNE.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param siren siren, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String siren) {
      return siren != null ? SIREN_COMMUNE.col().equalTo(siren) : SIREN_COMMUNE.col().isNull();
   }
}
