package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.*;
import java.util.*;
import java.util.function.Supplier;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import org.apache.spark.sql.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.executor.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.CODE_DEPARTEMENT;

/**
 * Dataset du fichier des SIREN de communes.
 * @author Marc Le Bihan
 */
@Service
public class SirenCommunesDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -3295106125860463240L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(SirenCommunesDataset.class);

   /** Chargeur de données open data depuis le fichier CSV. */
   private final SirenCommunesCsvLoader loader;

   /**
    * Construire un dataset de siren de commune.
    * @param loader Chargeur de CSV
    */
   @Autowired
   public SirenCommunesDataset(SirenCommunesCsvLoader loader) {
      this.loader = loader;
   }

   /**
    * Obtenir un Dataset Row des SIREN des communes.
    * @param anneeCOG Année du Code Officiel Géographique de référence.
    * @param tri Tri à sélectionner, peut valoir null.
    * @param optionsCreationLecture Options de constitution du dataset.
    * @param historique Historique de constitution du dataset.
    * @return Dataset de SIREN des communes.
    */
   public Dataset<Row> rowSirenCommunes(OptionsCreationLecture optionsCreationLecture, HistoriqueExecution historique, SirenCommunesTri tri, int anneeCOG) {
      OptionsCreationLecture options = (optionsCreationLecture == null) ? optionsCreationLecture() : optionsCreationLecture;

      if (historique != null) {
         historique.aucuneDeRegleValidation(this.session);
      }

      Supplier<Dataset<Row>> worker = () -> {
         super.setStageDescription(this.messageSource, "row.siren.libelle.long", "row.siren.libelle.court", anneeCOG);
         LOGGER.info(this.messageSource.getMessage("row.siren.libelle.long", new Object[] {anneeCOG}, Locale.getDefault()));

         return this.loader.loadOpenData(anneeCOG);
      };

      // Si une restriction à quelques communes a été demandée, prendre toutes celles de leur départements
      Column postFiltre = restrictionAuxDepartementsDeCommunes(options, CODE_DEPARTEMENT, null, true);
      boolean anneeEligibleCache = options.anneeEligibleMiseEnCache(anneeCOG);

      Dataset<Row> sirenCommunaux = constitutionStandard(options, historique, worker, anneeEligibleCache, new ConditionPostFiltrageConstante(postFiltre),
         new CacheParqueteur<>(options, this.session, exportCSV(), "cog-siren-communaux", "annee_{0,number,#0}", tri, anneeCOG));

      sirenCommunaux = sirenCommunaux.cache();
      return sirenCommunaux;
   }
}
