package fr.ecoemploi.adapters.outbound.spark.dataset.groupement;

import java.io.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import static org.apache.spark.sql.types.DataTypes.*;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Chargeur de données brutes depuis un fichier CSV  BANATIC pour les fichiers de périmètres EPCI et syndicats
 * @author Marc Le Bihan
 */
@Component
public class EPCIPerimetreCsvLoader extends AbstractSparkCsvLoader {
   @Serial
   private static final long serialVersionUID = -8676731148519480574L;

   /** Nom du fichier CSV. */
   private final String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   private final String repertoireFichier;

   /** Session Spark. */
   private final SparkSession session;

   /**
    * Codes régions pour la lecture des fichiers BANATIC porteurs de tous les syndicats et des groupements SANS fiscalité propre également
    */
   private final String[] codesRegions = new String[] {
      "ARA", "BFC", "BRE", "COR", "CVL",
      "GES", "GUA", "GUY", "HDF", "IDF",
      "LRE", "MAY", "MTQ", "NAQ", "NOR",
      "OCC", "PAC", "PDL"
   };

   /**
    * Construire un chargeur de fichier csv pour les groupements.
    * @param nomFichier Nom du fichier csv
    * @param repertoireFichier Répertoire du fichier
    * @param session Session Spark
    */
   @Autowired
   public EPCIPerimetreCsvLoader(@Value("${perimetres.fichier.csv.nom}") String nomFichier, @Value("${territoire.dir}") String repertoireFichier, SparkSession session) {
      this.nomFichier = nomFichier;
      this.repertoireFichier = repertoireFichier;
      this.session = session;
   }

   /**
    * Charger le fichier CSV des périmètres d'EPCI et de syndicats.
    * @param annee Année à charger.
    * @return Dataset des périmètres.
    * @throws IllegalArgumentException s'il n'existe pas de fichier de groupements pour l'année désirée.
    */
   public Dataset<Row> loadOpenData(int annee) {
      StructType schema = schema(annee);
      Dataset<Row> csv = load(schema, annee);
      csv = rename(csv, annee);
      csv = cast(csv, annee);
      csv = select(csv, annee);

      return csv;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected StructType schema(int annee) {
      // Région siège               Département siège    Arrondissement siège Commune siège        N° SIREN    Nom du groupement          Nature juridique  Syndicat à la carte
      // 84 - Auvergne-Rhône-Alpes  01 - Ain             4 - Nantua           210102836 - Oyonnax  200042935   Haut - Bugey Agglomération CA                0
      //
      // Groupement interdépartemental Date de création  Date d'effet   Mode de répartition des sièges   Autre mode de répartition des sièges   Nombre de membres Population
      // 0                             2014-01-01        2014-01-01     ACCOR                                                                   42                65393
      //
      // Nombre de compétences exercées   Mode de financement  DGF Bonifiée   DSC   REOM
      // 35                               FPU

      // Autre redevance   TEOM  Autre taxe  Civilité Président   Prénom Président  Nom Président

      // Adresse du siège_1   Adresse du siège_2   Adresse du siège_3   Code postal du siège Ville du siège Téléphone du siège

      // Fax du siège   Courriel du siège Site internet  Adresse annexe_1  Adresse annexe_2
      // Adresse annexe_3  Code postal annexe Ville annexe  Téléphone annexe  Fax annexe
      // Type  Siren commune membre   Nom membre  Représentation-substitution   Compétence conservée Population membre
      // Adhésion siren    Adhésion nom   Adhésion population
      StructType schema = new StructType()
         .add("Région siège", StringType, false)
         .add("Département siège", StringType, false)
         .add("Arrondissement siège", StringType, true)
         .add("Commune siège", StringType, false)
         .add("N° SIREN", StringType, false)

         .add("Nom du groupement", StringType, false)
         .add("Nature juridique", StringType, false)
         .add("Syndicat à la carte", StringType, false)
         .add("Groupement interdépartemental", StringType, false)
         .add("Date de création", StringType, false)

         .add("Date d'effet", StringType, false)
         .add("Mode de répartition des sièges", StringType, false)
         .add("Autre mode de répartition des sièges", StringType, true)
         .add("Nombre de membres", IntegerType, false)
         .add("Population", IntegerType, true)

         .add("Nombre de compétences exercées", StringType, false)
         .add("Mode de financement", StringType, false)
         .add("DGF Bonifiée", StringType, false)
         .add("DSC", StringType, false)
         .add("REOM", StringType, false)

         .add("Autre redevance", StringType, true)
         .add("TEOM", StringType, false)
         .add("Autre taxe", StringType, true)
         .add("Civilité Président", StringType, true)
         .add("Prénom Président", StringType, true)

         .add("Nom Président", StringType, true)
         .add("Adresse du siège_1", StringType, true)
         .add("Adresse du siège_2", StringType, true)
         .add("Adresse du siège_3", StringType, true)
         .add("Code postal du siège Ville du siège", StringType, false)

         .add("Téléphone du siège", StringType, false)
         .add("Fax du siège", StringType, true)
         .add("Courriel du siège", StringType, true)
         .add("Site internet", StringType, true)
         .add("Adresse annexe_1", StringType, true)

         .add("Adresse annexe_2", StringType, true)
         .add("Adresse annexe_3", StringType, true)
         .add("Code postal annexe Ville annexe", StringType, true)
         .add("Téléphone annexe", StringType, true)
         .add("Fax annexe", StringType, true)

         .add("Type", StringType, false)
         .add("Siren membre", StringType, false)
         .add("Nom membre", StringType, false)
         .add("Représentation-substitution", StringType, true)
         .add("Compétence conservée", StringType, true)

         .add("Population membre", IntegerType, false);

      // Les fichiers de 2016 à 2023 ont un problème de nommage de colonnes sur trois champs
      if (annee >= 2016 && annee <= 2023) {
         schema = schema
            .add("Adhésion siren ", StringType, true)
            .add("Adhésion nom ", StringType, true)
            .add("Adhésion population ", StringType, true);
      }
      else {
         schema = schema
            .add("Adhésion siren", StringType, true)
            .add("Adhésion nom", StringType, true)
            .add("Adhésion population", StringType, true);
      }

      schema = schemaCompetences(schema, annee);
      return schema;
   }

   /**
    * {@inheritDoc}
    */
   protected Dataset<Row> load(StructType schema, int annee) {
      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      Dataset<Row> toutesRegions = null;

      for(String codeRegion : this.codesRegions) {
         File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, annee, this.nomFichier, codeRegion);
         AbstractSparkDataset.setStageDescription(this.session, "csv périmètres {0,number,#0}", annee);

         Dataset<Row> perimetresRegion = this.session.read().schema(schema).format("csv")
            .option("header", "true")
            .option("quote", "\"")
            .option("escape", "\"")
            .option("dec", ",")
            .option("sep", "\t")
            .option("encoding", "ISO-8859-1")
            .option("enforceSchema", false)
            .load(source.getAbsolutePath());

         toutesRegions = (toutesRegions != null) ? toutesRegions.union(perimetresRegion) : perimetresRegion;
      }

      return toutesRegions;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Dataset<Row> rename(Dataset<Row> dataset, int annee) {
      dataset = dataset
         .withColumnRenamed("Région siège", CODE_REGION.champ())
         .withColumnRenamed("Département siège", CODE_DEPARTEMENT.champ())
         .withColumnRenamed("Arrondissement siège", "arrondissementSiege")
         .withColumnRenamed("Commune siège", SIREN_COMMUNE_SIEGE.champ())
         .withColumnRenamed("N° SIREN", SIREN_GROUPEMENT.champ())

         .withColumnRenamed("Nom du groupement",NOM_GROUPEMENT.champ())
         .withColumnRenamed("Nature juridique", NATURE_JURIDIQUE.champ())
         .withColumnRenamed("Syndicat à la carte", "syndicatCarte")
         .withColumnRenamed("Groupement interdépartemental", "interdepartemental")
         .withColumnRenamed("Date de création", "dateCreation")

         .withColumnRenamed("Date d'effet", "dateEffet")
         .withColumnRenamed("Mode de répartition des sièges", "modeRepartitionSieges")
         .withColumnRenamed("Autre mode de répartition des sièges", "autreModeRepartitionSieges")
         .withColumnRenamed("Nombre de membres", "nombreCommunesMembres")
         .withColumnRenamed("Population", POPULATION_GROUPEMENT.champ())

         .withColumnRenamed("Nombre de compétences exercées", "nombreDeCompetencesExercees")
         .withColumnRenamed("Mode de financement", "modeDeFinancement")
         .withColumnRenamed("DGF Bonifiée", "DGFBonifiee")
         .withColumnRenamed("Autre redevance", "autreRedevance")
         .withColumnRenamed("Autre taxe", "autreTaxe")

         .withColumnRenamed("Civilité Président", "civilitePresident")
         .withColumnRenamed("Prénom Président", "prenomPresident")
         .withColumnRenamed("Nom Président", "nomPresident")
         .withColumnRenamed("Adresse du siège_1", "adresseSiege1")
         .withColumnRenamed("Adresse du siège_2", "adresseSiege2")

         .withColumnRenamed("Adresse du siège_3", "adresseSiege3")
         .withColumnRenamed("Code postal du siège Ville du siège", "codePostalEtVilleSiege")
         .withColumnRenamed("Téléphone du siège", "telephoneSiege")
         .withColumnRenamed("Fax du siège", "faxSiege")
         .withColumnRenamed("Courriel du siège", "courrielSiege")

         .withColumnRenamed("Site internet", "siteInternet")
         .withColumnRenamed("Adresse annexe_1", "annexeAdresse1")
         .withColumnRenamed("Adresse annexe_2", "annexeAdresse2")
         .withColumnRenamed("Adresse annexe_3", "annexeAdresse3")
         .withColumnRenamed("Code postal annexe Ville annexe", "codePostalEtVilleAnnexe")

         .withColumnRenamed("Téléphone annexe", "telephoneAnnexe")
         .withColumnRenamed("Fax annexe", "faxAnnexe")
         .withColumnRenamed("Type", "type")
         .withColumnRenamed("Siren membre", SIREN_MEMBRE.champ())
         .withColumn(SIREN_COMMUNE_MEMBRE.champ(), SIREN_MEMBRE.col())
         .withColumnRenamed("Nom membre", NOM_MEMBRE.champ())

         .withColumnRenamed("Représentation-substitution", "representationSubstitution")
         .withColumnRenamed("Compétence conservée", "competenceConservee")
         .withColumnRenamed("Population membre", POPULATION_MEMBRE.champ());

      // Les fichiers de 2016 à 2023 ont un problème de nommage de colonnes sur trois champs
      if (annee >= 2016 && annee <= 2023) {
         dataset = dataset
            .withColumnRenamed("Adhésion siren ", SIREN_ADHESION.champ())
            .withColumnRenamed("Adhésion nom ", NOM_ADHESION.champ())
            .withColumnRenamed("Adhésion population ", POPULATION_ADHESION.champ());
      }
      else {
         dataset = dataset
            .withColumnRenamed("Adhésion siren", SIREN_ADHESION.champ())
            .withColumnRenamed("Adhésion nom", NOM_ADHESION.champ())
            .withColumnRenamed("Adhésion population", POPULATION_ADHESION.champ());
      }

      return dataset;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Dataset<Row> cast(Dataset<Row> dataset, int annee) {
      StructType schema = schemaCompetences(new StructType(), annee);

      for(String champ : schema.fieldNames()) {
         // Les compétences deviennent des booléens
         dataset = dataset.withColumn(champ, col(champ).cast(BooleanType));
      }

      return dataset;
   }

   /**
    * Ajouter la partie compétences dans le schema.
    * @param schema Schéma à compléter.
    * @param anneeCOG Année du COG.
    * @return Schéma complété.
    */
   private StructType schemaCompetences(StructType schema, int anneeCOG) {
      schema = schema
         .add("C1004", StringType, true)

         .add("C1010", StringType, true)
         .add("C1015", StringType, true)
         .add("C1020", StringType, true)
         .add("C1025", StringType, true)
         .add("C1030", StringType, true)

         .add("C1502", StringType, true)
         .add("C1505", StringType, true)
         .add("C1507", StringType, true)
         .add("C1510", StringType, true)
         .add("C1515", StringType, true)

         .add("C1520", StringType, true)
         .add("C1525", StringType, true);

      if (anneeCOG >= 2020) {
         schema = schema.add("C1528", StringType, true);
         schema = schema.add("C1529", StringType, true);
      }

      schema = schema.add("C1530", StringType, true);

      if (anneeCOG >= 2020) {
         schema = schema.add("C1531", StringType, true);
         schema = schema.add("C1532", StringType, true);
         schema = schema.add("C1533", StringType, true);
         schema = schema.add("C1534", StringType, true);
      }

      if (anneeCOG >= 2018) {
         schema = schema.add("C1535", StringType, true);
      }

      schema = schema.add("C1540", StringType, true);

      if (anneeCOG >= 2018) {
         schema = schema.add("C1545", StringType, true);
         schema = schema.add("C1550", StringType, true);
         schema = schema.add("C1555", StringType, true);
         schema = schema.add("C1560", StringType, true);
      }

      schema = schema.add("C2005", StringType, true)
         .add("C2010", StringType, true)
         .add("C2015", StringType, true)
         .add("C2510", StringType, true)
         .add("C2515", StringType, true)
         .add("C2520", StringType, true);

      if (anneeCOG >= 2020) {
         schema = schema.add("C2521", StringType, true);
      }

      schema = schema.add("C2525", StringType, true);

      if (anneeCOG >= 2020) {
         schema = schema.add("C2526", StringType, true);
      }

      schema = schema.add("C3005", StringType, true);

      if (anneeCOG >= 2018) {
         schema = schema.add("C3010", StringType, true);
      }

      schema = schema.add("C3015", StringType, true)
         .add("C3020", StringType, true)

         .add("C3025", StringType, true)
         .add("C3210", StringType, true)
         .add("C3220", StringType, true)
         .add("C3505", StringType, true)
         .add("C3510", StringType, true)

         .add("C3515", StringType, true)
         .add("C4005", StringType, true)
         .add("C4006", StringType, true)
         .add("C4007", StringType, true);

      if (anneeCOG >= 2018) {
         schema = schema.add("C4008", StringType, true);
      }

      schema = schema.add("C4010", StringType, true)
         .add("C4015", StringType, true);

      if (anneeCOG >= 2018) {
         schema = schema.add("C4016", StringType, true);
      }

      schema = schema.add("C4017", StringType, true)
         .add("C4020", StringType, true)

         .add("C4025", StringType, true)
         .add("C4505", StringType, true)
         .add("C4510", StringType, true)
         .add("C4515", StringType, true)
         .add("C4520", StringType, true)

         .add("C4525", StringType, true)
         .add("C4530", StringType, true)
         .add("C4531", StringType, true)
         .add("C4532", StringType, true)
         .add("C4535", StringType, true)

         .add("C4540", StringType, true)
         .add("C4545", StringType, true)
         .add("C4550", StringType, true)
         .add("C4555", StringType, true)
         .add("C4560", StringType, true)

         .add("C5005", StringType, true)
         .add("C5010", StringType, true)
         .add("C5015", StringType, true)
         .add("C5210", StringType, true)
         .add("C5220", StringType, true)

         .add("C5505", StringType, true)
         .add("C5510", StringType, true)
         .add("C5515", StringType, true)
         .add("C5520", StringType, true)
         .add("C5525", StringType, true)

         .add("C5530", StringType, true)
         .add("C5535", StringType, true)
         .add("C5540", StringType, true)
         .add("C5545", StringType, true)
         .add("C5550", StringType, true)

         .add("C5555", StringType, true)
         .add("C7005", StringType, true)
         .add("C7010", StringType, true);

      if (anneeCOG >= 2018) {
         schema = schema.add("C7012", StringType, true);
      }

      schema = schema.add("C7015", StringType, true)
         .add("C7020", StringType, true)
         .add("C7025", StringType, true)
         .add("C7030", StringType, true)
         .add("C9905", StringType, true)
         .add("C9910", StringType, true)

         .add("C9915", StringType, true)
         .add("C9920", StringType, true)
         .add("C9922", StringType, true);

      if (anneeCOG >= 2018) {
         schema = schema.add("C9923", StringType, true);
      }

      if (anneeCOG >= 2020) {
         schema = schema.add("C9924", StringType, true);
      }

      schema = schema.add("C9925", StringType, true)
         .add("C9930", StringType, true)
         .add("C9935", StringType, true);

      if (anneeCOG >= 2018) {
         schema = schema.add("C9940", StringType, true);
      }

      schema = schema.add("C9950", StringType, true)
         .add("C9999", StringType, true);

      return schema;
   }
}
