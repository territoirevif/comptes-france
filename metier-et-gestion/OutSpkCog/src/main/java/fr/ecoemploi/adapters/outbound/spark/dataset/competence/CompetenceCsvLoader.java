package fr.ecoemploi.adapters.outbound.spark.dataset.competence;

import java.io.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import static org.apache.spark.sql.types.DataTypes.*;
import static org.apache.spark.sql.functions.*;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Chargeur de codes compétences depuis un fichier CSV
 * @author Marc Le Bihan
 */
@Component
public class CompetenceCsvLoader extends AbstractSparkCsvLoader {
   @Serial
   private static final long serialVersionUID = 3586365476091287126L;

   /** Nom du fichier CSV. */
   private final String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   private final String repertoireFichier;

   /** Session Spark. */
   private final SparkSession session;

   /**
    * Construire un chargeur de fichier csv pour les compétences.
    * @param nomFichier Nom du fichier csv
    * @param repertoireFichier Répertoire du fichier
    * @param session Session Spark
    */
   @Autowired
   public CompetenceCsvLoader(@Value("${competences.fichier.csv.nom}") String nomFichier, @Value("${territoire.dir}") String repertoireFichier, SparkSession session) {
      this.nomFichier = nomFichier;
      this.repertoireFichier = repertoireFichier;
      this.session = session;
   }

   /**
    * Charger le fichier CSV des compétences d'EPCI et de syndicats.
    * @param annee Année à charger.
    * @return Dataset des compétences.
    * @throws IllegalArgumentException s'il n'existe pas de fichier de compétences pour l'année désirée.
    */
   public Dataset<Row> loadOpenData(int annee) {
      StructType schema = schema(annee);
      Dataset<Row> csv = load(schema, 0); // Le fichier est toujours pris de la racine (année zéro)
      csv = rename(csv, annee);
      csv = cast(csv, annee);
      csv = select(csv, annee);

      return csv;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected StructType schema(int annee) {
      // "code","nom","categorie"
      // "1010","Hydraulique ","10"
      // "1030","Autres énergies ","10"
      // "1502","Eau (Traitement, Adduction, Distribution) ","15"
      return new StructType()
         .add("code", StringType, false)
         .add("nom", StringType, false)
         .add("categorie", StringType, false);
   }

   /**
    * {@inheritDoc}
    */
   protected Dataset<Row> load(StructType schema, int annee) {
      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);

      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, annee, this.nomFichier);
      AbstractSparkDataset.setStageDescription(this.session, "csv competences");

      return this.session.read().schema(schema).format("csv")
         .option("header", "true")
         .option("quote", "\"")
         .option("escape", "\"")
         .option("dec", ",")
         .option("sep", ",")
         .option("encoding", "UTF-8")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath());
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Dataset<Row> cast(Dataset<Row> dataset, int annee) {
      return dataset.withColumn(NOM_COMPETENCE.champ(), trim(NOM_COMPETENCE.col())) // Supprimer des espaces en fin de nom
         .withColumn(TYPE_COMPETENCE.champ(), lit(""))              // Obligatoire, Facultative, Optionnelle, NonApplicable
         .withColumn(EN_VIGUEUR_COMPETENCE.champ(), lit("")); // Inconnue [avant apparition], Active, Obsolete [supprimée/remplacée]
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Dataset<Row> rename(Dataset<Row> dataset, int annee) {
      return dataset.withColumnRenamed("code", CODE_COMPETENCE.champ())
         .withColumnRenamed("nom", NOM_COMPETENCE.champ())
         .withColumnRenamed("categorie", CATEGORIE_COMPETENCE.champ());
   }
}
