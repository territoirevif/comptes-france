package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.*;
import java.util.Map;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;

import static org.apache.spark.sql.types.DataTypes.*;

/**
 * Chargeur de fichier CSV pour les évènements communaux
 * @author Marc Le Bihan
 */
@Component
public class EvenementsCommunauxRowCsvLoader extends AbstractSparkCsvLoader {
   @Serial
   private static final long serialVersionUID = -8174307368723931008L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(EvenementsCommunauxRowCsvLoader.class);

   /** Nom du fichier CSV. */
   @Value("${evenements-communaux.fichier.csv.nom}")
   private String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${territoire.dir}")
   private String repertoireFichier;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Charger un dataset.
    * @param annee Année des évènements communaux.
    * @param sampleFraction Fraction de sample. Pas de sample réalisé si égal à 0.0
    * @param sampleSeed seed utilisé pour le sample. Pas de seed si égal à 0 ou null.
    * @return Ensemble des données attributaires accessibles sur les évènements communaux.
    */
   public Dataset<Row> loadOpenData(int annee, double sampleFraction, Long sampleSeed) {
      LOGGER.info("Constitution du dataset des évènements communaux de {}...", annee);

      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, annee, this.nomFichier);

      return rename(load(schema(), source, sampleFraction, sampleSeed), annee);
   }

   /**
    * Renvoyer le schéma du CSV source<br>
    * (Schéma 2024)
    * @return Schema.
    */
   protected StructType schema() {
      return new StructType()
         .add("MOD", IntegerType, false)         // Type d'événement de communes
         .add("DATE_EFF", DateType, false)      // Date d'effet (AAAA-MM-JJ)

         .add("TYPECOM_AV", StringType, false)  // Type de la commune avant événement
         .add("COM_AV", StringType, false)      // Code de la commune avant événement
         .add("TNCC_AV", StringType, false)     // Type de nom en clair
         .add("NCC_AV", StringType, false)      // Nom en clair (majuscules)
         .add("NCCENR_AV", StringType, false)   // Nom en clair (typographie riche)
         .add("LIBELLE_AV", StringType, false)  // Nom en clair (typographie riche) avec article

         .add("TYPECOM_AP", StringType, false)  // Type de commune après l'événement
         .add("COM_AP", StringType, false)      // Code de la commune après l'événement
         .add("TNCC_AP", StringType, false)     // Type de nom en clair
         .add("NCC_AP", StringType, false)      // Nom en clair (majuscules)
         .add("NCCENR_AP", StringType, false)   // Nom en clair (typographie riche)
         .add("LIBELLE_AP", StringType, false); // Nom en clair (typographie riche) avec article
   }

   /**
    * Renommer les champs du dataset.
    * @param dataset Dataset.
    */
   @Override
   protected Dataset<Row> rename(Dataset<Row> dataset, int annee) {
      Map<String, String> r1 = Map.of(
         "MOD", "typeEvenementCommunal",
         "DATE_EFF", "dateEffetEvenementCommunal",
         "TYPECOM_AV", "typeCommuneAvantEvenement",
         "COM_AV", "codeCommuneAvantEvenement",
         "TNCC_AV", "typeDeNomAvantEvenement",

         "NCC_AV", "nomCommuneMajusculesAvantEvenement",
         "NCCENR_AV", "nomCommuneAvantEvenement",
         "LIBELLE_AV", "nomCommuneAvecArticleAvantEvenement",
         "TYPECOM_AP", "typeCommuneApresEvenement",
         "COM_AP", "codeCommuneApresEvenement"
      );

      Map<String, String> r2 = Map.of(
         "TNCC_AP", "typeDeNomApresEvenement",
         "NCC_AP", "nomCommuneMajusculesApresEvenement",
         "NCCENR_AP", "nomCommuneApresEvenement",
         "LIBELLE_AP", "nomCommuneAvecArticleApresEvenement"
      );

      Dataset<Row> rename = super.renameFields(dataset, r1);
      rename = super.renameFields(rename, r2);

      return cast(rename, annee).selectExpr("*");
   }

   /**
    * Provoquer le chargement effectif de la source de données.
    * @param schema Schéma.
    */
   protected Dataset<Row> load(StructType schema, File source) {
      return load(schema, source, 0.0, null);
   }

   /**
    * Provoquer le chargement effectif de la source de données.
    * @param sampleFraction Fraction de sample. Pas de sample réalisé si égal à 0.0
    * @param sampleSeed seed utilisé pour le sample. Pas de seed si égal à 0 ou null.
    * @param schema Schéma.
    */
   protected Dataset<Row> load(StructType schema, File source, double sampleFraction, Long sampleSeed) {
      return applySample(this.session.read().schema(schema).format("csv")
         .option("header","true")
         .option("delimiter", ",")
         .option("encoding", "UTF-8")
         .option("dateFormat", "yyyy-MM-dd")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath()).selectExpr("*"), sampleFraction, sampleSeed);
   }
}
