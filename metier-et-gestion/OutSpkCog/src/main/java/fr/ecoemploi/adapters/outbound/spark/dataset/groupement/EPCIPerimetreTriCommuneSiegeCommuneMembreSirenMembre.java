package fr.ecoemploi.adapters.outbound.spark.dataset.groupement;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Partitionnement par département et tri par code de la commune siège, code de la commune membre (si applicable) et siren du membre d'un EPCI ou syndicat
 * @author Marc Le Bihan
 */
public class EPCIPerimetreTriCommuneSiegeCommuneMembreSirenMembre extends EPCIPerimetreTri {
   @Serial
   private static final long serialVersionUID = 558108813398685359L;

   /**
    * Constuire cette définition de tri.
    */
   public EPCIPerimetreTriCommuneSiegeCommuneMembreSirenMembre() {
      super("PARTITIONNEMENT-departement-TRI-commune_siege-commune_membre-siren_membre", true,
         CODE_DEPARTEMENT.champ(), false,
         CODE_DEPARTEMENT.champ(), CODE_COMMUNE_SIEGE.champ(), CODE_COMMUNE_MEMBRE.champ(), SIREN_MEMBRE.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @param codeCommuneSiege Code commune siège, si null sera testé avec <code>isNull</code>
    * @param codeCommuneMembre Code commune, si null sera testé avec <code>isNull</code>
    * @param sirenMembre Siren membre, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommuneSiege, String codeCommuneMembre, String sirenMembre) {
      return equalTo(codeDepartement, codeCommuneSiege, codeCommuneMembre)
         .and(sirenMembre != null ? SIREN_MEMBRE.col().equalTo(sirenMembre) : SIREN_MEMBRE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @param codeCommuneSiege Code commune siège, si null sera testé avec <code>isNull</code>
    * @param codeCommuneMembre Code commune, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommuneSiege, String codeCommuneMembre) {
      return equalTo(codeDepartement, codeCommuneSiege)
         .and(codeCommuneMembre != null ? CODE_COMMUNE_MEMBRE.col().equalTo(codeCommuneMembre) : CODE_COMMUNE_MEMBRE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @param codeCommuneSiege Code commune siège, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommuneSiege) {
      return equalTo(codeDepartement)
         .and(codeCommuneSiege != null ? CODE_COMMUNE_SIEGE.col().equalTo(codeCommuneSiege) : CODE_COMMUNE_SIEGE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
