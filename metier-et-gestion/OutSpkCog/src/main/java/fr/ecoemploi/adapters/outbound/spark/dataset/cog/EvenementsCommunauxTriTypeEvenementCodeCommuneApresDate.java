package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.Serial;
import java.time.LocalDate;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Tri d'évènements communaux par Type d'évènement communal, Code Commune après changement, puis date d'effet.
 * @author Marc LE BIHAN
 */
public class EvenementsCommunauxTriTypeEvenementCodeCommuneApresDate extends EvenementsCommunauxTri {
   @Serial
   private static final long serialVersionUID = 6670742324750048579L;

   /**
    * Constuire cette définition de tri.
    */
   public EvenementsCommunauxTriTypeEvenementCodeCommuneApresDate() {
      super("TRI-type_evenement-commune_apres_evenement-dateEvenement", true, null, false, TYPE_EVENEMENT_COMMUNAL.champ(), CODE_COMMUNE_APRES_EVENEMENT.champ(), DATE_EFFET_EVENEMENT_COMMUNAL.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param typeEvenementCommunal Type d'évènement communal, si null sera testé avec isNull
    * @param codeCommune Code commune, si null sera testé avec isNull
    * @param dateEffet Date d'effet de l'évènement, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String typeEvenementCommunal, String codeCommune, LocalDate dateEffet) {
      return equalTo(typeEvenementCommunal, codeCommune)
         .and(dateEffet != null ? DATE_EFFET_EVENEMENT_COMMUNAL.col().equalTo(dateEffet) : DATE_EFFET_EVENEMENT_COMMUNAL.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param typeEvenementCommunal Type d'évènement communal, si null sera testé avec isNull
    * @param codeCommune Code commune, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String typeEvenementCommunal, String codeCommune) {
      return equalTo(typeEvenementCommunal)
         .and(codeCommune != null ? CODE_COMMUNE_APRES_EVENEMENT.col().equalTo(codeCommune) : CODE_COMMUNE_APRES_EVENEMENT.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param typeEvenementCommunal Type d'évènement communal, si null sera testé avec isNull
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String typeEvenementCommunal) {
      return typeEvenementCommunal != null ? TYPE_EVENEMENT_COMMUNAL.col().equalTo(typeEvenementCommunal) : TYPE_EVENEMENT_COMMUNAL.col().isNull();
   }
}
