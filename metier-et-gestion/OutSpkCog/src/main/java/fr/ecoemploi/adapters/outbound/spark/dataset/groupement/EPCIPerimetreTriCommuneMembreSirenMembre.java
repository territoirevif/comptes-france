package fr.ecoemploi.adapters.outbound.spark.dataset.groupement;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Partitionnement par département et tri par code commune membre (si applicable) et siren du membre de périmètres d'EPCI ou de syndicats
 * @author Marc Le Bihan
 */
public class EPCIPerimetreTriCommuneMembreSirenMembre extends EPCIPerimetreTri {
   @Serial
   private static final long serialVersionUID = -9150037537669778197L;

   /**
    * Constuire cette définition de tri.
    */
   public EPCIPerimetreTriCommuneMembreSirenMembre() {
      super("PARTITIONNEMENT-departement-TRI-commune_membre-siren_membre", true,
         CODE_DEPARTEMENT.champ(), false,
         CODE_DEPARTEMENT.champ(), CODE_COMMUNE_MEMBRE.champ(), SIREN_MEMBRE.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @param codeCommuneMembre Code commune, si null sera testé avec <code>isNull</code>
    * @param sirenMembre Siren membre, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommuneMembre, String sirenMembre) {
      return equalTo(codeDepartement, codeCommuneMembre)
         .and(sirenMembre != null ? SIREN_MEMBRE.col().equalTo(sirenMembre) : SIREN_MEMBRE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @param codeCommuneMembre Code commune membre, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement, String codeCommuneMembre) {
      return equalTo(codeDepartement)
         .and(codeCommuneMembre != null ? CODE_COMMUNE_MEMBRE.col().equalTo(codeCommuneMembre) : CODE_COMMUNE_MEMBRE.col().isNull());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeDepartement Code département, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeDepartement) {
      return codeDepartement != null ? CODE_DEPARTEMENT.col().equalTo(codeDepartement) : CODE_DEPARTEMENT.col().isNull();
   }
}
