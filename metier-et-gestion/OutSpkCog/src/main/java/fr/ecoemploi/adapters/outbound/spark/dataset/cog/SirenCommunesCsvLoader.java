package fr.ecoemploi.adapters.outbound.spark.dataset.cog;

import java.io.*;
import java.text.MessageFormat;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;
import fr.ecoemploi.adapters.outbound.spark.dataset.core.verifications.Verification;

import static org.apache.spark.sql.types.DataTypes.*;

/**
 * Chargeur de fichier CSV.
 * @author Marc Le Bihan
 */
@Component
public class SirenCommunesCsvLoader implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -7627230346188532853L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(SirenCommunesCsvLoader.class);

   /** Nom du fichier CSV. */
   @Value("${siren_communes.fichier.csv.nom}")
   private String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   @Value("${territoire.dir}")
   private String repertoireFichier;

   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Charger et renommer le fichier CSV du Dataset.
    * @param anneeCOG Année du Code Officiel Géographique.
    * @param verifications Vérifications demandées.
    * @return Dataset des SIREN communaux.
    */
   public Dataset<Row> loadOpenData(int anneeCOG, @SuppressWarnings("unused") Verification... verifications) {
      LOGGER.info("Acquisition des SIREN des communes pour le COG d'année {}...", anneeCOG);

      File repertoireParent = AbstractSparkDataset.assertExistenceRepertoire(this.repertoireFichier);
      File source = AbstractSparkDataset.assertExistenceFichierAnnuel(repertoireParent, anneeCOG, this.nomFichier);

      String nomChampPopulationTotale = MessageFormat.format("ptot_{0,number,#0}", anneeCOG);
      String nomChampPopulationMunicipale = MessageFormat.format("pmun_{0,number,#0}", anneeCOG);
      String nomChampPopulationCompteeApart = MessageFormat.format("pcap_{0,number,#0}", anneeCOG);

      String champRegion = switch(anneeCOG) {
         case 2016 -> "reg";
         case 2017, 2018 -> "reg_com";
         default -> "Reg_com";
      };

      StructType schema = new StructType()
         .add(champRegion, StringType, false)
         .add("dep_com", StringType, false)
         .add("siren", StringType, false)
         .add("insee", StringType, false)
         .add("nom_com", StringType, false)
         .add(nomChampPopulationTotale, IntegerType, false)
         .add(nomChampPopulationMunicipale, IntegerType, false)
         .add(nomChampPopulationCompteeApart, IntegerType, false);

      AbstractSparkDataset.setStageDescription(this.session, "csv siren communaux {0,number,#0}", anneeCOG);

      return this.session.read().schema(schema).format("csv")
         .option("header","true")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath())
         .selectExpr("*")
         .withColumnRenamed(champRegion, "codeRegion")
         .withColumnRenamed("dep_com", "codeDepartement")
         .withColumnRenamed("siren", "sirenCommune")
         .withColumnRenamed("insee", "codeCommune")
         .withColumnRenamed("nom_com", "nomCommune")
         .withColumnRenamed(nomChampPopulationTotale, "populationTotale")
         .withColumnRenamed(nomChampPopulationMunicipale, "populationMunicipale")
         .withColumnRenamed(nomChampPopulationCompteeApart, "populationCompteApart");
   }
}
