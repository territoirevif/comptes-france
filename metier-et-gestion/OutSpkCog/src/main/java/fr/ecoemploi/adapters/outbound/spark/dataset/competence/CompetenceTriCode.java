package fr.ecoemploi.adapters.outbound.spark.dataset.competence;

import java.io.Serial;

import org.apache.spark.sql.Column;

import static fr.ecoemploi.adapters.outbound.spark.dataset.core.metadata.NomChamp.*;

/**
 * Tri par code de compétence, aucun partionnement
 * @author Marc Le Bihan
 */
public class CompetenceTriCode extends CompetenceTri {
   @Serial
   private static final long serialVersionUID = -9121552125067325818L;

   /**
    * Constuire cette définition de tri.
    */
   public CompetenceTriCode() {
      super("TRI-code_competence", false,
         null, false,
         CODE_COMPETENCE.champ());
   }

   /**
    * Donner la clef de jointure pour un tri.
    * @param codeCompetence Code compétence, si null sera testé avec <code>isNull</code>
    * @return Colonne condition de jointure.
    */
   public Column equalTo(String codeCompetence) {
      return codeCompetence != null ? CODE_COMPETENCE.col().equalTo(codeCompetence) : CODE_COMPETENCE.col().isNull();
   }
}
