package fr.ecoemploi.adapters.outbound.spark.dataset.mobilite;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * Application de test pour Spark
 * @author Marc LE BIHAN
 */
@SpringBootApplication
@ComponentScan(basePackages={"fr", "com"})
public class SparkMobiliteTestApplication {
}
