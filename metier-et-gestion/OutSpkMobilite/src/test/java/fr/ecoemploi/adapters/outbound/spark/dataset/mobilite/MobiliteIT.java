package fr.ecoemploi.adapters.outbound.spark.dataset.mobilite;

import java.io.*;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertTrue;

import fr.ecoemploi.adapters.outbound.spark.AbstractRestIT;
import fr.ecoemploi.adapters.outbound.spark.dataset.cog.CogDataset;

/**
 * Tests sur la mobilité.
 * @author Marc Le Bihan
 */
@ActiveProfiles("test")
@SpringBootTest(classes = SparkMobiliteTestApplication.class)
class MobiliteIT extends AbstractRestIT implements Serializable {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = 7952382483920970461L;

   /** Dataset des flux d'actis de plus de 15 ans avec emploi. */
   @Autowired
   private FluxActifs15AnsAvecEmploiDataset fluxActifsDataset;
   
   /** Dataset du Code Officiel Géographique */
   @Autowired
   private CogDataset cogDataset;
   
   /** Session Spark. */
   @Autowired
   private SparkSession session;

   /**
    * Lecture des flux d'actifs.
    */
   @Test
   @DisplayName("Flux d'actifs entre domicile et travail")
   void trajetsDomicileTravail() {
      Dataset<Row> ds = this.fluxActifsDataset.rowFluxDomicileTravailPlusDe15ansAvecEmploi(this.session, 2017);
      
      ds.show(500, false);
      assertTrue(ds.count() != 0, "Une information de mobilité au moins aurait du être lue");
   }

   /**
    * Flux domicile travail dans une intercommunalité.
    */
   @Test
   @DisplayName("Flux d'actifs entre domicile et travail dans une intercommunalité")
   void trajetsDomicileTravailInterco() {
      Dataset<Row> communes = this.cogDataset.rowCommunes(null, null, 2020, false).where("typeCommune = 'COM' and codeEPCI = '242900645'");
      Dataset<Row> mobilite = this.fluxActifsDataset.mobiliteDomicileTravail(this.session, communes, "29");
      
      this.fluxActifsDataset.exportCSV().exporterCSV(mobilite, "csv_flux_mobilite_", true);
      
      assertTrue(mobilite.count() != 0, "Une information de mobilité au moins aurait du être lue");
   }
}
