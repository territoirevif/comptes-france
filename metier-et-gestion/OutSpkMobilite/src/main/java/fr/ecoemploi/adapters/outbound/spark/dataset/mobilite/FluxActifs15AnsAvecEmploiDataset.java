package fr.ecoemploi.adapters.outbound.spark.dataset.mobilite;

import fr.ecoemploi.adapters.outbound.spark.dataset.core.*;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.Serial;
import java.util.*;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.*;

/**
 * Dataset de flux d'actifs de plus de 15 ans avec emploi (trajets domicile-travail),<br>
 * issus du recensement de la population 2017, données de 2016.
 * @author Marc Le Bihan
 */
@Service
public class FluxActifs15AnsAvecEmploiDataset extends AbstractSparkDataset {
   /** Serial ID. */
   @Serial
   private static final long serialVersionUID = -6471453800291293258L;

   /** Logger. */
   private static final Logger LOGGER = LoggerFactory.getLogger(FluxActifs15AnsAvecEmploiDataset.class);

   /** Nom du fichier CSV. */
   private final String nomFichier;

   /** Chemin d'accès au répertoire du fichier. */
   private final String repertoireFichier;

   /**
    * Construire un dataset de lecture des flux d'actifs se déplaçant.
    * @param fichier Nom du fichier de recensement.
    * @param repertoire Répertoire du fichier.
    */
   @Autowired
   public FluxActifs15AnsAvecEmploiDataset(@Value("${mobilite.fichier.csv.nom}") String fichier,
      @Value("${mobilite.dir}") String repertoire) {
      this.repertoireFichier = repertoire;
      this.nomFichier = fichier;
   }

   /**
    * Obtenir un Dataset Row des flux de d'actifs de plus de 15 ans avec emploi sur leurs trajets domicile-travail.
    * @param session Session Spark.
    * @param annee Année considérée.
    * @return Dataset des flux de trajets domicile-travail.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> rowFluxDomicileTravailPlusDe15ansAvecEmploi(SparkSession session, int annee) {
      LOGGER.info("Acquisition des flux de trajets domicile-travail des plus de 15 ans avec emploi pour l'année {}...", annee);

      File repertoireParent = assertExistenceRepertoire(this.repertoireFichier);
      File source = assertExistenceFichierAnnuel(repertoireParent, annee, this.nomFichier);
      
      return session.read().schema(schema()).format("csv")
         .option("header","true")
         .option("sep", ";")
         .option("enforceSchema", false)
         .load(source.getAbsolutePath())
         .selectExpr("*")
         .withColumnRenamed("CODGEO", "code_commune_origine")
         .withColumnRenamed("LIBGEO", "nom_commune_origine")
         .withColumnRenamed("DCLT", "code_commune_destination")
         .withColumnRenamed("L_DCLT", "nom_commune_destination")
         .withColumnRenamed("NBFLUX_C17_ACTOCC15P", "flux_actifs_avec_emploi_plus_de_15ans");
   }
   
   /**
    * Créer la couche de la mobilité domicile travail des plus de 15 ans ayant un emploi.
    * @param session Session Spark. 
    * @param communes Dataset de communes à considérer.
    * @param codeDepartement Département d'examen.
    * @return Dataset de données de mobilité.
    */
   @SuppressWarnings("java:S1192") // Noms des colonnes répétées dans tout le source
   public Dataset<Row> mobiliteDomicileTravail(SparkSession session, Dataset<Row> communes, String codeDepartement) {
      Dataset<Row> mobilite = this.rowFluxDomicileTravailPlusDe15ansAvecEmploi(session, 2017);

      // Lister toutes les communes des villes de notre étude.
      List<Row> codesCommunesRow = communes.selectExpr("codeCommune").collectAsList();
      List<String> codesCommunesEtude = new ArrayList<>();
      codesCommunesRow.forEach(r -> codesCommunesEtude.add(r.getString(0)));
      Object[] etude = codesCommunesEtude.toArray();

      Column join = communes.col("codeCommune").equalTo(col("code_commune_origine"))
         .or(communes.col("codeCommune").equalTo(col("code_commune_destination")));

      mobilite = mobilite.join(communes, join, "leftsemi");

      // Travaille dans sa commune si la commune d'origine et de destination sont les mêmes.
      Column commune = mobilite.col("code_commune_origine").equalTo(mobilite.col("code_commune_destination"));

      // Travaille dans l'ensemble d'étude si la commune source comme destination y sont toutes les deux.
      Column ensembleDepart = mobilite.col("code_commune_destination").isin(etude).and(mobilite.col("code_commune_origine").isin(etude));

      // Travaille dans le département si la commune de destination est dedans,
      // que la commune de départ est dans celle de l'ensemble d'étude,
      // mais que celle de destination n'y est pas.
      Column departement = mobilite.col("code_commune_destination").startsWith(codeDepartement)
         .and(mobilite.col("code_commune_origine").isin(etude))
         .and(not(mobilite.col("code_commune_destination").isin(etude)));

      // Vient du département si la commune d'origine est dedans, mais n'est pas dans l'ensemble d'étude,
      // mais que celle de destination est dans l'ensemble d'étude.
      Column immigreDepuisDepartement = mobilite.col("code_commune_origine").startsWith(codeDepartement)
         .and(mobilite.col("code_commune_destination").isin(etude))
         .and(not(mobilite.col("code_commune_origine").isin(etude)));

      // Emigre hors du département si la commune de destination n'est pas dans le département.
      Column emigreHorsDepartement = not(mobilite.col("code_commune_destination").startsWith(codeDepartement));

      // Immigre dans le département si la commune d'origine n'est pas dans le département.
      Column immigreDansDepartement = not(mobilite.col("code_commune_origine").startsWith(codeDepartement));

      // Déterminer les types de trajets.
      mobilite = mobilite.withColumn("type_trajet",
         when(departement, lit("Emigre_Vers_Departement"))                  // Travaille dans le département.
         .when(immigreDepuisDepartement, lit("Immigre_Depuis_Departement")) // Vient travailler depuis le département.
         .when(commune, lit("Commune"))                                     // Travaille dans sa commune.
         .when(ensembleDepart, lit("Ensemble_Depart"))                      // Travaille dans une des communes de départ (exemple : intercommunalité).
         .when(emigreHorsDepartement, lit("Emigre_Hors_Departement"))       // Travaille hors du département.
         .when(immigreDansDepartement, lit("Immigre_Depuis_France"))        // Nous rejoint depuis un autre département.
         .otherwise(lit("Indetermine"))                                     // Indéterminé.
      );
      
      return mobilite;
   }   
   
   /**
    * Renvoyer le schema du dataset.
    * @return schema.
    */
   public StructType schema() {
      /* CODGEO;LIBGEO;DCLT;L_DCLT;NBFLUX_C17_ACTOCC15P
         01001;L'Abergement-Clémenciat;01001;L'Abergement-Clémenciat;76.0998155247401
         01001;L'Abergement-Clémenciat;01053;Bourg-en-Bresse;35.4057922591865
         01001;L'Abergement-Clémenciat;01093;Châtillon-sur-Chalaronne;55.5485039157492  */

      StructType schema = new StructType();
      schema = schema.add("CODGEO", StringType, false)
         .add("LIBGEO", StringType, false)
         .add("DCLT", StringType, false)
         .add("L_DCLT", StringType, false)
         .add("NBFLUX_C17_ACTOCC15P", DoubleType, false);

      return schema;
   }
}
